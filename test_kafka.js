const { Kafka } = require("kafkajs");

const kafka = new Kafka({
  clientId: "getgps",
  brokers: ["localhost:9092"]
});

const consumer = kafka.consumer({ groupId: "sensor-telemetry" });

async function init() {
  await consumer.connect();
  await consumer.subscribe({ topic: "sensors", fromBeginning: true });

  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      console.log({
        value: message.value.toString()
      });
    }
  });
}

init();
