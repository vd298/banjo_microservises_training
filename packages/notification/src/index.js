import db from "@lib/db";
import queue from "@lib/queue";
import request from "@lib/request";
import crypto from "crypto";

function makeCallbackData(data, secret) {
  const out = {
    data: Buffer.from(JSON.stringify(data)).toString("base64")
  };
  const sign = crypto
    .createHmac("sha256", secret)
    .update(out.data)
    .digest("hex");

  out.sign = Buffer.from(sign).toString("base64");
  return out;
}

async function getUserById(user_id) {
  const user = db.user.findOne({
    where: { id: user_id },
    raw: true
  });
  return user;
}

async function getMerchantById(merchant_id) {
  const merchant = await db.merchant.findOne({
    where: { id: merchant_id },
    raw: true
  });
  return merchant;
}

async function sendEmail(code, to, user, msg, lang) {
  const data = {
    lang,
    code,
    to,
    body: { msg }
  };

  await queue.newJob("mail-service", {
    method: "send",
    data,
    realmId: user.realm
  });
}

async function sendSupportMessage(code, user, msg) {
  const data = {
    message: msg,
    user_id: user.id
  };

  await queue.newJob("support-service", {
    method: "sendNotification",
    data,
    realmId: user.realm,
    userId: user.id
  });
}

async function sendCallback(code, merchant, msg, counter) {
  let callback = merchant.callback;
  const dataTosend = makeCallbackData(msg, merchant.secret);
  const result = await request.post(callback, dataTosend);
  if (result.status == 200) return;

  if (counter === 0) return;
  if (!counter) counter = 10;
  setTimeout(() => {
    sendCallback(code, merchant, msg, counter - 1);
  }, 1000);
}

async function send(data) {
  if (!data.data.merchant_id) throw "MERCHANTIDNOTFOUND";

  const merchant = await getMerchantById(data.data.merchant_id);
  const user = await getUserById(merchant.user_id);

  if (!user) throw "USERNOTFOUND";

  if (data.channels.includes("email") && !data.data.email)
    data.data.email = user.email;

  const lang = data.lang || "en";

  if (
    data.channels.includes("email") &&
    data.data.email &&
    data.data.message_email
  ) {
    await sendEmail(
      data.code,
      data.data.email,
      user,
      data.data.message_email,
      lang
    );
  }

  if (data.channels.includes("support") && data.data.message_support) {
    await sendSupportMessage(data.code, user, data.data.message_support);
  }

  if (data.channels.includes("callback") && data.data.message_callback) {
    sendCallback(data.code, merchant, data.data.message_callback);
  }
  return { success: true };
}

export default {
  send
};
