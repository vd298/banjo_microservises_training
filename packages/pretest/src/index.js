import db from "@lib/db";
import crypto from "crypto";
import memstore from "@lib/memstore";
import callbackServer from "./callback_server";
import uuid from "uuid/v4";

let result = {};

function wait(tm) {
  return new Promise((res, rej) => {
    setTimeout(() => {
      res();
    }, tm);
  });
}

async function before() {
  callbackServer.shutdown();
  callbackServer.run();

  await db.user.destroy({
    truncate: true,
    cascade: true
  });
  await db.account.destroy({
    truncate: true,
    cascade: true
  });
  await db.merchant_accounts.destroy({
    truncate: true,
    cascade: true
  });
  await db.permission.destroy({
    truncate: true,
    cascade: true
  });
  await db.resource.destroy({
    truncate: true,
    cascade: true
  });
  await db.user_account_invitations.destroy({
    truncate: true,
    cascade: true
  });
  await db.user_accounts.destroy({
    truncate: true,
    cascade: true
  });
  await db.currency.destroy({
    truncate: true,
    cascade: true
  });

  await db.banks.destroy({
    truncate: true,
    cascade: true
  });
  await db.payment_providers.destroy({
    truncate: true,
    cascade: true
  });
  await db.bank_accounts.destroy({
    truncate: true,
    cascade: true
  });
  await db.merchant_account_mappings.destroy({
    truncate: true,
    cascade: true
  });
  await db.payment_protocols.destroy({
    truncate: true,
    cascade: true
  });
  await db.role.update(
    { weight: 200 },
    {
      where: { role_name: "Owner" }
    }
  );
  await db.realm.update(
    { tariff: "7fbf6a37-dfa9-43c5-a6ad-2aff43528c57" },
    {
      where: { id: "046cce25-f407-45c7-8be9-3bf198093408" }
    }
  );

  await db.role.update(
    { weight: 100 },
    {
      where: { role_name: "Manager" }
    }
  );

  result.realm = await db.realm.findOne({
    where: { id: "046cce25-f407-45c7-8be9-3bf198093408" }
  });
  result.botRealm = await db.realm.findOne({
    where: { id: "2803d532-b4a2-4ced-91ae-677bcb8dae57" }
  });
  result.merchantRealm = await db.realm.findOne({
    where: { id: "988987e0-3f7a-4bdc-a26f-fbbcba5e8843" }
  });
  var date = new Date();
  date.setDate(date.getDate() + 1);
  result.paymentProtocolUPI = await db.payment_protocols.create({
    id: "44e1ec76-a196-4793-b2f4-a7e569cda66c",
    removed: 0,
    protocol_type: "UPI",
    identifier_label: "UTR",
    account_label: "VPA",
    settings: null,
    transaction_identifier_label: null,
    instruction: null
  });
  result.paymentProtocolIMPS = await db.payment_protocols.create({
    id: "eddb576b-0a8e-4c81-bb4e-228e662e85b2",
    removed: 0,
    protocol_type: "IMPS",
    identifier_label: "IFSC",
    account_label: "Account No",
    settings: null,
    transaction_identifier_label: null,
    instruction: null
  });
  result.bank1 = await db.banks.create({
    id: "44e1ec76-a196-4793-b2f4-a7e569cda66c",
    removed: 0,
    short_name: "TBIN",
    name: "TEST BANK OF INDIA",
    address: "Andheri",
    city: "Mumbai",
    country: "IN"
  });
  result.bank2 = await db.banks.create({
    id: "6dd0da45-abea-464c-a37a-f8e55f31db95",
    removed: 0,
    short_name: "BOIN",
    name: "BANK OF INDIA",
    address: "Hadapsar",
    city: "Pune",
    country: "IN"
  });
  result.paymentProvider1 = await db.payment_providers.create({
    id: "9b26660d-dd73-43e9-ad24-b76579732065",
    removed: 0,
    name: "MD",
    status: "ACTIVE"
  });
  result.bankAccount1 = await db.bank_accounts.create({
    id: "1e325ca7-5f70-407f-92d7-b1f63508e9e1",
    removed: 0,
    bank_id: result.bank1.id,
    account_name: "TEST ACC HOLDER",
    acc_no: "123456789",
    currency: "INR",
    payment_provider_id: result.paymentProvider1.id,
    payment_data: {
      _arr: [
        {
          identifier: "TBIN01234567",
          payment_protocol: result.paymentProtocolIMPS.id
        }
      ]
    }
  });
  result.acc1 = await db.account.create({
    id: "63af7aa8-23d9-4e5a-8963-eb7b9734644b",
    removed: 0,
    name: "Datta Merchant",
    address: "Merchant Niwas, Gawade Colony, Opp Tata Motors, Chinchwad",
    country: "IN",
    legal_entity: true,
    registration_no: "ADASD3123AHLK",
    region: "MAHARASHTRA",
    billing_currency: "INR",
    status: "ACTIVE",
    realm_id: result.realm.id
    // plan_id: "7fbf6a37-dfa9-43c5-a6ad-2aff43528c57"
  });
  result.acc2 = await db.account.create({
    id: "93332248-0144-42b8-96a8-422963bb65d2",
    removed: 0,
    name: "VikrantMerch",
    address: "Merchantwadi",
    country: "IN",
    legal_entity: false,
    registration_no: "",
    pid: "63af7aa8-23d9-4e5a-8963-eb7b9734644b",
    region: "MAHARASHTRA",
    billing_currency: "INR",
    status: "ACTIVE",
    realm_id: result.realm.id
  });

  result.user1 = await db.user.create({
    id: "3df4c34c-600a-11ed-9b6a-0242ac120002",
    removed: 0,
    first_name: "Datta",
    last_name: "Bhise",
    email: "db154+autotests@enovate-it.com",
    mobile: "+919860860466",
    username: "db154",
    password: crypto
      .createHash("sha256")
      .update("Passw0rd")
      .digest("hex"),
    mobile_verified: true,
    email_verified: true,
    status: "ACTIVE",
    user_type: "PORTAL"
  });
  result.user2 = await db.user.create({
    id: "3df4c34c-600a-11ed-9b6a-0242ac120003",
    removed: 0,
    first_name: "Aditi",
    last_name: "Phalke",
    email: "ap2691@enovate-it.com",
    mobile: "+919860860445",
    username: "ap2691",
    password: crypto
      .createHash("sha256")
      .update("Passw0rd")
      .digest("hex"),
    mobile_verified: false,
    email_verified: false,
    status: "ACTIVE",
    user_type: "PORTAL"
  });
  result.user3 = await db.user.create({
    id: "1e50984c-8cd1-11ed-a1eb-0242ac120002",
    removed: 0,
    first_name: "Prashin",
    last_name: "More",
    email: "pm261@enovate-it.com",
    mobile: "+919324115782",
    username: "pm261",
    password: crypto
      .createHash("sha256")
      .update("Passw0rd")
      .digest("hex"),
    mobile_verified: false,
    email_verified: false,
    status: "ACTIVE",
    user_type: "PORTAL"
  });
  result.merchant_account = await db.merchant_accounts.create({
    id: "1e50984c-8cd1-11ed-a1eb-0242ac121112",
    removed: 0,
    name: "Grocery",
    account_id: result.acc1.id,
    acc_no: "12332",
    currency: "INR",
    settlement_currency: "INR",
    status: "ACTIVE"
  });
  await db.user_accounts.create({
    id: "3df4c72a-600a-11ed-9b6a-0242ac120001",
    removed: 0,
    account_id: result.acc1.id,
    user_id: result.user1.id,
    main_role: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4",
    start_date: "2022-11-09T08:54:31.400Z",
    merchant_account_id: result.merchant_account.id
  });
  await db.user_accounts.create({
    id: "3df4c72a-600a-11ed-9b6a-0242ac120003",
    removed: 0,
    account_id: result.acc1.id,
    user_id: result.user2.id,
    main_role: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4",
    start_date: "2022-11-09T08:54:31.400Z",
    merchant_account_id: result.merchant_account.id
  });
  await db.user_accounts.create({
    id: "3df4c72a-600a-11ed-9b6a-0242ac120002",
    removed: 0,
    account_id: result.acc2.id,
    user_id: result.user1.id,
    main_role: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4",
    start_date: "2022-11-09T08:54:31.400Z",
    merchant_account_id: result.merchant_account.id
  });

  await db.user_account_invitations.create({
    id: "cdac17e0-8ccb-11ed-a1eb-0242ac120002",
    email: "pm261@enovate-it.com",
    account_id: result.acc2.id,
    token: "123456",
    is_registered_user: false,
    removed: 0,
    expiry: date,
    role: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4",
    merchant_account_id: result.merchant_account.id,
    user_id: "1e50984c-8cd1-11ed-a1eb-0242ac120002"
  });
  await db.user_account_invitations.create({
    id: "cdac17e0-8ccb-11ed-a1eb-0242ac120001",
    email: "as263@enovate-it.com",
    account_id: result.acc2.id,
    token: "1234561",
    is_registered_user: false,
    removed: 0,
    expiry: date,
    role: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4",
    merchant_account_id: result.merchant_account.id
  });
  await db.user_account_invitations.create({
    id: "cdac17e0-8ccb-11ed-a1eb-0242ac120003",
    email: "as263@enovate-it.com",
    account_id: result.acc2.id,
    token: "1234562",
    is_registered_user: false,
    removed: 0,
    expiry: date,
    role: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4",
    merchant_account_id: result.merchant_account.id
  });
  await db.merchant_account_mappings.create({
    id: "dcac17e0-8ccb-11ed-a1eb-0242ac120003",
    merchant_account_id: result.merchant_account.id,
    bank_account: "1e325ca7-5f70-407f-92d7-b1f63508e9e1",
    start_time: Date.now(),
    end_time: null
  });
  await db.wallet.update(
    { balance: 100 },
    {
      where: {
        bank_account_id: "1e325ca7-5f70-407f-92d7-b1f63508e9e1",
        type: db.wallet.TYPES.COLLECTION
      }
    }
  );

  const realmId = result.realm.id;
  global.realmId = realmId;
  global.merchantRealmId = result.merchantRealm.id;
  result.realmHost = "test.realm";
  const realmToken = result.realm.token;
  const realmHost = result.realmHost;
  global.realmToken = realmToken;
  global.realmHost = realmHost;

  await memstore.del("srv" + realmToken);

  return result;
}

async function after() {}

export default {
  before,
  after,
  wait
};
