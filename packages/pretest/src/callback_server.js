const express = require("express");
import config from "@lib/config";

const app = express();
const port = config.merchantCbPort;

app.get("/", (req, res) => {
  res.status(200).end();
});

let server = null;

export default {
  run() {
    server = app.listen(port, () => {
      // console.log(`Example app listening at http://localhost:${port}`);
    });
  },
  shutdown() {
    if (server) server.close();
  }
};
