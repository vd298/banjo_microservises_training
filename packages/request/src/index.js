import axios from "axios";
import logs from "@lib/logs";
import FormData from "form-data";

async function get(url, token, headers = {}, savelog = true) {
  let res;
  let config = { headers };

  if (token) {
    config.headers["Authorization"] = `Bearer ${token}`;
  }

  try {
    res = await axios.get(url, config);
  } catch (e) {
    //console.log("Request error:", url, config);
    res = {
      status: "Error",
      error: {},
      data: {}
    };
  }
  if (savelog)
    logs.saveCustom("cryptologs", {
      url,
      method: "get",
      response: res.data,
      status: res.status
    });
  return res;
}

async function post(url, data, token, headers = {}, savelog = true) {
  let res;
  let config = { headers };

  if (token) {
    config.headers["Authorization"] = `Bearer ${token}`;
  }
  try {
    res = await axios.post(url, data, config);
  } catch (e) {
    /*console.log(
      "Request error:",
      url,
      data,
      config,
      e.response ? JSON.stringify(e.response.data, null, 4) : ""
    );*/
    res = {
      status: "Error",
      error: e.response ? e.response.data : {},
      data: {}
    };
  }
  if (savelog)
    logs.saveCustom("cryptologs", {
      url,
      method: "post",
      request: data,
      response: res.data,
      error: res.error,
      status: res.status
    });
  return res;
}

async function postMultipart(url, data, token, headers) {
  const form = new FormData();

  if (!headers) headers = {};

  for (let k in data) {
    form.append(k, data[k]);
  }

  headers["Content-Type"] = `multipart/form-data; boundary=${form._boundary}`;

  if (token) {
    headers["Authorization"] = `Bearer ${token}`;
  }

  const res = await axios({
    method: "post",
    url: url,
    data: form,
    headers
  });
  return res;
}

export default {
  get,
  post,
  postMultipart
};
