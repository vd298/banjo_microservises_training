const authService = {
  inviteUser: {
    allowRoles: ["Owner", "Manager"],
    denyRoles: ["Supervisor"]
  }
};

module.exports = {
  authService
};
