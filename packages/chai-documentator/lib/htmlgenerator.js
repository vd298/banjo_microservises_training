process.chdir(`${__dirname}/../`);
const _PATH = `${__dirname}/../../../docs/index.pug`;
const _HTML_PATH = `${__dirname}/../../../docs/public/index.html`;
const _JSON_PATH = `${__dirname}/../../../docs/json`;
const _SERVICES_PATH = `${__dirname}/../../../src`;
const _SWAGGER_PATH = `${__dirname}/../../../docs/public/swagger.json`;

const pug = require("pug");
const yaml = require("js-yaml");
const fs = require("fs");
const slugify = require("slugify");

let SERVICES = {};

let RESULT = [];
let itIndex = 0;

function readServices() {
  const out = {};
  fs.readdirSync(_SERVICES_PATH).forEach((dir) => {
    try {
      let pkg = JSON.parse(
        fs.readFileSync(_SERVICES_PATH + "/" + dir + "/package.json").toString()
      );
      const cls = getService(_SERVICES_PATH + "/" + dir);
      if (cls) out[pkg.name] = cls;
    } catch (err) {
      console.log(err.message);
    }
  });
  return out;
}

function getService(path) {
  if (!fs.existsSync(`${path}/dist/Service.js`)) return null;
  const cls = require(`${path}/dist/Service.js`);
  const instance = new cls.default({});
  const methods = instance.publicMethods();
  return methods;
}

function addResultRecord(reqData) {
  RESULT.push(reqData);
}

function getSchema(service, method) {
  const conf = SERVICES[service][method];
  if (!conf || !conf.schema)
    return {
      type: "object",
      properties: {},
      required: []
    };
  return conf.schema;
}
function getResponseSchemaFromService(service, method) {
  const conf = SERVICES[service][method];
  if (!conf || !conf.responseSchema) return null;
  return conf.responseSchema;
}

SERVICES = readServices();

fs.readdirSync(_JSON_PATH).forEach((file) => {
  let content = JSON.parse(fs.readFileSync(_JSON_PATH + "/" + file).toString());
  content.data.forEach((test) => {
    addResultRecord(test);
  });
});

const html = pug.renderFile(_PATH, {
  pretty: true,
  services: RESULT
});

fs.writeFileSync(_HTML_PATH, html);
let swagger;
try {
  swagger = yaml.load(
    fs.readFileSync(`${__dirname}/../../../docs/swagger_tpl.yml`, "utf8")
  );
} catch (e) {
  swagger = require(`${__dirname}/../../../docs/swagger_tpl.json`);
}

swagger.paths = {};
const exampleOkResponseSchema = {
  type: "object",
  properties: {
    header: {
      type: "object",
      properties: {
        status: { type: "string", enum: ["OK", "ERROR"] }
      }
    },
    data: { type: "object" }
  }
};
const exampleErrorResponseSchema = {
  type: "object",
  properties: {
    header: {
      type: "object",
      properties: {
        id: { type: "string", maxLength: 36, description: "Request ID" },
        status: { type: "string", enum: ["OK", "ERROR"] }
      }
    },
    error: {
      type: "object",
      properties: {
        code: { type: "string" },
        message: { type: "string" }
      }
    }
  }
};
swagger.components.schemas = {
  exampleResponse: {
    type: "object",
    properties: {
      anyOf: [
        {
          header: {
            type: "object",
            properties: {
              id: { type: "string", maxLength: 36, description: "Request ID" },
              status: { type: "string", enum: ["OK"] }
            }
          },
          data: {
            anyOf: [{ type: "null" }, { type: "object" }]
          }
        },
        {
          header: {
            type: "object",
            properties: {
              status: { type: "string", enum: ["ERROR"] }
            }
          },
          error: {
            properties: {
              code: { type: "string" },
              message: { type: "string" }
            }
          }
        }
      ]
    }
  }
};
const getResponseSchema = (response) => {
  if (response && response.header && response.header.status == "OK") {
    const schema = JSON.parse(JSON.stringify(exampleOkResponseSchema));
    schema.properties.header.example = response.header;
    schema.properties.data = {
      example: response.data,
      type:
        response.data === null
          ? "null"
          : Array.isArray(response.data)
          ? "array"
          : "object"
    };
    return schema;
  } else if (response && response.header && response.header.status == "ERROR") {
    const schema = JSON.parse(JSON.stringify(exampleErrorResponseSchema));
    schema.properties.header.example = response.header;
    schema.properties.error = {
      ...schema.properties.error,
      example: response.error
    };
    return schema;
  }
};
RESULT.forEach((service) => {
  const path = `/rest/${service.request.body.header.service}/${slugify(
    service.request.body.header.method,
    {
      lower: true
    }
  )}/${slugify(service.meta.title, { lower: true })}`;
  let parameters = [
    {
      in: "header",
      name: "authorization",
      schema: {
        type: "string"
      },
      value: service.request["http-headers"]["authorization"],
      required: true
    }
  ];
  const schema = getSchema(
    service.request.body.header.service,
    service.request.body.header.method
  );
  const responseSchema = getResponseSchemaFromService(
    service.request.body.header.service,
    service.request.body.header.method
  );
  let example = {},
    security;
  let exampleResponse = {};
  const headerSchema = {
    type: "object",
    example: service.request.body.header,
    properties: {
      id: { type: "string", maxLength: 36, description: "Request ID" },
      nonce: {
        type: "string",
        maxLength: 36,
        description:
          "Generate and send unique nonce to avoid duplicate requests"
      },
      version: {
        type: "string",
        enum: [service.request.body.header.version],
        description: "Service Version"
      },
      service: {
        type: "string",
        enum: [service.request.body.header.service],
        description: "Service Name"
      },
      method: {
        type: "string",
        enum: [service.request.body.header.method],
        description: "Service Method Name"
      }
    },
    required: ["service", "method", "version"]
  };
  if (service.request.body.header.token) {
    headerSchema.properties["token"] = { type: "string" };
    headerSchema.required.push("token");
  }
  example = {
    summary: service.meta.title,
    schema: {
      properties: {
        header: headerSchema,
        data: {
          type: schema.type,
          example: service.request.body.data,
          properties: schema.properties,
          required: schema.required
        }
      }
    }
  };
  exampleResponse = {
    summary: service.meta.title,
    schema: responseSchema
      ? { ...responseSchema, example: service.response.body.data }
      : getResponseSchema(service.response.body)
  };
  swagger.paths[path] = {
    post: {
      tags: [service.meta.tags],
      summary: service.meta.title,
      description: service.meta.description,
      produces: [service.response["http-headers"]["content-type"]],
      parameters,
      security,
      requestBody: {
        content: {
          [service.response["http-headers"]["content-type"]]: example
        }
      },
      responses: {
        "200": {
          content: {
            [service.response["http-headers"]["content-type"]]: exampleResponse
          }
        }
      }
    }
  };
});

// Creating guides web portal navbar
const navbarPath = `${__dirname}/../../../docs/guide/_navbar.md`;
const stagingContent = `* Environment: staging
* [API](https://api-docs-staging.polygon.com)
* [Novatum website](https://staging.polygon.com)`;
const productionContent = `* Environment: production
* [API](https://api-docs.polygon.com)
* [Novatum website](https://account.polygon.com)`;

const content =
  process.env.NODE_ENV === "production" ? productionContent : stagingContent;

try {
  fs.writeFileSync(navbarPath, content);
} catch (err) {
  console.log("Failed to create the navbar for the API guides portal");
  console.log(err);
}

fs.writeFileSync(_SWAGGER_PATH, JSON.stringify(swagger));
console.log("fin");
process.exit(0);
