import db from "@lib/db";
import crypto from "crypto";
import Crypto from "./Crypto";

const Op = db.Sequelize.Op;

async function checkAccountByUser(acc_no, user) {
  const res = await db.account.findOne({
    where: { acc_no, owner: user },
    attributes: ["id"],
    raw: true
  });
  return res ? res.id : null;
}

async function getAccount(acc_no) {
  const res = await db.account.findOne({
    where: { acc_no },
    raw: true
  });
  return res;
}

function sign(str, masterKey) {
  return crypto
    .createHash("sha256")
    .update(str + masterKey)
    .digest("base64");
}

/**
 * @method getDateDifference
 * @author Aditya Tapaswi
 * @param {Date} d1 Contains date
 * @param {Date} d2 Contains date
 * @since 2 NOV 2022
 * @summary Returns difference in long int
 */
function getDateDifference(d1, d2) {
  return d2.getTime() - d1.getTime();
}

/**
 * @method generateHash
 * @author Aditya Tapaswi
 * @param {String} string Contains string to be hashed
 * @since 2 NOV 2022
 * @summary Returns hash of string
 */
function generateHash(string, algo = "sha256", digest = "hex") {
  const stringHashed = crypto
    .createHash(algo)
    .update(string)
    .digest(digest);
  return stringHashed;
}
/**
 * @method objectValue
 * @author Datta Bhise
 * @since 2 Feb 2023
 * @summary Return value from object for provided path
 */
const objectValue = (object, path) => {
  for (let p of path.split(".")) {
    object = object ? object[p] : undefined;
  }
  return object;
};
export default {
  generateHash,
  getDateDifference,
  sign,
  objectValue,
  Crypto
};
