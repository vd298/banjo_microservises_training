import crypto, { publicDecrypt } from "crypto";

function generateKeyPair(encoding = "base64", format = "der") {
  // Generating Key Pairs
  const { privateKey, publicKey } = crypto.generateKeyPairSync("rsa", {
    modulusLength: 2048,
    publicKeyEncoding: {
      type: "spki",
      format
    },
    privateKeyEncoding: {
      type: "pkcs8",
      format
    }
  });
  return {
    privateKey: privateKey.toString(encoding),
    publicKey: publicKey.toString(encoding)
  };
}

function calculateSignature(
  data,
  privateKey,
  keyEncoding = "base64",
  encoding = "base64",
  format = "der"
) {
  const key = crypto.createPrivateKey({
    key: Buffer.from(privateKey, keyEncoding),
    type: "pkcs8",
    format
  });
  // Sign the data and returned signature in buffer
  return crypto.sign("SHA256", Buffer.from(data), key).toString(encoding);
}

function validateSignature(data, sign, publicKey, encoding = "base64") {
  const key = crypto.createPublicKey({
    key: Buffer.from(publicKey, encoding),
    type: "spki",
    format: "der"
  });
  // --------------------------
  var verifier = crypto.createVerify("SHA256");
  verifier.update(Buffer.from(data));
  return verifier.verify(key, sign, encoding);
}
function createPrivateKey(privateKey) {
  return crypto.createPrivateKey({
    key: Buffer.from(privateKey, "base64"),
    type: "pkcs8",
    format: "der"
  });
}
function createPublicKey({ publicKey, format, encoding = "base64" }) {
  return crypto.createPublicKey({
    key: Buffer.from(publicKey, encoding),
    type: "spki",
    format
  });
}
function publicEncryptText(
  plainText,
  encoding = "base64",
  { publicKey, format = "der", encodingKey = "base64" }
) {
  const key = createPublicKey({ publicKey, format, encoding: encodingKey });
  return crypto.publicEncrypt(
    {
      key: key.export({ format: "pem", type: "spki" }),
      padding: crypto.constants.RSA_PKCS1_OAEP_PADDING,
      oaepHash: "sha256"
    },
    Buffer.from(plainText, encoding)
  );
}

function privateDecryptText(encryptedText, privateKey) {
  // const key = createPublicKey({ privateKey, format, encoding: encodingKey });
  return crypto
    .privateDecrypt(
      {
        key: privateKey,
        padding: crypto.constants.RSA_PKCS1_OAEP_PADDING,
        oaepHash: "sha256"
      },
      Buffer.from(encryptedText, "base64")
    )
    .toString("base64");
}
function createSignatureByPublicKey(dataStr, publicKey) {
  const hash = crypto
    .createHash("sha256")
    .update(dataStr)
    .digest("base64");
  return publicEncryptText(hash, "base64", { publicKey }).toString("base64");
}
function validateSignatureWithPrivateKey(
  signature,
  dataStr,
  privateKey,
  encoding = "base64"
) {
  const hash = crypto
    .createHash("sha256")
    .update(dataStr)
    .digest("base64");
  return hash === privateDecryptText(signature, privateKey, encoding);
}
export default {
  generateKeyPair,
  calculateSignature,
  validateSignature,
  createPrivateKey,
  createPublicKey,
  publicEncryptText,
  privateDecryptText,
  createSignatureByPublicKey,
  validateSignatureWithPrivateKey
};
