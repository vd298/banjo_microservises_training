import local from "./local";
import yandexDisk from "./yandexDisk";
import minio from "./min.io.js";

export default {
  local,
  yandexDisk,
  minio
};
