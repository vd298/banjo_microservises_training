const config = require("@lib/config");
const memstore = require("@lib/memstore").default;
const Connectors = require("./connectors");

async function user(request, response, user) {
  const Connector = Connectors[config.logs.user_connector || "local"];
  let loglevels = await getLogLevel();
  if (loglevels.indexOf("1") != -1) {
    let res = await Connector.save(request, response, user);
    return res;
  }
  return null;
}

async function warning(request, response, user) {
  const Connector = Connectors[config.logs.warning_connector || "local"];
  let loglevels = await getLogLevel();
  if (loglevels.indexOf("2") != -1) {
    let res = await Connector.save(request, response, user);
    return res;
  }
  return null;
}

async function debug(log_object) {
  const Connector = Connectors[config.logs.debug_connector || "local"];
  let loglevels = await getLogLevel();
  if (loglevels.indexOf("3") != -1) {
    let res = await Connector.save(
      log_object.request,
      log_object.response,
      log_object.user
    );
    return res;
  }
  return null;
}

async function error(request, response, user) {
  const Connector = Connectors[config.logs.error_connector || "local"];
  let loglevels = await getLogLevel();
  if (loglevels.indexOf("4") != -1) {
    let res = await Connector.save(request, response, user);
    return res;
  }
  return null;
}

async function saveCustom(collection, data) {
  const Connector = Connectors[config.logs.savecustom_connector || "local"];
  let res = await Connector.saveCustom(collection, data);
  return res;
}

async function getLogLevel() {
  let loglevel = await memstore.get("loglevel");
  if (!loglevel) loglevel = config.logs.level;
  let arr_loglevel = [];

  loglevel = loglevel.split(",");
  if (loglevel.length == 1) arr_loglevel.push(loglevel);
  else arr_loglevel = loglevel;
  return arr_loglevel;
}

exports.user = user;
exports.warning = warning;
exports.debug = debug;
exports.error = error;
exports.saveCustom = saveCustom;
