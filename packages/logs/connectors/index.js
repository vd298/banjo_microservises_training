const local = require("./local");
const mongo = require("./mongo");

exports.local = local;
exports.mongo = mongo;
