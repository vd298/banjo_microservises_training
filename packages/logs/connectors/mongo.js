const config = require("@lib/config");
const memstore = require("@lib/memstore").default;
const MongoClient = require("mongodb").MongoClient;

var db;

function connectMongo() {
  return new Promise(res => {
    MongoClient.connect(config.logs.connectionString, (err, client) => {
      if (!!client) db = client.db(config.logs.dbName);
      res(db);
    });
  });
}

connectMongo();

async function save(request, response, user, loglevel, log_object) {
  let log_status = await memstore.get("log_status");

  return new Promise(res => {
    if (!config.logs || !db || log_status != "on") return res(false);
    if (
      config.logs.ignore &&
      request.header &&
      config.logs.ignore.includes(
        `${request.header.service}:${request.header.method}`
      )
    )
      return res(false);

    db.collection("logs").insertOne(
      { request, response, ct: Date.now(), user },
      err => {
        res(!res);
      }
    );
  });
}

function saveCustom(collection, data) {
  return new Promise(res => {
    if (!config.logs || !config.logs.on || !db) return res(false);
    if (!data.ct) data.ct = Date.now();
    db.collection(collection).insertOne(data, err => {
      res(!err);
    });
  });
}

exports.save = save;
exports.saveCustom = saveCustom;
