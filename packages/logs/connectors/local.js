const memstore = require("@lib/memstore").default;
const config = require("@lib/config");

async function save(request, response, user, loglevel, log_object) {
  let log_status = await memstore.get("log_status");

  return new Promise(res => {
    if (!config.logs || log_status != "on") return res(false);
    if (
      config.logs.ignore &&
      request.header &&
      config.logs.ignore.includes(
        `${request.header.service}:${request.header.method}`
      )
    )
      return res(false);

    console.log({ request, response, ct: Date.now(), user });
  });
}

function saveCustom(collection, data) {
  return new Promise(res => {
    if (!config.logs || !config.logs.on || !db) return res(false);
    if (!data.ct) data.ct = Date.now();
    console.log(collection, data);
  });
}

exports.save = save;
exports.saveCustom = saveCustom;
