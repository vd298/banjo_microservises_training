/**
 * All DEVS Kindly follow following methodology to implement configurable schemes
 *
 * 1 - Identify schemes according to entities
 * 2 - Identify repetitive schema properties
 * 3 - Declare scheme request wise and use same property configurations if repetitive entity found
 * 4 - If unique schema please create a same as old method
 */

module.exports = {
  USERS_SCHEMA: require("./modules/user"),
  DEVICE_SCHEMA: require("./modules/device"),
  UNIT_SCHEMA: require("./modules/unit"),
  MIGRATION_SCHEMA: require("./modules/migration"),
  COMMON: require("./modules/common"),
  ACCOUNT_SCHEMA: require("./modules/account"),
  PUSH_SCHEMA: require("./modules/push"),
  MERCHANT_SCHEMA: require("./modules/merchant"),
  TRANSACTION_SCHEMA: require("./modules/transaction")
};
