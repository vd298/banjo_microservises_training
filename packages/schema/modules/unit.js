const { ID_PROPERTY } = require("./common");
const NAME_SCHEMA_PROPERTY = { type: "string", minLength: 3, maxLength: 50 };
const UNIT_STATUS_SCHEMA_PROPERTY = {
  type: "string",
  enum: ["ACTIVE", "INACTIVE", "BLOCKED"]
};
const UNITS = {
  device_list: {
    description: "unit Ids"
  },
  name: NAME_SCHEMA_PROPERTY,
  status: UNIT_STATUS_SCHEMA_PROPERTY,
  attached_unit_to_id: {
    ...ID_PROPERTY,
    description: "Unit attached to Id"
  },
  allocate_to_id: {
    ...ID_PROPERTY,
    description: "Unit attached to Id"
  },
  unit_driver_id: {
    ...ID_PROPERTY,
    description: "ID of unit driver"
  },
  unit_lat: {
    type: "decimal",
    description: "Unit Latitude"
  },
  unit_long: {
    type: "decimal",
    description: "Unit Longitude"
  },
  unit_icon: {
    type: "string",
    contentEncoding: "base64",
    contentMediaType: "image/png"
  }
};
const CREATE_UNIT = {
  type: "object",
  properties: UNITS,
  required: ["name"]
};

const DELETE_UNIT = {
  type: "object",
  properties: {
    id: ID_PROPERTY
  },
  required: ["id"]
};

const GET_ONE_UNIT = {
  type: "object",
  properties: {
    id: ID_PROPERTY
  },
  required: ["id"]
};
const UPDATE_UNIT = {
  type: "object",
  properties: {
    id: ID_PROPERTY,
    ...UNITS
  },
  required: ["id"]
};

module.exports = {
  CREATE_UNIT,
  DELETE_UNIT,
  GET_ONE_UNIT,
  UPDATE_UNIT
};
