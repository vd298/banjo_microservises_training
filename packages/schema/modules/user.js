const PASSWORD_SCHEMA_PROPERTY = {
  type: "string",
  pattern: /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)[A-Za-z\d@$!%*#?&_]{8,15}$/.source,
  sensitive: true,
  secure: true
};
const EMAIL_SCHEMA_PROPERTY = { type: "string" };
const NAME_SCHEMA_PROPERTY = { type: "string", minLength: 3, maxLength: 50 };
const MOBILE_SCHEMA_PROPERTY = {
  type: "string",
  maxLength: 15,
  format: "phone"
};
const USERNAME_SCHEMA_PROPERTY = { type: "string", maxLength: 50 };
const USER_STATUS_SCHEMA_PROPERTY = {
  type: "string",
  enum: ["ACTIVE", "INACTIVE", "BLOCKED"]
};
const TOKEN_SCHEMA_PROPERTY = { type: "string" };
const CODE_SCHEMA_PROPERTY = { type: "string" };
const LOGIN_SCHEMA = {
  type: "object",
  properties: {
    username: USERNAME_SCHEMA_PROPERTY,
    password: PASSWORD_SCHEMA_PROPERTY
  },
  required: ["username", "password"]
};
const CHANGE_PASSWORD_SCHEMA = {
  type: "object",
  properties: {
    old_password: PASSWORD_SCHEMA_PROPERTY,
    new_password: PASSWORD_SCHEMA_PROPERTY
  },
  required: ["old_password", "new_password"]
};
const RESET_PASSWORD_REQUEST_SCHEMA = {
  type: "object",
  properties: {
    email: EMAIL_SCHEMA_PROPERTY
  },
  required: ["email"]
};
const RESET_PASSWORD_SCHEMA = {
  type: "object",
  properties: {
    code: { type: "string" },
    step: { type: "integer" },
    new_password: PASSWORD_SCHEMA_PROPERTY
  },
  required: ["code", "step"]
};
const UPDATE_PROFILE_SCHEMA = {
  type: "object",
  properties: {
    first_name: NAME_SCHEMA_PROPERTY,
    last_name: NAME_SCHEMA_PROPERTY,
    email: EMAIL_SCHEMA_PROPERTY,
    username: USERNAME_SCHEMA_PROPERTY,
    mobile: MOBILE_SCHEMA_PROPERTY,
    profile_pic: { type: ["string", "null"] },
    timezone: { type: "string", maxLength: 50 },
    status: USER_STATUS_SCHEMA_PROPERTY
  }
};
const VERIFY_EMAIL_REQUEST_SCHEMA = {
  type: "object",
  properties: {
    email: EMAIL_SCHEMA_PROPERTY
  },
  required: ["email"]
};
const VERIFY_EMAIL_SCHEMA = {
  type: "object",
  properties: {
    token: TOKEN_SCHEMA_PROPERTY,
    code: CODE_SCHEMA_PROPERTY
  },
  required: ["token", "code"]
};
const VERIFY_MOBILE_REQUEST_SCHEMA = {
  type: "object",
  properties: {}
};
const VERIFY_MOBILE_SCHEMA = {
  type: "object",
  properties: {
    code: CODE_SCHEMA_PROPERTY
  },
  required: ["code"]
};

module.exports = {
  LOGIN_SCHEMA,
  CHANGE_PASSWORD_SCHEMA,
  RESET_PASSWORD_REQUEST_SCHEMA,
  RESET_PASSWORD_SCHEMA,
  UPDATE_PROFILE_SCHEMA,
  VERIFY_EMAIL_REQUEST_SCHEMA,
  VERIFY_EMAIL_SCHEMA,
  VERIFY_MOBILE_REQUEST_SCHEMA,
  VERIFY_MOBILE_SCHEMA,
  USER_STATUS_SCHEMA_PROPERTY
};
