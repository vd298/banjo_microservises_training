const PAGINATION_PROPERTIES = {
  page: {
    type: "number",
    format: "integer",
    description: "Page number starts from 1, Default 1"
  },
  page_size: {
    type: "number",
    format: "integer",
    description: "Default 100, Max 200 records per API call"
  }
};
const PAGINATION_SCHEMA = {
  type: "object",
  properties: PAGINATION_PROPERTIES
};
const BOOLEAN_PROPERTY = { type: "boolean" };
const STRING_PROPERTY = { type: "string" };
const ID_PROPERTY = {
  type: "string",
  format: "uuid"
};
const NAME_SCHEMA_PROPERTY = { type: "string", minLength: 3, maxLength: 50 };
const ACCOUNT_TYPE_PROPERTY = {
  type: "string",
  enum: ["DEALER", "CONSUMER"],
  format: "string"
};
const CURRENCY_PROPERTY = { type: "string", minLength: 3, maxLength: 4 };
const DATE_PROPERTY = { type: "date" };
const AMOUNT_PROPERTY = { type: "number", minimum: 0.01 };
const SETTLEMENT_AMOUNT_PROPERTY = { type: "number", minimum: 0.01 };
module.exports = {
  PAGINATION_SCHEMA,
  BOOLEAN_PROPERTY,
  PAGINATION_PROPERTIES,
  ID_PROPERTY,
  NAME_SCHEMA_PROPERTY,
  ACCOUNT_TYPE_PROPERTY,
  STRING_PROPERTY,
  AMOUNT_PROPERTY,
  CURRENCY_PROPERTY,
  DATE_PROPERTY,
  SETTLEMENT_AMOUNT_PROPERTY
};
