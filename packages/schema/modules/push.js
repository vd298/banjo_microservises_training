const COMMON = require("./common");

const SEND_TO_ALL_USER_DEVICES = {
  type: "object",
  properties: {
    title: COMMON.STRING_PROPERTY,
    body: COMMON.STRING_PROPERTY
  },
  required: ["title", "body"]
};

const PUBLIC_METHOD_SCHEMA = {
  type: "object",
  properties: {
    to: COMMON.STRING_PROPERTY,
    title: COMMON.STRING_PROPERTY,
    body: COMMON.STRING_PROPERTY,
    icon: COMMON.STRING_PROPERTY,
    click_action: COMMON.STRING_PROPERTY
  },
  required: ["title", "body"]
};

const SEND_V2 = {
  type: "object",
  properties: {
    push_action: COMMON.STRING_PROPERTY,
    push_title: COMMON.STRING_PROPERTY,
    push_message: COMMON.STRING_PROPERTY
  },
  required: ["push_title", "push_message"]
};

const SAVE_TOKEN = {
  type: "object",
  properties: {
    token: COMMON.STRING_PROPERTY,
    notification_id: COMMON.STRING_PROPERTY
  },
  required: ["token", "notification_id"]
};

const REMOVE_TOKEN = {
  type: "object",
  properties: {
    notification_id: COMMON.STRING_PROPERTY
  },
  required: ["notification_id"]
};

module.exports = {
  SEND_TO_ALL_USER_DEVICES,
  SEND_V2,
  SAVE_TOKEN,
  REMOVE_TOKEN,
  PUBLIC_METHOD_SCHEMA
};
