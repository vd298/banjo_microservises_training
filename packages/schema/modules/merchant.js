const COMMON = require("./common");
const ORDER_ID = {
  type: "string",
  minLength: 1,
  maxLength: 40,
  description: "Merchant's unique order id"
};
const TX_ID = {
  type: "string",
  format: "uuid",
  description: "Merchant's unique order id"
};
const COLLECTION_REQUEST = {
  type: "object",
  properties: {
    order: {
      type: "object",
      properties: {
        order_id: ORDER_ID,
        description: {
          type: "string",
          minLength: 4,
          maxLength: 60,
          description: "Merchant's unique order id"
        }
      },
      required: ["order_id", "description"]
    },
    currency: COMMON.CURRENCY_PROPERTY,
    amount: { type: "stringnumber", minimum: 0.01 },
    protocols: {
      type: "array",
      items: {
        type: "string"
      }
    },
    user_ip: {
      type: "string",
      oneOf: [{ format: "ipv4" }, { format: "ipv6" }]
      // format: ["ipv4", "ipv6"]
    },
    user_id: {
      type: "string",
      minLength: 1,
      maxLength: 100
    },
    return_url: {
      type: "string",
      format: "uri",
      maxLength: 255,
      description:
        "Return URL, `status`, `tx_id` & `order_id` query string parameters will added to this URL, after payment is redirected"
    },
    payer_email: {
      type: "string"
    },
    payer_phone: {
      type: "string"
    },
    payer_name: {
      type: "string"
    },
    payer_vpa: {
      type: "string"
    }
  },
  additionalProperties: false,
  required: ["amount", "currency", "order", "return_url", "user_id"]
};

const COLLECTION_REQUEST_H2H = {
  type: "object",
  properties: {
    order: {
      type: "object",
      properties: {
        order_id: ORDER_ID,
        description: {
          type: "string",
          minLength: 4,
          maxLength: 60,
          description: "Merchant's unique order id"
        }
      },
      required: ["order_id", "description"]
    },
    currency: COMMON.CURRENCY_PROPERTY,
    amount: { type: "stringnumber", minimum: 0.01 },
    protocols: {
      type: "array",
      items: {
        type: "string"
      }
    },
    user_ip: {
      type: "string",
      oneOf: [{ format: "ipv4" }, { format: "ipv6" }]
    },
    user_id: {
      type: "string",
      minLength: 1,
      maxLength: 100
    },
    email: {
      type: "string"
    },
    phone: {
      type: "string"
    }
  },
  additionalProperties: false,
  required: ["amount", "currency", "order", "user_id"]
};

const PAYOUT_REQUEST = {
  type: "object",
  properties: {
    order: {
      type: "object",
      properties: {
        order_id: ORDER_ID,
        description: {
          type: "string",
          minLength: 4,
          maxLength: 60,
          description: "Merchant's unique order id"
        },
        protocol: {
          type: "string",
          minLength: 2,
          maxLength: 30,
          description: "Payout Protocol Type"
        },
        destination: {
          type: "string",
          maxLength: 60,
          description: "Payout Destination Bank Account number/Address"
        }
      },
      required: ["order_id", "description"]
    },
    currency: COMMON.CURRENCY_PROPERTY,
    amount: { type: "stringnumber", minimum: 0.01 },
    protocols: {
      type: "array",
      items: {
        type: "string"
      }
    },
    user_ip: {
      type: "string",
      oneOf: [{ format: "ipv4" }, { format: "ipv6" }]
      // format: ["ipv4", "ipv6"]
    },
    user_id: {
      type: "string"
    },
    user_agent: { type: "string" },
    beneficiary_id: {
      type: "string",
      format: "uuid"
    },
    email: {
      type: "string"
    },
    phone: {
      type: "string"
    }
  },
  additionalProperties: false,
  required: [
    "amount",
    "currency",
    "order",
    "user_ip",
    "user_agent",
    "beneficiary_id"
  ]
};

const PAYOUT_BANK_ACC_CHECK = {
  type: "object",
  properties: {
    currency: COMMON.CURRENCY_PROPERTY,
    amount: { type: "stringnumber", minimum: 0.01 },
    protocol: {
      type: "string"
    },
    merchant_account_id: {
      type: "string",
      format: "uuid"
    }
  },
  required: ["amount", "currency", "merchant_account_id"]
};
const TX_STATUS_REQUEST = {
  type: "object",
  description:
    "Get transaction status by either order id or transaction id, if both passed then order id will be used to find transaction",
  properties: {
    transaction_id: TX_ID,
    order_id: ORDER_ID
  },
  required: []
};
const TX_LIST_REQUEST = {
  type: "object",
  description: "Get transaction list",
  properties: {
    transaction_id: TX_ID,
    order_id: ORDER_ID,
    status: ORDER_ID,
    date_from: COMMON.DATE_PROPERTY,
    date_to: COMMON.DATE_PROPERTY,
    pagination: COMMON.PAGINATION_SCHEMA
  },
  required: []
};

module.exports = {
  COLLECTION_REQUEST,
  PAYOUT_REQUEST,
  TX_STATUS_REQUEST,
  TX_LIST_REQUEST,
  PAYOUT_BANK_ACC_CHECK,
  COLLECTION_REQUEST_H2H
};
