const { ID_PROPERTY } = require("./common");
const USERS_SCHEMA = require("./user");

const NAME_SCHEMA_PROPERTY = { type: "string", minLength: 3, maxLength: 50 };

const DEVICES = {
  device_name: NAME_SCHEMA_PROPERTY,
  version: { type: "string", minLength: 1, maxLength: 50 },
  identifier: { type: "string", minLength: 1, maxLength: 40 },
  status: USERS_SCHEMA.USER_STATUS_SCHEMA_PROPERTY,
  model_id: { type: "string" },
  sim_card_number: { type: "string", minLength: 1, maxLength: 15 },
  sim_operator: { type: "string", minLength: 1, maxLength: 20 }
};
const CREATE_DEVICES = {
  type: "object",
  properties: { ...DEVICES, owner_account_id: { type: "string" } },
  required: [
    "device_name",
    "version",
    "identifier",
    "status",
    "model_id",
    "sim_card_number",
    "sim_operator"
  ]
};

const UPDATE_DEVICES = {
  type: "object",
  properties: {
    id: ID_PROPERTY,
    ...DEVICES
  },
  required: ["id"]
};
const REALLOCATE_DEVICE = {
  type: "object",
  properties: {
    device_id: {
      type: "string",
      format: "uuid",
      description: "Device Id to be reallocated"
    },
    account_id: {
      type: "string",
      format: "uuid",
      description: "Id of user to whom device is to be allocated"
    }
  }
};
module.exports = {
  CREATE_DEVICES,
  UPDATE_DEVICES,
  REALLOCATE_DEVICE
};
