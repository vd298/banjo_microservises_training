const COMMON = require("./common");
const SOURCE_PROPERTY = {
  type: "string",
  minLength: 1,
  maxLength: 20,
  description: "Migration account name,e.g wialon"
};
const TOKEN_PROPERTY = {
  type: "string",
  description: "Once user logged in send auth-token"
};
const DISCONNECT_MIGRATION = {
  type: "object",
  properties: {
    source: SOURCE_PROPERTY
  },
  required: ["source"]
};
const SAVE_TOKEN = {
  type: "object",
  properties: {
    token: TOKEN_PROPERTY,
    source: SOURCE_PROPERTY
  },
  required: ["token", "source"]
};
const GET_UNIT_DEVICES = {
  type: "object",
  properties: {
    source: SOURCE_PROPERTY,
    unit: COMMON.PAGINATION_SCHEMA,
    device: COMMON.PAGINATION_SCHEMA
  },
  required: ["source", "unit", "device"]
};
const TOGGLE_MIGRATION = {
  type: "object",
  properties: {
    id: COMMON.ID_PROPERTY,
    source: SOURCE_PROPERTY,
    enable: COMMON.BOOLEAN_PROPERTY
  },
  required: ["id", "source", "enable"]
};
const IMPORT_UNITS_DEVICES = {
  type: "object",
  properties: {
    source: SOURCE_PROPERTY
  },
  required: ["source"]
};
module.exports = {
  DISCONNECT_MIGRATION,
  SAVE_TOKEN,
  GET_UNIT_DEVICES,
  TOGGLE_MIGRATION,
  IMPORT_UNITS_DEVICES
};
