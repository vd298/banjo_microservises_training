const COMMON = require("./common");
const USERS_SCHEMA = require("./user");

const {
  PAGINATION_PROPERTIES,
  ID_PROPERTY,
  BOOLEAN_PROPERTY
} = require("./common");

const REMOVE_USER_ACCOUNT = {
  type: "object",
  properties: {
    user_id: {
      type: "string",
      description: "user id",
      format: "uuid"
    }
  },
  required: ["user_id"]
};
const GET_DEALER_LIST = {
  ...COMMON.PAGINATION_SCHEMA,
  type: "object",
  properties: {
    type: {
      type: "string",
      enum: ["DEALER", "CONSUMER"],
      format: "string"
    }
  },
  required: ["type"]
};
const UPDATE_USER_ROLE_ACCOUNT = {
  type: "object",
  properties: {
    role: {
      type: "string",
      description: "role",
      format: "uuid"
    }
  },
  required: ["role"]
};
const UPDATE_DEALER_ACCOUNT_STATUS = {
  type: "object",
  properties: {
    type: {
      type: "string",
      enum: ["DEALER", "CONSUMER"],
      format: "string"
    },
    status: USERS_SCHEMA.USER_STATUS_SCHEMA_PROPERTY,
    account_id: ID_PROPERTY
  },
  required: ["type", "status", "account_id"]
};
const CREATE_DEALER_ACCOUNT = {
  type: "object",
  properties: {
    name: COMMON.NAME_SCHEMA_PROPERTY,
    address: {
      type: "string"
    },
    country: {
      type: "string"
    },
    legal_entity: {
      type: "boolean"
    },
    registration_no: {
      type: "string"
    },
    email: {
      type: "string"
    },
    type: {
      type: "string",
      enum: ["DEALER", "CONSUMER"]
    },
    region: {
      type: "string"
    },
    billing_currency: {
      type: "string"
    }
  },
  required: ["type", "address", "name", "country", "currency"]
};
const ADD_ROLE_SCHEMA = {
  type: "object",
  properties: {
    role_name: {
      type: "string",
      description: "Name of role",
      maxLength: 255
    },
    permissions: {
      type: "object",
      description: "Permissions of role"
    },
    users: {
      type: "array",
      description: "User ids of role"
    }
  },
  required: ["role_name", "permissions"]
};
const DELETE_ROLE_SCHEMA = {
  type: "object",
  properties: {
    role_id: ID_PROPERTY
  },
  required: ["role_id"]
};
const GET_ALL_PERMISSIONS_ADMIN_SCHEMA = {
  type: "object",
  properties: {
    type: {
      type: "string",
      description: "Type of account",
      enum: ["DEALER", "CONSUMER"]
    }
  },
  required: ["type"]
};
const SELECT_ACCOUNT_SCHEMA = {
  type: "object",
  properties: {
    account_id: ID_PROPERTY
  },
  required: ["account_id"]
};
const UPDATE_ROLE_SCHEMA = {
  type: "object",
  properties: {
    role_id: ID_PROPERTY,
    role_name: {
      type: "string",
      description: "Name of role",
      maxLength: 255
    },
    permissions: {
      type: "object",
      description: "Permissions of role"
    },
    users: {
      type: "array",
      description: "User ids of role"
    }
  },
  required: ["role_id"]
};
const GET_ONE_ROLE = {
  type: "object",
  properties: {
    ...PAGINATION_PROPERTIES,
    role_id: ID_PROPERTY,
    only_users: BOOLEAN_PROPERTY
  },
  required: ["role_id"]
};
const GET_ROLE_LIST = {
  type: "object",
  properties: {
    type: COMMON.ACCOUNT_TYPE_PROPERTY,
    account_id: ID_PROPERTY
  }
};
const SELECT_ACCOUNT_RESPONSE = {
  type: "object",
  properties: {
    account: {
      type: "array",
      items: [
        {
          type: "string"
        }
      ]
    },
    devices: {
      type: "array",
      items: [
        {
          type: "string"
        }
      ]
    },
    role: {
      type: "array",
      items: [
        {
          type: "string"
        }
      ]
    },
    units: {
      type: "array",
      items: [
        {
          type: "string"
        }
      ]
    },
    user: {
      type: "array",
      items: [
        {
          type: "string"
        }
      ]
    },
    user_accounts_invitations: {
      type: "array",
      items: [
        {
          type: "string"
        }
      ]
    }
  }
};

const GET_ALL_PERMISSION_RESPONSE = {
  type: "object",
  properties: {
    account: {
      type: "object",
      properties: {
        label: {
          type: "string"
        },
        op: {
          type: "object",
          properties: {
            r: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: {}
                }
              },
              required: ["per", "parent"]
            },
            a: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            },
            u: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            },
            d: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            }
          },
          required: ["r", "a", "u", "d"]
        }
      },
      required: ["label", "op"]
    },
    devices: {
      type: "object",
      properties: {
        label: {
          type: "string"
        },
        op: {
          type: "object",
          properties: {
            r: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: {}
                }
              },
              required: ["per", "parent"]
            },
            a: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            },
            u: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            },
            d: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            }
          },
          required: ["r", "a", "u", "d"]
        }
      },
      required: ["label", "op"]
    },
    role: {
      type: "object",
      properties: {
        label: {
          type: "string"
        },
        op: {
          type: "object",
          properties: {
            r: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: {}
                }
              },
              required: ["per", "parent"]
            },
            a: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            },
            u: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            },
            d: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            }
          },
          required: ["r", "a", "u", "d"]
        }
      },
      required: ["label", "op"]
    },
    units: {
      type: "object",
      properties: {
        label: {
          type: "string"
        },
        op: {
          type: "object",
          properties: {
            r: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: {}
                }
              },
              required: ["per", "parent"]
            },
            a: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            },
            u: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            },
            d: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            }
          },
          required: ["r", "a", "u", "d"]
        }
      },
      required: ["label", "op"]
    },
    user: {
      type: "object",
      properties: {
        label: {
          type: "string"
        },
        op: {
          type: "object",
          properties: {
            r: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: {}
                }
              },
              required: ["per", "parent"]
            },
            a: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            },
            u: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            },
            d: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            }
          },
          required: ["r", "a", "u", "d"]
        }
      },
      required: ["label", "op"]
    },
    user_accounts_invitations: {
      type: "object",
      properties: {
        label: {
          type: "string"
        },
        op: {
          type: "object",
          properties: {
            r: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: {}
                }
              },
              required: ["per", "parent"]
            },
            a: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            },
            u: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            },
            d: {
              type: "object",
              properties: {
                per: {
                  type: "boolean"
                },
                parent: {
                  type: "array",
                  items: [
                    {
                      type: "string"
                    }
                  ]
                }
              },
              required: ["per", "parent"]
            }
          },
          required: ["r", "a", "u", "d"]
        }
      },
      required: ["label", "op"]
    }
  },
  required: [
    "account",
    "devices",
    "role",
    "units",
    "user",
    "user_accounts_invitations"
  ]
};
const GET_ONE_ROLE_RESPONSE = {
  type: "object",
  properties: {
    role: {
      type: "object",
      properties: {
        role_name: {
          type: "string"
        },
        id: {
          type: "string"
        },
        permissions: SELECT_ACCOUNT_RESPONSE,
        is_default: {
          type: "boolean"
        }
      },
      required: ["role_name", "id", "permissions", "is_default"]
    },
    count: {
      type: "integer"
    },
    users: {
      type: "array",
      items: {
        type: "string",
        format: "uuid"
      }
    },
    permissions: GET_ALL_PERMISSION_RESPONSE,
    labels: {
      type: "object",
      properties: {
        r: {
          type: "string"
        },
        a: {
          type: "string"
        },
        u: {
          type: "string"
        },
        d: {
          type: "string"
        }
      },
      required: ["r", "a", "u", "d"]
    },
    has_next: {
      type: "boolean"
    }
  },
  required: ["role", "count", "users", "permissions", "labels", "has_next"]
};
const ADD_UPDATE_ROLE_RESPONSE = {
  type: "object",
  properties: {
    id: {
      type: "string"
    },
    role_name: {
      type: "string"
    },
    permissions: {
      type: "object",
      properties: {
        devices: {
          type: "array",
          items: [
            {
              type: "string"
            },
            {
              type: "string"
            },
            {
              type: "string"
            }
          ]
        },
        role: {
          type: "array",
          items: [
            {
              type: "string"
            },
            {
              type: "string"
            },
            {
              type: "string"
            },
            {
              type: "string"
            }
          ]
        },
        units: {
          type: "array",
          items: [
            {
              type: "string"
            },
            {
              type: "string"
            },
            {
              type: "string"
            },
            {
              type: "string"
            }
          ]
        },
        user: {
          type: "array",
          items: [
            {
              type: "string"
            },
            {
              type: "string"
            },
            {
              type: "string"
            },
            {
              type: "string"
            }
          ]
        },
        user_accounts_invitations: {
          type: "array",
          items: [
            {
              type: "string"
            },
            {
              type: "string"
            },
            {
              type: "string"
            },
            {
              type: "string"
            }
          ]
        }
      },
      required: [
        "devices",
        "role",
        "units",
        "user",
        "user_accounts_invitations"
      ]
    },
    account_id: {
      type: "string"
    },
    is_default: {
      type: "boolean"
    }
  },
  required: ["id", "role_name", "permissions", "account_id", "is_default"]
};
module.exports = {
  REMOVE_USER_ACCOUNT,
  GET_ONE_ROLE,
  ADD_ROLE_SCHEMA,
  UPDATE_ROLE_SCHEMA,
  DELETE_ROLE_SCHEMA,
  SELECT_ACCOUNT_SCHEMA,
  GET_ONE_ROLE_RESPONSE,
  ADD_UPDATE_ROLE_RESPONSE,
  SELECT_ACCOUNT_RESPONSE,
  GET_ALL_PERMISSION_RESPONSE,
  UPDATE_USER_ROLE_ACCOUNT,
  GET_DEALER_LIST,
  UPDATE_DEALER_ACCOUNT_STATUS,
  CREATE_DEALER_ACCOUNT,
  GET_ALL_PERMISSIONS_ADMIN_SCHEMA,
  GET_ROLE_LIST
};
