const COMMON = require("./common");
const ORDER_ID = {
  type: ["string", "null"],
  minLength: 1,
  maxLength: 40,
  description: "Merchant's unique order id"
};
const TX_ID = {
  type: "string",
  format: "uuid",
  description: "Transfer ID"
};
const COLLECTION_TRIGGER = {
  type: "object",
  properties: {
    transfer_id: TX_ID,
    order_id: ORDER_ID,
    currency: COMMON.CURRENCY_PROPERTY,
    amount: COMMON.AMOUNT_PROPERTY,
    data: {
      type: "object"
    }
  },
  required: []
};
const DIRECT_DEPOSIT_TRIGGER = {
  type: "object",
  properties: {
    merchant_account_id: TX_ID,
    bank_account_id: TX_ID,
    src_currency: COMMON.CURRENCY_PROPERTY,
    dst_currency: COMMON.CURRENCY_PROPERTY,
    src_amount: COMMON.AMOUNT_PROPERTY,
    dst_amount: COMMON.AMOUNT_PROPERTY
  },
  required: [
    "merchant_account_id",
    "src_currency",
    "dst_currency",
    "src_amount",
    "dst_amount",
    "bank_account_id"
  ]
};

const PAYOUT_TRIGGER = {
  type: "object",
  properties: {
    transfer_id: TX_ID,
    order_id: ORDER_ID,
    currency: COMMON.CURRENCY_PROPERTY,
    amount: COMMON.AMOUNT_PROPERTY,
    data: {
      type: "object"
    }
  },
  required: []
};
const SETTLEMENT_PAYOUT_TRIGGER = {
  type: "object",
  properties: {
    transfer_id: TX_ID,
    order_id: ORDER_ID,
    currency: COMMON.CURRENCY_PROPERTY,
    amount: COMMON.SETTLEMENT_AMOUNT_PROPERTY,
    data: {
      type: "object"
    }
  },
  required: []
};
const TX_ACTIVITY_PAGE_OPENED = {
  type: "object",
  properties: {
    transfer_id: TX_ID,
    note: {
      type: "string",
      maxLength: "150",
      description: "Note for activity"
    }
  },
  required: ["transfer_id"]
};
//Vaibhav Vaidya, 4th April 2023, Allow higher BRN limit for Staging for testing
const isDevEnv = ["development", "staging", "dev"].includes(
  process.env.NODE_ENV
);
let defaultMaxBRNLength = "200";
if (isDevEnv) {
  defaultMaxBRNLength = "200";
}
const TX_ACTIVITY_SUBMIT_BRN = {
  type: "object",
  properties: {
    transfer_id: TX_ID,
    brn: {
      type: "string",
      minLength: "10",
      maxLength: defaultMaxBRNLength,
      description: "Bank Reference Number"
    },
    note: {
      type: "string",
      maxLength: "150",
      description: "Note for activity"
    }
  },
  required: ["transfer_id", "brn", "note"]
};
const TX_ACTIVITY_SUBMIT_VPA = {
  type: "object",
  properties: {
    transfer_id: TX_ID,
    vpa: {
      type: "string",
      minLength: "10",
      maxLength: "50",
      description: "UPI Address"
    }
  },
  required: ["transfer_id", "vpa"]
};
const TX_ACTIVITY_APPROVE = {
  type: "object",
  properties: {
    transfer_id: TX_ID,
    brn: {
      type: "string",
      maxLength: "200",
      description: "BRN"
    },
    note: {
      type: "string",
      maxLength: "150",
      description: "Note for activity"
    }
  },
  required: ["transfer_id"]
};
const TX_ACTIVITY_REJECT = {
  type: "object",
  properties: {
    transfer_id: TX_ID,
    note: {
      type: "string",
      maxLength: "150",
      description: "Note for activity"
    }
  },
  required: ["transfer_id"]
};
const GET_TX_DETAILS = {
  type: "object",
  properties: {
    transfer_id: TX_ID
  },
  required: ["transfer_id"]
};
module.exports = {
  COLLECTION_TRIGGER,
  PAYOUT_TRIGGER,
  TX_ACTIVITY_PAGE_OPENED,
  TX_ACTIVITY_SUBMIT_BRN,
  TX_ACTIVITY_APPROVE,
  TX_ACTIVITY_REJECT,
  GET_TX_DETAILS,
  SETTLEMENT_PAYOUT_TRIGGER,
  DIRECT_DEPOSIT_TRIGGER
};
