const { I18n } = require("i18n");
const path = require("path");
const config = require("@lib/config");
const i18n = new I18n({
  locales: config.locales,
  defaultLocale: config.defaultLocale,
  directory: path.join(__dirname, "../", "locales"),
  retryInDefaultLocale: true,
  updateFiles: false,
  autoReload: true
});
const DEFAULT_OPTIONS = {
  lang: config.defaultLocale,
  data: {}
};

class BaseError extends Error {
  constructor(code, options) {
    options = { ...DEFAULT_OPTIONS, ...options };
    options.lang = options.lang || DEFAULT_OPTIONS.lang;
    const title = i18n.__mf(
      {
        phrase: `${options.title || `${code}_TITLE`}`,
        locale: options.lang
      },
      options.data
    );
    const message = i18n.__mf(
      {
        phrase: `${options.message || `${code}_MESSAGE`}`,
        locale: options.lang
      },
      options.data
    );
    let techMessage;
    if (options.dev_message) {
      techMessage = i18n.__mf(
        { phrase: `${options.dev_message}`, locale: options.lang },
        options.data
      );
    }
    super(message);
    this.dev_message = techMessage;
    this.message = message;
    this.title = title;
    this.code = code;
    this.name = "BANJO_ERROR";
  }
  toJSON() {
    return {
      code: this.code,
      message: this.message,
      title: this.title,
      category: this.name,
      dev_message: this.dev_message
    };
  }
  toString() {
    return `${this.name}/${this.code} ${this.message}`;
  }
}

module.exports = BaseError;
