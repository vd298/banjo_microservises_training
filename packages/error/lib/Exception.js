const BaseError = require("./BaseError");

class Exception extends BaseError {
  constructor(message, options = {}) {
    super("EXCEPTION", { ...options, message, title: "Something Went Wrong" });
    this.name = "BANJO_EXCEPTION";
  }
}

module.exports = Exception;
