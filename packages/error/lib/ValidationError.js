const BaseError = require("./BaseError");

class ValidationError extends BaseError {
  constructor(code, options) {
    super(code, options);
    this.name = "BANJO_VALIDATION_ERROR";
  }
}

module.exports = ValidationError;
