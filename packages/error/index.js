/**
 * All DEVS Kindly follow following methodology to give proper errors to users
 *
 * [1 - Unable to connect to your account]
 * [2 - Your changes were saved], [3 - but we could not connect to your account due to technical error at end.] [4 - Please try connecting again.]
 * [5 - If this issue keeps happening contact our support]
 * 1 - Reason of the error
 * 2 - Provide reassurance
 * 3 - Why it happened
 * 4 - Suggest alternative solution
 * 5 - Give them way out
 */

module.exports = {
  Error: require("./lib/BaseError"),
  ValidationError: require("./lib/ValidationError"),
  Exception: require("./lib/Exception"),
  ServiceError: require("./lib/BaseError")
};
