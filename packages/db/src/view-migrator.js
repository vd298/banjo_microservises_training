const fs = require("fs");
// const path = require("path");
// const views = {};
// const viewPath = path.join(__dirname, "../views");
// fs.readdirSync(viewPath)
//   .filter((file) => {
//     return file.indexOf(".") !== 0 && file.slice(-3) === ".js";
//   })
//   .forEach((file) => {
//     views[file] = require(path.join(viewPath, file));
//   });
async function dropViews(queryInterface) {
  const stack = fetchViewsList(0);
  console.log(`View-migrator. Dropping Views`);
  // const traceDependency = function(files) {
  //   for (let file of files) {
  //     const v = views[file];
  //     if (!v) {
  //       throw `View file '${path.join(viewPath, file)}' does not exist`;
  //     }
  //     if (stack.indexOf(file) < 0) {
  //       stack.push(v);
  //     }
  //     if (v.dependent_views && v.dependent_views.length) {
  //       traceDependency(v.dependent_views);
  //     }
  //   }
  // };
  // traceDependency(Object.keys(views));
  const transaction = await queryInterface.sequelize.transaction();
  try {
    for (const view of stack) {
      let currentView = require(__dirname + `/../views/${view}`);
      console.log(`View-Migrator. Dropping View:${currentView.view_name}`);
      if (
        typeof currentView.drop_sql == "string" &&
        currentView.drop_sql.indexOf("CASCADE") == -1
      ) {
        currentView.drop_sql += " CASCADE";
      }
      await queryInterface.sequelize.query(currentView.drop_sql, {
        transaction
      });
    }
    await transaction.commit();
  } catch (err) {
    await transaction.rollback();
    throw err;
  }
}
async function migrateViews(queryInterface, Sequelize) {
  const stack = fetchViewsList(1);
  console.log(`View-Migrator. Creating Views`);
  // const traceDependency = function(files) {
  //   for (let file of files) {
  //     const v = views[file];
  //     if (!v) {
  //       throw `View file '${path.join(viewPath, file)}' does not exist`;
  //     }
  //     if (v.dependent_views && v.dependent_views.length) {
  //       traceDependency(v.dependent_views);
  //     }
  //     if (stack.indexOf(file) < 0) {
  //       stack.push(v);
  //     }
  //   }
  // };
  // traceDependency(Object.keys(views));
  const transaction = await queryInterface.sequelize.transaction();
  try {
    for (const view of stack) {
      let currentView = require(__dirname + `/../views/${view}`);
      console.log(`View-Migrator. Creating View:${currentView.view_name}`);
      await queryInterface.sequelize.query(currentView.sql, {
        transaction
      });
    }
    await transaction.commit();
  } catch (err) {
    await transaction.rollback();
    throw err;
  }
}

/**
 * @method fetchViewsList
 * @author Vaibhav Vaidya
 * @param {number} orderBy Use 1 for insertion sequence and 0 for removal sequence
 * @since 23 Feb 2023
 * @summary Fetch list of views for insertion/removal
 */
function fetchViewsList(orderBy = 0) {
  if (typeof orderBy == "string" && !isNaN(orderBy)) {
    orderBy = Number(orderBy);
  }
  const viewFolder = __dirname + "/../views";
  let viewFileNames = fs.readdirSync(viewFolder).filter((file) => {
      return file.indexOf(".") !== 0 && file.slice(-3) === ".js";
    }),
    viewList = [];
  let checkCounterList = [],
    counterLimit = viewFileNames.length * 2;
  for (let i = 0; i < viewFileNames.length; i++) {
    let currentView = require(__dirname + `/../views/${viewFileNames[i]}`);
    let viewName = viewFileNames[i];

    //Fail safe for cyclic dependency.
    let checkObj = checkCounterList.find((ele) => ele && ele.name == viewName);
    if (checkObj == undefined) {
      checkCounterList.push({ name: viewName, counter: 0 });
    } else if (checkObj) {
      checkObj.counter++;
      if (checkObj.counter > counterLimit) {
        console.error(
          `Migrator.js. Func: fetchViewsList. Unable to build view list, Check for circular depedency. Unable to resolve for:${viewName}`
        );
        throw "CIRCULAR_VIEW_DEPENDENCY";
      }
    }
    //Main Logic
    if (
      currentView &&
      (currentView.dependent_views == undefined ||
        (Array.isArray(currentView.dependent_views) &&
          currentView.dependent_views.length == 0))
    ) {
      //No depended views, Directly push for execution
      if (orderBy == 0 && checkObj == undefined) {
        viewFileNames.splice(i, 1);
        viewFileNames.push(viewName);
        i--;
      } else {
        _checkpushView(viewList, viewName);
      }
    }
    //Depended views, Check those views are already pushed. If yes then Push else check next
    else if (
      Array.isArray(currentView.dependent_views) &&
      currentView.dependent_views.length > 0
    ) {
      let result = _checkArrayDependency(
        viewList,
        currentView.dependent_views,
        viewName,
        orderBy
      );
      if (result == false) {
        //Push views that are depended to last position
        viewFileNames.splice(i, 1);
        viewFileNames.push(viewName);
        i--;
      } else {
        _checkpushView(viewList, viewName);
      }
    } else if (currentView.dependent_views) {
      console.error(
        `Migrator.js. Func: fetchViewsList. Improper data in property dependent_views for file name:${viewName}`
      );
      viewFileNames.splice(i, 1);
      i--;
    }
  }
  return viewList;
}

function _checkArrayDependency(
  viewList,
  dependent_views,
  viewName,
  orderBy = 0
) {
  let resultFlag = false;
  for (let i = 0; i < dependent_views.length; i++) {
    let searchString = dependent_views[i];
    searchString = searchString.replace(/\.js$/g, "");
    searchString += `.js`;
    if (viewList.find((ele) => ele == searchString)) {
      resultFlag = true;
    } else {
      resultFlag = false;
      break;
    }
  }
  return orderBy ? resultFlag : !resultFlag;
}

function _checkpushView(viewArr, viewName) {
  if (Array.isArray(viewArr) && typeof viewName == "string") {
    let foundFlag = viewArr.find((ele) => ele == viewName);
    if (foundFlag == undefined) {
      viewArr.push(viewName);
    }
  }
}

module.exports = {
  dropViews,
  migrateViews,
  fetchViewsList
};
