const configD = require("../../config");
module.exports = {
  dependent_views: ["vw_tx_transfer"], //Specify view file name
  sql: `
  CREATE MATERIALIZED VIEW ${config.schema}.mvw_fetch_yesterday_transactions_activity as (
    select
        t.transfer_type AS "Transaction Type",
        COUNT(*) AS "Count",
        t.activity AS "Activity",
        ROUND(CAST(SUM(t.src_amount) AS numeric), 2) as "Total",
        ROUND(CAST(SUM(t.src_amount) / ${configD.usdRate} AS numeric), 2) AS "Total(USD)"
      from
        ${config.schema}.vw_tx_transfer t
     WHERE t.src_currency in('INR') and t.ctime >= date_trunc('day', CURRENT_DATE - INTERVAL '1 day') AND
        t.ctime <= date_trunc('day', CURRENT_DATE) 
      group by
        t.activity,
        t.transfer_type
      order by
        t.transfer_type
  ) with data
`,
  view_name: `${config.schema}.mvw_fetch_yesterday_transactions_activity`,
  drop_sql: `DROP MATERIALIZED view if exists ${config.schema}.mvw_fetch_yesterday_transactions_activity`
};
