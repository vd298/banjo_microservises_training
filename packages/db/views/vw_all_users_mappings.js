module.exports = {
  dependent_views: [], //Specify view file name
  sql: `
    CREATE OR REPLACE view ${config.schema}.vw_all_users_mappings as 
  select
      ua.*,
      u.first_name,
      u.last_name,
      u.email,
      u.mobile,
      u.username,
      u.status as user_status,
      u.user_type,
      u.ips,
      u.webhook_url,
      u.telegram_id,
      a.name as account_name,
      a.legal_entity ,
      a.type,
      a.status as acc_status, 
      r.role_name as primary_role,
      (with select_users as(
      select
          json_array_elements_text (additional_roles ->'_arr') as sec_role,
          id
      from
          ${config.schema}.user_accounts ua2
      where
          ua2.id = ua.id
  )
      select
          array_agg(r.role_name)
      from
          select_users ua
      inner join ${config.schema}.roles r on
          r.id = ua.sec_role::uuid) as secondary_role
  from
      ${config.schema}.users u
  inner join ${config.schema}.user_accounts ua on
      ua.user_id = u.id
  inner join ${config.schema}.accounts a on
      ua.account_id = a.id
  left join ${config.schema}.roles r on 
      r.id = ua.main_role;`,
  view_name: `${config.schema}.vw_all_users_mappings`,
  drop_sql: `DROP view if exists ${config.schema}.vw_all_users_mappings`,
};
