module.exports = {
  sql: `
  CREATE OR REPLACE VIEW ${config.schema}.vw_bank_acc_limits as 
    with transfer_info as (
	select distinct on (tr.transfer_id) tr.amount, t.dst_wallet_id  from ${config.schema}.transactions tr
	left join ${config.schema}.transfers t on (tr.transfer_id=t.id)
	where tr.activity in ('BRN_SUBMITTED', 'APPROVED')
	and tr.mtime< (DATE_TRUNC('day',NOW()))
)
, agg_transfer_info as (
	select coalesce(sum(ti.amount),0) as daily_deposit, ti.dst_wallet_id from transfer_info ti
	group by ti.dst_wallet_id 
)

select ba.id, ba.account_name, ba.acc_no, ba.removed, ba.currency, ba.status, coalesce(t.daily_deposit,0) as daily_deposit from 
${config.schema}.bank_accounts ba
left join ${config.schema}.merchant_account_mappings mam on (mam.bank_account=ba.id and mam.removed=0)
left join ${config.schema}.wallets w on (mam.id=w.merchant_account_mapping_id and w.removed=0)
left join agg_transfer_info t on (w.id=t.dst_wallet_id)`,
  view_name: `${config.schema}.vw_bank_acc_limits`,
  drop_sql: `DROP VIEW IF EXISTS ${config.schema}.vw_bank_acc_limits`
};
