module.exports = {
  dependent_views: ["vw_bank_acc_payment_protocols.js"], //Specify view file name
  sql: `CREATE OR REPLACE view ${config.schema}.vw_bank_accounts_limits as(
select bapp.*,
coalesce(rl2.min_amount,rl.min_amount) as min_amount,
coalesce(rl2.max_amount, rl.max_amount) as max_amount,
case when (rl2.min_amount is not null or rl2.max_amount is not null) then 0 else 1 end as global_limit,
coalesce(rl2.daily_deposit, rl.daily_deposit) as daily_deposit,
coalesce(rl2.limit_type, rl.limit_type) as limit_type,
coalesce(rl2.id, rl.id) as _id,
coalesce(rl2.realm_id, rl.realm_id) as realm_id
from ${config.schema}.vw_bank_acc_payment_protocols bapp
left join ${config.schema}.ref_limits rl on (bapp.payment_protocol_id=rl.protocol_id and rl.bank_account_protocol_id is null)
left join ${config.schema}.ref_limits rl2 on (bapp.payment_protocol_id=rl2.protocol_id and bapp.currency=rl2.currency and rl2.bank_account_protocol_id=bapp.bank_account_protocol_id
)
)
`,
  view_name: `${config.schema}.vw_bank_accounts_limits`,
  drop_sql: `DROP VIEW IF EXISTS ${config.schema}.vw_bank_accounts_limits`
};
