module.exports = {
  dependent_views: [], //Specify view file name
  sql: `
  set search_path to ${config.schema};
  CREATE OR REPLACE VIEW ${config.schema}.vw_tx_transfer as 
with basic_transfer_info as (
select t.id, t.id as transfer_id, t.ctime, t.mtime, t.removed, t.src_amount, t.src_currency, t.dst_amount, t.dst_currency, t.ref_num,
t.maker, t.checker, t.remover, t.updater, t.rtime, t.signobject,t.settlement_id,
t.is_exchange, t.exchange_id, t."data", t.account_id, t.merchant_account_id, t.order_num, t.transfer_type, t.transfer_type as "type", t.event_name, t.description,
t.src_wallet_id, src_w.acc_label as src_wallet_label, src_w.acc_no as src_wallet_acc_no, src_w.currency as src_wallet_currency, src_w.ref_num as src_wallet_ref_num, src_w.type as src_wallet_type, src_w.bank_account_id as src_bank_acc_id,
t.dst_wallet_id, dst_w.acc_label as dst_wallet_label, dst_w.acc_no as dst_wallet_acc_no, dst_w.currency as dst_wallet_currency, dst_w.ref_num as dst_wallet_ref_num,dst_w.type as dst_wallet_type, dst_w.bank_account_id as dst_bank_acc_id,
src_ma_acc.name as src_macc_name, src_ma_acc.acc_no as src_macc_no, src_ma_acc.status as src_macc_status, src_ma_acc.category as src_macc_category,src_ma_acc.auto_payouts,src_ma_acc.expiry_hours,src_ma_acc.payout_expiry_hours,
src_acc.name as src_acc_name, src_acc.registration_no as src_acc_registration_no, src_acc.status as src_acc_status, src_acc.country as src_acc_country, src_acc.type as src_acc_type, src_acc.realm_id,
src_b.short_name as src_bank_short_name, src_b.name as src_bank_name, src_b.address as src_bank_address, src_b.city as src_bank_city, src_b.country as src_bank_country,
dst_b.short_name as dst_bank_short_name, dst_b.name as dst_bank_name, dst_b.address as dst_bank_address, dst_b.city as dst_bank_city, dst_b.country as dst_bank_country,
t.counterpart_id, cp.full_name as counter_party_name, cp.is_legal_entity as counter_party_legal_entity, cp.bank_name as counter_party_bank_name, cp.bank_code as counter_party_bank_code, b.vpa as beneficiary_vpa, b.ifsc as beneficiary_ifsc, b.acc_no  as beneficiary_acc_no, b.first_name  as beneficiary_first_name, b.last_name as beneficiary_last_name,
src_ba.acc_no as src_bank_acc_no, dst_ba.acc_no as dst_bank_acc_no
from ${config.schema}.transfers t
--join columns are not null
inner join (
	${config.schema}.merchant_accounts src_ma_acc
		inner join ${config.schema}.accounts src_acc on (src_ma_acc.account_id=src_acc.id))
	on (t.merchant_account_id =src_ma_acc.id)
left join ${config.schema}.counterparts cp on (t.counterpart_id=cp.id)
--join Columns are nullable, Exchanges commented currently
--left join ${config.schema}.exchanges ex on (t.exchange_id=ex.id)
--join on wallets and fetch info
left join (${config.schema}.wallets src_w 
	inner join ${config.schema}.merchant_account_mappings src_map on (src_w.merchant_account_mapping_id=src_map.id)
	inner join ${config.schema}.bank_accounts src_ba on (src_map.bank_account=src_ba.id)
	inner join ${config.schema}.banks src_b on (src_ba.bank_id=src_b.id)
) on (t.src_wallet_id=src_w.id)
left join (${config.schema}.wallets dst_w 
	inner join ${config.schema}.merchant_account_mappings dst_map on (dst_w.merchant_account_mapping_id=dst_map.id)
	inner join ${config.schema}.bank_accounts dst_ba on (dst_map.bank_account=dst_ba.id)
	inner join ${config.schema}.banks dst_b on (dst_ba.bank_id=dst_b.id)
) on (t.dst_wallet_id=dst_w.id)
left join ${config.schema}.beneficiaries b ON (t.beneficiary_id = b.id)
)
,last_transaction_info as(
--this will always fetch the last status
	select distinct on (tr.transfer_id) tr.* from ${config.schema}.transactions tr
	where tr.removed=0 and tr.activity not in ('FEE','REVERSE_FEE')
	order by tr.transfer_id, tr.ctime desc
)
,brn_transaction_info as(
--Fetch BRN Transaction info
	select distinct on (tr.transfer_id) tr.transfer_id,tr.brn as last_brn from ${config.schema}.transactions tr
	where tr.removed=0 AND tr.activity<>'FEE' and tr.activity in ('BRN_SUBMIT', 'APPROVED','SETTLEMENT_APPROVED','INTERNAL_FUNDS_SHIFT') and tr.brn is not null
	order by tr.transfer_id, tr.ctime desc
)
,action_info as(
--Fetch Activity info
	select distinct on (tr.transfer_id) tr.transfer_id, tr.data->>'activity_action' as activity_action, tr.data->>'action_type' as action_type,
	tr.data->>'admin_id' as admin_id
	from ${config.schema}.transactions tr
	where tr.removed=0 and tr.activity in ('REJECTED', 'APPROVED','SETTLEMENT_APPROVED')
	order by tr.transfer_id, tr.ctime desc
)
select t.*, tr.status, tr.activity, bti.last_brn, aai.activity_action, aai.action_type, aai.admin_id from basic_transfer_info t
left join last_transaction_info tr on (t.transfer_id=tr.transfer_id)
left join brn_transaction_info bti on (t.transfer_id=bti.transfer_id)
left join action_info aai on (t.transfer_id=aai.transfer_id)
`,
  view_name: `${config.schema}.vw_tx_transfer`,
  drop_sql: `DROP view if exists ${config.schema}.vw_tx_transfer`
};
