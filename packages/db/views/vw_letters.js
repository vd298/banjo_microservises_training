module.exports = {
  dependent_views: [], //Specify view file name
  sql: `CREATE OR REPLACE view ${config.schema}.vw_letters as select *, 
          (case when send_email then 10 else 0 end + 
          case when send_sms then 7 else 0 end + case when send_push then 15 else 0 end) 
          as channel from ${config.schema}.letters l ;`,
  view_name: `${config.schema}.vw_letters`,
  drop_sql: `DROP view if exists ${config.schema}.vw_letters`
};
