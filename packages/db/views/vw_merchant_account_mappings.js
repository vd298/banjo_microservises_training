module.exports = {
  dependent_views: [], //Specify view file name
  sql: `
  set search_path to ${config.schema};
  CREATE OR REPLACE VIEW ${config.schema}.vw_merchant_account_mappings as 
SELECT ba.acc_no,
       ba.request_timestamp as bank_account_request_timestamp,
       ba.account_name as bank_account_name,
       ba.id           as bank_account_id,
       ba.currency     as bank_account_currency,
       a.name          as merchant_name,
       a.id            as merchant_id,
       ma.name         as merchant_account_name,
       ma.acc_no       as merchant_account_no,
       ma.currency     as merchant_account_currency,
       b.name          as bank_name,
       b.id            as bank_id,
       pp.name         as payment_provider_name,
       pp.id           as payment_provider_id,
       (mam.start_time < now() AND (mam.end_time is null OR mam.end_time > now())) as active_relation,
       mam.id,                 
       mam.ctime,              
       mam.mtime,              
       mam.removed,            
       mam.maker,             
       mam.checker,           
       mam.remover,           
       mam.updater,            
       mam.rtime,            
       mam.signobject,         
       mam.merchant_account_id,
       mam.bank_account,     
       mam.start_time,       
       mam.end_time,           
       a.realm_id      as realm_id
FROM ${config.schema}.merchant_account_mappings mam
         INNER JOIN ${config.schema}.bank_accounts ba on mam.bank_account = ba.id
         INNER JOIN ${config.schema}.payment_providers pp on pp.id = ba.payment_provider_id AND pp.removed=0
         INNER JOIN ${config.schema}.banks b on ba.bank_id = b.id AND b.removed=0
         INNER JOIN ${config.schema}.merchant_accounts ma on mam.merchant_account_id = ma.id  AND ma.removed=0
         INNER JOIN ${config.schema}.accounts a on ma.account_id = a.id AND a.removed=0`,
  view_name: `${config.schema}.vw_merchant_account_mappings`,
  drop_sql: `set search_path to ${config.schema}; DROP VIEW IF EXISTS ${config.schema}.vw_merchant_account_mappings`
};
