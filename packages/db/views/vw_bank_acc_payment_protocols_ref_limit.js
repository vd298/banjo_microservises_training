module.exports = {
  dependent_views: [], //Specify view file name
  sql: `CREATE OR replace VIEW ${config.schema}.vw_bank_acc_payment_protocols_ref_limit AS SELECT 
       coalesce(rl2.id,rl.id) as id,
       bap.id as bank_accounts_protocol_id,
       ba.id AS bank_account_id,
       ba.ctime,
       ba.mtime,
       ba.removed,
       ba.maker,
       ba.checker,
       ba.remover,
       ba.updater,
       ba.rtime,
       ba.signobject,
       ba.bank_id,
       ba.account_name,
       ba.acc_no,
       ba.currency,
       ba.payment_provider_id,
       ba.status,
       ba.request_timestamp,
       ba.fc_merchant_id,
       pp.protocol_type,
       pp.identifier_label,
       pp.account_label,
       pp.instruction,
       pp.transaction_identifier_label,
       bap.id AS bank_account_protocol_id,
       bap.payment_protocol_id,
       coalesce(rl2.min_amount,rl.min_amount) as min_amount,
coalesce(rl2.max_amount, rl.max_amount) as max_amount,
case when (rl2.min_amount is not null or rl2.max_amount is not null) then 0 else 1 end as global_limit,
coalesce(rl2.daily_deposit, rl.daily_deposit) as daily_deposit,
coalesce(rl2.limit_type, rl.limit_type) as limit_type
FROM ${config.schema}.bank_accounts ba
left JOIN ${config.schema}.bank_accounts_protocols bap ON bap.bank_account_id = ba.id AND bap.removed = 0
join ${config.schema}.payment_protocols pp on bap.payment_protocol_id=pp.id and pp.removed=0
left join ${config.schema}.ref_limits rl on (bap.payment_protocol_id=rl.protocol_id and rl.bank_account_protocol_id is null)
left join ${config.schema}.ref_limits rl2 on (bap.payment_protocol_id=rl2.protocol_id and ba.currency=rl2.currency and rl2.bank_account_protocol_id=bap.id
)
`,
  view_name: `${config.schema}.vw_bank_acc_payment_protocols_ref_limit`,
  drop_sql: `DROP VIEW IF EXISTS ${config.schema}.vw_bank_acc_payment_protocols_ref_limit`
};
