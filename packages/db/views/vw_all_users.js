module.exports = {
    dependent_views: [], //Specify view file name
    sql: `
    CREATE OR REPLACE view ${config.schema}.vw_all_users as 
  select
      u.*,
      ua.id as user_account_id,
      ua.account_id as account_id,
      a.name as account_name,
      a.legal_entity ,
      a.type,
      a.status as acc_status,
      ua.ctime as u_a_ctime,
      ua.main_role ,
      ua.additional_roles,
      ua.start_date,
      r.role_name as primary_role,
      (
          select
            count(*)
           
          from ${config.schema}.user_accounts ua2
          where
            u.id = ua2.user_id
            AND ua2.start_date  < now() AND (ua2.end_date  is null OR ua2.end_date > now())
      ) allocation_count,
      (with select_users as(
      select
          json_array_elements_text (additional_roles ->'_arr') as sec_role,
          id
      from
          ${config.schema}.user_accounts ua2
      where
          ua2.id = ua.id
  )
      select
          array_agg(r.role_name)
      from
          select_users ua
      inner join ${config.schema}.roles r on
          r.id = ua.sec_role::uuid) as secondary_role
  from
      ${config.schema}.users u
  inner join ${config.schema}.user_accounts ua on
      ua.user_id = u.id AND ((ua.end_date is null or ua.end_date>now()) AND ua.start_date<now())
  inner join ${config.schema}.accounts a on
      ua.account_id = a.id
  left join ${config.schema}.roles r on 
      r.id = ua.main_role;`,
    view_name: `${config.schema}.vw_all_users`,
    drop_sql: `DROP view if exists ${config.schema}.vw_all_users`
  };
  