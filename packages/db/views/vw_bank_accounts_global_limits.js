module.exports = {
  dependent_views: [], //Specify view file name
  sql: `CREATE OR REPLACE view ${config.schema}.vw_bank_accounts_global_limits as(
      SELECT rl.*, pp.protocol_type from ref_limits rl
        left join payment_protocols pp on pp.id = rl.protocol_id and pp.removed = 0
         where rl.bank_account_protocol_id is null
)
`,
  view_name: `${config.schema}.vw_bank_accounts_global_limits`,
  drop_sql: `DROP VIEW IF EXISTS ${config.schema}.vw_bank_accounts_global_limits`
};
