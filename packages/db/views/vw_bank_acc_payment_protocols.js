module.exports = {
  dependent_views: [], //Specify view file name
  sql: `CREATE OR replace VIEW ${config.schema}.vw_bank_acc_payment_protocols AS SELECT bap.id as id,
       ba.id AS bank_account_id,
       ba.ctime,
       ba.mtime,
       ba.removed,
       ba.maker,
       ba.checker,
       ba.remover,
       ba.updater,
       ba.rtime,
       ba.signobject,
       ba.bank_id,
       ba.account_name,
       ba.acc_no,
       ba.currency,
       ba.payment_provider_id,
       ba.status,
       ba.request_timestamp,
       ba.fc_merchant_id,
       pp.protocol_type,
       pp.identifier_label,
       pp.account_label,
       pp.instruction,
       pp.transaction_identifier_label,
       bap.id AS bank_account_protocol_id,
       bap.payment_protocol_id
FROM ${config.schema}.bank_accounts ba
left JOIN ${config.schema}.bank_accounts_protocols bap ON bap.bank_account_id = ba.id AND bap.removed = 0
left join ${config.schema}.payment_protocols pp on bap.payment_protocol_id=pp.id and pp.removed=0;
`,
  view_name: `${config.schema}.vw_bank_acc_payment_protocols`,
  drop_sql: `DROP VIEW IF EXISTS ${config.schema}.vw_bank_acc_payment_protocols`
};
