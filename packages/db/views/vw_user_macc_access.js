module.exports = {
  dependent_views: [], //Specify view file name
  sql: `
  CREATE OR REPLACE VIEW ${config.schema}.vw_user_macc_access as (
  select ua.id as user_account_id, ua.user_id as user_id, ua.main_role, ua.additional_roles, ma.id as merchant_account_id, ua.start_date, ua.end_date
FROM ${config.schema}.user_accounts ua
inner join ${config.schema}.users u on (u.id=ua.user_id and u.removed=0)
inner join ${config.schema}.accounts ac on (ac.id=ua.account_id and ac.removed=0)
inner join ${config.schema}.merchant_accounts ma on (ma.account_id=ac.id and ma.removed=0)
inner join ${config.schema}.permissions p on (p.user_id=u.id and p.removed=0 and (p.allow=true or p.write=true))
inner join ${config.schema}.resources r on (r.id=p.resource_id and r.removed=0 and ma.id=r.object_id and r.object_type = 'MERCHANT_ACCOUNT') 
where ua.removed=0 and ua.end_date is null)
`,
  view_name: `${config.schema}.vw_user_macc_access`,
  drop_sql: `DROP view if exists ${config.schema}.vw_user_macc_access`
};
