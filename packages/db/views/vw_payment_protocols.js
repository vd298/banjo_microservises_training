module.exports = {
  dependent_views: [], //Specify view file name
  sql: `CREATE OR REPLACE view ${config.schema}.vw_payment_protocols as select id as _id,protocol_type as name,* from ${config.schema}.payment_protocols`,
  view_name: `${config.schema}.vw_payment_protocols`,
  drop_sql: `DROP view if exists ${config.schema}.vw_payment_protocols`
};
