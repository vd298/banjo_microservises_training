module.exports = {
  dependent_views: [], //Specify view file name
  sql: `
  CREATE OR REPLACE VIEW ${config.schema}.vw_resources_permissions
AS SELECT r.id,
	r.identifier,
	r.alias,
	r.resource_type,
	r.object_type,
	r.object_id,
	p.resource_id,
	p.role_id,
	p.user_id,
	p.allow,
	p.deny,
	p."write"
FROM ${config.schema}.resources r
left join ${config.schema}.permissions p on p.resource_id = r.id`,
  view_name: `${config.schema}.vw_resources_permissions`,
  drop_sql: `DROP view if exists ${config.schema}.vw_resources_permissions`
};
