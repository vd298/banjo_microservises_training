module.exports = {
  dependent_views: [], //Specify view file name
  sql: `CREATE OR REPLACE view ${config.schema}.vw_transactions as 
  select tr.id, tr.id as transaction_id,  tr.ctime, tr.mtime, tr.removed, tr.maker, tr.checker, tr.remover, tr.updater, tr.rtime, tr.signobject, tr.transfer_id, tr.statement_note, tr.internal_note, tr.data,
  tr.src_acc_id, tr.dst_acc_id, tr.amount, tr.src_balance, tr.dst_balance, tr.currency, tr.tariff_id, tr.plan_id, tr.status,
  tr.brn, tr.hash, tr.activity, tr.txtype,
  src_macc.name as src_macc_name, src_macc.acc_no as src_macc_no, src_macc.status as src_macc_status, src_macc.category as src_macc_category,
  dst_macc.name as dst_macc_name, dst_macc.acc_no as dst_macc_no, dst_macc.status as dst_macc_status, dst_macc.category as dst_macc_category,
  ta.name as tariff_name, ta.description as tariff_description,
  p.name as plan_name, p.description as plan_description
  from ${config.schema}.transactions tr
  left join ${config.schema}.merchant_accounts src_macc on (tr.src_acc_id=src_macc.id)
  left join ${config.schema}.merchant_accounts dst_macc on (tr.dst_acc_id=dst_macc.id)
  left join ${config.schema}.tariffs ta on (tr.tariff_id=ta.id)
  left join ${config.schema}.tariffplans p on (tr.plan_id=p.id)
  `,
  view_name: `${config.schema}.vw_transactions`,
  drop_sql: `DROP view if exists ${config.schema}.vw_transactions`
};
