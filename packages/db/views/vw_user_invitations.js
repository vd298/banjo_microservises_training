module.exports = {
  dependent_views: [], //Specify view file name
  sql: `CREATE OR REPLACE VIEW ${config.schema}.vw_user_invitations
          AS SELECT uai.id,
                  uai.account_id,
                  uai.user_id,
                  uai.accepted,
                  uai."role",
                  uai."token",
                  uai.counter,
                  uai.ctime,
                  uai.mtime,
                  uai.removed,
                  uai.maker,
                  uai.checker,
                  uai.remover,
                  uai.updater,
                  uai.rtime,
                  uai.signobject,
                  uai.expiry,
                  uai.merchant_account_id,
                  ua.additional_roles,
                  ua.start_date,
                  ua.end_date,
                  u.mobile_verified,
                  u.email_verified,
                  u.first_name,
                  u.last_name,
                  u.user_type,
                  u.status,
                  u.email
          FROM ${config.schema}.user_account_invitations uai
          join ${config.schema}.users u on u.id = uai.user_id
          left join ${config.schema}.user_accounts ua on ua.user_id = uai.user_id`,
  view_name: `${config.schema}.vw_user_invitations`,
  drop_sql: `DROP view if exists ${config.schema}.vw_user_invitations`
};
