module.exports = {
  dependent_views: ["vw_merchant_account_mappings.js", "vw_bank_accounts.js"], //Specify view file name
  sql: `
  CREATE OR REPLACE VIEW ${config.schema}.vw_bank_accounts_per_merchant_account as 
    SELECT distinct on (ba.id) ba.* FROM vw_bank_accounts ba`,
  view_name: `${config.schema}.vw_bank_accounts_per_merchant_account`,
  drop_sql: `DROP VIEW IF EXISTS ${config.schema}.vw_bank_accounts_per_merchant_account`
};
