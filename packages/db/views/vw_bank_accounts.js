module.exports = {
  dependent_views: ["vw_merchant_account_mappings.js"], //Specify view file name
  sql: `
  CREATE OR REPLACE VIEW ${config.schema}.vw_bank_accounts as 
    SELECT ba.*,
          b.name as bank_name,
          b.bank_type as bank_type,
          b.realm_id as realm_id,
          coalesce(mam.active_relation,false) as active_relation,
          mam.id as mapping_id,
          mam.merchant_id,
          mam.merchant_name,
          mam.merchant_account_id,
          mam.merchant_account_name,
          mam.merchant_account_no,
          (
            select
              count(*)
            from
              ${config.schema}.vw_merchant_account_mappings vmam
            where
              vmam.bank_account_id = ba.id AND vmam.start_time < now() AND (vmam.end_time is null OR vmam.end_time > now())) allocation_count
    FROM ${config.schema}.bank_accounts ba
            INNER JOIN ${config.schema}.banks b ON ba.bank_id = b.id AND b.removed = 0
            LEFT JOIN ${config.schema}.vw_merchant_account_mappings mam ON mam.bank_account_id = ba.id AND mam.active_relation`,
  view_name: `${config.schema}.vw_bank_accounts`,
  drop_sql: `DROP VIEW IF EXISTS ${config.schema}.vw_bank_accounts`
};
