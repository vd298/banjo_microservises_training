module.exports = {
  dependent_views: ["vw_bank_accounts.js", "vw_merchant_account_mappings.js"], //Specify view file name
  sql: `
  set search_path to ${config.schema};
  CREATE OR REPLACE VIEW ${config.schema}.vw_merchant_accounts as
  select ma.*, (select coalesce(count(*), '0') as active_count from ${config.schema}.vw_bank_accounts where status = 'ACTIVE' and removed = 0 and merchant_account_id = ma.id),
(select coalesce(count(*), '0') as inactive_count from ${config.schema}.vw_bank_accounts where status = 'INACTIVE' and removed = 0 and merchant_account_id = ma.id)
,c."name" as category_name from ${config.schema}.merchant_accounts ma
left join ${config.schema}.categories c on c.id = ma.category
where ma.removed = 0;`,
  view_name: "vw_merchant_accounts",
  drop_sql: `set search_path to ${config.schema}; DROP VIEW IF EXISTS ${config.schema}.vw_merchant_accounts`
};
