"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { schema: config.schema, tableName: "bank_config" },
      [
        {
          id: "6135779e-4cae-46b6-b6f7-55ff4b2a7f0f",
          ctime: "2023-03-22T21:23:34.140Z",
          mtime: "2023-03-22T21:23:34.140Z",
          removed: 0,
          maker: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4",
          checker: null,
          remover: null,
          updater: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4",
          rtime: null,
          signobject: null,
          domain_name: "indusnet.indusind.com",
          bank_id: "c769eac0-a1d9-470a-9f55-c03d05a27d40",
          brn_regex: "[0-9]{12}"
        },
        {
          id: "a171e077-7126-48d8-a24f-ecf9fd8aea26",
          ctime: "2023-03-22T21:23:34.135Z",
          mtime: "2023-03-22T21:23:34.135Z",
          removed: 0,
          maker: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4",
          checker: null,
          remover: null,
          updater: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4",
          rtime: null,
          signobject: null,
          domain_name: "netbanking.hdfcbank.com",
          bank_id: "b96dcd7c-0a93-4058-8af8-88752e5e5553",
          brn_regex: "[0-9]{12}"
        },
        {
          id: "38f229d8-1acf-4fcc-b1fd-03013f2c3051",
          ctime: "2023-03-22T21:23:34.138Z",
          mtime: "2023-03-22T21:23:34.138Z",
          removed: 0,
          maker: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4",
          checker: null,
          remover: null,
          updater: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4",
          rtime: null,
          signobject: null,
          domain_name: "cibnext.icicibank.com",
          bank_id: "64860a1c-888a-4de1-9927-46b5d8b1b96e",
          brn_regex: "[0-9]{12}"
        },
        {
          id: "bf66eb95-56e1-49e9-832d-adebb589a185",
          ctime: "2023-03-22T21:23:34.139Z",
          mtime: "2023-03-22T21:23:34.139Z",
          removed: 0,
          maker: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4",
          checker: null,
          remover: null,
          updater: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4",
          rtime: null,
          signobject: null,
          domain_name: "my.idfcfirstbank.com",
          bank_id: "55a87d9a-0999-4bef-8551-f52902adc0ec",
          brn_regex: "[0-9]{12}"
        }
      ],
      { ignoreDuplicates: true }
    );
  },

  down: (queryInterface, Sequelize) => {
    const Op = Sequelize.Op;
    return queryInterface.bulkDelete(
      { schema: config.schema, tableName: "payment_protocols" },
      {
        id: {
          [Op.in]: [
            "6135779e-4cae-46b6-b6f7-55ff4b2a7f0f",
            "a171e077-7126-48d8-a24f-ecf9fd8aea26",
            "38f229d8-1acf-4fcc-b1fd-03013f2c3051",
            "bf66eb95-56e1-49e9-832d-adebb589a185"
          ]
        }
      },
      {}
    );
  }
};
