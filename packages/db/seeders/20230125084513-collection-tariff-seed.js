"use strict";
const vw = require("../views/vw_tx_transfer");
const tariffId = "e44a55bc-3398-409e-9dd5-c2860f61f529";
const planId = "7fbf6a37-dfa9-43c5-a6ad-2aff43528c57";
const triggerId = "2a963444-1318-4e6f-b1da-40457af93718";
const realmId = "046cce25-f407-45c7-8be9-3bf198093408";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.sequelize.query(
        `INSERT INTO ${config.schema}.triggers (id, ctime, mtime, removed, maker, checker, remover, updater, rtime, signobject, name, service, method, cron, data, ttype, tablename, conditions) VALUES ('${triggerId}', '2023-01-07 13:04:11.574000', '2023-01-25 08:42:29.828000', 0, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4', null, null, null, null, null, 'CollectionTrigger', 'transaction-service', 'collectionTrigger', null, '{"trigger":"transaction-service:collectionTrigger","variables":[],"data":{"transfer_id":"6d1f5d3f-1e9d-45c3-8dcf-bab95e458335","action":"APPROVE_TX","description":"897898","currency":"INR","amount":550,"order_id":"1345692","bank_account_id":"35a6c5e3-021d-4577-9f32-984d12006e70","wallet_id":"f4dee061-5b24-4a8f-a967-fd749a464496","data":{"ua":{"ua":"PostmanRuntime/7.29.2","browser":{},"engine":{},"os":{},"device":{},"cpu":{}},"note":"approving transaction"}},"transfers":{"service":"transaction-service","method":"collectionTrigger","data":{"transfer_id":"6d1f5d3f-1e9d-45c3-8dcf-bab95e458335","action":"APPROVE_TX","description":"897898","currency":"INR","amount":550,"order_id":"1345692","bank_account_id":"35a6c5e3-021d-4577-9f32-984d12006e70","wallet_id":"f4dee061-5b24-4a8f-a967-fd749a464496","data":{"ua":{"ua":"PostmanRuntime/7.29.2","browser":{},"engine":{},"os":{},"device":{},"cpu":{}},"note":"approving transaction"}},"list":[],"tags":[],"output":{}},"result":{"dst_wallet_id":"f4dee061-5b24-4a8f-a967-fd749a464496","src_wallet_id":"db19bf2a-8ab2-40ea-9086-777e6b338d67","dst_currency":"INR","dst_amount":550,"src_currency":"INR","src_amount":550,"fee_wallet_id":"9eb51629-9daa-4579-b0b4-f48170449433"}}', 1, null, null);`,
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `INSERT INTO ${config.schema}.tariffs (id, name, description, trigger, data, variables, actions, rules, stop_on_rules, pid, ctime, mtime, rtime, signobject, removed, maker, remover, checker, updater) VALUES ('${tariffId}', 'Collection Transaction (Default)', 'Money Collection Transaction', '${triggerId}', '{"trigger":"transaction-service:collectionTrigger","variables":{},"data":{"transfer_id":"5b5c618c-ed6c-41c8-adcc-43d2fad0f04c","action":"APPROVE_TX","description":"897898","currency":"INR","amount":5361,"order_id":"1345684","bank_account_id":"35a6c5e3-021d-4577-9f32-984d12006e70","wallet_id":"f4dee061-5b24-4a8f-a967-fd749a464496","data":{"ua":{"ua":"PostmanRuntime/7.29.2","browser":{},"engine":{},"os":{},"device":{},"cpu":{}}}},"transfers":{"service":"transaction-service","method":"collectionTrigger","data":{"transfer_id":"5b5c618c-ed6c-41c8-adcc-43d2fad0f04c","action":"APPROVE_TX","description":"897898","currency":"INR","amount":5361,"order_id":"1345684","bank_account_id":"35a6c5e3-021d-4577-9f32-984d12006e70","wallet_id":"f4dee061-5b24-4a8f-a967-fd749a464496","data":{"ua":{"ua":"PostmanRuntime/7.29.2","browser":{},"engine":{},"os":{},"device":{},"cpu":{}}}},"list":{},"tags":{},"output":{}},"result":{"dst_wallet_id":"f4dee061-5b24-4a8f-a967-fd749a464496","src_wallet_id":"db19bf2a-8ab2-40ea-9086-777e6b338d67","dst_currency":"INR","dst_amount":5361,"src_currency":"INR","src_amount":5361,"fee_wallet_id":"9eb51629-9daa-4579-b0b4-f48170449433"}}', '{"_arr":[{"id":"extModel807-1","key":"COLLECTION_CHARGE","value":"2","descript":"Collection Charges (Pecent)"}]}', '{"_arr":[{"type":"transfer_activity","name":"Collection Request","options":{"txtype":"COLLECTION","txsubtype":"REQUEST","note":"Transaction Requested"},"id":"extModel228-2"},{"type":"transfer_activity","name":"Page Opned","options":{"txtype":"COLLECTION","txsubtype":"OPEN","note":"Payment page opened"},"id":"extModel269-1"},{"type":"transfer","name":"Approve Transaction","options":{"txtype":"COLLECTION","txsubtype":"APPROVED","parent_id":"root:data:order_id","acc_src":"root:result:src_wallet_id","acc_dst":"root:result:dst_wallet_id","fee":"100","currency":"src","feetype":"PERCENTS","amount_field":"root:data:amount","minimum_amount":"","hold":false,"hidden":false,"description_src":"","description_dst":""},"id":"extModel940-1"},{"type":"transfer","name":"Fee Charges","options":{"txtype":"COLLECTION","txsubtype":"FEE","parent_id":"root:data:order_id","acc_src":"root:result:dst_wallet_id","acc_dst":"root:result:fee_wallet_id","fee":"$COLLECTION_CHARGE","currency":"src","feetype":"PERCENTS","amount_field":"root:data:amount","minimum_amount":"","hold":false,"hidden":false,"description_src":"","description_dst":""},"id":"extModel201-1"},{"type":"transfer_activity","name":"Reject Transaction","options":{"txtype":"COLLECTION","txsubtype":"REJECTED","note":"Rejected"},"id":"extModel269-3"},{"type":"transfer_activity","name":"BRN Submitted","options":{"txtype":"COLLECTION","txsubtype":"BRN_SUBMIT","note":"BRN Submitted"},"id":"extModel228-1"}]}', '{"_arr":[{"id":"extModel261-1","render_function":null,"value_field":"root:data:action","ne":false,"operator":"=","value":"''SUBMIT_BRN''","action":["BRN Submitted"],"result":true,"stop":true},{"id":"extModel559-1","render_function":null,"value_field":"root:data:action","ne":false,"operator":"=","value":"''REJECT_TX''","action":["Reject Transaction"],"result":true,"stop":true},{"id":"extModel375-1","render_function":null,"value_field":"root:data:action","ne":false,"operator":"=","value":"''APPROVE_TX''","action":["Approve Transaction"],"result":true,"stop":false},{"id":"extModel234-1","render_function":null,"value_field":"root:data:action","ne":false,"operator":"=","value":"''APPROVE_TX''","action":["Fee Charges"],"result":true,"stop":false},{"id":"extModel252-1","render_function":null,"value_field":"root:data:action","ne":false,"operator":"=","value":"''COLLECTION_REQUEST''","action":["Collection Request"],"result":true,"stop":true}]}', false, null, '2023-01-09 10:18:32.044000', '2023-01-25 07:47:33.822000', null, null, 0, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4', null, null, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4');`,
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `INSERT INTO ${config.schema}.tariffplans (id, name, description, tariffs, variables, active, ctime, mtime, rtime, signobject, removed, maker, remover, checker, updater, user_type, category_type, plan_code) VALUES ('${planId}', 'Default Plan', 'A plan from seeds', '{"_arr":["${tariffId}"]}', '{"_arr":[]}', true, '2022-12-19 11:55:02.680000', '2023-01-09 10:18:53.429000', null, null, 0, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4', null, null, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4', null, null, null);`,
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `update ${config.schema}.realms set tariff='${tariffId}' WHERE id='${realmId}'`,
        { transaction: t }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.sequelize.query(
        `DELETE FROM ${config.schema}.tariffplans WHERE id='${planId}';
      DELETE FROM ${config.schema}.tariffs WHERE id='${tariffId}';
      DELETE FROM ${config.schema}.triggers WHERE id='${triggerId}';
      `,
        { transaction: t }
      );
    });
  }
};
