"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { schema: config.schema, tableName: "merchant_categories" },
      [
        {
          id: "603514b8-f865-11ed-b67e-0242ac120002",
          name: "Banjo Pay",
          removed: 0,
          ctime: new Date(),
          mtime: new Date(),
          rtime: new Date()
        }
      ],
      { schema: config.schema }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      { schema: config.schema, tableName: "merchant_categories" },
      null,
      {}
    );
  }
};
