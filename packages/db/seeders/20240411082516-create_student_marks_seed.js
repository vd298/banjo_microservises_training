"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "StudentMarks",
      [
        {
          studentId: 1,
          marks: 80,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          studentId: 1,
          marks: 75,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          studentId: 2,
          marks: 90,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          studentId: 2,
          marks: 85,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        // Add more seed data as needed
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("StudentMarks", null, {});
  },
};
