"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { schema: config.schema, tableName: "currency" },
      [
        {
          code: 978,
          name: "Euro",
          abbr: "EUR",
          fraction_points: 2
        },
        {
          code: 840,
          name: "United States Dollar",
          abbr: "USD",
          fraction_points: 2
        },
        {
          code: 356,
          name: "Indian Rupee",
          abbr: "INR",
          fraction_points: 2
        },
        {
          code: 643,
          name: "Russian Ruble",
          abbr: "RUB",
          fraction_points: 2
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      { schema: config.schema, tableName: "currency" },
      null,
      {}
    );
  }
};
