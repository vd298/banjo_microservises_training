"use strict";
const vw = require("../views/vw_tx_transfer");
const tariffId = "7e8d12c1-a94b-4c17-8263-76b5dccea5fe";
const planId = "7fbf6a37-dfa9-43c5-a6ad-2aff43528c57";
const triggerId = "5d88b2f7-1ac8-4997-a913-c2af25b555cb";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.sequelize.query(
        `INSERT INTO ${config.schema}.triggers (id, ctime, mtime, removed, maker, checker, remover, updater, rtime, signobject, name, service, method, cron, data, ttype, tablename, conditions) VALUES ('${triggerId}', '2023-04-18 17:09:41.954', '2023-04-26 16:36:40.925', 0, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4', NULL, NULL, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4', NULL, NULL, 'SettlementPayoutTrigger', 'transaction-service', 'settlementPayoutTrigger', NULL, '{"trigger":"transaction-service:settlementPayoutTrigger","variables":[],"data":{"brn":"jjl;kl;","transfer_id":"7dd1deeb-c88b-438e-84a7-97c69f2f97a6","action":"APPROVE_TX","description":null,"currency":"EUR","amount":18.783999999999985,"order_id":null,"bank_account_id":"9cf16891-86bf-4eb2-8f84-2125589a2366","wallet_id":"d8c64ff9-6c50-4bb8-b532-671878b4b02a","data":{"note":"","admin_id":"8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4","activity_action":"APPROVED","action_type":"ADMIN"}},"transfers":{"service":"transaction-service","method":"settlementPayoutTrigger","data":{"brn":"jjl;kl;","transfer_id":"7dd1deeb-c88b-438e-84a7-97c69f2f97a6","action":"APPROVE_TX","description":null,"currency":"EUR","amount":18.783999999999985,"order_id":null,"bank_account_id":"9cf16891-86bf-4eb2-8f84-2125589a2366","wallet_id":"d8c64ff9-6c50-4bb8-b532-671878b4b02a","data":{"note":"","admin_id":"8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4","activity_action":"APPROVED","action_type":"ADMIN"}},"list":[],"tags":[],"output":{}},"result":{"dst_wallet_id":"d8c64ff9-6c50-4bb8-b532-671878b4b02a","src_wallet_id":"ed896646-1b7d-45f3-975d-d4730132ef34","dst_currency":"EUR","dst_amount":18.783999999999985,"src_currency":"EUR","src_amount":18.783999999999985,"fee_wallet_id":"6f0a6934-ba21-4334-9f6c-ad516aca1a30"}}'::json, 1, NULL, NULL);`,
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `INSERT INTO ${config.schema}.tariffs (id, name, description, trigger, data, variables, actions, rules, stop_on_rules, pid, ctime, mtime, rtime, signobject, removed, maker, remover, checker, updater) VALUES ('${tariffId}'::uuid, 'Settlement Payout Transaction (Default)', 'Money Payout Transaction', '${triggerId}'::uuid, '{"trigger":"transaction-service:settlementPayoutTrigger","variables":{},"data":{"brn":"qqwewrq","transfer_id":"68c96c43-298c-4d08-aeeb-a5e87ac3f29a","action":"APPROVE_TX","description":null,"currency":"USD","amount":65.98,"order_id":null,"bank_account_id":"5b62036c-e461-4470-82e8-bc23a69ec555","wallet_id":"cb1aa8b0-93c3-41f6-91d6-bb28b14c5315","data":{"note":"","admin_id":"8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4","activity_action":"APPROVED","action_type":"ADMIN"}},"transfers":{"service":"transaction-service","method":"settlementPayoutTrigger","data":{"brn":"qqwewrq","transfer_id":"68c96c43-298c-4d08-aeeb-a5e87ac3f29a","action":"APPROVE_TX","description":null,"currency":"USD","amount":65.98,"order_id":null,"bank_account_id":"5b62036c-e461-4470-82e8-bc23a69ec555","wallet_id":"cb1aa8b0-93c3-41f6-91d6-bb28b14c5315","data":{"note":"","admin_id":"8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4","activity_action":"APPROVED","action_type":"ADMIN"}},"list":{},"tags":{},"output":{}},"result":{"dst_wallet_id":"cb1aa8b0-93c3-41f6-91d6-bb28b14c5315","src_wallet_id":"cb1aa8b0-93c3-41f6-91d6-bb28b14c5315","dst_currency":"USD","dst_amount":65.98,"src_currency":"USD","src_amount":65.98,"fee_wallet_id":"c851cf23-bbd2-4e32-bd41-b987c73b47b2"}}'::json, '{"_arr":[{"id":"extModel1983-1","key":"SETTLEMNT_PAYOUT_CHARGE","value":"2","descript":"Settlement Payout Charges (Percent)"}]}'::json, '{"_arr":[{"type":"transfer_activity","name":"Reject Payout","options":{"txtype":"PAYOUT","txsubtype":"REJECTED","note":"Reject Payout"},"id":"extModel1123-1"},{"type":"transfer_activity","name":"BRN Submitted","options":{"txtype":"PAYOUT","txsubtype":"BRN_SUBMIT","note":"BRN Submitted"},"id":"extModel277-3"},{"type":"transfer","name":"Execute Payout","options":{"txtype":"PAYOUT","txsubtype":"APPROVED","parent_id":"root:data:wallet_id","acc_src":"root:result:src_wallet_id","acc_dst":"root:result:dst_wallet_id","fee":"100","currency":"src","feetype":"PERCENTS","amount_field":"root:data:amount","minimum_amount":"","hold":false,"hidden":false,"description_src":"","description_dst":""},"id":"extModel2495-1"},{"type":"transfer","name":"Fee Charges","options":{"txtype":"PAYOUT","txsubtype":"FEE","parent_id":"root:result:fee_wallet_id","acc_src":"root:result:dst_wallet_id","acc_dst":"root:result:fee_wallet_id","fee":"$SETTLEMNT_PAYOUT_CHARGE","currency":"src","feetype":"PERCENTS","amount_field":"root:data:amount","minimum_amount":"","hold":false,"hidden":false,"description_src":"","description_dst":""},"id":"extModel3145-1"},{"type":"transfer_activity","name":"Payout Request","options":{"txtype":"PAYOUT","txsubtype":"REQUEST","note":"Payout Request"},"id":"extModel1123-2"}]}'::json, '{"_arr":[{"id":"extModel310-6","render_function":null,"value_field":"root:data:action","ne":false,"operator":"=","value":"''SUBMIT_BRN''","action":["BRN Submitted"],"result":true,"stop":true},{"id":"extModel310-5","render_function":null,"value_field":"root:data:action","ne":false,"operator":"=","value":"''REJECT_TX''","action":["Reject Payout"],"result":true,"stop":true},{"id":"extModel528-1","render_function":null,"value_field":"root:data:action","ne":false,"operator":"=","value":"''APPROVE_TX''","action":["Fee Charges"],"result":true,"stop":false},{"id":"extModel310-2","render_function":null,"value_field":"root:data:action","ne":false,"operator":"=","value":"''APPROVE_TX''","action":["Execute Payout"],"result":true,"stop":false},{"id":"extModel310-1","render_function":null,"value_field":"root:data:action","ne":false,"operator":"=","value":"''PAYOUT_REQUEST''","action":["Payout Request"],"result":true,"stop":true}]}'::json, false, NULL, '2023-04-24 14:25:35.367', '2023-06-05 20:15:53.037', '2023-04-24 23:41:45.019', NULL, 0, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4'::uuid, NULL, NULL, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4'::uuid);`,
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `UPDATE ${config.schema}.tariffplans SET tariffs = jsonb_set(tariffs::jsonb,array['_arr'],(tariffs->'_arr')::jsonb || '"7e8d12c1-a94b-4c17-8263-76b5dccea5fe"'::jsonb)
        WHERE id = '${planId}'`,
        { transaction: t }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.sequelize.query(
        `
      DELETE FROM ${config.schema}.tariffs WHERE id='${tariffId}';
      DELETE FROM ${config.schema}.triggers WHERE id='${triggerId}';
      `,
        { transaction: t }
      );
    });
  }
};
