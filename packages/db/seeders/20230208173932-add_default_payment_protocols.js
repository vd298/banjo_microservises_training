"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { schema: config.schema, tableName: "payment_protocols" },
      [
        {
          id: "3ae5f73a-716b-40ce-aaac-939d0a41a415",
          ctime: new Date(),
          mtime: new Date(),
          removed: 0,
          maker: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4",
          protocol_type: "UPI",
          identifier_label: "UPI",
          account_label: "UPI",
          transaction_identifier_label: "Submitted BRN"
        },
        {
          id: "f3f01260-d459-4724-ba1a-ea62f694c2e1",
          ctime: new Date(),
          mtime: new Date(),
          removed: 0,
          maker: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4",
          protocol_type: "RTGS",
          identifier_label: "RTGS",
          account_label: "RTGS",
          transaction_identifier_label: "Submitted BRN"
        },
        {
          id: "94f84239-8cf4-4400-85c0-42d2b60a253d",
          ctime: new Date(),
          mtime: new Date(),
          removed: 0,
          maker: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4",
          protocol_type: "IMPS",
          identifier_label: "IMPS",
          account_label: "IMPS",
          transaction_identifier_label: "Submitted BRN"
        },
        {
          id: "ec9402e2-9153-4761-a3fc-5c92dbbcd2f5",
          ctime: new Date(),
          mtime: new Date(),
          removed: 0,
          maker: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4",
          protocol_type: "NEFT",
          identifier_label: "NEFT",
          account_label: "NEFT",
          transaction_identifier_label: "Submitted BRN"
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    const Op = Sequelize.Op;
    return queryInterface.bulkDelete(
      { schema: config.schema, tableName: "payment_protocols" },
      {
        id: {
          [Op.in]: [
            "3ae5f73a-716b-40ce-aaac-939d0a41a415",
            "f3f01260-d459-4724-ba1a-ea62f694c2e1",
            "94f84239-8cf4-4400-85c0-42d2b60a253d",
            "ec9402e2-9153-4761-a3fc-5c92dbbcd2f5"
          ]
        }
      },
      {}
    );
  }
};
