"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "Students",
      [
        { name: "Vinay", createdAt: new Date(), updatedAt: new Date() },
        { name: "Vaibhav", createdAt: new Date(), updatedAt: new Date() },
        // Add more seed data as needed
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("Students", null, {});
  },
};
