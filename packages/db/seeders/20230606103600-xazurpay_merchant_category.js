"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { schema: config.schema, tableName: "merchant_categories" },
      [
        {
          id: "63170d7e-0456-11ee-be56-0242ac120002",
          name: "XazurPay",
          removed: 0,
          ctime: new Date(),
          mtime: new Date(),
          rtime: new Date()
        }
      ],
      { schema: config.schema }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      { schema: config.schema, tableName: "merchant_categories" },
      null,
      {}
    );
  }
};
