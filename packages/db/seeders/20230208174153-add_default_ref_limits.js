"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { schema: config.schema, tableName: "ref_limits" },
      [
        {
          id: "5a23608f-a9d6-4528-bbe9-2e8cd89e3a7e",
          ctime: new Date(),
          mtime: new Date(),
          removed: 0,
          protocol_id: "3ae5f73a-716b-40ce-aaac-939d0a41a415",
          currency: "INR",
          min_amount: 1,
          max_amount: 100000
        },
        {
          id: "9f4d6b6d-d1c5-472a-bd35-ba38abc7c6a3",
          ctime: new Date(),
          mtime: new Date(),
          removed: 0,
          protocol_id: "ec9402e2-9153-4761-a3fc-5c92dbbcd2f5",
          currency: "INR",
          min_amount: 1,
          max_amount: 200000
        },
        {
          id: "8b25f3a2-b586-487e-a393-fc30d0c460d2",
          ctime: new Date(),
          mtime: new Date(),
          removed: 0,
          protocol_id: "94f84239-8cf4-4400-85c0-42d2b60a253d",
          currency: "INR",
          min_amount: 1,
          max_amount: 200000
        },
        {
          id: "37efabb1-caac-40bb-b155-3512a8467b45",
          ctime: new Date(),
          mtime: new Date(),
          removed: 0,
          protocol_id: "f3f01260-d459-4724-ba1a-ea62f694c2e1",
          currency: "INR",
          min_amount: 200000,
          max_amount: null
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    const Op = Sequelize.Op;
    return queryInterface.bulkDelete(
      { schema: config.schema, tableName: "payment_protocols" },
      {
        id: {
          [Op.in]: [
            "3ae5f73a-716b-40ce-aaac-939d0a41a415",
            "f3f01260-d459-4724-ba1a-ea62f694c2e1",
            "94f84239-8cf4-4400-85c0-42d2b60a253d",
            "ec9402e2-9153-4761-a3fc-5c92dbbcd2f5"
          ]
        }
      },
      {}
    );
  }
};
