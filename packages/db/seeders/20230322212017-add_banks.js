"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { schema: config.schema, tableName: "banks" },
      [
        {
          id: "c769eac0-a1d9-470a-9f55-c03d05a27d40",
          ctime: "2023-03-22T21:19:24.706Z",
          mtime: "2023-03-22T21:19:24.706Z",
          removed: 0,
          maker: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4",
          checker: null,
          remover: null,
          updater: null,
          rtime: null,
          signobject: null,
          short_name: "INDUS",
          name: "INDUS",
          address: "Malkapur",
          city: "Malkapur",
          country: "IN"
        },
        {
          id: "b96dcd7c-0a93-4058-8af8-88752e5e5553",
          ctime: "2023-03-22T21:19:24.701Z",
          mtime: "2023-03-22T21:19:24.701Z",
          removed: 0,
          maker: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4",
          checker: null,
          remover: null,
          updater: null,
          rtime: null,
          signobject: null,
          short_name: "HDFC",
          name: "HDFC",
          address: "Mumbai",
          city: "Mumbai",
          country: "IN"
        },
        {
          id: "64860a1c-888a-4de1-9927-46b5d8b1b96e",
          ctime: "2023-03-22T21:19:24.703Z",
          mtime: "2023-03-22T21:19:24.703Z",
          removed: 0,
          maker: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4",
          checker: null,
          remover: null,
          updater: null,
          rtime: null,
          signobject: null,
          short_name: "ICICI",
          name: "ICICI",
          address: "Mumbai",
          city: "Mumbai",
          country: "IN"
        },
        {
          id: "55a87d9a-0999-4bef-8551-f52902adc0ec",
          ctime: "2023-03-22T21:19:24.704Z",
          mtime: "2023-03-22T21:19:24.704Z",
          removed: 0,
          maker: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4",
          checker: null,
          remover: null,
          updater: null,
          rtime: null,
          signobject: null,
          short_name: "IDFCFirst",
          name: "IDFCFirst",
          address: "Mumbai",
          city: "Mumbai",
          country: "IN"
        }
      ],
      { ignoreDuplicates: true }
    );
  },

  down: (queryInterface, Sequelize) => {
    const Op = Sequelize.Op;
    return queryInterface.bulkDelete(
      { schema: config.schema, tableName: "payment_protocols" },
      {
        id: {
          [Op.in]: [
            "c769eac0-a1d9-470a-9f55-c03d05a27d40",
            "b96dcd7c-0a93-4058-8af8-88752e5e5553",
            "64860a1c-888a-4de1-9927-46b5d8b1b96e",
            "55a87d9a-0999-4bef-8551-f52902adc0ec"
          ]
        }
      },
      {}
    );
  }
};
