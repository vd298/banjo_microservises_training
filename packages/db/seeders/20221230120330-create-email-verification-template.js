"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(
      `INSERT INTO 
        ${
          config.schema
        }.letters (id,ctime,mtime,removed,maker,checker,remover,updater,rtime,signobject,realm,transporter,code,letter_name,from_email,to_email,subject,"text",html,"data",sms_gateway,to_cc,to_bcc,send_email,target_group,send_sms,send_push,to_mobile,sms_text,push_title,push_message,push_action,lang,status,category,sub_category) VALUES
   ('25c6a2b3-ff09-45ea-91e4-a9320c6d98be','2022-11-18 15:52:12.604','2022-11-18 16:03:39.323',0,'8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4',NULL,NULL,'8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4',NULL,NULL,NULL,'0f3af297-8957-4493-bda7-898732ab05ee','1110-EMAIL-CONFIRM','10-Email Confirmation','banjodeve@gmail.com','','Banjo email confirmation Please confirm your email id','','extend base-template
block content
                  tr
                      td.mobile-spacing.mobile-spacing-right(style=''text-align: center;'')
                          p(style=''margin-bottom:24px; margin-top: 0; color: #202e55;     font-weight: bold; text-align: left;  letter-spacing: -0.8px; font-size: 24px;'') Please Confirm Your Registration.
                  tr
                      td.email-title
                          p(style=''font-size: 22px; margin: 0; padding-bottom: 15px; font-weight: bold; text-align: left; line-height: 1.38; letter-spacing: 1.74px; color: #202e55;'')
                              | Hello,
                  tr
                      td
                          p(style=''font-size: 17px; font-weight: normal; text-align: left; margin: 0; line-height: 1.78; color: #202e55;'')
                              | Thank you for registering with Banjo Tech LTD 
                  tr
                      td
                          p(style=''font-size: 17px; font-weight: normal; text-align: left; margin: 0; line-height: 1.78; color: #202e55;'')
                              | To confirm your email address please click the “Confirm Email”  button below..
                  tr
                      td(style=''text-align: center; margin-top: 0; margin-bottom: 0;'')
                          a(target='' _blank'' href=${"`${base_url}auth/restore?code=${restoreCode}`"} style=''font-size: 17px; font-weight: bold;                    color: #ffffff !important; width: 260px; height: 42px; border-radius: 21px; box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16); background-color: #e2001a; border: none; outline: none; cursor: pointer;  display: inline-block; line-height: 42px; text-align: center;'') Confirm Email
                  tr
                      td
                          p(style=''font-size: 16px; font-weight: normal; text-align: left; margin-top: 24px; margin-bottom: 0; line-height: 1.78; color: #202e55;'')
                              |Or You can click the link below to confirm your email.
                              a(target='' _blank''  href=${"`${url}`"} style=''font-size: 17px; line-height: 1.78; text-decoration: none; color: #e2001a;'')  #{base_url}?code=#{body.code}&token=#{body.token}&user=#{body.userId}&confirm=true
                  tr(style=''border-collapse: collapse;'')
                      td
                          p(style=''padding: 0; margin-top: 24px !important; margin-bottom: 24px !important; border-bottom: 1px solid #ebedf1; background: none; height: 1px; width: 100%; margin: 0px;'')
                  tr
                      td
                          p(style=''font-size: 15px; font-weight: normal; text-align: left; margin: 0; line-height: 1.78; color: #202e55; margin-top: 0; margin-bottom: 0;'')
                              | For security reasons, kindly do not forward this email to anyone. It could give access to your bank account. Please ignore the email If you did not try to register with Banjo.','{
                  "lang": "English",
                  "code": "1110",
                  "to": "maximtushev@gmail.com",
                  "body": {
                      "code": "709797",
                      "userId":123,
                      "token":"21asdsdgad",
                     "expiryTime" : "2"
                  }
              }',NULL,'','',true,'CUSTOMER',false,false,NULL,'','','',NULL,'en','CUSTOMER','1000','100');
              `
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(
      `
                 DELETE FROM ${config.schema}.letters
              WHERE id='25c6a2b3-ff09-45ea-91e4-a9320c6d98be'::uuid;  
            `
    );
  }
};
