"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { schema: config.schema, tableName: "merchant_categories" },
      [
        {
          id: "4e5a9386-09df-46fa-8818-e9d25ccdd572",
          name: "DigiPayment",
          removed: 0,
          ctime: new Date(),
          mtime: new Date(),
          rtime: new Date()
        }
      ],
      { schema: config.schema }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      { schema: config.schema, tableName: "merchant_categories" },
      null,
      {}
    );
  }
};
