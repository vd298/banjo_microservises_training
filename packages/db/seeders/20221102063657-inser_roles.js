"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { schema: config.schema, tableName: "roles" },
      [
        {
          id: "cc248393-e01d-4fd4-bea8-a4d8dcb4879d",
          ctime: new Date(),
          mtime: new Date(),
          role_name: "Manager"
        },
        {
          id: "ee6fa088-c5f1-4da6-97a1-1b981cf7cfd5",
          ctime: new Date(),
          mtime: new Date(),
          role_name: "Supervisor"
        },
        {
          id: "55cd0ea9-5dfc-4232-b58c-990a4f134f04",
          ctime: new Date(),
          mtime: new Date(),
          role_name: "Support Executive"
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      { schema: config.schema, tableName: "roles" },
      null,
      {}
    );
  }
};
