"use strict";

const common_fields = require("../config/utility/common_fields");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      `merchant_categories`,
      {
        ...common_fields,
        name: {
          type: Sequelize.STRING(50)
        }
      },
      {
        schema: config.schema
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable({
      schema: config.schema,
      tableName: "merchant_categories"
    });
  }
};
