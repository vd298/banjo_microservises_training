"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "bank_accounts" },
        "is_crypto",
        { type: Sequelize.BOOLEAN, defaultValue: false },
        { transaction: t }
      );
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "bank_accounts" },
        "acc_no",
        {
          type: Sequelize.STRING(100)
        },
        {
          transaction: t
        }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "bank_accounts" },
        "is_crypto",
        { type: Sequelize.BOOLEAN },
        {
          transaction: t
        }
      );
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "bank_accounts" },
        "acc_no",
        {
          type: Sequelize.STRING(40)
        },
        {
          transaction: t
        }
      );
    });
  }
};
