"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.sequelize.query(
        `INSERT INTO ${config.schema}.letters
(id, ctime, mtime, removed, maker, checker, remover, updater, rtime, signobject, realm, transporter, code, letter_name, from_email, to_email, subject, "text", html, "data", sms_gateway, to_cc, to_bcc, send_email, target_group, send_sms, send_push, to_mobile, sms_text, push_title, push_message, push_action, lang, status, category, sub_category)
VALUES('4e412d55-58ef-48be-85c9-b70999bbe1c4'::uuid, '2023-01-14 13:05:59.733', '2023-01-14 13:13:11.152', 0, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4'::uuid, NULL, NULL, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4'::uuid, NULL, NULL, NULL, NULL, 'API-USER-CREDENTIALS-EMAIL', 'api-user-credentials', 'banjodeve@gmail.com', '', 'Banjo API user credentials', '', 'extend base-template
block content
    tr(style=''border-collapse: collapse;'')
      td.w320.mobile-spacing(style=''font-size:18px; font-weight: 500; color: #5E5873; padding: 0 40px 16px;'')
        | Hey 
        strong #{invite_user_name}
        | ,
    tr(style=''border-collapse: collapse;'')
      td(style=''margin: 0; padding: 0 40px 30px;'')
        h3(style=''font-size: 18px;        font-weight: 500; color: #5E5873; margin:0;'')
          | You have been added a new user to Banjo!
    tr(style=''border-collapse: collapse;'')
      td(style=''padding: 0; margin: 0;'')
        img(style=''display: block; border: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;'', width=''100%'', src=''http://polygon.getgps.online:8109/Admin.Data.getFile/?name=invite-user.jpg&tmp=9b50373b-de9a-4447-b2d5-45574e9f49a6'', alt=''invite user'')
    tr(style=''border-collapse: collapse;'')
      td(style=''padding: 30px 40px; margin: 0;'')
        p(style="font-size: 16px;        font-family: ''Montserrat'', sans-serif; margin-bottom: 15px; color: #5E5873;")
          strong Username: 
          | #{username} 
        p(style="font-size: 16px;        font-family: ''Montserrat'', sans-serif; margin-bottom: 15px; color: #5E5873;")
          strong Password:
          |  #{password}
        p(style="font-size: 16px; font-family: ''Montserrat'', sans-serif; margin-bottom: 15px; color: #5E5873;")
          | For the account you created', '{
"username": "Prashin",
"password": "Passw0rd"
}', NULL, '', '', true, 'CUSTOMER':: ${config.schema}."enum_letters_target_group", false, false, NULL, '', '', '', NULL, 'en', 'CUSTOMER':: ${config.schema}."enum_letters_status", '', '');
`
      )
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.sequelize.query(
        ` 
        DELETE FROM  ${config.schema}.letters
        WHERE id='4e412d55-58ef-48be-85c9-b70999bbe1c4'::uuid; 
      `
      )
    ]);
  }
};
