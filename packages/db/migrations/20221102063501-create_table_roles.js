"use strict";

const common_fields = require("../config/utility/common_fields");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      `roles`,
      {
        ...common_fields,
        role_name: {
          type: Sequelize.STRING
        },
        pid: {
          type: Sequelize.UUID
        }
      },
      {
        schema: config.schema
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable({
      schema: config.schema,
      tableName: "roles"
    });
  }
};
