"use strict";

const common_fields = require("../config/utility/common_fields");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      `api_credentials`,
      {
        ...common_fields,
        name: {
          type: Sequelize.STRING(50)
        },
        aggregator_id: {
          type: Sequelize.STRING(50)
        },
        secret_key: {
          type: Sequelize.STRING(250)
        },
        account_id: {
          type: Sequelize.UUID
        },
        merchant_account_id: {
          type: Sequelize.UUID
        },
        category: {
          type: Sequelize.Sequelize.UUID
        },
        bank: {
          type: Sequelize.UUID
        },
        bank_account: {
          type: Sequelize.Sequelize.UUID
        },
        currency: {
          type: Sequelize.STRING(4)
        }
      },
      {
        schema: config.schema
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable({
      schema: config.schema,
      tableName: "api_credentials"
    });
  }
};
