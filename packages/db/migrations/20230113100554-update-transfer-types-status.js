"use strict";
const vw_resources_permissions = require("../views/vw_resources_permissions");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transfers" },
        "account_id",
        { type: Sequelize.UUID },
        { transaction: t }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transfers" },
        "merchant_account_id",
        { type: Sequelize.UUID },
        { transaction: t }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transfers" },
        "order_num",
        { type: Sequelize.STRING(40) },
        { transaction: t }
      );
      await queryInterface.addIndex(
        { schema: config.schema, tableName: "transfers" },
        ["order_num", "merchant_account_id"],
        { unique: true, transaction: t }
      );
      await queryInterface.addIndex(
        { schema: config.schema, tableName: "transfers" },
        ["ref_num"],
        { unique: true, transaction: t }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transfers" },
        "transfer_type",
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        ` drop type ${config.schema}.enum_transfers_transfer_type`,
        {
          transaction: t
        }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transfers" },
        "transfer_type",
        {
          type: Sequelize.ENUM(
            "PAYOUT",
            "PAYIN",
            "COLLECTION",
            "REFUND",
            "RETURN",
            "REVERSE",
            "TRANSFER"
          )
        },
        { transaction: t }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeIndex(
        { schema: config.schema, tableName: "transfers" },
        ["order_num", "merchant_account_id"],
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `drop index ${config.schema}.transfers_ref_num`,
        {
          transaction: t
        }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transfers" },
        "merchant_account_id",
        {
          transaction: t
        }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transfers" },
        "account_id",
        {
          transaction: t
        }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transfers" },
        "order_num",
        {
          transaction: t
        }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transfers" },
        "transfer_type",
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        ` drop type ${config.schema}.enum_transfers_transfer_type`,
        {
          transaction: t
        }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transfers" },
        "transfer_type",
        {
          type: Sequelize.ENUM("PAYOUT", "PAYIN", "TRANSFER")
        },
        { transaction: t }
      );
    });
  }
};
