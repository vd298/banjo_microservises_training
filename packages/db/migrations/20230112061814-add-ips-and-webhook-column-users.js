"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "users" },
        "ips",
        {
          type: Sequelize.JSONB
        },
        {
          transaction: t
        }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "users" },
        "webhook_url",
        {
          type: Sequelize.STRING(255)
        },
        {
          transaction: t
        }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "users" },
        "webhook_url",
        {
          transaction: t
        }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "users" },
        "ips",
        {
          transaction: t
        }
      );
    });
  }
};
