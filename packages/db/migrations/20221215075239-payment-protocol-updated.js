'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      { schema: config.schema, tableName: "payment_protocols" },
      "identifier_code",
      {
        schema: config.schema
      }
    );
    await queryInterface.addColumn(
      { schema: config.schema, tableName: `payment_protocols` },
      "transaction_identifier_label",
      {
        type: Sequelize.Sequelize.STRING(50)
      },
      {
        schema: config.schema
      }
    );
    await queryInterface.addColumn(
      { schema: config.schema, tableName: `payment_protocols` },
      "instruction",
      {
        type: Sequelize.TEXT
      },
      {
        schema: config.schema
      }
    );
  },

  down:async (queryInterface, Sequelize) => {
   
    await queryInterface.addColumn(
      { schema: config.schema, tableName: `payment_protocols` },
      "identifier_code",
      {
        type: Sequelize.STRING(15)
      },
      {
        schema: config.schema
      }
    );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: `payment_protocols` },
        "instruction",
        {
          schema: config.schema
        }
      );     
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: `payment_protocols` },
        "transaction_identifier_label",
        {
          schema: config.schema
        }
      );
    


  }
};
