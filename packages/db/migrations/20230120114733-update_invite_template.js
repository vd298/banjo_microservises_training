"use strict";
const fs = require("fs");
const path = require("path");
var global_data;

async function readFile() {
  global_data = global_data = fs
    .readFileSync(path.join(__dirname, "..", "templates", "invitation.pug"))
    .toString();
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await readFile();
    var template = global_data;
    await queryInterface.sequelize.query(
      ` 
        DELETE FROM ${config.schema}.letters
      WHERE id='8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce5'::uuid;  
      `
    );
    await queryInterface.sequelize.query(
      `
        INSERT INTO ${config.schema}.letters (id, ctime, mtime, removed, maker, checker, remover, updater, rtime, signobject, realm, transporter, code, letter_name, from_email, to_email, subject, "text", html, "data", sms_gateway, to_cc, to_bcc, send_email, target_group, send_sms, send_push, to_mobile, sms_text, push_title, push_message, push_action, lang, status, category, sub_category) VALUES('8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce5', '2022-12-08 12:20:08.168', '2022-12-08 12:21:20.363', 0, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4', NULL, NULL, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4', NULL, NULL, NULL, NULL, 'INVITE-EMAIL', 'Invite email', 'banjodeve@gmail.com', '', 'Invite User ', '', '${template}','{
   "invite_user_name": "Dipak Gaware",
   "token":"3e6e55e8-af36-4572-9c79-1befe1c2f0fa",
   "email": "pm261@enovate-it.com",
   "base_url": "http://localhost:8080/"
}', NULL, '', '', true, 'CUSTOMER', false, false, NULL, '', '', '', NULL, 'en', 'CUSTOMER', '1000', '100');;
        
        `
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(
      ` 
        DELETE FROM ${config.schema}.letters
      WHERE id='8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce5'::uuid;  
      `
    );
  }
};
