"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "payment_providers",
      {
        ...common_fields,
        name: {
          type: Sequelize.STRING(50)
        },
        status: {
          type: Sequelize.ENUM("ACTIVE", "INACTIVE")
        }
      },
      {
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "payment_providers"
    });
  }
};