"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "files",
      {
        ...common_fields,
        owner: {
          type: Sequelize.UUID
        },
        code: {
          type: Sequelize.UUID
        },
        filename: {
          type: Sequelize.STRING
        },
        file_size: {
          type: Sequelize.INTEGER
        },
        mime_type: {
          type: Sequelize.STRING
        },
        upload_date: {
          type: Sequelize.DATE
        },
        storage_date: {
          type: Sequelize.DATE
        },
        ctime: {
          allowNull: false,
          type: Sequelize.DATE
        },
        mtime: {
          allowNull: false,
          type: Sequelize.DATE
        },
        removed: {
          type: Sequelize.INTEGER,
          defaultValue: 0
        }
      },
      {
        logger: console.log,
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "files"
    });
  }
};
