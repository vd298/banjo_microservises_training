"use strict";

const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      `permissions`,
      {
        ...common_fields,
        resource_id: {
          type: Sequelize.UUID,
          allowNull: false
        },
        role_id: {
          type: Sequelize.UUID
        },
        user_id: {
          type: Sequelize.UUID
        },
        allow: {
          type: Sequelize.BOOLEAN
        },
        deny: {
          type: Sequelize.BOOLEAN
        },
        write: {
          type: Sequelize.BOOLEAN
        }
      },
      {
        logger: console.log,
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "permissions"
    });
  }
};
