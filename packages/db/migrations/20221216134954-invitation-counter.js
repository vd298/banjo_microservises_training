"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      { schema: config.schema, tableName: "user_account_invitations" },
      "counter",
      { type: Sequelize.INTEGER, defaultValue: 0 }
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      { schema: config.schema, tableName: "user_account_invitations" },
      "counter"
    );
  }
};
