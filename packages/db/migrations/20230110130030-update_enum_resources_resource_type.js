"use strict";
const vw_resources_permissions = require("../views/vw_resources_permissions");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "resources" },
        "object_type",
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        ` drop type ${config.schema}.enum_resources_object_type`,
        {
          transaction: t
        }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "resources" },
        "object_type",
        {
          type: Sequelize.ENUM(
            "BANK",
            "BANK_ACCOUNT",
            "USER",
            "ACCOUNT",
            "MERCHANT_ACCOUNT"
          )
        },
        { transaction: t }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "resources" },
        "object_type",
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        ` drop type ${config.schema}.enum_resources_object_type`,
        {
          transaction: t
        }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "resources" },
        "object_type",
        {
          type: Sequelize.ENUM("BANK", "BANK ACCOUNT", "USER", "ACCOUNT")
        },
        { transaction: t }
      );
    });
  }
};
