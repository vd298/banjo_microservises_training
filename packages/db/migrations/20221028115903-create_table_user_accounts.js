"use strict";

const common_fields = require("../config/utility/common_fields");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      `user_accounts`,
      {
        ...common_fields,
        account_id: {
          type: Sequelize.UUID
        },
        user_id: {
          type: Sequelize.UUID
        },
        main_role: {
          type: Sequelize.UUID
        },
        additional_roles: {
          type: Sequelize.JSON
        },
        start_date: { type: Sequelize.DATE },
        end_date: { type: Sequelize.DATE }
      },
      {
        schema: config.schema
      }
    );
    await queryInterface.addIndex(
      `${config.schema}.user_accounts`,
      ["account_id", "user_id", "rtime", "removed"],
      {
        unique: true
      },
      { schema: config.schema }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable({
      schema: config.schema,
      tableName: "user_accounts"
    });
  }
};
