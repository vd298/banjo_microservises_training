"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "bank_config",
      {
        ...common_fields,
        domain_name: {
          type: Sequelize.STRING(100),
          allowNull: false
        },
        bank_id: {
          type: Sequelize.UUID,
          allowNull: false
        },
        brn_regex: {
          type: Sequelize.STRING(500),
          allowNull: false
        }
      },
      {
        schema: config.schema
      }
    );
    await queryInterface.addIndex(
      `${config.schema}.bank_config`,
      ["bank_id"],
      {},
      { schema: config.schema }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "bank_config"
    });
  }
};
