"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transfers" },
        "ext_user_id",
        {
          type: Sequelize.STRING(255),

          allowNull: true
        },
        { transaction: t }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transfers" },
        "ext_user_id",
        {
          transaction: t
        }
      );
    });
  }
};
