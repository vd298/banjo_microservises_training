"use strict";
const extendPermissions = require("../config/default_merchant_realm_permissions");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return extendPermissions(queryInterface);
  },

  down: (queryInterface, Sequelize) => {
    return extendPermissions(queryInterface);
  }
};
