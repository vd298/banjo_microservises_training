const dependedView = require("../views/vw_user_accounts");
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "users" },
        "password",
        {
          type: Sequelize.STRING(150)
        },
        { transaction: t }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "users" },
        "password",
        {
          type: Sequelize.STRING(100)
        },
        { transaction: t }
      );
    });
  }
};
