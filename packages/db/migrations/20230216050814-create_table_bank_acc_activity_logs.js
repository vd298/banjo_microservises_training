"use strict";

const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      `bank_account_activity_logs`,
      {
        ...common_fields,
        bank_account_id: {
          type: Sequelize.UUID,
          allowNull: false
        },
        note: {
          type: Sequelize.STRING(255)
        },
        old_status: {
          type: Sequelize.ENUM("ACTIVE", "INACTIVE"),
          allowNull: false
        },
        new_status: {
          type: Sequelize.ENUM("ACTIVE", "INACTIVE"),
          allowNull: false
        }
      },
      {
        logger: console.log,
        schema: config.schema
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable({
      schema: config.schema,
      tableName: "bank_account_activity_logs"
    });
    await queryInterface.sequelize.query(
      `drop type if exists ${config.schema}.enum_bank_account_activity_logs_old_status`
    );
    await queryInterface.sequelize.query(
      `drop type if exists ${config.schema}.enum_bank_account_activity_logs_new_status`
    );
  }
};
