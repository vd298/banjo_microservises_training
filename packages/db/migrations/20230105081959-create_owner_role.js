"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      let ownerId, ownerResp = await queryInterface.sequelize.query(
        ` 
        DELETE FROM ${config.schema}.roles
      WHERE role_name='Owner' returning id;  
      `, {
        raw: true,
        type: Sequelize.QueryTypes.SELECT,
        transaction: t
      }
      );
      if (ownerResp && ownerResp.length) {
        ownerId = ownerResp[0].id;
      }
      await queryInterface.sequelize.query(
        `INSERT INTO ${config.schema}.roles(id, ctime, mtime, removed, maker, checker, remover, updater, rtime, signobject, role_name, pid) VALUES
        ('8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4', '2022-12-29 19:13:51.379', '2022-12-29 19:13:51.379', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'Owner', NULL); `,
        {
          raw: true,
          transaction: t
        }
      );
      if (ownerId) {
        await queryInterface.sequelize.query(
          `update ${config.schema}.user_account_invitations set role = '8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4' where role = $1`,
          {
            bind: [ownerId],
            raw: true,
            transaction: t
          }
        );
        await queryInterface.sequelize.query(
          `update ${config.schema}.user_accounts set main_role='8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4' where main_role=$1`,
          {
            bind: [ownerId],
            raw: true,
            transaction: t
          }
        );
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(
      ` 
        DELETE FROM ${config.schema}.roles WHERE id = $1
  `,
      {
        bind: ['8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4'],
        raw: true
      })
  }
};
