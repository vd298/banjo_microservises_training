"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { schema: config.schema, tableName: "realms" },
      [
        {
          id: "046cce25-f407-45c7-8be9-3bf198093408",
          name: "Banjo Default",
          token:
            "0490a107d82aae5c559d18593db181e51707c849a766cebffa0346e0671c625e",
          ctime: new Date(),
          mtime: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      { schema: config.schema, tableName: "realms" },
      null,
      {}
    );
  }
};
