"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "ref_limits" },
        "daily_deposit",
        {
          type: Sequelize.DOUBLE,
          allowNull: true,
          defaultValue: 0
        },
        {
          transaction: t
        }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "ref_limits" },
        "daily_deposit",
        {
          transaction: t
        }
      );
    });
  }
};
