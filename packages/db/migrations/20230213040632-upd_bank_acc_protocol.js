"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "bank_accounts_protocols" },
        "identifier",
        {
          type: Sequelize.STRING(300)
        },
        {
          transaction: t
        }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "bank_accounts_protocols" },
        "identifier",
        {
          transaction: t
        }
      );
    });
  }
};
