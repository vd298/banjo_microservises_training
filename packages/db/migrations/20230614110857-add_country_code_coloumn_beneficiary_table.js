"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "beneficiaries" },
        "country_code",
        { type: Sequelize.STRING(10) },
        { transaction: t }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "beneficiaries" },
        "country_code",
        { type: Sequelize.STRING(10) },
        {
          transaction: t
        }
      );
    });
  }
};
