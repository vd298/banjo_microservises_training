"use strict";
let restoreCode = "${restoreCode}";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.sequelize.query(
        `UPDATE ${config.schema}.letters
SET html='extend base-template
block content
    tr(style=''border-collapse: collapse;'')
      td.w320.mobile-spacing(style=''font-size:18px; font-weight: 500; color: #5E5873; padding: 0 40px 16px;'')
        | Hey 
        strong #{invite_user_name}
        | ,
    tr(style=''border-collapse: collapse;'')
      td(style=''margin: 0; padding: 0 40px 30px;'')
        h3(style=''font-size: 18px;        font-weight: 500; color: #5E5873; margin:0;'')
          | You have been invited to Banjo!
    tr(style=''border-collapse: collapse;'')
      td(style=''padding: 0; margin: 0;'')
        img(style=''display: block; border: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;'', width=''100%'', src="http://banjo:8109/Admin.Data.getFile/?name=invite-user.jpg&tmp=9b50373b-de9a-4447-b2d5-45574e9f49a6", alt="invite user")
    tr(style=''border-collapse: collapse;'')
      td(style=''padding: 30px 40px; margin: 0;'')
        p(style="font-size: 16px;        font-family: ''Montserrat'', sans-serif; margin-bottom: 15px; color: #5E5873;")
          | John with enovate-it pvt ltd has invited you to use banjo to collaborate with them. Use the button below to set up your account and get started:
        p(style="font-size: 16px; font-family: ''Montserrat'', sans-serif; margin-bottom: 15px; color: #5E5873;")
          | If you did not sign up for ResourceRoot, please ignore this email or contact us at 
          a(style=''color:#7367F0; text-decoration:none;'', href="banjo.com", target="_blank") support@banjo.com
        p(style="font-size: 16px; font-family: ''Montserrat'', sans-serif; margin-bottom: 0; color: #5E5873;")
          | Verification Link: 
          a(style=''color:#7367F0; text-decoration:none;'', href="http://localhost:8080/welcome-user/#{token}", target="_blank") www.localhost:8080/welcome-user/#{token}  

    td(style=''padding: 0 40px 30px;'')
        a(href=''banjo.com'', target="_blank", style=''font-size: 16px; font-weight: 500;        color: #ffffff; background: #7367F0;border-radius: 5px; border: none; outline: none; cursor: pointer;  display: inline-block; padding: 10px 35px; text-align: center;'') Sign up
',"data"='
{
   "invite_user_name": "Dipak Gaware",
   "token":"3e6e55e8-af36-4572-9c79-1befe1c2f0fa"
}'
WHERE id='8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce5'::uuid;
`
      ),
      queryInterface.sequelize.query(
        `UPDATE ${config.schema}.letters
SET html='doctype transitional
head
  meta(http-equiv=''Content-Type'' content=''text/html; charset=utf-8'')
  meta(name=''viewport'' content=''width=device-width, initial-scale=1'')
  meta(name=''color-scheme'' content=''light dark'')
  meta(name=''supported-color-schemes'' content=''light dark'')
  link(rel=''stylesheet'' href=''https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'')
  link(href=''http://db.onlinewebfonts.com/c/03e852a9d1635cf25800b41001ee80c7?family=Trebuchet+MS'' rel=''stylesheet'' type=''text/css'')
  title Confirm your registration
  style(type=''text/css'').
    body {
    -webkit-font-smoothing:antialiased;
    -webkit-text-size-adjust:none;
    width: 100%;
    height: 100%;
    color: #202e55;
    font-weight: 400;
    font-size: 18px;
    }
    h1 {
    margin: 15px 0;
    font-weight: 900;
    font-size: 28px;
    line-height: 1.38;
    letter-spacing: 5.8px;
    }
    a {
    font-size: 16px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.13;
    letter-spacing: normal;
    text-align: left;
    color: #15c !important;
    text-decoration: none;
    min-width: 102px;
    display: inline-block;
    }
    a.soical-icon {
    min-width: 75px;
    }
    .force-full-width {
    width: 100% !important;
    }
    .force-width-80 {
    width: 80% !important;
    }
    .body-padding {
    padding: 0 75px;
    }
    .mobile-align {
    text-align: right;
    }
    @media (prefers-color-scheme: dark) {
    body {
    background-color: #F8F9FA !important;
    background-image: linear-gradient(#F8F9FA ,#F8F9FA) !important;
    }
    .dark {
    background-color: #F8F9FA !important;
    background-image: none !important;
    }
    h1, h2, p, span, a, b { color: #ffffff !important; }
    .link { color: #91ADD4 !important; }
    }
    @media (prefers-color-scheme: light) {
    body {
    background-color: #F8F9FA !important;
    background-image: radial-gradient(rgba(255, 255, 255, 0.32), #F8F9FA) !important;
    }
    .dark {
    background-color: #F8F9FA !important;
    background-image: radial-gradient(rgba(255, 255, 255, 0.32), #F8F9FA) !important;
    }
    }
  style(type=''text/css'' media=''screen'').
    @media screen {
    @import url(''https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;0,900;1,300;1,400;1,500;1,700&display=swap'');
    @import url(''http://db.onlinewebfonts.com/c/03e852a9d1635cf25800b41001ee80c7?family=Trebuchet+MS'');
    * {
    font-family: ''Trebuchet+MS'', sans-serif !important;
    }
    .w280 {
    width: 280px !important;
    }
    }
  style(type=''text/css'' media=''only screen and (max-width: 480px)'').
    /* Mobile styles */
    @media only screen and (max-width: 480px) {
    table[class*="w320"] {
    width: 320px !important; min-width: 600px;
    }
    td[class*="w320"] {
    width: 280px !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
    }
    img[class*="w320"] {
    width: 120px !important;
    }
    td[class*="mobile-spacing"] {
    padding-top: 10px !important;
    padding-bottom: 10px !important;
    }
    *[class*="mobile-hide"] {
    display: none !important;
    }
    *[class*="mobile-br"] {
    font-size: 12px !important;
    }
    td[class*="mobile-w20"] {
    width: 20px !important;
    }
    img[class*="mobile-w20"] {
    width: 20px !important;
    }
    td[class*="mobile-center"] {
    text-align: center !important;
    }
    table[class*="w100p"] {
    width: 100% !important;
    }
    td[class*="activate-now"] {
    padding-right: 0 !important;
    padding-top: 20px !important;
    }
    td[class*="mobile-block"] {
    display: block !important;
    }
    td[class*="mobile-align"] {
    text-align: left !important;
    }
    table[class*="force-full-width"] {
    padding: 16px !important;
    }
    tr[class*="shadow-effect"] {
    display: none;
    }
    a {
    font-size: 14px;
    }
    a.soical-icon {
    padding: 0 10px;
    }
    .text-resize {
    font-size: 21px !important;
    letter-spacing: 2.8px !important;
    }
    .email-title h1 {
    font-size: 24px !important;
    letter-spacing: 1.8px !important;
    }
    a.soical-icon {
    min-width: auto;
    padding: 0 10px;
    }
    a.soical-icon img {
    height: 24px !important;
    }
    .link-list a {
    padding: 0 10px !important;
    }
    body {
    background-color: #F8F9FA !important;
    background-image: linear-gradient(#F8F9FA,#F8F9FA) !important;
    }
    .dark {
    background-color: #F8F9FA !important;
    background-image: linear-gradient(#F8F9FA,#F8F9FA) !important;
    }
    table[class*="body-background"] {
    background-size: contain;
    }
    td[class*="mobile-spacing-right"] {
    padding-right: 30px !important;
    }
    table[class*="footer-bg"] {
    background-size: cover !important;
    background-position: center bottom !important;
    }
    p[class*="full-width"] {
    width: 100% !important;
    }
    }
table(align=''center'' cellpadding=''0'' cellspacing=''0'' width=''100%'')
  tr
    td.dark(align=''center'' valign=''top'' bgcolor=''#F8F9FA'' style=''background-color:#F8F9FA;background-image: radial-gradient(rgba(255, 255, 255, 0.32), #F8F9FA); padding-bottom: 15px;'' width=''100%'')
      center
        table.w320(style=''z-index: 10; position: relative;'' cellspacing=''0'' cellpadding=''0'' width=''600'')
          tr
            td(align=''center'' valign=''top'')
              table(style=''margin:0 auto;'' cellspacing=''0'' cellpadding=''0'' width=''100%'')
                tr
                  td(style=''text-align:center;padding-top: 50px; padding-bottom: 0; margin: 0;'')
                    a(style=''text-decoration: none; margin-bottom: 10px;'' href=''#'')
                      img(width=''100%''  src=''http://banjo.com/banjo-logo.png'', alt=''Banjo logo'')
              table.force-full-width.body-background(cellspacing=''0'' cellpadding=''0'' style="padding: 32px; background-color:#ffffff; background-repeat: no-repeat;")
                 block content
              table.force-full-width.footer-bg(cellspacing=''0'' cellpadding=''0'' bgcolor=''#ffffff'' style="margin: 0; background-color: #fff; width: 100%; padding: 10px 32px;")

                tr
                   td(style=''padding: 0 0px 30px; margin: 0;  width: 100%;'')
                      hr(style=''border: 1px solid #EBE9F1; background: none;'')
                tr(style=''border-collapse: collapse;'')
                  td(style=''padding: 0 40px; margin: 0;'')

                tr(style=''border-collapse: collapse;'')
                   td      
                        p(style="font-size: 16px; margin: 0;font-family: ''Montserrat'', sans-serif; margin-bottom: 15px;  color: #5E5873;")
                          | Not sure why you received this email? 
                          a(style=''color:#7367F0; text-decoration:none;'', href=\''\${app_url}\''\, target=''_blank'') Please let us know.         



               
                tr
                   td.mobile-spacing.mobile-spacing-right(style=''text-align: center; padding-top: 70px;'')
                     p(style=''margin-bottom:8px; margin-top: 0; color: #202e55;     font-weight: normal; text-align: left;  font-size: 18px;'') Thanks,
                tr
                   td.mobile-spacing.mobile-spacing-right(style=''text-align: center;'')
                     p(style=''margin-bottom:21px; margin-top: 0; color: #202e55;     font-weight: normal; text-align: left;   font-size: 18px;'') The Banjo Team<br><br>
   
              table(style=''padding-bottom:50px;'')                
                tr(style=''border-collapse: collapse;'')
                  td(style=''text-align: center; color: #c9cdd6;'')
',"data"='{
"image_url":"http://polygon.getgps.online:8102/images/",
"app_url":"http://polygon.getgps.online:8102/"
}'
WHERE id='be066e8d-2db7-4490-80d8-3375341504b9'::uuid;
`
      )
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.sequelize.query(
        `UPDATE ${config.schema}.letters
  SET html='extend base-template
  block content
      tr(style=''border-collapse: collapse;'')
        td.w320.mobile-spacing(style=''font-size:18px; font-weight: 500; color: #5E5873; padding: 0 40px 16px;'')
          | Hey
          strong #{invite_user_name}
          | ,
      tr(style=''border-collapse: collapse;'')
        td(style=''margin: 0; padding: 0 40px 30px;'')
          h3(style=''font-size: 18px;        font-weight: 500; color: #5E5873; margin:0;'')
            | You have been invited to Banjo!
      tr(style=''border-collapse: collapse;'')
        td(style=''padding: 0; margin: 0;'')
          img(style=''display: block; border: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;'', width=''100%'', src="http://polygon.getgps.online:8109/Admin.Data.getFile/?name=invite-user.jpg&tmp=9b50373b-de9a-4447-b2d5-45574e9f49a6", alt="invite user")
      tr(style=''border-collapse: collapse;'')
        td(style=''padding: 30px 40px; margin: 0;'')
          p(style="font-size: 16px;        font-family: ''Montserrat'', sans-serif; margin-bottom: 15px; color: #5E5873;")
            | John with enovate-it pvt ltd has invited you to use banjo to collaborate with them. Use the button below to set up your account and get started:
          p(style="font-size: 16px; font-family: ''Montserrat'', sans-serif; margin-bottom: 15px; color: #5E5873;")
            | If you did not sign up for ResourceRoot, please ignore this email or contact us at
            a(style=''color:#7367F0; text-decoration:none;'', href="polygon.com", target="_blank") support@polygon.com
          p(style="font-size: 16px; font-family: ''Montserrat'', sans-serif; margin-bottom: 0; color: #5E5873;")
            | Verification Link:
            a(style=''color:#7367F0; text-decoration:none;'', href="http://localhost:8080/welcome-user/#{token}", target="_blank") www.localhost:8080/welcome-user/#{token}

      td(style=''padding: 0 40px 30px;'')
          a(href="polygon.com", target="_blank", style=''font-size: 16px; font-weight: 500;        color: #ffffff; background: #7367F0;border-radius: 5px; border: none; outline: none; cursor: pointer;  display: inline-block; padding: 10px 35px; text-align: center;'') Sign up
  ', "data"='{
     "invite_user_name": "Dipak Gaware",
     "token":"3e6e55e8-af36-4572-9c79-1befe1c2f0fa"
  }'
  WHERE id='8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce5'::uuid;
  `
      ),
      queryInterface.sequelize.query(
        `UPDATE ${config.schema}.letters
  SET html='doctype transitional
head
  meta(http-equiv=''Content-Type'' content=''text/html; charset=utf-8'')
  meta(name=''viewport'' content=''width=device-width, initial-scale=1'')
  meta(name=''color-scheme'' content=''light dark'')
  meta(name=''supported-color-schemes'' content=''light dark'')
  link(rel=''stylesheet'' href=''https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'')
  link(href=''http://db.onlinewebfonts.com/c/03e852a9d1635cf25800b41001ee80c7?family=Trebuchet+MS'' rel=''stylesheet'' type=''text/css'')
  title Confirm your registration
  style(type=''text/css'').
    body {
    -webkit-font-smoothing:antialiased;
    -webkit-text-size-adjust:none;
    width: 100%;
    height: 100%;
    color: #202e55;
    font-weight: 400;
    font-size: 18px;
    }
    h1 {
    margin: 15px 0;
    font-weight: 900;
    font-size: 28px;
    line-height: 1.38;
    letter-spacing: 5.8px;
    }
    a {
    font-size: 16px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.13;
    letter-spacing: normal;
    text-align: left;
    color: #15c !important;
    text-decoration: none;
    min-width: 102px;
    display: inline-block;
    }
    a.soical-icon {
    min-width: 75px;
    }
    .force-full-width {
    width: 100% !important;
    }
    .force-width-80 {
    width: 80% !important;
    }
    .body-padding {
    padding: 0 75px;
    }
    .mobile-align {
    text-align: right;
    }
    @media (prefers-color-scheme: dark) {
    body {
    background-color: #F8F9FA !important;
    background-image: linear-gradient(#F8F9FA ,#F8F9FA) !important;
    }
    .dark {
    background-color: #F8F9FA !important;
    background-image: none !important;
    }
    h1, h2, p, span, a, b { color: #ffffff !important; }
    .link { color: #91ADD4 !important; }
    }
    @media (prefers-color-scheme: light) {
    body {
    background-color: #F8F9FA !important;
    background-image: radial-gradient(rgba(255, 255, 255, 0.32), #F8F9FA) !important;
    }
    .dark {
    background-color: #F8F9FA !important;
    background-image: radial-gradient(rgba(255, 255, 255, 0.32), #F8F9FA) !important;
    }
    }
  style(type=''text/css'' media=''screen'').
    @media screen {
    @import url(''https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;0,900;1,300;1,400;1,500;1,700&display=swap'');
    @import url(''http://db.onlinewebfonts.com/c/03e852a9d1635cf25800b41001ee80c7?family=Trebuchet+MS'');
    * {
    font-family: ''Trebuchet+MS'', sans-serif !important;
    }
    .w280 {
    width: 280px !important;
    }
    }
  style(type=''text/css'' media=''only screen and (max-width: 480px)'').
    /* Mobile styles */
    @media only screen and (max-width: 480px) {
    table[class*="w320"] {
    width: 320px !important; min-width: 600px;
    }
    td[class*="w320"] {
    width: 280px !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
    }
    img[class*="w320"] {
    width: 120px !important;
    }
    td[class*="mobile-spacing"] {
    padding-top: 10px !important;
    padding-bottom: 10px !important;
    }
    *[class*="mobile-hide"] {
    display: none !important;
    }
    *[class*="mobile-br"] {
    font-size: 12px !important;
    }
    td[class*="mobile-w20"] {
    width: 20px !important;
    }
    img[class*="mobile-w20"] {
    width: 20px !important;
    }
    td[class*="mobile-center"] {
    text-align: center !important;
    }
    table[class*="w100p"] {
    width: 100% !important;
    }
    td[class*="activate-now"] {
    padding-right: 0 !important;
    padding-top: 20px !important;
    }
    td[class*="mobile-block"] {
    display: block !important;
    }
    td[class*="mobile-align"] {
    text-align: left !important;
    }
    table[class*="force-full-width"] {
    padding: 16px !important;
    }
    tr[class*="shadow-effect"] {
    display: none;
    }
    a {
    font-size: 14px;
    }
    a.soical-icon {
    padding: 0 10px;
    }
    .text-resize {
    font-size: 21px !important;
    letter-spacing: 2.8px !important;
    }
    .email-title h1 {
    font-size: 24px !important;
    letter-spacing: 1.8px !important;
    }
    a.soical-icon {
    min-width: auto;
    padding: 0 10px;
    }
    a.soical-icon img {
    height: 24px !important;
    }
    .link-list a {
    padding: 0 10px !important;
    }
    body {
    background-color: #F8F9FA !important;
    background-image: linear-gradient(#F8F9FA,#F8F9FA) !important;
    }
    .dark {
    background-color: #F8F9FA !important;
    background-image: linear-gradient(#F8F9FA,#F8F9FA) !important;
    }
    table[class*="body-background"] {
    background-size: contain;
    }
    td[class*="mobile-spacing-right"] {
    padding-right: 30px !important;
    }
    table[class*="footer-bg"] {
    background-size: cover !important;
    background-position: center bottom !important;
    }
    p[class*="full-width"] {
    width: 100% !important;
    }
    }
table(align=''center'' cellpadding=''0'' cellspacing=''0'' width=''100%'')
  tr
    td.dark(align=''center'' valign=''top'' bgcolor=''#F8F9FA'' style=''background-color:#F8F9FA;background-image: radial-gradient(rgba(255, 255, 255, 0.32), #F8F9FA); padding-bottom: 15px;'' width=''100%'')
      center
        table.w320(style=''z-index: 10; position: relative;'' cellspacing=''0'' cellpadding=''0'' width=''600'')
          tr
            td(align=''center'' valign=''top'')
              table(style=''margin:0 auto;'' cellspacing=''0'' cellpadding=''0'' width=''100%'')
                tr
                  td(style=''text-align:center;padding-top: 50px; padding-bottom: 0; margin: 0;'')
                    a(style=''text-decoration: none; margin-bottom: 10px;'' href=''#'')
                      img(width=''100%''  src=''http://polygon.getgps.online:8102/images/polygon-logo.png'', alt=''Polygon logo'')
              table.force-full-width.body-background(cellspacing=''0'' cellpadding=''0'' style="padding: 32px; background-color:#ffffff; background-repeat: no-repeat;")
                 block content
              table.force-full-width.footer-bg(cellspacing=''0'' cellpadding=''0'' bgcolor=''#ffffff'' style="margin: 0; background-color: #fff; width: 100%; padding: 10px 32px;")

                tr
                   td(style=''padding: 0 0px 30px; margin: 0;  width: 100%;'')
                      hr(style=''border: 1px solid #EBE9F1; background: none;'')
                tr(style=''border-collapse: collapse;'')
                  td(style=''padding: 0 40px; margin: 0;'')

                tr(style=''border-collapse: collapse;'')
                   td      
                        p(style="font-size: 16px; margin: 0;font-family: ''Montserrat'', sans-serif; margin-bottom: 15px;  color: #5E5873;")
                          | Not sure why you received this email? 
                          a(style=''color:#7367F0; text-decoration:none;'', href=\`\${app_url}\`\, target=''_blank'') Please let us know.         



               
                tr
                   td.mobile-spacing.mobile-spacing-right(style=''text-align: center; padding-top: 70px;'')
                     p(style=''margin-bottom:8px; margin-top: 0; color: #202e55;     font-weight: normal; text-align: left;  font-size: 18px;'') Thanks,
                tr
                   td.mobile-spacing.mobile-spacing-right(style=''text-align: center;'')
                     p(style=''margin-bottom:21px; margin-top: 0; color: #202e55;     font-weight: normal; text-align: left;   font-size: 18px;'') The Polygon Team<br><br>
   
              table(style=''padding-bottom:50px;'')                
                tr(style=''border-collapse: collapse;'')
                  td(style=''text-align: center; color: #c9cdd6;'')
  ', "data"='{
"image_url":"http://polygon.getgps.online:8102/images/",
"app_url":"http://polygon.getgps.online:8102/"
}'
  WHERE id='be066e8d-2db7-4490-80d8-3375341504b9'::uuid;
  `
      )
    ]);
  }
};
