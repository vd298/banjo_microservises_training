"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "ref_limits",
      {
        ...common_fields,
        protocol_id: {
          type: Sequelize.UUID,
          allowNull: false,
          defaultValue: Sequelize.DataTypes.UUIDV4
        },
        currency: {
          type: Sequelize.STRING(4),
          allowNull: false
        },
        min_amount: {
          type: Sequelize.DOUBLE
        },
        max_amount: {
          type: Sequelize.DOUBLE
        },
        bank_account_protocol_id: {
          type: Sequelize.UUID,
          allowNull: true
        }
      },
      {
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "ref_limits"
    });
  }
};
