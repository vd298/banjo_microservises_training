"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "settlements",
      {
        src_currency: {
          type: Sequelize.STRING(4),
          allowNull: false
        },
        dst_currency: {
          type: Sequelize.STRING(4),
          allowNull: false
        },
        src_amount: {
          type: Sequelize.DOUBLE,
          allowNull: false
        },
        dst_amount: {
          type: Sequelize.DOUBLE,
          allowNull: false
        },
        markup_rate: {
          type: Sequelize.DOUBLE
        },
        conversion_rate: {
          type: Sequelize.DOUBLE
        },
        status: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: 2 // Status 2 means in progress
        },
        merchant_account_id: {
          type: Sequelize.UUID,
          allowNull: false
        },
        ...common_fields
      },
      {
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "settlements"
    });
  }
};
