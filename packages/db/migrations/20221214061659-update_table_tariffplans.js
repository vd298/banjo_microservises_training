"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "tariffplans" },
        "user_type",
        {
          type: Sequelize.UUID
        },
        { transaction: t }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "tariffplans" },
        "category_type",
        {
          type: Sequelize.UUID
        },
        { transaction: t }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "tariffplans" },
        "plan_code",
        {
          type: Sequelize.STRING(5)
        },
        { transaction: t }
      );
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "tariffplans" },
        "name",
        {
          type: Sequelize.STRING(60)
        },
        { transaction: t }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "tariffplans" },
        "user_type",
        {
          transaction: t
        }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "tariffplans" },
        "category_type",
        {
          transaction: t
        }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "tariffplans" },
        "plan_code",
        {
          transaction: t
        }
      );
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "tariffplans" },
        "name",
        {
          type: Sequelize.STRING(40)
        },
        { transaction: t }
      );
    });
  }
};
