"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "merchant_accounts" },
        "auto_payouts",
        { type: Sequelize.INTEGER, defaultValue: 0 },
        { transaction: t }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "merchant_accounts" },
        "auto_payouts",
        { type: Sequelize.INTEGER },
        {
          transaction: t
        }
      );
    });
  }
};
