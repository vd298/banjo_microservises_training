"use strict";
const vw = require("../views/vw_tx_transfer");
const planId = "7fbf6a37-dfa9-43c5-a6ad-2aff43528c57";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.sequelize.query(
        `INSERT INTO ${config.schema}."groups"
("_id", "name", code, description, desktopclasname, autorun, modelaccess, reportsaccess, pagesaccess, apiaccess, ctime, mtime, rtime, signobject, removed, maker, remover, checker, updater)
VALUES('e4ede5d1-bf57-4b37-a625-6b4534ecf656'::uuid, 'users', 'users', '', NULL, '', '{"Crm-modules-banks-model-IBANModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-support-model-SupportModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-accounts-model-AccountsModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-currency-model-CurrencyModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-userDocs-model-userDocsModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-userType-model-UserTypeCombo": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-settings-model-CountriesModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-merchants-model-MerchantsModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-tariffs-model-PlansValuesModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-accountHolders-model-UsersModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-accounts-model-AccountsModelView": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-accounts-model-AccountsUserModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-accounts-model-RealmAccountsModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-accountHolders-model-IndustryModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-accounts-model-BankConnectorsModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-notifications-model-NotificationsModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-onboardingLog-model-OnboardingLogModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-tmTagsViolations-model-violationsModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-userRiskTypes-model-UserRiskTypesCombo": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-reconcRietumu-model-ReconciliationModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-bankConnectors-model-BankConnectorsCombo": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-transactions-model-UserTransactionsModel": {"add": true, "del": true, "read": true, "modify": true}, "Crm-modules-invoiceTemplates-model-InvoiceTemplatesModel": {"add": true, "del": true, "read": true, "modify": true}}'::jsonb, NULL, NULL, '{}'::json, '2022-05-23 20:16:56.343', '2022-05-23 20:16:56.343', NULL, NULL, 0, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4'::uuid, NULL, NULL, NULL);
`,
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `UPDATE ${config.schema}.admin_users
SET login='admin', pass='ebfc7910077770c8340f63cd2dca2ac1f120444f', superuser=1, tel=NULL, email=NULL, "name"=NULL, ip=NULL, xgroups=NULL, state=1, "position"=NULL, status='ACTIVE', dblauth=NULL, groupid='e4ede5d1-bf57-4b37-a625-6b4534ecf656'::uuid, ctime='2023-06-16 02:01:54.855', mtime='2023-06-16 02:01:54.855', rtime='2023-06-16 02:01:54.855', signobject=NULL, removed=0, maker=NULL, remover=NULL, checker=NULL, updater=NULL
WHERE "_id"='8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4'::uuid;`,
        { transaction: t }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.sequelize.query(
        `DELETE FROM ${config.schema}."groups"
WHERE "_id"='e4ede5d1-bf57-4b37-a625-6b4534ecf656'::uuid;
`,
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `UPDATE ${config.schema}.admin_users
SET login='admin', pass='ebfc7910077770c8340f63cd2dca2ac1f120444f', superuser=1, tel=NULL, email=NULL, "name"=NULL, ip=NULL, xgroups=NULL, state=1, "position"=NULL, status='ACTIVE', dblauth=NULL, groupid=NULL, ctime='2023-06-16 02:01:54.855', mtime='2023-06-16 02:01:54.855', rtime='2023-06-16 02:01:54.855', signobject=NULL, removed=0, maker=NULL, remover=NULL, checker=NULL, updater=NULL
WHERE "_id"='8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4'::uuid;
`,
        { transaction: t }
      );
    });
  }
};
