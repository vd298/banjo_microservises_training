"use strict";

const common_fields = require("../config/utility/common_fields");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      `users`,
      {
        ...common_fields,
        first_name: {
          type: Sequelize.STRING(50)
        },
        last_name: {
          type: Sequelize.STRING(50)
        },
        email: {
          type: Sequelize.STRING(50)
        },
        mobile: {
          type: Sequelize.STRING(15)
        },
        username: {
          type: Sequelize.STRING(20)
        },
        password: {
          type: Sequelize.STRING(100)
        },
        mobile_verified: {
          type: Sequelize.BOOLEAN,
          defaultValue: false
        },
        email_verified: {
          type: Sequelize.BOOLEAN,
          defaultValue: false
        },
        settings: {
          type: Sequelize.JSONB
        },
        status: {
          type: Sequelize.ENUM("ACTIVE", "INACTIVE", "BLOCKED")
        },
        profile_pic: {
          type: Sequelize.STRING(100)
        },
        timezone: { type: Sequelize.INTEGER }
      },
      {
        schema: config.schema
      }
    );
    await queryInterface.addIndex(
      `${config.schema}.users`,
      ["email", "rtime", "removed"],
      {
        unique: true
      },
      { schema: config.schema }
    );
    await queryInterface.addIndex(
      `${config.schema}.users`,
      ["username", "rtime", "removed"],
      {
        unique: true
      },
      { schema: config.schema }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable({
      schema: config.schema,
      tableName: "users"
    });
  }
};
