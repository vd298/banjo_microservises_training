"use strict";

const vw = require("../views/vw_bank_accounts");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.sequelize.query(
        `alter table ${config.schema}.bank_accounts drop column payment_data;`,
        {
          transaction: t
        }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.sequelize.query(
        `alter table ${config.schema}.bank_accounts add column payment_data jsonb default null;`,
        {
          transaction: t
        }
      );
    });
  }
};
