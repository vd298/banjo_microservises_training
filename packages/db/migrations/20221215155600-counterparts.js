"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "counterparts",
      {
        ...common_fields,
        wallet_id: {
          type: Sequelize.STRING(20),
          allowNull: false
        },
        payment_type_id: {
          type: Sequelize.STRING(40),
          allowNull: false
        },
        is_internal_account: {
          type: Sequelize.BOOLEAN,
          allowNull: false
        },
        full_name: {
          type: Sequelize.STRING(60),
          allowNull: false
        },
        is_legal_entity: {
          type: Sequelize.BOOLEAN,
          allowNull: false
        },
        acc_no: {
          type: Sequelize.STRING(40),
          allowNull: false
        },
        bank_name: {
          type: Sequelize.STRING(60)
        },
        bank_code: {
          type: Sequelize.STRING(20)
        },
        currency: {
          type: Sequelize.STRING(4),
          allowNull: false
        }
      },
      {
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "counterparts"
    });
  }
};
