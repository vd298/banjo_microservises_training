"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "bank_accounts_protocols",
      {
        ...common_fields,
        bank_account_id: {
          type: Sequelize.UUID,
          allowNull: false
        },
        payment_protocol_id: {
          type: Sequelize.UUID,
          allowNull: false
        }
      },
      {
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "bank_accounts_protocols"
    });
  }
};
