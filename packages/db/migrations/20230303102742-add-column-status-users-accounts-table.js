"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "user_accounts" },
        "status",
        {
          type: Sequelize.ENUM("ACTIVE", "INACTIVE", "BLOCKED"),

          allowNull: false,
          defaultValue: "ACTIVE"
        },
        { transaction: t }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "user_accounts" },
        "status",
        {
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop type if exists ${config.schema}.enum_user_accounts_status`,
        { transaction: t }
      );
    });
  }
};
