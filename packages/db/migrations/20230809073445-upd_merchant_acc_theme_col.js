"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "merchant_accounts" },
        "payment_page_theme",
        {
          type: Sequelize.STRING(50)
        },
        { transaction: t }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "merchant_accounts" },
        "payment_page_theme",
        {
          transaction: t
        }
      );
    });
  }
};
