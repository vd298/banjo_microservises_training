"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn(
      { schema: config.schema, tableName: "merchant_accounts" },
      "payment_page_theme",
      {
        type: Sequelize.STRING(50),
        defaultValue: "v2"
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn(
      { schema: config.schema, tableName: "merchant_accounts" },
      "payment_page_theme",
      {
        type: Sequelize.STRING(50)
      }
    );
  }
};
