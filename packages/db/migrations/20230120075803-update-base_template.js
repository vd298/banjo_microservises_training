"use strict";
const fs = require("fs");
const path = require("path");
var global_data;

async function readFile() {
  global_data = fs
    .readFileSync(path.join(__dirname, "..", "templates", "base_template.pug"))
    .toString();
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await readFile();
    var base_template = global_data;
    await queryInterface.sequelize.query(
      ` 
        DELETE FROM ${config.schema}.letters
      WHERE id='be066e8d-2db7-4490-80d8-3375341504b9'::uuid;  
      `
    );
    await queryInterface.sequelize.query(
      `
        INSERT INTO ${config.schema}.letters
(id, ctime, mtime, removed, maker, checker, remover, updater, rtime, signobject, realm, transporter, code, letter_name, from_email, to_email, subject, "text", html, "data", sms_gateway, to_cc, to_bcc, send_email, target_group, send_sms, send_push, to_mobile, sms_text, push_title, push_message, push_action, lang, status, category, sub_category)
VALUES('be066e8d-2db7-4490-80d8-3375341504b9'::uuid, '2022-12-22 13:44:23.255', '2022-12-22 13:44:23.255', 0, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4'::uuid, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'base-template', 'Base Template For all Emails', NULL, NULL, NULL, '', '${base_template}', '{
"image_url":"http://polygon.getgps.online:8102/images/",
"app_url":"http://polygon.getgps.online:8102/"
}', NULL, NULL, NULL, false, 'CUSTOMER'::${config.schema}."enum_letters_target_group", false, false, NULL, '', '', '', NULL, 'en', 'CUSTOMER'::${config.schema}."enum_letters_status", '', '');
        `
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(
      ` 
        DELETE FROM ${config.schema}.letters
      WHERE id='be066e8d-2db7-4490-80d8-3375341504b9'::uuid;  
      `
    );
  }
};
