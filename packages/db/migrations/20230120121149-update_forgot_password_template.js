"use strict";
const fs = require("fs");
const path = require("path");
var global_data;

async function readFile() {
  global_data = global_data = fs
    .readFileSync(
      path.join(__dirname, "..", "templates", "forgot_password.pug")
    )
    .toString();
}
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await readFile();
    var template = global_data;
    await queryInterface.sequelize.query(
      ` 
        DELETE FROM ${config.schema}.letters
      WHERE id='ce97769f-5a1e-4675-9d3b-2921b2c51e15'::uuid;  
      `
    );
    await queryInterface.sequelize.query(
      `
        INSERT INTO ${config.schema}.letters
(id, ctime, mtime, removed, maker, checker, remover, updater, rtime, signobject, realm, transporter, code, letter_name, from_email, to_email, subject, "text", html, "data", sms_gateway, to_cc, to_bcc, send_email, target_group, send_sms, send_push, to_mobile, sms_text, push_title, push_message, push_action, lang, status, category, sub_category)
VALUES('ce97769f-5a1e-4675-9d3b-2921b2c51e15'::uuid, '2023-01-16 13:37:09.897', '2023-01-17 14:59:37.284', 0, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4'::uuid, NULL, NULL, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4'::uuid, NULL, NULL, '046cce25-f407-45c7-8be9-3bf198093408'::uuid, NULL, '1211-PASSW0RD-RESTORE', '11-Password-Restore', 'banjodeve@gmail.com', '', 'Banjo Password Recovery', '', '${template}', '{
      "restoreCode": "lTJtrb4SyI",
      "code": "1211",
      "to": "mt101@tadbox.com",
      "first_name":"Test",
      "last_name":"Account",
      "base_url": "http://localhost:8080/"
}', NULL, '', '', true, 'CUSTOMER'::${config.schema}.enum_letters_target_group, false, false, NULL, '', '', '', NULL, 'en', 'CUSTOMER'::${config.schema}.enum_letters_status, '1000', '200');

        `
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(
      ` 
        DELETE FROM ${config.schema}.letters
      WHERE id='ce97769f-5a1e-4675-9d3b-2921b2c51e15'::uuid;  
      `
    );
  }
};
