"use strict";
const fs = require("fs");
const path = require("path");
var global_data;

async function readFile() {
  global_data = global_data = fs
    .readFileSync(path.join(__dirname, "..", "templates", "bots-alert.pug"))
    .toString();
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await readFile();
    var template = global_data;
    await queryInterface.sequelize.query(
      ` 
        DELETE FROM ${config.schema}.letters
      WHERE id='267f929c-09ab-48f7-a760-229009401f88'::uuid;  
      `
    );
    await queryInterface.sequelize.query(
      `
        INSERT INTO ${config.schema}.letters (id, ctime, mtime, removed, maker, checker, remover, updater, rtime, signobject, realm, transporter, code, letter_name, from_email, to_email, subject, "text", html, "data", sms_gateway, to_cc, to_bcc, send_email, target_group, send_sms, send_push, to_mobile, sms_text, push_title, push_message, push_action, lang, status, category, sub_category) VALUES('267f929c-09ab-48f7-a760-229009401f88', '2023-03-15 12:20:08.168', '2023-03-15 12:21:20.363', 0, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4', NULL, NULL, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4', NULL, NULL, NULL, NULL, 'BOTS-BRN-ALERT', 'BRN Processing failure', 'banjodeve@gmail.com', 'db154+banjo@enovate-it.com, vv161+banjo@enovate-it.com, pm261+banjo@enovate-it.com, as263+banjo@enovate-it.com', 'BRN EXTRACT ALERT', '', '${template}','{
   "acc_no": "123456",
   "bank_host":"indusnet.indusind.com",
   "name": "INDUS"
}', NULL, '', '', true, 'CUSTOMER'::${config.schema}.enum_letters_target_group, false, false, NULL, '', '', '', NULL, 'en', 'CUSTOMER'::${config.schema}.enum_letters_status, '5000', '100');
        
        `
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(
      ` 
        DELETE FROM ${config.schema}.letters
      WHERE id='267f929c-09ab-48f7-a760-229009401f88'::uuid;  
      `
    );
  }
};
