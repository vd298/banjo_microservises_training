"use strict";
const extendPermissions = require("../config/default_realm_permissions");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return extendPermissions(queryInterface);
  },

  down: (queryInterface, Sequelize) => {
    return extendPermissions(queryInterface);
  }
};
