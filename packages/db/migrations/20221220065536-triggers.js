"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "triggers",
      {
        ...common_fields,
        name: Sequelize.STRING,
        service: Sequelize.STRING,
        method: Sequelize.STRING,
        cron: Sequelize.STRING,
        data: Sequelize.JSON,
        ttype: Sequelize.INTEGER,
        tablename: Sequelize.STRING(255),
        conditions: Sequelize.STRING(255)
      },
      {
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "triggers"
    });
  }
};
