"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "account_categories",
      {
        ...common_fields,
        category_id: {
          type: Sequelize.UUID
        },
        account_id: {
          type: Sequelize.UUID
        }
      },
      {
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "account_categories"
    });
  }
};