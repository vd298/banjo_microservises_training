"use strict";

const common_fields = require("../config/utility/common_fields");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      `banks_bulk_templates`,
      {
        ...common_fields,
        template_name: {
          type: Sequelize.STRING(50)
        },
        bank_id: {
          type: Sequelize.UUID,
          allowNull: false
        },
        protocol_id: {
          type: Sequelize.UUID
        },
        description: {
          type: Sequelize.STRING(50)
        },
        template: {
          type: Sequelize.JSONB,
          allowNull: true
        }
      },
      {
        schema: config.schema
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable({
      schema: config.schema,
      tableName: "banks_bulk_templates"
    });
  }
};
