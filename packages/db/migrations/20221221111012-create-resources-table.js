"use strict";

const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      `resources`,
      {
        ...common_fields,
        identifier: {
          type: Sequelize.STRING(150),
          allowNull: false
        },
        alias: {
          type: Sequelize.STRING(50),
          allowNull: false
        },
        resource_type: {
          type: Sequelize.ENUM("CLIENT", "API")
        },
        object_type: {
          type: Sequelize.ENUM("BANK", "BANK ACCOUNT", "USER", "ACCOUNT")
        },
        object_id: {
          type: Sequelize.UUID
        }
      },
      {
        logger: console.log,
        schema: config.schema
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable({
      schema: config.schema,
      tableName: "resources"
    });
    await queryInterface.sequelize.query(
      `drop type if exists ${config.schema}.enum_resources_resource_type`
    );
    await queryInterface.sequelize.query(
      `drop type if exists ${config.schema}.enum_resources_object_type`
    );
  }
};
