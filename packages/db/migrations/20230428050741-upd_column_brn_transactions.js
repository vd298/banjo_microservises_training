"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn(
      { schema: config.schema, tableName: "transactions" },
      "brn",
      {
        type: Sequelize.STRING(200)
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn(
      { schema: config.schema, tableName: "transactions" },
      "brn",
      {
        type: Sequelize.STRING(40)
      }
    );
  }
};
