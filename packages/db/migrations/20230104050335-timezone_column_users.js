"use strict";
const vw_user_invitations = require("../views/vw_user_invitations");
const vw_user_accounts = require("../views/vw_user_accounts");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (transaction) => {
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "users" },
        "timezone",
        {
          type: Sequelize.STRING(50)
        },
        {
          transaction
        }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (transaction) => {
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "users" },
        "timezone",
        {
          type: Sequelize.INTEGER
        },
        {
          transaction
        }
      );
    });
  }
};
