"use strict";
const vw = require("../views/vw_tx_transfer");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addIndex(
        `${config.schema}.account_categories`,
        ["category_id"],
        {
          name: "ac_category_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(`${config.schema}.accounts`, ["plan_id"], {
        name: "acc_plan_id_index",
        transaction: t
      });
      await queryInterface.addIndex(`${config.schema}.accounts`, ["realm_id"], {
        name: "acc_realm_id_index",
        transaction: t
      });
      await queryInterface.addIndex(
        `${config.schema}.admin_users`,
        ["groupid"],
        {
          name: "acc_groupid_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.bank_account_activity_logs`,
        ["bank_account_id"],
        {
          name: "acc_bank_account_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.bank_account_activity_logs`,
        ["bank_account_id"],
        {
          name: "baccl_bank_account_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.bank_accounts`,
        ["payment_provider_id"],
        {
          name: "ba_payment_provider_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.bank_accounts`,
        ["fc_merchant_id"],
        {
          name: "ba_fc_merchant_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.bank_accounts_protocols`,
        ["bank_account_id"],
        {
          name: "bap_bank_account_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.bank_accounts_protocols`,
        ["payment_protocol_id"],
        {
          name: "bap_payment_protocol_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(`${config.schema}.banks`, ["country"], {
        name: "b_country_index",
        transaction: t
      });
      await queryInterface.addIndex(`${config.schema}.categories`, ["code"], {
        name: "c_code_index",
        transaction: t
      });
      await queryInterface.addIndex(
        `${config.schema}.counterparts`,
        ["bank_code"],
        {
          name: "c_bank_code_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(`${config.schema}.countries`, ["abbr2"], {
        name: "c_abbr2_index",
        transaction: t
      });
      await queryInterface.addIndex(`${config.schema}.countries`, ["abbr3"], {
        name: "c_abbr3_index",
        transaction: t
      });
      await queryInterface.addIndex(
        `${config.schema}.exchanges`,
        ["ledger_wallet_id"],
        {
          name: "b_ledger_wallet_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.merchant_account_mappings`,
        ["merchant_account_id"],
        {
          name: "mam_merchant_account_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.merchant_accounts`,
        ["account_id"],
        {
          name: "ma_account_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.merchant_accounts`,
        ["category"],
        {
          name: "ma_category_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.permissions`,
        ["resource_id"],
        {
          name: "p_resource_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.permissions`,
        ["role_id"],
        {
          name: "p_role_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.permissions`,
        ["user_id"],
        {
          name: "p_user_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(`${config.schema}.realms`, ["tariff"], {
        name: "b_tariff_index",
        transaction: t
      });
      await queryInterface.addIndex(
        `${config.schema}.ref_limits`,
        ["protocol_id"],
        {
          name: "rl_protocol_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.ref_limits`,
        ["bank_account_protocol_id"],
        {
          name: "rl_bank_account_protocol_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.resources`,
        ["object_id"],
        {
          name: "r_object_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(`${config.schema}.roles`, ["account_id"], {
        name: "b_account_id_index",
        transaction: t
      });
      await queryInterface.addIndex(`${config.schema}.roles`, ["role_name"], {
        name: "b_role_name_index",
        transaction: t
      });
      // await queryInterface.addIndex(
      //   `${config.schema}.settlements`,
      //   ["merchant_account_id"],
      //   {
      //     name: "s_merchant_account_id_index",
      //     transaction: t
      //   }
      // );
      await queryInterface.addIndex(
        `${config.schema}.tariffplans`,
        ["plan_code"],
        {
          name: "tp_plan_code_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(`${config.schema}.tariffs`, ["trigger"], {
        name: "t_trigger_index",
        transaction: t
      });
      await queryInterface.addIndex(
        `${config.schema}.transactions`,
        ["src_acc_id"],
        {
          name: "tr_src_acc_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.transactions`,
        ["dst_acc_id"],
        {
          name: "tr_dst_acc_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.transactions`,
        ["tariff_id"],
        {
          name: "tr_tariff_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.transactions`,
        ["plan_id"],
        {
          name: "tr_plan_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(`${config.schema}.transactions`, ["brn"], {
        name: "tr_brn_index",
        transaction: t
      });
      await queryInterface.addIndex(
        `${config.schema}.transactions`,
        ["external_id"],
        {
          name: "tr_external_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(`${config.schema}.transfers`, ["ref_num"], {
        name: "tr_ref_num_index",
        transaction: t
      });
      await queryInterface.addIndex(
        `${config.schema}.transfers`,
        ["account_id"],
        {
          name: "t_account_id_index",
          transaction: t
        }
      );

      await queryInterface.addIndex(
        `${config.schema}.transfers`,
        ["exchange_id"],
        {
          name: "t_exchange_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.transfers`,
        ["merchant_account_id"],
        {
          name: "r_merchant_account_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.transfers`,
        ["order_num"],
        {
          name: "t_order_num_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.transfers`,
        ["src_wallet_id"],
        {
          name: "t_src_wallet_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.transfers`,
        ["dst_wallet_id"],
        {
          name: "t_dst_wallet_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(`${config.schema}.transfers`, ["user_id"], {
        name: "t_user_id_index",
        transaction: t
      });
      await queryInterface.addIndex(
        `${config.schema}.transfers`,
        ["ext_user_id"],
        {
          name: "t_ext_user_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.tx_webhooks`,
        ["user_id"],
        {
          name: "b_user_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.tx_webhooks`,
        ["transaction_id"],
        {
          name: "b_transaction_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.user_account_invitations`,
        ["account_id"],
        {
          name: "uai_account_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.user_account_invitations`,
        ["user_id"],
        {
          name: "uai_user_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.user_accounts`,
        ["user_id"],
        {
          name: "ua_user_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.user_accounts`,
        ["main_role"],
        {
          name: "ua_main_role_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.user_accounts`,
        ["account_id"],
        {
          name: "ua_account_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(`${config.schema}.users`, ["email"], {
        name: "u_email_index",
        transaction: t
      });
      await queryInterface.addIndex(`${config.schema}.users`, ["username"], {
        name: "u_username_index",
        transaction: t
      });
      await queryInterface.addIndex(
        `${config.schema}.users`,
        ["mobile_verified"],
        {
          name: "u_mobile_verified_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.users`,
        ["email_verified"],
        {
          name: "u_email_verified_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.wallets`,
        ["merchant_account_mapping_id"],
        {
          name: "w_merchant_account_mapping_id_index",
          transaction: t
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.wallets`,
        ["bank_account_id"],
        {
          name: "w_bank_account_id_index",
          transaction: t
        }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.ac_category_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.acc_plan_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.acc_realm_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.acc_groupid_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.acc_bank_account_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.baccl_bank_account_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.bacc_bank_account_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.ba_payment_provider_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.ba_fc_merchant_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.bap_bank_account_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.bap_payment_protocol_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.b_country_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.c_code_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.c_bank_code_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.c_abbr2_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.c_abbr3_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.b_ledger_wallet_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.mam_merchant_account_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.ma_account_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.ma_category_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.p_resource_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.p_role_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.p_user_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.b_tariff_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.rl_protocol_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.rl_bank_account_protocol_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.r_object_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.b_account_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.b_role_name_index`,
        {
          raw: true,
          transaction: t
        }
      );

      // await queryInterface.sequelize.query(
      //   `drop index if exists ${config.schema}.s_merchant_account_id_index`,
      //   {
      //     raw: true,
      //     transaction: t
      //   }
      // );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.tp_plan_code_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.t_trigger_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.tr_src_acc_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.tr_dst_acc_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.tr_tariff_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.tr_plan_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.tr_brn_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.tr_external_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.tr_ref_num_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.t_account_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.t_exchange_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.r_merchant_account_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.t_order_num_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.t_src_wallet_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.t_dst_wallet_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.t_user_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.t_ext_user_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.b_user_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.b_transaction_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.uai_account_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.uai_user_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.ua_user_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.ua_main_role_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.ua_account_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.u_email_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.u_username_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.u_mobile_verified_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.u_email_verified_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.w_merchant_account_mapping_id_index`,
        {
          raw: true,
          transaction: t
        }
      );

      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.w_bank_account_id_index`,
        {
          raw: true,
          transaction: t
        }
      );
    });
  }
};
