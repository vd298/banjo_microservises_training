"use strict";
const tableNames = [
  "admin_users",
  "banks",
  "categories",
  "counterparts",
  "exchanges",
  "files",
  "groups",
  "messages",
  "payment_providers",
  "permissions",
  "ref_limits",
  "resources",
  "roles",
  "tariffplans",
  "tariffs_es",
  "transporters",
  "triggers",
  "viewset"
];
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      tableNames.forEach(async (tableName) => {
        const SequelizeObj = { type: Sequelize.UUID };
        if (tableName !== "admin_users") {
          SequelizeObj.defaultValue = "046cce25-f407-45c7-8be9-3bf198093408";
        }
        await queryInterface.addColumn(
          { schema: config.schema, tableName },
          "realm_id",
          {
            ...SequelizeObj
          },
          { transaction: t }
        );
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      tableNames.forEach(async (tableName) => {
        await queryInterface.removeColumn(
          { schema: config.schema, tableName },
          "realm_id",
          {
            transaction: t
          }
        );
      });
    });
  }
};
