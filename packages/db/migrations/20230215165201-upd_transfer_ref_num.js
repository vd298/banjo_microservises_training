"use strict";
const vw = require("../views/vw_tx_transfer");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "transfers" },
        "ref_num",
        { type: Sequelize.STRING(200), allowNull: true },
        { transaction: t }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "transfers" },
        "ref_num",
        { type: Sequelize.DOUBLE },
        {
          transaction: t
        }
      );
    });
  }
};
