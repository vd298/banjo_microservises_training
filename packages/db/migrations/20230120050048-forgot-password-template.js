"use strict";
const restoreCode = "${restoreCode}";
const to = "${to}";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.sequelize.query(
        `
        INSERT INTO ${config.schema}.letters
(id, ctime, mtime, removed, maker, checker, remover, updater, rtime, signobject, realm, transporter, code, letter_name, from_email, to_email, subject, "text", html, "data", sms_gateway, to_cc, to_bcc, send_email, target_group, send_sms, send_push, to_mobile, sms_text, push_title, push_message, push_action, lang, status, category, sub_category)
VALUES('ce97769f-5a1e-4675-9d3b-2921b2c51e15'::uuid, '2023-01-16 13:37:09.897', '2023-01-17 14:59:37.284', 0, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4'::uuid, NULL, NULL, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4'::uuid, NULL, NULL, '046cce25-f407-45c7-8be9-3bf198093408'::uuid, NULL, '1211-PASSW0RD-RESTORE', '11-Password-Restore', 'banjodeve@gmail.com', '', 'Banjo Password Recovery', '', 'extend base-template
block content
     tr
               td.mobile-spacing.mobile-spacing-right(style=''text-align: center;'')
                      p(style=''margin-bottom:21px; margin-top: 0; color: #202e55;     font-weight: bold; text-align: left;  letter-spacing: -0.8px; font-size: 22px;'') Restore Password.
     tr  
              td.support-title
                     p(style=''font-size: 18px; margin: 0; padding-bottom: 15px; font-weight: normal; text-align: left; line-height: 1.5; color: #202e55;'')
                      | Hello #{first_name} #{last_name}
     tr  
             td.support-title
                     p(style=''font-size: 18px; margin: 0; padding-bottom: 15px; font-weight: normal; text-align: left; line-height: 1.5; color: #202e55;'')
                      | Please click button below to recover the password.
     tr  
             td.support-title
                     p(style=''font-size: 18px; margin: 0; padding-bottom: 15px; font-weight: normal; text-align: left; line-height: 1.5; color: #202e55;'')
                      | Your confirmation code is: #{restoreCode}
     tr
              td(style=''text-align: center; margin-top: 0; margin-bottom: 0; padding-bottom: 20px'')
                   a(target='' _blank''  href=\`\${base_url}forgot-password-set-new-password?code=${restoreCode}&email=${to}\`\ style=''font-size: 17px; font-weight: 700; color: #fff !important; width: 268px; height: 42px; border-radius: 21px; box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16); background-color: #202e55; border: none; outline: none; cursor: pointer;  display: inline-block; line-height: 42px; text-align: center; text-decoration: none;'') Recover password
     tr  
              td.support-title
                  p(style=''font-size: 18px; margin: 0; padding-bottom: 15px; font-weight: normal; text-align: left; line-height: 1.5; color: #202e55;'')
                      | Kindly ingnore the email if you didn''t try to restore the password. For any queries please contact 
                      a(target='' _blank''  href=\`\${base_url}\`\ style=''font-size: 18px; line-height: 1.78; text-decoration: none; color: #e2001a;'') support
                      tr(style=''border-collapse: collapse;'')
                      td
                          p(style=''padding: 0; margin-top: 24px !important; margin-bottom: 24px !important; border-bottom: 2px solid #ebedf1; background: none; height: 1px; width: 100%; margin: 0px;'')
                  tr
                      td
                          p(style=''font-size: 15px; font-weight: 100; text-align: left; margin: 0; line-height: 1.78; color: #202e55; margin-top: 0; margin-bottom: 0;'')
                              | For security reasons, kindly do not forward this email to anyone. It could give access to your bank account. Please ignore the email If you did not try to register with banjo. 
', '{
      "restoreCode": "lTJtrb4SyI",
      "code": "1211",
      "to": "mt101@tadbox.com",
      "first_name":"Test",
      "last_name":"Account",
      "base_url": "http://localhost:8080/"
}', NULL, '', '', true, 'CUSTOMER'::${config.schema}.enum_letters_target_group, false, false, NULL, '', '', '', NULL, 'en', 'CUSTOMER'::${config.schema}.enum_letters_status, '1000', '200');

        `
      )
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.sequelize.query(
        ` 
        DELETE FROM ${config.schema}.letters
      WHERE id='ce97769f-5a1e-4675-9d3b-2921b2c51e15'::uuid;  
      `
      )
    ]);
  }
};
