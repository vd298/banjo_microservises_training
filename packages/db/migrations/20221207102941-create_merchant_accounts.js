"use strict";

const common_fields = require("../config/utility/common_fields");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      `merchant_accounts`,
      {
        ...common_fields,
        account_id: {
          type: Sequelize.UUID
        },
        name: {
          type: Sequelize.STRING(50),
          allowNull: false
        },
        acc_no: {
          type: Sequelize.STRING(16)
        },
        currency: {
          type: Sequelize.STRING(4)
        },
        settlement_currency: {
          type: Sequelize.STRING(4)
        },
        category: {
          type: Sequelize.UUID
        },
        status: {
          type: Sequelize.ENUM("ACTIVE", "INACTIVE", "BLOCKED"),
          allowNull: false
        }
      },
      {
        schema: config.schema
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable({
      schema: config.schema,
      tableName: "merchant_accounts"
    });
  }
};
