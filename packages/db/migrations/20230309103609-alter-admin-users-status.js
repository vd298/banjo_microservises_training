const dependedView = require("../views/vw_user_invitations");
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn(
      { schema: config.schema, tableName: "admin_users" },
      "status",
      {
        type: Sequelize.STRING(15),
        allowNull: true
      }
    );
    await queryInterface.bulkUpdate(
      { schema: config.schema, tableName: "admin_users" },
      { status: "ACTIVE" },
      {}, //Where
      {} //options
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn(
      { schema: config.schema, tableName: "admin_users" },
      "status",
      {
        type: Sequelize.STRING(15),
        allowNull: false
      }
    );
  }
};
