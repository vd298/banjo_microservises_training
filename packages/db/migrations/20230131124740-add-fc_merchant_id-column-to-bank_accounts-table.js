"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "bank_accounts" },
        "fc_merchant_id",
        {
          type: Sequelize.STRING(15)
        },
        {
          transaction: t
        }
      );
      await queryInterface.addIndex(
        { schema: config.schema, tableName: "bank_accounts" },
        ["fc_merchant_id"],
        { unique: true, transaction: t }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeIndex(
        { schema: config.schema, tableName: "bank_accounts" },
        ["fc_merchant_id"],
        { transaction: t }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "bank_accounts" },
        "fc_merchant_id",
        {
          transaction: t
        }
      );
    });
  }
};
