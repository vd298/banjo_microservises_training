"use strict";

const common_fields = require("../config/utility/common_fields");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      `accounts`,
      {
        ...common_fields,
        name: {
          type: Sequelize.STRING(50),
          allowNull: false
        },
        address: {
          type: Sequelize.STRING(150)
        },
        city: {
          type: Sequelize.STRING(50)
        },
        country: {
          type: Sequelize.STRING(2),
          allowNull: false
        },
        legal_entity: {
          type: Sequelize.BOOLEAN,
          defaultValue: false,
          allowNull: false
        },
        registration_no: {
          type: Sequelize.STRING(30)
        },
        plan_id: {
          type: Sequelize.UUID,
          allowNull: true
        },
        type: {
          type: Sequelize.ENUM("MERCHANT"),
          defaultValue: "MERCHANT",
          allowNull: false
        },
        status: {
          type: Sequelize.ENUM("ACTIVE", "INACTIVE", "BLOCKED"),
          allowNull: false
        },
        realm_id: {
          type: Sequelize.UUID,
          allowNull: false
        }
      },
      {
        schema: config.schema
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable({
      schema: config.schema,
      tableName: "accounts"
    });
  }
};
