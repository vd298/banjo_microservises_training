"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    let sql = `alter table ${config.schema}.bank_accounts_protocols alter column removed set default 0;`;
    let updSql = `update ${config.schema}.bank_accounts_protocols set removed=0 where removed is null`;
    await queryInterface.sequelize.query(sql);
    await queryInterface.sequelize.query(updSql);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.resolve();
  }
};
