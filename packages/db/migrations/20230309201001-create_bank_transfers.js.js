"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "bank_transfers",
      {
        ...common_fields,
        bank_external_id: {
          type: Sequelize.STRING(100),
          allowNull: false
        },
        bank_status: {
          type: Sequelize.STRING(100),
          allowNull: false
        },
        transfer_id: {
          type: Sequelize.UUID,
          allowNull: false
        },
        bank_domain: {
          type: Sequelize.STRING(300),
          allowNull: false
        },
        data: {
          type: Sequelize.JSONB,
          allowNull: true
        }
      },
      {
        schema: config.schema
      }
    );
    await queryInterface.addIndex(
      `${config.schema}.bank_transfers`,
      ["bank_external_id"],
      {},
      { schema: config.schema }
    );
    await queryInterface.addIndex(
      `${config.schema}.bank_transfers`,
      ["bank_status"],
      {},
      { schema: config.schema }
    );
    await queryInterface.addIndex(
      `${config.schema}.bank_transfers`,
      ["transfer_id"],
      {},
      { schema: config.schema }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "bank_transfers"
    });
  }
};
