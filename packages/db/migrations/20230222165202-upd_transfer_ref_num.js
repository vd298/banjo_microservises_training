"use strict";
const vw = require("../views/vw_tx_transfer");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "transfers" },
        "ref_num",
        { type: Sequelize.STRING(200), allowNull: true },
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        ` 
        drop index if exists ${config.schema}.transfers_ref_num`,
        {
          raw: true,
          type: Sequelize.QueryTypes.SELECT,
          transaction: t
        }
      );
      // await queryInterface.removeIndex("transfers", ["ref_num"], {
      //   transaction: t,
      //   tableName: "transfers",
      //   logging: console.log
      // });
      await queryInterface.addIndex(`${config.schema}.transfers`, ["ref_num"], {
        name: "unique_transfers_ref_num",
        tableName: `${config.schema}.transfers`,
        type: "UNIQUE",
        transaction: t
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      //Vaibhav Vaidya, Standard sequelize does not work, use type casting.
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "transfers" },
        "ref_num",
        { type: "float USING ref_num::float" },
        {
          transaction: t
        }
      );
      await queryInterface.sequelize.query(
        `drop index if exists ${config.schema}.unique_transfers_ref_num`,
        {
          raw: true,
          type: Sequelize.QueryTypes.SELECT,
          transaction: t
        }
      );
      // await queryInterface.removeIndex(
      //   "",
      //   ["unique_transfers_ref_num"],
      //   { transaction: t, tableName: "transfers", logging: console.log }
      // );
      await queryInterface.addIndex(`${config.schema}.transfers`, ["ref_num"], {
        name: "transfers_ref_num",
        transaction: t,
        logging: console.log
      });
    });
  }
};
