"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "realms" },
        "status",
        {
          type: Sequelize.ENUM("ACTIVE", "INACTIVE", "BLOCKED"),
          allowNull: false,
          defaultValue: "ACTIVE"
        },
        { transaction: t }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "realms" },
        "status",
        {
          transaction: t
        }
      );
      await queryInterface.sequelize.query(
        `drop type if exists ${config.schema}.enum_realms_status`,
        { transaction: t }
      );
    });
  }
};
