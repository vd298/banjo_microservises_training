"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    try {
      //Vaibhav Vaidya, This migration is duplicated by 20230112061814-add-ips-and-webhook-column-users
      // await queryInterface.addColumn(
      //   { schema: config.schema, tableName: "users" },
      //   "ips",
      //   {
      //     type: Sequelize.ARRAY(Sequelize.TEXT)
      //   }
      // );
      return Promise.resolve();
    } catch (e) {
      return Promise.reject(e);
    }
  },

  down: async (queryInterface, Sequelize) => {
    try {
      //Vaibhav Vaidya, This migration is duplicated by 20230112061814-add-ips-and-webhook-column-users
      // await queryInterface.removeColumn(
      //   { schema: config.schema, tableName: "users" },
      //   "ips"
      // );
      return Promise.resolve();
    } catch (e) {
      return Promise.reject(e);
    }
  }
};
