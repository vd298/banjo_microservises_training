"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { schema: config.schema, tableName: "realms" },
      [
        {
          id: "988987e0-3f7a-4bdc-a26f-fbbcba5e8843",
          pid: "046cce25-f407-45c7-8be9-3bf198093408",
          name: "Merchant Realm",
          token:
            "4991f018b94dd4a8af39d975ab8ee8f3c5a01ae1a75431bb1cda6214221cd769",
          ctime: new Date(),
          mtime: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      { schema: config.schema, tableName: "realms" },
      null,
      {}
    );
  }
};
