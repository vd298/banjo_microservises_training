const dependedView = require("../views/vw_user_invitations");
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn(
      { schema: config.schema, tableName: "wallets" },
      "acc_label",
      {
        type: Sequelize.STRING(50),
        allowNull: false
      }
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn(
      { schema: config.schema, tableName: "wallets" },
      "acc_label",
      {
        type: Sequelize.STRING(20),
        allowNull: false
      }
    );
  }
};
