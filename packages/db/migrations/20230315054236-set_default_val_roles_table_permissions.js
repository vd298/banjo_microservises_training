"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    let sql = `update ${config.schema}.roles set permissions='{"role":{"view":true,"create":true,"edit":true,"delete":true}}' where role_name='Owner'`;
    let sql1 = `update ${config.schema}.roles set permissions='{"role":{"view":false,"create":false,"edit":false,"delete":false}}' where permissions is null`;
     
    await queryInterface.sequelize.query(sql);
    await queryInterface.sequelize.query(sql1);

  },

  down: async (queryInterface, Sequelize) => {
    return Promise.resolve();
  }
};
