"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("tests", {
      name: {
        type: Sequelize.STRING(50)
      },
      realm_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "realms",
          key: "id"
        },
        onUpdate: "cascade",
        onDelete: "cascade"
      },
      user_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: "users",
          key: "id"
        },
        onUpdate: "cascade",
        onDelete: "cascade"
      },
      service: {
        type: Sequelize.STRING
      },
      method: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      data: {
        type: Sequelize.JSON
      },
      result: {
        type: Sequelize.TEXT
      },
      status: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      ...common_fields
    },
      {
        schema: config.schema
      });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "tests"
    });
  }
};
