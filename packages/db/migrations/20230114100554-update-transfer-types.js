"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "transfers" },
        "counterpart_id",
        {
          type: Sequelize.UUID,
          allowNull: true
        },
        { transaction: t }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transfers" },
        "src_wallet_id",
        {
          type: Sequelize.UUID,
          allowNull: false
        },
        { transaction: t }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transfers" },
        "dst_wallet_id",
        {
          type: Sequelize.UUID,
          allowNull: false
        },
        { transaction: t }
      );
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "transactions" },
        "statement_note",
        {
          type: Sequelize.STRING(60),
          allowNull: true
        },
        { transaction: t }
      );
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "transactions" },
        "internal_note",
        {
          type: Sequelize.STRING(150),
          allowNull: true
        },
        { transaction: t }
      );
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "transactions" },
        "src_acc_id",
        {
          type: Sequelize.UUID,
          allowNull: true
        },
        { transaction: t }
      );
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "transactions" },
        "dst_acc_id",
        {
          type: Sequelize.UUID,
          allowNull: true
        },
        { transaction: t }
      );
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "transactions" },
        "amount",
        {
          type: Sequelize.DOUBLE,
          allowNull: true
        },
        { transaction: t }
      );
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "transactions" },
        "currency",
        {
          type: Sequelize.STRING(4),
          allowNull: true
        },
        { transaction: t }
      );
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "transactions" },
        "src_balance",
        {
          type: Sequelize.DOUBLE,
          allowNull: true
        },
        { transaction: t }
      );
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "transactions" },
        "dst_balance",
        {
          type: Sequelize.DOUBLE,
          allowNull: true
        },
        { transaction: t }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transfers" },
        "transfer_type",
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        ` drop type ${config.schema}.enum_transfers_transfer_type`,
        {
          transaction: t
        }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transfers" },
        "transfer_type",
        {
          type: Sequelize.STRING(20)
        },
        { transaction: t }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transfers" },
        "event_name",
        {
          type: Sequelize.STRING(60)
        },
        { transaction: t }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transactions" },
        "activity",
        {
          type: Sequelize.STRING(20)
        },
        { transaction: t }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transfers" },
        "description",
        {
          type: Sequelize.STRING(60)
        },
        { transaction: t }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transactions" },
        "txtype",
        {
          type: Sequelize.STRING(20)
        },
        { transaction: t }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transfers" },
        "src_wallet_id",
        { transaction: t }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transfers" },
        "dst_wallet_id",
        { transaction: t }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transactions" },
        "activity",
        { transaction: t }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transactions" },
        "txtype",
        { transaction: t }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transfers" },
        "description",
        { transaction: t }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transfers" },
        "event_name",
        { transaction: t }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transfers" },
        "transfer_type",
        { transaction: t }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transfers" },
        "transfer_type",
        {
          type: Sequelize.ENUM(
            "PAYOUT",
            "PAYIN",
            "COLLECTION",
            "REFUND",
            "RETURN",
            "REVERSE",
            "TRANSFER"
          )
        },
        { transaction: t }
      );
    });
  }
};
