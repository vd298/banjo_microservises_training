"use stric";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.sequelize.query(
        `INSERT INTO ${config.schema}.letters
(id, ctime, mtime, removed, maker, checker, remover, updater, rtime, signobject, realm, transporter, code, letter_name, from_email, to_email, subject, "text", html, "data", sms_gateway, to_cc, to_bcc, send_email, target_group, send_sms, send_push, to_mobile, sms_text, push_title, push_message, push_action, lang, status, category, sub_category)
VALUES('031e7dd9-360f-4a1e-a9b2-a8820af52341'::uuid, '2024-03-05 02:13:17.518', '2024-03-14 11:28:17.202', 0, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4'::uuid, NULL, NULL, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4'::uuid, NULL, NULL, NULL, NULL, 'RASHIPAY-REPORT-EMAIL', 'Rashipay Excel Report', 'banjodeve@gmail.com', '', 'Rashipay Excel Report', '', 'extend base-template
block content
    tr(style=''border-collapse: collapse;'')
      td.w320.mobile-spacing(style=''font-size:18px; font-weight: 500; color: #5E5873; padding: 0 40px 16px;'')
        | Dear Team,
    tr(style=''border-collapse: collapse;'')
      td(style=''padding: 0; margin: 0;'')
    tr(style=''border-collapse: collapse;'')
      td(style=''padding: 20px 40px; margin: 0;'')
        p(style="font-size: 16px;        font-family: ''Montserrat'', sans-serif; margin-bottom: 15px; color: #5E5873;")
          | Please find attached the daily transaction report of  #{date} for Rapidogate with this email', '{
   "acc_name": "Big Bazaar",
   "date":"13th March 2024"
}', NULL, '', '', true, 'CUSTOMER'::${config.schema}."enum_letters_target_group", false, false, NULL, '', '', '', NULL, 'en', 'CUSTOMER'::${config.schema}."enum_letters_status", '5000', '100');`
      )
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.sequelize.query(
        ` 
        DELETE FROM ${config.schema}.letters
      WHERE id='031e7dd9-360f-4a1e-a9b2-a8820af52341'::uuid;  
      `
      )
    ]);
  }
};
