"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      { schema: config.schema, tableName: "bank_accounts" },
      "payment_data",
      {
        type: Sequelize.JSONB
      }
    );  
    await queryInterface.removeColumn(
      { schema: config.schema, tableName: "bank_accounts" },
      "payment_type"
    )
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      { schema: config.schema, tableName: "bank_accounts" },
      "payment_data"
    );
    await queryInterface.addColumn(
      { schema: config.schema, tableName: "bank_accounts" },
      "payment_type",
      {
        type: Sequelize.UUID
      }
    );  
  }
};
