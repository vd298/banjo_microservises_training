"use strict";

let schemaName = "";

module.exports = {
  up: (queryInterface) => {
    schemaName = config.schema;

    console.log("schemaName:", schemaName);

    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.sequelize.query(
        `SET search_path TO ${schemaName}, staging, public;`,
        { transaction: t }
      );
/*      await queryInterface.sequelize.query(
        `DROP EXTENSION IF EXISTS "uuid-ossp";`,
        { transaction: t }
      );*/
      await queryInterface.sequelize.query(
        `CREATE EXTENSION IF NOT EXISTS "uuid-ossp" with schema ${config.schema}`,
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `CREATE TABLE ${schemaName}.admin_users (
          _id uuid default uuid_generate_v4() primary key,
          login character varying(20),
          pass character varying(80),
          superuser smallint,
          tel character varying(15),
          email character varying(255),
          name character varying(20),
          ip character varying(15),
          xgroups json,
          state integer,
          "position" character varying(20),
          status character(15),
          dblauth integer,
          groupid uuid,
          
          ctime timestamptz NOT NULL default now(),
          mtime timestamptz NOT NULL default now(),
          rtime timestamptz,
          signobject json,
          removed smallint default 0,
          maker uuid,
          remover uuid,
          checker uuid, 
          updater uuid
          );`,
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `CREATE TABLE ${schemaName}.countries (
          id uuid default uuid_generate_v4() primary key,
          code character varying(50),
          name character varying(50),
          abbr2 character varying(2),
          abbr3 character varying(3),
          
          ctime timestamptz NOT NULL default now(),
          mtime timestamptz NOT NULL default now(),
          rtime timestamptz,
          signobject json,
          removed smallint default 0,
          maker uuid,
          remover uuid,
          checker uuid, 
          updater uuid)`,
        { transaction: t }
      );

      await queryInterface.sequelize.query(
        `CREATE TABLE ${schemaName}.groups (
          _id uuid default uuid_generate_v4() primary key,
          name character varying(30),
          code character varying(15),
          description character varying(255),
          desktopclasname character varying(100),
          autorun character varying(100),
          modelaccess jsonb,
          reportsaccess jsonb,
          pagesaccess character(255),
          apiaccess json,
          
          ctime timestamptz NOT NULL default now(),
          mtime timestamptz NOT NULL default now(),
          rtime timestamptz,
          signobject json,
          removed smallint default 0,
          maker uuid,
          remover uuid,
          checker uuid, 
          updater uuid
      );`,
        { transaction: t }
      );

      await queryInterface.sequelize.query(
        `CREATE TABLE ${schemaName}.messages (
          _id uuid default uuid_generate_v4() primary key,
          subject character varying(255),
          body text,
          to_name jsonb,
          fromuser character varying(255),
          touser jsonb,
          new smallint,
          owner uuid,
          
          ctime timestamptz NOT NULL default now(),
          mtime timestamptz NOT NULL default now(),
          rtime timestamptz,
          signobject json,
          removed smallint default 0,
          maker uuid,
          remover uuid,
          checker uuid, 
          updater uuid
      );`,
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `CREATE TABLE ${schemaName}.realms (
          id uuid default uuid_generate_v4() primary key,
          name character varying(40),
          token character varying(80),
          ip character varying(255),
          permissions json,
          activateuserlink character varying(255),
          tariff uuid,
          variables json,
          domain character varying(255),
          pid uuid,
          cors json,
          admin_realm boolean DEFAULT false,
          
          ctime timestamptz NOT NULL default now(),
          mtime timestamptz NOT NULL default now(),
          rtime timestamptz,
          signobject json,
          removed smallint default 0,
          maker uuid,
          remover uuid,
          checker uuid, 
          updater uuid
      );`,
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `CREATE TABLE ${schemaName}.signset (
          _id uuid default uuid_generate_v4() primary key,
          module character varying(255),
          priority json,
          active integer,
          realm uuid,
          
          ctime timestamptz NOT NULL default now(),
          mtime timestamptz NOT NULL default now(),
          rtime timestamptz,
          signobject json,
          removed smallint default 0,
          maker uuid,
          remover uuid,
          checker uuid, 
          updater uuid
      );`,
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `CREATE TABLE ${schemaName}.tariffplans (
          id uuid default uuid_generate_v4() primary key,
          name character varying(40),
          description text,
          tariffs json,
          variables json,
          active boolean default true,
          
          ctime timestamptz NOT NULL default now(),
          mtime timestamptz NOT NULL default now(),
          rtime timestamptz,
          signobject json,
          removed smallint default 0,
          maker uuid,
          remover uuid,
          checker uuid, 
          updater uuid
      );`,
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `CREATE TABLE ${schemaName}.tariffs (
          id uuid default uuid_generate_v4() primary key,
          name character varying(255),
          description text,
          trigger uuid,
          data json,
          variables json,
          actions json,
          rules json,
          stop_on_rules boolean DEFAULT false,
          pid uuid,

          ctime timestamptz NOT NULL default now(),
          mtime timestamptz NOT NULL default now(),
          rtime timestamptz,
          signobject json,
          removed smallint default 0,
          maker uuid,
          remover uuid,
          checker uuid, 
          updater uuid
      );`,
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `CREATE TABLE ${schemaName}.tariffs_es (
          id uuid default uuid_generate_v4() primary key,
          name character varying(40) NOT NULL,
          pcategory uuid,
          ptype uuid,
          fee_withdrawal double precision,
          fee_transfer double precision,
          fee_masspayment double precision,
          fee_merchant double precision,
          enb_deposit boolean,
          enb_withdrawal boolean,
          enb_merchant boolean,
          
          ctime timestamptz NOT NULL default now(),
          mtime timestamptz NOT NULL default now(),
          rtime timestamptz,
          signobject json,
          removed smallint default 0,
          maker uuid,
          remover uuid,
          checker uuid, 
          updater uuid 
      );`,
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `CREATE TABLE ${schemaName}.transporters (
          id uuid default uuid_generate_v4() primary key,
          host_transporter character varying(255),
          port_transporter integer,
          secure_transporter boolean,
          user_transporter character varying(255),
          password_transporter character varying(255),
          
          
          ctime timestamptz NOT NULL default now(),
          mtime timestamptz NOT NULL default now(),
          rtime timestamptz,
          signobject json,
          removed smallint default 0,
          maker uuid,
          remover uuid,
          checker uuid, 
          updater uuid 
      );`,
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `CREATE TABLE ${schemaName}.currency (
          id uuid default uuid_generate_v4() primary key,
          code integer,
          name character varying(50),
          abbr character varying(3),
          crypto boolean DEFAULT false,
          fraction_points smallint,

          ctime timestamptz NOT NULL default now(),
          mtime timestamptz NOT NULL default now(),
          rtime timestamptz,
          signobject json,
          removed smallint default 0,
          maker uuid,
          remover uuid,
          checker uuid, 
          updater uuid 
      );`,
        { transaction: t }
      );
    });
  },

  down: (queryInterface) => {
    schemaName = config.schema;

    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.sequelize.query(
        `DROP TABLE ${[
          "currency",
          "transporters",
          "tariffs_es",
          "tariffs",
          "tariffplans",
          "signset",
          "realms",
          "messages",
          "groups",
          "countries",
          "admin_users"
        ]
          .map((tableName) => `${schemaName}.${tableName}`)
          .join(",")}`,
        {
          transaction: t
        }
      );
      await queryInterface.sequelize.query(
        `SET search_path TO ${schemaName}; DROP EXTENSION IF EXISTS "uuid-ossp"`,
        { transaction: t }
      );
    });
  }
};
