"use strict";
const vw = require("../views/vw_merchant_account_mappings");
const dependedView = require("../views/vw_bank_accounts");
module.exports = {
  //Vaibhav Vaidya note, Due to expanded columns/view changes that occur at later stage, this migration file was emptied on purpose.
  //Do no delete, as it causes problem with migration rollback
  up: async (queryInterface, Sequelize) => {
    //Vaibhav Vaidya note, Views are now Auto Created/Deleted
    return Promise.resolve();
  },

  down: async (queryInterface, Sequelize) => {
    //Vaibhav Vaidya note, Views are now Auto Created/Deleted
    return Promise.resolve();
  }
};
