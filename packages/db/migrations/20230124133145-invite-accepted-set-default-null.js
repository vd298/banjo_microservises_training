const dependedView = require("../views/vw_user_invitations");
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn(
      { schema: config.schema, tableName: "user_account_invitations" },
      "accepted",
      {
        type: Sequelize.BOOLEAN,
        defaultValue: null
      }
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn(
      { schema: config.schema, tableName: "user_account_invitations" },
      "accepted",
      {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      }
    );
  }
};
