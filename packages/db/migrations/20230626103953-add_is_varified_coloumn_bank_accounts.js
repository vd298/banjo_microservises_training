"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "bank_accounts" },
        "is_verified",
        { type: Sequelize.INTEGER, defaultValue: 1 },
        { transaction: t }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "bank_accounts" },
        "is_verified",
        { type: Sequelize.INTEGER },
        {
          transaction: t
        }
      );
    });
  }
};
