"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { schema: config.schema, tableName: "realms" },
      [
        {
          pid: "046cce25-f407-45c7-8be9-3bf198093408",
          id: "2803d532-b4a2-4ced-91ae-677bcb8dae57",
          name: "BOT Realm",
          token:
            "37f934d302727ebb95f622b3e790b5ec53ba92e988f577e857de11c5ad864c3f",
          ctime: new Date(),
          mtime: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      { schema: config.schema, tableName: "realms" },
      { id: "2803d532-b4a2-4ced-91ae-677bcb8dae57" },
      {}
    );
  }
};
