"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transactions" },
        "external_id",
        {
          type: Sequelize.STRING(40)
        },
        {
          transaction: t
        }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transactions" },
        "external_type",
        {
          type: Sequelize.STRING(25)
        },
        {
          transaction: t
        }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transactions" },
        "external_id",
        {
          transaction: t
        }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transactions" },
        "external_type",
        {
          transaction: t
        }
      );
    });
  }
};
