"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "transactions",
      {
        ...common_fields,
        transfer_id: {
          type: Sequelize.UUID,
          allowNull: false
        },
        statement_note: {
          type: Sequelize.STRING(60),
          allowNull: false
        },
        internal_note: {
          type: Sequelize.STRING(150),
          allowNull: false
        },
        src_acc_id: {
          type: Sequelize.UUID,
          allowNull: false
        },
        dst_acc_id: {
          type: Sequelize.UUID,
          allowNull: false
        },
        amount: {
          type: Sequelize.DOUBLE,
          allowNull: false
        },
        src_balance: {
          type: Sequelize.DOUBLE,
          allowNull: false
        },
        dst_balance: {
          type: Sequelize.DOUBLE,
          allowNull: false
        },
        currency: {
          type: Sequelize.STRING(4),
          allowNull: false
        },
        tariff_id: {
          type: Sequelize.UUID
        },
        plan_id: {
          type: Sequelize.UUID
        },
        status: {
          type: Sequelize.ENUM(
            "CREATED",
            "PENDING",
            "REJECTED",
            "SUCCESS",
            "REFUND"
          ),
          allowNull: false,
          defaultValue: "CREATED"
        },
        brn: {
          type: Sequelize.STRING(40)
        },
        hash: {
          type: Sequelize.STRING(40)
        }
      },
      {
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "transactions"
    });
  }
};
