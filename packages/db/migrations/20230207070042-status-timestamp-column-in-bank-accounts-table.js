"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "bank_accounts" },
        "status",
        {
          type: Sequelize.ENUM("ACTIVE", "INACTIVE"),
          allowNull: false,
          defaultValue: "ACTIVE"
        },
        { transaction: t }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "bank_accounts" },
        "request_timestamp",
        {
          type: Sequelize.DATE
        },
        { transaction: t }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "bank_accounts" },
        "status",
        {
          transaction: t
        }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "bank_accounts" },
        "request_timestamp",
        {
          transaction: t
        }
      );
      await queryInterface.sequelize.query(
        `drop type if exists ${config.schema}.enum_bank_accounts_status`,
        { transaction: t }
      );
    });
  }
};
