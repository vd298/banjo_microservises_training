"use strict";
const vw = require("../views/vw_tx_transfer");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      //Vaibhav Vaidya Note: Uncomment below and run migration IF failing due to null rows.
      // await queryInterface.sequelize.query(
      //   `truncate ${config.schema}.user_account_invitations;`,
      //   { replacements: {}, transaction: t}
      // );
      // await queryInterface.sequelize.query(
      //   `truncate ${config.schema}.user_accounts;`,
      //   {
      //     replacements: {},
      //     transaction: t
      //   }
      // );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "user_account_invitations" },
        "merchant_account_id",
        {
          allowNull: true,
          type: Sequelize.UUID
        },
        {
          transaction: t
        }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "user_accounts" },
        "merchant_account_id",
        {
          allowNull: true,
          type: Sequelize.UUID
        },
        {
          transaction: t
        }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "user_account_invitations" },
        "merchant_account_id",
        {
          transaction: t
        }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "user_accounts" },
        "merchant_account_id",
        {
          transaction: t
        }
      );
    });
  }
};
