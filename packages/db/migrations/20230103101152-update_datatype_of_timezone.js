"use strict";
const common_fields = require("../config/utility/common_fields");
const vw_user_accounts = require("../views/vw_user_accounts");

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "users" },
        "timezone",
        { type: Sequelize.STRING(50) },
        { transaction: t }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "users" },
        "timezone",
        { type: Sequelize.STRING(50) },
        {
          transaction: t
        }
      );
    });
  }
};
