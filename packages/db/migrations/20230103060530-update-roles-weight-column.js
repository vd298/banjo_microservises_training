"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      { schema: config.schema, tableName: "roles" },
      "weight",
      {
        type: Sequelize.INTEGER
      }
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      { schema: config.schema, tableName: "roles" },
      "weight"
    );
  }
};
