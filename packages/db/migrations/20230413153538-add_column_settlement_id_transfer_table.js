"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transfers" },
        "settlement_id",
        { type: Sequelize.UUID },
        { transaction: t }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transfers" },
        "settlement_id",
        { type: Sequelize.UUID },
        {
          transaction: t
        }
      );
    });
  }
};
