"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "transfers",
      {
        ...common_fields,
        src_amount: {
          type: Sequelize.DOUBLE,
          allowNull: false
        },
        src_currency: {
          type: Sequelize.STRING(4),
          allowNull: false
        },
        dst_amount: {
          type: Sequelize.DOUBLE,
          allowNull: false
        },
        dst_currency: {
          type: Sequelize.STRING(4),
          allowNull: false
        },
        transfer_type: {
          type: Sequelize.ENUM("PAYOUT", "PAYIN", "TRANSFER"),
          allowNull: false
        },
        ref_num: {
          type: Sequelize.DOUBLE,
          allowNull: false
        },
        counterpart_id: {
          type: Sequelize.UUID,
          allowNull: false
        },
        is_exchange: {
          type: Sequelize.BOOLEAN,
          defaultValue: false
        },
        exchange_id: {
          type: Sequelize.UUID
        },
        data: {
          type: Sequelize.JSONB
        }
      },
      {
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "transfers"
    });
  }
};
