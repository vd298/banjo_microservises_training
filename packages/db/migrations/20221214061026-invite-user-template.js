"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.sequelize.query(
        `
        INSERT INTO ${config.schema}.letters (id, ctime, mtime, removed, maker, checker, remover, updater, rtime, signobject, realm, transporter, code, letter_name, from_email, to_email, subject, "text", html, "data", sms_gateway, to_cc, to_bcc, send_email, target_group, send_sms, send_push, to_mobile, sms_text, push_title, push_message, push_action, lang, status, category, sub_category) VALUES('8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce5', '2022-12-08 12:20:08.168', '2022-12-08 12:21:20.363', 0, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4', NULL, NULL, '8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4', NULL, NULL, NULL, NULL, 'INVITE-EMAIL', 'Invite email', '', '', 'Invite User ', '', 'extend base-template
block content
    tr(style=''border-collapse: collapse;'')
      td.w320.mobile-spacing(style=''font-size:18px; font-weight: 500; color: #5E5873; padding: 0 40px 16px;'')
        | Hey 
        strong #{invite_user_name}
        | ,
    tr(style=''border-collapse: collapse;'')
      td(style=''margin: 0; padding: 0 40px 30px;'')
        h3(style=''font-size: 18px;\
        font-weight: 500; color: #5E5873; margin:0;'')
          | You have been invited to Banjo!
    tr(style=''border-collapse: collapse;'')
      td(style=''padding: 0; margin: 0;'')
        img(style=''display: block; border: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;'', width=''100%'', src=''http://polygon.getgps.online:8109/Admin.Data.getFile/?name=invite-user.jpg&tmp=9b50373b-de9a-4447-b2d5-45574e9f49a6'', alt=''invite user'')
    tr(style=''border-collapse: collapse;'')
      td(style=''padding: 30px 40px; margin: 0;'')
        p(style="font-size: 16px;\
        font-family: ''Montserrat'', sans-serif; margin-bottom: 15px; color: #5E5873;")
          | John with enovate-it pvt ltd has invited you to use banjo to collaborate with them. Use the button below to set up your account and get started:
        p(style="font-size: 16px; font-family: ''Montserrat'', sans-serif; margin-bottom: 15px; color: #5E5873;")
          | If you did not sign up for ResourceRoot, please ignore this email or contact us at 
          a(style=''color:#7367F0; text-decoration:none;'', href=''polygon.com'', target=''_blank'') support@polygon.com
        p(style="font-size: 16px; font-family: ''Montserrat'', sans-serif; margin-bottom: 0; color: #5E5873;")
          | Verification Link: 
          a(style=''color:#7367F0; text-decoration:none;'', href=''http://polygon.getgps.online:8102/welcome-user/#{token}'', target=''_blank'') www.polygon.getgps.online:8102/welcome-user/#{token}  

    td(style=''padding: 0 40px 30px;'')
        a(href=''polygon.com'', target=''_blank'', style=''font-size: 16px; font-weight: 500;\
        color: #ffffff; background: #7367F0;border-radius: 5px; border: none; outline: none; cursor: pointer;  display: inline-block; padding: 10px 35px; text-align: center;'') Sign up', '{
   "invite_user_name": "Dipak Gaware","token":"3e6e55e8-af36-4572-9c79-1befe1c2f0fa"
}', NULL, '', '', true, 'CUSTOMER', false, false, NULL, '', '', '', NULL, 'en', 'CUSTOMER', '1000', '100');;

        
        
        `
      )
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.sequelize.query(
        ` 
        DELETE FROM ${config.schema}.letters
      WHERE id='8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce5'::uuid;  
      `
      )
    ]);
  }
};
