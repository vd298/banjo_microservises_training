"use strict";
const vw = require("../views/vw_tx_transfer");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "wallets" },
        "negative",
        {
          type: Sequelize.BOOLEAN,
          defaultValue: false,
          allowNull: false
        },
        { transaction: t }
      );
      await queryInterface.addIndex(
        { schema: config.schema, tableName: "transactions" },
        ["brn"],
        { unique: true, transaction: t }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transactions" },
        "data",
        {
          type: Sequelize.JSONB
        },
        { transaction: t }
      );
      await queryInterface.sequelize.query(
        `SET search_path TO ${config.schema};
        CREATE OR REPLACE FUNCTION balance_check(NEW RECORD)
        RETURNS VOID AS
          $BODY$
          BEGIN
              RAISE EXCEPTION 'Insufficient balance on account id %s', NEW.id;
          END;
          $BODY$
        LANGUAGE plpgsql;
        CREATE OR REPLACE RULE balance_check_update AS ON UPDATE
            TO wallets WHERE (NEW.negative<>true AND NEW.balance < 0)
        DO ALSO SELECT balance_check(NEW);
        CREATE OR REPLACE RULE balance_check_insert AS ON INSERT
            TO wallets WHERE (NEW.negative<>true AND NEW.balance < 0)
        DO ALSO SELECT balance_check(NEW);`,
        { transaction: t }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.sequelize.query(
        `DROP INDEX IF EXISTS ${config.schema}.transactions_brn`,
        {
          transaction: t
        }
      );
      await queryInterface.sequelize.query(
        `SET search_path TO ${config.schema};
         DROP RULE balance_check_update ON wallets;
         DROP RULE balance_check_insert ON wallets;
         DROP function balance_check;`,
        { transaction: t }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transactions" },
        "data",
        { transaction: t }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "wallets" },
        "negative",
        { transaction: t }
      );
    });
  }
};
