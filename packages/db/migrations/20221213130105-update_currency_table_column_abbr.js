"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn(
      { schema: config.schema, tableName: "currency" },
      "abbr",
      {
        type: Sequelize.STRING(4),
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn(
      { schema: config.schema, tableName: "currency" },
      "abbr",
      {
        type: Sequelize.STRING(3),
      }
    );
  },
};
