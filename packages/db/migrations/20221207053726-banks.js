"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "banks",
      {
        ...common_fields,
        short_name: {
          type: Sequelize.STRING(20),
          allowNull: false
        },
        name: {
          type: Sequelize.STRING(50),
          allowNull: false
        },
        address: {
          type: Sequelize.STRING(150),
          allowNull: false
        },
        city: {
          type: Sequelize.STRING(50),
          allowNull: false
        },
        country: {
          type: Sequelize.STRING(2),
          allowNull: false
        },
      },
      {
        logger: console.log,
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "banks"
    });
  }
};
