"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "roles" },
        "type",
        {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: 0
        },
        { transaction: t }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "roles" },
        "account_id",
        {
          type: Sequelize.UUID
        },
        { transaction: t }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "roles" },
        "permissions",
        {
          type: Sequelize.JSON
        },
        { transaction: t }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "roles" },
        "type",
        {
          transaction: t
        }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "roles" },
        "account_id",
        {
          transaction: t
        }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "roles" },
        "permissions",
        {
          transaction: t
        }
      );
    });
  }
};
