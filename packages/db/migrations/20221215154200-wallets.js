"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "wallets",
      {
        ...common_fields,
        acc_label: {
          type: Sequelize.STRING(20),
          allowNull: false
        },
        acc_no: {
          type: Sequelize.STRING(40),
          allowNull: false
        },
        currency: {
          type: Sequelize.STRING(4),
          allowNull: false
        },
        balance: {
          type: Sequelize.DOUBLE,
          allowNull: false
        },
        balance_hash: {
          type: Sequelize.STRING(64),
          allowNull: false
        },
        ref_num: {
          type: Sequelize.STRING(12)
        },
        type: {
          type: Sequelize.ENUM("WALLET", "LEDGER", "COLLECTION", "FEE")
        },
        pid: {
          type: Sequelize.UUID
        },
        merchant_account_mapping_id: {
          type: Sequelize.UUID
        },
        bank_account_id: {
          type: Sequelize.UUID
        }
      },
      {
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "wallets"
    });
  }
};
