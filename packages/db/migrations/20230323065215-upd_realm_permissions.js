"use strict";
const extendPermissions = require("../config/bot_realm_permissions");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return extendPermissions(queryInterface);
  },

  down: (queryInterface, Sequelize) => {
    return extendPermissions(queryInterface);
  }
};
