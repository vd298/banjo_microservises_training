"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.sequelize.query(
        `drop index ${config.schema}.bank_accounts_fc_merchant_id`,
        {
          transaction: t
        }
      );
      await queryInterface.sequelize.query(
        `CREATE UNIQUE INDEX fc_merchant_id ON ${config.schema}.bank_accounts (fc_merchant_id)
          WHERE fc_merchant_id IS NOT NULL;`,
        {
          transaction: t
        }
      );
      // await queryInterface.addIndex(
      //   { schema: config.schema, tableName: "bank_accounts" },
      //   ["fc_merchant_id"],
      //   { unique: true, allowNull: true, transaction: t }
      // );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.sequelize.query(
        `drop index ${config.schema}.fc_merchant_id`,
        {
          transaction: t
        }
      );
      await queryInterface.addIndex(
        { schema: config.schema, tableName: "bank_accounts" },
        ["fc_merchant_id"],
        { unique: true, transaction: t }
      );
    });
  }
};
