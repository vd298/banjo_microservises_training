"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      { schema: config.schema, tableName: "users" },
      "user_type",
      {
        type: Sequelize.ENUM("API", "PORTAL")
      }
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      { schema: config.schema, tableName: "users" },
      "user_type"
    );
    await queryInterface.sequelize.query(
      `drop type if exists ${config.schema}.enum_users_user_type`
    );
  }
};
