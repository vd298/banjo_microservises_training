"use strict";

const { configureScope } = require("@sentry/node");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(
      `alter table if exists merchant_accounts_mapping rename to merchant_account_mappings`,
      {
        logger: console.log,
        schema: config.schema
      }
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(
      `alter table if exists merchant_account_mappings rename to merchant_accounts_mapping`,
      {
        logger: console.log,
        schema: config.schema
      }
    );
  }
};
