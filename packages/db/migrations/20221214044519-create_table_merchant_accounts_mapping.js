"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "merchant_account_mappings",
      {
        ...common_fields,
        merchant_account_id: {
          allowNull: false,
          type: Sequelize.UUID
        },
        bank_account: {
          allowNull: false,
          type: Sequelize.UUID
        },
        start_time: {
          allowNull: false,
          type: Sequelize.DATE
        },
        end_time: {
          allowNull: true,
          type: Sequelize.DATE
        }
      },
      {
        logger: console.log,
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "merchant_account_mappings"
    });
  },
};
