"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "payment_protocols",
      {
        ...common_fields,
        protocol_type: {
          type: Sequelize.ENUM( 
          'IMPS',
          'UPI',
          'NEFT',
          'RTGS',
          'WALLET',
          'IBAN',
          'CHAPS',
          'BACS',
          'SWIFT',
          'FP'
          ),
          allowNull: false
        },
        identifier_code: {
          type: Sequelize.STRING(15),
          allowNull: false
        },
        identifier_label: {
          type: Sequelize.STRING(20),
          allowNull: false
        },
        account_label: {
          type: Sequelize.STRING(20),
          allowNull: false
        },
        settings: {
          type: Sequelize.JSONB,      }
        },
      {
        logger: console.log,
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "payment_protocols"
    });
  }
};
