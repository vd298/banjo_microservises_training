"use strict";
const vw_resources_permissions = require("../views/vw_resources_permissions");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.sequelize.query(
        `UPDATE ${config.schema}.merchant_categories
          SET "name"='Xazur Pay'
          WHERE id='63170d7e-0456-11ee-be56-0242ac120002'::uuid;
          `,
        {
          transaction: t
        }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.sequelize.query(
        `UPDATE ${config.schema}.merchant_categories
          SET "name"='XazurPay'
          WHERE id='63170d7e-0456-11ee-be56-0242ac120002'::uuid;
        `,
        {
          transaction: t
        }
      );
    });
  }
};
