"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "exchanges",
      {
        ...common_fields,
        src_amount: {
          type: Sequelize.DOUBLE,
          allowNull: false
        },
        src_currency: {
          type: Sequelize.STRING(4),
          allowNull: false
        },
        dst_amount: {
          type: Sequelize.DOUBLE,
          allowNull: false
        },
        dst_currency: {
          type: Sequelize.STRING(4),
          allowNull: false
        },
        applied_rate: {
          type: Sequelize.DOUBLE,
          allowNull: false
        },
        quoted_rate: {
          type: Sequelize.DOUBLE
        },
        actual_rate: {
          type: Sequelize.DOUBLE
        },
        ledger_wallet_id: {
          type: Sequelize.UUID
        },
        data: {
          type: Sequelize.JSONB
        }
      },
      {
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "exchanges"
    });
  }
};
