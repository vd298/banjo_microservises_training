"use strict";

const common_fields = require("../config/utility/common_fields");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      `user_account_invitations`,
      {
        ...common_fields,
        email: {
          type: Sequelize.STRING(80)
        },
        account_id: {
          type: Sequelize.UUID
        },
        expiry: {
          type: Sequelize.DATE
        },
        accepted: {
          type: Sequelize.BOOLEAN,
          defaultValue: false
        },
        is_registered_user: { type: Sequelize.BOOLEAN },
        role: { type: Sequelize.UUID },
        token: { type: Sequelize.STRING(80) }
      },
      {
        schema: config.schema
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable({
      schema: config.schema,
      tableName: "user_account_invitations"
    });
  }
};
