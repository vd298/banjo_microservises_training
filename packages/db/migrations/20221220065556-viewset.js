"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.createTable(
          "viewset",
          {
            id: {
              allowNull: false,
              defaultValue: Sequelize.UUIDV4,
              primaryKey: true,
              type: Sequelize.UUID
            },
            name: {
              allowNull: false,
              type: Sequelize.STRING(50)
            },
            sql: {
              allowNull: false,
              type: Sequelize.TEXT
            }
          },
          {
            transaction: t,
            schema: config.schema
          }
        )
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.dropTable(
          {
            schema: config.schema,
            tableName: "viewset"
          },
          { transaction: t }
        )
      ]);
    });
  }
};
