"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          { schema: config.schema, tableName: "ref_limits" },
          "limit_type",
          {
            type: Sequelize.INTEGER,
            defaultValue: 0
          },
          { transaction: t }
        )
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn(
          { schema: config.schema, tableName: "ref_limits" },
          "limit_type",
          {
            transaction: t
          }
        )
      ]);
    });
  }
};
