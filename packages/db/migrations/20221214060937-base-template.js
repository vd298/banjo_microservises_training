"use stric";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.sequelize.query(
        `
        INSERT INTO ${config.schema}.letters (id,ctime,mtime,removed,maker,checker,remover,updater,rtime,signobject,realm,transporter,code,letter_name,from_email,to_email,subject,"text",html,"data",sms_gateway,to_cc,to_bcc,send_email,target_group,send_sms,send_push,to_mobile,sms_text,push_title,push_message,push_action,lang,status,category,sub_category) VALUES
	 ('be066e8d-2db7-4490-80d8-3375341504b9','2022-11-17 21:26:26.251+05:30','2022-11-17 21:26:26.251+05:30',0,'8ea6ef78-c4a3-11e9-aa8c-2a2ae2dbcce4',NULL,NULL,NULL,NULL,NULL,'9abd52bf-b1ac-4580-8e5a-9c486156b38e',NULL,'base-template','Base Template For all Emails',NULL,NULL,NULL,'','doctype transitional
head
  meta(http-equiv=''Content-Type'' content=''text/html; charset=utf-8'')
  meta(name=''viewport'' content=''width=device-width, initial-scale=1'')
  meta(name=''color-scheme'' content=''light dark'')
  meta(name=''supported-color-schemes'' content=''light dark'')
  link(rel=''stylesheet'' href=''https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'')
  link(href=''http://db.onlinewebfonts.com/c/03e852a9d1635cf25800b41001ee80c7?family=Trebuchet+MS'' rel=''stylesheet'' type=''text/css'')
  title Confirm your registration
  style(type=''text/css'').
    body {
    -webkit-font-smoothing:antialiased;
    -webkit-text-size-adjust:none;
    width: 100%;
    height: 100%;
    color: #202e55;
    font-weight: 400;
    font-size: 18px;
    }
    h1 {
    margin: 15px 0;
    font-weight: 900;
    font-size: 28px;
    line-height: 1.38;
    letter-spacing: 5.8px;
    }
    a {
    font-size: 16px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.13;
    letter-spacing: normal;
    text-align: left;
    color: #15c !important;
    text-decoration: none;
    min-width: 102px;
    display: inline-block;
    }
    a.soical-icon {
    min-width: 75px;
    }
    .force-full-width {
    width: 100% !important;
    }
    .force-width-80 {
    width: 80% !important;
    }
    .body-padding {
    padding: 0 75px;
    }
    .mobile-align {
    text-align: right;
    }
    @media (prefers-color-scheme: dark) {
    body {
    background-color: #F8F9FA !important;
    background-image: linear-gradient(#F8F9FA ,#F8F9FA) !important;
    }
    .dark {
    background-color: #F8F9FA !important;
    background-image: none !important;
    }
    h1, h2, p, span, a, b { color: #ffffff !important; }
    .link { color: #91ADD4 !important; }
    }
    @media (prefers-color-scheme: light) {
    body {
    background-color: #F8F9FA !important;
    background-image: radial-gradient(rgba(255, 255, 255, 0.32), #F8F9FA) !important;
    }
    .dark {
    background-color: #F8F9FA !important;
    background-image: radial-gradient(rgba(255, 255, 255, 0.32), #F8F9FA) !important;
    }
    }
  style(type=''text/css'' media=''screen'').
    @media screen {
    @import url(''https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;0,900;1,300;1,400;1,500;1,700&display=swap'');
    @import url(''http://db.onlinewebfonts.com/c/03e852a9d1635cf25800b41001ee80c7?family=Trebuchet+MS'');
    * {
    font-family: ''Trebuchet+MS'', sans-serif !important;
    }
    .w280 {
    width: 280px !important;
    }
    }
  style(type=''text/css'' media=''only screen and (max-width: 480px)'').
    /* Mobile styles */
    @media only screen and (max-width: 480px) {
    table[class*="w320"] {
    width: 320px !important; min-width: 600px;
    }
    td[class*="w320"] {
    width: 280px !important;
    padding-left: 20px !important;
    padding-right: 20px !important;
    }
    img[class*="w320"] {
    width: 120px !important;
    }
    td[class*="mobile-spacing"] {
    padding-top: 10px !important;
    padding-bottom: 10px !important;
    }
    *[class*="mobile-hide"] {
    display: none !important;
    }
    *[class*="mobile-br"] {
    font-size: 12px !important;
    }
    td[class*="mobile-w20"] {
    width: 20px !important;
    }
    img[class*="mobile-w20"] {
    width: 20px !important;
    }
    td[class*="mobile-center"] {
    text-align: center !important;
    }
    table[class*="w100p"] {
    width: 100% !important;
    }
    td[class*="activate-now"] {
    padding-right: 0 !important;
    padding-top: 20px !important;
    }
    td[class*="mobile-block"] {
    display: block !important;
    }
    td[class*="mobile-align"] {
    text-align: left !important;
    }
    table[class*="force-full-width"] {
    padding: 16px !important;
    }
    tr[class*="shadow-effect"] {
    display: none;
    }
    a {
    font-size: 14px;
    }
    a.soical-icon {
    padding: 0 10px;
    }
    .text-resize {
    font-size: 21px !important;
    letter-spacing: 2.8px !important;
    }
    .email-title h1 {
    font-size: 24px !important;
    letter-spacing: 1.8px !important;
    }
    a.soical-icon {
    min-width: auto;
    padding: 0 10px;
    }
    a.soical-icon img {
    height: 24px !important;
    }
    .link-list a {
    padding: 0 10px !important;
    }
    body {
    background-color: #F8F9FA !important;
    background-image: linear-gradient(#F8F9FA,#F8F9FA) !important;
    }
    .dark {
    background-color: #F8F9FA !important;
    background-image: linear-gradient(#F8F9FA,#F8F9FA) !important;
    }
    table[class*="body-background"] {
    background-size: contain;
    }
    td[class*="mobile-spacing-right"] {
    padding-right: 30px !important;
    }
    table[class*="footer-bg"] {
    background-size: cover !important;
    background-position: center bottom !important;
    }
    p[class*="full-width"] {
    width: 100% !important;
    }
    }
table(align=''center'' cellpadding=''0'' cellspacing=''0'' width=''100%'')
  tr
    td.dark(align=''center'' valign=''top'' bgcolor=''#F8F9FA'' style=''background-color:#F8F9FA;background-image: radial-gradient(rgba(255, 255, 255, 0.32), #F8F9FA); padding-bottom: 15px;'' width=''100%'')
      center
        table.w320(style=''z-index: 10; position: relative;'' cellspacing=''0'' cellpadding=''0'' width=''600'')
          tr
            td(align=''center'' valign=''top'')
              table(style=''margin:0 auto;'' cellspacing=''0'' cellpadding=''0'' width=''100%'')
                tr
                 block content
              table.force-full-width.footer-bg(cellspacing=''0'' cellpadding=''0'' bgcolor=''#ffffff'' style="margin: 0; background-color: #fff; width: 100%; padding: 10px 32px;")
                tr(style=''border-collapse: collapse;'')
                   td                    
                tr
                   td.mobile-spacing.mobile-spacing-right(style=''text-align: center; padding-top: 70px;'')
                     p(style=''margin-bottom:8px; margin-top: 0; color: #202e55;     font-weight: normal; text-align: left;  font-size: 18px;'') Thanks,
                tr
                   td.mobile-spacing.mobile-spacing-right(style=''text-align: center;'')
                     p(style=''margin-bottom:21px; margin-top: 0; color: #202e55;     font-weight: normal; text-align: left;   font-size: 18px;'') The Banjo Team<br><br>
   
              table(style=''padding-bottom:50px;'')                
                tr(style=''border-collapse: collapse;'')
                  td(style=''text-align: center; color: #c9cdd6;'')
              
          ','{}',NULL,NULL,NULL,false,'CUSTOMER',false,false,NULL,'','','',NULL,'en','CUSTOMER','','');

        
        
        `
      )
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.sequelize.query(
        ` 
        DELETE FROM ${config.schema}.letters
      WHERE id='be066e8d-2db7-4490-80d8-3375341504b9'::uuid;  
      `
      )
    ]);
  }
};
