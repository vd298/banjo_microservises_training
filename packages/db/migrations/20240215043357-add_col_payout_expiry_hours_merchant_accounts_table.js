"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "merchant_accounts" },
        "payout_expiry_hours",
        { type: Sequelize.DOUBLE, defaultValue: 24 },
        { transaction: t }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "merchant_accounts" },
        "payout_expiry_hours",
        {
          transaction: t
        }
      );
    });
  }
};
