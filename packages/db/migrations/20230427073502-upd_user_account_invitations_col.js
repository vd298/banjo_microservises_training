"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    let sql = `ALTER TABLE ${config.schema}.user_account_invitations ALTER COLUMN accepted SET DEFAULT NULL;`;
    await queryInterface.sequelize.query(sql);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.resolve();
  }
};
