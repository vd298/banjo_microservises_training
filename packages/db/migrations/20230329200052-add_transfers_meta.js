"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "transfers_meta",
      {
        ...common_fields,
        transfer_id: {
          type: Sequelize.UUID,
          allowNull: false
        },
        transfer_request: {
          type: Sequelize.JSONB,
          allowNull: true
        },
        transfer_response: {
          type: Sequelize.JSONB,
          allowNull: true
        }
      },
      {
        schema: config.schema
      }
    );
    await queryInterface.addIndex(
      `${config.schema}.transfers_meta`,
      ["transfer_id"],
      {},
      { schema: config.schema }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "transfers_meta"
    });
  }
};
