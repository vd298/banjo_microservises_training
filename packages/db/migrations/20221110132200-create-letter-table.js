"use strict";

const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      `letters`,
      {
        ...common_fields,
        realm: {
          type: Sequelize.UUID
        },
        transporter: {
          type: Sequelize.UUID
        },
        code: {
          type: Sequelize.STRING(30)
        },
        letter_name: {
          type: Sequelize.STRING(30)
        },
        from_email: {
          type: Sequelize.STRING
        },
        to_email: {
          type: Sequelize.STRING
        },
        subject: {
          type: Sequelize.STRING
        },
        text: {
          type: Sequelize.TEXT
        },
        html: {
          type: Sequelize.TEXT
        },
        data: {
          type: Sequelize.TEXT
        },
        sms_gateway: {
          type: Sequelize.STRING(10)
        },
        to_cc: {
          type: Sequelize.STRING(1000)
        },
        to_bcc: {
          type: Sequelize.STRING(1000)
        },
        send_email: {
          type: Sequelize.BOOLEAN
        },
        target_group: {
          type: Sequelize.ENUM("CUSTOMER", "AGENT")
        },
        send_sms: {
          type: Sequelize.BOOLEAN
        },
        send_push: {
          type: Sequelize.BOOLEAN
        },
        to_mobile: {
          type: Sequelize.STRING(255)
        },
        sms_text: {
          type: Sequelize.STRING(255)
        },
        push_title: {
          type: Sequelize.STRING(255)
        },
        push_message: {
          type: Sequelize.STRING(255)
        },
        push_action: {
          type: Sequelize.STRING(255)
        },
        lang: {
          type: Sequelize.STRING(2)
        },
        status: {
          type: Sequelize.ENUM("CUSTOMER", "AGENT"),
          defaultValue: "CUSTOMER"
        },
        category: {
          type: Sequelize.STRING(50)
        },
        sub_category: {
          type: Sequelize.STRING(50)
        }
      },
      {
        logger: console.log,
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "letters"
    });
  }
};
