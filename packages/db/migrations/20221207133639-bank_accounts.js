"use strict";
const common_fields = require("../config/utility/common_fields");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      "bank_accounts",
      {
        ...common_fields,
        payment_type: {
          type: Sequelize.UUID,
          allowNull: false
        },
        bank_id: {
          type: Sequelize.UUID,
          allowNull: false
        },
        account_name: {
          type: Sequelize.STRING(50),
          allowNull: false
        },
        acc_no: {
          type: Sequelize.STRING(40),
          allowNull: false
        },
        currency: {
          type: Sequelize.STRING(4)
        },
        payment_provider_id: {
          type: Sequelize.UUID
        }
      },
      {
        logger: console.log,
        schema: config.schema
      }
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable({
      schema: config.schema,
      tableName: "bank_accounts"
    });
  }
};
