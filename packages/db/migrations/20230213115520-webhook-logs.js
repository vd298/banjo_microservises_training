"use strict";
const dependedView = require("../views/vw_user_accounts");
const common_fields = require("../config/utility/common_fields");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "users" },
        "password",
        {
          type: Sequelize.STRING(500)
        },
        { transaction: t }
      );
      await queryInterface.createTable(
        `tx_webhooks`,
        {
          ...common_fields,
          user_id: {
            type: Sequelize.UUID,
            allowNull: false
          },
          transaction_id: {
            type: Sequelize.UUID,
            allowNull: false
          },
          logs: {
            type: Sequelize.JSONB
          },
          counter: {
            type: Sequelize.INTEGER,
            defaultValue: 0
          },
          status: {
            type: Sequelize.INTEGER,
            defaultValue: 0
          }
        },
        {
          schema: config.schema
        }
      );
      await queryInterface.addIndex(
        `${config.schema}.tx_webhooks`,
        ["user_id", "transaction_id"],
        {
          unique: true
        },
        { schema: config.schema }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "transfers" },
        "user_id",
        { type: Sequelize.UUID },
        { transaction: t }
      );
    });
  },
  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.changeColumn(
        { schema: config.schema, tableName: "users" },
        "password",
        {
          type: Sequelize.STRING(150)
        },
        { transaction: t }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "transfers" },
        "user_id",
        { transaction: t }
      );
      await queryInterface.dropTable({
        schema: config.schema,
        tableName: "tx_webhooks"
      });
    });
  }
};
