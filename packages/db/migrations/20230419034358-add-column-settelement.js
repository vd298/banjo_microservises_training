"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "settlements" },
        "exchange_account_id",
        {
          type: Sequelize.UUID
        },
        { transaction: t }
      );
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "settlements" },
        "settlement_data",
        {
          type: Sequelize.JSON
        },
        { transaction: t }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "settlements" },
        "exchange_account_id",
        {
          transaction: t
        }
      );
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "settlements" },
        "settlement_data",
        {
          transaction: t
        }
      );
    });
  }
};
