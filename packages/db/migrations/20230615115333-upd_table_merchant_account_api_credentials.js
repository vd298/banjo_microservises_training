"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.addColumn(
        { schema: config.schema, tableName: "merchant_accounts" },
        "api_credential_id",
        {
          type: Sequelize.UUID
        },
        { transaction: t }
      );
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(async (t) => {
      await queryInterface.removeColumn(
        { schema: config.schema, tableName: "merchant_accounts" },
        "api_credential_id",
        {
          transaction: t
        }
      );
    });
  }
};
