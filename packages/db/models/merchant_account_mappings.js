"use strict";
// const Sequelize = require("sequelize-hierarchy")();
const { common_fields, schema } = require("../config/utility");
module.exports = (sequelize, Sequelize, sequelizeHierarchy) => {
  const merchant_account_mappings = sequelize.define(
    "merchant_account_mappings",
    {
      ...common_fields,
      merchant_account_id: {
        allowNull: false,
        type: Sequelize.UUID
      },
      bank_account: {
        allowNull: false,
        type: Sequelize.UUID
      },
      start_time: {
        allowNull: false,
        type: Sequelize.DATE
      },
      end_time: {
        allowNull: true,
        type: Sequelize.DATE
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  merchant_account_mappings.associate = function(models) {};
  return merchant_account_mappings;
};
