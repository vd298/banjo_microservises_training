"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, Sequelize) => {
  const model = sequelize.define(
    "transfer",
    {
      ...common_fields,
      src_amount: {
        type: Sequelize.DOUBLE,
        allowNull: false
      },
      src_currency: {
        type: Sequelize.STRING(4),
        allowNull: false
      },
      dst_amount: {
        type: Sequelize.DOUBLE,
        allowNull: false
      },
      dst_currency: {
        type: Sequelize.STRING(4),
        allowNull: false
      },
      event_name: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      transfer_type: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      ref_num: {
        type: Sequelize.STRING(20),
        allowNull: false,
        unique: true
      },
      counterpart_id: {
        type: Sequelize.UUID,
        allowNull: true
      },
      is_exchange: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      exchange_id: {
        type: Sequelize.UUID
      },
      data: {
        type: Sequelize.JSONB
      },
      account_id: {
        type: Sequelize.UUID
      },
      merchant_account_id: {
        type: Sequelize.UUID
      },
      order_num: {
        type: Sequelize.STRING(40)
      },
      description: {
        type: Sequelize.STRING(60)
      },
      src_wallet_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      dst_wallet_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      user_id: {
        type: Sequelize.UUID
      },
      settlement_id: {
        type: Sequelize.UUID
      },
      beneficiary_id: {
        type: Sequelize.UUID
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  model.associate = function(models) {
    // associations can be defined here
  };
  model.TRANSFER_TYPE = {
    PAYOUT: "PAYOUT",
    PAYIN: "PAYIN",
    COLLECTION: "COLLECTION",
    REFUND: "REFUND",
    RETURN: "RETURN",
    REVERSE: "REVERSE",
    REVERSE: "TRANSFER",
    SETTLEMENT: "SETTLEMENT",
    DIRECT_DEPOSIT: "DIRECT_DEPOSIT"
  };
  model.TRANSFER_STATUSES = {
    REQUEST: "REQUEST",
    BRN_SUBMIT: "BRN_SUBMIT",
    APPROVED: "APPROVED",
    REJECTED: "REJECTED",
    VPA_SUBMIT: "VPA_SUBMIT",
    PENDING: "PENDING",
    REVERSE: "REVERSE",
    SETTLEMENT: "SETTLEMENT_APPROVED",
    EXCHANGE_WITHDRAW: "EXCHANGE_WITHDRAW",
    INTERNAL_FUNDS_SHIFT: "INTERNAL_FUNDS_SHIFT"
  };
  model.WEBHOOK_UPDATES = ["APPROVED", "REJECTED", "SETTLEMENT_APPROVED"];
  model.adminModelName = "Crm.modules.Transfers.model.TransfersModel";
  return model;
};
