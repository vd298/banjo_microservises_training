"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, Sequelize) => {
  const model = sequelize.define(
    "bank_accounts",
    {
      ...common_fields,
      bank_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      account_name: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      acc_no: {
        type: Sequelize.STRING(100),
        allowNull: false
      },
      currency: {
        type: Sequelize.STRING(4)
      },
      payment_provider_id: {
        type: Sequelize.UUID
      },
      status: {
        type: Sequelize.ENUM("ACTIVE", "INACTIVE"),
        allowNull: false,
        defaultValue: "ACTIVE"
      },
      request_timestamp: {
        type: Sequelize.DATE
      },
      is_crypto: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      is_verified: {
        type: Sequelize.INTEGER
      }    
    },
    {
      createdAt: false,
      updatedAt: false,
      deletedAt: false,
      freezeTableName: true
    },
    schema
  );
  model.associate = function(models) {
    // associations can be defined here
  };
  model.STATUS = {
    ACTIVE: "ACTIVE",
    INACTIVE: "INACTIVE"
  };
  model.adminModelName = "Crm.modules.bankAccounts.model.BankAccountsModel";
  return model;
};
