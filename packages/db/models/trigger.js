"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, DataTypes) => {
  const server = sequelize.define(
    "trigger",
    {
      ...common_fields,
      name: DataTypes.STRING,
      service: DataTypes.STRING,
      method: DataTypes.STRING,
      cron: DataTypes.STRING,
      ttype: DataTypes.INTEGER,
      tablename: DataTypes.STRING,
      conditions: DataTypes.STRING,
      data: DataTypes.JSON,
      realm_id: {
        type: DataTypes.UUID
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  server.associate = function(models) {
    // associations can be defined here
  };
  return server;
};
