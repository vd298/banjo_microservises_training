"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, Sequelize) => {
  const model = sequelize.define(
    "transfers_meta",
    {
      ...common_fields,
      transfer_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      transfer_request: {
        type: Sequelize.JSONB,
        allowNull: false
      },
      transfer_response: {
        type: Sequelize.JSONB,
        allowNull: false
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  return model;
};
