"use strict";
// const Sequelize = require("sequelize-hierarchy")();
const { common_fields, schema } = require("../config/utility");
module.exports = (sequelize, Sequelize, sequelizeHierarchy) => {
  const api_credentials = sequelize.define(
    "api_credentials",
    {
      ...common_fields,
      name: {
        type: Sequelize.STRING(50)
      },
      aggregator_id: {
        type: Sequelize.STRING(50)
      },
      secret_key: {
        type: Sequelize.STRING(250)
      },
      account_id: {
        type: Sequelize.UUID
      },
      merchant_account_id: {
        type: Sequelize.UUID
      },
      category: {
        type: Sequelize.UUID
      },
      bank: {
        type: Sequelize.UUID
      },
      bank_account: {
        type: Sequelize.UUID
      },
      currency: {
        type: Sequelize.STRING(4)
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  return api_credentials;
};
