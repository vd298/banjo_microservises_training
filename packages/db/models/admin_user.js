"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, DataTypes) => {
  const admin_user = sequelize.define(
    "admin_user",
    {
      _id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true
      },
      login: DataTypes.STRING(30),
      pass: DataTypes.STRING(100),
      status: DataTypes.STRING(10),
      name: DataTypes.STRING(50),
      superuser: DataTypes.INTEGER,
      ctime: DataTypes.DATE,
      mtime: DataTypes.DATE,
      signobject: DataTypes.JSON,
      removed: DataTypes.INTEGER,
      checker: DataTypes.UUID,
      maker: DataTypes.UUID,
      updater: DataTypes.UUID,
      remover: DataTypes.UUID,
      rtime: DataTypes.UUID,
      realm_id: {
        type: DataTypes.UUID
      }
    },
    {
      id: "_id",
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },

    schema
  );
  admin_user.associate = function(models) {
    // associations can be defined here
  };
  admin_user.STATUS = {
    ACTIVE: "ACTIVE",
    INACTIVE: "INACTIVE",
    BLOCKED: "BLOCKED"
  };
  return admin_user;
};
