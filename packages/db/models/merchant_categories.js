"use strict";
// const Sequelize = require("sequelize-hierarchy")();
const { common_fields, schema } = require("../config/utility");
module.exports = (sequelize, Sequelize, sequelizeHierarchy) => {
  const merchant_categories = sequelize.define(
    "merchant_categories",
    {
      ...common_fields,
      name: {
        type: Sequelize.STRING(50),
        allowNull: false
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );

  return merchant_categories;
};
