"use strict";
const { common_fields, schema } = require("../config/utility");
module.exports = (sequelize, Sequelize) => {
  const userAccount = sequelize.define(
    "user_accounts",
    {
      ...common_fields,
      account_id: {
        type: Sequelize.UUID
      },
      user_id: {
        type: Sequelize.UUID
      },
      main_role: {
        type: Sequelize.UUID
      },
      additional_roles: {
        type: Sequelize.JSON
      },
      start_date: { type: Sequelize.DATE },
      end_date: { type: Sequelize.DATE },
      status: {
        type: Sequelize.ENUM("ACTIVE", "INACTIVE", "BLOCKED")
      },
      merchant_account_id: {
        type: Sequelize.UUID
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  userAccount.associate = function(models) {
    userAccount.belongsTo(models.role, {
      as: "role",
      foreignKey: "main_role",
      targetKey: "id"
    });
  };
  return userAccount;
};
