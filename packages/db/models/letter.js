"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, DataTypes) => {
  const letter = sequelize.define(
    "letter",
    {
      ...common_fields,
      realm: {
        type: DataTypes.UUID
      },
      transporter: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "transporters",
          key: "id"
        },
        //onUpdate: "cascade",
        onDelete: "cascade"
      },
      lang: DataTypes.STRING(2),
      code: DataTypes.STRING(30),
      letter_name: DataTypes.STRING,
      from_email: DataTypes.STRING,
      to_email: DataTypes.STRING,
      subject: DataTypes.STRING,
      text: DataTypes.TEXT,
      html: DataTypes.TEXT,
      data: DataTypes.TEXT,
      sms_gateway: DataTypes.STRING(10),
      to_cc: DataTypes.STRING(1000),
      to_bcc: DataTypes.STRING(1000),
      send_email: DataTypes.BOOLEAN,
      send_sms: DataTypes.BOOLEAN,
      send_push: DataTypes.BOOLEAN,
      to_mobile: DataTypes.STRING(255),
      sms_text: DataTypes.STRING(255),
      push_title: DataTypes.STRING(200),
      push_message: DataTypes.STRING(255),
      push_action: DataTypes.STRING(30),
      target_group: {
        type: DataTypes.ENUM("CUSTOMER", "AGENT"),
        defaultValue: "CUSTOMER"
      },
      category: DataTypes.STRING(50),
      sub_category: DataTypes.STRING(50),
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  letter.associate = function(models) {
    // associations can be defined here
  };
  return letter;
};
