"use strict";
const { common_fields, schema } = require("../config/utility");
module.exports = (sequelize, DataTypes) => {
  const provider = sequelize.define(
    "files",
    {
      ...common_fields,
      code: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true
      },
      filename: DataTypes.STRING,
      file_size: DataTypes.INTEGER,
      mime_type: DataTypes.STRING,
      upload_date: DataTypes.DATE,
      storage_date: DataTypes.DATE,
      ctime: {
        allowNull: false,
        type: DataTypes.DATE
      },
      mtime: {
        allowNull: false,
        type: DataTypes.DATE
      },
      removed: DataTypes.INTEGER,
      owner: {
        type: DataTypes.UUID
      },
      realm_id: {
        type: DataTypes.UUID
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  provider.associate = function(models) {
    // associations can be defined here
  };
  return provider;
};
