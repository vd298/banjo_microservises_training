"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, Sequelize) => {
  const model = sequelize.define(
    "tx_webhooks",
    {
      ...common_fields,
      user_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      transaction_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      logs: {
        type: Sequelize.JSONB
      },
      counter: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      status: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  model.associate = function(models) {
    // associations can be defined here
  };
  model.STATUS = { CREATED: 0, COMPLETED: 1, FAILED: -1 };
  model.TYPE = { COLLECTION_UPDATES: "COLLECTION_UPDATES" };
  return model;
};
