"use strict";
// const Sequelize = require("sequelize-hierarchy")();
const { common_fields, schema } = require("../config/utility");
module.exports = (sequelize, Sequelize, sequelizeHierarchy) => {
  const account = sequelize.define(
    "account",
    {
      ...common_fields,
      name: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      address: {
        type: Sequelize.STRING(150)
      },
      city: {
        type: Sequelize.STRING(50)
      },
      country: {
        type: Sequelize.STRING(2),
        allowNull: false
      },
      legal_entity: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false
      },
      registration_no: {
        type: Sequelize.STRING(30)
      },
      plan_id: {
        type: Sequelize.UUID
      },
      type: {
        type: Sequelize.ENUM("MERCHANT"),
        defaultValue: "MERCHANT",
        allowNull: false
      },
      status: {
        type: Sequelize.ENUM("ACTIVE", "INACTIVE", "BLOCKED"),
        allowNull: false
      },
      realm_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      variables: {
        type: Sequelize.JSON
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  // account.isHierarchy();
  account.associate = function(models) {
    // hasMany(db.meal, { as: "Food", foreignKey: "idFood" });
    account.belongsToMany(models.user, {
      as: "users",
      foreignKey: "account_id",
      through: {
        model: models.user_accounts,
        unique: true
      }
    });
  };
  account.STATUS = {
    ACTIVE: "ACTIVE",
    INACTIVE: "INACTIVE",
    BLOCKED: "BLOCKED"
  };
  return account;
};
