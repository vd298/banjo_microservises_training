"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, DataTypes) => {
  const currency = sequelize.define(
    "currency",
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true
      },
      code: DataTypes.INTEGER,
      name: DataTypes.STRING(50),
      abbr: DataTypes.STRING(5),
      crypto: DataTypes.BOOLEAN
    },
    {
      createdAt: false,
      updatedAt: false,
      deletedAt: false,
      freezeTableName: true
    },
    schema
  );
  currency.associate = function(models) {
    // associations can be defined here
  };
  return currency;
};
