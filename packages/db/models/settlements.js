"use strict";
// const Sequelize = require("sequelize-hierarchy")();
const { common_fields, schema } = require("../config/utility");
module.exports = (sequelize, Sequelize, sequelizeHierarchy) => {
  const settlement = sequelize.define(
    "settlements",
    {
      ...common_fields,
      src_currency: {
        type: Sequelize.STRING(4),
        allowNull: false
      },
      dst_currency: {
        type: Sequelize.STRING(4),
        allowNull: false
      },
      src_amount: {
        type: Sequelize.DOUBLE,
        allowNull: false,
        defaultValue: 0
      },
      dst_amount: {
        type: Sequelize.DOUBLE,
        allowNull: false,
        defaultValue: 0
      },
      markup_rate: {
        defaultValue: 0,
        type: Sequelize.DOUBLE
      },
      conversion_rate: {
        defaultValue: 0,
        type: Sequelize.DOUBLE
      },
      status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 2 // Status 2 means in progress
      },
      merchant_account_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      exchange_account_id: {
        type: Sequelize.UUID
      },
      settlement_data: {
        type: Sequelize.JSON
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );

  return settlement;
};
