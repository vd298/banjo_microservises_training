"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, Sequelize) => {
  const exchange = sequelize.define(
    "exchange",
    {
      ...common_fields,
      src_amount: {
        type: Sequelize.DOUBLE,
        allowNull: false
      },
      src_currency: {
        type: Sequelize.STRING(4),
        allowNull: false
      },
      dst_amount: {
        type: Sequelize.DOUBLE,
        allowNull: false
      },
      dst_currency: {
        type: Sequelize.STRING(4),
        allowNull: false
      },
      applied_rate: {
        type: Sequelize.DOUBLE,
        allowNull: false
      },
      quoted_rate: {
        type: Sequelize.DOUBLE
      },
      actual_rate: {
        type: Sequelize.DOUBLE
      },
      ledger_wallet_id: {
        type: Sequelize.UUID
      },
      data: {
        type: Sequelize.JSONB
      },
      realm_id: {
        type: Sequelize.UUID
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  exchange.associate = function(exchange) {
    // associations can be defined here
  };
  return exchange;
};
