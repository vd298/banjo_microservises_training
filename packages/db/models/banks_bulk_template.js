"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, Sequelize) => {
  const banks_bulk_templates = sequelize.define(
    "banks_bulk_templates",
    {
      ...common_fields,
      bank_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      protocol_id: {
        type: Sequelize.UUID
      },
      description: {
        type: Sequelize.STRING(50)
      },
      template: {
        type: Sequelize.JSONB,
        allowNull: false
      },
      template_name: {
        type: Sequelize.STRING(50),
        allowNull: false
      }
    },
    {
      createdAt: false,
      updatedAt: false,
      deletedAt: false
    },
    schema
  );
  banks_bulk_templates.associate = function(banks_bulk_templates) {
    // associations can be defined here
  };
  return banks_bulk_templates;
};
