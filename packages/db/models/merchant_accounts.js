"use strict";
// const Sequelize = require("sequelize-hierarchy")();
const { common_fields, schema } = require("../config/utility");
module.exports = (sequelize, Sequelize, sequelizeHierarchy) => {
  const merchant_accounts = sequelize.define(
    "merchant_accounts",
    {
      ...common_fields,
      account_id: {
        type: Sequelize.UUID
      },
      name: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      acc_no: {
        type: Sequelize.STRING(16)
      },
      currency: {
        type: Sequelize.STRING(4)
      },
      settlement_currency: {
        type: Sequelize.STRING(4)
      },
      category: {
        type: Sequelize.UUID
      },
      status: {
        type: Sequelize.ENUM("ACTIVE", "INACTIVE", "BLOCKED"),
        allowNull: false
      },
      auto_payouts: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      api_credential_id: {
        type: Sequelize.UUID
      },
      expiry_hours: {
        type: Sequelize.DOUBLE,
        defaultValue: 24
      },
      payout_expiry_hours: {
        type: Sequelize.DOUBLE,
        defaultValue: 24
      },
      payment_page_theme: {
        type: Sequelize.STRING(50),
        defaultValue: "v2"
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  // account.isHierarchy();
  merchant_accounts.associate = function(models) {
    // hasMany(db.meal, { as: "Food", foreignKey: "idFood" });
    // merchant_accounts.belongsToMany(models.user, {
    //   as: "users",
    //   foreignKey: "account_id",
    //   through: {
    //     model: models.user_accounts,
    //     unique: true
    //   }
    // });
  };
  merchant_accounts.STATUS = {
    ACTIVE: "ACTIVE",
    INACTIVE: "INACTIVE",
    BLOCKED: "BLOCKED"
  };
  return merchant_accounts;
};
