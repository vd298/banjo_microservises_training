"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, Sequelize) => {
  const model = sequelize.define(
    "payment_providers",
    {
      ...common_fields,
      name: {
        type: Sequelize.STRING(50)
      },
      status: {
        type: Sequelize.ENUM("ACTIVE", "INACTIVE")
      },
      realm_id: {
        type: Sequelize.UUID
      }
    },
    {
      createdAt: false,
      updatedAt: false,
      deletedAt: false,
      freezeTableName: true
    },
    schema
  );
  model.associate = function(models) {
    // associations can be defined here
  };
  return model;
};
