"use strict";
const { common_fields, schema } = require("../config/utility");
module.exports = (sequelize, Sequelize) => {
  const permission = sequelize.define(
    "permission",
    {
      ...common_fields,
      resource_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      role_id: {
        type: Sequelize.UUID
      },
      user_id: {
        type: Sequelize.UUID
      },
      allow: {
        type: Sequelize.BOOLEAN
      },
      deny: {
        type: Sequelize.BOOLEAN
      },
      write: {
        type: Sequelize.BOOLEAN
      },
      realm_id: {
        type: Sequelize.UUID
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );

  return permission;
};
