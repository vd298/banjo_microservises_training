"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, Sequelize) => {
  const lost_transactions = sequelize.define(
    "lost_transactions",
    {
      ...common_fields,
      transfer_id: {
        type: Sequelize.UUID
      },
      amount: {
        type: Sequelize.DOUBLE
      },
      brn: {
        type: Sequelize.STRING(200)
      },
      bank_name: {
        type: Sequelize.STRING(50)
      },
      domain_name: {
        type: Sequelize.STRING(100)
      },
      bank_account_id: {
        type: Sequelize.UUID
      },
      acc_no: {
        type: Sequelize.STRING(40)
      },
      transaction_date: {
        type: Sequelize.DATE
      },
      processed: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0 // processed 0 means not processed yet
      },
      account_id: {
        type: Sequelize.UUID
      },
      merchant_account_id: {
        type: Sequelize.UUID
      },
      admin_id: {
        type: Sequelize.UUID
      },
      transaction_data: {
        type: Sequelize.JSONB
      },
      note: {
        type: Sequelize.STRING(255)
      }
    },
    {
      createdAt: false,
      updatedAt: false,
      deletedAt: false
    },
    schema
  );
  lost_transactions.associate = function(lost_transactions) {
    // associations can be defined here
  };
  return lost_transactions;
};
