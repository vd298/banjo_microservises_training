"use strict";
const { common_fields, schema } = require("../config/utility");
module.exports = (sequelize, Sequelize) => {
  const user = sequelize.define(
    "user",
    {
      ...common_fields,
      first_name: {
        type: Sequelize.STRING(50)
      },
      last_name: {
        type: Sequelize.STRING(50)
      },
      email: {
        type: Sequelize.STRING(50)
      },
      mobile: {
        type: Sequelize.STRING(15)
      },
      // API KEY will be stored when user type is 'API'
      username: {
        type: Sequelize.STRING(20)
      },
      // Public key will be stored in this field when user type is 'API'
      password: {
        type: Sequelize.STRING(500)
      },
      mobile_verified: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      email_verified: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      settings: {
        type: Sequelize.JSONB
      },
      status: {
        type: Sequelize.ENUM("ACTIVE", "INACTIVE", "BLOCKED")
      },
      profile_pic: {
        type: Sequelize.STRING(100)
      },
      timezone: { type: Sequelize.STRING(50) },
      user_type: {
        allowNull: false,
        type: Sequelize.ENUM("API", "PORTAL")
      },
      ips: {
        type: Sequelize.JSONB
      },
      webhook_url: {
        type: Sequelize.STRING(255)
      },
      telegram_id: {
        type: Sequelize.STRING(200)
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  user.associate = function(models) {
    user.belongsToMany(models.account, {
      as: "accounts",
      through: { model: models.user_accounts, unique: true },
      foreignKey: "user_id"
    });
  };
  user.STATUS = {
    ACTIVE: "ACTIVE",
    INACTIVE: "INACTIVE",
    BLOCKED: "BLOCKED"
  };
  user.USER_TYPE = {
    API: "API",
    PORTAL: "PORTAL"
  };
  return user;
};
