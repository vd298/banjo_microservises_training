"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, Sequelize) => {
  const beneficiaries = sequelize.define(
    "beneficiaries",
    {
      ...common_fields,
      account_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      merchant_account_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      protocols: {
        type: Sequelize.ARRAY(Sequelize.UUID),
        allowNull: false
      },
      first_name: {
        type: Sequelize.STRING(100),
        allowNull: false
      },
      last_name: {
        type: Sequelize.STRING(100),
        allowNull: false
      },
      email: {
        type: Sequelize.STRING(50)
      },
      mobile: {
        type: Sequelize.STRING(15)
      },
      acc_no: {
        type: Sequelize.STRING(100)
      },
      ifsc: {
        type: Sequelize.STRING(15)
      },
      bank_name: {
        type: Sequelize.STRING(300)
      },
      network_type: {
        type: Sequelize.STRING(50)
      },
      crypto_wallet_address: {
        type: Sequelize.STRING(50)
      },
      description: {
        type: Sequelize.STRING(300)
      },
      vpa: {
        type: Sequelize.STRING(50)
      },
      country_code: {
        type: Sequelize.STRING(10)
      }
    },
    {
      createdAt: false,
      updatedAt: false,
      deletedAt: false
    },
    schema
  );
  beneficiaries.associate = function(beneficiaries) {
    // associations can be defined here
  };
  return beneficiaries;
};
