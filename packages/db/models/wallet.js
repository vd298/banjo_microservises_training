"use strict";
const crypto = require("crypto");
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, Sequelize) => {
  const wallet = sequelize.define(
    "wallet",
    {
      ...common_fields,
      acc_label: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      acc_no: {
        type: Sequelize.STRING(40),
        allowNull: false
      },
      currency: {
        type: Sequelize.STRING(4),
        allowNull: false
      },
      balance: {
        type: Sequelize.DOUBLE,
        allowNull: false
      },
      balance_hash: {
        type: Sequelize.STRING(64),
        allowNull: false
      },
      ref_num: {
        type: Sequelize.STRING(12)
      },
      type: {
        type: Sequelize.ENUM("WALLET", "LEDGER", "COLLECTION", "FEE")
      },
      pid: {
        type: Sequelize.UUID
      },
      merchant_account_mapping_id: {
        type: Sequelize.UUID
      },
      bank_account_id: {
        type: Sequelize.UUID
      },
      negative: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  wallet.associate = function(models) {
    // associations can be defined here
  };
  wallet.generateHash = function(string) {
    const stringHashed = crypto
      .createHash("sha256")
      .update(string)
      .digest("hex");
    return stringHashed;
  };
  wallet.beforeUpdate((walletInfo, options) => {
    if (walletInfo && walletInfo.id && !isNaN(walletInfo.balance)) {
      walletInfo.balance_hash = wallet.generateHash(
        walletInfo.id + walletInfo.balance
      );
    }
  });
  wallet.TYPES = {
    WALLET: "WALLET",
    LEDGER: "LEDGER",
    COLLECTION: "COLLECTION",
    FEE: "FEE"
  };
  return wallet;
};
