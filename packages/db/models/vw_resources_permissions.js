"use strict";
module.exports = (sequelize, DataTypes) => {
  const vw_resources_permissions = sequelize.define(
    "vw_resources_permissions",
    {
      id: { type: DataTypes.UUID, primaryKey: true },
      resource_id: DataTypes.UUID,
      role_id: DataTypes.UUID,
      user_id: DataTypes.UUID,
      allow: DataTypes.BOOLEAN,
      deny: DataTypes.BOOLEAN,
      write: DataTypes.BOOLEAN,
      identifier: DataTypes.STRING
    },
    {
      createdAt: false,
      updatedAt: false,
      deletedAt: false
    }
  );
  return vw_resources_permissions;
};
