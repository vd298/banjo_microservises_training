"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, Sequelize) => {
  const model = sequelize.define(
    "bank_account_activity_logs",
    {
      ...common_fields,
      bank_account_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      note: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      old_status: {
        type: Sequelize.ENUM("ACTIVE", "INACTIVE"),
        allowNull: false
      },
      new_status: {
        type: Sequelize.ENUM("ACTIVE", "INACTIVE"),
        allowNull: false
      }
    },
    {
      createdAt: false,
      updatedAt: false,
      deletedAt: false
    },
    schema
  );
  model.associate = function(models) {
    // associations can be defined here
  };
  return model;
};
