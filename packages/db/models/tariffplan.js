"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, DataTypes) => {
  const tariffplan = sequelize.define(
    "tariffplan",
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true
      },
      name: DataTypes.STRING,
      description: DataTypes.TEXT,
      tariffs: DataTypes.JSON,
      variables: DataTypes.JSON,
      active: DataTypes.CHAR,
      user_type: DataTypes.UUID,
      category_type: DataTypes.UUID,
      plan_code: DataTypes.STRING,
      ctime: {
        allowNull: false,
        type: DataTypes.DATE
      },
      mtime: {
        allowNull: false,
        type: DataTypes.DATE
      },
      removed: DataTypes.INTEGER,
      signobject: DataTypes.JSON,
      maker: DataTypes.UUID,
      realm_id: {
        type: DataTypes.UUID
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  tariffplan.associate = function(models) {
    // associations can be defined here
  };
  return tariffplan;
};
