"use strict";
// const Sequelize = require("sequelize-hierarchy")();
const { common_fields, schema } = require("../config/utility");
module.exports = (sequelize, Sequelize, sequelizeHierarchy) => {
  const ref_limits = sequelize.define(
    "ref_limits",
    {
      ...common_fields,
      protocol_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      currency: {
        type: Sequelize.STRING(4),
        allowNull: false
      },
      min_amount: {
        type: Sequelize.DOUBLE,
        defaultValue: 0
      },
      max_amount: {
        type: Sequelize.DOUBLE,
        defaultValue: 1000000000
      },
      bank_account_protocol_id: {
        type: Sequelize.UUID,
        allowNull: true
      },
      limit_type: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      daily_deposit: {
        type: Sequelize.DOUBLE,
        defaultValue: 0
      },
      realm_id: {
        type: Sequelize.UUID
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );

  ref_limits.LIMIT_TYPES = {
    DEPOSIT: 0,
    WITHDRAW: 1
  };

  return ref_limits;
};
