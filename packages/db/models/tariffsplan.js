"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, DataTypes) => {
  const server = sequelize.define(
    "tariffsplan",
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true
      },
      name: DataTypes.STRING,
      description: DataTypes.TEXT,
      tariffs: DataTypes.JSON,
      variables: DataTypes.JSON,
      active: DataTypes.CHAR,
      ctime: {
        allowNull: false,
        type: DataTypes.DATE
      },
      mtime: {
        allowNull: false,
        type: DataTypes.DATE
      },

      stime: DataTypes.BIGINT,
      ltime: DataTypes.BIGINT,
      removed: DataTypes.INTEGER,
      signobject: DataTypes.JSON,
      maker: DataTypes.UUID,
      realm_id: {
        type: DataTypes.UUID
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  server.associate = function(models) {
    // associations can be defined here
  };
  return server;
};
