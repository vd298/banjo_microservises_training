"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, Sequelize) => {
  const model = sequelize.define(
    "banks",
    {
      ...common_fields,
      short_name: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      name: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      address: {
        type: Sequelize.STRING(150),
        allowNull: false
      },
      city: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      country: {
        type: Sequelize.STRING(2),
        allowNull: false
      },
      bank_type: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      realm_id: {
        type: Sequelize.UUID
      }
    },
    {
      createdAt: false,
      updatedAt: false,
      deletedAt: false,
      freezeTableName: true
    },
    schema
  );

  model.TYPE = [{ 0: "bank" }, { 1: "exchange" }, { 2: "acquirer" }];

  model.associate = function(models) {
    // associations can be defined here
  };
  return model;
};
