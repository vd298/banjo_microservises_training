"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, DataTypes) => {
  const realm = sequelize.define(
    "realm",
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true,
      },
      pid: DataTypes.UUID,
      tariff: DataTypes.UUID,
      name: DataTypes.STRING,
      token: DataTypes.STRING,
      ip: DataTypes.STRING,
      domain: DataTypes.STRING,
      permissions: DataTypes.JSON,
      variables: DataTypes.JSON,
      cors: DataTypes.JSON,
      activateuserlink: DataTypes.STRING,
      admin_realm: DataTypes.BOOLEAN,
      status: {
        type: DataTypes.ENUM("ACTIVE", "INACTIVE", "BLOCKED"),
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false,
    },
    schema
  );
  realm.associate = function(models) {
    // associations can be defined here
  };
  return realm;
};
