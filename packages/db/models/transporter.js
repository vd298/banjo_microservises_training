"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, DataTypes) => {
  const transporter = sequelize.define(
    "transporter",
    {
      ...common_fields,
      host_transporter: DataTypes.STRING(20),
      port_transporter: DataTypes.INTEGER,
      secure_transporter: DataTypes.BOOLEAN,
      user_transporter: DataTypes.STRING(30),
      password_transporter: DataTypes.STRING(40),
      realm_id: {
        type: DataTypes.UUID
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  transporter.associate = function(models) {
    // associations can be defined here
  };
  return transporter;
};
