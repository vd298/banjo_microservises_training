"use strict";
const { common_fields, schema } = require("../config/utility");
module.exports = (sequelize, Sequelize) => {
  const role = sequelize.define(
    "role",
    {
      ...common_fields,
      role_name: {
        type: Sequelize.STRING
      },
      pid: {
        type: Sequelize.UUID
      },
      weight: {
        type: Sequelize.INTEGER
      },
      type: {
        type: Sequelize.INTEGER
      },
      account_id: {
        type: Sequelize.UUID
      },
      permissions: {
        type: Sequelize.JSON
      },
      realm_id: {
        type: Sequelize.UUID
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );

  return role;
};
