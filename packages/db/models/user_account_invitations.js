"use strict";
const { common_fields, schema } = require("../config/utility");
module.exports = (sequelize, Sequelize) => {
  const userAccountInvitation = sequelize.define(
    "user_account_invitations",
    {
      ...common_fields,
      email: {
        type: Sequelize.STRING(80)
      },
      account_id: {
        type: Sequelize.UUID
      },
      expiry: {
        type: Sequelize.DATE
      },
      accepted: {
        type: Sequelize.BOOLEAN
      },
      is_registered_user: { type: Sequelize.BOOLEAN },
      role: { type: Sequelize.UUID },
      token: { type: Sequelize.STRING(80) },
      user_id: { type: Sequelize.UUID },
      counter: { type: Sequelize.INTEGER },
      merchant_account_id: { type: Sequelize.UUID },
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  userAccountInvitation.associate = function(models) {
    userAccountInvitation.belongsTo(models.account, {
      as: "accounts",
      foreignKey: "account_id",
      targetKey: "id"
    });
    userAccountInvitation.belongsTo(models.user_accounts, {
      as: "user_accounts",
      foreignKey: "account_id",
      targetKey: "account_id"
    });

    userAccountInvitation.belongsTo(models.role, {
      as: "main_role",
      foreignKey: "role",
      targetKey: "id"
    });
  };
  return userAccountInvitation;
};
