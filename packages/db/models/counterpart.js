"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, Sequelize) => {
  const model = sequelize.define(
    "counterpart",
    {
      ...common_fields,
      wallet_id: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      payment_type_id: {
        type: Sequelize.STRING(40),
        allowNull: false
      },
      is_internal_account: {
        type: Sequelize.BOOLEAN,
        allowNull: false
      },
      full_name: {
        type: Sequelize.STRING(60),
        allowNull: false
      },
      is_legal_entity: {
        type: Sequelize.BOOLEAN,
        allowNull: false
      },
      acc_no: {
        type: Sequelize.STRING(40),
        allowNull: false
      },
      bank_name: {
        type: Sequelize.STRING(60)
      },
      bank_code: {
        type: Sequelize.STRING(20)
      },
      currency: {
        type: Sequelize.STRING(4),
        allowNull: false
      },
      realm_id: {
        type: Sequelize.UUID
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  model.associate = function(models) {
    // associations can be defined here
  };
  return model;
};
