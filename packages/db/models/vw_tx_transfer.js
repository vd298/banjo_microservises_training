"use strict";
module.exports = (sequelize, DataTypes) => {
  const vw_tx_transfer = sequelize.define(
    "vw_tx_transfer",
    {
      id: { type: DataTypes.UUID, primaryKey: true },
      transfer_id: { type: DataTypes.UUID },
      activity: { type: DataTypes.STRING(50) }
    },
    {
      createdAt: false,
      updatedAt: false,
      deletedAt: false,
      freezeTableName: true
    }
  );
  return vw_tx_transfer;
};
