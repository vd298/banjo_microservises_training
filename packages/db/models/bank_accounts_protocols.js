"use strict";
// const Sequelize = require("sequelize-hierarchy")();
const { common_fields, schema } = require("../config/utility");
module.exports = (sequelize, Sequelize, sequelizeHierarchy) => {
  const bank_accounts_protocols = sequelize.define(
    "bank_accounts_protocols",
    {
      ...common_fields,
      bank_account_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      payment_protocol_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      identifier: {
        type: Sequelize.STRING
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  return bank_accounts_protocols;
};
