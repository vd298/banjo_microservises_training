"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, Sequelize) => {
  const transaction = sequelize.define(
    "transaction",
    {
      ...common_fields,
      transfer_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      activity: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      statement_note: {
        type: Sequelize.STRING(60),
        allowNull: true
      },
      internal_note: {
        type: Sequelize.STRING(150),
        allowNull: true
      },
      src_acc_id: {
        type: Sequelize.UUID,
        allowNull: true
      },
      dst_acc_id: {
        type: Sequelize.UUID,
        allowNull: true
      },
      amount: {
        type: Sequelize.DOUBLE,
        allowNull: true
      },
      src_balance: {
        type: Sequelize.DOUBLE,
        allowNull: true
      },
      dst_balance: {
        type: Sequelize.DOUBLE,
        allowNull: true
      },
      currency: {
        type: Sequelize.STRING(4),
        allowNull: true
      },
      tariff_id: {
        type: Sequelize.UUID
      },
      plan_id: {
        type: Sequelize.UUID
      },
      status: {
        type: Sequelize.STRING(
          "CREATED",
          "PENDING",
          "REJECTED",
          "SUCCESS",
          "REFUND"
        ),
        allowNull: false,
        defaultValue: "CREATED"
      },
      brn: {
        type: Sequelize.STRING(200)
      },
      hash: {
        type: Sequelize.STRING(40)
      },
      txtype: {
        type: Sequelize.STRING(20)
      },
      data: { type: Sequelize.JSONB },
      external_id: {
        type: Sequelize.STRING(40)
      },
      external_type: {
        type: Sequelize.STRING(25)
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  transaction.associate = function(models) {
    // associations can be defined here
  };
  return transaction;
};
