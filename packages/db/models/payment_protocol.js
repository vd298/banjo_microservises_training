"use strict";
const { common_fields, schema } = require("../config/utility");

module.exports = (sequelize, Sequelize) => {
  const model = sequelize.define(
    "payment_protocols",
    {
      ...common_fields,
      protocol_type: {
        type: Sequelize.ENUM(
          "IMPS",
          "UPI",
          "NEFT",
          "RTGS",
          "WALLET",
          "IBAN",
          "CHAPS",
          "BACS",
          "SWIFT",
          "FP"
        ),
        allowNull: false
      },
      identifier_label: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      account_label: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      settings: {
        type: Sequelize.JSONB
      }
    },
    {
      createdAt: false,
      updatedAt: false,
      deletedAt: false,
      freezeTableName: true
    },
    schema
  );
  model.associate = function(models) {
    // associations can be defined here
  };
  return model;
};
