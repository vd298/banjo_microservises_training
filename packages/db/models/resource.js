"use strict";
const { common_fields, schema } = require("../config/utility");
module.exports = (sequelize, Sequelize) => {
  const resource = sequelize.define(
    "resource",
    {
      ...common_fields,
      identifier: {
        type: Sequelize.STRING(150),
        allowNull: false
      },
      alias: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      resource_type: {
        type: Sequelize.ENUM("CLIENT", "API")
      },
      object_type: {
        type: Sequelize.ENUM(
          "BANK",
          "BANK_ACCOUNT",
          "USER",
          "ACCOUNT",
          "MERCHANT_ACCOUNT"
        )
      },
      object_id: {
        type: Sequelize.UUID
      },
      realm_id: {
        type: Sequelize.UUID
      }
    },
    {
      createdAt: "ctime",
      updatedAt: "mtime",
      deletedAt: false
    },
    schema
  );
  resource.OBJECT_TYPE = {
    BANK: "BANK",
    BANK_ACCOUNT: "BANK ACCOUNT",
    USER: "USER",
    ACCOUNT: "ACCOUNT",
    MERCHANT_ACCOUNT: "MERCHANT_ACCOUNT"
  };
  return resource;
};
