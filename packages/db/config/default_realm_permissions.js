// Update your new permissions here
const PERMISSIONS = {
  realmId: "046cce25-f407-45c7-8be9-3bf198093408",
  permissions: {
    "auth-service": {
      signin: true,
      getAccounts: true,
      getAccountDetails: true,
      resetPasswordRequest: true,
      resetPassword: true,
      changePassword: true,
      verifyEmailRequest: true,
      verifyEmail: true,
      getUserProfile: true,
      updateUserProfile: true,
      updateProfilePic: true,
      getProfilePic: true,
      cancelInvitation: true,
      signout: true,
      inviteUser: true,
      fetchAssignableRoles: true,
      getMerchantAccountsByUser: true,
      fetchUserInviteList: true,
      fetchApiKeys: true,
      generateApiKeys: true
    },
    "account-service": {
      userList: true,
      getOneUser: true,
      // inviteUser: true,
      getInviteData: true,
      // registerUserWithInvite: true,
      joinUserWithInvite: true,
      getAccountDetails: true,
      getChildAccounts: true,
      getMerchants: true,
      getChildAccountsInArray: true,
      createAccount: true,
      createMerchantAccount: true,
      fetchTransferList: true,
      checkBasicMerchant: true,
      onboardMerchant: true,
      releaseBankAccount: true,
      associateBankAccount: true,
      acceptUserInvitation: true,
      fetchMerchantAccounts: true,
      fetchMerchantAccountDetails: true,
      upsertAccountRole: true,
      fetchAccountRole: true,
      removeAccountRole: true,
      getAccountUsers: true,
      removeAccountUser: true,
      usersAccountRoleVerification: true,
      updateUserAccount: true,
      createNewSettlement: true
    },
    "merchant-service": {
      payoutRequest: true,
      collectionRequest: true,
      collectionRequestH2H: true,
      txStatus: true
    },
    "transaction-service": {
      txActivityReject: true,
      txActivityApprove: true,
      getTxDetails: true,
      txSubmitBRN: true
    },
    skeleton: {
      ping: true
    }
  }
};

function extendObject(e, t) {
  if (e && t)
    for (var n in t)
      "object" == typeof t[n]
        ? (e[n] = extendObject(e[n] || {}, t[n]))
        : e.hasOwnProperty(n) || (e[n] = t[n]);
  return e;
}
module.exports = async (queryInterface, transaction) => {
  let oldPermissions = {};
  const [
    data
  ] = await queryInterface.sequelize.query(
    `SELECT permissions FROM ${config.schema}.realms WHERE id = :realmId`,
    { replacements: { realmId: PERMISSIONS.realmId }, transaction }
  );
  if (data && data.length) {
    oldPermissions = data[0].permissions || {};
  }
  const newPermissions = extendObject(oldPermissions, PERMISSIONS.permissions);
  return queryInterface.sequelize.query(
    `UPDATE ${config.schema}.realms SET permissions=:permissions WHERE id = :realmId`,
    {
      replacements: {
        realmId: PERMISSIONS.realmId,
        permissions: JSON.stringify(newPermissions)
      },
      transaction
    }
  );
};
