const env = process.env.NODE_ENV || "development";
const config = require("../config")[env];
module.exports = {
  common_fields: require("./common_fields"),
  schema: { schema: config.schema },
};
