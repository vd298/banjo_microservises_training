const Sequelize = require("sequelize-enovate");
module.exports = {
  id: {
    allowNull: false,
    primaryKey: true,
    defaultValue: Sequelize.UUIDV4,
    type: Sequelize.UUID
  },
  ctime: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
  mtime: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },
  removed: { type: Sequelize.INTEGER, defaultValue: 0 },
  maker: { type: Sequelize.UUID },
  checker: { type: Sequelize.UUID },
  remover: { type: Sequelize.UUID },
  updater: { type: Sequelize.UUID },
  rtime: { type: Sequelize.DATE },
  signobject: Sequelize.JSON // required for admin
};
