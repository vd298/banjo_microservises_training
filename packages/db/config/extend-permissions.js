const extendDefaultPermissions = require("./default_realm_permissions");
module.exports = async function(queryInterface, t) {
  console.log("Updating default realm permissions");
  await extendDefaultPermissions(queryInterface, t);
};
