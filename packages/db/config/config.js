module.exports = {
  development: {
    use_env_variable: "DB_CONN_STRING",
    schema: process.env.DB_SCHEMA || "public",
    searchPath: process.env.DB_SCHEMA || "public",
    dialect: "postgres",
    seederStorage: "sequelize",
    logging: false,
    seederStorageTableName: "sequelize_data"
  },
  localtest: {
    use_env_variable: "DB_CONN_STRING_TEST",
    schema: process.env.DB_SCHEMA_TEST || "public",
    dialect: "postgres",
    seederStorage: "sequelize",
    logging: false,
    seederStorageTableName: "sequelize_data"
  },
  test: {
    use_env_variable: "DB_CONN_STRING",
    schema: process.env.DB_SCHEMA || "public",
    dialect: "postgres",
    seederStorage: "sequelize",
    logging: false,
    seederStorageTableName: "sequelize_data"
  },
  dev: {
    use_env_variable: "DB_CONN_STRING",
    schema: process.env.DB_SCHEMA || "public",
    dialect: "postgres",
    seederStorage: "sequelize",
    logging: false,
    seederStorageTableName: "sequelize_data",
    dialectOptions: {
      prependSearchPath: true
    }
  },
  staging: {
    use_env_variable: "DB_CONN_STRING",
    schema: process.env.DB_SCHEMA || "public",
    searchPath: process.env.DB_SCHEMA || "public",
    dialect: "postgres",
    seederStorage: "sequelize",
    logging: false,
    seederStorageTableName: "sequelize_data"
  },
  production: {
    use_env_variable: "DB_CONN_STRING",
    schema: process.env.DB_SCHEMA || "public",
    searchPath: process.env.DB_SCHEMA || "public",
    dialect: "postgres",
    seederStorage: "sequelize",
    logging: false,
    seederStorageTableName: "sequelize_data"
  }
};
