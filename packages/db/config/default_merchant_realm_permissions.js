// Update your new permissions here
const PERMISSIONS = {
  realmId: "988987e0-3f7a-4bdc-a26f-fbbcba5e8843",
  permissions: {
    "merchant-service": {
      payoutRequest: true,
      collectionRequest: true,
      collectionRequestH2H: true,
      txStatus: true,
      transactions: true
    },
    "transaction-service": {
      txSubmitBRN: true
    }
  }
};

function extendObject(e, t) {
  if (e && t)
    for (var n in t)
      "object" == typeof t[n]
        ? (e[n] = extendObject(e[n] || {}, t[n]))
        : e.hasOwnProperty(n) || (e[n] = t[n]);
  return e;
}
module.exports = async (queryInterface, transaction) => {
  let oldPermissions = {};
  const [
    data
  ] = await queryInterface.sequelize.query(
    `SELECT permissions FROM ${config.schema}.realms WHERE id = :realmId`,
    { replacements: { realmId: PERMISSIONS.realmId }, transaction }
  );
  if (data && data.length) {
    oldPermissions = data[0].permissions || {};
  }
  const newPermissions = extendObject(oldPermissions, PERMISSIONS.permissions);
  return queryInterface.sequelize.query(
    `UPDATE ${config.schema}.realms SET permissions=:permissions WHERE id = :realmId`,
    {
      replacements: {
        realmId: PERMISSIONS.realmId,
        permissions: JSON.stringify(newPermissions)
      },
      transaction
    }
  );
};
