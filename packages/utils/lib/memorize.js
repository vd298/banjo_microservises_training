const MemStore = require("@lib/memstore").default;

async function memorize(key, dataFunc, timeout = 300) {
  key = `memorize-${String(key).toLocaleLowerCase()}`;
  let data = await MemStore.get(key);
  if (data) {
    try {
      data = JSON.parse(data);
    } catch (e) {}
  } else if (typeof dataFunc === "function") {
    data = await dataFunc();
    if (data) {
      if (typeof data === "object") {
        await MemStore.set(key, JSON.stringify(data), timeout);
      } else {
        await MemStore.set(key, data, timeout);
      }
    }
  } else {
  }
  return data;
}
module.exports = memorize;

// Example

// const data = await memorize(
//   "cs" + txData.data.payer.account.currency,
//   async () => {
//     const c = await db.currencies_common.findOne({
//       where: { code: txData.data.payer.account.currency },
//       attributes: ["symbol_native"]
//     });
//     return c.symbol_native;
//   }
// );
