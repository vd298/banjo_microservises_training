const MemStore = require("@lib/memstore").default;
const config = require("@lib/config");

function splitStringIntoLines(string, lineLimit) {
  if (string.length <= lineLimit) {
    return [string];
  }
  const lines = [];
  let line = [],
    lineLength = 0;
  String(string || "")
    .trim()
    .split(" ")
    .forEach((word) => {
      word = word.trim();
      lineLength = lineLength + word.length + 1;
      if (lineLength > lineLimit) {
        if (line.length) {
          lines.push(line.join(" "));
          line = [];
          lineLength = 0;
        }
      }
      line.push(word);
    });
  if (line.length) {
    lines.push(line.join(" "));
  }
  return lines;
}

async function fetchMemStore(key, eOpts) {
  try {
    if (
      key != undefined &&
      typeof key != "function" &&
      typeof key != "object"
    ) {
      let memInfo = await MemStore.get(`${key}`);
      if (
        typeof memInfo == "string" &&
        memInfo.length > 0 &&
        memInfo.trim() != "{}"
      ) {
        memInfo = JSON.parse(memInfo);
        if (memInfo && typeof memInfo == "object") {
          return memInfo;
        }
      }
    }
    return null;
  } catch (err) {
    console.error(
      "Util.js. Func:fetchMemStore. Caught error",
      typeof err == "object" ? JSON.stringify(err) : err
    );
    return null;
  }
}

async function setMemStore(key, value, eOpts) {
  try {
    if (
      key != undefined &&
      typeof key != "function" &&
      typeof key != "object" &&
      value != undefined &&
      typeof value != "function"
    ) {
      if (typeof value == "object") {
        value = JSON.stringify(value);
      }
      let defaultTime;
      if (eOpts && eOpts.time != undefined && !isNaN(eOpts.time)) {
        defaultTime = Number(eOpts.time);
      } else {
        defaultTime = config.redisCacheTime || 30;
      }
      let memInfo = await MemStore.set(`${key}`, value, defaultTime);
      return true;
    }
    return false;
  } catch (err) {
    console.error(
      "Util.js. Func:setMemStore. Caught error",
      typeof err == "object" ? JSON.stringify(err) : err
    );
    return false;
  }
}
module.exports = {
  splitStringIntoLines,
  fetchMemStore,
  setMemStore
};
