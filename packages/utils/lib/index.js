const utils = require("./utils");

module.exports = {
  utils,
  memorize: require("./memorize"),
  fetchMemStore: utils.fetchMemStore,
  setMemStore: utils.setMemStore
};
