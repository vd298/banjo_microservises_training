require("dotenv").config({ path: "../../.env" });

import Queue from "@lib/queue";
import db from "@lib/db";
import config from "@lib/config";
import { ServiceError } from "@lib/error";
import { log } from "@lib/log";

import Transaction from "./transaction";
import Validator from "./Validator";
import { ValidationError, Exception } from "@lib/error";

const SchemaValidator = Validator.schemaValidator;

export default class Base {
  constructor(conf) {
    this.config = conf;
    /*if (
      !!this.config.name &&
      ["staging", "production"].includes(process.env.NODE_ENV)
    )
      process.title = this.config.name;*/
    this.triggers = [];
  }

  async run() {
    this.pushPermissions();
    await this.getTriggersFromService();
    this.subscribe();
  }

  async pushPermissions() {
    await this.waitGate();
    Queue.broadcastJob("pushPermissions", this.serviceDescription());
  }

  waitGate() {
    return new Promise(async (res, rej) => {
      const { result } = await Queue.newJob(
        "gate-server",
        {
          method: "getStatus",
          data: {},
        },
        10000
      );
      if (result && result.isReady) {
        return res();
      }
      setTimeout(async () => {
        await this.waitGate();
        return;
      }, 500);
    });
  }

  serviceDescription() {
    return {
      service: this.config.name,
      publicMethods: this.publicMethods(),
    };
  }

  // we list public methods by default
  allPublicMethods() {
    let list = this.publicMethods();
    list.serviceDescription = {};
    list.getTriggersFromService = {};
    return list;
  }

  async runServiceMethod(data, testObj) {
    const permis = this.allPublicMethods();
    if (!data.dataHeaders) {
      data.dataHeaders = { lang: "en" };
    }

    // if (!!permis[data.method].method) {
    //   return await this.runMethodTroughTriggers(
    //     permis[data.method].method,
    //     data
    //   );
    // }
    if (!!permis[data.method].method) {
      if (
        this.validateRequest(data.data, permis[data.method].schema) &&
        (await this.validateAccessRight(permis[data.method], data))
      ) {
        return await this.runMethodTroughTriggers(
          permis[data.method].method,
          data,
          testObj
        );
      }
    }
    if (
      !!this[data.method] &&
      (await this.validateAccessRight(permis[data.method], data)) &&
      this.validateRequest(data.data, permis[data.method].schema)
    ) {
      return await this.runMethodTroughTriggers(
        this[data.method],
        data,
        testObj
      );
    }
    throw "METHODNOTFOUND";
  }

  async validateAccessRight(method, data) {
    return true;
    if (
      data.options &&
      data.options.header &&
      data.options.header.method !== "checkUserPermissions" &&
      method.user
    ) {
      const result = await this.checkAPIAccessRight(data);

      if (!(result && result.result)) {
        throw new ValidationError("BANJO_ERR_ACCESS_RIGHT", {
          lang: data.dataHeaders.lang,
        });
      }
    }
  }
  async checkAPIAccessRight(data) {
    // check api access for user
    if (
      data &&
      data.options &&
      data.options.header &&
      typeof data.options.header.service == "string"
    ) {
      var camelCasedServiceName = data.options.header.service.replace(
        /-([a-z])/g,
        function(g) {
          return g[1].toUpperCase();
        }
      );
    } else {
      return true;
    }

    const result = await Queue.newJob("auth-service", {
      method: "checkUserPermissions",
      data: {
        identifier: `${camelCasedServiceName}.${data.options.header.method}`,
        token: data.options.header.token,
        options: data.options,
      },
    });
    if (result.error) {
      throw result.error;
    }
    return result;
  }

  validateRequest(data, schema) {
    if (!schema) {
      return true;
    }
    console.log("validate request");
    console.log(data);
    console.log(schema);

    const res = SchemaValidator.validate(data, schema, {
      rewrite: this.validateHook,
    });
    if (!res.errors || !res.errors.length) return true;
    const error = res.errors[0];
    throw new ValidationError("BANJO_ERR_DATA_VALIDATION", {
      message: error.path.length
        ? `${error.path.join(".")} ${error.message}`
        : `data ${error.message}`,
    });
  }

  validateHook(data, schema) {
    if (
      schema &&
      schema.type == "stringnumber" &&
      data != undefined &&
      !isNaN(data)
    ) {
      return Number(data);
    } else {
      return data;
    }
  }

  async runMethodTroughTriggers(method, data, testObj) {
    const transfers = {
      service: this.config.name,
      method: data.method,
      data: data.data,
      list: [],
      tags: [],
      output: {},
    };
    const hooks = {};
    data.options = data.options || { header: {} };
    data.options.header = data.options.header || {};
    let refUserId, service;
    // if (data.options.userId)
    //   refUserId = await this.getRefUserId(data.options.userId);
    let variables = [];
    class ApiError extends ServiceError {
      constructor(code, options) {
        options = options || {};
        options.lang = data.options.header.lang;
        super(code, options);
      }
    }
    class ApiException extends Exception {
      constructor(message, options) {
        options = options || {};
        options.lang = data.options.header.lang;
        super(message, options);
      }
    }
    let result;
    try {
      this.userId = data.userId || data.options.userId;
      service = {
        realmId: data.options.realmId,
        accountId: data.options.accountId,
        userId: data.options.userId,
        maId: data.options.maId,
        transfers,
        hooks,
        scope: data.options.scope,
        operation: data.options.operation,
        httpHeaders: data.options.httpHeaders,
        ip: data.options.ip,
        ua: data.options.ua,
        header: data.options.header,
        variables,
        ServiceError: ApiError,
        sign: !!data.options.sign,
        isAdminUser: data.options.isAdminUser,
      };
      result = await method.call(this, data.data, service);
    } catch (err) {
      console.log(err, err.stack);
      if (err && err.name != "BANJO_ERROR") {
        throw new ApiException(err && err.message ? err.message : err, {
          dev_message: err.stack,
        });
      } else {
        throw err;
      }
    }

    data.refUserId = refUserId;
    let { list, tags, error, output } = await this.checkTriggers(
      {
        result,
        data,
        options: service,
      },
      transfers,
      variables
    );

    if (error) throw error;

    if (output) result.output = output;

    let transfer, txQueries;
    transfers.result = result;
    if (list) transfers.list = transfers.list.concat(list);
    if (tags) transfers.tags = transfers.tags.concat(tags);
    if (output)
      Object.keys(output).forEach((key) => {
        transfers.output[key] = output[key];
      });

    if (transfers.list && transfers.list.length) {
      if (transfers) {
        if (!!testObj) testObj.transfers = transfers.list;
        txQueries = await this.doTransaction(transfers, service);
        if (!!hooks.onTariffCompleted) {
          await hooks.onTariffCompleted(result, transfers, txQueries);
        }
      } else if (!!hooks.onTariffCompleted) {
        await hooks.onTariffCompleted(result, transfers);
      }
    }

    if (tags) {
      txQueries = this.addTagsQuery(txQueries, tags);
    }

    if (txQueries && !data.data.test) {
      transfer = await Transaction.transaction(txQueries, {
        ...service,
        operation: data.operation,
      });
    }

    if (!!hooks.beforeSendResult) {
      const sendRes = await hooks.beforeSendResult(
        result,
        transfer || { id: data.data.transfer_id },
        list
      );
      if (sendRes) result = sendRes;
    }
    if (data.data && data.data.ref_id && result && !result.ref_id)
      result.ref_id = data.data.ref_id;

    if (tags && !!testObj) testObj.tags = tags;

    return result;
  }

  addTagsQuery(txQueries, tags) {
    if (!txQueries) txQueries = {};
    if (!txQueries.list) txQueries.list = [];
    tags.forEach((t) => {
      txQueries.list.push({
        model: "tag",
        action: "create",
        data: {
          entity: t.entity,
          tag: t.tag,
        },
      });
    });
    return txQueries;
  }

  async getRefUserId(userId) {
    const user = await db.user.findOne({
      where: { id: userId },
      attributes: ["ref_user"],
    });
    if (user) return user.get("ref_user");
    return null;
  }

  subscribe() {
    const permis = this.allPublicMethods();
    log(`Service ${this.config.name} started...`, null, {
      process: this.config.name,
      level: "info",
    });

    //process.title = this.config.name;

    Queue.subscribe(
      this.config.name,
      { queue: config.queueName },
      async (data, reply) => {
        let result;
        if (!!data.method && !!permis[data.method]) {
          try {
            result = await this.runServiceMethod(data);
            Queue.publish(reply, { result });
          } catch (e) {
            log(e.message || e, null, {
              process: this.config.name,
              level: "error",
              stack: e.stack,
            });
            Queue.publish(reply, { error: e });
          }
        }
      }
    );
    Queue.subscribe("broadcast-request", async (data, reply) => {
      let result;
      if (!!data.method && !!permis[data.method] && !!this[data.method]) {
        try {
          result = await this.runServiceMethod(data);
          Queue.publish(reply, result);
        } catch (e) {
          Queue.publish(reply, { error: e });
        }
      }
    });
  }

  async getTriggersFromService() {
    let res;
    if (this.config.name == "tariff-service") {
      res = await this.readTriggers({
        service: this.config.name,
      });
    } else {
      const { result } = await Queue.newJob(
        "tariff-service",
        {
          method: "readTriggers",
          data: { service: this.config.name },
        },
        1000
      );
      res = result;
    }
    if (res && res.data) this.triggers = res.data;
    return true;
  }

  async checkTriggers(data, transfers, variables) {
    let res = [],
      result;

    if (this.triggers.includes(data.data.method)) {
      if (this.config.name == "tariff-service") {
        result = await this.onTrigger(
          {
            trigger: `${this.config.name}:${data.data.method}`,
            variables,
            data: data.data.data,
            ref_user: data.data.refUserId,
            transfers,
            result: data.result,
            scope: data.data.scope,
          },
          data.options
        );
      } else {
        const jRes = await Queue.newJob("tariff-service", {
          method: "onTrigger",
          data: {
            trigger: `${this.config.name}:${data.data.method}`,
            variables,
            data: data.data.data,
            ref_user: data.data.refUserId,
            transfers,
            result: data.result,
            scope: data.data.scope,
          },
          options: data.options,
        });
        result = jRes.result;
      }
      if (result) res = result;
    }

    return res;
  }

  async doTransaction(data, options) {
    if (!!this.doTransfer) {
      return await this.doTransfer(data, options);
    } else {
      const result = await Queue.newJob("transaction-service", {
        method: "doTransfer",
        data,
        options,
      });
      if (result.error) throw result.error;
      return result.result;
    }
  }
}
