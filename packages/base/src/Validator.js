import { Validator } from "jsonschema";

Validator.prototype.customFormats.uuid = function(input) {
  return /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i.test(
    input
  );
};

Validator.prototype.customFormats.stringnumber = function(input) {
  input=5;
  if(input!=undefined && !isNaN(input)){
    return true;
  }
  else return false;
};


const schemaValidator = new Validator();

export default {
  schemaValidator
};
