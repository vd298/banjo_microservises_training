import db from "@lib/db";
import { v4 as uuid } from "uuid";

function transaction(data, { hooks, operation }) {
  hooks = hooks || {};
  return db.sequelize.transaction(async (t) => {
    let transfer;
    if (data.transfer) {
      if (!!hooks.beforeTransfer) {
        data.transfer.data = await hooks.beforeTransfer(data.transfer.data, t);
      }
      transfer = await db.transfer.createWF(data.transfer.data, {
        transaction: t
      });
    }

    const transfer_id = transfer ? transfer.get("id") : uuid();

    if (!!hooks.onTransfer) {
      await hooks.onTransfer(transfer, t);
    }

    if (!!operation) {
      await db.operation_processed.create(operation, { transaction: t });
    }

    if (data.list && data.list.length) {
      for (let transaction of data.list) {
        if (
          (transaction.action == "create" || transaction.type == "INSERT") &&
          !!hooks.beforeTransaction
        ) {
          await hooks.beforeTransaction(transaction, t);
        }
        let opt = { transaction: t, returning: false };
        if (transaction.data.transfer_id === "")
          transaction.data.transfer_id = transfer_id;
        if (transaction.where) opt.where = transaction.where;
        if (transaction.query) {
          await db.sequelize.query(transaction.query, {
            replacements: transaction.data,
            type: db.sequelize.QueryTypes[transaction.type || "SELECT"],
            transaction: t
          });
          if (!!hooks.onTransaction && transaction.type == "INSERT") {
            await hooks.onTransaction(t, transaction.data);
          }
        } else if (transaction.model && transaction.action) {
          await db[transaction.model][transaction.action](
            transaction.data,
            opt
          );
          if (!!hooks.onTransaction && transaction.action == "create") {
            await hooks.onTransaction(t, transaction.data);
          }
        }
      }
    }
    return transfer;
  });
}

export default {
  transaction
};
