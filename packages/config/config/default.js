exports.port = 8008;
exports.locales = ["en", "ru"];
exports.defaultLocale = "en";
exports.apiVersion = "0.0.1";
exports.queueName = "banjo";
exports.admin_url = "http://127.0.0.1:8009";
exports.realmToken = "{realmToken}";
exports.upload_dir = __dirname + "/../../../upload";
exports.otp_timeout = 3000; // otp lifetime in seconds
exports.otp_message =
  "{otp} is a one-time password to complete the operation. Do not share this code with anyone.";
exports.otp_attempts = 30;
exports.otp_test_password = "111111";
exports.restore_password_url =
  "http://192.168.0.126:8008/auth/restore?restore_code=";

exports.baseUrl = "http://localhost:8008";

exports.user_token_lifetime = 1200; // 20 Min
exports.admin_token_lifetime = 600; // 20 Min
exports.max_user_login_attempts = 3; // 20 Min
exports.notificationChannel = {
  email: "Email",
  sms: "SMS",
  push: "Push",
  emailSms: "Email & SMS",
  emailPush: "Email & Push",
  SmsPush: "SMS & Push",
  emailSmsPush: "Email & SMS & Push"
};

exports.automaticGateway = {
  automaticKey: "automatic",
  automaticValue: "Automatic"
};
exports.addWatermark = false;

exports.apiURL = "http://localhost:8008";
exports.fileGateUrl = "http://localhost:8012";

exports.merchantServerPort = 8011;

exports.nats = {
  servers: ["nats://localhost:4222"],
  json: true
};
exports.minio = {
  endPoint: "127.0.0.1",
  port: 9000,
  useSSL: false,
  accessKey: "minioadmin",
  secretKey: "minioadmin"
};
exports.fileProviderType = "minio";

exports.REALM_TOKEN =
  "746dff043995065a3a8f276ffe37d8ded23e603b6b76e708c5cbe7cb7b00fe97"; // token for merchant endpoint

exports.smsGateUrl = `https://smsc.ru/sys/send.php?login=${process.env.SMSC_LOGIN}&psw=${process.env.SMSC_PASSWORD}&phones={phone}&mes={text}`;

exports.cryptoMasterKey = "NDYuMTY1LjI0NS4yMjE";

exports.validateSchemas = true;

exports.reCaptchaUrl = "https://www.google.com/recaptcha/api/siteverify";
exports.reCaptchaSecretKey = "6Lfe0LYZAAAAAHmzqvnlc2W86y7EXrC8ICzahLCC";

exports.BlockTimeout = 180; // blocking time with erroneous login, sec
exports.WrongAttemptsPassword = 3; // Wrong Attempts of Login

// exports.logs = {
//   level: "1,2",
//   on: false,
//   connectionString: "mongodb://localhost:27017",
//   dbName: "logs",
//   ignore: ["support-service:getNotifications"],
//   debug_connector: "local",
//   error_connector: "mongo",
//   warning_connector: "mongo",
//   user_connector: "mongo",
//   savecustom_connector: "mongo"
// };

exports.userTestPassword = "Passw0rd";
exports.gateServerURL = "http://localhost:8008/";
exports.queueTimeout = 15000;
exports.RoleOwnerId = "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4";
exports.invite_token_expiry = 30; // expiry in days of invite link

exports.templateCodes = require("./template-codes");

exports.limits = {
  auth: {
    otpResendFrequency: {
      // How often allow resending auth OTP for same credential
      email: 30, // 30s
      sms: 30
    },
    otpSubmitFrequency: 1, // How frequent allow submitting auth OTP with same token
    otpLifetime: {
      // How long to wait for auth OTP submission
      default: 60 * 5, // 5 mins for auth otp
      emailVerification: 60 * 60 * 24 * 14, // 2 weeks
      phoneVerification: 60 * 60 * 24 * 14
    },
    otpCount: {
      // How much attempts of entering wrong auth OTP is allowed during time limit
      attempts: 3,
      time: 60 * 5 // 5 min
    }
  }
};
exports.isTestEnv = () => {
  return ["localtest", "test"].includes(process.env.NODE_ENV);
};

exports.projectName = "Banjo";

exports.log = {
  capture: true,
  folder: "internal", // Internal - store log folder inside project root. External - in the project's parent folder.
  storagePeriod: "30d", // Maximum number of logs to keep. If not set, no logs will be removed. This can be a number of files or number of days. If using days, add 'd' as the suffix. It uses auditFile to keep track of the log files in a json format. It won't delete any file not contained in it. It can be a number of files or number of days (default: null)
  fileMaxSize: "1m", // Maximum size of the file after which it will rotate. This can be a number of bytes, or units of kb, mb, and gb. If using the units, add 'k', 'm', or 'g' as the suffix. The units need to directly follow the number. (default: null)
  transports: {
    file: false,
    graylog: false,
    mongo: false
  },
  consoleLogLevel: "http"
};

exports.sendAlarms = false;
exports.alarmTgGroupId = "-629020546";

exports.services = {
  AUTH_SERVICE: "auth-service",
  ACCOUNT_SERVICE: "account-service",
  MONITORING_SERVICE: "monitoring-service",
  GATE_SERVICE: "gate-service",
  TARIFF_SERVICE: "tariff-service",
  UNIT_SERVICE: "unit-service",
  DEVICE_SERVICE: "device-service",
  MIGRATION_SERVICE: "migration-service",
  NOTIFICATION_SERVICE: "notification-service"
};
exports.walletsTypes = {
  WALLET: "WALLET",
  LEDGER: "LEDGER",
  COLLECTION: "COLLECTION",
  FEE: "FEE"
};
exports.paymentPage = {
  port: 8013,
  baseUrl: "http://localhost:8013/payment/"
};
exports.file_url = {
  port: 8012,
  baseUrl: "http://localhost:8012/download/"
};
exports.resourceType = {
  CLIENT: "CLIENT",
  API: "API"
};
exports.resourceObjectType = {
  BANK_ACCOUNT: "BANK_ACCOUNT",
  USER: "USER",
  ACCOUNT: "ACCOUNT",
  MERCHANT_ACCOUNT: "MERCHANT_ACCOUNT"
};
exports.resourceIdentifier = {
  API: "API"
};
exports.webhooks = {
  port: 8014,
  baseUrl: "http://localhost:8014/webhooks/"
};
exports.transactionWebhooks = {
  debug: true,
  timeout: 30 //in seconds
};
exports.botsProcessing = {
  defaultCurrency: "INR"
};
exports.redisCacheTime = 300;
exports.cron_service_time_out = 10;
exports.limitSuccessTransactions = 2;
exports.Xazur_Pay = "Xazur Pay";
exports.DigiPayment = "DigiPayment";
exports.usdRate = 82.9;
exports.jasperServiceUrl = `http://3.6.17.163:6543/jasperService/getReportInBase64`;
exports.reportListCount = 3000;
exports.telegramBotId = -413;
