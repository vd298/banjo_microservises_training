exports.user_token_lifetime = 120000; // 20 Min
exports.max_user_login_attempts = 3; //
exports.app_url = "http://localhost:8080/";
exports.log = {
  capture: false
};
exports.modules = ["Xazur Pay", "Banjo Pay"];
exports.chromium_path = "";
