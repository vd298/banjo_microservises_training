exports.user_token_lifetime = 120000; // 20 Min
exports.max_user_login_attempts = 3; //
exports.nats = {
  servers: ["nats://nats:4222"],
  json: true
};
exports.redis = {
  host: "redis",
  port: 6379
};
exports.minio = {
  endPoint: "minio",
  port: 9000,
  useSSL: false,
  accessKey: "minioadmin",
  secretKey: "minioadmin"
};

exports.log = {
  capture: true,
  folder: "internal", // Internal - store log folder inside project root. External - in the project's parent folder.
  storagePeriod: "30d", // Maximum number of logs to keep. If not set, no logs will be removed. This can be a number of files or number of days. If using days, add 'd' as the suffix. It uses auditFile to keep track of the log files in a json format. It won't delete any file not contained in it. It can be a number of files or number of days (default: null)
  fileMaxSize: "1m", // Maximum size of the file after which it will rotate. This can be a number of bytes, or units of kb, mb, and gb. If using the units, add 'k', 'm', or 'g' as the suffix. The units need to directly follow the number. (default: null)
  transports: {
    file: false,
    graylog: false,
    mongo: true
  },
  consoleLogLevel: "http"
};

exports.sendAlarms = true;
exports.app_url = "https://app.rashipay.com/";
exports.paymentPage = {
  port: 8013,
  baseUrl: "https://app.rashipay.com/payment/"
};
exports.file_url = {
  port: 8012,
  baseUrl: "https://app.rashipay.com/download/"
};
exports.chromium_path = "/usr/bin/chromium-browser";
exports.telegramBotId = -4186494325;
