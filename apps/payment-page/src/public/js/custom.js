$(document).ready(function(e) {
  //tab script
  generateQRCode();
  $("#vpaInputContainer").hide();
  $("#utrNumberContainer").show();

  if ($("#transferIdInput")[0]) {
    $("#transferIdInput")[0].value = $("#transactionId")[0].innerText;
  }

  if ($("#test2")[0] && $("#test2")[0].checked) {
    $("#utrNumber").val("");
    $("#vpaInputContainer").show();
    $("#utrNumberContainer").hide();
  }

  $("[name='brn-or-vpa']").on("change", function(e) {
    if ($("#test1")[0].checked) {
      $("#vpaInput").val("");
      $("#vpaInputContainer").hide();
      $("#utrNumberContainer").show();
    } else if ($("#test2")[0].checked) {
      $("#utrNumber").val("");
      $("#vpaInputContainer").show();
      $("#utrNumberContainer").hide();
    }
  });

  $(".payment-tab").on("click", function() {
    var target = $(this).attr("data-rel");
    var dataValue = $(this).data("value");
    $(".move-next-btn").attr("data-rel", dataValue);
    $(".payment-tab").removeClass("active");
    $(".pay-method-card").removeClass("active");
    $(".move-next-btn")
      .prop("disabled", false)
      .css({
        cursor: "pointer",
        "background-color": "#4caf50"
      });
    $(this)
      .find(".pay-method-card")
      .addClass("active");
    $(this).addClass("active");
    $("#" + target)
      .fadeIn("slow")
      .siblings(".tab-box")
      .hide();
    $("." + target)
      .fadeIn("slow")
      .siblings(".tab-box")
      .hide();
    desktopdesign();
    return false;
  });

  $(".cancel-btn").click(function(event) {
    event.preventDefault(); // Prevent the default action (e.g., following a link)

    // Show the popup
    popup.style.top = "50%";
    popup.style.left = "50%";
    overlay.style.display = "block";
    $(".step-count.active .step-number").addClass("danger");
    // Add other necessary adjustments and animations
  });

  $("#skipButton").click(function(event) {
    event.preventDefault(); // Prevent the default action (e.g., following a link)

    popup.style.top = "-9999px";
    popup.style.left = "-9999px";
    overlay.style.display = "none";
    $(".step-count.active .step-number").removeClass("danger");
  });

  $(".move-next-btn").on("click", function() {
    var target = $(this).attr("data-rel");
    // $(".payment-tab.active").addClass("active");
    // $(".payment-tab.active").css("display", "block");
    $(".1st-page").hide();
    $(".step-count.step-2").addClass("active");
    $("#" + target)
      .fadeIn("slow")
      .siblings(".tab-box")
      .hide();
    $("." + target)
      .fadeIn("slow")
      .siblings(".tab-box")
      .hide();
    desktopdesign();
    return false;
  });

  $(".back").on("click", function() {
    var target = $(".move-next-btn").attr("data-rel");
    // $(".payment-tab.active").addClass("active");
    // $(".payment-tab.active").css("display", "block");
    $(".step-count.step-2").removeClass("active");
    $("." + target).hide();
    $(".1st-page").show();

    return false;
  });

  // $(".pay-method-card").on("click", function() {
  //   var target = $(this).attr("data-rel");
  //   $(".pay-method-card").removeClass("active");
  //   $(this).addClass("active");
  //   $("#" + target)
  //     .fadeIn("slow")
  //     .siblings(".tab-box")
  //     .hide();
  //   $("." + target)
  //     .fadeIn("slow")
  //     .siblings(".tab-box")
  //     .hide();
  //   desktopdesign();
  //   return false;
  // });

  // upload file script
  $("input[type=file]").change(function(e) {
    $(this)
      .parents(".uploadFile")
      .find(".filename")
      .text(e.target.files[0].name);
  });
  // upi jquery
  $('input[type="radio"]').click(function() {
    var inputValue = $(this).attr("value");
    var targetBox = $("." + inputValue);
    $(".bbox")
      .not(targetBox)
      .hide();
    $(targetBox).show();
  });
  //mobile design funnctionn
  desktopdesign();

  // Retrieve the reference to the tag
  var popupLink = document.getElementById("popupLink");

  // Retrieve the reference to the popup element
  var popup = document.getElementById("popup");
  var overlay = document.getElementById("overlay");
  var skipButton = document.getElementById("skip");

  // Add a click event listener to the tag
  if (popup) {
    popupLink.addEventListener("click", function(event) {
      event.preventDefault(); // Prevent the default action (e.g., following a link)

      // Show the popup
      popup.style.top = "50%";
      popup.style.left = "50%";
      overlay.style.display = "block";
      $(".step-count.active .step-number").css({
        "background-color": "#f0573a",
        color: "#F24423"
      });
      // Add other necessary adjustments and animations
    });
  }

  if (skipButton) {
    // Add a click event listener to the "Skip" button
    skipButton.addEventListener("click", function() {
      // Hide the popup
      popup.style.top = "-9999px";
      popup.style.left = "-9999px";
      overlay.style.display = "none";
      $(".step-count.active .step-number").css({
        "background-color": "#f0573a",
        color: "#F24423"
      });
      // Add other necessary adjustments and animations
    });
  }

  function handleInput(event) {
    const inputValue = event.target.value;
    const vpaLabel = $("#vpa-label");

    if (inputValue && inputValue.length) {
      vpaLabel.css({
        top: "5px",
        fontSize: "11px"
      });
    } else {
      vpaLabel.css({
        top: "67px",
        fontSize: "14px"
      });
    }
  }

  // Attach the input event handler
  $("#payer_vpa").on("input", handleInput);
});
//mobile design
$(window).resize(function() {
  desktopdesign();
});
function desktopdesign() {
  docsize = $(document).width();
  if (docsize > 991) {
    $(".qrimg").insertAfter(".qr-inst");
  } else {
    $(".qrimg").insertAfter(".radio-row");
  }
}

function generateQRCode(text) {
  var qrDataTxt = $(".qr-img").data("qr-data");
  if (qrDataTxt) {
    new QRCode(document.querySelector(".qr-code"), {
      text: `${qrDataTxt}`,
      width: 180, //default 128
      height: 180,
      colorDark: "#000000",
      colorLight: "#ffffff",
      correctLevel: QRCode.CorrectLevel.H
    });
  }
}

function validateField(e, option) {
  let btnId, errorCls;
  var identifier = e.target.value;
  switch (option) {
    case "imps":
      btnId = "impsSubmitBtn";
      break;
    case "upi":
      btnId = "utrSubmitBtn";
      break;
    case "rtgs":
      btnId = "rtgsSubmitBtn";
      break;
    case "neft":
      btnId = "neftSubmitBtn";
      break;
    case "vpa":
      btnId = "utrSubmitBtn";
      break;
    default:
      btnId = "impsSubmitBtn";
      break;
  }
  const submitBtn = document.getElementById(btnId);
  const validationFailed = document.getElementById("validationError");

  // Check if the entered value is a whole number
  if (!Number.isInteger(Number(identifier))) {
    validateInput(
      submitBtn,
      validationFailed,
      "Please enter a valid whole number."
    );
    return; // Exit the function as the validation failed
  }

  if (identifier.length === 12) {
    validateInput(submitBtn, validationFailed);
  } else {
    validateInput(
      submitBtn,
      validationFailed,
      "Please enter valid transaction id."
    );
  }
}

// function validateField(e, option) {
//   let btnId, errorCls;
//   // switch (option) {
//   //   case "imps":
//   //     btnId = "impsSubmitBtn";
//   //     errorCls = "impsTxIdValidationError";
//   //     break;
//   //   case "upi":
//   //     btnId = "utrSubmitBtn";
//   //     errorCls = "upiTxIdValidationError";
//   //     break;
//   //   case "rtgs":
//   //     btnId = "rtgsSubmitBtn";
//   //     errorCls = "rtgsTxIdValidationError";
//   //     break;
//   //   case "neft":
//   //     btnId = "neftSubmitBtn";
//   //     errorCls = "neftTxIdValidationError";
//   //     break;
//   //   case "vpa":
//   //     btnId = "utrSubmitBtn";
//   //     errorCls = "vpaTxIdValidationError";
//   //     break;
//   //   default:
//   //     btnId = "impsSubmitBtn";
//   //     errorCls = "impsTxIdValidationError";
//   //     break;
//   // }
//   var identifier = e.target.value;
//   const submitBtn = document.getElementById("submitBtn");
//   const validationFailed = document.getElementById(errorCls);
//   // if (option === "vpa") {
//   //   let validUpi = /^[a-zA-Z0-9.\-_]{2,49}@[a-zA-Z._]{2,49}$/.test(identifier);
//   //   if (validUpi) {
//   //     validateInput(submitBtn, validationFailed);
//   //   } else {
//   //     validateInput(submitBtn, validationFailed, "Please enter valid UPI ID.");
//   //   }
//   // } else {
//   if (identifier.length === 12) {
//     validateInput(submitBtn, validationFailed);
//   } else {
//     validateInput(
//       submitBtn,
//       validationFailed,
//       "Please enter valid transaction id."
//     );
//   }
//   // }
// }

function validateInput(submitBtn, validationFailed, error = null) {
  if (error) {
    $(".error-img").show();
    submitBtn.disabled = true;
    submitBtn.style.cursor = "not-allowed";
    submitBtn.style.backgroundColor = "#e0e0e0";
    validationFailed.innerText = error;
  } else {
    $(".error-img").hide();
    submitBtn.disabled = false;
    submitBtn.style.cursor = "pointer";
    submitBtn.style.backgroundColor = "#4caf50";
    validationFailed.innerText = "";
  }
}

function openUpiApp(upiLink, app) {
  var url = upiLink.replace("upi://", `${app}://`);
  window.location.href = url;
}

function copyTextToClipboard(text) {
  const textarea = document.createElement("textarea");
  textarea.value = text;
  document.body.appendChild(textarea);
  textarea.select();
  document.execCommand("copy");
  document.body.removeChild(textarea);

  // Show the toast message
  const toast = document.createElement("div");
  toast.classList.add("toast-container");
  toast.innerText = "Copied to clipboard";
  document.body.appendChild(toast);
  setTimeout(() => {
    document.body.removeChild(toast);
  }, 2000); // Toast message will disappear after 2 seconds (2000ms)
}

function extractUpiIdFromLink(upiLink) {
  const url = new URL(upiLink);
  const upiId = url.searchParams.get("pa");
  return upiId;
}
