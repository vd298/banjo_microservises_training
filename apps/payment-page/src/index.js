import express from "express";
import bodyParser from "body-parser";
import helmet from "helmet";
import config from "@lib/config";
import path from "path";
import controllers from "./lib/controllers";
import routes from "./routes";
const xss = require("xss-clean");
const compression = require("compression");
const cors = require("cors");
import UAParser from "ua-parser-js";

const app = express();
app.use((req, res, next) => {
  const parser = new UAParser(req.headers["user-agent"]);
  req.ua = parser.getResult();
  req.ip_address =
    req.headers["x-real-ip"] ||
    req.headers["x-forwarded-for"] ||
    req.connection.remoteAddress;
  next();
});
app.use(controllers.util.ServiceMethodMiddleware);
// set security HTTP headers
app.use(helmet());
// gzip compression
app.use(compression());
// enable cors
app.use(cors());
// sanitize request data
app.use(xss());

//https://stackoverflow.com/questions/33060044/express-csrf-token-validation
app.set("view engine", "pug");
app.set("views", path.join(__dirname, "views"));
app.use(bodyParser.json({ limit: "4mb" }));
app.use(
  bodyParser.urlencoded({ limit: "4mb", extended: true, parameterLimit: 8000 })
);
// Routes
app.use("/payment", routes);
app.use("/payment", express.static(path.join(__dirname, "public")));

const server = app.listen(config.paymentPage.port, () => {
  console.log("Payment page is running at %s", server.address().port);
});

const io = require("socket.io")(server);
io.on("connection", function(socket) {
  socket.on("upi-waiting", (transfer_id) => {
    console.log(`upi-waiting. Received Transfer id: ${transfer_id}`);
    if (transfer_id === "") {
    } else {
      controllers.payment.paymentResponse(socket, transfer_id);
    }
  });

  socket.on("disconnect", function() {});
});
