import Queue from "@lib/queue";

function ServiceMethodMiddleware(req, res, next) {
  req.callServiceMethod = async function(service, method, data) {
    let res = await Queue.newJob(service, {
      method: method,
      data,
      options: {
        ua: req.ua,
        ip: req.ip_address,
        source: "payment-page"
      }
    });
    return res;
  };
  next();
}
export default {
  ServiceMethodMiddleware
};
