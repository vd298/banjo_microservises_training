import Queue from "@lib/queue";

var symbols = {
  USD: "$",
  EUR: "€",
  GBP: "£",
  INR: "₹"
  // Add more currency codes and their corresponding symbols as needed
};

function extractUpiIdFromLink(upiLink) {
  const url = new URL(upiLink);
  const upiId = url.searchParams.get("pa");
  return upiId;
}

function paymentRedirect(req, res) {
  res.render("payment-redirect", { params: req.params });
}
async function paymentResponse(socket, transferId) {
  Queue.subscribe(
    `UPI-Payment-Status.*`,
    async ({ transfer_id = null, status = "REJECTED" }) => {
      console.log(
        `UPI-Payment-Status. Received Transfer id: ${transfer_id} with status ${status}`
      );
      if (transfer_id == transferId) {
        if (status === "COMPLETED") {
          socket.emit("transaction-successful", transfer_id);
        } else {
          socket.emit("transaction-failed", transfer_id);
        }
      }
    }
  );
}
async function paymentSubmit(req, res) {
  var logoUrl;
  try {
    console.log("req.file", req.receipt);
    if (req.file) {
      const params = {
        Bucket: process.env.BUCKET_NAME,
        Key: req.file.originalname,
        Body: req.file.buffer
      };

      const data = await s3.upload(params).promise();
      logoUrl = data.Location;
      console.log(
        "File: payment.js, Func: paymentSubmit, Saved url to s3, logoUrl: ",
        logoUrl
      );
      let txRes = await req.callServiceMethod(
        "transaction-service",
        "txActivitySaveReceipt",
        {
          transfer_id: req.body.transfer_id,
          url: logoUrl
        }
      );
      if (txRes.result) {
        console.log("Saved receipt successfully");
      } else {
        res.json({ success: false, message: "Failed in saving receipt" });
      }
    }
  } catch (error) {
    console.error(error);
    res.json({ success: false, message: error.message });
  }
  if (req.body.vpa && req.body.vpa !== "") {
    if (/^[a-zA-Z0-9.\-_]{2,49}@[a-zA-Z._]{2,49}$/.test(req.body.vpa)) {
      let txRes = await req.callServiceMethod(
        "transaction-service",
        "txActivitySubmitVPA",
        {
          transfer_id: req.body.transfer_id,
          vpa: req.body.vpa
        }
      );
      if (txRes.result && txRes.result.validationError) {
        res.render("payment", {
          invalidValue: req.body.vpa,
          validationError: {
            payment_type: "UPI",
            message: txRes.result.validationError.split(":")[1]
          },
          payment: {
            merchant_name: txRes.result.paymentDetails.merchant_name,
            merchant_acc_name:
              txRes.result.paymentDetails.merchant_account_name,
            id: txRes.result.paymentDetails.id,
            order_num: txRes.result.paymentDetails.order_num,
            amount: txRes.result.paymentDetails.dst_amount,
            currency: txRes.result.paymentDetails.dst_currency,
            description: txRes.result.paymentDetails.description,
            fc_merchant_id: txRes.result.paymentDetails.fc_merchant_id,
            symbols
          },
          paymentOptions: txRes.result.paymentDetails.payment_options.map(
            (option) => {
              return {
                type: option.protocol_type,
                label: `${option.protocol_type} Transfer`,
                options: {
                  selected: option.protocol_type == "UPI" ? true : false,
                  upi_link:
                    option.protocol_type == "UPI"
                      ? `upi://pay?pa=${option.identifier}&pn=${txRes.result.merchant_name}&tn=${txRes.result.ref_num}&am=${txRes.result.src_amount}&cu=${txRes.result.src_currency}`
                      : undefined,
                  bank_name: option.bank_name,
                  account_holder: option.account_name,
                  account_number: option.acc_no,
                  code: option.identifier
                  // account_type: "SAVING"
                }
              };
            }
          )
        });
      } else {
        res.render("tx_waiting", {
          payment: {
            merchant_name: txRes.result.merchant_name,
            merchant_acc_name: txRes.result.merchant_account_name,
            id: txRes.result.id,
            order_num: txRes.result.order_num,
            amount: txRes.result.dst_amount,
            currency: txRes.result.dst_currency,
            description: txRes.result.description
          }
        });
      }
    } else {
      res.render("error", {
        title: "Invalid UPI Address",
        message: "Please check your UPI ID again."
      });
    }
  } else if (!!req.body.brn && req.body.brn !== "") {
    let txRes = await req.callServiceMethod(
      "transaction-service",
      "txActivitySubmitBRN",
      {
        transfer_id: req.body.transfer_id,
        note: req.body.payment_type,
        brn: req.body.brn,
        payer_vpa: req.body.payer_vpa
      }
    );
    if (txRes.result) {
      if (txRes.result.success) {
        res.render("tx_status", {
          // success: txRes.result,
          payment: {
            merchant_name: txRes.result.merchant_name,
            merchant_acc_name: txRes.result.merchant_account_name,
            id: txRes.result.id,
            order_num: txRes.result.order_num,
            amount: txRes.result.amount,
            currency: txRes.result.currency,
            description: txRes.result.description,
            return_url: txRes.result.data.return_url,
            theme: txRes.result.payment_page_theme,
            symbols
          }
        });
      } else if (txRes.result.validationError) {
        res.render("payment", {
          payment_type: req.body.payment_type,
          invalidValue: req.body.brn,
          validationError: {
            payment_type: req.body.payment_type,
            message: txRes.result.validationError.split(":")[1]
          },
          payment: {
            merchant_name: txRes.result.paymentDetails.merchant_name,
            merchant_acc_name:
              txRes.result.paymentDetails.merchant_account_name,
            id: txRes.result.paymentDetails.id,
            order_num: txRes.result.paymentDetails.order_num,
            amount: txRes.result.paymentDetails.dst_amount,
            currency: txRes.result.paymentDetails.dst_currency,
            description: txRes.result.paymentDetails.description,
            fc_merchant_id: txRes.result.paymentDetails.fc_merchant_id,
            symbols
          },
          paymentOptions: txRes.result.paymentDetails.payment_options.map(
            (option) => {
              return {
                type: option.protocol_type,
                label: `${option.protocol_type} Transfer`,
                options: {
                  selected:
                    option.protocol_type == req.body.payment_type
                      ? true
                      : false,
                  upi_link:
                    option.protocol_type == "UPI"
                      ? `upi://pay?pa=${option.identifier}&pn=${txRes.result.merchant_name}&tn=${txRes.result.ref_num}&am=${txRes.result.src_amount}&cu=${txRes.result.src_currency}`
                      : undefined,
                  bank_name: option.bank_name,
                  account_holder: option.account_name,
                  account_number: option.acc_no,
                  code: option.identifier
                  // account_type: "SAVING"
                }
              };
            }
          )
        });
      } else {
        res.render("paymentfailed", {
          payment: {
            merchant_name: txRes.result.paymentDetails.merchant_name,
            order_id: txRes.result.paymentDetails.order_num,
            transfer_id: txRes.result.paymentDetails.id,
            description: txRes.result.paymentDetails.description,
            theme: txRes.result.paymentDetails.payment_page_theme,
            symbols
          }
        });
      }
    } else {
      let payment = { symbols };
      if (txRes.result) {
        payment = {
          merchant_name: txRes.result.paymentDetails.merchant_name,
          order_id: txRes.result.paymentDetails.order_num,
          transfer_id: txRes.result.paymentDetails.id,
          description: txRes.result.paymentDetails.description,
          return_url: txRes.result.data.return_url,
          theme: txRes.result.paymentDetails.payment_page_theme,
          symbols
        };
      }
      res.render("paymentfailed", {
        error: txRes.error,
        payment
      });
    }
  }
}

async function rejectTransaction(req, res) {
  let txRes = await req.callServiceMethod(
    "transaction-service",
    "txActivityReject",
    {
      transfer_id: req.body.transfer_id,
      activity_action: "REJECTED",
      action_type: "USER",
      note: req.body.note || ""
    }
  );
  if (txRes.result) {
    if (txRes.result.status === "REJECTED") {
      res.render("paymentfailed", {
        payment: {
          merchant_name: txRes.result.paymentDetails.merchant_name,
          order_id: txRes.result.paymentDetails.order_num,
          transfer_id: txRes.result.paymentDetails.id,
          description: txRes.result.paymentDetails.description,
          return_url: txRes.result.data.return_url,
          theme: txRes.result.paymentDetails.payment_page_theme,
          symbols
        }
      });
    } else {
      res.render("paymentfailed", {
        error: txRes.error,
        payment: null
      });
    }
  }
}

async function paymentPage(req, res) {
  let txRes = await req.callServiceMethod(
    "transaction-service",
    "txActivityPageOpened",
    {
      transfer_id: req.body.transfer_id
    }
  );
  if (txRes.result) {
    if (txRes.result.success) {
      let paymentOptions = await createPaymentOptionObject(txRes);
      res.render("payment", {
        payment: {
          merchant_name: txRes.result.merchant_name,
          merchant_acc_name: txRes.result.merchant_account_name,
          id: txRes.result.id,
          order_num: txRes.result.order_num,
          amount: txRes.result.dst_amount,
          currency: txRes.result.dst_currency,
          description: txRes.result.description,
          fc_merchant_id: txRes.result.fc_merchant_id,
          symbols,
          theme: txRes.result.payment_page_theme
        },
        paymentOptions
      });
    } else {
      res.render("paymentfailed", {
        error: txRes.result.error,
        payment: {
          merchant_name: txRes.result.paymentDetails.merchant_name,
          order_id: txRes.result.paymentDetails.order_num,
          transfer_id: txRes.result.paymentDetails.id,
          description: txRes.result.paymentDetails.description,
          return_url: txRes.result.data.return_url,
          theme: txRes.result.paymentDetails.payment_page_theme,
          symbols
        }
      });
    }
  } else {
    res.render("paymentfailed", {
      error: txRes.error,
      payment: null
    });
  }
}
async function createPaymentOptionObject(txRes) {
  if (
    txRes &&
    txRes.result &&
    txRes.result.data &&
    txRes.result.data.digiPaymentBankAcc
  ) {
    let resultArray = [];
    let txResObj = txRes.result.data.digiPaymentBankAcc;
    if (txResObj.account_number !== null && txResObj.bank_ifsc !== null) {
      let wire_protocols = ["RTGS", "NEFT", "IMPS"];
      for (let wire_protocol of wire_protocols) {
        let optionDataObj = {
          type: wire_protocol,
          label: `${wire_protocol} Transfer`,
          options: {
            selected: resultArray.length == 0 ? true : false,
            upi_link: undefined,
            bank_name: txResObj.name,
            account_holder: txResObj.account_name,
            account_number: txResObj.account_number,
            code: txResObj.bank_ifsc
          }
        };
        resultArray.push(optionDataObj);
      }
    }
    if (txResObj.dynamic_qr !== null) {
      const decodedUrl = decodeURIComponent(txResObj.dynamic_qr);

      const amount = txRes.result.src_amount; // Replace "------" with this amount

      const replacedUrl = decodedUrl.replace(/am=[^&]*/, `am=${amount}`);

      const startIndex = replacedUrl.indexOf("data=") + 5;
      const QRLink = replacedUrl.slice(startIndex);

      let optionDataObj = {
        type: "UPI",
        label: `UPI Transfer`,
        options: {
          selected: resultArray.length == 0 ? true : false,
          upi_link: QRLink,
          bank_name: txResObj.name,
          account_holder: txResObj.account_name,
          account_number: txResObj.account_number,
          code: txResObj.bank_upid
        }
      };
      resultArray.push(optionDataObj);
    }
    return resultArray;
  } else {
    return txRes.result.payment_options.map((option, index) => {
      return {
        type: option.protocol_type,
        label: `${option.protocol_type} Transfer`,
        options: {
          selected: index == 0 ? true : false,
          upi_link:
            option.protocol_type == "UPI"
              ? `upi://pay?pa=${option.identifier}&pn=${txRes.result.merchant_name}&tn=${txRes.result.ref_num}&am=${txRes.result.src_amount}&cu=${txRes.result.src_currency}`
              : undefined,
          bank_name: option.bank_name,
          account_holder: option.account_name,
          account_number: option.acc_no,
          code: option.identifier
          // account_type: "SAVING"
        }
      };
    });
  }
}

async function savePayerDetails(req, res) {
  let txRes = await req.callServiceMethod(
    "transaction-service",
    "txActivitySavePayerDetails",
    {
      data: req.body
    }
  );
  if (txRes.result) {
    if (txRes.result.success) {
      let paymentOptions = await createPaymentOptionObject(txRes);
      res.render("payment", {
        payment: {
          merchant_name: txRes.result.merchant_name,
          merchant_acc_name: txRes.result.merchant_account_name,
          id: txRes.result.id,
          order_num: txRes.result.order_num,
          amount: txRes.result.dst_amount,
          currency: txRes.result.dst_currency,
          description: txRes.result.description,
          fc_merchant_id: txRes.result.fc_merchant_id,
          symbols,
          theme: txRes.result.payment_page_theme
        },
        paymentOptions
      });
    } else {
      res.render("paymentfailed", {
        error: txRes.result.error,
        payment: {
          merchant_name: txRes.result.merchant_name,
          order_id: txRes.result.order_num,
          transfer_id: txRes.result.id,
          description: txRes.result.description,
          return_url: txRes.result.data.return_url,
          theme: txRes.result.payment_page_theme,
          symbols
        }
      });
    }
  } else {
    res.render("paymentfailed", {
      error: txRes.error,
      payment: null
    });
  }
}
export default {
  paymentSubmit,
  paymentRedirect,
  paymentPage,
  paymentResponse,
  rejectTransaction,
  savePayerDetails
};
