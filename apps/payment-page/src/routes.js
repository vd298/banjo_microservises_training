import express from "express";
import controllers from "./lib/controllers";
import multer from "multer";
const router = express.Router();
var limits = {
  files: 1,
  fileSize: 12 * 1024 * 1024
};
const upload = multer({
  storage: multer.memoryStorage(),
  limits
});
const fileUploadErrorHandler = (err, req, res, next) => {
  if (err) {
    res.json({ success: false, err });
  } else {
    next();
  }
};

router.get("/:txId", controllers.payment.paymentRedirect);
router.post("/execute", controllers.payment.paymentPage);
router.post(
  "/execute/submit",
  upload.single("receipt"),
  fileUploadErrorHandler,
  controllers.payment.paymentSubmit
);
router.post("/execute/reject", controllers.payment.rejectTransaction);
router.post("/execute/payer_details", controllers.payment.savePayerDetails);

export default router;
