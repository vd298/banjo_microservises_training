import express from "express";
import bodyParser from "body-parser";
import helmet from "helmet";
import config from "@lib/config";
import path from "path";
import controllers from "./lib/controllers";
import routes from "./routes";
const xss = require("xss-clean");
const compression = require("compression");
const cors = require("cors");
import UAParser from "ua-parser-js";
import multer from "multer";

import { capture } from "@lib/log";
capture();

const app = express();
const upload = multer();

app.use(upload.fields([]), (req, res, next) => {
  const parser = new UAParser(req.headers["user-agent"]);
  req.ua = parser.getResult();
  req.ip_address =
    req.headers["x-real-ip"] ||
    req.headers["x-forwarded-for"] ||
    req.connection.remoteAddress;
  next();
});
// set security HTTP headers
app.use(helmet());
// gzip compression
app.use(compression());
// // enable cors
// app.use(cors());
// sanitize request data
app.use((req, resp, next) => {
  console.log(`Webhook Request`, req.body);
  next();
});
app.use(xss());
app.use(bodyParser.json({ limit: "4mb" }));
app.use(
  bodyParser.urlencoded({ limit: "4mb", extended: true, parameterLimit: 8000 })
);
// Routes
app.use("/webhooks", routes);

const server = app.listen(config.webhooks.port, () => {
  console.log("Webhooks is running at %s", server.address().port);
});
