import Queue from "@lib/queue";

async function handleXazurPayStatus(req, res) {
  console.log(
    `Webhooks. Func:handleXazurPayStatus. Received Webhook call`,
    req.body
  );
  if (req && req.body) {
    try {
      Queue.newJob("merchant-service", {
        method: "xazurpayStatus",
        data: req.body
      });
      res.sendStatus(200);
    } catch (error) {
      throw new Error(error);
    }
  }
}
module.exports = {
  handleXazurPayStatus
};
