import Queue from "@lib/queue";

async function handleFreechargeStatus(req, res) {
  console.log(`Webhooks. Func:handleFreechargeStatus. Received Webhook call`);
  if (req && req.body) {
    console.log(JSON.stringify(req.body, null, 2));
    console.log(process.env.NODE_ENV);
  }
  if (
    req &&
    req.body &&
    process &&
    process.env &&
    ["development", "staging"].indexOf(process.env.NODE_ENV) > -1
  )
    console.log(
      `Webhook Received with data:`,
      JSON.stringify(req.body, null, 2)
    );
  try {
    Queue.newJob("transaction-service", {
      method: "paymentStatus",
      data: req.body,
      options: {
        ua: req.ua,
        ip: req.ip_address,
        source: "webhook"
      }
    });
    res.sendStatus(200);
  } catch (error) {
    throw new Error(error);
  }
}
module.exports = {
  handleFreechargeStatus
};
