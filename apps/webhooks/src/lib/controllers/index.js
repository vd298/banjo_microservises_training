import freecharge from "./freecharge-handler";
import xazurpay from "./xazurpay-handler";
import digiPayment from "./digiPayment-handler";

export default {
  freecharge,
  xazurpay,
  digiPayment
};
