import Queue from "@lib/queue";

async function handleDigiPaymentStatus(req, res) {
  console.log(
    `Webhooks. Func:handleDigiPaymentStatus. Received Webhook call`,
    req.body
  );
  if (req && req.body) {
    try {
      Queue.newJob("merchant-service", {
        method: "digiPaymentStatus",
        data: req.body
      });
      res.sendStatus(200);
    } catch (error) {
      throw new Error(error);
    }
  }
}
module.exports = {
  handleDigiPaymentStatus
};
