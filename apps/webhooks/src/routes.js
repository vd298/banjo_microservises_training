import express from "express";
import controllers from "./lib/controllers";
const router = express.Router();

router.post(
  "/freecharge-status",
  controllers.freecharge.handleFreechargeStatus
);

router.post("/xazurpay-status", controllers.xazurpay.handleXazurPayStatus);
router.post(
  "/digiPayment-status",
  controllers.digiPayment.handleDigiPaymentStatus
);

export default router;
