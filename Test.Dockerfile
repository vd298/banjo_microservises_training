FROM node:18-alpine

WORKDIR /app

COPY packages /app/packages
COPY lerna.json /app/lerna.json
COPY package.json /app/package.json
COPY node_modules /app/node_modules

COPY docs /app/docs
COPY src /app/src

RUN mkdir -p /app/docs/json