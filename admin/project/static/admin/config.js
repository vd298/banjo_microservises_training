__CONFIG__ = {
  FirstRedirect: "Crm.modules.accounts.view.AccountsGrid",

  LoginUrl: "/Crm.Admin.login/",

  MainToolbar: "main.MainToolbar",

  LogoText: "Banjo System",

  BlockchainUrl: "https://www.blockchain.com/explorer/transactions/",
  WazirxUrl: "https://wazirx.com/",

  NavigationTree: [
    {
      text: "Merchants",
      view: "Crm.modules.accounts.view.AccountsGrid",
      iconCls: "x-fa fa-users"
    },
    {
      text: "Transfers",
      view: "Crm.modules.Transfers.view.TransfersGrid",
      iconCls: "x-fa fa-list"
    },
    {
      text: "Transfers Lite",
      view: "Crm.modules.Transfers.view.TransfersLiteGrid",
      iconCls: "x-fa fa-list"
    },
    {
      text: "Global Limits",
      view: "Crm.modules.globalLimits.view.GlobalLimitGrid",
      iconCls: "x-fa fa-list"
    },
    {
      text: "Bank Account Pool",
      iconCls: "x-fa fa-briefcase",
      children: [
        {
          text: "All Bank Accounts",
          view: "Crm.modules.bankAccounts.view.BankAccountsGrid",
          iconCls: "x-fa fa-venus-mars"
        },
        {
          text: "Assigned Accounts",
          view: "Crm.modules.accountsPool.view.AssignedAccountsGrid",
          iconCls: "x-fa fa-link"
        },
        {
          text: "Unassigned Accounts",
          view: "Crm.modules.accountsPool.view.UnassignedAccountsGrid",
          iconCls: "x-fa fa-unlink"
        }
      ]
    },
    {
      text: "Banks",
      iconCls: "x-fa fa-bank",
      children: [
        {
          text: "Banks",
          view: "Crm.modules.banks.view.BanksGrid",
          iconCls: "x-fa fa-bank"
        },
        {
          text: "Payment Protocols",
          view: "Crm.modules.paymentProtocols.view.PaymentProtocolsGrid",
          iconCls: "x-fa fa-credit-card"
        },
        {
          text: "Payment Providers",
          view: "Crm.modules.paymentProvider.view.paymentProviderGrid",
          iconCls: "x-fa fa-usd"
        }
      ]
    },
    {
      text: "Banks Tx Sync",
      iconCls: "x-fa fa-bank",
      children: [
        {
          text: "Banks Configuration",
          view: "Crm.modules.BankConfig.view.BankConfigGrid",
          iconCls: "x-fa fa-bank"
        }
      ]
    },
    ,
    {
      text: "Tariffs",
      iconCls: "x-fa fa-money",
      children: [
        {
          text: "Realms",
          view: "Crm.modules.realm.view.RealmGrid",
          iconCls: "x-fa fa-venus-mars"
        },
        {
          text: "Plans",
          view: "Crm.modules.tariffs.view.PlansGrid",
          iconCls: "x-fa fa-umbrella"
        },
        {
          text: "Tariffs",
          view: "Crm.modules.tariffs.view.TariffsGrid",
          iconCls: "x-fa fa-money"
        },
        {
          text: "Triggers",
          view: "Crm.modules.tariffs.view.TriggersGrid",
          iconCls: "x-fa fa-bolt"
        },
        {
          text: "Views",
          view: "Crm.modules.viewset.view.ViewsetGrid",
          iconCls: "x-fa fa-eye"
        }
      ]
    },
    {
      text: "Settlements",
      view: "Crm.modules.settlements.view.SettlementsGrid",
      iconCls: "x-fa fa-clock-o"
    },
    {
      text: "Transfers Fee Report",
      view: "Crm.modules.transfersFeeReport.view.TransfersFeeReportGrid",
      iconCls: "x-fa fa-usd"
    },
    {
      text: "Lost Transactions",
      view: "Crm.modules.lostTransactions.view.LostTransactionsGrid",
      iconCls: "x-fa fa-bank"
    },
    {
      text: "API credentials",
      view: "Crm.modules.apicredentials.view.ApiCredentialsGrid",
      iconCls: "x-fa fa-users"
    },
    {
      text: "All Users",
      iconCls: "x-fa fa-users",
      children: [
        {
          text: "Api Users",
          view: "#Crm.modules.allUsers.view.AllAPIUserGrid",
          iconCls: "x-fa fa-users"
        },
        {
          text: "Portal Users",
          view: "#Crm.modules.allUsers.view.AllPortalUserGrid",
          iconCls: "x-fa fa-users"
        }
      ]
    },
    {
      text: "Settings",
      iconCls: "x-fa fa-wrench",
      children: [
        {
          text: "Admin Users",
          view: "Crm.modules.users.view.UsersGrid",
          iconCls: "x-fa fa-users"
        },
        {
          text: "Admin Roles",
          view: "Crm.modules.users.view.GroupsGrid",
          iconCls: "x-fa fa-lock"
        },
        {
          text: "User Roles",
          view: "Crm.modules.roles.view.RolesGrid",
          iconCls: "x-fa fa-users"
        },
        {
          text: "Workflow Settings",
          view: "Crm.modules.signset.view.SignsetGrid",
          iconCls: "x-fa fa-briefcase"
        },
        {
          text: "Email Transporters",
          view: "Crm.modules.transporters.view.TransporterGrid",
          iconCls: "x-fa fa-list"
        },
        {
          text: "Templates",
          view: "Crm.modules.letterTemplates.view.letterTemplatesGrid",
          iconCls: "x-fa fa-envelope"
        },
        {
          text: "Categories",
          view: "Crm.modules.categories.view.CategoryGrid",
          iconCls: "x-fa fa-list"
        },
        {
          text: "Countries",
          view: "Crm.modules.countries.view.CountriesGrid",
          iconCls: "x-fa fa-globe"
        },
        {
          text: "Currency",
          view: "Crm.modules.currency.view.CurrencyGrid",
          iconCls: "x-fa fa-dollar"
        },
        {
          text: "Files",
          view: "Crm.modules.files.view.FileGrid",
          iconCls: "x-fa fa-file"
        },
        {
          text: "Account Categories",
          view: "Crm.modules.accountCategories.view.accountCategoriesGrid",
          iconCls: "x-fa fa-list"
        },
        {
          text: "Merchant accounts mapping",
          view:
            "Crm.modules.merchantAccountsMapping.view.MerchantAccountsMappingGrid",
          iconCls: "x-fa fa-credit-card"
        }
      ]
    }
  ],
  downloadFileLink:
    location.protocol == "http:"
      ? location.protocol + "//" + location.hostname + ":8012/download"
      : location.protocol + "//" + location.hostname + "/download",

  buildTypeCombo(config) {
    var me = this;
    let combo = {
      name: "type",
      labelWidth: 150,
      fieldLabel: D.t("Type"),
      xtype: "dependedcombo",
      queryMode: "all",
      forceSelection: true,
      triggerAction: "all",
      allowBlank: false,
      editable: false,
      valueField: "key",
      displayField: "key",
      modelConfig: { enum_name: "enum_accounts_type" },
      dataModel: "Crm.modules.Util.model.FetchComboRecords",
      operator: "eq",
      listeners: {
        afterrender(e, eOpts) {
          me.setComponentEmptyText(e, eOpts);
        },
        change(e, newVal, oldVal, eOpts) {
          if (e.getValue() === null && e.getRawValue() == "") {
            e.lastSelectedRecords = [];
            e.clearValue();
          }
        }
      }
    };
    Object.assign(combo, config);
    return combo;
  },

  buildStatusCombo(config) {
    var me = this;
    let combo = {
      name: "status",
      labelWidth: 150,
      fieldLabel: D.t("Status"),
      xtype: "dependedcombo",
      queryMode: "all",
      forceSelection: true,
      triggerAction: "all",
      editable: false,
      allowBlank: false,
      valueField: "key",
      displayField: "key",
      modelConfig: { enum_name: "enum_accounts_status" },
      dataModel: "Crm.modules.Util.model.FetchComboRecords",
      operator: "eq",
      listeners: {
        afterrender(e, eOpts) {
          me.setComponentEmptyText(e, eOpts);
        },
        change(e, newVal, oldVal, eOpts) {
          if (e.getValue() === null && e.getRawValue() == "") {
            e.lastSelectedRecords = [];
            e.clearValue();
          }
        }
      }
    };
    Object.assign(combo, config);
    return combo;
  },

  buildRealmsCombo(config) {
    var me = this;
    let combo = {
      name: "realm_id",
      labelWidth: 150,
      fieldLabel: D.t("Realm"),
      xtype: "dependedcombo",
      queryMode: "all",
      forceSelection: true,
      triggerAction: "all",
      editable: false,
      allowBlank: false,
      valueField: "id",
      displayField: "name",
      modelConfig: {
        collection: "realms",
        fieldSet: "id,name",
        removedCheck: true
      },
      dataModel: "Crm.modules.Util.model.FetchComboRecords",
      operator: "eq",
      valueNotFoundText: "REALM NOT FOUND",
      autoLoadOnValue: true,
      listeners: {
        afterrender(e, eOpts) {
          me.setComponentEmptyText(e, eOpts);
        },
        change(e, newVal, oldVal, eOpts) {
          if (e.getValue() === null && e.getRawValue() == "") {
            e.lastSelectedRecords = [];
            e.clearValue();
          }
        }
      }
    };
    Object.assign(combo, config);
    return combo;
  },

  buildPlansCombo(config) {
    var me = this;
    let combo = {
      name: "plan_id",
      labelWidth: 150,
      fieldLabel: D.t("Plans"),
      xtype: "dependedcombo",
      queryMode: "all",
      forceSelection: true,
      triggerAction: "all",
      editable: false,
      allowBlank: false,
      valueField: "key",
      displayField: "name",
      modelConfig: { collection: "tariffplans", fieldSet: "id,name" },
      dataModel: "Crm.modules.Util.model.FetchComboRecords",
      operator: "eq",
      listeners: {
        afterrender(e, eOpts) {
          me.setComponentEmptyText(e, eOpts);
        },
        change(e, newVal, oldVal, eOpts) {
          if (e.getValue() === null && e.getRawValue() == "") {
            e.lastSelectedRecords = [];
            e.clearValue();
          }
        }
      }
    };
    Object.assign(combo, config);
    return combo;
  },

  buildCountryCombo(config) {
    let combo = {
      xtype: "dependedcombo",
      modelConfig: { collection: "countries", fieldSet: "id,abbr2,name" },
      name: "country",
      editable: true,
      fieldLabel: "Country",
      allowBlank: false,
      forceSelection: true,
      clearValueOnEmpty: true,
      valueField: "abbr2",
      displayField: "name",
      parentEl: null,
      parentField: null,
      dataModel: "Crm.modules.Util.model.FetchComboRecords",
      listeners: {
        afterrender(e, eOpts) {
          __CONFIG__.setComponentEmptyText(e, eOpts);
        },
        change(e, newVal, oldVal, eOpts) {
          if (e.getValue() === null && e.getRawValue() == "") {
            e.lastSelectedRecords = [];
            e.clearValue();
          }
        }
      }
    };
    Object.assign(combo, config);
    return combo;
  },

  getTxTransferType: function() {
    return [
      { type: "PAYOUT" },
      { type: "PAYIN" },
      { type: "COLLECTION" },
      { type: "REFUND" },
      { type: "RETURN" },
      { type: "REVERSE" },
      { type: "FEE" },
      { type: "REVERSE_FEE" },
      { type: "SETTLEMENT" },
      { type: "DIRECT_DEPOSIT" }
    ];
  },

  getTxActivityType: function() {
    return [
      { desc: "Collection Request", type: "REQUEST" },
      { desc: "Settlement Request", type: "REQUEST" },
      { desc: "Payout Request", type: "REQUEST" },
      { desc: "Payment Page Opened", type: "OPEN" },
      { desc: "BRN Submitted", type: "BRN_SUBMIT" },
      { desc: "VPA Submitted", type: "VPA_SUBMIT" },
      // { desc: "Payment Page Redirected", type: "REDIRECT" },
      { desc: "Settlement Approved", type: "SETTLEMENT_APPROVED" },
      { desc: "Internal Funds shift", type: "INTERNAL_FUNDS_SHIFT" },
      { desc: "Exchange Withdraw", type: "EXCHANGE_WITHDRAW" },
      { desc: "Transfer Approved", type: "APPROVED" },
      { desc: "Transfer Pending", type: "PENDING" },
      { desc: "Transfer Reverse", type: "REVERSE" },
      { desc: "Transfer Rejected", type: "REJECTED" },
      { desc: "Charge Fee", type: "FEE" },
      { desc: "Conversion Withdraw", type: "EXCH_WITHDRAW" },
      { desc: "Conversion Deposit", type: "EXCH_DEPOSIT" },
      { desc: "Return Charge Fee", type: "REVERSE_FEE" },
      { desc: "Payment Page Closed", type: "CLOSED" }
    ];
  },

  getTxActivityTypeGrid: function() {
    return [
      { desc: "Request", type: "REQUEST" },
      { desc: "Payment Page Opened", type: "OPEN" },
      { desc: "BRN Submitted", type: "BRN_SUBMIT" },
      { desc: "VPA Submitted", type: "VPA_SUBMIT" },
      // { desc: "Payment Page Redirected", type: "REDIRECT" },
      { desc: "Settlement Approved", type: "SETTLEMENT_APPROVED" },
      { desc: "Internal Funds shift", type: "INTERNAL_FUNDS_SHIFT" },
      { desc: "Exchange Withdraw", type: "EXCHANGE_WITHDRAW" },
      { desc: "Transfer Approved", type: "APPROVED" },
      { desc: "Transfer Pending", type: "PENDING" },
      { desc: "Transfer Reverse", type: "REVERSE" },
      { desc: "Transfer Rejected", type: "REJECTED" },
      { desc: "Charge Fee", type: "FEE" },
      { desc: "Conversion Withdraw", type: "EXCH_WITHDRAW" },
      { desc: "Conversion Deposit", type: "EXCH_DEPOSIT" },
      { desc: "Return Charge Fee", type: "REVERSE_FEE" },
      { desc: "Payment Page Closed", type: "CLOSED" }
    ];
  },

  getReportTypeCombo: function() {
    return [
      // { desc: "PDF", type: "PDF" },
      { desc: "Excel", type: "xlsx" }
    ];
  },
  getBulkTemplateHeaders: function(headerStore) {
    return Ext.create("Ext.data.Store", {
      fields: ["_id", "name"],
      name: "headerStore",
      data: [
        { _id: "transfer_id", name: "Transfer id" },
        { _id: "src_currency", name: "Source currency" },
        { _id: "src_amount", name: "Source amount" },
        { _id: "dst_currency", name: "Destination currency" },
        { _id: "dst_amount", name: "Destination amount" },
        { _id: "transfer_type", name: "Transfer type" },
        { _id: "ref_num", name: "Reference number" },
        { _id: "order_num", name: "Order number" },
        { _id: "is_exchange", name: "Is exchange transfer" },
        { _id: "exchange_id", name: "Exchange id" },
        { _id: "counter_party_name", name: "Counter party name" },
        {
          _id: "counter_party_bank_name",
          name: "Counter party bank name"
        },
        {
          _id: "counter_party_bank_code",
          name: "Counter party bank code"
        },
        { _id: "src_bank_short_name", name: "Source bank short name" },
        { _id: "src_bank_name", name: "Source bank name" },
        { _id: "src_bank_address", name: "Source bank address" },
        { _id: "src_bank_city", name: "Source bank city" },
        { _id: "src_bank_country", name: "Source bank country" },
        {
          _id: "dst_bank_short_name",
          name: "Destination bank short name"
        },
        { _id: "dst_bank_name", name: "Destination bank name" },
        { _id: "dst_bank_address", name: "Destination bank address" },
        { _id: "dst_bank_city", name: "Destination bank city" },
        { _id: "dst_bank_country", name: "Destination bank country" },
        { _id: "beneficiary_vpa", name: "Beneficiary VPA" },
        { _id: "beneficiary_ifsc", name: "Beneficiary IFSC" },
        { _id: "beneficiary_acc_no", name: "Beneficiary Account Number" },
        { _id: "beneficiary_first_name", name: "Beneficiary First Name" }
      ]
    });
  },

  setComponentEmptyText: function(e, eOpts) {
    let me = this,
      str = ``;
    if (e && e.xtype == "combo") str += `Select ${e.fieldLabel}`;
    else if (e.fieldLabel) str += e.fieldLabel;
    else if (e.name) str += e.name;
    if (e.maxLength != undefined && e.maxLength < 1001) {
      str += ` (Max characters: ${e.maxLength})`;
    }
    return e.setEmptyText(str);
  },
  toggleBankStatusById: function(e, form, res) {
    let status = "";
    if (res) {
      form.down("[name=bank_account_id]").store.reload();
      if (res && res.length && res[0].status === "ACTIVE") {
        form.down("[name=bank_account_id]").validator = function(value) {
          return true;
        };
        form
          .down("[name=toggleStatus]")
          .setText("Update Status: Inactive")
          .setHidden(false);
        status = res[0].status;
      } else if (res && res.length && res[0].status === "INACTIVE") {
        form.down("[name=bank_account_id]").validator = function(value) {
          return "Bank account status should be active";
        };
        form
          .down("[name=toggleStatus]")
          .setText("Update Status: Active")
          .setHidden(false);
        status = res[0].status;
      }
    }
    return status;
  },
  setAssignedAccountMessage: function(e, form, res) {
    let message = "";
    if (res && res.length) {
      if (res[0].status === "ACTIVE") {
        e.validator = function(value) {
          return true;
        };
      } else {
        e.validator = function(value) {
          return "Bank account status should be active";
        };
      }
      if (res[0].active_relation) {
        message = "The bank account is shared with";
        for (let i = 0; i < res.length; i++) {
          message += ` ${res[i].merchant_name}`;
          if (i >= 2 && i !== res.length - 1) {
            message += ", ....";
            break;
          } else if (i !== res.length - 1) {
            message += ",";
          }
        }
        selectedmerchName = form.down("[name=account]").getDisplayValue();
        if (selectedmerchName.length) {
          selectedmerchName = ` do you want to share with`;
          selectedmerchName += ` ${form
            .down("[name=account]")
            .getDisplayValue()}?`;
        }
        form
          .down("[name=message]")
          .setHidden(false)
          .setText(`${message} ${selectedmerchName}`);
      } else {
        message = "";
        selectedmerchName = "";
        form
          .down("[name=message]")
          .setHidden(true)
          .setText("");
      }
    }
    return message;
  },
  checkAlreadyAssigned(me, mes) {
    return new Promise((resolve) => {
      if (mes) {
        Ext.Msg.show({
          title: "Already assign bank account",
          message: mes,
          buttons: Ext.Msg.YESNO,
          icon: Ext.Msg.QUESTION,
          fn: function(btn) {
            if (btn === "yes") {
              return resolve(true);
            }
            resolve(false);
          }
        });
      } else resolve(true);
    });
  },
  formatCurrency: function(amount, precision, currency) {
    let isCrypto = false;
    if (currency) isCrypto = this.isCryptoCurrency(currency);
    var dotIndex = amount.toString().indexOf(".");
    const decimalPlaces = precision ? precision : isCrypto ? 6 : 2;
    const formattedAmount = parseFloat(amount || 0).toFixed(decimalPlaces);
    return dotIndex > -1 ? formattedAmount : amount;
  },
  isCryptoCurrency: function(currency) {
    const crypto = [
      "BTC",
      "XBT",
      "LTC",
      "NMC",
      "PPC",
      "DOGE",
      "XDG",
      "GRC",
      "XPM",
      "XRP",
      "NXT",
      "AUR",
      "DASH",
      "NEO",
      "MZC",
      "XMR",
      "TIT",
      "XVG",
      "XLM",
      "VTC",
      "ETH",
      "ETC",
      "XNO",
      "USDT",
      "FIRO",
      "ZEC",
      "BCH",
      "EOS",
      "ADA",
      "TRX",
      "CKB",
      "ALGO",
      "AVAX",
      "SHIB",
      "DOT",
      "DESO",
      "SAFEMOON"
    ].includes(currency.toUpperCase());
    return crypto;
  },
  calculateSrcAmount(acceptWindow, currentData) {
    let src_amount = acceptWindow.down("[name=src_amount]").getValue();
    let dst_amount = acceptWindow.down("[name=dst_amount]").getValue();
    let conversion_rate = acceptWindow
      .down("[name=conversion_rate]")
      .getValue();
    let markup_rate = acceptWindow.down("[name=markup_rate]").getValue();
    conversion_rate = conversion_rate > 0 ? conversion_rate : 1;
    src_amount =
      dst_amount / (conversion_rate - conversion_rate * (markup_rate / 100));

    const settlementCurrency = currentData.src_currency;

    src_amount = __CONFIG__.formatCurrency(src_amount, 0, settlementCurrency);

    let dstAmountComponent = acceptWindow.down("[name=src_amount]");
    if (dstAmountComponent) {
      dstAmountComponent.suspendEvents();
      acceptWindow.down("[name=src_amount]").setValue(src_amount);
      dstAmountComponent.resumeEvents(false);
    }
  },
  calculateDstAmount(acceptWindow, store) {
    let src_amount = acceptWindow.down("[name=src_amount]").getValue();
    let dst_amount = acceptWindow.down("[name=dst_amount]").getValue();
    let conversion_rate = acceptWindow
      .down("[name=conversion_rate]")
      .getValue();
    let markup_rate = acceptWindow.down("[name=markup_rate]").getValue();
    conversion_rate = conversion_rate > 0 ? conversion_rate : 1;

    dst_amount =
      src_amount * conversion_rate -
      src_amount * conversion_rate * (markup_rate / 100);
    const settlementCurrency = store.dst_currency;

    dst_amount = __CONFIG__.formatCurrency(dst_amount, 0, settlementCurrency);
    let dstAmountComponent = acceptWindow.down("[name=dst_amount]");
    if (dstAmountComponent) {
      dstAmountComponent.suspendEvents();
      acceptWindow.down("[name=dst_amount]").setValue(dst_amount);
      dstAmountComponent.resumeEvents(false);
    }
  },
  calculateMarkupRate(settlementAmount, val) {
    return settlementAmount * (val / 100);
  },
  buildENMStatusCombo(config) {
    var me = this;
    let combo = {
      name: "status",
      labelWidth: 150,
      fieldLabel: D.t("Status"),
      xtype: "dependedcombo",
      queryMode: "all",
      forceSelection: true,
      triggerAction: "all",
      editable: false,
      allowBlank: false,
      valueField: "key",
      displayField: "key",
      modelConfig: { enum_name: config.enum_name },
      dataModel: "Crm.modules.Util.model.FetchComboRecords",
      operator: "eq",
      listeners: {
        afterrender(e, eOpts) {
          me.setComponentEmptyText(e, eOpts);
        },
        change(e, newVal, oldVal, eOpts) {
          if (e.getValue() === null && e.getRawValue() == "") {
            e.lastSelectedRecords = [];
            e.clearValue();
          }
        }
      }
    };
    Object.assign(combo, config);
    return combo;
  }
};
