/**
 * @author Vaibhav Mali
 * @Date : 28 Sept 2018
 */
Ext.define("main.FormContainer", {
  extend: "Core.form.FormContainer",

  initComponent() {
    var me = this;
    if (this.controllerCls) {
      this.controller = Ext.create(this.controllerCls);
    } else if (!this.controller)
      this.controller = Ext.create("Core.form.FormController");

    this.items = this.buildForm();
    this.callParent(arguments);
    this.on("render", function() {
      var formPanel = Ext.getCmp("mainFormPanel");
      formPanel.add([
        {
          xtype: "textfield",
          name: "mtime",
          hidden: true
        }
      ]);
      if (__CONFIG__.refreshNeeded) {
        if (me && me.controller && me.controller.view) {
          me.controller.refreshFormData();
        }
        //window.location.reload();
      }
    });
  },

  buildForm: function() {
    return {
      xtype: "form",
      id: "mainFormPanel",
      tbar: this.buildButtons(),
      layout: this.formLayout,
      margin: this.formMargin,
      defaults: {
        xtype: "textfield",
        anchor: "100%",
        labelWidth: 150
      },
      items: this.buildAllItems()
    };
  },

  buildButtons: function() {
    var me = this;
    return [
      {
        text: D.t("Save"),
        iconCls: "x-fa fa-check",
        scale: "medium",
        style: "background: #53b7f4; color: #fff;",
        cls: "white_button",
        action: "apply"
      },
      {
        iconCls: "x-fa fa-refresh",
        scale: "medium",
        action: "refreshFormData",
        tooltip: D.t("Refresh Form Data")
      },
      "->",
      {
        text: D.t("Close"),
        iconCls: "x-fa fa-times",
        action: "gotolist",
        scale: "medium",
        style: "background: #53b7f4; color: #fff;",
        cls: "white_button"
      }
    ];
    me.setButtonsByPermissions();
  }
});
