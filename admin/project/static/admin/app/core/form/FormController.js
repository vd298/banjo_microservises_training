Ext.define("Core.form.FormController", {
  extend: "Ext.app.ViewController",

  requires: ["Ext.window.Toast"],

  init: function(view) {
    var me = this;
    if (!this.compDomain) this.compDomain = new Ext.app.domain.View(this);
    this.view = view;
    this.model = this.createModel();
    this.model.getAdminDetails(function(res) {
      if (res && res.error) {
        me.showToastMessage({ message: res.error.message, title: "Error" });
      }
    });

    me.loadData(function(data) {
      me.setTitle(data);
      me.setFieldsDisable(function() {});
    });
    this.setControls();
  },
  showToastMessage: function(data) {
    var me = this;
    Ext.toast({
      html: data.message,
      title: data.title || "Action Result",
      align: "t",
      iconCls: "fa fa-list",
      autoCloseDelay: 2000,
      closable: true,
      width: 200
    });
  },
  setControls: function() {
    var me = this;
    this.control({
      "[action=formclose]": {
        click: () => {
          this.closeView();
        }
      },
      "[action=apply]": {
        click: () => {
          this.save(false, (res) => {
            //D.a("Message","Data saved")
          });
        }
      },
      "[action=save]": {
        click: () => {
          this.save(true);
        }
      },
      "[action=remove]": {
        click: () => {
          this.deleteRecord_do(true);
        }
      },
      "[action=copy]": {
        click: () => {
          this.copyRecord(true);
        }
      },
      "[action=gotolist]": {
        click: () => {
          this.gotoListView(true);
        }
      },
      "[action=exportjson]": {
        click: () => {
          this.exportJson();
        }
      },
      "[action=importjson]": {
        change: (el) => {
          this.importJson();
        }
      }
    });

    this.view.on("activate", (grid, indx) => {
      if (!this.oldDocTitle) this.oldDocTitle = document.title;
      var form = this.view.down("form").getForm();
      if (form) {
        data = form.getValues();
        this.setTitle(data);
      }
    });
    this.view.on("close", (grid, indx) => {
      if (this.oldDocTitle) document.title = this.oldDocTitle;
      try {
        if (me && me.view && me.view.currentData)
          me.setValues(me.view.currentData);
      } catch (err) {
        console.error(`FormController. Unable to reset form data. Error:`, err);
      }
    });

    this.view.on("disablePanels", (data, cmp) => {
      if (data.maker == undefined) {
        var tabPanel = Ext.getCmp(cmp);
        var items = tabPanel.items.items;

        for (var i = 1; i < items.length; i++) {
          var panel = items[i];
          panel.setDisabled(true);
        }
      }
    });

    this.view.down("form").on({
      validitychange: (th, valid, eOpts) => {
        var el = this.view.down("[action=apply]");
        if (el) el.setDisabled(!valid);
        el = this.view.down("[action=save]");
        if (el) el.setDisabled(!valid);
      }
    });
    this.checkPermissions();
  },

  closeView: function() {
    var me = this;
    try {
      if (me && me.view && me.view.currentData)
        me.setValues(me.view.currentData);
    } catch (err) {
      console.error(`FormController. Unable to reset form. Error:`, err);
    }
    if (!this.view.listClose && !this.view.noHash) {
      window.history.back();
    } else {
      this.view.close();
    }
  },

  checkPermissions: function() {
    var me = this,
      el;
    this.model.getPermissions(function(permis) {
      if (!permis.modify && !permis.add) {
        el = me.view.down("[action=apply]");
        if (el) el.setDisabled(true);
        el = me.view.down("[action=save]");
        if (el) el.setDisabled(true);
        me.setFormReadOnly();
      }
      if (!permis.del) {
        el = me.view.down("[action=remove]");
        if (el) el.setDisabled(true);
      }
    });
  },
  setFormReadOnly: function() {
    this.view
      .query("textfield,combo,xdatefield,numberfield,checkbox,gridfield")
      .forEach(function(itm) {
        itm.setReadOnly(true);
      });
  },

  deleteRecord_do: function(store, rec) {
    var me = this;
    D.c("Removing", "Delete the record?", [], function() {
      var data = me.view
        .down("form")
        .getForm()
        .getValues();
      me.model.remove([data[me.model.idField]], function() {
        me.view.fireEvent("remove", me.view);
        me.view.close();
      });
    });
  },

  copyRecord: function() {
    var me = this,
      data = this.view
        .down("form")
        .getForm()
        .getValues();
    me.model.getNewObjectId(function(_id) {
      data[me.model.idField] = _id;

      if (me.view.copyTexts) {
        for (var i in me.view.copyTexts) {
          data[i] += " " + me.view.copyTexts[i];
        }
      }

      me.model.write(data, function(data, err) {
        var cls = Object.getPrototypeOf(me.view).$className;
        me.view.close();
        setTimeout(function() {
          location = "./#" + cls + "~" + _id;
        }, 500);
      });
    });
  },

  createModel: function() {
    if (this.model)
      return Ext.isString(this.model) ? Ext.create(this.model) : this.model;

    if (this.view.model)
      return Ext.isString(this.view.model)
        ? Ext.create(this.view.model)
        : this.view.model;

    if (this.view.scope && this.view.scope.model) return this.view.scope.model;

    var m = Object.getPrototypeOf(this.view).$className.replace(
      ".view.",
      ".model."
    );

    if (m.substr(-4) == "Form") m = m.substr(0, m.length - 4) + "Model";
    else m += "Model";

    return Ext.create(m);
  },

  gotoListView: function() {
    //Vaibhav Vaidya, 24 April 2023, Reset form on close action
    var me = this;
    try {
      if (me && me.view && me.view.currentData)
        me.setValues(me.view.currentData);
    } catch (err) {
      console.error(`FormController. Unable to reset form. Error:`, err);
    }
    location =
      "#" +
      Object.getPrototypeOf(this.view).$className.replace(/Form$/, "Grid");
  },

  loadData: function(callback) {
    var me = this;

    var cb = function(data) {
      if (!!me.afterDataLoad)
        me.afterDataLoad(data, function(data) {
          me.setValues(data);
          callback(data);
        });
      else {
        me.setValues(data);
        callback(data);
      }
    };

    if (this.view.recordId) {
      me.model.readRecord(this.view.recordId, function(data) {
        if (data && data.signobject) {
          if (data.signobject.shouldSign)
            me.view.addSignButton(data.signobject);
          if (data.signobject.blocked) {
            me.setFormReadOnly();
            var b = me.view.down("[action=save]");
            if (b) b.setDisabled(true);
            b = me.view.down("[action=apply]");
            if (b) b.setDisabled(true);
            b = me.view.down("[action=remove]");
            if (b) b.setDisabled(true);
          }
        }
        var oo = {};
        oo[me.model.idField] = me.view.recordId;
        cb(data[me.model.idField] ? data : oo);
      });
    } else {
      me.model.getNewObjectId(function(_id) {
        var oo = {};
        oo[me.model.idField] = _id;
        cb(oo);
      });
    }
  },

  setValues: function(data) {
    if (window.__CB_REC__) {
      Ext.apply(data, __CB_REC__);
      window.__CB_REC__ = null;
      this.view.s = true;
    }
    this.view.currentData = data;
    var form = this.view.down("form");
    this.view.fireEvent("beforesetvalues", form, data);
    form.getForm().setValues(data);
    this.view.fireEvent("setvalues", form, data);
    form.getForm().isValid();
  },

  setTitle: function(data) {
    if (this.view.titleTpl) {
      var ttl = new Ext.XTemplate(this.view.titleTpl).apply(data);
      this.view.setTitle(ttl);
      document.title = ttl + " " + D.t("ConsoleTitle");
    }
  },

  save: function(closewin, cb) {
    var me = this,
      form = me.view.down("form").getForm(),
      data = {};

    var sb1 = me.view.down("[action=save]"),
      sb2 = me.view.down("[action=apply]");

    if (sb1 && !!sb1.setDisabled) sb1.setDisabled(true);
    if (sb2 && !!sb2.setDisabled) sb2.setDisabled(true);

    if (form) {
      data = form.getValues();
    }

    var setButtonsStatus = function() {
      if (sb1 && !!sb1.setDisabled) sb1.setDisabled(false);
      if (sb2 && !!sb2.setDisabled) sb2.setDisabled(false);
    };

    var changePanelStatus = function() {
      var cls = Object.getPrototypeOf(me.view).$className.split(".");
      cls = cls[cls.length - 1];
      var tabPanel = Ext.getCmp(`${cls}TabPanel`);
      if (tabPanel) {
        var items = tabPanel.items.items;

        for (var i = 1; i < items.length; i++) {
          var panel = items[i];
          panel.setDisabled(false);
        }
      }
    };

    if (me.view.fireEvent("beforesave", me.view, data) === false) {
      setButtonsStatus();
      return;
    }
    let newData = data;
    me.model.write(data, function(data, err) {
      if (
        data &&
        data.record &&
        data.record.signobject &&
        data.record.signobject.shouldSign
      ) {
        me.view.addSignButton(data.record.signobject);
      }
      me.view.currentData = Object.assign(me.view.currentData, newData);
      setButtonsStatus();
      //Vaibhav Vaidya, 24 Jan 2023, Add logic to build error object if returned in different format
      if (data && data.success == false && data.message) {
        data.error = {
          message: data.message
        };
      }
      if (data && data.error) {
        let defaultErrMsg = `Data Save/Update Failed.`;
        if (typeof data.error == "object" && Array.isArray(data.error)) {
          if (data.error && data.error.length && data.error[0].message) {
            if (data.error[0].field) {
              defaultErrMsg += `Error for field:` + data.error[0].field;
            }
            if (data.error[0].message) {
              defaultErrMsg += `Message:` + data.error[0].message;
            }
            Ext.toast({
              html: defaultErrMsg,
              title: "Save/Update Failed",
              align: "t",
              iconCls: "fa fa-list",
              autoCloseDelay: 2000,
              closable: true,
              width: 200
            });
          }
          return me.showErrorMessage(data.error);
        } else {
          if (data.error && data.error && data.error.message) {
            if (data.error.field) {
              defaultErrMsg += `Error for field:` + data.error.field;
            }
            if (data.error.message) {
              defaultErrMsg += `Message:` + data.error.message;
            }
            Ext.toast({
              html: defaultErrMsg,
              title: "Save/Update Failed",
              align: "t",
              iconCls: "fa fa-list",
              autoCloseDelay: 2000,
              closable: true,
              width: 200
            });
          }
          me.showErrorMessage([
            { field: data.error.field, message: data.error.message }
          ]);
          return;
        }
      }
      if (err) {
        me.showErrorMessage(err); //win, err)
        return;
      }
      me.displaySaveToast();
      changePanelStatus();
      if (me.view.fireEvent("save", me.view, data) === false) {
        if (!!cb) cb(data);
        return;
      }
      if (closewin && !!me.view.close) me.view.close();

      if (!!cb) cb(data);
    });
  },

  showErrorMessage: function(err) {
    var me = this;
    err.forEach(function(item) {
      var el = me.view.down("[name=" + item.field + "]");
      if (el && !!el.setActiveError) {
        el.setActiveError(item.message);
      }
    });
  },

  setFieldsDisable: function(cb) {
    var me = this;

    me.model.getFields(function(fields) {
      fields.forEach(function(f) {
        //if(!f.visible)
        //    me.removeField(f.name)
        //else
        if (!f.editable) me.disableField(f.name);
      });
      cb();
    });
  },

  removeField: function(name) {
    var fld = this.view.down("[name=" + name + "]");
    if (fld && fld.ownerCt) fld.ownerCt.remove(fld);
  },

  disableField: function(name) {
    var fld = this.view.down("[name=" + name + "]");
    if (fld && !!fld.setReadOnly) fld.setReadOnly(true);
  },

  setFormReadOnly: function() {
    var f = function(elm) {
      if (elm) {
        if (!!elm.items) {
          elm.items.items.forEach(function(i) {
            f(i);
          });
        }
        if (!!elm.setReadOnly) elm.setReadOnly(true);
      }
    };
    f(this.view.down("form"));
  },

  download(filename, text) {
    var element = document.createElement("a");
    element.setAttribute(
      "href",
      "data:text/json;charset=utf-8," + encodeURIComponent(text)
    );
    element.setAttribute("download", filename);
    element.style.display = "none";
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  },

  displaySaveToast: function() {
    //Vaibhav Vaidya, 18 Jan 2023, Add toast message on successful save
    Ext.toast({
      html: "Data Saved Successfully.",
      title: "Success",
      align: "t",
      iconCls: "fa fa-check",
      autoCloseDelay: 300,
      closable: true,
      width: 200
    });
  },

  exportJson() {
    const data = this.view
      .down("form")
      .getForm()
      .getValues();
    delete data[this.model.idField];
    this.download(`${data.name}.json`, JSON.stringify(data));
  },
  importJson() {
    const file = this.view.down("[action=importjson]").fileInputEl.dom.files[0];
    if (file) {
      const reader = new FileReader();
      reader.readAsText(file, "UTF-8");
      reader.onload = (evt) => {
        let data;
        try {
          data = JSON.parse(evt.target.result);
        } catch (e) {}
        if (data) {
          this.view
            .down("form")
            .getForm()
            .setValues(data);
        } else {
          D.a("", "Data is not found in the file", [], () => {});
        }
      };
      reader.onerror = (evt) => {};
    }
  }
});
