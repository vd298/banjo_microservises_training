Ext.define("Core.form.FormContainer", {
  extend: "Admin.view.main.AbstractContainer",

  formMargin: "10",
  formLayout: "anchor",
  layout: "fit",

  requires: [
    "Ext.tab.Panel",
    "Ext.layout.container.Border",
    "Ext.form.field.ComboBox",
    "Ext.form.field.Number",
    "Ext.form.FieldContainer"
  ],

  initComponent() {
    if (this.controllerCls) {
      this.controller = Ext.create(this.controllerCls);
    } else if (!this.controller)
      this.controller = Ext.create("Core.form.FormController");

    this.items = this.buildForm();
    this.callParent(arguments);
    document.getElementById("main-view-detail-wrap").scrollTop = 0;
  },

  setTitle: function(title) {
    this.down("form").setTitle(title);
  },

  buildButtons: function() {
    var me = this;
    var btns = [
      {
        text: D.t("Save"),
        iconCls: "x-fa fa-check",
        scale: "medium",
        action: "apply",
        style: "background: #53b7f4; color: #fff;",
        cls: "white_button"
      },
      "->",
      {
        text: D.t("Close"),
        iconCls: "x-fa fa-times",
        scale: "medium",
        action: "gotolist",
        style: "background: #53b7f4; color: #fff;",
        cls: "white_button"
      }
    ];
    if (this.allowImportExport) {
      btns.splice(1, 0, "-", {
        text: D.t("Export/Import"),
        style:
          "background: #53b7f4; color: #fff; top: 0; height:32px; font-size:24px;",
        cls: "white_button",
        iconCls: "x-fa fa-caret-down",
        iconAlign: "right",
        arrowVisible: false,
        menu: [
          {
            text: D.t("Export to file"),
            xtype: "button",
            margin: 5,
            action: "exportjson"
          },
          {
            xtype: "filefield",
            buttonOnly: true,
            margin: 5,
            action: "importjson",
            buttonConfig: {
              text: D.t("Import from file"),
              width: "100%"
            }
          }
        ]
      });
    }

    return btns;
  },

  buildForm: function() {
    return {
      xtype: "form",
      tbar: this.buildButtons(),
      layout: this.formLayout,
      margin: this.formMargin,
      title: this.title,
      defaults: {
        xtype: "textfield",
        anchor: "100%",
        labelWidth: 150
      },
      items: this.buildAllItems()
    };
  },

  buildAllItems: function() {
    var items = this.buildItems();

    if (!Ext.isArray(items)) items = [items];

    items.push({
      name:
        this.controller.model && this.controller.model.idField
          ? this.controller.model.idField
          : "_id",
      hidden: true
    });

    return items;
  },

  buildItems: function() {
    return [];
  },

  addSignButton: function(signobject) {
    var bPanel = this.down("form").getDockedItems('toolbar[dock="top"]');
    if (bPanel && bPanel[0]) {
      bPanel[0].insert(
        0,
        Ext.create("Core.sign.SignButton", {
          parentView: this,
          signobject: signobject
        })
      );
      bPanel[0].insert(1, "-");
    }
  }
});
