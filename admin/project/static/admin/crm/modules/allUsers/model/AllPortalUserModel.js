Ext.define("Crm.modules.allUsers.model.AllPortalUserModel", {
  extend: "Crm.modules.allUsers.model.AllUserAccountModel",

  /* scope:server */
  buildWhere: function(params, cb) {
    params.filters.push({
      _property: "user_type",
      _value: "PORTAL",
      _operator: "eq"
    });
    var me = this,
      args = arguments;
    me.callParent(args);
  }
});
