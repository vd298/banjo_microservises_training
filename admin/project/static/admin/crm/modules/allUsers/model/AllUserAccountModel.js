Ext.define("Crm.modules.allUsers.model.AllUserAccountModel", {
  extend: "Crm.classes.DataModel",

  collection: "vw_all_users",
  idField: "id",
  foreignKeyFilter: [
    {
      collection: "accounts",
      alias: "a",
      on: "   vw_all_users.account_id = accounts.id",
      type: "inner"
    }
  ],
  //removeAction: "remove",

  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "account_id",
      type: "ObjectID",
      sort: 1,
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "email",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "first_name",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "last_name",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "username",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "status",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "user_type",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ips",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "webhook_url",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "telegram_id",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "user_account_id",
      sort: 1,
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "account_id",
      sort: 1,
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "account_name",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "main_role",
      sort: 1,
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "primary_role",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      sort: 1,
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "allocation_count",
      type: "integer",
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  /* scope:server */
  async write(data, cb) {
    var me = this;
    Object.keys(data).forEach(function(key, index) {
      if (typeof data[key] == "string" && data[key].length > 0) {
        data[key] = data[key].trim();
      }
    });

    let arrIPs = [];
    if (data && typeof data.ips == "string" && data.ips.length > 0) {
      arrIPs = data.ips.split(",");
      delete data.ips;
    }
    data.ips = arrIPs;
    if (!!data && !!data.id) {
      try {
        console.log(
          `AllUserAccountModel. Func:write. ${me.collection} table Database data:`,
          data
        );
        const params = {
          service: "auth-service",
          method: "updateUser",
          data: data,
          writeFlag: true,
          options: {
            realmId: me.user.profile.realm_id
          }
        };
        const updateUser = await me.callApi(params);
        console.log(
          `AllUserAccountModel. Func:write. response from callApi:`,
          updateUser
        );
        return cb({ success: true });
      } catch (error) {
        console.error(
          `AllUserAccountModel. Func:write. ${me.collection} table Database Error:`,
          error
        );
        return cb({
          success: false,
          message: `User update failed: ${error}`
        });
      }
    }
    return cb({ success: false, message: "No data found" });
  },
  /* scope:server */
  async $uniqueCheck(data, cb) {
    let sql = `select u.* from users u join user_accounts ua on ua.user_id = u.id join accounts a on a.id = ua.account_id   `;
    sql = await this.buildRealmConditionQuery(sql, "a.realm_id");
    sql += ` where removed=0 and LOWER(${data.changeCol})=LOWER('${data.changeVal}') and id!='${data.id}'`;

    const res = await this.src.db.query(sql);
    if (res && res.length) {
      let message = data.changeCol;
      return cb({
        result: false,

        message: `${message} must be unique`
      });
    } else {
      return cb({ result: true });
    }
  }
});
