Ext.define("Crm.modules.allUsers.model.AllUserAccountAllocationModel", {
  extend: "Crm.classes.DataModel",

  collection: "vw_all_users_mappings",
  idField: "id",
  //removeAction: "remove",
  foreignKeyFilter: [
    {
      collection: "accounts",
      alias: "a",
      on: "  vw_all_users_mappings.account_id= accounts.id  ",
      type: "inner"
    }
  ],

  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "account_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "user_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "email",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "first_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "last_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "username",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "status",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "user_type",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ips",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "webhook_url",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "telegram_id",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "account_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "main_role",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "primary_role",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      sort: -1,
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "start_date",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "end_date",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    }
  ]
});
