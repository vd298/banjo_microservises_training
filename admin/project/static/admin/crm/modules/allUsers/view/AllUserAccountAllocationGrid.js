Ext.define("Crm.modules.allUsers.view.AllUserAccountAllocationGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Active Merchant Accounts"),
  controllerCls:
    "Crm.modules.allUsers.view.AllUserAccountAllocationGridController",

  filterable: true,
  filterbar: true,
  gridCfg: {
    viewConfig: {
      getRowClass: (record) => {
        if (record.data.active_relation) return "success";
      }
    }
  },
  buildColumns: function() {
    return [
      {
        dataIndex: "id",
        hidden: true
      },
      {
        text: D.t("Email"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "email",
        hidden: true
      },
      {
        text: D.t("Name"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "first_name",
        hidden: true
      },
      {
        text: D.t("last_name"),
        flex: 1,
        sortable: true,
        hidden: true,
        filter: true,
        dataIndex: "last_name"
      },
      {
        text: D.t("Username"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "username",
        hidden: true
      },
      {
        text: D.t("Merchant"),
        flex: 1,
        sortable: true,
        renderer: function(v, m, r) {
          if (v) {
            return (
              '<a href="#Crm.modules.bankAccounts.view.AccountsForm~' +
              v +
              '">' +
              r.data.account_name +
              "</a>"
            );
          }
          return v;
        },
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "id",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create("Crm.modules.accounts.model.AccountsModel"),
            fieldSet: ["id", "name"],
            scope: this
          }),
          operator: "eq"
        },
        dataIndex: "account_id"
      },
      {
        dataIndex: "account_name",
        hidden: true
      },
      {
        text: D.t("Status"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "status",
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "status",
          displayField: "status",
          store: {
            fields: ["status"],
            data: [{ status: "ACTIVE" }, { status: "INACTIVE" }]
          },
          operator: "eq"
        }
      },
      {
        text: D.t("User Type"),
        flex: 1,
        sortable: true,
        filter: {
          xtype: "dependedcombo",
          queryMode: "all",
          forceSelection: true,
          triggerAction: "all",
          editable: false,
          value: "PORTAL",
          valueField: "key",
          displayField: "key",
          modelConfig: { enum_name: "enum_users_user_type" },
          dataModel: "Crm.modules.Util.model.FetchComboRecords",
          operator: "eq"
        },
        dataIndex: "user_type"
      },
      {
        xtype: "tagfield",
        text: D.t("IP Addresses"),
        name: "ips",
        growMax: 40,
        valueField: "ips",
        displayField: "ips",
        emptyText: "Multiple Entries accepted, Hit enter to separate values",
        fieldLabel: D.t("IP Addresses"),
        expand: Ext.emptyFn,
        hideTrigger: true,
        queryMode: "local",
        store: Ext.create("Core.data.Store", {
          dataModel: "Crm.modules.accounts.model.UserAccountInvitationsModel",
          fieldSet: ["ips"],
          scope: this
        }),
        forceSelection: false,
        triggerOnClick: false,
        createNewOnEnter: true,
        hidden: true,
        allowBlank: true
      },
      {
        text: D.t("Webhook Url"),
        flex: 1,
        sortable: true,
        filter: true,
        hidden: true,
        dataIndex: "webhook_url"
      },
      {
        dataIndex: "telegram_id",
        text: D.t("Telegram ID"),
        flex: 1,
        sortable: true,
        filter: true,
        hidden: true
      },
      {
        text: D.t("Role"),
        flex: 1,
        sortable: true,
        hidden: true,
        filter: {
          xtype: "dependedcombo",
          editable: true,
          fieldSet: "id,role_name",
          valueField: "id",
          displayField: "role_name",
          parentEl: null,
          parentField: null,
          dataModel: "Crm.modules.roles.model.RolesModel"
        },
        renderer: function(v, m, r) {
          return v ? r.data.primary_role : "-";
        },
        dataIndex: "main_role"
      },
      {
        dataIndex: "primary_role",
        hidden: true
      },
      {
        xtype: "datecolumn",
        format: "d.m.Y H:i:s",
        filter: {
          xtype: "datefield",
          format: "d.m.Y"
        },
        text: D.t("Start Date"),
        flex: 1,
        sortable: true,
        dataIndex: "start_date"
      },
      {
        xtype: "datecolumn",
        format: "d.m.Y H:i:s",
        filter: {
          xtype: "datefield",
          format: "d.m.Y"
        },
        text: D.t("End Date"),
        flex: 1,
        sortable: true,
        dataIndex: "end_date"
      }
    ];
  },

  buildTbar: function() {
    var items = [
      {
        tooltip: this.buttonReloadText,
        iconCls: "x-fa fa-refresh",
        action: "refresh"
      }
    ];

    if (this.filterable) items.push("->", this.buildSearchField());
    return items;
  }
});
