Ext.define("Crm.modules.allUsers.view.AllAPIUserFormController", {
  extend: "Core.form.FormController",

  setControls() {
    const me = this;
    this.control({
      "[name=username]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();
          formData.changeCol = "username";
          formData.changeVal = formData.username;
          this.model.runOnServer("uniqueCheck", formData, function(res) {
            e.validator = function(value) {
              if (res.result) {
                return true;
              } else {
                return res.message;
              }
            };
            me.view.down("form").isValid();
          });
        },
      },
    });
    this.callParent(arguments);
  },

  
});
