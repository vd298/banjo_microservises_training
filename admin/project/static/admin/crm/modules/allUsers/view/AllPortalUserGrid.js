Ext.define("Crm.modules.allUsers.view.AllPortalUserGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("All Portal Users"),

  filterable: true,
  filterbar: true,

  buildColumns: function() {
    const me = this;
    return [
      {
        dataIndex: "id",
        hidden: true
      },
      {
        text: D.t("Email"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "email"
      },
      {
        text: D.t("Name"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "first_name",
        renderer: function(v, m, r) {
          let name = "";
          if(v){
            name+=v;
          }
          if(r.data.last_name){
            name += " " + r.data.last_name
          }
          return name;
        }
      },
      {
        text: D.t("last_name"),
        flex: 1,
        sortable: true,
        hidden: true,
        filter: true,
        dataIndex: "last_name"
      },
      {
        text: D.t("Username"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "username"
      },
      {
        text: D.t("Status"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "status",
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "status",
          displayField: "status",
          store: {
            fields: ["status"],
            data: [
              { status: "ACTIVE" },
              { status: "INACTIVE" },
              { status: "BLOCKED" }
            ]
          },
          operator: "eq"
        }
      },
      {
        text: D.t("User Type"),
        flex: 1,
        sortable: true,
        hidden: true,
        filter: {
          xtype: "dependedcombo",
          queryMode: "all",
          forceSelection: true,
          triggerAction: "all",
          editable: false,
          value: "PORTAL",
          valueField: "key",
          displayField: "key",
          modelConfig: { enum_name: "enum_users_user_type" },
          dataModel: "Crm.modules.Util.model.FetchComboRecords",
          operator: "eq"
        },
        dataIndex: "user_type"
      },
      {
        dataIndex: "account_id",
        hidden: true
      },
      {
        dataIndex: "telegram_id",
        text: D.t("Telegram ID"),
        flex: 1,
        sortable: true,
        filter: true
      },
      {
        dataIndex: "user_account_id",
        hidden: true
      },
      {
        text: D.t("Merchant"),
        flex: 1,
        sortable: true,
        hidden: true,
        renderer: function(v, m, r) {
          if (v) {
            return (
              '<a href="#Crm.modules.bankAccounts.view.AccountsForm~' +
              v +
              '">' +
              r.data.account_name +
              "</a>"
            );
          }
          return v;
        },
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "id",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create("Crm.modules.accounts.model.AccountsModel"),
            fieldSet: ["id", "name"],
            scope: this
          }),
          operator: "eq"
        },
        dataIndex: "account_id"
      },
      {
        dataIndex: "account_name",
        hidden: true
      },
      {
        text: D.t("Role"),
        flex: 1,
        sortable: true,
        hidden: true,
        filter: {
          xtype: "dependedcombo",
          editable: true,
          fieldSet: "id,role_name",
          valueField: "id",
          displayField: "role_name",
          parentEl: null,
          parentField: null,
          dataModel: "Crm.modules.roles.model.RolesModel"
        },
        renderer: function(v, m, r) {
          return v ? r.data.primary_role : "-";
        },
        dataIndex: "main_role"
      },
      {
        dataIndex: "primary_role",
        hidden: true
      },
      {
        xtype: "datecolumn",
        format: "d.m.Y H:i:s",
        filter: {
          xtype: "datefield",
          format: "d.m.Y"
        },
        text: D.t("Registered On"),
        flex: 1,
        sortable: true,
        dataIndex: "ctime"
      },
      {
        text: D.t("Associated Merchant Accounts"),
        dataIndex: "allocation_count",
        sortable: true,
        filter: { xtype: "numberfield" }
      }
    ];
  },

  buildTbar: function() {
    var items = [];

    if (this.importButton) {
      items.push("-", {
        text: this.buttonImportText,
        iconCls: "x-fa fa-cloud-download",
        action: "import"
      });
    }
    if (this.exportButton) {
      items.push("-", {
        text: this.buttonExportText,
        iconCls: "x-fa fa-cloud-upload",
        action: "export"
      });
    }

    items.push("-", {
      tooltip: this.buttonReloadText,
      iconCls: "x-fa fa-refresh",
      action: "refresh"
    });

    if (this.filterable) items.push("->", this.buildSearchField());
    return items;
  },

  buildButtonsColumns: function() {
    var me = this;
    return [
      {
        xtype: "actioncolumn",
        width: 30,
        menuDisabled: true,
        items: [
          {
            iconCls: "x-fa fa-pencil-square-o",
            tooltip: this.buttonEditTooltip,
            isDisabled: function() {
              return !me.permis.modify && !me.permis.read;
            },
            handler: function(grid, rowIndex) {
              me.fireEvent("edit", grid, rowIndex);
            }
          }
        ]
      }
    ];
  }
});
