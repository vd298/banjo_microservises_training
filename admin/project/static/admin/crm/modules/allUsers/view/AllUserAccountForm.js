Ext.define("Crm.modules.allUsers.view.AllUserAccountForm", {
  extend: "Core.form.FormContainer",

    titleTpl: D.t("All Users"),
    iconCls: "x-fa fa-user",
    requires: [
      "Desktop.core.widgets.GridField",
      "Core.form.DateField",
      "Core.form.DependedCombo",
      "Ext.form.field.Tag"
    ],
    controllerCls: "Crm.modules.allUsers.view.AllUserAccountFormController",
    formLayout: "fit",
    formMargin: 0,
    buildItems() {
      return {
        xtype: "tabpanel",
        layout: "fit",
        items: [this.buildFormFields(), this.buildAssignedAccounts()],
        autoEl: { "data-test": "tab-items" }
      };
    },
    buildFormFields() {
      var me = this;
      return {
        xtype: "panel",
        layout: "anchor",
        title: D.t("General"),
        width: "50%",
        defaults: {
          anchor: "100%",
          xtype: "textfield",
          labelWidth: 150,
          margin: 5
        },
        items: [
          {
            name: "id",
            hidden: true
          },
          {
            name: "email",
            fieldLabel: D.t("Email"),
            readOnly:true,
            allowBlank: false,
            maxLength: 50,
            hidden: true
          },
          {
            name: "user_type",
            fieldLabel: D.t("User Type"),
            readOnly:true,
            allowBlank: false,
            maxLength: 50,
          },
          {
            name: "first_name",
            fieldLabel: D.t("Name"),
            allowBlank: false,
            hidden: true
          },
          {
            name: "username",
            fieldLabel: D.t("Username"),
            readOnly:true,
            allowBlank: false,
            maxLength: 50,
          },
          {
            xtype: "tagfield",
            text: D.t("IP Addresses"),
            name: "ips",
            growMax: 40,
            valueField: "ips",
            displayField: "ips",
            emptyText: "Multiple Entries accepted, Hit enter to separate values",
            fieldLabel: D.t("IP Addresses"),
            expand: Ext.emptyFn,
            hideTrigger: true,
            queryMode: "local",
            store: Ext.create("Core.data.Store", {
              dataModel: "Crm.modules.accounts.model.UserAccountInvitationsModel",
              fieldSet: ["ips"],
              scope: this
            }),
            forceSelection: false,
            triggerOnClick: false,
            createNewOnEnter: true,
            hidden: true,
            allowBlank: true
          },
          {
            name: "webhook_url",
            hidden: true,
            allowBlank: false,
            fieldLabel: D.t("Webhook URL")
          },
          
          {
            name: "telegram_id",
            fieldLabel: D.t("Telegram Id"),
            allowBlank: false,
            maxLength: 20,
            hidden: true
          },
          {
            name: "primary_role",
            fieldLabel: D.t("Primary Role"),
            allowBlank: false,
            maxLength: 20,
            hidden: true
          },
          {
            xtype: "combo",
            name: "status",
            allowBlank: false,
            flex: 1,
            fieldLabel: D.t("Status"),
            valueField: "status",
            displayField: "status",
            store: {
              fields: ["status"],
              data: [{ status: "ACTIVE" }, { status: "INACTIVE" },{status: "BLOCKED"}]
            },
            hidden: true
          }, 
        ]
      };
    },
   
    buildAssignedAccounts() {
      return {
        xtype: "panel",
        layout: "fit",
        title: D.t("Assigned Accounts"),
        items: Ext.create(
          "Crm.modules.allUsers.view.AllUserAccountAllocationGrid",
          {
            observe: [
              { property: "user_id", param: "id" }
            ],
            scope: this
          }
        )
      };
    }
  });
  