Ext.define("Crm.modules.allUsers.view.AllUserAccountFormController", {
  extend: "Core.form.FormController",

  setControls() {
    const me = this;
    this.control({
       "[name=user_type]": {
        change: function(v) {
          const roleCombo = me.view.down("[name=primary_role]");
          const email = me.view.down("[name=email]");
          const ips = me.view.down("[name=ips]");
          const status = me.view.down("[name=status]");
          const webhook_url = me.view.down("[name=webhook_url]");
          const telegram_id = me.view.down("[name=telegram_id]");
          const first_name = me.view.down("[name=first_name]");
          if (v.value === "API") {
            
            //PORTAL fields
            roleCombo.setConfig("hidden", true);
            roleCombo.setConfig("allowBlank", true);
            email.setConfig("hidden", true);
            email.setConfig("allowBlank", true);
            status.setConfig("hidden", true);
            status.setConfig("allowBlank", true);
            telegram_id.setConfig("hidden", true);
            telegram_id.setConfig("allowBlank", true);
            first_name.setConfig("hidden", true);
            first_name.setConfig("allowBlank", true);
            //API fields
           
            ips.setConfig("hidden", false);
            ips.setConfig("allowBlank", true);
            webhook_url.setConfig("hidden", false);
            webhook_url.setConfig("allowBlank", false);
          
          } else {
            //PORTAL fields
            
            email.setConfig("hidden", false);
            email.setConfig("allowBlank", false);
            status.setConfig("hidden", false);
            status.setConfig("allowBlank", false);
            status.setConfig("hidden", false);
            status.setConfig("allowBlank", false);
            telegram_id.setConfig("hidden", false);
            telegram_id.setConfig("allowBlank", true);
            first_name.setConfig("hidden", false);
            first_name.setConfig("allowBlank", false);
            //API fields
            
            ips.setConfig("hidden", true);
            ips.setConfig("allowBlank", true)
            webhook_url.setConfig("hidden", true);
            webhook_url.setConfig("allowBlank", true);
          }
        }
      }
    });
    this.callParent(arguments);
  },

  
});
