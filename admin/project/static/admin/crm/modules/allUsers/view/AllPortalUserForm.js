Ext.define("Crm.modules.allUsers.view.AllPortalUserForm", {
  extend: "Core.form.FormContainer",

    titleTpl: D.t("All Portal Users"),
    iconCls: "x-fa fa-user",
    requires: [
      "Desktop.core.widgets.GridField",
      "Core.form.DateField",
      "Core.form.DependedCombo",
      "Ext.form.field.Tag"
    ],
    controllerCls: "Crm.modules.allUsers.view.AllPortalUserFormController",
    formLayout: "fit",
    formMargin: 0,
    buildItems() {
      return {
        xtype: "tabpanel",
        layout: "fit",
        items: [this.buildFormFields(), this.buildAssignedAccounts()],
        autoEl: { "data-test": "tab-items" }
      };
    },
    buildFormFields() {
      var me = this;
      return {
        xtype: "panel",
        layout: "anchor",
        title: D.t("General"),
        width: "50%",
        defaults: {
          anchor: "100%",
          xtype: "textfield",
          labelWidth: 150,
          margin: 5
        },
        items: [
          {
            name: "id",
            hidden: true
          },
          {
            name: "email",
            fieldLabel: D.t("Email"),
            readOnly:true,
            allowBlank: false,
            maxLength: 50
          },
          {
            name: "user_type",
            fieldLabel: D.t("User Type"),
            readOnly:true,
            maxLength: 50,
            hidden: true
          },
          {
            name: "first_name",
            fieldLabel: D.t("First Name"),
            allowBlank: false,
          },
          {
            name: "last_name",
            fieldLabel: D.t("Last Name"),
          },
          {
            name: "username",
            fieldLabel: D.t("Username"),
            allowBlank: false,
            maxLength: 50,
          },
          {
            name: "telegram_id",
            fieldLabel: D.t("Telegram Id"),
          },
          {
            name: "primary_role",
            fieldLabel: D.t("Primary Role"),
            allowBlank: false,
            maxLength: 20,
            hidden: true
          },
          {
            xtype: "combo",
            name: "status",
            allowBlank: false,
            flex: 1,
            fieldLabel: D.t("Status"),
            valueField: "status",
            displayField: "status",
            store: {
              fields: ["status"],
              data: [{ status: "ACTIVE" }, { status: "INACTIVE" },{status: "BLOCKED"}]
            }
          }, 
        ]
      };
    },
   
    buildAssignedAccounts() {
      return {
        xtype: "panel",
        layout: "fit",
        title: D.t("Assigned Accounts"),
        items: Ext.create(
          "Crm.modules.allUsers.view.AllUserAccountAllocationGrid",
          {
            observe: [
              { property: "user_id", param: "id" }
            ],
            scope: this
          }
        )
      };
    }
  });
  