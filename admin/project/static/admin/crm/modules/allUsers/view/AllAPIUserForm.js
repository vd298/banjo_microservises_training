Ext.define("Crm.modules.allUsers.view.AllAPIUserForm", {
  extend: "Core.form.FormContainer",

    titleTpl: D.t("All Api Users"),
    iconCls: "x-fa fa-user",
    requires: [
      "Desktop.core.widgets.GridField",
      "Core.form.DateField",
      "Core.form.DependedCombo",
      "Ext.form.field.Tag"
    ],
    controllerCls: "Crm.modules.allUsers.view.AllAPIUserFormController",
    formLayout: "fit",
    formMargin: 0,
    buildItems() {
      return {
        xtype: "tabpanel",
        layout: "fit",
        items: [this.buildFormFields(), this.buildAssignedAccounts()],
        autoEl: { "data-test": "tab-items" }
      };
    },
    buildFormFields() {
      var me = this;
      return {
        xtype: "panel",
        layout: "anchor",
        title: D.t("General"),
        width: "50%",
        defaults: {
          anchor: "100%",
          xtype: "textfield",
          labelWidth: 150,
          margin: 5
        },
        items: [
          {
            name: "id",
            hidden: true
          },
          {
            name: "user_type",
            fieldLabel: D.t("User Type"),
            readOnly:true,
            hidden: true,
            maxLength: 50
          },
          {
            name: "username",
            fieldLabel: D.t("Username"),
            allowBlank: false,
            maxLength: 50,
          },
          {
            xtype: "tagfield",
            text: D.t("IP Addresses"),
            name: "ips",
            growMax: 40,
            valueField: "ips",
            displayField: "ips",
            emptyText: "Multiple Entries accepted, Hit enter to separate values",
            fieldLabel: D.t("IP Addresses"),
            expand: Ext.emptyFn,
            hideTrigger: true,
            queryMode: "local",
            store: Ext.create("Core.data.Store", {
              dataModel: "Crm.modules.accounts.model.UserAccountInvitationsModel",
              fieldSet: ["ips"],
              scope: this
            }),
            forceSelection: false,
            triggerOnClick: false,
            createNewOnEnter: true,
            allowBlank: true
          },
          {
            name: "webhook_url",
            allowBlank: false,
            fieldLabel: D.t("Webhook URL")
          },
          {
            xtype: "combo",
            name: "status",
            allowBlank: true,
            flex: 1,
            fieldLabel: D.t("Status"),
            valueField: "status",
            displayField: "status",
            store: {
              fields: ["status"],
              data: [{ status: "ACTIVE" }, { status: "INACTIVE" },{status: "BLOCKED"}]
            }
          }, 
        ]
      };
    },
   
    buildAssignedAccounts() {
      return {
        xtype: "panel",
        layout: "fit",
        title: D.t("Assigned Accounts"),
        items: Ext.create(
          "Crm.modules.allUsers.view.AllUserAccountAllocationGrid",
          {
            observe: [
              { property: "user_id", param: "id" }
            ],
            scope: this
          }
        )
      };
    }
  });
  