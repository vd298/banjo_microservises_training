Ext.define("Crm.modules.CreationMerchantUsers.view.CreationMerchantUsersForm", {
  extend: "Crm.modules.accounts.view.UserAccountInvitationsForm",

  controllerCls: "Crm.modules.CreationMerchantUsers.view.CreationMerchantUsersFormController"

  , onClose: function (me) {
    return;
  }

  , buildItems() {
    return {
      xtype: "panel",
      layout: "anchor",
      defaults: {
        anchor: "100%",
        xtype: "textfield",
        labelWidth: 150,
        margin: 5
      },
      items: [
        {
          name: "id",
          hidden: true
        },
        {
          name: "account_id",
          hidden: true
        },
        {
          name: "accepted",
          hidden: true
        },
        {
          name: "email",
          fieldLabel: D.t("Email"),
          maxLength: 50,
          allowBlank: false,
          regex: /^(")?(?:[^\."])(?:(?:[\.])?(?:[\w\-!#$%&'*+\/=?\^_`{|}~]))*\1@(\w[\-\w]*\.){1,5}([A-Za-z]){2,6}$/,
          regexText: "Invalid email address"
        },
        this.buildUserTypesCombo(),
        this.buildRolesCombo()
      ]
    };
  },

  buildButtons: function () {
    var btns = [
      "->",
      {
        text: D.t("Save and close"),
        iconCls: "x-fa fa-check-square-o",
        scale: "medium",
        action: "save"
      },
      "-",
      { text: D.t("Close"), iconCls: "x-fa fa-ban", action: "gotolist" }
    ];
    return btns;
  },

});
