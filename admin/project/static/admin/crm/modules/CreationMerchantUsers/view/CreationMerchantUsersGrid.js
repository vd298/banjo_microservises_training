Ext.define("Crm.modules.CreationMerchantUsers.view.CreationMerchantUsersGrid", {
  extend: "Crm.modules.accounts.view.UserAccountInvitationsGrid",

  filterable: false,
  filterbar: false,
  controllerCls: "Crm.modules.CreationMerchantUsers.view.CreationMerchantUsersGridController",
  detailsInDialogWindow: true

  ,buildTbar: function () {
    var items = [
      {
        text: this.buttonAddText,
        tooltip: this.buttonAddTooltip,
        iconCls: "x-fa fa-plus",
        scale: "medium",
        action: "addUser"
      }
    ];
    return items;
  },

});