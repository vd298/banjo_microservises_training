Ext.define("Crm.modules.CreationMerchantUsers.view.CreationMerchantUsersGridController", {
  extend: "Crm.modules.accounts.view.UserAccountInvitationsGridController",

  init: function (view) {
    this.view = view;
    this.model = this.view.model;
    this.setControls();
  },

  setControls: function () {
    var me = this;
    this.control({
      "[action=addUser]": {
        click: function (el) {
          me.addRecord();
        }
      },
      "[action=refresh]": {
        click: function (el) {
          me.reloadData();
        }
      },
      grid: {
        cellkeydown: function (cell, td, i, rec, tr, rowIndex, e, eOpts) {
          if (e.keyCode == 13) {
            //me.gotoRecordHash(rec.data);
          }
        },
        celldblclick: function (cell, td, i, rec) {
          //me.gotoRecordHash(rec.data);
        },
        itemcontextmenu: function (vw, record, item, index, e, options) {
          e.stopEvent();
        }
      }
    });
    this.view.on("activate", function (grid, indx) {
      if (!me.view.observeObject)
        document.title = me.view.title + " " + D.t("ConsoleTitle");
    });
    this.view.on("edit", function (grid, indx) {
      //me.gotoRecordHash(grid.getStore().getAt(indx).data);
    });
    this.view.on("delete", function (grid, indx) {
      me.deleteRecord(grid.getStore(), indx);
    })
    this.callParent(arguments);
  },

  gotoRecordHash: function (data) {
    var me = this;
    if (!!this.view.observeObject) {
      window.__CB_REC__ = this.view.observeObject;
    }
    if (data && data[this.view.model.idField]) {
      var hash =
        this.generateDetailsCls() + "~" + data[this.view.model.idField];
      if (this.view.detailsInDialogWindow) {
        Ext.create(this.generateDetailsCls(), {
          noHash: true,
          recordId: data[this.view.model.idField],
          gridComponent: me.view,
          modal: true
        });
      } else if (this.view.detailsInNewWindow) window.open("./#" + hash);
      else location.hash = hash;
    }
  },

  deleteRecord_do: function (store, rec) {
    var me = this;
    D.c("Removing", "Delete the record?", [], function () {
      store.remove(rec);;
    });
  },

});
