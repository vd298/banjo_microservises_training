Ext.define("Crm.modules.CreationMerchantUsers.view.CreationMerchantUsersFormController", {
  extend: "Crm.modules.accounts.view.UserAccountInvitationsFormController"

  ,loadData: function (callback) {
    var me = this;

    var cb = function (data) {
      me.setValues(data);
      if (!!me.afterDataLoad)
        me.afterDataLoad(data, function (data) {
          me.setValues(data);
          callback(data);
        });
      else {
        me.setValues(data);
        callback(data);
      }
    };

    if (this.view.recordId) {
      me.model.readRecord(this.view.recordId, function (data) {
        if (data && data.signobject) {
          if (data.signobject.shouldSign)
            me.view.addSignButton(data.signobject);
          if (data.signobject.blocked) {
            me.setFormReadOnly();
            var b = me.view.down("[action=save]");
            if (b) b.setDisabled(true);
            b = me.view.down("[action=apply]");
            if (b) b.setDisabled(true);
            b = me.view.down("[action=remove]");
            if (b) b.setDisabled(true);
          }
        }
        var oo = {};
        oo[me.model.idField] = me.view.recordId;
        cb(data[me.model.idField] ? data : oo);
      });
    } else {
      me.model.getNewObjectId(function (_id) {
        var oo = {};
        oo[me.model.idField] = _id;
        cb(oo);
      });
    }
  },

  save: function (closewin, cb) {
    var me = this,
      form = me.view.down("form").getForm(),
      data = {};

    var sb1 = me.view.down("[action=save]"),
      sb2 = me.view.down("[action=apply]");

    if (sb1 && !!sb1.setDisabled) sb1.setDisabled(true);
    if (sb2 && !!sb2.setDisabled) sb2.setDisabled(true);

    if (form) {
      data = form.getValues();
    }

    var setButtonsStatus = function () {
      if (sb1 && !!sb1.setDisabled) sb1.setDisabled(false);
      if (sb2 && !!sb2.setDisabled) sb2.setDisabled(false);
    };

    if (me.view.fireEvent("beforesave", me.view, data) === false) {
      setButtonsStatus();
      return;
    }
    var merchantAccGrid = me.view.gridComponent.store;
    var creationForm = me.view.gridComponent.up("[name=mainCreatePanel]");
    if (merchantAccGrid) {
      me.view.gridComponent.store.add(data);
      if (creationForm) {
        let storeLength = merchantAccGrid.getRange().length;
        if (storeLength) {
          creationForm.down("[id=move-next]").setDisabled(false);
        }
        else {
          creationForm.down("[id=move-next]").setDisabled(true);
        }
      }
      me.view.close();
    }
    else {
      Ext.toast({
        html: "Data Save/Update Failed. Unable to attach to Merchant Account Grid store",
        title: "Save/Update Failed",
        align: "t",
        iconCls: "fa fa-list",
        autoCloseDelay: 2000,
        closable: true,
        width: 200
      });
    }
  }

  , closeView: function () {
    this.view.close();
  },

  gotoListView: function () {
    this.view.close();
  },

  deleteRecord_do: function (store, rec) {
    var me = this;
    var merchantAccGrid = me.view.gridComponent.store;
    if (merchantAccGrid && rec && rec.name) {
      merchantAccGrid.store.remove(rec)
    }
  },
});
