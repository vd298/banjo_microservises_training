Ext.define("Crm.modules.bankAccounts.model.BankAccountsModel", {
  extend: "Crm.classes.DataModel",

  collection: "vw_bank_accounts_limits",
  idField: "id",
  foreignKeyFilter: [
    {
      collection: "ref_limits",
      alias: "rl",
      on: " vw_bank_accounts_limits._id = ref_limits.id ",
      type: "inner"
    }
  ],
  fields: [
    {
      name: "id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "bank_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "account_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "acc_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "currency",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "payment_provider_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  async beforeSave(data, cb) {
    const me = this;
    for (let i = 0; i < data.payment_data.length; i++) {
      let params = {
        service: "account-service",
        method: "attachBankAccountProtocols",
        data: {
          bank_account_id: data.id,
          payment_protocol_id: data.payment_data[i].payment_protocol
        },
        options: {
          realmId: me.user.profile.realm_id
        }
      };
      var res = await me.callApi(params);
      if (res.result) {
        cb(data);
      } else {
        return cb({
          success: false,
          message: "Could not attach protocol"
        });
      }
    }
    cb(data);
  }
});
