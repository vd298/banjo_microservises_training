Ext.define("Crm.modules.roles.view.RolesForm", {
  extend: "Core.form.FormWindow",
  requires: ["Ext.form.HtmlEditor"],
  titleTpl: D.t("Roles"),
  iconCls: "x-fa fa-user",
  controllerCls: "Crm.modules.roles.view.RolesFormController",
  width: 500,
  height: 200,
  syncSize: function() {},
  buildItems: function() {
    return [
      {
        name: "id",
        hidden: true
      },
      {
        name: "role_name",
        fieldLabel: D.t("Name"),
        allowBlank: false,
        maxLength: 50,
        minLength: 1,
        validator: function(value) {
          return true;
        }
      },
      {
        name: "weight",
        fieldLabel: D.t("Weight"),
        allowBlank: false,
        minValue: 1,
        allowNegative: false,
        maskRe: /[0-9]/,
        allowPureDecimal: true,
        validator: function(value) {
          return true;
        }
      }
    ];
  },
  buildButtons: function() {
    var btns = [
      "->",
      {
        text: D.t("Save"),
        iconCls: "x-fa fa-check-square-o",
        scale: "medium",
        action: "save"
      },

      "-",
      { text: D.t("Close"), iconCls: "x-fa fa-ban", action: "formclose" }
    ];
    return btns;
  }
});
