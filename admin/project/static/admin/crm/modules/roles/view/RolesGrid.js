Ext.define("Crm.modules.roles.view.RolesGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("User Roles"),
  iconCls: "x-fa fa-users",

  filterable: true,
  filterbar: true,

  buildColumns: function() {
    return [
      {
        name: "id",
        type: "ObjectID",
        dataIndex: "id",
        sortable: true,
        filter: true,
        hidden: true
      },
      {
        text: D.t("name"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "role_name"
      },
      {
        text: D.t("weight"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "weight"
      }
    ];
  }
});
