Ext.define("Crm.modules.roles.view.RolesFormController", {
  extend: "Core.form.FormController",
  setControls() {
    this.control({
      "[name=role_name]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();
          const save = me.view.down("[action=save]");
          formData.role_name = formData.role_name.trim();
          this.model.runOnServer("checkRoles", formData, function(res) {
            if (!res.result) {
              e.validator = function(value) {
                return res.message;
              };
            } else {
              e.validator = function(value) {
                return true;
              };
            }
            me.view.down("form").isValid();
          });
        }
      }
    });
    this.callParent(arguments);
  }
});
