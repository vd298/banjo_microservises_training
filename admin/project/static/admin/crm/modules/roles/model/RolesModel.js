Ext.define("Crm.modules.roles.model.RolesModel", {
  extend: "Core.data.DataModel",

  collection: "roles",
  idField: "id",

  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "role_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "pid",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "weight",
      type: "integer",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    }
  ],
  async beforeSave(data, cb) {
    data.role_name = data.role_name.trim();
    cb(data);
  },
  /* scope:server */
  insertDataToDb: async function(data, cb) {
    const me = this;
    data.ctime = new Date();
    data.mtime = new Date();
    data = await me.addValidRealmsID(data);
    this.dbCollection.insert(data, cb);
  },
  /* scope:server */
  async $checkRoles(data, cb) {
    if (data && data.role_name && data.id) {
      data.role_name = data.role_name.trim();
      let sql = `select role_name from roles where removed=0 and role_name ilike $1 and id<>$2`;
      sql = await this.buildRealmConditionQuery(sql);
      const res = await this.src.db.query(sql, [data.role_name, data.id]);
      if (res && res.length) {
        return cb({
          result: false,
          message: "Role name must be unique."
        });
      } else {
        return cb({ result: true });
      }
    } else {
      return cb({ result: false, message: "Role name required." });
    }
  }
});
