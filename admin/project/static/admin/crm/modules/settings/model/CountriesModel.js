Ext.define("Crm.modules.settings.model.CountriesModel", {
  extend: "Core.data.DataModel",

  collection: "countries",
  idField: "id",
  strongRequest: true,
  removedFilter: true,
  //removeAction: "remove",

  maxLimit: 1000,

  fields: [
    {
      name: "id",
      type: "int",
      visible: true
    },
    {
      name: "name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "code",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "abbr2",
      type: "string",
      sort: 1,
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "abbr3",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    }
  ],
  async afterGetData(data, cb) {
    if (data.length) {
      data.map((key) => {
        key.country_code = key.name + " " + "(+" + key.code + ")";
      });
    }
    cb(data);
  }
});
