Ext.define("Crm.modules.accounts.view.UserAccountInvitationsFormController", {
  extend: "Core.form.FormController",

  setControls() {
    const me = this;
    let firstTime = true;
    this.control({
      "[action=cancelInvitation]": {
        click: () => {
          const me = this;
          const formData = me.view.down("form").getValues();
          this.model.cancelInvitation({ id: formData.id }, function(res) {
            if (res.result && res.result.success) {
              return Ext.Msg.alert("Alert", res.result.message);
            } else if (res.error) {
              return Ext.Msg.alert("Error", res.error.message);
            }
          });
          me.view.down("[name=cancelInvitation]").disable();
        }
      },
      "[name=user_type]": {
        change: function(v) {
          const roleCombo = me.view.down("[name=role]");
          const email = me.view.down("[name=email]");
          const owner = me.view.down("[name=owner_id]");
          const ips = me.view.down("[name=ips]");
          const webhook_url = me.view.down("[name=webhook_url]");
          const allow = me.view.down("[name=allow]");
          const deny = me.view.down("[name=deny]");
          const write = me.view.down("[name=write]");
          const merchant_account_id = me.view.down(
            "[name=merchant_account_id]"
          );
          if (firstTime) {
            me.setMerchantAccountsStore(me);
            firstTime = false;
          }
          if (v.value === "API") {
            me.setOwnerUserStore(me);
            //PORTAL fields
            roleCombo.setConfig("hidden", true);
            roleCombo.setConfig("allowBlank", true);
            email.setConfig("hidden", true);
            email.setConfig("allowBlank", true);

            //API fields
            owner.setConfig("hidden", false);
            owner.setConfig("allowBlank", false);
            ips.setConfig("hidden", false);
            webhook_url.setConfig("hidden", false);
            webhook_url.setConfig("allowBlank", false);
            allow.setConfig("hidden", false);
            deny.setConfig("hidden", false);
            write.setConfig("hidden", false);
            merchant_account_id.setConfig("hidden", false);
            merchant_account_id.setConfig("allowBlank", false);
          } else {
            //PORTAL fields
            roleCombo.setConfig("hidden", false);
            roleCombo.setConfig("allowBlank", false);
            email.setConfig("hidden", false);
            email.setConfig("allowBlank", false);

            //API fields
            owner.setConfig("hidden", true);
            owner.setConfig("allowBlank", true);
            ips.setConfig("hidden", true);
            webhook_url.setConfig("hidden", true);
            webhook_url.setConfig("allowBlank", true);
            allow.setConfig("hidden", true);
            deny.setConfig("hidden", true);
            write.setConfig("hidden", true);
            merchant_account_id.setConfig("hidden", false);
            merchant_account_id.setConfig("allowBlank", false);
          }
        }
      }
    });

    this.populateComboboxes();
    this.makeEditable();
    this.callParent(arguments);
  },
  afterDataLoad(data, cb) {
    if (!data.token || data.token == "" || data.accepted) {
      this.view.down("[name=cancelInvitation]").disable();
      let removeBtn = this.view.down(`[action=remove]`);
      if (removeBtn) {
        removeBtn.disable();
      }
    }
    cb(data);
  },
  populateComboboxes() {
    var me = this;

    this.model.populateComboBox(
      {
        query:
          "SELECT unnest(enum_range(NULL::enum_users_user_type)) as user_type;"
      },
      function(res) {
        if (!!res.length && res.length) {
          me.view.userTypesStore.loadData(
            res.map((row) => {
              return { key: row.user_type, name: row.user_type };
            })
          );
          me.loadData(function(data) {});
        }
      }
    );
  },

  setMerchantAccountsStore(me) {
    var form = me.view.down("form").getForm();
    if (form) {
      data = form.getValues();
      console.log("fired", data);
      const proxyFilters = {
        filters: [
          {
            _property: "account_id",
            _value: data.account_id,
            _operator: "eq"
          }
        ]
      };
      const store = Ext.create("Core.data.ComboStore", {
        storeId: "MerchantAccountsModel",
        dataModel: Ext.create(
          "Crm.modules.accounts.model.MerchantAccountsModel"
        ),
        fieldSet: ["id", "name"],
        exProxyParams: proxyFilters,
        scope: me
      });
      me.view.down("[name=merchant_account_id]").setStore(store);
    }
  },

  setOwnerUserStore(me) {
    var form = me.view.down("form").getForm();
    if (form) {
      data = form.getValues();
      console.log("fired", data);
      const proxyFilters = {
        filters: [
          {
            _property: "account_id",
            _value: data.account_id,
            _operator: "eq"
          }
        ]
      };
      const store = Ext.create("Core.data.ComboStore", {
        storeId: "UserAccountOwnerCombo",
        dataModel: Ext.create(
          "Crm.modules.accounts.model.UserAccountOwnerCombo"
        ),
        fieldSet: ["id", "email", "first_name", "last_name"],
        exProxyParams: proxyFilters,
        scope: me
      });
      me.view.down("[name=owner_id]").setStore(store);
    }
  },

  makeEditable() {
    this.model.checkIfExists({ id: this.view.recordId }, (resp) => {
      if (!resp) {
        this.view
          .down("form")
          .down("[name=email]")
          .setReadOnly(false);
        this.view
          .down("form")
          .down("[name=user_type]")
          .setReadOnly(false);
        this.view
          .down("form")
          .down("[name=account_id]")
          .setReadOnly(false);
        this.view
          .down("form")
          .down("[name=role]")
          .setReadOnly(false);
        this.view
          .down("form")
          .down("[name=telegram_id]")
          .setHidden(true);
        this.view
          .down("form")
          .down("[name=merchant_account_id]")
          .setReadOnly(false);
      }
      if (resp.length && resp[0].accepted) {
        this.view
          .down("form")
          .down("[name=status]")
          .setHidden(false)
          .setConfig("allowBlank", false);
      } else {
        this.view
          .down("form")
          .down("[name=status]")
          .setHidden(true)
          .setConfig("allowBlank", true);
      }
    });
  }
});
