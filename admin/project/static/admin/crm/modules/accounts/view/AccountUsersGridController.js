Ext.define("Crm.modules.accounts.view.AccountUsersGridController", {
  extend: "Core.grid.GridController",

  gotoRecordHash: function(data) {
    if (!!this.view.observeObject) {
      window.__CB_REC__ = this.view.observeObject;
    }
    if (data && data[this.view.model.idField]) {
      var hash =
        this.generateDetailsCls() + "~" + data[this.view.model.idField];
      if (data["email"]) {
        Ext.create("Crm.modules.accounts.view.EditUsersForm", {
          noHash: true,
          recordId: data[this.view.model.idField]
        });
      } else if (this.view.detailsInNewWindow) window.open("./#" + hash);
      else location.hash = hash;
    }
  },
  deleteRecord: function(store, index) {
    let me = this;
    Ext.Msg.confirm(
      D.t("Delete Record"),
      D.t("Do you want to delete?", []),
      function(btn) {
        if (btn == "yes") {
          let rec = store.getAt(index);
          me.model.runOnServer("deleteUser", { id: rec.id }, function(res) {
            if (res) {
              // me.view.fireEvent("reload");
              store.reload();
            }
          });
        }
      }
    );
  }
});
