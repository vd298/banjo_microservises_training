Ext.define("Crm.modules.accounts.view.MerchantTransfersGrid", {
  extend: "Crm.modules.Transfers.view.TransfersGrid",

  buildColumns: function() {
    var me = this;
    var parentObj = me.callParent(arguments);
    if (parentObj && parentObj.length > 0) {
      for (let i = 0; i < parentObj.length; i++) {
        if (
          parentObj[i] &&
          ["src_acc_name"].indexOf(parentObj[i].dataIndex) > -1
        ) {
          parentObj.splice(i, 1);
          i--;
        }
      }
    }
    return parentObj;
  }
});
