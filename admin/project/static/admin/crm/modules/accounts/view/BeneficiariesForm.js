Ext.define("Crm.modules.accounts.view.BeneficiariesForm", {
  extend: "Core.form.FormWindow",

  titleTpl: D.t("Beneficiary"),
  requires: ["Desktop.core.widgets.GridField"],
  formLayout: "fit",
  closeHash: "Crm.modules.accounts.view.BeneficiariesGrid",
  onClose: function(me) {
    window.history.back();
  },
  formMargin: 0,

  width: "50vw",
  maxHeight: 650,
  height: "65vh",
  model: "Crm.modules.accounts.model.BeneficiariesModel",

  syncSize: function() {},

  controllerCls: "Crm.modules.accounts.view.BeneficiariesFormController",

  buildItems() {
    var me = this;
    return {
      xtype: "tabpanel",
      layout: "fit",
      id: "MerchantAccountsFormTabPanel",
      items: [this.buildDetailsPanel()],
      autoEl: { "data-test": "tab-items" }
    };
  },

  buildDetailsPanel() {
    return {
      xtype: "panel",
      layout: "anchor",
      autoScroll: true,
      title: D.t("Details"),
      defaults: {
        anchor: "100%",
        xtype: "textfield",
        labelWidth: 150,
        margin: 5
      },
      items: [
        {
          name: "account_id",
          hidden: true
        },
        {
          name: "id",
          hidden: true
        },
        {
          name: "ctime",
          hidden: true
        },
        this.buildMerchantAccountCombo(),
        this.buildProtocolCombo(),
        {
          fieldLabel: D.t("First name"),
          name: "first_name",
          allowBlank: false
        },
        {
          fieldLabel: D.t("Last name"),
          name: "last_name",
          allowBlank: false
        },
        {
          fieldLabel: D.t("Email"),
          name: "email",
          maxLength: 50,
          regex: /^(")?(?:[^\."])(?:(?:[\.])?(?:[\w\-!#$%&'*+\/=?\^_`{|}~]))*\1@(\w[\-\w]*\.){1,5}([A-Za-z]){2,6}$/,
          regexText: "Invalid email address"
        },
        {
          name: "country_code",
          fieldLabel: D.t("Country Code"),
          maxLength: 6,
          maskRe: /^[\-\+0-9]{1,6}$/
        },
        {
          fieldLabel: D.t("Mobile number"),
          name: "mobile",
          maxLength: 15,
          xtype: "numberfield"
        },
        // {
        //   xtype: "fieldcontainer",
        //   layout: "vbox",
        //   name: "bank_fields",
        //   defaults: {
        //     anchor: "100%",
        //     xtype: "textfield",
        //     labelWidth: 150
        //   },
        //   items: [
        {
          name: "bank_name",
          fieldLabel: D.t("Bank Name"),
          hidden: true
        },
        {
          name: "acc_no",
          fieldLabel: D.t("Account Number"),
          hidden: true,
          xtype: "numberfield"
        },
        {
          name: "re_acc_no",
          fieldLabel: D.t("Re-Enter Account Number"),
          hidden: true,
          xtype: "numberfield"
        },
        {
          name: "ifsc",
          fieldLabel: D.t("IFSC"),
          hidden: true
        },
        //   ]
        // },
        // {
        //   xtype: "fieldcontainer",
        //   layout: "vbox",
        //   name: "crypto_fields",
        //   defaults: {
        //     anchor: "100%",
        //     xtype: "textfield",
        //     labelWidth: 150
        //   },
        //   items: [
        {
          name: "network_type",
          fieldLabel: D.t("Network type"),
          hidden: true
        },
        {
          name: "crypto_wallet_address",
          fieldLabel: D.t("Crypto wallet address"),
          hidden: true
        },
        {
          name: "description",
          fieldLabel: D.t("Description"),
          hidden: true
        },
        //   ]
        // },
        // {
        //   xtype: "fieldcontainer",
        //   layout: "vbox",
        //   name: "upi_fields",
        //   defaults: {
        //     anchor: "100%",
        //     xtype: "textfield",
        //     labelWidth: 150
        //   },
        //   items: [
        {
          name: "vpa",
          fieldLabel: D.t("VPA"),
          hidden: true
        }
        //   ]
        // }
      ]
    };
  },

  buildMerchantAccountCombo() {
    return {
      name: "merchant_account_id",
      xtype: "combo",
      fieldLabel: D.t("Merchant Account"),
      queryMode: "all",
      displayField: "name",
      editable: true,
      valueField: "id",
      validator: true,
      allowBlank: false,
      minChars: 1,
      store: Ext.create("Core.data.ComboStore", {
        storeId: "account_id",
        dataModel: Ext.create(
          "Crm.modules.accounts.model.MerchantAccountsModel"
        ),
        fieldSet: ["name", "id"],
        scope: this
      })
    };
  },

  buildProtocolCombo() {
    return {
      name: "protocols",
      fieldLabel: D.t("Payment Protocol"),
      xtype: "tagfield",
      editable: true,
      allowBlank: false,
      validateBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "all",
      displayField: "name",
      flex: 1,
      valueField: "_id",
      tpl:
        '<tpl for="."><div class="x-boundlist-item" style="height:40px;"><b>{name}</b>&nbsp;&nbsp;(label: {identifier_label})</div><div style="clear:both"></div></tpl>',
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create(
          "Crm.modules.bankAccounts.model.PaymentTypeCombo"
        ),
        fieldSet: ["_id", "name", "identifier_label"],
        scope: this
      }),
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      }
    };
  }
});
