Ext.define("Crm.modules.accounts.view.AccountsGridController", {
  extend: "Core.grid.GridController",

  setControls: function() {
    var me = this;
    me.callParent(arguments);
  },

  addRecord: function() {
    var me = this;
    me.view.model.getNewObjectId(function(_id) {
      var oo = {};
      oo[me.view.model.idField] = _id;
      return me.createNewAccount(oo);
    });
  },

  /**
   * @method createNewAccount
   * @author Vaibhav Vaidya
   * @since 6 Dec 2022
   * @summary Window with Card layout panel to handle for new Account creation
   */
  createNewAccount: function(formData) {
    var me = this;
    var adjustWindow = Ext.create("Ext.window.Window", {
      title: "Create New Merchant Profile",
      name: "createMerchantWindow",
      height: "80vh",
      maxHeight: 550,
      width: "70vw",
      layout: "fit",
      margin: 20,
      modal: true,
      items: [
        {
          xtype: "panel",
          width: "60vw",
          height: "80vh",
          layout: "card",
          bodyStyle: "padding:10px",
          name: "mainCreatePanel",
          margin: 20,
          modal: true,
          deferredRender: true,
          defaults: {
            border: false
          },
          bbar: [
            {
              id: "move-prev",
              text: "Back",
              xtype: "button",
              cls: "account-wizard-button",
              handler: function(btn) {
                me.navigate(adjustWindow, btn.up("panel"), "prev");
              },
              border: 5,
              style: {
                backgroundColor: "#35baf6",
                borderColor: "#35baf6",
                borderStyle: "solid",
                color: "#fff"
              },
              disabled: true,
              hidden: true
            },
            "->",
            {
              id: "move-next",
              text: "Step 2: Create Merchant Bank Accounts",
              xtype: "button",
              cls: "account-wizard-button",
              handler: function(btn) {
                me.navigate(adjustWindow, btn.up("panel"), "next");
              },
              border: 5,
              style: {
                backgroundColor: "#35baf6",
                borderColor: "#35baf6",
                borderStyle: "solid",
                color: "#fff"
              },
              disabled: true
            }
          ],
          items: [
            {
              id: "account_1",
              name: "account_1",
              xtype: "form",
              layout: "anchor",
              scrollable: true,
              submitEmptyText: false,
              defaults: {
                labelWidth: "15",
                width: "70%",
                labelStyle: "white-space: nowrap;"
              },
              defaultType: "textfield",
              items: [
                {
                  name: "id",
                  hidden: true,
                  value: me.uuidv4()
                },
                {
                  fieldLabel: "Account Name:",
                  name: "name",
                  allowBlank: false,
                  submitEmptyText: false,
                  tabIndex: 1,
                  maxLength: 50,
                  listeners: {
                    afterrender(e, eOpts) {
                      __CONFIG__.setComponentEmptyText(e, eOpts);
                    }
                  }
                },
                {
                  xtype: "textarea",
                  fieldLabel: "Address:",
                  name: "address",
                  allowBlank: false,
                  submitEmptyText: false,
                  maxLength: 150,
                  tabIndex: 2,
                  listeners: {
                    afterrender(e, eOpts) {
                      __CONFIG__.setComponentEmptyText(e, eOpts);
                    }
                  }
                },
                {
                  fieldLabel: "City:",
                  name: "city",
                  allowBlank: false,
                  submitEmptyText: false,
                  maxLength: 50,
                  tabIndex: 3,
                  listeners: {
                    afterrender(e, eOpts) {
                      __CONFIG__.setComponentEmptyText(e, eOpts);
                    }
                  }
                },
                __CONFIG__.buildCountryCombo({
                  labelWidth: "15",
                  width: "70%"
                }),
                {
                  xtype: "checkbox",
                  fieldLabel: "Legal Entity:",
                  name: "legal_entity",
                  allowBlank: false,
                  submitValue: true,
                  uncheckedValue: false,
                  submitEmptyText: false,
                  listeners: {
                    change: function(e, newVal, oldVal, eOpts) {
                      let regNo = adjustWindow.down("[name=registration_no]");
                      let currForm = adjustWindow.down("[name=account_1]");
                      if (regNo && (newVal == true || newVal == 1)) {
                        regNo.setEditable(true);
                        regNo.allowBlank = false;
                        __CONFIG__.setComponentEmptyText(regNo, eOpts);
                        if (currForm) currForm.isValid();
                      } else if (regNo) {
                        regNo.setValue("");
                        regNo.setEditable(false);
                        regNo.allowBlank = true;
                        regNo.setEmptyText("Not required for non-legal entity");
                        if (currForm) currForm.isValid();
                      }
                    }
                  }
                },
                {
                  fieldLabel: "Registration Number:",
                  name: "registration_no",
                  allowBlank: true,
                  submitEmptyText: false,
                  editable: false,
                  maxLength: 30,
                  emptyText: "Not required for non-legal entity"
                },
                // __CONFIG__.buildPlansCombo({
                //   labelWidth: "15",
                //   width: "60%",
                // }),
                __CONFIG__.buildTypeCombo({
                  labelWidth: "15",
                  width: "70%",
                  value: "MERCHANT",
                  editable: false,
                  forceSelection: false,
                  readOnly: true,
                  value: "MERCHANT"
                }),
                __CONFIG__.buildStatusCombo({
                  labelWidth: "15",
                  width: "70%",
                  editable: false,
                  forceSelection: false,
                  value: "INACTIVE"
                }),
                __CONFIG__.buildRealmsCombo({
                  labelWidth: "15",
                  width: "70%",
                  editable: true
                  // defaultValue: "046cce25-f407-45c7-8be9-3bf198093408",
                  // value: "046cce25-f407-45c7-8be9-3bf198093408"
                })
              ],
              listeners: {
                activate: function(context, eOpts) {
                  if (adjustWindow) me.checkSetButtons(adjustWindow);
                },
                validitychange: function(context, valid, eOpts) {
                  if (adjustWindow) me.checkSetButtons(adjustWindow, valid);
                },
                resize: function(c, oW, oH, nW, nH) {
                  me.syncSize(c);
                },
                afterRender: function(e, eOpts) {
                  e.isValid();
                  Ext.on("resize", function() {
                    me.syncSize(e);
                  });
                }
              }
            },
            {
              id: "account_2",
              xtype: "form",
              layout: "anchor",
              defaultType: "textfield",
              reserveScrollbar: true,
              submitEmptyText: false,
              autoScroll: true,
              fieldDefaults: {
                width: "70%",
                xtype: "displayfield",
                labelWidth: "15"
              },
              items: [
                {
                  xtype: "displayfield",
                  name: "info_text",
                  readOnly: true,
                  editable: false,
                  shrinkwrap: 3,
                  style: {
                    "font-weight": "bold"
                  },
                  value: `Create Merchant Bank Account(Atleast one necessary)`
                },
                me.buildAccountsPanel(formData),
                {
                  xtype: "menuseparator",
                  width: "100%",
                  hidden: true
                },
                {
                  xtype: "label",
                  name: "warn_label_1",
                  hidden: true,
                  text: D.t(""),
                  style: {
                    color: "red"
                  }
                }
              ],
              listeners: {
                activate: function(context, eOpts) {
                  if (adjustWindow) {
                    let FormResp = me.checkSetButtons(adjustWindow, {
                      gridName: "CreationMerchantAccountsGrid"
                    });
                    if (FormResp == false) {
                      me.checkGridValidity(adjustWindow, {
                        name: "CreationMerchantAccountsGrid"
                      });
                    }
                  }
                },
                validitychange: function(context, valid, eOpts) {
                  if (adjustWindow) {
                    let FormResp = me.checkSetButtons(adjustWindow);
                    if (FormResp == false) {
                      me.checkGridValidity(adjustWindow, {
                        name: "CreationMerchantAccountsGrid"
                      });
                    }
                  }
                }
              }
            },
            {
              //Displaying of Account's TariffPlans
              id: "account_3",
              xtype: "form",
              layout: "anchor",
              defaultType: "displayfield",
              reserveScrollbar: true,
              submitEmptyText: false,
              autoScroll: true,
              fieldDefaults: {
                width: "70%",
                xtype: "displayfield",
                labelWidth: "15"
              },
              items: [
                {
                  xtype: "displayfield",
                  name: "info_text",
                  readOnly: true,
                  editable: false,
                  shrinkwrap: 3,
                  style: {
                    "font-weight": "bold"
                  },
                  value: `Select the applicable Tariff Plan`
                },
                me.buildTariffPlansPanel(formData)
              ],
              listeners: {
                activate: function(context, eOpts) {
                  if (adjustWindow) {
                    me.checkSetButtons(adjustWindow);
                  }
                },
                validitychange: function(context, valid, eOpts) {
                  if (adjustWindow) me.checkSetButtons(adjustWindow, valid);
                }
              }
            },
            {
              //Displaying of Account's Users
              id: "account_4",
              xtype: "form",
              layout: "anchor",
              name: "ownerInviteForm",
              defaultType: "textfield",
              reserveScrollbar: true,
              autoScroll: true,
              fieldDefaults: {
                width: "70%",
                xtype: "textfield",
                labelWidth: "15"
              },
              items: [
                {
                  xtype: "displayfield",
                  name: "user_auth_text",
                  readOnly: true,
                  editable: false,
                  shrinkwrap: 3,
                  value: `Account's Users`
                },
                //Vaibhav Vaidya, BANJO-625, Only single User i.e Owner will be registered during account creation
                //me.buildAccountUsersGrid(formData),
                {
                  fieldLabel: "Email of Owner:",
                  name: "email",
                  allowBlank: false,
                  submitEmptyText: false,
                  maxLength: 50,
                  regex: /^(")?(?:[^\."])(?:(?:[\.])?(?:[\w\-!#$%&'*+\/=?\^_`{|}~]))*\1@(\w[\-\w]*\.){1,5}([A-Za-z]){2,6}$/,
                  regexText: "Invalid email address",
                  listeners: {
                    afterrender(e, eOpts) {
                      __CONFIG__.setComponentEmptyText(e, eOpts);
                    }
                  }
                },
                me.buildUserTypesCombo(),
                me.buildRolesCombo(),
                {
                  flex: 1,
                  xtype: "displayfield",
                  name: "role_display_text",
                  fieldLabel: " ",
                  allowBlank: true,
                  readOnly: true,
                  hidden: true
                }
                // {
                //   xtype: 'menuseparator',
                //   width: '100%'
                // }
              ],
              listeners: {
                activate: function(context, eOpts) {
                  if (adjustWindow)
                    me.checkSetButtons(
                      adjustWindow,
                      null,
                      `[name=ownerInviteForm]`
                    );
                },
                validitychange: function(context, valid, eOpts) {
                  if (adjustWindow)
                    me.checkSetButtons(
                      adjustWindow,
                      valid,
                      `[name=ownerInviteForm]`
                    );
                }
              }
            }
          ],
          renderTo: Ext.getBody()
        }
      ],
      listeners: {
        resize: function(context, width, height, eOpts) {}
      }
    }).show();
  },

  syncSize: function(c) {
    var width = Ext.Element.getViewportWidth(),
      height = Ext.Element.getViewportHeight();
    c.setHeight(Math.floor(height * 0.9));
  },

  gridCheck: function(adjustWindow, eOpts) {
    Ext.GlobalEvents.fireEvent(`${eOpts.name}ValidCheck`);
  },

  checkGridValidity: function(adjustWindow, eOpts) {
    var me = this;
    var nBtn = adjustWindow.down("[id=move-next]"),
      currentGrid = adjustWindow.down(`[name=${eOpts.name}]`);
    if (nBtn) {
      if (
        currentGrid &&
        currentGrid.store &&
        currentGrid.store.getRange() &&
        currentGrid.store.getRange().length
      ) {
        nBtn.setDisabled(false);
        return false;
      } else if (nBtn) {
        nBtn.setDisabled(true);
        return true;
      }
    }
    return false;
  },

  checkSetButtons: function(adjustWindow, valid, name = "form") {
    var nBtn = adjustWindow.down("[id=move-next]"),
      currentForm = adjustWindow.down(name);
    if (nBtn) {
      if (valid == undefined && currentForm && currentForm.isValid) {
        valid = currentForm.isValid();
      }
      if (valid == false) {
        nBtn.setDisabled(true);
        return true;
      } else {
        nBtn.setDisabled(false);
        return false;
      }
    }
    return false;
  },

  navigate: async function(adjustWindow, panel, direction) {
    var me = this;
    var layout = panel.getLayout(),
      currentForm,
      currentFormContext,
      currentFormVals,
      createResp;
    //Vaibhav Vaidya, Note: Based on Panel inputs, check their input first then proceed
    if (layout && layout.activeItem && layout.activeItem.id) {
      currentForm = layout.activeItem.id;
      currentFormContext = layout.activeItem.form;
      currentFormVals = layout.activeItem.form.getValues();
    }
    let panel1Str = `Step 1: Merchant Basic Info`;
    let panel2Str = `Step 2: Create Merchant Accounts`;
    let panel3Str = `Step 3: Assign Tariff Plans`;
    let panel4Str = `Step 4(Final): Create Merchant User Accounts`;
    let submitStr = `Submit Merchant Creation Request`;
    switch (currentForm) {
      //Basic info
      case "account_1":
        {
          if (direction == "next") {
            let formCheckResp,
              basicInfo = panel.down("[id=account_1]"),
              formVals = { basic_info: {} },
              msg =
                "Problem displaying status of Create Merchant request. Contact Support Team";
            if (basicInfo && basicInfo.form) {
              formVals.basic_info = basicInfo.form.getValues();
            }
            await me
              .checkBasicMerchant(formVals)
              .then(function(formCheckResp) {
                layout[direction]();
                Ext.getCmp("move-next")
                  .setHidden(false)
                  .setText(panel3Str);
                Ext.getCmp("move-prev")
                  .setDisabled(false)
                  .setHidden(false)
                  .setText(panel1Str);
              })
              .catch(function(formCheckResp) {
                if (
                  formCheckResp &&
                  formCheckResp.result &&
                  formCheckResp.result.error &&
                  Array.isArray(formCheckResp.result.error)
                ) {
                  for (let currErr of formCheckResp.result.error) {
                    let el = adjustWindow.down(`[name=${currErr.field}]`);
                    if (el && !!el.setActiveError) {
                      el.setActiveError(currErr.message);
                    }
                  }
                } else if (
                  formCheckResp &&
                  formCheckResp.success == false &&
                  formCheckResp.message
                ) {
                  me.showToastMessage(formCheckResp);
                }
              });
          }
          return;
        }
        break;
      //Merchant Accounts
      case "account_2":
        {
          if (direction == "next") {
            layout[direction]();
            Ext.getCmp("move-prev").setHidden(false);
            Ext.getCmp("move-next").setText(panel4Str);
            Ext.getCmp("move-prev").setText(panel2Str);
          } else {
            layout[direction]();
            Ext.getCmp("move-next").setText(panel2Str);
            Ext.getCmp("move-prev").setHidden(true);
          }
        }
        break;
      //Merchant TariffPlans
      case "account_3":
        {
          let account_4 = panel.down("[id=account_4]");
          //add Owner type role
          account_4
            .down("[name=role_display_text]")
            .setHidden(true)
            .setValue("");
          await me.model.runOnServer("fetchOwnerRoleId", {}, function(res) {
            if (res && res.id) {
              console.log(res.id);
              account_4.down("[name=role]").setValue(res.id);
            } else {
              account_4
                .down("[name=role_display_text]")
                .setHidden(false)
                .setValue("Need to create Owner role ");
            }
          });
          if (direction == "next") {
            layout[direction]();
            Ext.getCmp("move-prev").setHidden(false);
            Ext.getCmp("move-next").setText(submitStr);
            Ext.getCmp("move-prev").setText(panel3Str);
          } else {
            layout[direction]();
            Ext.getCmp("move-next").setText(panel3Str);
            Ext.getCmp("move-prev").setText(panel1Str);
          }
        }
        break;
      //Merchant Users
      case "account_4":
        {
          if (direction == "next") {
            let basicInfo = panel.down("[id=account_1]"),
              formVals = { basic_info: {} },
              msg =
                "Problem displaying status of Create Merchant request. Contact Support Team";
            if (basicInfo && basicInfo.form) {
              formVals.basic_info = basicInfo.form.getValues();
            }
            var tariffInfo = panel.down("[id=account_3]");
            if (tariffInfo && tariffInfo.form) {
              Object.assign(formVals.basic_info, tariffInfo.form.getValues());
              if (formVals.basic_info.tariff)
                formVals.basic_info.plan_id = formVals.basic_info.tariff;
            }
            var merchantAccountsComp = panel.down("[id=account_2]");
            if (merchantAccountsComp) {
              var merchAccStore = merchantAccountsComp.down(
                "[name=CreationMerchantAccountsGrid]"
              );
              if (
                merchAccStore &&
                merchAccStore.store &&
                merchAccStore.store.getRange().length
              ) {
                formVals.merchant_accounts = merchAccStore.store
                  .getRange()
                  .map((r) => {
                    return r.data;
                  });
              }
            }
            var userAccounts = panel.down("[id=account_4]");
            if (userAccounts && userAccounts.form) {
              formVals.invited_users = [userAccounts.form.getValues()];
              // var userAccStore = userAccounts.down("[name=CreationMerchantUsersGrid]");
              // if (userAccStore && userAccStore.store && userAccStore.store.getRange().length) {
              //   formVals.invited_users = userAccStore.store.getRange().map(r => { return r.data });
              // }
            }
            Ext.getCmp("move-next").setDisabled(true);
            createResp = await me
              .performMerchantCreate(formVals)
              .then(function(val) {
                adjustWindow.close();
                if (!!val.result.allBanksAssigned) {
                  msg =
                    "Merchant Creation successful. All bank accounts associated successfully.";
                  if (val.result.name)
                    msg += `<br> Merchant Name: ${val.result.name} <br>`;
                  if (val.result.registration_no != undefined)
                    msg += `<br> Registration Number: ${val.result.registration_no} <br>`;
                  if (val.result.status != undefined)
                    msg += `<br> Merchant Status: ${val.result.status} <br>`;
                  D.a("Merchant Creation Successful", msg, [], () => {});
                } else {
                  msg =
                    "Some bank accounts failed to associate with merchant account. Please add bank accounts manually.";
                  if (val.result.name)
                    msg += `<br> Merchant Name: ${val.result.name} <br>`;
                  if (val.result.registration_no != undefined)
                    msg += `<br> Registration Number: ${val.result.registration_no} <br>`;
                  if (val.result.status != undefined)
                    msg += `<br> Merchant Status: ${val.result.status} <br>`;
                  D.a("Merchant Creation Successful", msg, [], () => {
                    window.open(
                      "#Crm.modules.accounts.view.AccountsForm~" +
                        val.result.id,
                      "_self"
                    );
                  });
                }

                me.view.store.reload();
              })
              .catch(function(val) {
                Ext.getCmp("move-next").setDisabled(true);
                msg =
                  "Problem during merchant creation, Contact support team if error persists";
                if (val && val.message) msg = val.message;
                else if (val && val.error && val.error.message)
                  msg = val.error.message;
                D.a("Merchant creation failure", msg);
              });
          } else {
            layout[direction]();
            Ext.getCmp("move-prev");
            Ext.getCmp("move-next").setText(panel3Str);
            Ext.getCmp("move-prev").setText(panel2Str);
          }
        }
        break;
      default:
        {
          D.a(
            "Invalid Form Selected",
            "Please close form and restart the process. Contact Support Team incase problem persists."
          );
        }
        break;
    }
  },

  buildAccountsPanel(formData) {
    return Ext.create(
      "Crm.modules.CreationMerchantAccounts.view.CreationMerchantAccountsGrid",
      {
        scope: this,
        account_id: formData.id,
        name: "CreationMerchantAccountsGrid",
        observe: [{ property: "account_id", param: "id" }]
      }
    );
  },

  buildTariffPlansPanel(formData) {
    return Ext.create(
      "Crm.modules.CreationMerchantTariffPlans.view.CreationTariffSettingsPanel",
      {
        scope: this,
        height: 400,
        account_id: formData.id,
        name: "CreationTariffSettingsPanel",
        observe: [{ property: "account_id", param: "id" }]
      }
    );
  },

  buildAccountUsersGrid(formData) {
    return Ext.create(
      "Crm.modules.CreationMerchantUsers.view.CreationMerchantUsersGrid",
      {
        scope: this,
        account_id: formData.id,
        name: "CreationMerchantUsersGrid",
        observe: [{ property: "account_id", param: "id" }]
      }
    );
  },

  performMerchantCreate: function(formVals) {
    var me = this,
      funcName = "createMerchantAccount";
    var createPromise = new Promise(function(resolve, reject) {
      me.startLoadingAnim();
      me.model.runOnServer(funcName, formVals, function(respData) {
        me.stopLoadingAnim();
        if (respData && respData.actionStatus == true) {
          resolve(respData);
        } else {
          reject(respData);
        }
      });
    });
    return createPromise;
  },

  checkBasicMerchant: function(formVals) {
    var me = this,
      funcName = "checkBasicMerchant";
    var checkPromise = new Promise(function(resolve, reject) {
      me.model.runOnServer(funcName, formVals, function(respData) {
        if (respData && respData.actionStatus == true) {
          resolve(respData);
        } else {
          reject(respData);
        }
      });
    });
    return checkPromise;
  },

  buildCountryCombo: function(config) {
    let combo = {
      xtype: "dependedcombo",
      name: "country",
      editable: true,
      fieldSet: "id,abbr2,name",
      fieldLabel: "Country",
      allowBlank: false,
      forceSelection: true,
      valueField: "abbr2",
      displayField: "name",
      parentEl: null,
      parentField: null,
      dataModel: "Crm.modules.settings.model.CountriesModel",
      listeners: {
        afterrender(e, eOpts) {
          __CONFIG__.setComponentEmptyText(e, eOpts);
        }
      }
    };
    Object.assign(combo, config);
    return combo;
  },

  startLoadingAnim: function() {
    Ext.MessageBox.wait("Processing...");
  },
  stopLoadingAnim: function() {
    Ext.MessageBox.hide();
  },

  uuidv4: function() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
      (
        c ^
        (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
      ).toString(16)
    );
  },

  buildUserTypesCombo() {
    var me = this;
    return {
      name: "user_type",
      labelWidth: 150,
      fieldLabel: D.t("User Type"),
      xtype: "dependedcombo",
      queryMode: "all",
      forceSelection: true,
      triggerAction: "all",
      editable: false,
      value: "PORTAL",
      defaultValue: "PORTAL",
      allowBlank: false,
      readOnly: true,
      valueField: "key",
      displayField: "key",
      modelConfig: { enum_name: "enum_users_user_type" },
      dataModel: "Crm.modules.Util.model.FetchComboRecords",
      operator: "eq",
      listeners: {
        afterrender(e, eOpts) {
          __CONFIG__.setComponentEmptyText(e, eOpts);
        }
      }
    };
  },

  buildRolesCombo() {
    return {
      xtype: "dependedcombo",
      name: "role",
      labelWidth: 150,
      fieldLabel: D.t("Main Role"),
      editable: true,
      forceSelection: true,
      fieldSet: "id,role_name",
      valueField: "id",
      displayField: "role_name",
      // value: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4",
      // defaultValue: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4",
      parentEl: null,
      parentField: null,
      dataModel: "Crm.modules.roles.model.RolesModel",
      readOnly: true
    };
  }
});
