Ext.define("Crm.modules.accounts.view.UserAccountInvitationsGridController", {
  extend: "Core.grid.GridController",

  setControls: function() {
    var me = this;
    this.control({
      "[action=refresh]": {
        click: function(el) {
          me.reloadData();
        }
      }
    });

    this.view.on("resendInvitation", function(grid, index) {
      const me = this;
      Ext.Msg.confirm(
        D.t("Resend Invitation"),
        D.t("Do you want to resend the invitation?", []),
        function(btn) {
          if (btn == "yes") {
            let data = grid.eventPosition.record.data;
            me.model.resendInvitation(data, function(res) {
              if (res.result) {
                return Ext.Msg.alert("Alert", "Resent invitation");
              } else if (res.error) {
                D.a(res.error.title, res.error.message);
              }
            });
          }
        }
      );
    });

    this.view.on("cancelInvitation", function(grid, index) {
      let data = grid.eventPosition.record.data;
      this.model.cancelInvitation({ id: data.id, email: data.email }, function(
        res
      ) {
        if (res.result && res.result.success) {
          return Ext.Msg.alert("Alert", "Invitation cancelled");
        } else if (res.error) {
          return Ext.Msg.alert("Error", res.error);
        }
      });
    });

    //yc274 temporary fix for dialog
    setTimeout(() => {
      me.reloadData();
    }, 500);
    //

    this.callParent(arguments);
  }
});
