Ext.define("Crm.modules.accounts.view.UserAccountsForm", {
  extend: "Core.form.FormWindow",

  titleTpl: D.t("Users: {name}"),
  requires: ["Desktop.core.widgets.GridField"],
  formLayout: "fit",

  formMargin: 0,

  width: 650,
  height: 380,

  syncSize: function() {},

  controllerCls: "Crm.modules.accounts.view.UserAccountsFormController",

  buildItems() {
    return {
      xtype: "panel",
      layout: "anchor",
      defaults: {
        anchor: "100%",
        xtype: "textfield",
        labelWidth: 150,
        margin: 5
      },
      items: [
        {
          name: "id",
          hidden: true
        },
        {
          name: "account_id",
          hidden: true
        },
        this.buildUsers(),
        this.buildMainRoleCombo(),
        this.buildAdditionalRoleCombo()
      ]
    };
  },
  buildUsers() {
    return {
      name: "user_id",
      labelWidth: 150,
      fieldLabel: D.t("Select Existing User"),
      xtype: "combo",
      editable: false,
      queryMode: "local",
      // Template for the dropdown menu.
      // Note the use of the "x-list-plain" and "x-boundlist-item" class,
      // this is required to make the items selectable.
      tpl: Ext.create(
        "Ext.XTemplate",
        '<ul class="x-list-plain"><tpl for=".">',
        '<li role="option" class="x-boundlist-item">{first_name}  {last_name}</li>',
        "</tpl></ul>"
      ),
      // template for the content inside text field
      displayTpl: Ext.create(
        "Ext.XTemplate",
        '<tpl for=".">',
        "{first_name}  {last_name}",
        "</tpl>"
      ),
      flex: 1,
      valueField: "id",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.accounts.model.UsersModel"),
        fieldSet: ["id", "first_name", "last_name"],
        scope: this
      })
    };
  },
  buildMainRoleCombo() {
    return {
      name: "main_role",
      labelWidth: 150,
      fieldLabel: D.t("Main Role"),
      xtype: "combo",
      editable: true,
      forceSelection: true,
      queryMode: "local",
      displayField: "role_name",
      flex: 1,
      valueField: "id",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.roles.model.RolesModel"),
        fieldSet: ["id", "role_name"],
        scope: this
      })
    };
  },
  buildAdditionalRoleCombo() {
    return {
      name: "additional_role",
      labelWidth: 150,
      fieldLabel: D.t("Additional Roles"),
      xtype: "tagfield",
      queryMode: "local",
      displayField: "role_name",
      flex: 1,
      valueField: "id",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.roles.model.RolesModel"),
        fieldSet: ["id", "role_name"],
        scope: this
      })
    };
  }
});
