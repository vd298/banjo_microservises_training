Ext.define("Crm.modules.accounts.view.MerchantBankAccountsGridController", {
  extend: "Crm.modules.accountsPool.view.AssignedAccountsGridController",

  setControls() {
    let me = this;
    this.control({
      "[action=merchBankAccAddEdit]": {
        click: function(el) {
          me.addUpdateMerchBankAcc();
        }
      },
      "[action=refresh]": {
        click: function(el) {
          me.reloadData();
        }
      },
      "[action=import]": {
        click: function(el) {
          me.importData();
        }
      },
      "[action=export]": {
        click: function(el) {
          me.exportData();
        }
      },
      grid: {
        cellkeydown: function(cell, td, i, rec, tr, rowIndex, e, eOpts) {
          me.addUpdateMerchBankAcc(rec.data);
        },
        celldblclick: function(cell, td, i, rec) {},
        itemcontextmenu: function(vw, record, item, index, e, options) {
          e.stopEvent();
        }
      }
    });
    this.view.on("activate", function(grid, indx) {
      if (!me.view.observeObject)
        document.title = me.view.title + " " + D.t("ConsoleTitle");
    });
    this.view.on("edit", function(grid, indx) {});
    this.view.on("delete", function(grid, indx) {
      me.deleteRecord(grid.getStore(), indx);
    });
    this.initButtonsByPermissions();

    this.view.on("unassignAccount", function(grid, data) {
      me.unassignAccount(data);
    });

    //yc274 temporary fix for dialog
    setTimeout(() => {
      me.reloadData();
    }, 500);
    //
  },

  addUpdateMerchBankAcc: function(request) {
    var me = this,
      addmerchBankAccWin,
      message = "",
      selectedmerchName = "",
      status = "";
    var addBtn = me.view.down("[action=merchBankAccAddEdit]");
    if (addBtn) {
      addBtn.disable();
    }
    function validatorTxt(v) {
      return v && v.toString().trim() != "" ? true : " Field should be valid.";
    }
    addmerchBankAccWin = Ext.create("Ext.window.Window", {
      width: "50%",

      name: "merchBankAccWin",

      title: "Allocate Bank Account",

      iconCls: "x-fa fa-files-o",

      modal: true,

      items: [
        {
          xtype: "form",
          layout: "anchor",
          name: "notes_form",
          padding: 10,
          defaults: { xtype: "textfield", anchor: "100%", labelWidth: 100 },
          items: [
            {
              name: "id",
              hidden: true,
              allowBlank: true,
              maxLength: 36,
              value: request && request.id ? request.id : ""
            },
            {
              hidden: true,
              editable: false,
              name: "status",
              value: request && request.status ? request.status : ""
            },
            {
              name: "message",
              xtype: "label",
              text: `${message} ${selectedmerchName}`,
              hidden: true,
              id: "login_error"
            },
            {
              name: "bank_id",
              flex: 1,
              xtype: "combo",
              labelWidth: 150,
              allowBlank: false,
              forceSelection: true,
              fieldLabel: D.t("Select Bank"),
              value: request && request.bank_id ? request.bank_id : "",
              valueField: "id",
              displayField: "name",
              store: Ext.create("Core.data.ComboStore", {
                dataModel: Ext.create("Crm.modules.banks.model.BanksModel"),
                exProxyParams: {
                  filters: [
                    {
                      _property: "bank_type",
                      _value: 0,
                      _operator: "eq"
                    }
                  ]
                },
                fieldSet: ["id", "name"],
                scope: this
              }),
              listeners: {
                change: (el, v) => {
                  addmerchBankAccWin
                    .down("[name=bank_account_id]")
                    .clearValue();
                  addmerchBankAccWin
                    .down("[name=toggleStatus]")
                    .setHidden(true);
                  const proxyFilters = {
                    filters: [
                      { _property: "bank_id", _value: v, _operator: "eq" },
                      {
                        _property: "currency",
                        _value: addmerchBankAccWin.down(
                          "[name=merchant_account_id]"
                        ).valueCollection.items[0].data.currency,
                        _operator: "eq"
                      }
                    ]
                  };

                  const store = Ext.create("Core.data.ComboStore", {
                    dataModel: Ext.create(
                      "Crm.modules.accountsPool.model.AccountsPoolModel"
                    ),
                    fieldSet: [
                      "id",
                      "account_name",
                      "acc_no",
                      "currency",
                      "status"
                    ],
                    exProxyParams: proxyFilters,
                    scope: this
                  });

                  addmerchBankAccWin
                    .down("[name=bank_account_id]")
                    .setStore(store);
                }
              }
            },
            {
              xtype: "fieldcontainer",
              layout: "hbox",
              align: "center",
              defaults: {
                flex: 1
              },
              style: {
                "align-item": "center",
                "justify-content": "center"
              },
              items: [
                {
                  flex: 3,
                  xtype: "combo",
                  name: "bank_account_id",
                  labelWidth: 150,
                  typeAhead: true,
                  fieldLabel: D.t("Select Bank Account"),
                  valueField: "id",
                  forceSelection: true,
                  allowBlank: false,
                  value:
                    request && request.bank_account_id
                      ? request.bank_account_id
                      : "",
                  displayField: "account_name",
                  tpl:
                    '<tpl for="."><div class="x-boundlist-item" style="height:40px;"><b>{account_name}</b>&nbsp;&nbsp;(A/C: {acc_no})&nbsp;(Status: {status})</div><div style="clear:both"></div></tpl>',
                  listeners: {
                    change: async (el, v) => {
                      status = "";
                      this.model.runOnServer(
                        "fetchAssignedAccountsById",
                        { id: v },
                        function(res) {
                          if (res && res.length) {
                            status = res[0].status;
                            if (v) {
                              addmerchBankAccWin
                                .down("[name=toggleStatus]")
                                .setText(
                                  res[0].status === "ACTIVE"
                                    ? "Update Status: Inactive"
                                    : "Update Status: Active"
                                )
                                .setHidden(false);
                            } else {
                              addmerchBankAccWin
                                .down("[name=toggleStatus]")
                                .setHidden(true);
                            }
                            message = __CONFIG__.setAssignedAccountMessage(
                              el,
                              addmerchBankAccWin,
                              res
                            );
                          } else {
                            message = "";
                            selectedmerchName = "";
                            addmerchBankAccWin
                              .down("[name=message]")
                              .setHidden(true)
                              .setText("");
                          }
                          addmerchBankAccWin
                            .down("[name=notes_form]")
                            .isValid();
                        }
                      );
                      el.validate();
                    },
                    select: async (el, v) => {
                      let formValid = addmerchBankAccWin
                        .down("[name=notes_form]")
                        .isValid();
                      if (formValid) {
                        addmerchBankAccWin.down("[name=saveBtn]").enable();
                      }
                    }
                  }
                },
                {
                  xtype: "button",
                  name: "toggleStatus",
                  action: "toggleBankStatus",
                  text: "",
                  hidden: true,
                  style: {
                    "margin-left": "10px"
                  },
                  listeners: {
                    click: function(el, v) {
                      const formData = addmerchBankAccWin
                        .down("[name=notes_form]")
                        .getValues();
                      me.model.runOnServer(
                        "toggleBankStatusById",
                        {
                          id: formData.bank_account_id,
                          status: status === "ACTIVE" ? "INACTIVE" : "ACTIVE"
                        },
                        function(res) {
                          status = __CONFIG__.toggleBankStatusById(
                            el,
                            addmerchBankAccWin,
                            res
                          );
                          addmerchBankAccWin
                            .down("[name=notes_form]")
                            .isValid();
                        }
                      );
                    }
                  }
                }
              ]
            },
            {
              flex: 1,
              xtype: "combo",
              name: "account",
              labelWidth: 150,
              allowBlank: false,
              fieldLabel: D.t("Select Merchant"),
              hidden: true,
              value: me.view.up("form").down("[name=account_id]").value,
              store: Ext.create("Core.data.ComboStore", {
                dataModel: Ext.create(
                  "Crm.modules.accounts.model.AccountsModel"
                ),
                fieldSet: ["id", "name"],
                scope: this
              }),
              valueField: "id",
              displayField: "name",
              listeners: {
                change: (el, v) => {
                  addmerchBankAccWin
                    .down("[name=merchant_account_id]")
                    .clearValue();
                  const proxyFilters = {
                    filters: [
                      { _property: "account_id", _value: v, _operator: "eq" }
                    ]
                  };
                  const store = Ext.create("Core.data.ComboStore", {
                    dataModel: Ext.create(
                      "Crm.modules.accounts.model.MerchantAccountsModel"
                    ),
                    fieldSet: ["id", "name", "currency"],
                    exProxyParams: proxyFilters,
                    scope: this
                  });
                  addmerchBankAccWin
                    .down("[name=merchant_account_id]")
                    .setStore(store);

                  selectedmerchName = ` do you want to share with ${addmerchBankAccWin
                    .down("[name=notes_form]")
                    .down("[name=account]")
                    .getDisplayValue()}?`;
                  addmerchBankAccWin
                    .down("[name=message]")
                    .setText(`${message} ${selectedmerchName}`);
                }
              }
            },
            {
              flex: 1,
              xtype: "combo",
              labelWidth: 150,
              name: "merchant_account_id",
              hidden: true,
              fieldLabel: D.t("Select Merchant Account"),
              valueField: "id",
              allowBlank: false,
              displayField: "name",
              store: Ext.create("Core.data.ComboStore", {
                dataModel: Ext.create(
                  "Crm.modules.accounts.model.MerchantAccountsModel"
                ),
                fieldSet: ["id", "name", "currency"],
                scope: this
              }),
              value: me.view.up("form").down("[name=id]").value,
              listeners: {
                change: function(combo) {
                  const currentSelected = addmerchBankAccWin.down(
                    "[name=bank_id]"
                  ).value;
                  addmerchBankAccWin.down("[name=bank_id]").setValue("");
                  addmerchBankAccWin
                    .down("[name=bank_id]")
                    .setValue(currentSelected);
                  addmerchBankAccWin
                    .down("[name=bank_account_id]")
                    .clearValue();
                }
              }
            },
            {
              xtype: "fieldcontainer",
              anchor: "100%",
              layout: "hbox",
              align: "center",
              margin: { top: 15 },
              items: [
                {
                  xtype: "button",
                  text: D.t("Save"),
                  name: "saveBtn",
                  iconCls: "x-fa fa-check-square-o",
                  formBind: true,
                  listeners: {
                    click: async function(e, v) {
                      var actionBtn = e;
                      if (actionBtn) {
                        actionBtn.disable();
                      }
                      function showErrorMessage(err) {
                        var me = this;
                        addmerchBankAccWin
                          .down("[name=bank_account_id]")
                          .clearValue();

                        if (!err || (err && !Array.isArray(err))) {
                          D.a(
                            "Failure",
                            "Database Failure,Please try after some time."
                          );
                        } else {
                          err.forEach(function(item) {
                            var el = addmerchBankAccWin.down(
                              "[name=" + item.field + "]"
                            );
                            if (el && !!el.setActiveError) {
                              el.setActiveError(item.message);
                            }
                          });
                        }
                      }
                      var reqData = this.up("form").getValues() || {};
                      let mes = addmerchBankAccWin.down("[name=message]").text;

                      const isAssigned = await __CONFIG__.checkAlreadyAssigned(
                        me,
                        mes
                      );
                      if (!isAssigned) {
                        actionBtn.enable();
                        return;
                      }
                      me.model.runOnServer("assignAccount", reqData, function(
                        res
                      ) {
                        if (res.result) {
                          addmerchBankAccWin.close();
                        } else {
                          addmerchBankAccWin
                            .down("[name=bank_account_id]")
                            .clearValue();
                          actionBtn.enable();
                          D.a(res.error.title, res.error.message);
                        }
                      });
                    }
                  }
                },
                {
                  xtype: "button",
                  margin: { left: 5 },
                  text: D.t("Close"),
                  iconCls: "x-fa fa-ban",
                  listeners: {
                    click: function() {
                      addmerchBankAccWin.close();
                    }
                  }
                }
              ]
            }
          ]
        }
      ],
      listeners: {
        close: function() {
          if (addBtn) {
            addBtn.enable();
          }
          if (me && me.view && me.view.store) {
            me.view.store.reload();
          }
        }
      }
    }).show();
  },

  unassignAccount: function(data) {
    var me = this;
    Ext.Msg.show({
      title: "Unassign bank account?",
      message: `You are unassigning a bank account from ${data.merchant_name} >> ${data.merchant_account_name}. Would you like to proceed?`,
      buttons: Ext.Msg.YESNO,
      icon: Ext.Msg.QUESTION,
      fn: function(btn) {
        if (btn === "yes") {
          me.model.runOnServer(
            "unassignAccount",
            { mapping_id: data.mapping_id },
            function(res) {
              if (res.result) {
                me.view.store.reload();
              } else {
                D.a(res.error.title, res.error.message);
              }
            }
          );
        } else if (btn === "no") {
          return;
        }
      }
    });
  }
});
