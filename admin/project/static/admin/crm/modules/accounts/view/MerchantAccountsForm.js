Ext.define("Crm.modules.accounts.view.MerchantAccountsForm", {
  extend: "Core.form.FormWindow",

  titleTpl: D.t("Merchant Account"),
  requires: ["Desktop.core.widgets.GridField"],
  formLayout: "fit",
  closeHash: "Crm.modules.accounts.view.MerchantAccountsGrid",
  onClose: function(me) {
    window.history.back();
  },
  formMargin: 0,

  width: "50vw",
  maxHeight: 650,
  height: "65vh",
  model: "Crm.modules.accounts.model.MerchantAccountsModel",

  syncSize: function() {},

  controllerCls: "Crm.modules.accounts.view.MerchantAccountsFormController",

  buildItems() {
    var me = this;
    return {
      xtype: "tabpanel",
      layout: "fit",
      id: "MerchantAccountsFormTabPanel",
      items: [
        this.buildDetailsPanel(),
        this.buildAccountsPanel(),
        this.buildTransfersGrid(me)
      ],
      autoEl: { "data-test": "tab-items" }
    };
  },

  buildDetailsPanel() {
    const me = this;
    return {
      scrollable: true,
      xtype: "panel",
      layout: "anchor",
      title: D.t("Details"),
      defaults: {
        anchor: "100%",
        xtype: "textfield",
        labelWidth: 150,
        margin: 5
      },
      items: [
        {
          name: "id",
          hidden: true
        },
        {
          name: "account_id",
          hidden: true
        },
        {
          name: "acc_no",
          readOnly: true,
          fieldLabel: D.t("Account Number"),
          emptyText: D.t("Auto Generated")
        },
        {
          name: "name",
          fieldLabel: D.t("Name"),
          maxLength: 50,
          allowBlank: false
        },
        this.buildCurrencyCombo(),
        this.buildSettlementCurrencyCombo(),

        this.buildBasicForm(me)
      ]
    };
  },
  buildBasicForm(scope) {
    let me = scope;
    return {
      xtype: "panel",
      layout: "anchor",
      title: D.t("Configuration"),

      defaults: {
        anchor: "100%",
        xtype: "textfield",
        labelWidth: 150
      },
      items: [
        this.buildStatusCombo(),
        this.buildCategoryCombo(),
        {
          name: "expiry_hours",
          fieldLabel: D.t("Maximum Collection Tx Lifetime (Hours)"),
          xtype: "numberfield",
          allowBlank: false,
          minValue: 1
        },
        {
          flex: 1,
          xtype: "displayfield",
          name: "expiry_hours_display",
          labelSeparator: "",
          fieldLabel: " ",
          allowBlank: true,
          readOnly: true,
          value: `If the specified number of hours are crossed then all open Collection Transfer will be marked rejected.`
        },
        {
          name: "payout_expiry_hours",
          fieldLabel: D.t("Maximum Payout Tx Lifetime (Hours)"),
          xtype: "numberfield",
          allowBlank: false,
          minValue: 1
        },
        {
          flex: 1,
          xtype: "displayfield",
          name: "payout_expiry_hours_display",
          labelSeparator: "",
          fieldLabel: " ",
          allowBlank: true,
          readOnly: true,
          value: `If the specified number of hours are crossed then all open Payout Transfer will be marked rejected.`
        },
        {
          xtype: "checkbox",
          name: "auto_payouts",
          fieldLabel: D.t("Enable API payouts"),
          listeners: {
            change: function(e, newVal, oldVal, eOpts) {
              if (newVal) {
                me.down("[name='api_credentials']").setHidden(false);
              } else {
                me.down("[name='api_credentials']").setHidden(true);
              }
            }
          }
        },
        this.buildAPICredentialsCombo(),
        this.buildPaymentPageThemesCombo()
      ]
    };
  },
  buildCurrencyCombo() {
    return {
      name: "currency",
      fieldLabel: D.t("Currency"),
      xtype: "combo",
      editable: false,
      allowBlank: false,
      validateBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "all",
      displayField: "currencyDisplay",
      flex: 1,
      valueField: "abbr",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.bankAccounts.model.currencyCombo"),
        fieldSet: ["name", "abbr"],
        scope: this
      }),
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      }
    };
  },

  buildSettlementCurrencyCombo() {
    return {
      name: "settlement_currency",
      fieldLabel: D.t("Settlement Currency"),
      xtype: "combo",
      editable: false,
      allowBlank: false,
      validateBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "all",
      displayField: "currencyDisplay",
      flex: 1,
      valueField: "abbr",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.bankAccounts.model.currencyCombo"),
        fieldSet: ["name", "abbr"],
        scope: this
      }),
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      }
    };
  },

  buildStatusCombo() {
    this.statusTypesStore = Ext.create("Ext.data.Store", {
      fields: ["key", "name"],
      data: []
    });
    return {
      name: "status",
      labelWidth: 150,
      fieldLabel: D.t("Status"),
      xtype: "combo",
      queryMode: "local",
      forceSelection: true,
      triggerAction: "all",
      editable: false,
      allowBlank: false,
      valueField: "key",
      displayField: "name",
      store: this.statusTypesStore,
      operator: "eq",
      forceSelection: true,
      validateBlank: true,
      validateOnChange: true,
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      }
    };
  },
  buildCategoryCombo() {
    return {
      name: "category",
      xtype: "combo",
      fieldLabel: D.t("Category"),
      queryMode: "all",
      displayField: "name",
      editable: true,
      valueField: "id",
      validator: true,
      allowBlank: false,
      minChars: 1,
      store: Ext.create("Core.data.ComboStore", {
        storeId: "category",
        dataModel: Ext.create("Crm.modules.categories.model.CategoryModel"),
        fieldSet: ["name", "id"],
        scope: this
      })
    };
  },
  buildAPICredentialsCombo() {
    return {
      name: "api_credentials",
      xtype: "combo",
      fieldLabel: D.t("API Credentials to use"),
      queryMode: "all",
      displayField: "name",
      editable: true,
      valueField: "id",
      hidden: true,
      validator: true,
      minChars: 1,
      store: Ext.create("Core.data.ComboStore", {
        storeId: "category",
        dataModel: Ext.create(
          "Crm.modules.apicredentials.model.ApiCredentialsModel"
        ),
        fieldSet: ["name", "id", "secret_key"],
        scope: this
      })
    };
  },
  buildAccountsPanel() {
    return {
      xtype: "panel",
      id: "bank_accounts_tab",
      layout: "fit",
      title: D.t("Bank Accounts"),
      items: Ext.create(
        "Crm.modules.accounts.view.MerchantAccountAssignedAccountsGrid",
        {
          scope: this,
          observe: [
            { property: "merchant_id", param: "account_id" },
            { property: "merchant_account_id", param: "id" }
          ],
          controllerCls:
            "Crm.modules.accounts.view.MerchantBankAccountsGridController",
          buildTbar() {
            return [
              {
                text: this.buttonAddText,
                tooltip: this.buttonAddTooltip,
                iconCls: "x-fa fa-plus",
                scale: "medium",
                action: "merchBankAccAddEdit"
              },
              {
                tooltip: "Reload list",
                iconCls: "x-fa fa-refresh",
                action: "refresh"
              }
            ];
          }
        }
      )
    };
  },
  buildTransfersGrid(scope) {
    return {
      xtype: "panel",
      layout: "fit",
      title: D.t("Transfers"),
      items: Ext.create(
        "Crm.modules.accounts.view.MerchantAccountTransfersGrid",
        {
          scope: scope,
          observe: [{ property: "merchant_account_id", param: "id" }]
        }
      )
    };
  },
  buildButtons: function() {
    var btns = [
      "->",
      { text: D.t("Save"), iconCls: "x-fa fa-check", action: "apply" },
      "-",
      { text: D.t("Close"), iconCls: "x-fa fa-ban", action: "formclose" }
    ];
    return btns;
  },
  buildPaymentPageThemesCombo() {
    return {
      name: "payment_page_theme",
      xtype: "combo",
      fieldLabel: D.t("Payment page theme"),
      queryMode: "all",
      displayField: "name",
      editable: true,
      valueField: "value",
      validator: true,
      minChars: 1,
      store: {
        fields: ["name"],
        data: [
          { name: "Payment Theme v1 (Full Page)", value: "v1" },
          { name: "Payment Theme v2(Minimized, Wizard)", value: "v2" }
        ]
      }
    };
  }
});
