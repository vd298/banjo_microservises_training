Ext.define("Crm.modules.accounts.view.UserAccountInvitationsGrid", {
  extend: "Core.grid.GridContainer",
  controllerCls:
    "Crm.modules.accounts.view.UserAccountInvitationsGridController",

  title: D.t("Invited Users"),

  filterable: true,
  filterbar: true,

  buildColumns: function() {
    const me = this;
    return [
      {
        dataIndex: "id",
        hidden: true
      },
      {
        text: D.t("Email"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "email"
      },
      {
        dataIndex: "token",
        hidden: true
      },
      {
        dataIndex: "account_id",
        hidden: true
      },
      {
        dataIndex: "merchant_account_id",
        hidden: true
      },
      {
        dataIndex: "role",
        hidden: true
      },
      {
        dataIndex: "user_id",
        hidden: true
      },
      {
        dataIndex: "token",
        hidden: true
      },
      {
        dataIndex: "ips",
        hidden: true
      },
      {
        dataIndex: "webhook_url",
        hidden: true
      },
      {
        dataIndex: "user_type",
        hidden: true
      },
      {
        dataIndex: "telegram_id",
        text: D.t("Telegram ID"),
        flex: 1,
        sortable: true,
        filter: true
      },
      {
        dataIndex: "status",
        text: D.t("status"),
        flex: 1,
        sortable: true,
        filter: true,
        hidden: true
      },
      {
        text: D.t("Role"),
        flex: 1,
        sortable: true,
        filter: {
          xtype: "dependedcombo",
          editable: true,
          fieldSet: "id,role_name",
          valueField: "id",
          displayField: "role_name",
          parentEl: null,
          parentField: null,
          dataModel: "Crm.modules.roles.model.RolesModel"
        },
        renderer: function(v, m, r) {
          if (v) {
            return r.data.role_name;
          }
          return v;
        },
        dataIndex: "role"
      },
      {
        text: D.t("Accepted"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "accepted",
        renderer: function(v, m, r) {
          return v && v === true ? "Yes" : "No";
        }
      }
    ];
  },

  buildTbar() {
    let items = this.callParent(arguments);
    return items;
  },

  buildButtonsColumns: function() {
    var me = this;
    return [
      {
        xtype: "actioncolumn",
        width: 90,
        menuDisabled: true,
        items: [
          {
            iconCls: "x-fa fa-pencil-square-o",
            tooltip: this.buttonEditTooltip,
            isDisabled: function() {
              return !me.permis.modify && !me.permis.read;
            },
            handler: function(grid, rowIndex) {
              me.fireEvent("edit", grid, rowIndex);
            }
          },

          {
            iconCls: "x-fa fa-repeat",
            tooltip: "Resend invitation",
            isDisabled: function(g, index) {
              if (
                (!!g.grid.store.data.items[index].data.user_type &&
                  g.grid.store.data.items[index].data.user_type === "API") ||
                g.grid.store.data.items[index].data.accepted
              ) {
                return true;
              } else {
                return !g.grid.store.data.items[index].data.token;
              }
            },
            handler: function(grid, rowIndex) {
              me.fireEvent("resendInvitation", grid, rowIndex);
            }
          },
          {
            iconCls: "x-fa fa-ban",
            tooltip: "Cancel invitation",
            isDisabled: function(g, index) {
              if (
                !!g.grid.store.data.items[index].data.user_type &&
                g.grid.store.data.items[index].data.user_type === "API"
              ) {
                return true;
              } else {
                return (
                  !me.permis.del || !g.grid.store.data.items[index].data.token
                );
              }
            },
            handler: function(grid, rowIndex) {
              me.fireEvent("cancelInvitation", grid, rowIndex);
            }
          }
        ]
      }
    ];
  }
});
