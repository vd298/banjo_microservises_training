Ext.define("Crm.modules.accounts.view.BeneficiariesGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Beneficiaries"),

  filterable: true,
  filterbar: true,

  // controllerCls: "Crm.modules.accounts.view.AccountsGridController",

  buildColumns: function() {
    this.statusTypesStore = Ext.create("Ext.data.Store", {
      fields: ["key", "name"],
      data: []
    });
    return [
      {
        dataIndex: "id",
        hidden: true
      },
      {
        dataIndex: "account_id",
        hidden: true
      },
      {
        text: D.t("Merchant name"),
        flex: 1,
        width: 80,
        sortable: true,
        renderer: function(v, m, r) {
          if (v) {
            return (
              '<a href="#Crm.modules.accounts.view.MerchantAccountsForm~' +
              v +
              '">' +
              r.data.merchant_account_name +
              "</a> "
            );
          }
          return v;
        },
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "id",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create(
              "Crm.modules.accounts.model.MerchantAccountsModel"
            ),
            fieldSet: ["id", "name"],
            scope: this
          }),
          operator: "eq"
        },
        dataIndex: "merchant_account_id"
      },

      {
        text: D.t("Protocols"),
        flex: 1,
        sortable: true,
        filter: true,
        hidden: true,
        dataIndex: "protocols"
      },
      {
        text: D.t("First name"),
        flex: 1,
        width: 80,
        sortable: true,
        filter: true,
        dataIndex: "first_name"
      },
      {
        text: D.t("Last name"),
        flex: 1,
        width: 80,
        sortable: true,
        filter: true,
        dataIndex: "last_name"
      },
      {
        dataIndex: "country_code",
        hidden: true
      }
    ];
  }
});
