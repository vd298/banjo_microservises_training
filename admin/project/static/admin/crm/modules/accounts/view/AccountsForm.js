Ext.define("Crm.modules.accounts.view.AccountsForm", {
  extend: "Core.form.FormContainer",

  titleTpl: D.t("Edit Merchant info"),
  iconCls: "x-fa fa-user",
  requires: [
    "Desktop.core.widgets.GridField",
    "Core.form.DateField",
    "Core.form.DependedCombo",
    "Ext.form.field.Tag"
  ],
  formLayout: "fit",
  width: "75vw",
  height: "90vh",
  manageHeight: true,
  scrollable: true,

  syncSize: function() {},

  formMargin: 0,
  name: "merchantAccountEditForm",

  model: "Crm.modules.accounts.model.AccountsModel",

  controllerCls: "Crm.modules.accounts.view.AccountsFormController",

  buildItems() {
    var me = this;
    return {
      xtype: "tabpanel",
      layout: "fit",
      activeTab: 0,
      items: [
        this.buildBasicForm(me),
        this.buildPlansForm(me),
        this.buildMerchantAccountsGrid(me),
        this.buildUserInvitationGrid(me),
        this.buildTransfersGrid(me),
        this.buildBeneficiariesGrid(me)
      ],
      autoEl: { "data-test": "tab-items" }
    };
  },

  buildBasicForm(scope) {
    let me = scope;
    return {
      xtype: "panel",
      layout: "anchor",
      title: D.t("Basic Info"),
      activeTab: 0,
      defaults: {
        anchor: "100%",
        xtype: "textfield",
        labelWidth: 150,
        margin: 5
      },
      items: [
        {
          name: "id",
          hidden: true
        },
        {
          name: "name",
          fieldLabel: D.t("Name"),
          maxLength: 50,
          allowBlank: false
        },
        {
          name: "address",
          fieldLabel: D.t("Address"),
          maxLength: 150,
          allowBlank: false
        },
        {
          name: "city",
          fieldLabel: D.t("City"),
          maxLength: 50,
          allowBlank: false
        },
        __CONFIG__.buildCountryCombo({ forceSelection: false }),
        {
          xtype: "checkbox",
          name: "legal_entity",
          fieldLabel: D.t("Legal Entity")
        },
        {
          name: "registration_no",
          fieldLabel: D.t("Registration Number"),
          maxLength: 30,
          disabled: true,
          allowBlank: false
        },
        __CONFIG__.buildTypeCombo({ forceSelection: false }),
        __CONFIG__.buildStatusCombo({ forceSelection: false }),
        __CONFIG__.buildRealmsCombo({ forceSelection: false }),
        this.buildCategoriesGrid(me)
      ]
    };
  },

  buildCountriesCombo() {
    return {
      xtype: "dependedcombo",
      name: "country",
      labelWidth: 150,
      fieldLabel: D.t("Country"),
      editable: true,
      fieldSet: "name,abbr2",
      valueField: "abbr2",
      displayField: "name",
      parentEl: null,
      parentField: null,
      dataModel: "Crm.modules.countries.model.countryCombo"
    };
  },

  buildTypeCombo() {
    this.typesStore = Ext.create("Ext.data.Store", {
      fields: ["key", "name"],
      data: []
    });
    return {
      name: "type",
      labelWidth: 150,
      fieldLabel: D.t("Type"),
      xtype: "combo",
      queryMode: "local",
      forceSelection: true,
      triggerAction: "all",
      editable: false,
      valueField: "key",
      displayField: "name",
      store: this.typesStore,
      operator: "eq",
      allowBlank: false
    };
  },

  buildStatusCombo() {
    this.statusTypesStore = Ext.create("Ext.data.Store", {
      fields: ["key", "name"],
      data: []
    });
    return {
      name: "status",
      labelWidth: 150,
      fieldLabel: D.t("Status"),
      xtype: "combo",
      queryMode: "local",
      forceSelection: true,
      triggerAction: "all",
      editable: false,
      valueField: "key",
      displayField: "name",
      store: this.statusTypesStore,
      operator: "eq",
      allowBlank: false
    };
  },

  buildRealmsCombo() {
    return {
      xtype: "dependedcombo",
      name: "realm_id",
      labelWidth: 150,
      fieldLabel: D.t("Realm"),
      editable: true,
      fieldSet: "id,name",
      valueField: "id",
      displayField: "name",
      parentEl: null,
      parentField: null,
      dataModel: "Crm.modules.realm.model.RealmModel"
    };
  },

  buildCategoriesGrid(scope) {
    return {
      xtype: "panel",
      layout: "fit",
      region: "center",
      items: Ext.create(
        "Crm.modules.merchantCategories.view.MerchantCategoriesGrid",
        {
          title: "Categories",
          iconCls: null,
          scope: scope,
          height: "30vh",
          observe: [{ property: "account_id", param: "id" }]
        }
      )
    };
  },

  buildPlansForm(scope) {
    return Ext.create("Crm.modules.tariffs.view.TariffSettingsPanel", {});
  },
  buildMerchantAccountsGrid(scope) {
    return {
      xtype: "panel",
      layout: "fit",
      title: D.t("Merchant Accounts"),
      name: "merchant_accounts_tab",
      items: Ext.create("Crm.modules.accounts.view.MerchantAccountsGrid", {
        scope: scope,
        observe: [{ property: "account_id", param: "id" }]
      })
    };
  },
  buildUserInvitationGrid(scope) {
    return {
      xtype: "panel",
      layout: "fit",
      title: D.t("Users"),
      items: Ext.create(
        "Crm.modules.accounts.view.UserAccountInvitationsGrid",
        {
          scope: scope,
          observe: [{ property: "account_id", param: "id" }]
        }
      )
    };
  },
  buildTransfersGrid(scope) {
    return {
      xtype: "panel",
      layout: "fit",
      title: D.t("Transfers"),
      items: Ext.create("Crm.modules.accounts.view.MerchantTransfersGrid", {
        scope: scope,
        observe: [{ property: "account_id", param: "id" }]
      })
    };
  },
  buildBeneficiariesGrid(scope) {
    return {
      xtype: "panel",
      layout: "fit",
      title: D.t("Beneficiaries"),
      name: "beneficiaries_tab",
      items: Ext.create("Crm.modules.accounts.view.BeneficiariesGrid", {
        scope: scope,
        observe: [{ property: "account_id", param: "id" }]
      })
    };
  }
});
