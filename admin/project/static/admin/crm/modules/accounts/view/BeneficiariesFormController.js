Ext.define("Crm.modules.accounts.view.BeneficiariesFormController", {
  extend: "Core.form.FormController",

  setControls() {
    const me = this;

    this.control({
      "[name=account_id]": {
        change: function(v) {
          me.setMerchantAccountsStore(me);
        }
      },
      "[name=acc_no]": {
        change: function(v) {
          const ctime = me.view.down("[name=ctime]").getValue();
          if (!ctime) {
            const acc_no = me.view.down("[name=acc_no]").getValue();
            const re_acc_no = me.view.down("[name=re_acc_no]").getValue();
            if (!re_acc_no) {
              me.view.down("[name=re_acc_no]").validator = function(value) {
                return "required";
              };
            } else if (re_acc_no === acc_no) {
              me.view.down("[name=re_acc_no]").validator = function(value) {
                return true;
              };
            } else {
              me.view.down("[name=re_acc_no]").validator = function(value) {
                return "Account number must be same";
              };
            }
            me.view.down("form").isValid();
          }
        }
      },
      "[name=re_acc_no]": {
        change: function(e, newVal, oldVal) {
          const ctime = me.view.down("[name=ctime]").getValue();
          if (!ctime) {
            const acc_no = me.view.down("[name=acc_no]").getValue();
            const re_acc_no = me.view.down("[name=re_acc_no]").getValue();
            if (re_acc_no === acc_no) {
              e.validator = function(value) {
                return true;
              };
            } else {
              e.validator = function(value) {
                return "Account number must be same";
              };
            }
            e.isValid();
          }
        }
      },
      "[name=protocols]": {
        change: function(e, newVal, oldVal) {
          const protocolName = [];
          const bank_name = me.view.down("[name=bank_name]");
          const acc_no = me.view.down("[name=acc_no]");
          const re_acc_no = me.view.down("[name=re_acc_no]");
          const ifsc = me.view.down("[name=ifsc]");
          console.log(ifsc.getValue());
          const network_type = me.view.down("[name=network_type]");
          const ctime = me.view.down("[name=ctime]").getValue();
          const crypto_wallet_address = me.view.down(
            "[name=crypto_wallet_address]"
          );
          const description = me.view.down("[name=description]");
          const vpa = me.view.down("[name=vpa]");
          if (e && e.lastSelection && e.lastSelection.length) {
            for (let i = 0; i < e.lastSelection.length; i++) {
              if (e.lastSelection[i].data && e.lastSelection[i].data.name) {
                protocolName.push(e.lastSelection[i].data.name);
              }
            }
          }

          if (protocolName.length && protocolName.indexOf("UPI") >= 0) {
            vpa.setConfig("hidden", false);
            vpa.setConfig("allowBlank", false);
          } else {
            vpa.setConfig("hidden", true);
            vpa.setConfig("allowBlank", true);
          }
          if (protocolName.length && protocolName.indexOf("USDT") >= 0) {
            description.setConfig("hidden", false);
            network_type.setConfig("hidden", false);
            network_type.setConfig("allowBlank", false);
            crypto_wallet_address.setConfig("hidden", false);
            crypto_wallet_address.setConfig("allowBlank", false);
          } else {
            description.setConfig("hidden", true);
            network_type.setConfig("hidden", true);
            network_type.setConfig("allowBlank", true);
            crypto_wallet_address.setConfig("hidden", true);
            crypto_wallet_address.setConfig("allowBlank", true);
          }
          if (
            protocolName.length &&
            (protocolName.indexOf("IMPS") >= 0 ||
              protocolName.indexOf("RTGS") >= 0 ||
              protocolName.indexOf("NEFT") >= 0)
          ) {
            bank_name.setConfig("hidden", false);
            bank_name.setConfig("allowBlank", false);
            acc_no.setConfig("hidden", false);
            acc_no.setConfig("allowBlank", false);

            !ctime && re_acc_no.setConfig("hidden", false);
            !ctime && re_acc_no.setConfig("allowBlank", false);
            ifsc.setConfig("hidden", false);
            ifsc.setConfig("allowBlank", false);
          } else {
            bank_name.setConfig("hidden", true);
            bank_name.setConfig("allowBlank", true);
            acc_no.setConfig("hidden", true);
            acc_no.setConfig("allowBlank", true);
            re_acc_no.setConfig("hidden", true);
            re_acc_no.setConfig("allowBlank", true);
            ifsc.setConfig("hidden", true);
            ifsc.setConfig("allowBlank", true);
          }
          me.view.down("form").isValid();
        }
      }
    });

    this.callParent(arguments);
  },

  setMerchantAccountsStore(me) {
    var form = me.view.down("form").getForm();
    if (form) {
      data = form.getValues();
      console.log("fired", data);
      const proxyFilters = {
        filters: [
          {
            _property: "account_id",
            _value: data.account_id,
            _operator: "eq"
          }
        ]
      };
      const store = Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create(
          "Crm.modules.accounts.model.MerchantAccountsModel"
        ),
        fieldSet: ["id", "name"],
        exProxyParams: proxyFilters,
        scope: me
      });
      me.view.down("[name=merchant_account_id]").setStore(store);
    }
  }
});
