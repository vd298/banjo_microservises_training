Ext.define("Crm.modules.accounts.view.AccountUsersFormController", {
  extend: "Core.form.FormController",

  setControls() {
    this.control({
      "[action=apply]": {
        click: () => {
          this.doClose = false;
        }
      },
      "[action=save]": {
        click: () => {}
      },
      "[action=formclose]": {
        click: () => {
          window.history.go(-1);
        }
      }
    });

    this.callParent(arguments);
  },
  async save(closewin, cb) {
    var me = this,
      form = me.view.down("form").getForm(),
      data = {};

    var sb1 = me.view.down("[action=save]"),
      sb2 = me.view.down("[action=apply]");

    if (sb1 && !!sb1.setDisabled) sb1.setDisabled(true);
    if (sb2 && !!sb2.setDisabled) sb2.setDisabled(true);

    if (form) {
      data = form.getValues();
    }

    var setButtonsStatus = function() {
      if (sb1 && !!sb1.setDisabled) sb1.setDisabled(false);
      if (sb2 && !!sb2.setDisabled) sb2.setDisabled(false);
    };

    if (me.view.fireEvent("beforesave", me.view, data) === false) {
      setButtonsStatus();
      return;
    }
    try {
      let res = await me.model.callApi(
        "account-service",
        "inviteUserFromAdmin",
        {
          email: data.email,
          role: data.role
        },
        {
          header: { lang: "en" },
          accountId: data.account_id
        }
      );
      if (res) {
        if (!!cb) cb(res);
        window.history.go(-2);
      } else {
        Ext.Msg.alert("Error", "Something went wrong", function(btn) {
          if (btn == "ok") {
            // console.log(btn);
          }
        });
      }
      if (closewin && !!me.view.close) {
        me.view.close();
        window.history.go(-2);
      }
    } catch (err) {
      console.log(err);
      Ext.Msg.alert(err.title, err.message, function(btn) {
        if (btn == "ok") {
          // console.log(btn);
          window.history.go(-2);
        }
      });
      if (closewin && !!me.view.close) {
        me.view.close();
      }
      if (!!cb) cb(data);
    }
  }
});
