Ext.define("Crm.modules.accounts.view.AccountsGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Merchants"),

  filterable: true,
  filterbar: true,

  controllerCls: "Crm.modules.accounts.view.AccountsGridController",

  buildColumns: function() {
    this.statusTypesStore = Ext.create("Ext.data.Store", {
      fields: ["key", "name"],
      data: []
    });
    return [
      {
        text: D.t("Merchant name"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "name"
      },
      {
        text: D.t("Address"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "address"
      },
      {
        flex: 1,
        text: D.t("City"),
        sortable: true,
        filter: true,
        dataIndex: "city"
      },
      {
        text: D.t("Country"),
        flex: 1,
        sortable: true,
        filter: {
          xtype: "dependedcombo",
          editable: true,
          fieldSet: "name,abbr2",
          valueField: "abbr2",
          displayField: "name",
          parentEl: null,
          parentField: null,
          dataModel: "Crm.modules.countries.model.countryCombo"
        },
        dataIndex: "country"
      },
      {
        text: D.t("Legal Entity"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "legal_entity",
        renderer: function(v, m, r) {
          return v ? "Yes" : "No";
        }
      },
      {
        text: D.t("Status"),
        flex: 1,
        sortable: true,
        dataIndex: "status",
        filter: __CONFIG__.buildStatusCombo({
          forceSelection: false,
          fieldLabel: null
        })
      }
    ];
  }
});
