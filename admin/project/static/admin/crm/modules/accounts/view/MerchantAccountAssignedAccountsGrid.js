Ext.define("Crm.modules.accounts.view.MerchantAccountAssignedAccountsGrid", {
  extend: "Crm.modules.accountsPool.view.AssignedAccountsGrid",
  buildButtonsColumns() {
    var me = this;
    return [
      {
        xtype: "actioncolumn",
        width: 54,
        items: [
          {
            tooltip: D.t("Release this bank account from merchant account"),
            text: D.t("Unassign Account"),
            iconCls: "x-fa fa-unlink",
            action: "unassignAccount",
            handler: function(grid, index) {
              me.fireEvent(
                "unassignAccount",
                grid,
                grid.store.getAt(index).data
              );
            }
          }
        ]
      }
    ];
  }
});
