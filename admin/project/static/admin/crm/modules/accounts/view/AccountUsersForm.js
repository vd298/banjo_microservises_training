Ext.define("Crm.modules.accounts.view.AccountUsersForm", {
  extend: "Core.form.FormWindow",

  titleTpl: D.t("Users: {name}"),
  requires: ["Desktop.core.widgets.GridField"],
  formLayout: "fit",

  formMargin: 0,

  width: 650,
  height: 380,

  syncSize: function() {},

  controllerCls: "Crm.modules.accounts.view.AccountUsersFormController",

  buildItems() {
    return {
      xtype: "tabpanel",
      layout: "fit",
      items: [this.buildGeneral(), this.buildInvitationList()],
      autoEl: { "data-test": "tab-items" }
    };
  },
  buildGeneral() {
    return {
      xtype: "panel",
      layout: "anchor",
      title: D.t("New Invite"),
      defaults: {
        anchor: "100%",
        xtype: "textfield",
        labelWidth: 150,
        margin: 5
      },
      items: [
        {
          name: "id",
          hidden: true
        },
        {
          name: "account_id",
          hidden: true
        },
        {
          name: "email",
          fieldLabel: D.t("Email")
        },
        this.buildMainRoleCombo()
      ]
    };
  },
  buildInvitationList() {
    this.userInvitationGrid = Ext.create(
      "Crm.modules.accounts.view.UserAccountsInvitationGrid",
      {
        title: null,
        iconCls: null,
        scope: this,
        observe: [{ property: "account_id", param: "account_id" }]
      }
    );
    return {
      xtype: "panel",
      layout: "fit",
      region: "center",
      cls: "grayTitlePanel",
      title: D.t("Invitation"),
      items: this.userInvitationGrid
    };
  },
  buildMainRoleCombo() {
    return {
      name: "role",
      labelWidth: 150,
      fieldLabel: D.t("Main Role"),
      xtype: "combo",
      editable: false,
      queryMode: "local",
      displayField: "role_name",
      flex: 1,
      valueField: "id",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.roles.model.RolesModel"),
        fieldSet: ["id", "role_name"],
        scope: this
      })
    };
  },

  buildButtons: function() {
    var btns = [
      "->",
      {
        text: D.t("Save"),
        iconCls: "x-fa fa-check-square-o",
        scale: "medium",
        action: "save",
        style: "background: #53b7f4; color: #fff;",
        cls: "white_button"
      },

      "-",
      { text: D.t("Close"), iconCls: "x-fa fa-ban", action: "formclose" }
    ];
    if (this.allowCopy)
      btns.splice(1, 0, {
        tooltip: D.t("Make a copy"),
        iconCls: "x-fa fa-copy",
        action: "copy"
      });
    return btns;
  }
});
