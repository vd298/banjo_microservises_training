Ext.define("Crm.modules.accounts.view.MerchantAccountsGridController", {
  extend: "Core.grid.GridController",

  init: function(view) {
    this.view = view;
    this.model = this.view.model;
    this.callParent(arguments);
  },

  setControls: function() {
    var me = this;
    this.control({
      "[action=refresh]": {
        click: function(el) {
          me.reloadData();
        }
      }
    });

    //yc274 temporary fix for dialog
    setTimeout(() => {
      me.reloadData();
    }, 500);
    //
    this.populateComboboxes();
    this.callParent(arguments);
  },

  populateComboboxes() {
    var me = this;
    this.model.populateComboBox(
      {
        query:
          "SELECT unnest(enum_range(NULL::enum_merchant_accounts_status)) as status_type;"
      },
      function(res) {
        if (!!res.length && res.length) {
          me.view.statusTypesStore.loadData(
            res.map((row) => {
              return { key: row.status_type, name: row.status_type };
            })
          );
        }
      }
    );

    this.model.populateComboBox(
      {
        query: "SELECT id, abbr from currency;"
      },
      function(res) {
        if (!!res.length && res.length) {
          me.view.currencyStore.loadData(
            res.map((row) => {
              return { key: row.id, name: row.abbr };
            })
          );

          me.view.settlementCurrencyStore.loadData(
            res.map((row) => {
              return { key: row.id, name: row.abbr };
            })
          );
        }
      }
    );
  }
});
