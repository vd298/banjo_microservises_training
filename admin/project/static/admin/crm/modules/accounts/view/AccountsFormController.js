Ext.define("Crm.modules.accounts.view.AccountsFormController", {
  extend: "Core.form.FormController",

  setControls() {
    var me = this;
    this.control({
      "[name=legal_entity]": {
        change: function(v) {
          if (v.value) {
            me.view
              .down("form")
              .down("[name=registration_no]")
              .enable();
          } else {
            me.view
              .down("form")
              .down("[name=registration_no]")
              .disable();
          }
        }
      },
      "[name=name]": {
        focusleave: function(v, e, o) {
          if (v.value) {
            me.model.checkMerchantName(
              {
                name: v.value,
                id: me.view.down("form").down("[name=id]").value
              },
              (res) => {
                if (!!res.length && res.length) {
                  v.setActiveError("Name already in use.");
                }
              }
            );
          }
        }
      }
    });

    this.view.on("activate", (grid, indx) => {
      me.view
        .down("form")
        .down("[xtype=tabpanel]")
        .setActiveTab(0);
    });
    this.callParent(arguments);
  },

  populateComboboxes() {
    var me = this;
    //Accounts Status
    this.model.populateComboBox(
      {
        query:
          "SELECT unnest(enum_range(NULL::enum_accounts_status)) as status_type;"
      },
      function(res) {
        if (!!res.length && res.length) {
          me.view.statusTypesStore.loadData(
            res.map((row) => {
              return {
                key: row.status_type,
                name: row.status_type
              };
            })
          );
        }
      }
    );
    // Accounts types
    this.model.populateComboBox(
      {
        query: "SELECT unnest(enum_range(NULL::enum_accounts_type)) as type;"
      },
      function(res) {
        if (!!res.length && res.length) {
          me.view.typesStore.loadData(
            res.map((row) => {
              return { key: row.type, name: row.type };
            })
          );
        }
      }
    );
  }
});
