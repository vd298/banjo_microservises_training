Ext.define("Crm.modules.accounts.view.AccountUsersGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Users"),

  filterable: true,
  filterbar: true,
  controllerCls: "Crm.modules.accounts.view.AccountUsersGridController",

  buildColumns: function() {
    return [
      {
        text: D.t("First name"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "first_name"
      },
      {
        text: D.t("Last name"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "last_name"
      },
      {
        text: D.t("Email"),
        width: 80,
        sortable: true,
        filter: true,
        dataIndex: "email"
      },
      {
        text: D.t("Phone"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "mobile"
      },
      {
        text: D.t("Username"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "username"
      },
      {
        text: D.t("Primary Role"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "primary_role"
      },
      {
        text: D.t("Secondary Roles"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "secondary_role",
        renderer: (v) => {
          return v ? v.join(",") : "";
        }
      },
      {
        text: D.t("Status"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "status"
      }
    ];
  }

  // buildTbar() {
  //   let items = this.callParent(arguments);
  //   items.splice(1, 0, {
  //     text: D.t("Link"),
  //     iconCls: "x-fa fa-link",
  //     action: "linkExistingAccount"
  //   });
  //   return items;
  // }
});
