Ext.define("Crm.modules.accounts.view.MerchantAccountsFormController", {
  extend: "Core.form.FormController",

  setControls: function() {
    var me = this;
    this.control({
      "[action=formclose]": {
        click: () => {
          this.closeView();
        }
      },
      "[action=apply]": {
        click: () => {
          this.save(false, (res) => {
            me.view.down("[name=currency]").disable();
            me.view.down("[name=settlement_currency]").disable();
          });
        }
      },
      "[action=save]": {
        click: () => {
          this.save(true);
        }
      },
      "[action=remove]": {
        click: () => {
          this.deleteRecord_do(true);
        }
      },
      "[action=copy]": {
        click: () => {
          this.copyRecord(true);
        }
      },
      "[action=gotolist]": {
        click: () => {
          this.gotoListView(true);
        }
      },
      "[action=exportjson]": {
        click: () => {
          this.exportJson();
        }
      },
      "[action=importjson]": {
        change: (el) => {
          this.importJson();
        }
      }
    });

    this.view.on("activate", (grid, indx) => {
      if (!this.oldDocTitle) this.oldDocTitle = document.title;
      var form = this.view.down("form").getForm();
      if (form) {
        data = form.getValues();
        this.setTitle(data);
      }
    });
    this.view.on("close", (grid, indx) => {
      if (this.oldDocTitle) document.title = this.oldDocTitle;
    });

    this.view.on("disablePanels", (data, cmp) => {
      if (data.maker == undefined) {
        var tabPanel = Ext.getCmp(cmp);
        var items = tabPanel.items.items;

        for (var i = 1; i < items.length; i++) {
          var panel = items[i];
          panel.setDisabled(true);
        }
      }
    });

    this.view.down("form").on({
      validitychange: (th, valid, eOpts) => {
        var el = this.view.down("[action=apply]");
        if (el) el.setDisabled(!valid);
        el = this.view.down("[action=save]");
        if (el) el.setDisabled(!valid);
      }
    });
    this.checkPermissions();
    this.hideBankAccountsTab(me);
    this.populateComboboxes();
  },
  populateComboboxes() {
    var me = this;
    this.model.populateComboBox(
      {
        query:
          "SELECT unnest(enum_range(NULL::enum_merchant_accounts_status)) as status_type;"
      },
      function(res) {
        if (!!res.length && res.length && Array.isArray(res)) {
          me.view.statusTypesStore.loadData(
            res.map((row) => {
              return { key: row.status_type, name: row.status_type };
            })
          );
        }
      }
    );
  },
  hideBankAccountsTab(me) {
    this.model.checkIfExists({ id: this.view.recordId }, (resp) => {
      if (!resp) {
        var panel = Ext.getCmp("bank_accounts_tab");
        panel.destroy();
      } else {
        me.view.down("[name=currency]").disable();
        me.view.down("[name=settlement_currency]").disable();
      }
    });
  },

  afterDataLoad(data, cb) {
    var me = this;
    if (me && data && data.ctime == undefined) {
      me.view.fireEvent("disablePanels", data, "MerchantAccountsFormTabPanel");
    }
    cb(data);
  }
});
