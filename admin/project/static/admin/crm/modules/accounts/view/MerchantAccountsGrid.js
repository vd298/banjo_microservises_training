Ext.define("Crm.modules.accounts.view.MerchantAccountsGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Merchants Accounts"),
  name: "MerchantAccountsGrid",

  filterable: true,
  filterbar: true,

  controllerCls: "Crm.modules.accounts.view.MerchantAccountsGridController",
  model: "Crm.modules.accounts.model.MerchantAccountsModel",

  buildColumns: function() {
    this.statusTypesStore = Ext.create("Ext.data.Store", {
      fields: ["key", "name"],
      data: []
    });

    this.currencyStore = Ext.create("Ext.data.Store", {
      fields: ["key", "name"],
      data: []
    });

    this.settlementCurrencyStore = Ext.create("Ext.data.Store", {
      fields: ["key", "name"],
      data: []
    });
    return [
      {
        dataIndex: "account_id",
        hidden: true
      },
      {
        dataIndex: "api_credential_id",
        hidden: true
      },
      {
        dataIndex: "category",
        hidden: true
      },
      {
        text: D.t("Name"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "name"
      },
      {
        text: D.t("Account Number"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "acc_no"
      },
      {
        text: D.t("Active Accounts"),
        dataIndex: "active_count",
        flex: 1,
        filter: true,
        sortable: true
      },
      {
        text: D.t("Inactive Accounts"),
        dataIndex: "inactive_count",
        flex: 1,
        filter: true,
        sortable: true
      },
      {
        text: D.t("Currency"),
        width: 80,
        flex: 1,
        sortable: true,
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: false,
          valueField: "name",
          displayField: "name",
          store: this.currencyStore,
          operator: "eq"
        },
        dataIndex: "currency"
      },
      {
        text: D.t("Settlement Currency"),
        width: 80,
        flex: 1,
        sortable: true,
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: false,
          valueField: "name",
          displayField: "name",
          store: this.settlementCurrencyStore,
          operator: "eq"
        },
        dataIndex: "settlement_currency"
      },
      {
        text: D.t("Category"),
        flex: 1,
        sortable: true,
        filter: {
          xtype: "dependedcombo",
          editable: true,
          fieldSet: "name,id",
          valueField: "name",
          displayField: "name",
          dataModel: "Crm.modules.categories.model.CategoryModel"
        },
        dataIndex: "category_name"
      },
      {
        text: D.t("Status"),
        flex: 1,
        sortable: true,
        dataIndex: "status",
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: false,
          valueField: "key",
          displayField: "name",
          store: this.statusTypesStore,
          operator: "eq"
        }
      }
    ];
  },
  buildButtonsColumns: function() {
    var me = this;
    return [
      {
        xtype: "actioncolumn",
        width: 90,
        menuDisabled: true,
        items: [
          {
            iconCls: "x-fa fa-pencil-square-o",
            tooltip: this.buttonEditTooltip,
            isDisabled: function() {
              return !me.permis.modify && !me.permis.read;
            },
            handler: function(grid, rowIndex) {
              me.fireEvent("edit", grid, rowIndex);
            }
          }
        ]
      }
    ];
  }
});
