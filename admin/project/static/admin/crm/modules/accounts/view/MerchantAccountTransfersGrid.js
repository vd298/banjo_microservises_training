Ext.define("Crm.modules.accounts.view.MerchantAccountTransfersGrid", {
  extend: "Crm.modules.Transfers.view.TransfersGrid",

  buildColumns: function() {
    var me = this;
    var parentObj = me.callParent(arguments);
    if (parentObj && parentObj.length > 0) {
      for (let i = 0; i < parentObj.length; i++) {
        if (
          parentObj[i] && typeof parentObj[i].dataIndex=='string' && 
          ["src_acc_name", "src_macc_name"].indexOf(parentObj[i].dataIndex) > -1
        ) {
          parentObj.splice(i, 1);
          i--;
        }
      }
    }
    return parentObj;
  }
});
