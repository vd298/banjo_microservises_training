Ext.define("Crm.modules.accounts.view.UserAccountInvitationsForm", {
  extend: "Core.form.FormWindow",

  titleTpl: D.t("Invite User"),
  requires: [
    "Desktop.core.widgets.GridField",
    "Core.form.DateField",
    "Core.form.DependedCombo",
    "Ext.form.field.Tag"
  ],
  noHash: true,
  onClose: function(me) {
    window.history.back();
  },
  formLayout: "fit",

  width: 650,
  height: 380,

  syncSize: function() {},

  formMargin: 0,
  model: "Crm.modules.accounts.model.UserAccountInvitationsModel",
  controllerCls:
    "Crm.modules.accounts.view.UserAccountInvitationsFormController",

  buildItems() {
    var me = this;
    return {
      xtype: "panel",
      layout: "anchor",
      name: "userAccountInvitationForm",
      defaults: {
        anchor: "100%",
        xtype: "textfield",
        labelWidth: 150,
        margin: 5
      },
      items: [
        {
          name: "account_id",
          hidden: true
        },
        {
          name: "id",
          hidden: true
        },
        {
          name: "token",
          hidden: true
        },
        {
          name: "accepted",
          hidden: true
        },
        this.buildUserTypesCombo(),
        {
          name: "email",
          hidden: true,
          fieldLabel: D.t("Email"),
          maxLength: 50,
          allowBlank: false,
          regex: /^(")?(?:[^\."])(?:(?:[\.])?(?:[\w\-!#$%&'*+\/=?\^_`{|}~]))*\1@(\w[\-\w]*\.){1,5}([A-Za-z]){2,6}$/,
          regexText: "Invalid email address",
          submitValue: true,
          readOnly: true
        },
        {
          flex: 1,
          xtype: "combo",
          name: "merchant_account_id",
          fieldLabel: D.t("Select Merchant Account"),
          valueField: "id",
          allowBlank: false,
          displayField: "name",
          hidden: true,
          readOnly: true,
          validateBlank: true,
          validateOnChange: true,
          queryMode: "all"
        },
        this.buildRolesCombo(),
        this.buildUserAccountsOwnersCombo(),
        {
          xtype: "tagfield",
          name: "ips",
          growMax: 40,
          valueField: "ips",
          displayField: "ips",
          emptyText: "Multiple Entries accepted, Hit enter to separate values",
          fieldLabel: D.t("IP Addresses"),
          expand: Ext.emptyFn,
          hideTrigger: true,
          queryMode: "local",
          store: Ext.create("Core.data.Store", {
            dataModel: "Crm.modules.accounts.model.UserAccountInvitationsModel",
            fieldSet: ["ips"],
            scope: me
          }),
          forceSelection: false,
          triggerOnClick: false,
          createNewOnEnter: true,
          hidden: true,
          allowBlank: true
        },
        {
          name: "webhook_url",
          hidden: true,
          allowBlank: false,
          fieldLabel: D.t("Webhook URL")
        },
        {
          xtype: "checkbox",
          name: "allow",
          hidden: true,
          disabled: true,
          labelWidth: 170,
          fieldLabel: D.t("Read Permission")
        },
        {
          name: "telegram_id",
          hidden: false,
          // readOnly: true,
          fieldLabel: D.t("Telegram ID")
        },
        {
          name: "status",
          hidden: false,
          fieldLabel: D.t("Merchant Account Link Status"),
          xtype: "dependedcombo",
          allowBlank: true,
          editable: true,
          valueField: "key",
          displayField: "key",
          modelConfig: { enum_name: "enum_user_accounts_status" },
          dataModel: "Crm.modules.Util.model.FetchComboRecords",
          operator: "eq"
        },
        {
          xtype: "checkbox",
          name: "deny",
          hidden: true,
          disabled: true,
          labelWidth: 170,
          fieldLabel: D.t("Delete Permission")
        },
        {
          xtype: "checkbox",
          name: "write",
          hidden: true,
          disabled: true,
          labelWidth: 170,
          fieldLabel: D.t("Write Permission")
        }
        // this.buildUserAccountStatusCombo()
      ]
    };
  },
  buildUserAccountStatusCombo() {
    var me = this;
    return {
      name: "status",
      labelWidth: 150,
      fieldLabel: D.t("User Account Status"),
      xtype: "dependedcombo",
      queryMode: "all",
      forceSelection: true,
      triggerAction: "all",
      editable: false,
      allowBlank: false,
      hidden: true,
      valueField: "key",
      displayField: "key",
      modelConfig: { enum_name: "enum_user_accounts_status" },
      dataModel: "Crm.modules.Util.model.FetchComboRecords",
      operator: "eq",
      listeners: {
        afterrender(e, eOpts) {
          __CONFIG__.setComponentEmptyText(e, eOpts);
        }
      }
    };
  },

  buildUserTypesCombo() {
    var me = this;
    this.userTypesStore = Ext.create("Ext.data.Store", {
      fields: ["key", "name"],
      storeId: "userTypesStore",
      data: []
    });
    return {
      name: "user_type",
      labelWidth: 150,
      fieldLabel: D.t("User Type"),
      xtype: "combo",
      queryMode: "local",
      forceSelection: true,
      triggerAction: "all",
      editable: false,
      valueField: "key",
      displayField: "name",
      store: this.userTypesStore,
      operator: "eq",
      allowBlank: false,
      readOnly: true
    };
  },

  buildRolesCombo() {
    return {
      name: "role",
      hidden: true,
      labelWidth: 150,
      fieldLabel: D.t("Main Role"),
      xtype: "combo",
      displayField: "role_name",
      flex: 1,
      valueField: "id",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.roles.model.RolesModel"),
        fieldSet: ["id", "role_name"],
        scope: this
      }),
      // readOnly: true,
      allowBlank: false,
      editable: true,
      forceSelection: true
    };
  },

  buildUserAccountsOwnersCombo() {
    var me = this;
    return {
      name: "owner_id",
      hidden: true,
      fieldLabel: D.t("Generated credentials to be mailed to Owner"),
      parentEl: null,
      parentField: null,
      xtype: "combo",
      editable: true,
      queryMode: "all",
      labelWidth: 150,
      forceSelection: true,
      minChars: 1,
      typeAhead: true,
      displayField: "email",
      allowBlank: false,
      flex: 1,
      valueField: "owner_id",
      tpl:
        '<tpl for=".">' +
        '<tpl if="first_name">' +
        '<div class="x-boundlist-item" style="height:40px;"><b>Name: {first_name} {last_name}</b>&nbsp;&nbsp;(Email: {email})</div><div style="clear:both"></div>' +
        "<tpl else>" +
        '<div class="x-boundlist-item" style="height:40px;"><b>Email: {email}</div><div style="clear:both"></div>' +
        "</tpl></tpl>",
      listeners: {
        select: async function(e, v) {
          let emailField = me.down("[name=email]");
          if (emailField && v && v.data && v.data.email) {
            emailField.setValue(v.data.email);
          }
        }
      }
    };
  },

  buildButtons: function() {
    var btns = [
      {
        text: D.t("Cancel invitation"),
        action: "cancelInvitation",
        name: "cancelInvitation"
      },

      "->",
      { text: D.t("Save"), iconCls: "x-fa fa-check", action: "apply" },
      "-",
      { text: D.t("Close"), iconCls: "x-fa fa-ban", action: "formclose" }
    ];
    return btns;
  }
});
