Ext.define("Crm.modules.accounts.view.UserAccountsFormController", {
  extend: "Core.form.FormController",

  setControls() {
    let me = this;
    this.control({
      "[action=apply]": {
        click: () => {
          this.doClose = false;
        }
      },
      "[action=save]": {
        click: () => {
          me.doClose = true;
          window.history.go(-1);
        }
      },
      "[action=formclose]": {
        click: () => {
          window.history.go(-1);
        }
      }
    });

    this.callParent(arguments);
  }
});
