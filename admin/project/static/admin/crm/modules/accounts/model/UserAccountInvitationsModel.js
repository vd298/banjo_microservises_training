Ext.define("Crm.modules.accounts.model.UserAccountInvitationsModel", {
  extend: "Crm.classes.DataModel",

  collection: "user_account_invitations",
  idField: "id",
  //removeAction: "remove",
  foreignKeyFilter: [
    {
      collection: "accounts",
      alias: "a",
      on: " user_account_invitations.account_id = accounts.id  ",
      type: "inner"
    }
  ],
  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "account_id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "token",
      type: "string",
      visible: true
    },
    {
      name: "account_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "user_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "email",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "role",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "accepted",
      type: "boolean",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "expiry",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "token",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "merchant_account_id",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  /* scope:server */
  async afterGetData(data, cb) {
    var me = this;

    let userIds = [];
    data.forEach((ele) => {
      if (ele && ele.user_id) userIds.push(ele.user_id);
    });

    var queryList = `('${userIds.join("', '")}')`;

    if (userIds.length) {
      await me.src.db.query(
        `select u.id, u.user_type, u.ips, u.webhook_url, u.telegram_id, ua.status from users u
          left join user_accounts ua on ua.removed = 0 and ua.user_id = u.id 
          where u.id in ${queryList};`,
        [],
        async function(err, resp) {
          if (err) {
            console.error(
              `UserAccountInvitationModel. Func:afterGetData. ${me.collection} table Database Error:`,
              err
            );
          } else if (resp.length) {
            let data_map = {};
            resp.forEach((res) => {
              data_map[res.id] = res;
            });
            data.map((ele) => {
              if (data_map[ele.user_id]) {
                ele.user_type = data_map[ele.user_id].user_type;
                ele.ips = data_map[ele.user_id].ips;
                // ele.ips = ["yash", "golu", "suresh"];
                ele.webhook_url = data_map[ele.user_id].webhook_url;
                ele.telegram_id = data_map[ele.user_id].telegram_id;
                ele.status = data_map[ele.user_id].status;
              }
              return ele;
            });
          }
        }
      );

      // temp fix to remove permissions complexity by Prashin More

      // me.src.db.query(
      //   `select id, allow, deny, write from permissions where user_id in ${queryList};`,
      //   [],
      //   async function(err, resp) {
      //     if (err) {
      //       console.error(
      //         `UserAccountInvitationModel. Func:afterGetData. ${me.collection} table Database Error:`,
      //         err
      //       );
      //     } else if (resp.length) {
      //       let data_map = {};
      //       resp.forEach((res) => {
      //         data_map[res.id] = res;
      //       });
      //       data.map((ele) => {
      //         ele.allow = data_map[ele.user_id].allow;
      //         ele.deny = data_map[ele.user_id].deny;
      //         ele.write = data_map[ele.user_id].write;
      //         return ele;
      //       });
      //     }
      //   }
      // );
    }

    if (!!data && data.length) {
      let sql = "select id, role_name from roles where removed=0 ";
      sql = await this.buildRealmConditionQuery(sql);
      me.src.db.query(sql, [], async function(err, resp) {
        if (err) {
          console.error(
            `UserAccountInvitationModel. Func:afterGetData. ${me.collection} table Database Error:`,
            err
          );
        } else if (resp.length) {
          let roles = {};
          resp.forEach((res) => {
            roles[res.id] = res.role_name;
          });
          data.map((ele) => {
            ele.role_name = roles[ele.role];
            return ele;
          });
        }
      });
    }

    return cb(data);
  },

  /* scope:server */
  async write(data, cb) {
    var me = this;

    Object.keys(data).forEach(function(key, index) {
      if (typeof data[key] == "string" && data[key].length > 0) {
        data[key] = data[key].trim();
      }
    });

    if (!!data && !!data.id) {
      data.accepted = null;
    }
    let arrIPs = [];
    if (data && typeof data.ips == "string" && data.ips.length > 0) {
      arrIPs = data.ips.split(",");
      delete data.ips;
    }
    data.ips = arrIPs;
    if (!!data && !!data.id) {
      try {
        console.log(
          `UserAccountInvitationsModel. Func:write. ${me.collection} table Database data:`,
          data
        );
        const params = {
          service: "auth-service",
          method: "inviteUser",
          data: data,
          options: {
            realmId: me.user.profile.realm_id
          },
          writeFlag: true
        };
        const account = await me.callApi(params);
        console.log(
          `UserAccountInvitationsModel. Func:write. response from callApi:`,
          account
        );
        if (account.error) {
          return cb({
            success: false,
            message: account.error.title
          });
        }
        return cb({ success: true });
      } catch (error) {
        console.error(
          `UserAccountInvitationsModel. Func:write. ${me.collection} table Database Error:`,
          error
        );
        return cb({
          success: false,
          message: `User invitation failed: ${error}`
        });
      }
    }
    return cb({ success: false, message: "No data found" });
  },

  /* scope:client */
  async populateComboBox(data, callbackFunction) {
    this.runOnServer("populateComboBox", data, callbackFunction);
  },

  /* scope:client */
  async checkIfExists(data, cb) {
    this.runOnServer("checkIfExists", data, cb);
  },

  /* scope:server */
  async $populateComboBox(data, callbackFunction) {
    try {
      const res = await this.src.db.query(data.query);
      callbackFunction(res);
    } catch (err) {
      console.error(
        `Func:populateComboBox, File: UserAccountInvitationsModel.js, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      callbackFunction(null);
    }
  },
  /* scope:client */
  cancelInvitation: function(data, cb) {
    this.runOnServer("cancelInvitation", data, cb);
  },

  /* scope:server */
  $cancelInvitation: async function(data, cb) {
    var me = this;
    if (data.id) {
      const res = await this.callApi({
        service: "auth-service",
        method: "cancelInvitation",
        data: {
          id: data.id
        },
        options: {
          realmId: me.user.profile.realm_id
        },
        writeFlag: true
      });
      return cb(res);
    }
  },

  /* scope:server */
  $checkIfExists: async function(data, cb) {
    var me = this;

    if (data.id) {
      me.src.db.query(
        "select * from user_account_invitations where id=$1",
        [data.id],
        function(err, resp) {
          if (err) {
            console.error(
              `userAccountInvitationsModel. Func:checkIfExists. ${me.collection} table Database Error:`,
              err
            );
            cb(false);
          } else if (!!resp && resp.length) {
            cb(resp);
          } else {
            cb(false);
          }
        }
      );
    } else {
      return cb(false);
    }
  },

  /* scope:client */
  resendInvitation: function(data, cb) {
    this.runOnServer("resendInvitation", data, cb);
  },

  /* scope:server */
  $resendInvitation: async function(data, cb) {
    var me = this;
    if (data.id) {
      const res = await this.callApi({
        service: "auth-service",
        method: "inviteUser",
        data,
        options: {
          realmId: me.user.profile.realm_id
        },
        writeFlag: true
      });
      return cb(res);
    } else {
      return cb(false);
    }
  }
});
