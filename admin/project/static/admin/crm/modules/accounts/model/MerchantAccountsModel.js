Ext.define("Crm.modules.accounts.model.MerchantAccountsModel", {
  extend: "Crm.classes.DataModel",

  collection: "vw_merchant_accounts",
  idField: "id",
  //removeAction: "remove",
  foreignKeyFilter: [
    {
      collection: "accounts",
      alias: "a",
      on: "  vw_merchant_accounts.account_id = accounts.id  ",
      type: "inner"
    }
  ],

  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "account_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "name",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "acc_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "currency",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "settlement_currency",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "category",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "status",
      type: "enumstring",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "active_count",
      type: "integer",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "inactive_count",
      type: "integer",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "category_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "expiry_hours",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "payout_expiry_hours",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "auto_payouts",
      type: "integer",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "api_credential_id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "payment_page_theme",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  /* scope:server */
  async write(data, cb) {
    var me = this;

    Object.keys(data).forEach(function(key, index) {
      if (typeof data[key] == "string" && data[key].length > 0) {
        data[key] = data[key].trim();
      }
    });
    if (!!data && !!data.id) {
      if (data.auto_payouts) {
        data.auto_payouts = 1;
      } else {
        data.auto_payouts = 0;
      }
      try {
        const params = {
          service: "account-service",
          method: "createMerchantAccount",
          realm: data.owner,
          user: data.owner,
          data: data,
          writeFlag: true
        };

        const account = await me.callApi(params);

        if (account.error) {
          return cb({ success: false, message: account.error.message });
        } else {
          if (data.auto_payouts) {
            const params = {
              service: "account-service",
              method: "assignAPICreds",
              data: {
                merchant_account_id: account.result.id,
                api_credentials: data.api_credentials
              },
              options: {
                realmId: me.user.profile.realm_id
              }
            };

            const assignAPICreds = await me.callApi(params);

            if (assignAPICreds.error) {
              if (
                assignAPICreds.error.code ===
                "BANJO_ERR_MERCHANT_ACCOUNT_IS_AlREADY_ALLCOCATE_TO_THIS_BANK_ACCOUNT"
              ) {
                return cb(data);
              } else {
                return cb({
                  success: false,
                  message: assignAPICreds.error.message
                });
              }
            } else {
              return cb(data);
            }
          }
        }
      } catch (error) {
        console.error(
          `MerchantAccountsModel. Func:write. ${me.collection} table Database Error:`,
          error
        );
        return cb({
          success: false,
          message: `Merchant account could not be updated. Error: ${error}`
        });
      }
    } else {
      return cb({ success: false, message: "No data found" });
    }
    return cb(data);
  },

  /* scope:client */
  async populateComboBox(data, callbackFunction) {
    this.runOnServer("populateComboBox", data, callbackFunction);
  },

  /* scope:server */
  async $populateComboBox(data, callbackFunction) {
    try {
      const res = await this.src.db.query(data.query);
      callbackFunction(res);
    } catch (err) {
      console.error(
        `Func:populateComboBox, File: MerchantAccountsModel.js, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      callbackFunction(err);
    }
  },

  /* scope:client */
  async checkIfExists(data, cb) {
    this.runOnServer("checkIfExists", data, cb);
  },

  /* scope:server */
  $checkIfExists: async function(data, cb) {
    var me = this;

    if (data.id) {
      let sql = "select vw_merchant_accounts.* from vw_merchant_accounts ";
      sql = await this.buildJoinQuery(sql);
      sql += ` where vw_merchant_accounts.id=$1 `;
      me.src.db.query(sql, [data.id], function(err, resp) {
        if (err) {
          console.error(
            `MerchantAccountsModel. Func:checkIfExists. ${me.collection} table Database Error:`,
            err
          );
          cb(false);
        } else if (!!resp && resp.length) {
          cb(true);
        } else {
          cb(false);
        }
      });
    } else {
      return cb(false);
    }
  },

  /* scope:server*/
  async beforeSave(data, cb) {
    data.api_credential_id = data.api_credentials;
  },

  async afterGetData(data, cb) {
    data.map((el) => {
      if (el.api_credential_id) {
        el.api_credentials = el.api_credential_id;
      }
    });
    cb(data);
  }
});
