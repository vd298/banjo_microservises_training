Ext.define("Crm.modules.accounts.model.AccountsModel", {
  extend: "Crm.classes.DataModel",

  collection: "accounts",
  idField: "id",
  //removeAction: "remove",

  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "status",
      type: "string",
      sort: 1,
      filterable: false,
      editable: true,
      visible: true
    },
    {
      name: "name",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "address",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "city",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "country",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "legal_entity",
      type: "boolean",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "type",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "registration_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "plan_id",
      type: "ObjectID",
      visible: true,
      editable: true
    },
    {
      name: "realm_id",
      type: "ObjectID",
      visible: true,
      editable: true
    },
    {
      name: "variables",
      type: "object",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  /* scope:server */
  async afterGetData(data, cb) {
    var me = this;
    if (!!data && data.length) {
      // me.src.db.query(
      //   "select id, variables from tariffplans;",
      //   [],
      //   async function(err, resp) {
      //     if (err) {
      //       console.error(
      //         `AccountsModel. Func:afterGetData. ${me.collection} table Database Error:`,
      //         err
      //       );
      //     } else if (resp.length) {
      //       let variables = {};
      //       resp.forEach((res) => {
      //         variables[res.id] = res.variables;
      //       });
      //       data.map((ele) => {
      //         if (!ele.variables) {
      //           ele.variables = variables[ele.plan_id];
      //         }
      //         ele.tariff = ele.plan_id;
      //         return ele;
      //       });
      //     }
      //   }
      // );
      data.map((ele) => {
        ele.tariff = ele.plan_id;
        return ele;
      });
    }
    return cb(data);
  },

  /* scope:server */
  async write(data, cb) {
    var me = this;

    Object.keys(data).forEach(function(key, index) {
      if (typeof data[key] == "string" && data[key].length > 0) {
        data[key] = data[key].trim();
      }
    });

    if (data.id) {
      data.legal_entity = data.legal_entity == "on" ? true : false;
      //Handling plans and variables
      data.plan_id = data.tariff;
    }

    if (!!data && !!data.id) {
      try {
        const params = {
          service: "account-service",
          method: "createAccount",
          realm: data.owner,
          user: data.owner,
          data: data,
          writeFlag: true,
          options: {
            realmId: me.user.profile.realm_id
          }
        };

        const account = await me.callApi(params);
        me.changeModelData(
          "Crm.modules.accounts.model.AccountsModel",
          "ins",
          {}
        );
        if (account && account.result && account.result.id)
          return cb({ success: true });
        else if (account && account.error) {
          let errMsg = me.config.MESSAGES.BANJO_WRITE_ERR;
          if (account.error.message) {
            errMsg = account.error.message;
          }
          return cb({
            success: false,
            message: errMsg
          });
        }
      } catch (error) {
        console.error(
          `AccountsModel. Func:write. ${me.collection} table Database Error:`,
          error
        );
        return cb({
          success: false,
          message:
            "Write error, data could not be saved. Please reverify input."
        });
      }
    }
    return cb({
      success: false,
      message: "Incorrect Data, Please fill empty fields"
    });
  },

  /* scope:client */
  async populateComboBox(data, callbackFunction) {
    this.runOnServer("populateComboBox", data, callbackFunction);
  },

  /* scope:client */
  async checkMerchantName(data, callbackFunction) {
    this.runOnServer("checkMerchantName", data, callbackFunction);
  },

  /* scope:server */
  async $populateComboBox(data, callbackFunction) {
    try {
      const res = await this.src.db.query(data.query);
      callbackFunction(res);
    } catch (err) {
      console.error(
        `Func:populateComboBox, File: AccountsModel.js, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      callbackFunction(err);
    }
  },

  /* scope:server */
  async $checkMerchantName(data, callbackFunction) {
    try {
      let res;
      if (data.id) {
        let sql = `select * from accounts where name = '${data.name}' and removed=0 and id!='${data.id}'`;
        sql = await this.buildRealmConditionQuery(sql);
        res = await this.src.db.query(sql);
      } else {
        let sql = `select * from accounts where name = '${data.name}' and removed=0'`;
        sql = await this.buildRealmConditionQuery(sql);
        res = await this.src.db.query(sql);
      }
      callbackFunction(res);
    } catch (err) {
      console.error(
        `Func:checkMerchantName, File: AccountsModel.js, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      console.log(err);
      callbackFunction(err);
    }
  },

  /* scope:server */
  async $createMerchantAccount(data, cb) {
    try {
      var me = this;
      //temptag
      console.log(
        `File:AccountsModel.js Func:$createMerchantAccount. Making API call to onboardMerchant`
      );
      if (!!data.legal_entity) {
        if (data.legal_entity == "on") {
          data.legal_entity = true;
        } else {
          data.legal_entity = false;
        }
      }
      var res = await me.callApi({
        service: "account-service",
        method: "onboardMerchant",
        data: data,
        options: {
          realmId: me.user.profile.realm_id
        },
        writeFlag: true
      });
      console.log(
        `File:AccountsModel.js Func:$createMerchantAccount.Received Response`
      );
      if (res && typeof res == "object") {
        console.log(JSON.stringify(res));
      }
      if (
        res &&
        res.result &&
        res.result.message == "Merchant onboarded successfully"
      ) {
        res.actionStatus = true;
      } else {
        res.actionStatus = false;
      }
      return cb(res);
    } catch (err) {
      console.error(
        `AccountsModel.js Func:performMerchantCreate, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      return cb(err);
    }
  },

  /* scope:server */
  async $checkBasicMerchant(data, cb) {
    try {
      var me = this,
        requestData;
      if (data && data.basic_info) {
        requestData = data.basic_info;
      } else {
        console.error(
          `AccountsModel. Func:checkBasicMerchant. Missing request data`
        );
        return cb({
          success: false,
          message: "Invalid Request, Missing request data."
        });
      }
      var res = await Promise.race([
        me.callApi({
          service: "account-service",
          method: "checkBasicMerchant",
          data: requestData,
          options: {
            realmId: me.user.profile.realm_id
          }
        }),
        new Promise((resolve, reject) => {
          setTimeout(
            () =>
              reject({
                success: false,
                message: "Request Timeout, Please try again after some time."
              }),
            45000
          );
        })
      ]);
      if (res && res.result && res.result.success) {
        res.actionStatus = true;
      } else {
        res.actionStatus = false;
      }
      return cb(res);
    } catch (err) {
      console.error(
        `AccountsModel.js Func:checkBasicMerchant, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      return cb(err);
    }
  },
  /* scope:server */
  async $fetchOwnerRoleId(data, cb) {
    try {
      var me = this;
      if (me.user && me.user.profile && me.user.profile.realm_id) {
        let sql = "select id from roles where role_name ilike $1";
        sql = await this.buildRealmConditionQuery(sql);
        const res = await this.src.db.query(sql, ["Owner"]);
        console.log(res[0].id);
        if (res && res[0]) {
          return cb({ id: res[0].id });
        } else {
          return cb({ id: null });
        }
      }
      return cb({ id: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4" });
    } catch (err) {
      console.error(
        `AccountsModel.js Func:fetchOwnerRoleId, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      return cb(err);
    }
  }
});
