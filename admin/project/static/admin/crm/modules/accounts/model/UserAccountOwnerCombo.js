Ext.define("Crm.modules.accounts.model.UserAccountOwnerCombo", {
  extend: "Crm.modules.accounts.model.UserAccountsModel",

  getData: async function(params, cb) {
    var me = this;
    let sqlHolders = [];
    let sql = `select ua.id, u.id as owner_id, u.email, u.first_name, u.last_name from user_accounts ua join users u on u.id = ua.user_id and u.email IS NOT NULL and ua.main_role = $1 and ua.account_id=$2 join accounts a on a.id = ua.account_id `;
    sql = await this.buildRealmConditionQuery(sql, " a.realm_id");
    let paramArray = [me.config.owner_role_id, params.filters[0]._value];
    if (params._filters && params._filters[0]._value) {
      sql = sql + ` where u.first_name ilike $3 or u.email ilike $4`;
      paramArray.push(`%${params._filters[0]._value}%`);
      paramArray.push(`%${params._filters[0]._value}%`);
    }
    
    let res = await this.src.db.query(sql, paramArray);

    if (res && res[0]) {
      return cb({ total: res.length, list: res });
    } else {
      console.error(
        "UserAccountOwnerCombo.js. Func:getData. Database Error. Error",
        res
      );
      return cb({ total: 0, list: [] });
    }
  }
});
