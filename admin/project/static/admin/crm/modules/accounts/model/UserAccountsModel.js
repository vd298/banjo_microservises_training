Ext.define("Crm.modules.accounts.model.UserAccountsModel", {
  extend: "Core.data.DataModel",

  collection: "users_accounts",
  idField: "id",
  //removeAction: "remove",
  foreignKeyFilter: [
    {
      collection: "accounts",
      alias: "a",
      on: " users_accounts.account_id =accounts.id  ",
      type: "inner"
    }
  ],

  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "account_id",
      type: "ObjectID",
      sort: 1,
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "user_id",
      sort: 1,
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "main_role",
      sort: 1,
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "additional_role",
      type: "object",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "start_date",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  async beforeSave(data, cb) {
    // let me = this;
    cb(data);
  }
});
