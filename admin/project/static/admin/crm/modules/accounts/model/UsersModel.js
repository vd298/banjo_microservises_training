Ext.define("Crm.modules.accounts.model.UsersModel", {
  extend: "Core.data.DataModel",

  collection: "users",
  idField: "id",
  //removeAction: "remove",
  foreignKeyFilter: [
    {
      collection: "user_accounts",
      alias: "ua",
      on: " users.id = user_accounts.user_id ",
      type: "inner"
    },
    {
      collection: "accounts",
      alias: "a",
      on: " accounts.id = user_accounts.account_id ",
      type: "inner"
    }
  ],
  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "first_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "last_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "email",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mobile",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "username",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "password",
      type: "string",
      filterable: false,
      editable: true,
      visible: false
    },
    {
      name: "status",
      type: "string",
      filterable: false,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  async beforeSave(data, cb) {
    let me = this;
    const Util = Ext.create("Crm.Util", { scope: me });
    let password = Util.getRandomPassword(10);
    data.password = Util.generateHash(password);
    console.log("Generated Password", password);
    this.account_id = data.account_id;
    this.user_account_id = data.user_account_id;
    this.main_role = data.main_role;
    this.additional_role = data.additional_role;
    cb(data);
  },

  async afterSave(data, cb) {
    console.log("After save data");
    // console.log("After save data", data);
    console.log("User Account Id", this.user_account_id);
    if (this.account_id) {
      Ext.create("Crm.modules.accounts.model.UserAccountsModel", {
        scope: this
      }).write(
        {
          account_id: this.account_id,
          id: this.user_account_id,
          user_id: data.id,
          start_date: new Date(),
          main_role: this.main_role || null,
          additional_role: this.additional_role || null
        },
        function() {
          cb(data);
        },
        { add: true, modify: true }
      );
      // cb(data);
    } else {
      console.log("No parent account associated");
      cb(null, "No parent account associated");
    }
  },
  async afterGetData(data, cb) {
    if (data.length) {
      data.map((key) => {
        key.full_name = key.first_name + " " + key.last_name;
      });
    }
    cb(data);
  }
});
