Ext.define("Crm.modules.accounts.model.BeneficiariesModel", {
  extend: "Crm.classes.DataModel",

  collection: "beneficiaries",
  idField: "id",
  foreignKeyFilter: [
    {
      collection: "accounts",
      alias: "a",
      on: " beneficiaries.account_id = accounts.id ",
      type: "inner"
    }
  ],

  fields: [
    {
      name: "id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "account_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "merchant_account_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "protocols",
      type: "array",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "first_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "last_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "email",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "country_code",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mobile",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "acc_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ifsc",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "bank_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "crypto_wallet_address",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "description",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "vpa",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    }
  ],
  /* scope:server */
  async write(data, cb) {
    var me = this;

    Object.keys(data).forEach(function(key, index) {
      if (typeof data[key] == "string" && data[key].length > 0) {
        data[key] = data[key].trim();
      }
    });

    if (!!data && !!data.id) {
      try {
        console.log(
          `BeneficiariesModel. Func:write. ${me.collection} table Database data:`,
          data
        );
        const params = {
          service: "account-service",
          method: "addUpdateBeneficiary",
          data: data,
          writeFlag: true,
          options: {
            realmId: me.user.profile.realm_id
          }
        };
        const beneficiaries = await me.callApi(params);
        console.log(
          `BeneficiariesModel. Func:write. response from callApi:`,
          beneficiaries
        );
        if (beneficiaries.error) {
          return cb({
            success: false,
            message: beneficiaries.error.title
          });
        }
        return cb({ success: true });
      } catch (error) {
        console.error(
          `BeneficiariesModel. Func:write. ${me.collection} table Database Error:`,
          error
        );
        return cb({
          success: false,
          message: `Beneficiaries failed: ${error}`
        });
      }
    }
    return cb({ success: false, message: "No data found" });
  },
  /* scope:server */
  async afterGetData(data, cb) {
    var me = this;
    const Util = Ext.create("Crm.Util", { scope: me });
    if (!!data && data.length) {
      let merchant_account_id = [];
      data.map((item) => {
        item.id && merchant_account_id.push(`'${item.id}'`);
      });

      let sqlPlaceHolders = [];
      let sql = `SELECT b.*,ma.name as merchant_account_name FROM beneficiaries b join merchant_accounts ma ON ma.id = b.merchant_account_id AND ma.removed = 0 WHERE ${Util.pushArray(
        data.map((e) => {
          if (typeof e.id == "string" && e.id.length) {
            return e.id;
          } else return null;
        }),
        sqlPlaceHolders,
        { colName: "b.id" }
      )} `;
      const resp = await this.src.db.query(
        `${sql}  ORDER BY b.mtime DESC`,
        sqlPlaceHolders
      );
      return cb(resp);
    }
    return cb(data);
  }
});
