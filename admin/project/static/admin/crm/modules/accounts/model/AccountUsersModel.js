Ext.define("Crm.modules.accounts.model.AccountUsersModel", {
  extend: "Crm.classes.DataModel",

  collection: "vw_user_accounts",
  idField: "id",
  //removeAction: "remove",
  removedFilter: true,
  foreignKeyFilter: [
    {
      collection: "accounts",
      alias: "a",
      on: " vw_user_accounts.account_id = accounts.id",
      type: "inner"
    }
  ],

  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "first_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "last_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "email",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mobile",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "username",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "password",
      type: "string",
      filterable: false,
      editable: true,
      visible: false
    },
    {
      name: "status",
      type: "string",
      filterable: false,
      editable: true,
      visible: true
    },
    {
      name: "account_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "user_account_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "main_role",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "additional_role",
      type: "object",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "primary_role",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "secondary_role",
      type: "array",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "start_date",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    }
  ],
  /* scope:server */
  buildWhere: function(params, callback) {
    params.filters = [
      {
        initialConfig: {
          property: "removed",
          value: 0,
          type: "combo",
          operator: "eq"
        },
        config: {
          property: "removed",
          value: 0,
          type: "combo",
          operator: "eq"
        },
        type: "combo",
        _property: "removed",
        _value: 0,
        _disableOnEmpty: 0,
        _operator: "eq",
        _root: "data",
        _id: "removed"
      }
    ].concat(params.filters);
    this.db.buildWhere(params, this, callback);
  },

  /* scope:server */
  async $deleteUser(data, cb) {
    let sql = "update users set removed=1 where id=$1";
    sql = await this.buildRealmConditionQuery(sql);
    await this.src.db.query(sql, [data.id]);
    cb({ success: true });
  }
});
