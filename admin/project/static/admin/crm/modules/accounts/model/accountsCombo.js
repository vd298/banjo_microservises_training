Ext.define("Crm.modules.accounts.model.accountsCombo", {
  extend: "Crm.modules.accounts.model.AccountUsersModel",

  getData: async function(params, cb) {
    let query = "SELECT name,id from accounts where removed = 0";
    query = await this.buildRealmConditionQuery(query);
    const res = await this.src.db.query(query, function(err, resp) {
      if (err) {
        console.error(
          "CurrencyCombo.js. Func:getData. Database Error. Error",
          err
        );
        return cb({ total: 0, list: {} });
      }
      return cb({ total: resp.length, list: resp.rows });
    });
  }
});
