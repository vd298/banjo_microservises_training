Ext.define("Crm.modules.accounts.model.merchantCombo", {
  extend: "Crm.modules.accounts.model.MerchantAccountsModel",

  getData: async function(params, cb) {
    let query = `SELECT merchant_accounts.name,merchant_accounts.id from merchant_accounts `;
    query = await this.buildJoinQuery(query);
    query += ` where merchant_accounts.account_id = '${params.v}' and merchant_accounts.removed = 0`;
    
    const res = await this.src.db.query(query, function(err, resp) {
      if (err) {
        console.error(
          "CurrencyCombo.js. Func:getData. Database Error. Error",
          err
        );
        console.log(resp);
        return cb({ total: 0, list: {} });
      }
      return cb({ total: resp.length, list: resp.rows });
    });
  }
});
