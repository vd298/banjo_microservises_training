Ext.define("Crm.modules.paymentProvider.model.paymentProviderStatusCombo", {
  extend: "Crm.modules.paymentProvider.model.paymentProviderModel",
  getData: async function(params, cb) {
    const res = await this.src.db.query(
      "SELECT unnest(enum_range(NULL::enum_payment_providers_status)) as status ;",
      function(err, resp) {
        if (err) {
          console.error(
            "UserRiskTypesCombo.js. Func:getData. Database Error. Error",
            err
          );
        }
        return cb({ total: resp.length, list: resp.rows });
      }
    );
  },
});
