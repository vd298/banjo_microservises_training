Ext.define("Crm.modules.paymentProvider.model.paymentProviderModel", {
  extend: "Core.data.DataModel",
  collection: "payment_providers",
  idField: "id",
  strongRequest: false,

  maxLimit: 1000,

  fields: [
    {
      name: "id",
      type: "ObjectID"
    },
    {
      name: "name",
      type: "string",
      sort: 1,
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "status",
      type: "string",
      filterable: false,
      editable: true,
      visible: true
    }
  ],

  /* scope:client */
  checkAssociatedBankAccounts: function(data, cb) {
    this.runOnServer("checkAssociatedBankAccounts", data, cb);
  },

  /* scope:server */
  async $checkAssociatedBankAccounts(data, cb) {
    let associated_accounts_count = await this.src.db
      .collection("bank_accounts")
      .findAll({ payment_provider_id: data.id, removed: 0 });

    cb(associated_accounts_count);
  },

  /* scope:server */
  beforeRemove: async function(ids, callback) {
    this.$checkAssociatedBankAccounts({ id: ids[0] }, (res) => {
      if (!!res && res.length) {
        callback(null);
      } else {
        callback(ids);
      }
    });
  },

  /* scope:server */
  async $checkUniqueName(data, cb) {
    if (data.id) {
      let sql = `select id from payment_providers where removed=0 and name ilike $1 and id != $2`;
      sql = await this.buildRealmConditionQuery(sql);
      const duplicateCheck = await this.src.db.query(sql, [data.name, data.id]);
      if (duplicateCheck[0]) {
        return cb({
          result: false,
          message: "Name parameter need to be unique."
        });
      } else {
        return cb({ result: true });
      }
    } else {
      return cb({ result: true });
    }
  },
  async beforeSave(data, cb) {
    data.name = data.name.trim();
    cb(data);
  }
});
