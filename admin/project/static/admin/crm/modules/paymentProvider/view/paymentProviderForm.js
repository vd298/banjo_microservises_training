Ext.define("Crm.modules.paymentProvider.view.paymentProviderForm", {
  extend: "Core.form.FormWindow",
  titleTpl: "Payment Provider: {name}",
  controllerCls:
    "Crm.modules.paymentProvider.view.paymentProviderFormController",
  width: 500,
  height: 200,
  syncSize: function() {},
  buildItems: function() {
    return [
      {
        name: "id",
        hidden: true
      },
      {
        name: "name",
        fieldLabel: D.t("Name"),
        allowBlank: false,
        maxLength: 50,
        minLength: 1,
        maskRe: /[a-zA-Z0-9]/,
        validator: function(value) {
          return true;
        }
      },
      this.buildStatusCombo()
    ];
  },
  buildStatusCombo() {
    return {
      name: "status",
      xtype: "combo",
      fieldLabel: D.t("Status"),
      queryMode: "all",
      displayField: "status",
      editable: false,
      valueField: "status",
      validator: true,
      allowBlank: false,
      store: Ext.create("Core.data.ComboStore", {
        storeId: "status",
        dataModel: Ext.create(
          "Crm.modules.paymentProvider.model.paymentProviderStatusCombo"
        ),
        fieldSet: ["status"],
        scope: this
      })
    };
  }
});
