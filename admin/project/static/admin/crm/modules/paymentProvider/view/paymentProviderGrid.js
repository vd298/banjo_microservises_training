Ext.define("Crm.modules.paymentProvider.view.paymentProviderGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Payment Provider"),
  iconCls: "x-fa fa-usd",
  filterable: true,
  filterbar: true,

  controllerCls:
    "Crm.modules.paymentProvider.view.paymentProviderGridController",

  buildColumns: function() {
    return [
      {
        text: D.t("Name"),
        flex: 1,
        filter: true,
        sortable: true,
        dataIndex: "name"
      },
      {
        text: D.t("Status"),
        flex: 1,
        sortable: true,
        dataIndex: "status",
        filter: {
          xtype: "combo",
          valueField: "status",
          displayField: "status",
          queryMode: "all",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create(
              "Crm.modules.paymentProvider.model.paymentProviderStatusCombo"
            ),
            fieldSet: ["status"],
            scope: this
          })
        }
      }
    ];
  }
});
