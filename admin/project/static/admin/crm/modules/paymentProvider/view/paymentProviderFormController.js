Ext.define("Crm.modules.paymentProvider.view.paymentProviderFormController", {
  extend: "Core.form.FormController",
  setControls() {
    this.control({
      "[name=name]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();
          if (formData.name.trim().length < 1) {
            e.validator = function(value) {
              return "White space not allowed";
            };
          }

          this.model.runOnServer("checkUniqueName", formData, function(res) {
            e.validator = function(value) {
              if (res.result) {
                return true;
              } else {
                return res.message;
              }
            };
            me.view.down("form").isValid();
          });
        }
      }
    });
    this.callParent(arguments);
  }
});
