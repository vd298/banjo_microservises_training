Ext.define("Crm.modules.paymentProvider.view.paymentProviderGridController", {
  extend: "Core.grid.GridController",
  setControls() {
    let me = this;
    this.view.on("delete", function(grid, indx) {
      me.model.runOnServer(
        "checkAssociatedBankAccounts",
        { id: grid.getRecord(indx).id },
        (res) => {
          if (!!res && res.length) {
            D.a(
              "Delete Error",
              `Payment provider cannot be deleted as it is assigned to ${res.length} bank accounts. Please de-assign before deleting.`,
              []
            );
          }
        }
      );
    });
    this.callParent(arguments);
  }
});
