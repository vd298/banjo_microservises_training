Ext.define("Crm.modules.settlements.view.SettlementsGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Settlements"),

  filterable: true,
  filterbar: true,

  controllerCls: "Crm.modules.settlements.view.SettlementsGridController",

  buildColumns: function() {
    const statusArray = [
      { key: 0, name: D.t("FAILED") },
      { key: 1, name: D.t("APPROVED") },
      { key: 2, name: D.t("INPROGRESS") },
      { key: 3, name: D.t("REJECTED") },
      { key: 4, name: D.t("PARTIALLY APPROVED") }
    ];
    this.statusTypesStore = Ext.create("Ext.data.Store", {
      fields: ["key", "name"],
      data: statusArray
    });
    return [
      {
        text: D.t("Merchant Account"),
        flex: 1,
        sortable: true,
        filter: true,
        renderer: function(v, m, r) {
          if (v) {
            return (
              '<a href="#Crm.modules.accounts.view.MerchantAccountsForm~' +
              v +
              '">' +
              r.data.merchant_account_name +
              "</a> "
            );
          }
          return v;
        },
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "id",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create(
              "Crm.modules.accounts.model.MerchantAccountsModel"
            ),
            fieldSet: ["id", "name"],
            scope: this
          }),
          operator: "eq"
        },
        dataIndex: "merchant_account_id"
      },
      {
        text: D.t("Merchant Currency"),
        flex: 1,
        sortable: true,
        filter: true,
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "abbr",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create("Crm.modules.currency.model.CurrencyModel"),
            fieldSet: ["name", "abbr"],
            scope: this
          }),
          operator: "eq"
        },
        dataIndex: "src_currency"
      },
      {
        text: D.t("Settlement Currency"),
        flex: 1,
        sortable: true,
        filter: true,
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "abbr",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create("Crm.modules.currency.model.CurrencyModel"),
            fieldSet: ["name", "abbr"],
            scope: this
          }),
          operator: "eq"
        },
        dataIndex: "dst_currency"
      },
      {
        text: D.t("Source Amount"),
        width: 80,
        sortable: true,
        filter: { xtype: "numberfield" },
        dataIndex: "src_amount",
        renderer: function(v, m, r) {
          if (v) {
           return __CONFIG__.formatCurrency(v,0,r.data.src_currency)
          }
          return v;
        },
      },
      {
        text: D.t("Destination Amount"),
        width: 80,
        sortable: true,
        filter: { xtype: "numberfield" },
        dataIndex: "dst_amount",
        renderer: function(v, m, r) {
          if (v) {
           return __CONFIG__.formatCurrency(v,0,r.data.dst_currency)
          }
          return v;
        },
      },
      {
        text: D.t("Markup Rate"),
        flex: 1,
        sortable: true,
        filter: true,
        filter: { xtype: "numberfield" },
        dataIndex: "markup_rate"
      },
      {
        text: D.t("Conversion Rate"),
        flex: 1,
        sortable: true,
        filter: { xtype: "numberfield" },
        dataIndex: "conversion_rate"
      },
      {
        text: D.t("Status"),
        flex: 1,
        sortable: true,
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "key",
          displayField: "name",
          store: this.statusTypesStore,
          operator: "eq"
        },
        renderer: function(v, m, r) {
          if (v) {
            if (statusArray[v]) {
              return statusArray[v].name;
            }
          }
          return "FAILED";
        },
        dataIndex: "status"
      }
    ];
  },
  buildButtonsColumns() {
    var me = this;
    return [
      {
        xtype: "actioncolumn",
        width: 54,
        items: [
          
        ]
      }
    ];
  }
});
