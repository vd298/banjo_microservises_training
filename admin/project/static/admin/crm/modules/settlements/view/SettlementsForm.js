Ext.define("Crm.modules.settlements.view.SettlementsForm", {
  extend: "Core.form.FormContainer",

  titleTpl: D.t("Settlements"),
  iconCls: "x-fa fa-user",
  requires: [
    "Desktop.core.widgets.GridField",
    "Core.form.DateField",
    "Core.form.DependedCombo",
    "Ext.form.field.Tag"
  ],
  formLayout: "fit",
  width: "75vw",
  height: "90vh",
  manageHeight: true,
  scrollable: true,

  syncSize: function() {},

  formMargin: 0,
  name: "merchantAccountEditForm",

  model: "Crm.modules.settlements.model.SettlementsModel",

  controllerCls: "Crm.modules.settlements.view.SettlementsFormController",

  buildItems() {
    var me = this;
    const statusArray = [
      { key: 0, name: D.t("FAILED") },
      { key: 1, name: D.t("APPROVED") },
      { key: 2, name: D.t("INPROGRESS") },
      { key: 3, name: D.t("REJECTED") },
      { key: 4, name: D.t("PARTIALLY APPROVED") }
    ];
    this.statusTypesStore = Ext.create("Ext.data.Store", {
      fields: ["key", "name"],
      data: statusArray
    });
    return {
      xtype: "tabpanel",
      layout: "fit",
      activeTab: 0,
      items: [this.buildBasicForm(me), this.buildTransferGrid()],
      autoEl: { "data-test": "tab-items" }
    };
  },

  buildTransferGrid() {
    const me = this;
    return {
      xtype: "panel",
      layout: "fit",
      region: "center",
      title: D.t("Transfer"),
      name: "transferGrid",
      items: Ext.create("Crm.modules.Transfers.view.SettlementTransfersGrid", {
        title: null,
        iconCls: null,
        scope: me,
        observe: [{ property: "settlement_id", param: "id" }]
      })
    };
  },
  buildBasicForm(scope) {
    let me = scope;
    return {
      xtype: "panel",
      layout: "anchor",
      title: D.t("Basic Info"),
      activeTab: 0,
      defaults: {
        anchor: "100%",
        xtype: "textfield",
        labelWidth: 200,
        margin: 5
      },
      items: [
        {
          name: "id",
          hidden: true
        },
        {
          name: "merchant_account_id",
          fieldLabel: D.t("Merchant Account"),
          xtype: "combo",
          editable: false,
          allowBlank: false,
          validateBlank: true,
          validateOnChange: true,
          forceSelection: true,
          queryMode: "all",
          displayField: "name",
          flex: 1,
          valueField: "id",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create(
              "Crm.modules.accounts.model.MerchantAccountsModel"
            ),
            fieldSet: ["id", "name"],
            scope: this
          }),
          allowBlank: false,
          readOnly: true,
          inputWrapCls: "",
          triggerWrapCls: "",
          fieldStyle: "background:none"
        },
        {
          name: "src_currency",
          fieldLabel: D.t("Merchant Currency"),
          xtype: "combo",
          editable: false,
          allowBlank: false,
          validateBlank: true,
          validateOnChange: true,
          forceSelection: true,
          queryMode: "all",
          valueField: "abbr",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create("Crm.modules.currency.model.CurrencyModel"),
            fieldSet: ["name", "abbr"],
            scope: this
          }),
          allowBlank: false,
          readOnly: true,
          inputWrapCls: "",
          triggerWrapCls: "",
          fieldStyle: "background:none"
        },
        {
          name: "dst_currency",
          fieldLabel: D.t("Settlement Currency"),
          xtype: "combo",
          editable: false,
          allowBlank: false,
          validateBlank: true,
          validateOnChange: true,
          forceSelection: true,
          queryMode: "all",
          valueField: "abbr",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create("Crm.modules.currency.model.CurrencyModel"),
            fieldSet: ["name", "abbr"],
            scope: this
          }),
          allowBlank: false,
          readOnly: true,
          inputWrapCls: "",
          triggerWrapCls: "",
          fieldStyle: "background:none"
        },
        {
          name: "src_amount",
          fieldLabel: D.t("Source Settlement Amount"),
          maxLength: 30,
          allowBlank: false,
          readOnly: true,
          inputWrapCls: "",
          triggerWrapCls: "",
          fieldStyle: "background:none"
        },
        {
          name: "dst_amount",
          fieldLabel: D.t("Destination Settlement Amount"),
          maxLength: 30,
          allowBlank: false,
          readOnly: true,
          inputWrapCls: "",
          triggerWrapCls: "",
          fieldStyle: "background:none"
        },
        {
          name: "markup_rate",
          fieldLabel: D.t("Markup Rate"),
          maxLength: 30,
          allowBlank: false,
          readOnly: true,
          inputWrapCls: "",
          triggerWrapCls: "",
          fieldStyle: "background:none"
        },
        {
          name: "conversion_rate",
          fieldLabel: D.t("Conversion Rate"),
          maxLength: 30,
          allowBlank: false,
          readOnly: true,
          inputWrapCls: "",
          triggerWrapCls: "",
          fieldStyle: "background:none"
        },
        {
          name: "exchange_account_id",
          fieldLabel: D.t("Exchange Account"),
          xtype: "combo",
          editable: false,
          validateBlank: true,
          validateOnChange: true,
          forceSelection: true,
          queryMode: "all",
          displayField: "account_name",
          flex: 1,
          valueField: "id",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create(
              "Crm.modules.accountsPool.model.ExchangeAccountsModel"
            ),
            fieldSet: ["id", "account_name"],
            scope: this
          }),
          readOnly: true,
          inputWrapCls: "",
          triggerWrapCls: "",
          fieldStyle: "background:none"
        },
        {
          fieldLabel: D.t("Status"),
          flex: 1,
          sortable: true,
          editable: false,
          validateBlank: true,
          validateOnChange: true,
          forceSelection: true,
          queryMode: "all",
          flex: 1,
          readOnly: true,
          inputWrapCls: "",
          triggerWrapCls: "",
          fieldStyle: "background:none",
          xtype: "combo",
          valueField: "key",
          displayField: "name",
          store: this.statusTypesStore,
          dataIndex: "status"
        }
      ]
    };
  }
});
