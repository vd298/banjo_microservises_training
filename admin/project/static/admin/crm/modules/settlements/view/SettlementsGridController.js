Ext.define("Crm.modules.settlements.view.SettlementsGridController", {
  extend: "Core.grid.GridController",

  setControls: function() {
    var me = this;
    var conversion_amount = 0;
    me.callParent(arguments);
  },

  addRecord: function() {
    var me = this;
    me.view.model.getNewObjectId(function(_id) {
      var oo = {};
      oo[me.view.model.idField] = _id;
      return me.createNewSettlement(oo);
    });
  },

  /**
   * @method createNewSettlement
   * @author Ajit Shinde
   * @since 3 May 2023
   * @summary Window with Card layout panel to handle for new settlement request creation
   */
  createNewSettlement: function(formData) {
    var me = this;
    var adjustWindow = Ext.create("Ext.window.Window", {
      title: "Create New Settlements",
      name: "createMerchantWindow",
      height: "80vh",
      maxHeight: 550,
      width: "70vw",
      layout: "fit",
      margin: 20,
      modal: true,
      items: [
        {
          xtype: "panel",
          width: "60vw",
          height: "80vh",
          layout: "card",
          bodyStyle: "padding:10px",
          name: "mainCreatePanel",
          margin: 20,
          modal: true,
          deferredRender: true,
          defaults: {
            border: false
          },
          bbar: [
            {
              id: "move-prev",
              text: "Back",
              xtype: "button",
              cls: "account-wizard-button",
              handler: function(btn) {
                me.navigate(adjustWindow, btn.up("panel"), "prev");
              },
              border: 5,
              style: {
                backgroundColor: "#35baf6",
                borderColor: "#35baf6",
                borderStyle: "solid",
                color: "#fff"
              },
              disabled: true,
              hidden: true
            },
            "->",
            {
              id: "move-next",
              text: "Step 2: Set Rate",
              xtype: "button",
              cls: "account-wizard-button",
              handler: function(btn) {
                me.navigate(adjustWindow, btn.up("panel"), "next");
              },
              border: 5,
              style: {
                backgroundColor: "#35baf6",
                borderColor: "#35baf6",
                borderStyle: "solid",
                color: "#fff"
              },
              disabled: true
            }
          ],
          items: [
            {
              id: "account_1",
              name: "account_1",
              xtype: "form",
              layout: "anchor",
              scrollable: true,
              submitEmptyText: false,
              defaults: {
                labelWidth: "20",
                width: "70%",
                labelStyle: "white-space: nowrap;"
              },
              defaultType: "textfield",
              items: [
                {
                  flex: 1,
                  xtype: "displayfield",
                  name: "settlement",
                  readOnly: true,
                  editable: false,
                  shrinkwrap: 3,
                  value: `Settlement basic details`
                },
                {
                  name: "id",
                  hidden: true,
                  value: me.uuidv4()
                },
                {
                  flex: 1,
                  xtype: "combo",
                  name: "account",
                  allowBlank: false,
                  forceSelection: true,
                  fieldLabel: D.t("Select Merchant"),
                  store: Ext.create("Core.data.ComboStore", {
                    dataModel: Ext.create(
                      "Crm.modules.accounts.model.AccountsModel"
                    ),
                    fieldSet: ["id", "name"],
                    scope: me
                  }),
                  valueField: "id",
                  displayField: "name",
                  listeners: {
                    change: (el, v) => {
                      adjustWindow
                        .down("[name=merchant_account_id]")
                        .clearValue();
                      adjustWindow
                        .down("[name=Merchant_name]")
                        .setValue(el.rawValue);

                      adjustWindow
                        .down("[name=merchant_account_name]")
                        .setValue("");
                      adjustWindow.down("[name=src_currency]").setValue("");

                      adjustWindow
                        .down("[name=src_currency_name]")
                        .setValue("")
                        .setHidden(true);

                      adjustWindow
                        .down("[name=all_amount]")
                        .setValue(0)
                        .setHidden(true);

                      adjustWindow.down("[name=dst_currency]").setValue("");
                      adjustWindow
                        .down("[name=dst_currency_name]")
                        .setValue("");
                      const proxyFilters = {
                        filters: [
                          {
                            _property: "account_id",
                            _value: v,
                            _operator: "eq"
                          }
                        ]
                      };
                      const store = Ext.create("Core.data.ComboStore", {
                        dataModel: Ext.create(
                          "Crm.modules.accounts.model.MerchantAccountsModel"
                        ),
                        fieldSet: ["id", "name"],
                        exProxyParams: proxyFilters,
                        scope: this
                      });

                      adjustWindow
                        .down("[name=merchant_account_id]")
                        .setStore(store);
                    }
                  }
                },
                {
                  flex: 1,
                  xtype: "combo",
                  name: "merchant_account_id",
                  fieldLabel: D.t("Select Merchant Account"),
                  valueField: "id",
                  allowBlank: false,
                  displayField: "name",
                  forceSelection: true,
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    },
                    change: (el, v) => {
                      if (!!v) {
                        var me = this;
                        this.model.runOnServer(
                          "fetchSettlementCurrencyId",
                          { merchant_account_id: v },
                          async function(res) {
                            if (res && res.length) {
                              adjustWindow
                                .down("[name=merchant_account_name]")
                                .setValue(el.rawValue);

                              adjustWindow
                                .down("[name=src_currency]")
                                .setValue(res[0].currency);

                              adjustWindow
                                .down("[name=src_currency_name]")
                                .setValue(res[0].currency_name)
                                .setHidden(false);
                              const balance = __CONFIG__.formatCurrency(
                                res[0].total_available_balance,
                                0,
                                res[0].currency
                              );
                              adjustWindow
                                .down("[name=all_amount]")
                                .setValue(`${balance} (${res[0].currency})`)
                                .setHidden(false);
                              //Vaibhav Vaidya, 24 April 2023, Add validation for minimum settlement available balance
                              let merchAccountId = adjustWindow.down(
                                "[name=merchant_account_id]"
                              );
                              let errMsg = null;
                              if (merchAccountId) {
                                if (!isNaN(balance) && Number(balance) <= 0) {
                                  errMsg = `Pick a Merchant account with greater than zero balance available for settlement`;
                                  merchAccountId.markInvalid(errMsg);
                                  merchAccountId.validator = function() {
                                    return errMsg;
                                  };
                                } else {
                                  merchAccountId.clearInvalid();
                                  merchAccountId.validator = function() {
                                    return true;
                                  };
                                }

                                if (
                                  !errMsg &&
                                  res[0].settlement_currency !== res[0].currency
                                ) {
                                  await me.model.runOnServer(
                                    "checkExchangeBankAccountForCurrency",
                                    {
                                      currency: res[0].settlement_currency
                                    },
                                    function(secondRes) {
                                      if (secondRes && secondRes.length) {
                                        merchAccountId.clearInvalid();
                                        merchAccountId.validator = function() {
                                          return true;
                                        };
                                      } else {
                                        let errMsg = `Pick a Merchant account with settlement currency ${res[0].settlement_currency} is same Exchange type bank account currency for settlement`;
                                        merchAccountId.markInvalid(errMsg);
                                        merchAccountId.validator = function() {
                                          return errMsg;
                                        };
                                      }
                                    }
                                  );
                                }
                              }

                              adjustWindow
                                .down("[name=dst_currency]")
                                .setValue(res[0].settlement_currency);
                              adjustWindow
                                .down("[name=dst_currency_name]")
                                .setValue(res[0].settlement_currency_name);
                              el.isValid();
                              var nBtn = adjustWindow.down("[id=move-next]");
                              if (errMsg) {
                                nBtn.setDisabled(true);
                              } else {
                                nBtn.setDisabled(false);
                              }
                            }
                          }
                        );
                      }
                    }
                  }
                },
                {
                  name: "src_currency_name",
                  xtype: "displayfield",
                  fieldLabel: D.t("Merchant Account Currency"),
                  allowBlank: true,
                  readOnly: true,
                  hidden: true,
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    }
                  }
                },
                {
                  name: "dst_currency_name",
                  xtype: "displayfield",
                  fieldLabel: D.t("Merchant Account Currency"),
                  allowBlank: true,
                  readOnly: true,
                  hidden: true,
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    }
                  }
                },

                {
                  name: "src_currency",
                  allowBlank: true,
                  readOnly: true,
                  hidden: true,
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    }
                  }
                },
                {
                  name: "dst_currency",
                  allowBlank: true,
                  readOnly: true,
                  hidden: true,
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    }
                  }
                },
                {
                  name: "all_amount",
                  flex: 1,
                  xtype: "displayfield",
                  fieldLabel: D.t("Total Available For Settlement"),
                  readOnly: true,
                  hidden: true,
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    }
                  }
                },
                {
                  name: "Merchant_name",
                  fieldLabel: D.t("Merchant Name"),
                  allowBlank: true,
                  readOnly: true,
                  hidden: true,
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    }
                  }
                },
                {
                  name: "merchant_account_name",
                  fieldLabel: D.t("Merchant Account Name"),
                  allowBlank: true,
                  readOnly: true,
                  hidden: true,
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    }
                  }
                }
              ],
              listeners: {
                activate: function(context, eOpts) {
                  if (adjustWindow) me.checkSetButtons(adjustWindow);
                },
                validitychange: function(context, valid, eOpts) {
                  if (adjustWindow) me.checkSetButtons(adjustWindow, valid);
                },
                resize: function(c, oW, oH, nW, nH) {
                  me.syncSize(c);
                },
                afterRender: function(e, eOpts) {
                  e.isValid();
                  Ext.on("resize", function() {
                    me.syncSize(e);
                  });
                }
              }
            },
            {
              id: "account_2",
              xtype: "form",
              layout: "anchor",
              defaultType: "textfield",
              reserveScrollbar: true,
              submitEmptyText: false,
              autoScroll: true,
              fieldDefaults: {
                labelWidth: "20",
                width: "70%",
                labelStyle: "white-space: nowrap;",
                labelSeparator: ""
              },
              items: [
                {
                  flex: 1,
                  xtype: "displayfield",
                  name: "settlement",
                  readOnly: true,
                  editable: false,
                  shrinkwrap: 3,
                  value: `Select bank account and settlement amount`
                },
                {
                  name: "Merchant_name",
                  fieldLabel: D.t("Merchant Name:"),
                  readOnly: true,
                  inputWrapCls: "",
                  triggerWrapCls: "",
                  fieldStyle: "background:none;padding: 0",
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    }
                  }
                },
                {
                  name: "merchant_account_name",
                  fieldLabel: D.t("Merchant Account Name: "),
                  readOnly: true,
                  inputWrapCls: "",
                  triggerWrapCls: "",
                  fieldStyle: "background:none;padding: 0",
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    }
                  }
                },
                {
                  flex: 1,
                  name: "src_amount_display",
                  xtype: "displayfield",
                  fieldLabel: D.t("Total Settlement Amount:"),
                  allowBlank: true,
                  readOnly: true,
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    }
                  }
                },
                {
                  name: "src_amount",
                  fieldLabel: D.t("Total Settlement Amount:"),
                  allowBlank: true,
                  readOnly: true,
                  value: 0,
                  hidden: true,
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    },
                    change: (el, v) => {
                      let value = adjustWindow
                        .down("[name=conversion_rate]")
                        .getValue(v);

                      me.calculateConversionRate(value, adjustWindow);
                    }
                  }
                },

                {
                  name: "dst_currency",
                  fieldLabel: D.t("Settlement Currency:"),
                  xtype: "combo",
                  readOnly: true,
                  editable: false,
                  validateBlank: true,
                  validateOnChange: true,
                  forceSelection: true,
                  queryMode: "all",
                  displayField: "name",
                  flex: 1,
                  style: {
                    padding: "0"
                  },
                  valueField: "abbr",
                  inputWrapCls: "",
                  triggerWrapCls: "",
                  fieldStyle: "background:none;padding: 0",
                  store: Ext.create("Core.data.ComboStore", {
                    dataModel: Ext.create(
                      "Crm.modules.currency.model.CurrencyModel"
                    ),
                    fieldSet: ["name", "abbr"],
                    scope: me
                  }),
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    },
                    change: (el, v) => {
                      adjustWindow.down("[name=dst_currency]").setValue(v);

                      let merchantAccountCurrency = adjustWindow
                        .down("[name=src_currency]")
                        .getValue();
                      if (merchantAccountCurrency === v) {
                        adjustWindow
                          .down("[name=conversion_rate]")
                          .setHidden(true)
                          .setConfig("allowBlank", true);
                        adjustWindow
                          .down("[name=conversion_rate_display]")
                          .setHidden(true);
                        adjustWindow
                          .down("[name=markup_rate]")
                          .setHidden(true)
                          .setConfig("allowBlank", true);
                        adjustWindow
                          .down("[name=markup_rate_value]")
                          .setHidden(true);
                      } else {
                        adjustWindow
                          .down("[name=conversion_rate]")
                          .setHidden(false)
                          .setConfig("allowBlank", false);
                        adjustWindow
                          .down("[name=conversion_rate_display]")
                          .setHidden(false)
                          .setStyle(
                            "display: flex;width: 70%;flex-direction: row-reverse;"
                          );
                        adjustWindow
                          .down("[name=markup_rate]")
                          .setHidden(false)
                          .setConfig("allowBlank", false);
                        adjustWindow
                          .down("[name=markup_rate_value]")
                          .setHidden(false);
                      }
                    }
                  }
                },
                {
                  name: "exchange_account_id",
                  fieldLabel: D.t("Select Exchange Account:"),
                  xtype: "combo",
                  hidden: true,
                  editable: true,
                  allowNull: false,
                  allowBlank: false,
                  validateBlank: true,
                  validateOnChange: true,
                  forceSelection: true,
                  queryMode: "all",
                  displayField: "account_name",
                  flex: 1,
                  valueField: "id",
                  tpl:
                    '<tpl for="."><div class="x-boundlist-item" style="height:40px;"><b>{account_name}</b>&nbsp;&nbsp;(Exchange name: {bank_name})&nbsp;&nbsp;(status: {status})&nbsp;</div><div style="clear:both"></div></tpl>',
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    },
                    change: async (el, v) => {
                      let currency = adjustWindow
                        .down("[name=src_currency]")
                        .getValue();

                      let errMsg;
                      let exchange_account_id = adjustWindow.down(
                        "[name=exchange_account_id]"
                      );
                      if (
                        el.selection &&
                        el.selection.data &&
                        el.selection.data.status &&
                        el.selection.data.status === "ACTIVE"
                      ) {
                        exchange_account_id.clearInvalid();
                        exchange_account_id.validator = function() {
                          return true;
                        };
                      } else {
                        errMsg = `Pick a active exchange account `;
                        exchange_account_id.markInvalid(errMsg);
                        exchange_account_id.validator = function() {
                          return errMsg;
                        };
                      }
                      if (
                        !errMsg &&
                        v &&
                        el.displayTplData &&
                        el.displayTplData.length &&
                        el.displayTplData[0].id === v
                      ) {
                        await this.model.runOnServer(
                          "checkExchangeBankAccountForCurrency",
                          {
                            currency,
                            bank_id: el.displayTplData[0].bank_id
                          },
                          function(res) {
                            if (res && res.length) {
                              el.validator = function(value) {
                                return true;
                              };
                            } else {
                              el.validator = function(value) {
                                return `please add a bank account of the type of ${currency} under ${el.displayTplData[0].bank_name} bank`;
                              };
                            }
                            el.isValid();
                          }
                        );
                      }
                    }
                  }
                },
                {
                  flex: 1,
                  xtype: "displayfield",
                  style:
                    "display: flex;width: 70%;flex-direction: row-reverse;",
                  name: "conversion_rate_display",
                  fieldLabel: " ",
                  allowBlank: true,
                  readOnly: true,
                  hidden: true,
                  value:
                    `Please check conversion rate on <a href="` +
                    __CONFIG__.WazirxUrl +
                    `" target="_blank" >` +
                    __CONFIG__.WazirxUrl +
                    `</a>`
                },
                {
                  flex: 1,
                  fieldLabel: "Conversion Rate:",
                  name: "conversion_rate",
                  allowBlank: false,
                  validateBlank: true,
                  hidden: true,
                  xtype: "numberfield",
                  minValue: 0,
                  decimalPrecision: 6,
                  submitEmptyText: false,
                  listeners: {
                    afterrender(e, eOpts) {
                      __CONFIG__.setComponentEmptyText(e, eOpts);
                    },
                    change: (el, v) => {
                      me.calculateConversionRate(v, adjustWindow);
                    }
                  }
                },

                {
                  flex: 1,
                  xtype: "displayfield",
                  name: "conversion_rate_value",
                  fieldLabel: "Conversion Total:",
                  allowBlank: true,
                  readOnly: true,
                  hidden: true
                },
                {
                  flex: 1,
                  fieldLabel: "Markup Rate(0-100):",
                  name: "markup_rate",
                  value: 0,
                  hidden: true,
                  xtype: "numberfield",
                  minValue: 0,
                  maxValue: 100,
                  submitEmptyText: false,
                  listeners: {
                    afterrender(e, eOpts) {
                      __CONFIG__.setComponentEmptyText(e, eOpts);
                    },
                    change: (el, v) => {
                      me.calculateMarkupRate(v, adjustWindow);
                    }
                  }
                },
                {
                  flex: 1,
                  xtype: "displayfield",
                  name: "markup_rate_value",
                  fieldLabel: "Markup Rate Calculation:",
                  allowBlank: true,
                  readOnly: true,
                  hidden: true
                },
                {
                  flex: 1,
                  xtype: "displayfield",
                  name: "final_calculation",
                  fieldLabel: "Final Settlement Amount:",
                  allowBlank: true,
                  readOnly: true,
                  hidden: true
                },
                {
                  flex: 1,
                  name: "dst_amount",

                  allowBlank: true,
                  readOnly: true,
                  hidden: true
                },
                {
                  flex: 1,
                  name: "src_currency",

                  allowBlank: true,
                  readOnly: true,
                  hidden: true
                },
                {
                  flex: 1,
                  name: "merchant_account_id",

                  allowBlank: true,
                  readOnly: true,
                  hidden: true
                },

                this.settlement_data(formData)
              ],
              listeners: {
                activate: function(context, eOpts) {},
                validitychange: function(context, valid, eOpts) {}
              }
            }
          ],
          renderTo: Ext.getBody()
        }
      ],
      listeners: {
        resize: function(context, width, height, eOpts) {}
      }
    }).show();
  },

  syncSize: function(c) {
    var width = Ext.Element.getViewportWidth(),
      height = Ext.Element.getViewportHeight();
    c.setHeight(Math.floor(height * 0.9));
  },

  gridCheck: function(adjustWindow, eOpts) {
    Ext.GlobalEvents.fireEvent(`${eOpts.name}ValidCheck`);
  },

  checkGridValidity: function(adjustWindow, eOpts) {
    var me = this;
    var nBtn = adjustWindow.down("[id=move-next]"),
      currentGrid = adjustWindow.down(`[name=${eOpts.name}]`);
    if (nBtn) {
      if (
        currentGrid &&
        currentGrid.store &&
        currentGrid.store.getRange() &&
        currentGrid.store.getRange().length
      ) {
        nBtn.setDisabled(false);
        return false;
      } else if (nBtn) {
        nBtn.setDisabled(true);
        return true;
      }
    }
    return false;
  },
  checkSetButtons: function(adjustWindow, valid, name = "form") {
    var nBtn = adjustWindow.down("[id=move-next]"),
      currentForm = adjustWindow.down(name);
    if (nBtn) {
      if (valid == undefined && currentForm && currentForm.isValid) {
        valid = currentForm.isValid();
      }
      if (valid == false) {
        nBtn.setDisabled(true);
        return true;
      } else {
        nBtn.setDisabled(false);
        return false;
      }
    }
    return false;
  },
  calculateConversionRate: function(v, adjustWindow) {
    var me = this;
    if (v) {
      let merchantAccountCurrency = adjustWindow
        .down("[name=src_currency]")
        .getValue();
      let merchantAccountSettlementCurrency = adjustWindow
        .down("[name=dst_currency]")
        .getValue();
      if (merchantAccountCurrency !== merchantAccountSettlementCurrency) {
        const settlementAmount = adjustWindow
          .down("[name=src_amount]")
          .getValue();
        conversion_amount = settlementAmount * v;
        const settlementCurrency = adjustWindow
          .down("[name=dst_currency]")
          .getValue();
        conversion_amount = __CONFIG__.formatCurrency(
          conversion_amount,
          0,
          settlementCurrency
        );
        adjustWindow
          .down("[name=conversion_rate_value]")
          .setHidden(false)
          .setValue(conversion_amount + "(" + settlementCurrency + ")");
      }
      me.calculateMarkupRate(v, adjustWindow);
    } else {
      adjustWindow
        .down("[name=conversion_rate_value]")
        .setHidden(true)
        .setValue(0);
      conversion_amount = 0;
    }
    let value = adjustWindow.down("[name=markup_rate]").getValue();
    me.calculateMarkupRate(value, adjustWindow);
  },
  calculateMarkupRate: function(v, adjustWindow) {
    if (v) {
      let markupTotal, settlementAmount, val;
      if (conversion_amount > 0) {
        markupTotal = __CONFIG__.calculateMarkupRate(conversion_amount, v);

        val = conversion_amount;
      } else {
        settlementAmount = adjustWindow.down("[name=src_amount]").getValue();

        markupTotal = __CONFIG__.calculateMarkupRate(settlementAmount, v);
        val = settlementAmount;
      }

      const settlementCurrency = adjustWindow
        .down("[name=dst_currency]")
        .getValue();
      const sourceCurrency = adjustWindow
        .down("[name=src_currency]")
        .getValue();
      markupTotal = __CONFIG__.formatCurrency(
        markupTotal,
        0,
        settlementCurrency
      );
      val = __CONFIG__.formatCurrency(val, 0, settlementCurrency);
      let msg =
        "Calculated markup percentage value is " +
        markupTotal +
        "(" +
        settlementCurrency +
        "), this will be subtracted from total of " +
        val +
        "(" +
        settlementCurrency +
        ")";
      adjustWindow
        .down("[name=markup_rate_value]")
        .setHidden(false)
        .setValue(msg);
      let final = val - markupTotal;
      adjustWindow
        .down("[name=final_calculation]")
        .setHidden(false)
        .setValue(final + "(" + settlementCurrency + ")");
      adjustWindow.down("[name=dst_amount]").setValue(final);
    } else {
      let settlementAmount = adjustWindow.down("[name=src_amount]").getValue();
      adjustWindow
        .down("[name=markup_rate_value]")
        .setHidden(true)
        .setValue(0);
      adjustWindow
        .down("[name=final_calculation]")
        .setHidden(true)
        .setValue(0);
      adjustWindow.down("[name=dst_amount]").setValue(settlementAmount);
    }
  },
  navigate: async function(adjustWindow, panel, direction) {
    var me = this;
    var layout = panel.getLayout(),
      currentForm,
      currentFormContext,
      currentFormVals;
    //Vaibhav Vaidya, Note: Based on Panel inputs, check their input first then proceed
    if (layout && layout.activeItem && layout.activeItem.id) {
      currentForm = layout.activeItem.id;
      currentFormContext = layout.activeItem.form;
      currentFormVals = layout.activeItem.form.getValues();
    }
    let panel1Str = `Step 1: Settlement Basic Info`;
    let panel2Str = `Step 2: Select Bank Account`;
    let panel3Str = `Step 3: Set Rate`;
    let submitStr = `Submit Settlement Request`;
    switch (currentForm) {
      //Basic info
      case "account_1":
        {
          if (direction == "next") {
            let formCheckResp,
              basicInfo = panel.down("[id=account_1]"),
              // exchangeRates = panel.down("[id=account_3]"),
              formVals = { basic_info: {} };
            let account_2 = panel.down("[id=account_2]");
            if (basicInfo && basicInfo.form) {
              formVals.basic_info = basicInfo.form.getValues();
              account_2.form.setValues(formVals.basic_info);
              let label =
                "Settlement Amount (" + formVals.basic_info.src_currency + "):";

              let a = account_2.down("[name=settlement_data]").store.getData()
                .items;
              if (
                (a && !a.length) ||
                (a &&
                  a.length &&
                  a[0].data.merchant_account_id !==
                    formVals.basic_info.merchant_account_id)
              ) {
                await this.model.runOnServer(
                  "fetchSettlementBankAmountByMerchantId",
                  {
                    merchant_account_id:
                      formVals.basic_info.merchant_account_id,
                    currency: formVals.basic_info.src_currency
                  },
                  function(res) {
                    if (res && res.length) {
                      account_2
                        .down("[name=settlement_data]")
                        .store.loadData([]);
                      account_2.down("[name=src_amount]").setValue(0);
                      account_2
                        .down("[name=src_amount_display]")
                        .setValue(`0 (${formVals.basic_info.src_currency})`);
                      a = [];
                      let src_amount = 0;

                      for (let currentBA of res) {
                        currentBA.amount = currentBA.max_amount_for_settlement;
                        src_amount += currentBA.max_amount_for_settlement;
                        a.push(currentBA);
                      }
                      src_amount = __CONFIG__.formatCurrency(
                        src_amount,
                        0,
                        formVals.basic_info.src_currency
                      );
                      account_2
                        .down("[name=settlement_data]")
                        .store.loadData(a, true);
                      account_2.down("[name=src_amount]").setValue(src_amount);
                      account_2.down("[name=dst_amount]").setValue(src_amount);
                      account_2
                        .down("[name=src_amount_display]")
                        .setValue(
                          `${src_amount} (${formVals.basic_info.src_currency})`
                        );
                    }
                  }
                );
              }
              account_2
                .down("[name=src_amount]")
                .setConfig("fieldLabel", label);
              let convRate = account_2.down("[name=conversion_rate]"),
                markupRate = account_2.down("[name=markup_rate]"),
                exchangeAccId = account_2.down("[name=exchange_account_id]"),
                settlementGrid = account_2.down("[name=settlement_data]");

              if (
                formVals &&
                formVals.basic_info &&
                formVals.basic_info.src_currency ===
                  formVals.basic_info.dst_currency
              ) {
                //Same Currency Block
                convRate
                  .setHidden(true)
                  .setValue(0)
                  .setConfig("allowBlank", true);

                markupRate
                  .setHidden(true)
                  .setValue(0)
                  .setConfig("allowBlank", true);
                exchangeAccId.setHidden(true).setConfig("allowBlank", true);
              } else {
                //Different Currency Block
                const proxyFilters = {
                  filters: [
                    {
                      _property: "currency",
                      _value: formVals.basic_info.dst_currency,
                      _operator: "eq"
                    },
                    {
                      _property: "bank_type",
                      _value: 1,
                      _operator: "eq"
                    }
                  ]
                };
                const store = Ext.create("Core.data.ComboStore", {
                  dataModel: Ext.create(
                    "Crm.modules.accountsPool.model.ExchangeAccountsModel"
                  ),
                  fieldSet: [
                    "id",
                    "account_name",
                    "bank_name",
                    "bank_id",
                    "status"
                  ],
                  exProxyParams: proxyFilters,
                  scope: this
                });
                convRate.setHidden(false).setConfig("allowBlank", false);
                markupRate.setHidden(false).setConfig("allowBlank", false);
                exchangeAccId
                  .setHidden(false)
                  .setConfig("allowBlank", false)
                  .setStore(store);
              }
            }
            //Vaibhav Vaidya, 28 April 2023, Add listeners
            me.addFinalValidateListeners(adjustWindow, panel, "on");
            layout[direction]();
            let bank_amount = account_2.down("[name=src_amount]").getValue();
            let nextBtn = Ext.getCmp("move-next"),
              prevBtn = Ext.getCmp("move-prev");
            if (nextBtn) {
              nextBtn.setHidden(false).setText(submitStr);
            }

            if (prevBtn)
              prevBtn
                .setDisabled(false)
                .setHidden(false)
                .setText(panel1Str);
          } else {
            me.addFinalValidateListeners(adjustWindow, panel, "un");
          }
        }
        break;

      //Merchant Accounts
      case "account_2":
        {
          if (direction == "next") {
            let basicInfo = panel.down("[id=account_1]"),
              bank_amount = panel.down("[id=account_2]"),
              formVals = { basic_info: {} };

            if (basicInfo && basicInfo.form) {
              formVals.basic_info = basicInfo.form.getValues();
            }

            // var exchangeRates = panel.down("[id=account_2]");
            if (bank_amount && bank_amount.form) {
              Object.assign(
                formVals.basic_info,
                bank_amount.form.getValues()
                // exchangeRates.form.getValues()
              );
              // formVals = [exchangeRates.form.getValues()];
            }
            let createResp = await me
              .createNewSettlements(formVals)
              .then(function(val) {
                adjustWindow.close();
                if (!!val.result) {
                  msg = "Settlement Creation successful.";
                  if (val.result.status != undefined)
                    msg += `<br> Merchant Status: Inprogress <br>`;
                  D.a("Settlement Creation successful.", msg, [], () => {});
                }
                me.view.store.reload();
              })
              .catch(function(val) {
                Ext.getCmp("move-next").setDisabled(true);
                msg =
                  "Problem during settlement creation, Contact support team if error persists";
                if (val && val.message) msg = val.message;
                else if (val && val.error && val.error.message)
                  msg = val.error.message;
                D.a("Settlement creation failure", msg);
              });
          } else {
            layout[direction]();

            Ext.getCmp("move-next").setText(panel2Str);
            Ext.getCmp("move-prev").setHidden(true);
          }
        }
        break;
      default:
        {
          D.a(
            "Invalid Form Selected",
            "Please close form and restart the process. Contact Support Team incase problem persists."
          );
        }
        break;
    }
  },

  uuidv4: function() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
      (
        c ^
        (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
      ).toString(16)
    );
  },
  startLoadingAnim: function() {
    Ext.MessageBox.wait("Processing...");
  },
  stopLoadingAnim: function() {
    Ext.MessageBox.hide();
  },
  createNewSettlements: function(formVals) {
    var me = this,
      funcName = "createNewSettlement";
    var createPromise = new Promise(function(resolve, reject) {
      me.startLoadingAnim();
      me.model.runOnServer(funcName, formVals, function(respData) {
        me.stopLoadingAnim();
        if (respData && respData.actionStatus == true) {
          resolve(respData);
        } else {
          reject(respData);
        }
      });
    });
    return createPromise;
  },

  addFinalValidateListeners: function(adjustWindow, panel, listenerUpd) {
    var me = this;
    let nextBtn = Ext.getCmp("move-next"),
      prevBtn = Ext.getCmp("move-prev");
    let account_2 = panel.down("[id=account_2]");
    if (account_2) {
      //UI Selection
      let convRate = account_2.down("[name=conversion_rate]"),
        markupRate = account_2.down("[name=markup_rate]"),
        exchangeAccId = account_2.down("[name=exchange_account_id]"),
        settlementGrid = account_2.down("[name=settlement_data]"),
        settlementStore;
      if (settlementGrid) {
        settlementStore = settlementGrid.store;
      }

      //Function for checking of validity
      let checkFinalSubmit = function() {
        let checkArr = [convRate, markupRate, exchangeAccId, settlementStore];
        let disableValidityFlag = false;
        for (let context of checkArr) {
          if (
            context &&
            context.allowBlank == false &&
            context.validate() == false
          ) {
            disableValidityFlag = true;
          } else if (context && context.data && context.data.length <= 0) {
            disableValidityFlag = true;
          }
        }
        nextBtn.setDisabled(disableValidityFlag);
      };

      //Add or Removing of listeners
      convRate[listenerUpd]("change", checkFinalSubmit);
      markupRate[listenerUpd]("change", checkFinalSubmit);
      exchangeAccId[listenerUpd]("change", checkFinalSubmit);
      if (settlementGrid && settlementGrid.store)
        settlementGrid.store[listenerUpd]("datachanged", checkFinalSubmit);
    }
  },

  settlement_data: function(formData) {
    const me = this;
    return {
      xtype: "gridfield",
      hideLabel: true,
      region: "center",
      autoScroll: true,
      scrollable: true,
      name: "settlement_data",
      layout: "fit",
      minHeight: 500,
      maxHeight: 501,
      fields: [
        "bank_name",
        "bank_account_id",
        "acc_no",
        "max_amount_for_settlement",
        "amount"
      ],
      buildTbar() {
        return [
          {
            tooltip: "Reset list",
            iconCls: "x-fa fa-refresh",
            activity: "refresh",
            handler: async function() {
              let obj = Ext.getCmp("move-next")
                .up("panel")
                .down("[id=account_2]");
              let currency = obj.down("[name=src_currency]").getValue();
              let merchant_account_id = obj
                .down("[name=merchant_account_id]")
                .getValue();
              await me.model.runOnServer(
                "fetchSettlementBankAmountByMerchantId",
                {
                  merchant_account_id,
                  currency
                },
                function(res) {
                  if (res && res.length) {
                    obj.down("[name=settlement_data]").store.loadData([]);
                    obj.down("[name=src_amount]").setValue(0);
                    obj
                      .down("[name=src_amount_display]")
                      .setValue(`0 (${currency})`);
                    a = [];
                    let src_amount = 0;

                    for (let currentBA of res) {
                      currentBA.amount = currentBA.max_amount_for_settlement;
                      src_amount += currentBA.max_amount_for_settlement;
                      a.push(currentBA);
                    }
                    src_amount = __CONFIG__.formatCurrency(
                      src_amount,
                      0,
                      currency
                    );
                    obj.down("[name=settlement_data]").store.loadData(a, true);
                    obj.down("[name=src_amount]").setValue(src_amount);
                    obj.down("[name=dst_amount]").setValue(src_amount);
                    obj
                      .down("[name=src_amount_display]")
                      .setValue(`${src_amount} (${currency})`);
                  }
                }
              );
            }
          }
        ];
      },
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        },

        change: function(e, newVal, oldVal, eOpts) {
          let data = e.store.getRange().map((e) => e.data);
          let amount = 0;
          for (let i = 0; i < data.length; i++) {
            const el = data[i];

            if (
              el.bank_account_id === newVal[i].bank_account_id &&
              el.max_amount_for_settlement < newVal[i].amount
            ) {
              Ext.getCmp("move-next").setDisabled(true);

              Ext.Msg.alert(
                "Error",
                "Amount for settlement must be less than or equal to max amount for settlement"
              );
              newVal[i].amount = el.max_amount_for_settlement;
              amount = parseFloat(amount);
              amount += parseFloat(el.max_amount_for_settlement);
              amount = __CONFIG__.formatCurrency(amount, 0, newVal[i].currency);
              e.store.loadData(newVal, true);
            } else {
              amount = parseFloat(amount);
              amount += parseFloat(newVal[i].amount);
              amount = __CONFIG__.formatCurrency(amount, 0, newVal[i].currency);
            }
          }
          let obj = Ext.getCmp("move-next")
            .up("panel")
            .down("[id=account_2]");

          obj.down("[name=src_amount]").setValue(amount);
          let currency = obj.down("[name=src_currency]").getValue();
          obj
            .down("[name=src_amount_display]")
            .setValue(`${amount} (${currency})`);

          if (amount > 0) Ext.getCmp("move-next").setDisabled(false);
          else Ext.getCmp("move-next").setDisabled(true);
        }
      },
      columns: [
        {
          text: D.t("Bank"),
          flex: 1,
          sortable: true,
          dataIndex: "bank_name",
          menuDisabled: false,
          editor: false
        },
        {
          text: D.t("Bank Account"),
          flex: 1,
          sortable: true,
          name: "bank_account_id",
          dataIndex: "bank_account_id",
          menuDisabled: false,
          forceSelection: true,
          allowNull: false,
          guideKeyField: "id",
          guideValueField: "account_name",
          xtype: "combocolumn",
          model: "Crm.modules.accountsPool.model.AssignedAccountsModel",
          editor: false,
          renderer: function(v, m, r) {
            if (r && r.data && r.data.account_name != undefined) {
              return r.data.account_name;
            }
            return v;
          }
        },
        {
          text: D.t("Account Number"),
          flex: 1,
          sortable: true,
          dataIndex: "acc_no",
          menuDisabled: false,
          editor: false
        },
        {
          text: D.t("Max Amount For Settlement"),
          flex: 1,
          sortable: true,
          dataIndex: "max_amount_for_settlement",
          menuDisabled: false,
          editor: false,
          renderer: function(v, m, r) {
            if (v) {
              return __CONFIG__.formatCurrency(v, 0, r.data.currency);
            }
            return v;
          }
        },
        {
          text: D.t("Amount"),
          flex: 1,
          sortable: true,
          dataIndex: "amount",
          menuDisabled: false,
          editor: {
            xtype: "numberfield",
            allowBlank: false,
            minValue: 0
          },
          renderer: function(v, m, r) {
            if (v) {
              return __CONFIG__.formatCurrency(v, 0, r.data.currency);
            }
            return v;
          }
        }
      ]
    };
  }
});
