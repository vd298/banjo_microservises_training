Ext.define("Crm.modules.settlements.model.SettlementsModel", {
  extend: "Crm.classes.DataModel",

  collection: "settlements",
  idField: "id",
  //removeAction: "remove",
  foreignKeyFilter: [
    {
      collection: "merchant_accounts",
      alias: "ma",
      on: " merchant_accounts.id = settlements.merchant_account_id ",
      type: "inner"
    },
    {
      collection: "accounts",
      alias: "a",
      on: " accounts.id = merchant_accounts.account_id ",
      type: "inner"
    }
  ],
  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "status",
      type: "integer",
      filterable: false,
      editable: true,
      visible: true
    },
    {
      name: "src_currency",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_currency",

      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_amount",
      type: "double",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_amount",
      type: "double",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "markup_rate",
      type: "double",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "conversion_rate",
      type: "double",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "merchant_account_id",
      type: "ObjectID",
      visible: true,
      editable: true
    },
    {
      name: "exchange_account_id",
      type: "ObjectID",
      visible: true,
      editable: true
    },
    {
      name: "settlement_data",
      type: "string",
      visible: true,
      editable: true
    },
    {
      name: "ctime",
      type: "date",
      // sort: -1,
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  /* scope:server */
  async afterGetData(data, cb) {
    var me = this;
    const Util = Ext.create("Crm.Util", { scope: me });
    if (!!data && data.length) {
      let merchant_account_id = [];
      data.map((item) => {
        item.id && merchant_account_id.push(`'${item.id}'`);
      });

      let sqlPlaceHolders = [];
      const resp = await this.src.db.query(
        `SELECT s.*,ma.name as merchant_account_name FROM settlements s join merchant_accounts ma ON ma.id = s.merchant_account_id AND ma.removed = 0 WHERE ${Util.pushArray(
          data.map((e) => {
            if (typeof e.id == "string" && e.id.length) {
              return e.id;
            } else return null;
          }),
          sqlPlaceHolders,
          { colName: "s.id" }
        )}  ORDER BY s.mtime DESC`,
        sqlPlaceHolders
      );
      cb(resp);
    }
    return cb(data);
  },

  /* scope:server */
  async $fetchSettlementCurrencyId(data, callbackFunction) {
    try {
      var me = this;
      const Util = Ext.create("Crm.Util", { scope: me });
      if (!data.merchant_account_id) {
        return callbackFunction([]);
      } else if (
        data &&
        typeof data.merchant_account_id == "string" &&
        data.merchant_account_id.length == 36
      ) {
        let sql = `select ma.*,c.name as currency_name from merchant_accounts ma inner join currency c on c.abbr = ma.currency  where ma.removed=0 and ma.id = $1`;
        // sql = await this.buildRealmConditionQuery(sql, "ma.realm_id");
        const merchantAccounts = await this.src.db.query(sql, [
          data.merchant_account_id
        ]);
        if (merchantAccounts && merchantAccounts.length) {
          const wallets = await this.callApi({
            service: "account-service",
            method: "fetchMerchantAccountWalletBalance",
            data: {
              src_currency: merchantAccounts[0].currency,
              merchant_account_id: data.merchant_account_id,
              w_type: "COLLECTION"
            },
            options: {
              realmId: me.user.profile.realm_id
            }
          });
          if (
            wallets &&
            wallets.result &&
            wallets.result.total_available_balance
          ) {
            merchantAccounts[0].total_available_balance =
              wallets.result.total_available_balance;
          } else {
            merchantAccounts[0].total_available_balance = 0;
          }
        }

        if (merchantAccounts && merchantAccounts.length) {
          return callbackFunction(merchantAccounts);
        } else {
          return callbackFunction([]);
        }
      } else {
        return callbackFunction([]);
      }
    } catch (err) {
      console.error(
        `Func:fetchSettlementCurrencyId, File: SettlementsModel.js, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      callbackFunction(err);
    }
  },

  /* scope:server */
  async $createNewSettlement(data, cb) {
    try {
      var me = this;
      console.log(
        `File:SettlementModel.js Func:$createNewSettlement. Making API call to createNewSettlement`,
        data
      );
      var res = await this.callApi({
        service: "account-service",
        method: "createNewSettlement",
        data: {
          ...data.basic_info,
          action_type: "ADMIN",
          admin_id: me.user.id || ""
        },
        writeFlag: true,
        options: {
          realmId: me.user.profile.realm_id
        }
      });
      console.log(
        `File:SettlementModel.js Func:$createMerchantAccount.Received Response`
      );
      if (res && typeof res == "object") {
        console.log(JSON.stringify(res));
      }
      if (
        res &&
        res.result &&
        res.result.message == "Settlement created successfully"
      ) {
        res.actionStatus = true;
        this.changeModelData(
          "Crm.modules.Transfers.model.TransfersModel",
          "ins",
          {}
        );
      } else {
        res.actionStatus = false;
      }
      return cb(res);
    } catch (err) {
      console.error(
        `SettlementModel.js Func:performMerchantCreate, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      return cb(err);
    }
  },

  /* scope:server */
  async $fetchSettlementBankAmountByMerchantId(data, cb) {
    try {
      var me = this;
      console.log(
        `File:SettlementModel.js Func:$fetchSettlementBankAmountByMerchantId. Making API call to fetchSettlementBankAmountByMerchantId`,
        data
      );
      let sql = `SELECT coalesce(sum(w.balance),0) as max_amount_for_settlement,w.bank_account_id,ma.merchant_account_id,ba.acc_no,b.name as bank_name, w.currency,ba.account_name FROM  wallets w 
      join merchant_account_mappings ma on ma.id = w.merchant_account_mapping_id and ma.merchant_account_id = $1 
      join bank_accounts ba on ba.id = w.bank_account_id
      join banks b on b.id = ba.bank_id 
      where w.removed = 0 and w.type = 'COLLECTION' and w.currency= $2 and w.balance > 0 `;
      sql = await this.buildRealmConditionQuery(sql, "b.realm_id");
      sql += ` GROUP BY w.bank_account_id,ba.account_name, ma.merchant_account_id,ba.acc_no,b.name,w.currency `;
      const res = await this.src.db.query(sql, [
        data.merchant_account_id,
        data.currency
      ]);
      return cb(res);
    } catch (err) {
      console.error(
        `SettlementModel.js Func:fetchSettlementBankAmountByMerchantId, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      return cb(err);
    }
  },

  /* scope:server */
  async $checkExchangeBankAccountForCurrency(data, cb) {
    try {
      var me = this;
      console.log(
        `File:SettlementModel.js Func:$checkExchangeBankAccountForCurrency.`,
        data
      );
      if (data.currency) {
        let parameter = [data.currency];
        let query = `SELECT ba.id FROM bank_accounts ba join banks b ON b.id = ba.bank_id AND b.removed = 0 AND bank_type = 1 where ba.removed = 0  and ba.currency= $1`;
        if (data.bank_id) {
          parameter.push(data.bank_id);
          query += ` and ba.bank_id=$2`;
        }
        query = await this.buildRealmConditionQuery(query, "b.realm_id");
        const res = await this.src.db.query(query, parameter);
        return cb(res);
      }
      return cb([]);
    } catch (err) {
      console.error(
        `SettlementModel.js Func:checkExchangeBankAccountForCurrency, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      return cb(err);
    }
  }
});
