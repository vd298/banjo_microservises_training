Ext.define(
  "Crm.modules.CreationMerchantAccounts.view.CreationMerchantAccountsGridController",
  {
    extend: "Core.grid.GridController",

    setControls: function() {
      var me = this;
      this.control({
        "[action=customAccAdd]": {
          click: function(el) {
            me.addRecord();
          }
        },
        "[action=refresh]": {
          click: function(el) {
            me.reloadData();
          }
        },
        grid: {
          cellkeydown: function(cell, td, i, rec, tr, rowIndex, e, eOpts) {
            if (e.keyCode == 13) {
              //me.gotoRecordHash(rec.data);
            }
          },
          celldblclick: function(cell, td, i, rec) {
            //me.gotoRecordHash(rec.data);
          },
          itemcontextmenu: function(vw, record, item, index, e, options) {
            e.stopEvent();
          }
        }
      });
      this.view.on("activate", function(grid, indx) {
        if (!me.view.observeObject)
          document.title = me.view.title + " " + D.t("ConsoleTitle");
      });
      this.view.on("edit", function(grid, indx) {
        //me.gotoRecordHash(grid.getStore().getAt(indx).data);
      });
      this.view.on("delete", function(grid, indx) {
        me.deleteRecord(grid.getStore(), indx);
      });
      // Ext.on("CreationMerchantAccountsGridValidCheck", function() {
      //   var merchantAccGrid = me.view.gridComponent.store;
      //   var creationForm = me.view.gridComponent.up("[name=mainCreatePanel]");
      //   let storeLength = merchantAccGrid.getRange().length;
      //   if (storeLength) {
      //     creationForm.down("[id=move-next]").setDisabled(false);
      //   } else {
      //     creationForm.down("[id=move-next]").setDisabled(true);
      //   }
      // });
      this.initButtonsByPermissions();
    },

    gotoRecordHash: function(data) {
      var me = this;
      if (!!this.view.observeObject) {
        window.__CB_REC__ = this.view.observeObject;
      }
      if (data && data[this.view.model.idField]) {
        var hash =
          this.generateDetailsCls() + "~" + data[this.view.model.idField];
        if (this.view.detailsInDialogWindow) {
          Ext.create(this.generateDetailsCls(), {
            noHash: true,
            recordId: data[this.view.model.idField],
            gridComponent: me.view,
            modal: true
          });
        } else if (this.view.detailsInNewWindow) window.open("./#" + hash);
        else location.hash = hash;
      }
    },

    deleteRecord_do: function(store, rec) {
      var me = this;
      D.c("Removing", "Delete the record?", [], function() {
        store.remove(rec);
        var merchantAccGrid = me.view.store;
        var creationForm = me.view.up("[name=mainCreatePanel]");
        let storeLength = merchantAccGrid.getRange().length;
        if (storeLength) {
          creationForm.down("[id=move-next]").setDisabled(false);
        } else {
          creationForm.down("[id=move-next]").setDisabled(true);
        }
      });
    }
  }
);
