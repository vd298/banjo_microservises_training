Ext.define("Crm.modules.globalLimits.view.GlobalLimitForm", {
  extend: "Core.form.FormContainer",

  titleTpl: D.t("Account Limits Form"),
  requires: ["Desktop.core.widgets.GridField", "Core.grid.ComboColumn"],
  formLayout: "fit",

  formMargin: 0,
  width: 800,
  height: 550,

  buildItems: function() {
    return {
      xtype: "panel",
      layout: "anchor",
      defaults: {
        anchor: "100%",
        xtype: "textfield",
        labelWidth: 150,
        margin: 5
      },
      items: [
        {
          name: "id",
          hidden: true
        },
        {
          name: "bank_account_id",
          hidden: true
        },
        this.buildProtocolCombo(),
        this.buildCurrencyCombo(),
        this.buildLimitTypeCombo(),
        {
          name: "min_amount",
          fieldLabel: D.t("Minimum amount"),
          xtype: "numberfield",
          minValue: 0,
          readOnly: true,
          inputWrapCls: "",
          triggerWrapCls: "",
          fieldStyle: "background:none"
        },
        {
          name: "max_amount",
          fieldLabel: D.t("Maximum amount"),
          xtype: "numberfield",
          minValue: 1,
          maxValue: 1000000000,
          readOnly: true,
          inputWrapCls: "",
          triggerWrapCls: "",
          fieldStyle: "background:none"
        },
        {
          name: "daily_deposit",
          fieldLabel: D.t("Maximum Daily Deposit"),
          xtype: "numberfield",
          value: null,
          allowNull: true,
          maxValue: 1000000000,
          readOnly: true,
          inputWrapCls: "",
          triggerWrapCls: "",
          fieldStyle: "background:none"
        }
      ]
    };
  },
  buildCurrencyCombo() {
    return {
      name: "currency",
      fieldLabel: D.t("Currency"),
      xtype: "combo",
      editable: false,
      allowBlank: false,
      validateBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "all",
      displayField: "currencyDisplay",
      flex: 1,
      valueField: "abbr",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.bankAccounts.model.currencyCombo"),
        fieldSet: ["name", "abbr"],
        scope: this
      }),
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      },
      readOnly: true,
      inputWrapCls: "",
      triggerWrapCls: "",
      fieldStyle: "background:none"
    };
  },
  buildProtocolCombo() {
    return {
      name: "protocol_id",
      fieldLabel: D.t("Payment protocol"),
      xtype: "combo",
      editable: false,
      allowBlank: false,
      validateBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "all",
      displayField: "name",
      flex: 1,
      valueField: "_id",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create(
          "Crm.modules.bankAccounts.model.PaymentTypeCombo"
        ),
        fieldSet: ["_id", "name"],
        scope: this
      }),
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      },
      readOnly: true,
      inputWrapCls: "",
      triggerWrapCls: "",
      fieldStyle: "background:none"
    };
  },
  buildLimitTypeCombo() {
    return {
      name: "limit_type",
      fieldLabel: D.t("Limit type"),
      xtype: "combo",
      editable: false,
      allowBlank: false,
      validateBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "all",
      displayField: "name",
      flex: 1,
      valueField: "key",
      store: {
        fields: ["key", "name"],
        data: [
          { key: 0, name: D.t("Deposit") },
          { key: 1, name: D.t("Withdraw") }
        ]
      },
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      },
      readOnly: true,
      inputWrapCls: "",
      triggerWrapCls: "",
      fieldStyle: "background:none"
    };
  },
  buildButtons: function() {
    var me = this;
    var btns = [
      "->",
      {
        text: D.t("Close"),
        iconCls: "x-fa fa-times",
        scale: "medium",
        action: "gotolist",
        style: "background: #53b7f4; color: #fff;",
        cls: "white_button"
      }
    ];
    if (this.allowImportExport) {
      btns.splice(1, 0, "-", {
        text: D.t("Export/Import"),
        style:
          "background: #53b7f4; color: #fff; top: 0; height:32px; font-size:24px;",
        cls: "white_button",
        iconCls: "x-fa fa-caret-down",
        iconAlign: "right",
        arrowVisible: false,
        menu: [
          {
            text: D.t("Export to file"),
            xtype: "button",
            margin: 5,
            action: "exportjson"
          },
          {
            xtype: "filefield",
            buttonOnly: true,
            margin: 5,
            action: "importjson",
            buttonConfig: {
              text: D.t("Import from file"),
              width: "100%"
            }
          }
        ]
      });
    }

    return btns;
  }
});
