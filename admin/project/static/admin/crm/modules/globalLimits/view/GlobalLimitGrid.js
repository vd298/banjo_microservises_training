Ext.define("Crm.modules.globalLimits.view.GlobalLimitGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Global Limit Grid"),
  scrollable: true,
  filterable: true,
  filterbar: true,

  buildColumns: function() {
    return [
      {
        hidden: true,
        dataIndex: "id"
      },
      {
        text: D.t("Currency"),
        dataIndex: "currency",
        flex: 1,
        sortable: true,
        filter: true
      },
      {
        text: D.t("Protocol Type"),
        dataIndex: "protocol_type",
        flex: 1,
        sortable: true,
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "protocol_type",
          displayField: "protocol_type",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create(
              "Crm.modules.paymentProtocols.model.protocolTypeCombo"
            ),
            fieldSet: ["protocol_type"],
            scope: this
          }),
          operator: "eq"
        }
      },
      {
        text: D.t("Min amount"),
        dataIndex: "min_amount",
        flex: 1,
        sortable: true,
        filter: { xtype: "numberfield" },
        renderer: function(v, m, r) {
          if (v) {
            return __CONFIG__.formatCurrency(v, 0, r.data.currency);
          }
          return v;
        }
      },
      {
        text: D.t("Max amount"),
        dataIndex: "max_amount",
        flex: 1,
        sortable: true,
        filter: true,
        renderer: function(v, m, r) {
          if (v) {
            return __CONFIG__.formatCurrency(v, 0, r.data.currency);
          }
          return v;
        }
      },
      {
        text: D.t("Limit type"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "limit_type",
        filter: {
          xtype: "combo",
          valueField: "key",
          displayField: "name",
          store: {
            fields: ["key", "name"],
            data: [
              { key: 0, name: D.t("Deposit") },
              { key: 1, name: D.t("Withdraw") }
            ]
          }
        },
        renderer: function(v, m, r) {
          const types = {
            0: "Deposit",
            1: "Withdraw"
          };
          if (v == null) {
            return D.t("");
          }
          return D.t(types[v]);
        }
      },
      {
        text: D.t("Max Daily Limit"),
        dataIndex: "daily_deposit",
        flex: 1,
        sortable: true,
        filter: { xtype: "numberfield" }
      }
    ];
  },
  buildTbar() {
    return [
      {
        tooltip: "Reload list",
        iconCls: "x-fa fa-refresh",
        action: "refresh"
      }
    ];
  },
  buildButtonsColumns() {
    var me = this;
    return [];
  }
});
