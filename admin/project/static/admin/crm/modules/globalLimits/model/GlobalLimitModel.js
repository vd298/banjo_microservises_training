Ext.define("Crm.modules.globalLimits.model.GlobalLimitModel", {
  extend: "Crm.classes.DataModel",

  collection: "vw_bank_accounts_global_limits",
  idField: "id",
  foreignKeyFilter: [
    {
      collection: "ref_limits",
      alias: "rl",
      on: " vw_bank_accounts_global_limits.id = ref_limits.id ",
      type: "inner"
    }
  ],
  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true,
      editable: true
    },
    {
      name: "currency",
      type: "string",
      sort: 1,
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "protocol_type",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "min_amount",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "max_amount",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "daily_deposit",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "protocol_id",
      type: "ObjectId",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "limit_type",
      type: "integer",
      filterable: true,
      editable: true,
      visible: true
    }
  ],
  async getData(params, cb) {
    let filtersSql = ``,
      queryParams = [];
    if (params && params.filters) {
      for (let i = 0; i < params.filters.length; i++) {
        if (params.filters[i].property && params.filters[i].value) {
          let preFix = "rl.";
          if (["protocol_type"].indexOf(params.filters[i].property) > -1) {
            preFix = "pp.";
          }
          filtersSql += " and ";
          queryParams.push(params.filters[i].value);
          filtersSql += `${preFix}${params.filters[i].property}=$${queryParams.length}`;
        }
      }
    }
    let sql = `SELECT rl.*, pp.protocol_type from ref_limits rl
        left join payment_protocols pp on pp.id = rl.protocol_id and pp.removed = 0
        where rl.bank_account_protocol_id is null ${filtersSql}`;

    sql = await this.buildRealmConditionQuery(sql, "rl.realm_id");
    const res = await this.src.db.query(sql, queryParams, function(err, resp) {
      if (err) {
        console.error(
          "GlobalLimitModel.js. Func:getData. Database Error. Error",
          err
        );
        return cb({ total: 0, list: {} });
      }
      return cb({ total: resp.length, list: resp });
    });
  },

  /* scope:server */
  async write(data, cb) {
    const me = this;
    const params = [];
    let sql = "UPDATE ref_limits SET";

    if (data.min_amount !== undefined && data.min_amount != "") {
      params.push(data.min_amount);
      sql += ` min_amount = $${params.length} ,`;
    }
    if (data.max_amount !== undefined && data.max_amount != "") {
      params.push(data.max_amount);
      sql += ` max_amount = $${params.length} ,`;
    }

    if (data.limit_type !== undefined && data.limit_type != "") {
      params.push(data.limit_type);
      sql += ` limit_type = $${params.length} ,`;
    }

    if (params.length > 0) {
      sql = sql.slice(0, -1);
      params.push(data.id);
      sql += `WHERE id = $${params.length} returning id;`;
    } else {
      return cb({
        success: false,
        message: "Insufficient Data to update record"
      });
    }
    let res = await me.src.db.query(sql, params);
    if (res && res.length) {
      me.changeModelData(
        "Crm.modules.globalLimits.model.GlobalLimitModel",
        "ins",
        data
      );
      return cb(data);
    } else {
      return cb({ success: false, message: "Update failed" });
    }
  }
});
