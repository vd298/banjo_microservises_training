Ext.define("Crm.modules.bankAccounts.view.AccountLimitGridController", {
  extend: "Core.grid.GridController",

  setControls: function() {
    var me = this;
    this.control({
      "[action=refresh]": {
        click: function(el) {
          me.reloadData();
        }
      }
    });

    this.view.on("deleteAccountLimit", function(grid, index) {
      const me = this;
      Ext.Msg.confirm(
        D.t("Delete limit?"),
        D.t("Do you want to delete the limit?", []),
        function(btn) {
          if (btn == "yes") {
            let data = grid.eventPosition.record.data;
            me.model.deleteAccountLimit(data, function(res) {
              if (!res.result) {
                return Ext.Msg.alert("Error", res.message);
              }
            });
          }
        }
      );
    });

    this.callParent(arguments);
  }
});
