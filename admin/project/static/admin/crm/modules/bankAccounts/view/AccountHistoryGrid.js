Ext.define("Crm.modules.bankAccounts.view.AccountHistoryGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Account Allocation History"),
  controllerCls: "Crm.modules.bankAccounts.view.AccountHistoryGridController",

  filterable: true,
  filterbar: true,
  gridCfg: {
    viewConfig: {
      getRowClass: (record) => {
        if (record.data.active_relation) return "success";
      }
    }
  },
  buildColumns: function() {
    return [
      {
        hidden: true,
        dataIndex: "id"
      },
      {
        hidden: true,
        dataIndex: "active_relation"
      },
      {
        hidden: true,
        dataIndex: "merchant_name"
      },
      {
        hidden: true,
        dataIndex: "merchant_account_name"
      },
      {
        text: D.t("Associated Merchant"),
        flex: 1,
        sortable: true,
        dataIndex: "merchant_id",
        renderer: function(v, m, r) {
          if (v) {
            return (
              '<a href="#Crm.modules.bankAccounts.view.AccountsForm~' +
              v +
              '">' +
              r.data.merchant_name +
              "</a>"
            );
          }
          return v;
        },
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "id",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create("Crm.modules.accounts.model.AccountsModel"),
            fieldSet: ["id", "name"],
            scope: this
          }),
          operator: "eq"
        }
      },
      {
        text: D.t("Associated Merchant Account"),
        flex: 1,
        sortable: true,
        dataIndex: "merchant_account_id",
        renderer: function(v, m, r) {
          if (v) {
            return (
              '<a href="#Crm.modules.accounts.view.MerchantAccountsForm~' +
              v +
              '">' +
              r.data.merchant_account_name +
              "</a> "
            );
          }
          return v;
        },
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "id",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create(
              "Crm.modules.accounts.model.MerchantAccountsModel"
            ),
            fieldSet: ["id", "name"],
            scope: this
          }),
          operator: "eq"
        }
      },
      {
        xtype: "datecolumn",
        format: "d.m.Y H:i:s",
        filter: {
          xtype: "datefield",
          format: "d.m.Y"
        },
        text: D.t("From"),
        flex: 1,
        sortable: true,
        dataIndex: "start_time"
      },
      {
        xtype: "datecolumn",
        format: "d.m.Y H:i:s",
        filter: {
          xtype: "datefield",
          format: "d.m.Y"
        },
        text: D.t("To"),
        flex: 1,
        sortable: true,
        dataIndex: "end_time"
      }
    ];
  },

  buildTbar: function() {
    var items = [
      {
        tooltip: this.buttonReloadText,
        iconCls: "x-fa fa-refresh",
        action: "refresh"
      }
    ];

    if (this.filterable) items.push("->", this.buildSearchField());
    return items;
  }
});
