Ext.define("Crm.modules.bankAccounts.view.ActiveAllocationGridController", {
  extend: "Core.grid.GridController",
  setControls() {
    let me = this;

    this.control({
      "[action=add]": {
        click: function(el) {
          me.addRecord();
        }
      },
      "[action=refresh]": {
        click: function(el) {
          me.reloadData();
        }
      },
      "[action=import]": {
        click: function(el) {
          me.importData();
        }
      },
      "[action=export]": {
        click: function(el) {
          me.exportData();
        }
      },
      grid: {
        cellkeydown: function(cell, td, i, rec, tr, rowIndex, e, eOpts) {
          return;
        },
        celldblclick: function(cell, td, i, rec) {
          return;
        },
        itemcontextmenu: function(vw, record, item, index, e, options) {
          e.stopEvent();
        }
      }
    });
    this.view.on("activate", function(grid, indx) {
      if (!me.view.observeObject)
        document.title = me.view.title + " " + D.t("ConsoleTitle");
    });
    this.view.on("edit", function(grid, indx) {
      return;
    });
    this.view.on("delete", function(grid, indx) {
      me.deleteRecord(grid.getStore(), indx);
    });

    //yc274 temporary fix for dialog
    setTimeout(() => {
      me.reloadData();
    }, 500);
    //
  }
});
