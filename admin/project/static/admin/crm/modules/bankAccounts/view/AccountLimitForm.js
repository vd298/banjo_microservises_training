Ext.define("Crm.modules.bankAccounts.view.AccountLimitForm", {
  extend: "Core.form.FormContainer",

  titleTpl: D.t("Account Limits Form"),
  requires: ["Desktop.core.widgets.GridField", "Core.grid.ComboColumn"],
  formLayout: "fit",

  formMargin: 0,
  width: 800,
  height: 550,
  controllerCls: "Crm.modules.bankAccounts.view.AccountLimitFormController",

  buildItems: function() {
    return {
      xtype: "panel",
      layout: "anchor",
      defaults: {
        anchor: "100%",
        xtype: "textfield",
        labelWidth: 150,
        margin: 5
      },
      items: [
        {
          fieldLabel: D.t("Global Limit Flag"),
          name: "global_limit",
          hidden: true
        },
        {
          fieldLabel: D.t("Id"),
          name: "id",
          hidden: true
        },
        {
          fieldLabel: D.t("Bank Account Id"),
          name: "bank_account_id",
          hidden: true
        },
        this.buildProtocolCombo(),
        this.buildCurrencyCombo(),
        this.buildLimitTypeCombo(),
        {
          name: "min_amount",
          fieldLabel: D.t("Minimum amount"),
          xtype: "numberfield",
          minValue: 0,
          listeners: {
            change: (e, newVal, oldVal) => {
              const me = this;
              const formData = me.down("form").getValues();
              if (newVal >= parseFloat(formData.max_amount)) {
                e.validator = function(value) {
                  return "Minimum amount cannot be greater than maximum amount";
                };
              } else {
                e.validator = function(value) {
                  return true;
                };
              }
            }
          }
        },
        {
          name: "max_amount",
          fieldLabel: D.t("Maximum amount"),
          xtype: "numberfield",
          minValue: 1,
          maxValue: 1000000000,
          listeners: {
            change: (e, newVal, oldVal) => {
              const me = this;
              const formData = me.down("form").getValues();
              if (newVal <= parseFloat(formData.min_amount)) {
                e.validator = function(value) {
                  return "Maximum amount cannot be smaller than minimum amount";
                };
              } else {
                e.validator = function(value) {
                  return true;
                };
              }
            }
          }
        },
        {
          name: "daily_deposit",
          fieldLabel: D.t("Maximum Daily Deposit"),
          xtype: "numberfield",
          value: null,
          allowNull: false,
          minValue: 1,
          maxValue: 1000000000,
          listeners: {
            change: (e, newVal, oldVal) => {
              const me = this;
              const formData = me.down("form").getValues();
              if (newVal <= parseFloat(formData.min_amount)) {
                e.validator = function(value) {
                  return "Daily deposit amount cannot be smaller than minimum amount";
                };
              } else if (newVal < parseFloat(formData.max_amount)) {
                e.validator = function(value) {
                  return "Daily deposit amount cannot be smaller than maximum amount";
                };
              } else {
                e.validator = function(value) {
                  return true;
                };
              }
            }
          }
        }
      ]
    };
  },
  buildCurrencyCombo() {
    return {
      name: "currency",
      fieldLabel: D.t("Currency"),
      xtype: "combo",
      editable: false,
      allowBlank: false,
      validateBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "all",
      displayField: "currencyDisplay",
      flex: 1,
      valueField: "abbr",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.bankAccounts.model.currencyCombo"),
        fieldSet: ["name", "abbr"],
        scope: this
      }),
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      }
    };
  },
  buildProtocolCombo() {
    return {
      name: "payment_protocol_id",
      fieldLabel: D.t("Payment protocol"),
      xtype: "combo",
      editable: false,
      allowBlank: false,
      validateBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "all",
      displayField: "name",
      flex: 1,
      valueField: "_id",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create(
          "Crm.modules.bankAccounts.model.PaymentTypeCombo"
        ),
        fieldSet: ["_id", "name"],
        scope: this
      }),
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      }
    };
  },
  buildLimitTypeCombo() {
    return {
      name: "limit_type",
      fieldLabel: D.t("Limit type"),
      xtype: "combo",
      editable: false,
      allowBlank: false,
      validateBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "all",
      displayField: "name",
      flex: 1,
      valueField: "key",
      store: {
        fields: ["key", "name"],
        data: [
          { key: 0, name: D.t("Deposit") },
          { key: 1, name: D.t("Withdraw") }
        ]
      },
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      }
    };
  }
});
