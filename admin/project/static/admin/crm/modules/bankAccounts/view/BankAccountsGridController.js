Ext.define("Crm.modules.bankAccounts.view.BankAccountsGridController", {
  extend: "Core.grid.GridController",
  setControls() {
    let me = this;
    this.view.on("delete", function(grid, indx) {
      me.model.runOnServer(
        "checkAssociatedMerchantAccounts",
        { id: grid.getRecord(indx).id },
        (res) => {
          if (!!res && res.length) {
            D.a(
              "Delete Error",
              `Bank account cannot be deleted as it is assigned to ${res.length} merchant accounts. Please de-assign before proceeding.`,
              []
            );
          }
        }
      );
    });

    this.view.on("activate", async () => {
      await this.view.store.reload();
    });
    this.callParent(arguments);
  }
});
