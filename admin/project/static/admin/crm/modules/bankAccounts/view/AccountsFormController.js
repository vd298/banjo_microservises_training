Ext.define("Crm.modules.bankAccounts.view.AccountsFormController", {
  extend: "Crm.modules.accounts.view.AccountsFormController",
  setControls: function() {
    this.control({
      "[action=formclose]": {
        click: () => {
          this.closeView();
        }
      },
      "[action=apply]": {
        click: () => {
          this.save(false, (res) => {
            //D.a("Message","Data saved")
          });
        }
      },
      "[action=save]": {
        click: () => {
          this.save(true);
        }
      },
      "[action=remove]": {
        click: () => {
          this.deleteRecord_do(true);
        }
      },
      "[action=copy]": {
        click: () => {
          this.copyRecord(true);
        }
      },
      "[action=gotolist]": {
        click: () => {
          window.history.go(-1);
        }
      },
      "[action=exportjson]": {
        click: () => {
          this.exportJson();
        }
      },
      "[action=importjson]": {
        change: (el) => {
          this.importJson();
        }
      }
    });

    this.view.on("activate", (grid, indx) => {
      if (!this.oldDocTitle) this.oldDocTitle = document.title;
      var form = this.view.down("form").getForm();
      if (form) {
        data = form.getValues();
        this.setTitle(data);
      }
    });
    this.view.on("close", (grid, indx) => {
      if (this.oldDocTitle) document.title = this.oldDocTitle;
    });
    this.view.down("form").on({
      validitychange: (th, valid, eOpts) => {
        var el = this.view.down("[action=apply]");
        if (el) el.setDisabled(!valid);
        el = this.view.down("[action=save]");
        if (el) el.setDisabled(!valid);
      }
    });
    this.checkPermissions();
  }
});
