Ext.define("Crm.modules.bankAccounts.view.AccountLimitFormController", {
  extend: "Core.form.FormController",
  setControls: function() {
    this.control({
      "[action=formclose]": {
        click: () => {
          this.closeView();
        }
      },
      "[action=apply]": {
        click: () => {
          this.save(false, (res) => {
            //D.a("Message","Data saved")
          });
        }
      },
      "[action=save]": {
        click: () => {
          this.save(true);
        }
      },
      "[action=remove]": {
        click: () => {
          this.deleteRecord_do(true);
        }
      },
      "[action=copy]": {
        click: () => {
          this.copyRecord(true);
        }
      },
      "[action=gotolist]": {
        click: () => {
          this.closeCheck();
        }
      },
      "[action=exportjson]": {
        click: () => {
          this.exportJson();
        }
      },
      "[action=importjson]": {
        change: (el) => {
          this.importJson();
        }
      }
    });

    this.view.on("activate", (grid, indx) => {
      if (!this.oldDocTitle) this.oldDocTitle = document.title;
      var form = this.view.down("form").getForm();
      if (form) {
        data = form.getValues();
        this.setTitle(data);
      }
    });
    this.view.on("close", (grid, indx) => {
      if (this.oldDocTitle) document.title = this.oldDocTitle;
      try {
        if (me && me.view && me.view.currentData)
          me.setValues(me.view.currentData);
      } catch (err) {
        console.error(`FormController. Unable to reset form data. Error:`, err);
      }
    });
    this.view.down("form").on({
      validitychange: (th, valid, eOpts) => {
        var el = this.view.down("[action=apply]");
        if (el) el.setDisabled(!valid);
        el = this.view.down("[action=save]");
        if (el) el.setDisabled(!valid);
      }
    });
    this.checkPermissions();
  },

  closeCheck: function() {
    var me = this;
    if (me && me.view && me.view.changeFlag) {
      Ext.Msg.confirm(D.t("Closing"), D.t("Do you want to save?", []), function(
        btn
      ) {
        if (btn == "yes") {
          me.save(false, function(data) {
            me.view.changeArr = [];
            me.view.changeFlag = false;
            __CONFIG__.refreshNeeded = true;
            me.gotoListView();
          });
        } else {
          var i = 0;
          for (i = 0; i < me.view.changeArr.length; i++) {
            if (
              me &&
              me.view &&
              me.view.down("[name=" + me.view.changeArr[i].name + "]")
            )
              me.view
                .down("[name=" + me.view.changeArr[i].name + "]")
                .setValue(me.view.changeArr[i].oldValue);
          }
          me.view.changeFlag = false;
          me.view.changeArr = [];
          __CONFIG__.refreshNeeded = true;
          me.gotoListView();
        }
      });
    } else {
      __CONFIG__.refreshNeeded = true;
      me.gotoListView();
    }
  },

  gotoListView: function() {
    var me = this;
    try {
      if (me && me.view && me.view.currentData)
        me.setValues(me.view.currentData);
    } catch (err) {
      console.error(`FormController. Unable to reset form. Error:`, err);
    }
    window.history.go(-1);
  }
});
