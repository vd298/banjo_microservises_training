Ext.define("Crm.modules.bankAccounts.view.BankAccountsFormController", {
  extend: "Core.form.FormController",
  setControls() {
    const me = this;

    this.control({
      "[action=formclose]": {
        click: () => {
          this.closeView();
        }
      },
      "[action=apply]": {
        click: () => {
          this.save(false, (res) => {
            me.view.down("[name=currency]").disable();
          });
        }
      },
      "[action=save]": {
        click: () => {
          this.save(true);
        }
      },
      "[action=remove]": {
        click: () => {
          this.deleteRecord_do(true);
        }
      },
      "[action=copy]": {
        click: () => {
          this.copyRecord(true);
        }
      },
      "[action=gotolist]": {
        click: () => {
          this.gotoListView(true);
        }
      },
      "[action=exportjson]": {
        click: () => {
          this.exportJson();
        }
      },
      "[action=importjson]": {
        change: (el) => {
          this.importJson();
        }
      },
      "[name=is_crypto]": {
        change: function(v) {
          let proxyFilters = {};
          const ctime = me.view
            .down("form")
            .down("[name=ctime]")
            .getValue();
          const acc_no = me.view.down("form").down("[name=acc_no]");
          const currency = me.view.down("form").down("[name=currency]");
          if (v.value) {
            acc_no
              .setConfig("regex", /^[a-zA-Z0-9]{27,}$/)
              .setConfig("fieldLabel", "Account Address");
            proxyFilters = {
              filters: [
                {
                  _property: "crypto",
                  _value: true,
                  _operator: "eq"
                }
              ]
            };
          } else {
            acc_no
              .setConfig("regex", /^[0-9]*$/)
              .setConfig("fieldLabel", "Account Number");
            proxyFilters = {
              filters: [
                {
                  _property: "crypto",
                  _value: false,
                  _operator: "eq"
                }
              ]
            };
          }
          if (!ctime) {
            acc_no.setValue("");
            currency.setValue("");
          }
          const store = Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create(
              "Crm.modules.bankAccounts.model.currencyCombo"
            ),
            fieldSet: ["name", "abbr"],
            exProxyParams: proxyFilters,
            scope: me
          });

          currency.setStore(store);
        }
      },
      "[name=acc_no]": {
        change: (e, newVal, oldVal) => {
          const formData = me.view.down("form").getValues();
          const save = me.view.down("[action=apply]");
          this.model.runOnServer("uniqueAccNo", formData, function(res) {
            if (!res.result) {
              e.validator = function(value) {
                save.setConfig("disabled", true);
                return res.message;
              };
            } else {
              e.validator = function(value) {
                return true;
              };
            }
            me.view.down("form").isValid();
          });
        }
      }
    });

    this.view.on("disablePanels", (data, cmp) => {
      if (data.maker == undefined) {
        var tabPanel = Ext.getCmp(cmp);
        var items = tabPanel.items.items;

        for (var i = 1; i < items.length; i++) {
          var panel = items[i];
          panel.setDisabled(true);
        }
      }
    });

    this.view.down("form").on({
      validitychange: (th, valid, eOpts) => {
        var el = this.view.down("[action=apply]");
        if (el) el.setDisabled(!valid);
        el = this.view.down("[action=save]");
        if (el) el.setDisabled(!valid);
      }
    });
    this.checkPermissions();

    //Refreshes model data after form close so latest changes are visible on grid.
    this.view.on("close", async (grid, indx) => {
      await me.model.refreshData({
        modelName: this.model.$className
      });
    });

    this.view.on("activate", function(grid, indx) {
      if (!me.oldDocTitle) me.oldDocTitle = document.title;
    });

    this.disableCurrency(me);
  },

  afterDataLoad(data, cb) {
    var me = this;
    let allocationGrid = me.view.down("[name=allocationHistoryGrid]");
    let activeAllocationGrid = me.view.down("[name=activeAllocationGrid]");
    if (me && allocationGrid && data && data.maker == undefined) {
      me.view.fireEvent("disablePanels", data, "BankAccountsFormTabPanel");
      allocationGrid.disable();
      activeAllocationGrid.disable();
    }
    cb(data);
  },

  disableCurrency(me) {
    me.model.runOnServer("checkIfExists", { id: me.view.recordId }, function(
      res
    ) {
      if (res) {
        me.view.down("[name=currency]").disable();
        me.view.down("[name=is_crypto]").disable();
      }
    });
  }
});
