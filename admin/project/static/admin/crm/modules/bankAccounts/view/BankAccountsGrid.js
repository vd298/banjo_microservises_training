Ext.define("Crm.modules.bankAccounts.view.BankAccountsGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Bank Accounts"),
  controllerCls: "Crm.modules.bankAccounts.view.BankAccountsGridController",

  filterable: true,
  filterbar: true,

  buildColumns: function() {
    const typeArray = [
      { key: 0, name: "Unverified" },
      { key: 1, name: "Verified" }
    ];
    this.typesStore = Ext.create("Ext.data.Store", {
      fields: ["key", "name"],
      data: typeArray
    });
    return [
      {
        hidden: true,
        dataIndex: "id"
      },
      {
        text: D.t("Banks"),
        flex: 1,
        sortable: true,
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "id",
          displayField: "short_name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create("Crm.modules.bankAccounts.model.banksCombo"),
            fieldSet: ["id", "short_name"],
            scope: this
          }),
          operator: "eq"
        },
        dataIndex: "bank_id",
        renderer: function(v, m, r) {
          if (r.data.bank_name) {
            return (
              '<a href="#Crm.modules.banks.view.BanksForm~' +
              v +
              '">' +
              r.data.bank_name +
              "</a>"
            );
          }
          return v;
        }
      },
      {
        text: D.t("Account Name"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "account_name"
      },
      {
        text: D.t("Account Number"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "acc_no"
      },
      {
        text: D.t("Currency"),
        flex: 1,
        sortable: true,
        renderer: function(v, m, r) {
          v = r.data.currency;
          return v;
        },
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "abbr",
          displayField: "currencyDisplay",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create(
              "Crm.modules.bankAccounts.model.currencyCombo"
            ),
            fieldSet: ["name", "abbr"],
            scope: this
          }),
          operator: "eq"
        },
        dataIndex: "currency"
      },
      {
        text: D.t("Payment Provider"),
        flex: 1,
        sortable: true,
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "id",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create(
              "Crm.modules.bankAccounts.model.paymentProviderCombo"
            ),
            fieldSet: ["id,name"],
            scope: this
          }),
          operator: "eq"
        },
        dataIndex: "payment_provider_id",

        renderer: function(v, m, r) {
          if (r.data.name) {
            return (
              '<a href="#Crm.modules.paymentProvider.view.paymentProviderForm~' +
              v +
              '">' +
              r.data.name +
              "</a>"
            );
          }
          return v;
        }
      },
      {
        text: D.t("Is Verified"),
        flex: 1,
        sortable: true,
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "key",
          displayField: "name",
          store: this.typesStore,
          operator: "eq"
        },
        renderer: function(v, m, r) {
          if (v) {
            if (typeArray[v].name) {
              return typeArray[v].name;
            }
          }
          return "Unverified";
        },
        dataIndex: "is_verified"
      }
    ];
  }
});
