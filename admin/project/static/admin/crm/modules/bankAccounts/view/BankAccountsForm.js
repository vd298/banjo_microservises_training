Ext.define("Crm.modules.bankAccounts.view.BankAccountsForm", {
  extend: "Core.form.FormContainer",

  titleTpl: D.t("Bank Accounts"),
  requires: ["Desktop.core.widgets.GridField", "Core.grid.ComboColumn"],
  formLayout: "fit",

  formMargin: 0,
  controllerCls: "Crm.modules.bankAccounts.view.BankAccountsFormController",
  width: 800,
  height: 550,

  buildItems: function() {
    return [
      {
        xtype: "tabpanel",
        layout: "fit",
        id: "BankAccountsFormTabPanel",
        firstFormPanel: true,
        items: [
          this.buildFormFields(),
          this.buildAllocationHistory(),
          this.buildLimitsGrid(),
          this.buildActiveAllocation()
        ]
      }
    ];
  },
  buildAllocationHistory() {
    const me = this;
    return {
      xtype: "panel",
      layout: "fit",
      region: "center",
      title: D.t("Allocation History"),
      name: "allocationHistoryGrid",
      items: Ext.create("Crm.modules.bankAccounts.view.AccountHistoryGrid", {
        title: null,
        iconCls: null,
        scope: me,
        observe: [{ property: "bank_account", param: "id" }]
      })
    };
  },
  buildLimitsGrid() {
    const me = this;
    return {
      xtype: "panel",
      layout: "fit",
      region: "center",
      title: D.t("Reference limits"),
      items: Ext.create("Crm.modules.bankAccounts.view.AccountLimitGrid", {
        title: null,
        iconCls: null,
        scope: me,
        observe: [{ property: "bank_account_id", param: "id" }]
      })
    };
  },

  buildActiveAllocation() {
    const me = this;
    return {
      xtype: "panel",
      layout: "fit",
      region: "center",
      title: D.t("Active Allocations"),
      name: "activeAllocationGrid",
      items: Ext.create("Crm.modules.bankAccounts.view.ActiveAllocationGrid", {
        title: null,
        iconCls: null,
        scope: me,
        observe: [{ property: "bank_account", param: "id" }]
      })
    };
  },
  buildFormFields() {
    var me = this;
    return {
      xtype: "panel",
      layout: "anchor",
      title: D.t("General"),
      width: "50%",
      defaults: {
        anchor: "100%",
        xtype: "textfield",
        labelWidth: 150,
        margin: 5
      },
      items: [
        {
          name: "id",
          hidden: true
        },
        {
          name: "ctime",
          hidden: true
        },
        this.buildFreechargeCombo(),
        this.buildBankCombo(),
        {
          name: "account_name",
          fieldLabel: D.t("Account Holder Name"),
          regex: /[A-Za-z0-9]+/,
          regexText: "At least one alphabet or number is required",
          allowBlank: false,
          maxLength: 50
        },
        {
          xtype: "checkbox",
          name: "is_crypto",
          fieldLabel: D.t("Is Crypto")
        },
        {
          name: "acc_no",
          fieldLabel: D.t("Account Number"),
          regex: /^[0-9]*$/,
          regexText: "Only numbers are allowed",
          allowBlank: false
        },
        this.buildCurrencyCombo(),
        this.buildPaymentProviderCombo(),
        {
          xtype: "combo",
          name: "status",
          allowBlank: false,
          flex: 1,
          fieldLabel: D.t("Status"),
          valueField: "status",
          displayField: "status",
          store: {
            fields: ["status"],
            data: [{ status: "ACTIVE" }, { status: "INACTIVE" }]
          },
          listeners: {
            change: (e, newVal, oldVal, eOpts) => {
              if (newVal === "INACTIVE") {
                this.down("[name=note]").setConfig({
                  hidden: false
                });
                this.down("[name=old_status]").setValue(oldVal);
              } else {
                this.down("[name=note]").setConfig({
                  hidden: true
                });
                this.down("[name=note]").setValue("");
                this.down("[name=old_status]").setValue(oldVal);
              }
            }
          }
        },
        {
          name: "old_status",
          hidden: true
        },
        {
          name: "note",
          flex: 1,
          hidden: true,
          fieldLabel: D.t("Note")
        },
        {
          xtype: "combo",
          name: "is_verified",
          allowBlank: false,
          flex: 1,
          fieldLabel: D.t("Is Verified"),
          valueField: "key",
          displayField: "type",
          value: 1,
          store: {
            fields: ["key", "type"],
            data: [
              { key: 0, type: "Unverified" },
              { key: 1, type: "Verified" }
            ]
          }
        },
        this.buildGrid()
      ]
    };
  },
  buildGrid: function() {
    const me = this;
    return {
      xtype: "gridfield",
      hideLabel: true,
      region: "center",
      autoScroll: true,
      scrollable: true,
      name: "payment_data",
      layout: "fit",
      minHeight: 500,
      maxHeight: 501,
      fields: ["identifier", "payment_protocol"],
      listeners: {
        change: function(e, newVal, oldVal, eOpts) {
          const save = me.down("[action=apply]");
          let data = e.store.getRange().map((e) => e.data);
          for (let i = 0; i < data.length; i++) {
            const el = data[i];
            if (i === 0) {
              continue;
            }
            if (el.payment_protocol === newVal[0].payment_protocol) {
              save.setDisabled(true);
              Ext.Msg.alert(
                "Error",
                "Payment protocols for banks accounts must be unique"
              );
              break;
            } else {
              save.setDisabled(false);
            }
          }
        }
      },
      columns: [
        {
          text: D.t("Protocol"),
          flex: 1,
          sortable: false,
          name: "payment_protocol",
          dataIndex: "payment_protocol",
          menuDisabled: true,
          forceSelection: true,
          allowNull: false,
          xtype: "combocolumn",
          model: "Crm.modules.bankAccounts.model.PaymentTypeCombo",
          editor: {
            xtype: "dependedcombo",
            name: "payment_protocol",
            editable: true,
            fieldSet: "_id, name",
            valueField: "_id",
            displayField: "name",
            parentEl: null,
            parentField: null,
            dataModel: "Crm.modules.bankAccounts.model.PaymentTypeCombo",
            allowBlank: false
          }
        },
        {
          text: D.t("Identifier"),
          flex: 1,
          sortable: false,
          dataIndex: "identifier",
          menuDisabled: true,
          editor: true
        }
      ]
    };
  },
  buildPaymentProviderCombo() {
    return {
      name: "payment_provider_id",
      fieldLabel: D.t("Payment Provider"),
      xtype: "combo",
      editable: false,
      allowBlank: false,
      validateBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "all",
      displayField: "name",
      flex: 1,
      valueField: "id",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create(
          "Crm.modules.bankAccounts.model.paymentProviderCombo"
        ),
        fieldSet: ["id,name"],
        scope: this
      }),
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      }
    };
  },
  buildFreechargeCombo() {
    var me = this;
    return {
      name: "fc_merchant_id",
      fieldLabel: D.t("Freecharge Account"),
      xtype: "combo",
      editable: true,
      queryMode: "all",
      displayField: "bankAccountName",
      flex: 1,
      valueField: "merchantId",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.bankAccounts.model.freechargeCombo"),
        scope: this
      }),
      tpl:
        '<tpl for="."><div class="x-boundlist-item" style="height:30px;"><b>{bankAccountName}</b>&nbsp;&nbsp;(Already in Use: {alreadyInUse} )&nbsp </div><div style="clear:both"></div></tpl>',
      listeners: {
        beforeselect: function(cmp, record) {
          return !record.data.alreadyInUse;
        },
        afterrender: function(context, eOpts) {
          context.isValid();
        },
        select: async function(e, v) {
          const values = e.store.data.items[0].data;
          me.down("[name=bank_id]").setValue(values.bank_id);
          me.down("[name=account_name]").setValue(values.bankAccountName);
          me.down("[name=acc_no]").setValue(values.bankAccountNo);
          me.down("[name=currency]").setValue(values.bankAccountCurrency);

          let a = me.down("[name=payment_data]").store.getData().items;
          let protocols = [
            {
              identifier: values.bankAccountVPA,
              payment_protocol: "3ae5f73a-716b-40ce-aaac-939d0a41a415"
            },
            {
              identifier: values.ifsc,
              payment_protocol: "ec9402e2-9153-4761-a3fc-5c92dbbcd2f5"
            }
          ];
          for (let j = 0; j < protocols.length; j++) {
            protocols[j].found = false;
            for (let i = 0; i < a.length; i++) {
              if (
                protocols[j] &&
                a &&
                a[i] &&
                a[i].data &&
                a[i].data.payment_protocol == protocols[j].payment_protocol
              ) {
                protocols[j].found = true;
                a[i].data.identifier = protocols[j].identifier;
              }
            }
            if (protocols[j].found == false) {
              a.push(protocols[j]);
            }
          }
          me.down("[name=payment_data]").store.loadData(a, true);
        }
      }
    };
  },
  buildBankCombo() {
    return {
      name: "bank_id",
      fieldLabel: D.t("Bank"),
      xtype: "combo",
      editable: false,
      allowBlank: false,
      validateBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "all",
      displayField: "short_name",
      flex: 1,
      valueField: "id",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.bankAccounts.model.banksCombo"),
        fieldSet: ["id,short_name"],
        scope: this
      }),
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      }
    };
  },
  buildCurrencyCombo() {
    const me = this;
    return {
      name: "currency",
      fieldLabel: D.t("Currency"),
      xtype: "combo",
      editable: false,
      allowBlank: false,
      validateBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "all",
      displayField: "currencyDisplay",
      flex: 1,
      valueField: "abbr",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.bankAccounts.model.currencyCombo"),
        fieldSet: ["name", "abbr"],
        exProxyParams: {
          filters: [
            {
              _property: "crypto",
              _value: false,
              _operator: "eq"
            }
          ]
        },
        scope: this
      }),
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      }
    };
  },
  buildValuesGrid() {
    return {
      xtype: "gridfield",
      region: "west",
      name: "values",

      fields: ["abbr", "amount", "value"],

      columns: [
        {
          text: D.t("Currency abbr"),
          flex: 1,
          sortable: false,
          dataIndex: "abbr",
          menuDisabled: true,
          editor: true
        },
        {
          text: D.t("Amount"),
          flex: 1,
          sortable: false,
          dataIndex: "amount",
          menuDisabled: true,
          editor: true,
          renderer: (v) => {
            return v ? v : "default";
          }
        },
        {
          text: D.t("Rate"),
          flex: 1,
          sortable: false,
          dataIndex: "value",
          menuDisabled: true,
          editor: true
        }
      ]
    };
  }
});
