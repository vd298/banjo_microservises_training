Ext.define("Crm.modules.bankAccounts.view.AccountLimitGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Account Limit Grid"),
  filterable: true,
  filterbar: true,
  height: 500,
  controllerCls: "Crm.modules.bankAccounts.view.AccountLimitGridController",

  gridCfg: {
    viewConfig: {
      getRowClass: (record) => {
        if (record.data.global_limit == 0) return "custom-limit";
      }
    }
  },

  buildColumns: function() {
    return [
      {
        text: D.t("Id"),
        hidden: true,
        dataIndex: "id"
      },
      {
        text: D.t("Global Limit Flag"),
        hidden: true,
        dataIndex: "global_limit"
      },
      {
        text: D.t("Bank Account Protocol Id"),
        hidden: true,
        dataIndex: "bank_account_protocol_id"
      },
      { text: D.t("Removed"), hidden: true, dataIndex: "removed" },
      {
        text: "Payment Protocol Id",
        hidden: true,
        dataIndex: "payment_protocol_id"
      },
      {
        text: D.t("Currency"),
        dataIndex: "currency",
        flex: 1,
        sortable: true,
        filter: true
      },
      {
        text: D.t("Protocol Type"),
        dataIndex: "protocol_type",
        flex: 1,
        sortable: true,
        filter: true
      },
      {
        text: D.t("Min amount"),
        dataIndex: "min_amount",
        flex: 1,
        sortable: true,
        filter: true
      },
      {
        text: D.t("Max amount"),
        dataIndex: "max_amount",
        flex: 1,
        sortable: true,
        filter: true
      },
      {
        text: D.t("Max Daily Limit"),
        dataIndex: "daily_deposit",
        flex: 1,
        sortable: true,
        filter: { xtype: "numberfield" }
      },
      {
        text: D.t("Limit type"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "limit_type",
        filter: {
          xtype: "combo",
          valueField: "key",
          displayField: "name",
          store: {
            fields: ["key", "name"],
            data: [
              { key: 0, name: D.t("Deposit") },
              { key: 1, name: D.t("Withdraw") }
            ]
          }
        },
        renderer: function(v, m, r) {
          const types = {
            0: "Deposit",
            1: "Withdraw"
          };
          if (v == null) {
            return D.t("");
          }
          return D.t(types[v]);
        }
      }
    ];
  },

  buildButtonsColumns: function() {
    var me = this;
    return [
      {
        xtype: "actioncolumn",
        width: 54,
        menuDisabled: true,
        items: [
          {
            iconCls: "x-fa fa-pencil-square-o",
            tooltip: this.buttonEditTooltip,
            isDisabled: function() {
              return !me.permis.modify && !me.permis.read;
            },
            handler: function(grid, rowIndex) {
              me.fireEvent("edit", grid, rowIndex);
            }
          },
          {
            iconCls: "x-fa fa-trash",
            tooltip: this.buttonDeleteTooltip,
            isDisabled: function() {
              return !me.permis.del;
            },
            handler: function(grid, rowIndex) {
              me.fireEvent("deleteAccountLimit", grid, rowIndex);
            }
          }
        ]
      }
    ];
  }
});
