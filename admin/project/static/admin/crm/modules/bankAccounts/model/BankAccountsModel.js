Ext.define("Crm.modules.bankAccounts.model.BankAccountsModel", {
  extend: "Crm.classes.DataModel",

  collection: "bank_accounts",
  idField: "id",
  removedFilter: true,
  foreignKeyFilter: [
    {
      collection: "banks",
      alias: "b",
      on: " bank_accounts.bank_id = banks.id",
      type: "inner"
    }
  ],
  fields: [
    {
      name: "id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "bank_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "account_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "acc_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "currency",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "payment_provider_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "fc_merchant_id",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "status",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "is_crypto",
      type: "boolean",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "is_verified",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  /* scope:client */
  checkAssociatedMerchantAccounts: function(data, cb) {
    this.runOnServer("checkAssociatedMerchantAccounts", data, cb);
  },

  /* scope:server */
  async $checkAssociatedMerchantAccounts(data, cb) {
    let associated_accounts_count = await this.src.db
      .collection("merchant_account_mappings")
      .findAll({ bank_account: data.id, end_time: null });

    cb(associated_accounts_count);
  },

  /* scope:server */
  beforeRemove: async function(ids, callback) {
    this.$checkAssociatedMerchantAccounts({ id: ids[0] }, (res) => {
      if (!!res && res.length) {
        callback(null);
      } else {
        callback(ids);
      }
    });
  },

  async $uniqueAccNo(data, cb) {
    let sql = `select bank_accounts.* from bank_accounts `;
    // sql = await this.buildJoinQuery(sql);
    sql += ` where bank_accounts.removed=0 and bank_accounts.acc_no = ($1) and bank_accounts.id!=($2)`;
    const res = await this.src.db.query(sql, [data.acc_no, data.id]);
    if (res && res.length) {
      return cb({
        result: false,
        message: "Acc numbers for banks accounts must be unique"
      });
    } else {
      return cb({ result: true });
    }
  },

  /* scope:server */
  async beforeSave(data, cb) {
    let res,
      me = this;
    let ifCurrencyUpdate = await this.$ifCurrencyUpdate(data);
    if (ifCurrencyUpdate) {
      return cb({
        success: false,
        message: "Account currency cannot updated after creation"
      });
    }

    if (data.fc_merchant_id.trim().length === 0) {
      delete data.fc_merchant_id;
    }
    if (data.old_status) {
      res = await this.callApi({
        service: "account-service",
        method: "logBankAccountActivity",
        data: {
          bank_account_id: data.id,
          new_status: data.status,
          note: data.note,
          old_status: data.old_status
        },
        options: {
          realmId: me.user.profile.realm_id
        }
      });
      if (!res || !res.result) {
        console.error(
          `BankAccountsModel. Func:beforeSave. Failed call for logBankAccountAcitivity.`,
          res
        );
        cb({ success: false });
      }
    }
    let apiWrite = false;
    let params = {
      service: "account-service",
      method: "attachBankAccountProtocols",
      data: {
        bank_account_id: data.id,
        payment_data: data.payment_data
      },
      options: {
        realmId: me.user.profile.realm_id
      }
    };
    try {
      res = await me.callApi(params);
    } catch (error) {
      return cb({
        success: false,
        message: "Could not attach protocol"
      });
    }
    if (res && res.result) {
      apiWrite = true;
    } else {
      apiWrite = false;
    }
    if (apiWrite === true) {
      return cb(data);
    } else {
      return cb({
        success: false,
        message: "Could not attach protocol"
      });
    }
  },

  /* scope:server */
  async afterGetData(data, callback) {
    if (!data || !data.length) return callback(data);
    let id_payment_provider = [];
    let bankIds = [];
    data.map((item) => {
      if (!!item && item.payment_provider_id) {
        id_payment_provider.push(`'${item.payment_provider_id}'`);
      }
      if (!!item && item.bank_id) {
        bankIds.push(`'${item.bank_id}'`);
      }
    });

    if (id_payment_provider.length) {
      let sql = `select id, name from payment_providers where id in (${id_payment_provider})`;
      // sql = await this.buildRealmConditionQuery(sql);
      let res = await this.src.db.query(sql);
      res.map((item) => {
        data.map((dataItem) => {
          if (dataItem.payment_provider_id === item.id) {
            dataItem.name = item.name;
            return;
          }
        });
      });
    }

    if (bankIds.length) {
      let sql = `select id,short_name from banks where  id in (${bankIds})`;
      sql = await this.buildRealmConditionQuery(sql);
      let res = await this.src.db.query(sql);
      res.map((item) => {
        data.map((dataItem) => {
          if (dataItem.bank_id === item.id) {
            dataItem.bank_name = item.short_name;
            return;
          }
        });
      });
    }

    data.map(async (item) => {
      let sql = `select pp.protocol_type as name, bap.identifier, pp.id as payment_protocol from bank_accounts_protocols bap
          left join payment_protocols pp on (pp.id = bap.payment_protocol_id and pp.removed = 0)
          where bap.removed = 0 and bap.bank_account_id = ($1)`;

      const payment_data = await this.src.db.query(sql, [item.id]);
      item.payment_data = payment_data;
      return item;
    });

    return callback(data);
  },

  /* scope:server*/
  afterSave: function(data, cb) {
    var me = this;
    const Utils = require("@lib/utils");
    if (data && data.acc_no && data.acc_no.length) {
      Utils.setMemStore(`bankAcc${data.acc_no}`, data);
    }
    return cb(data);
  },

  /* scope:client */
  async checkIfExists(data, cb) {
    this.runOnServer("checkIfExists", data, cb);
  },

  /* scope:server */
  $checkIfExists: async function(data, cb) {
    var me = this;
    let sql = `select bank_accounts.* from bank_accounts `;
    sql = await this.buildJoinQuery(sql);
    sql += " where bank_accounts.id=$1";
    if (data.id) {
      me.src.db.query(sql, [data.id], function(err, resp) {
        if (err) {
          console.error(
            `BankAccountsModel. Func:checkIfExists. ${me.collection} table Database Error:`,
            err
          );
          cb(false);
        } else if (!!resp && resp.length) {
          cb(true);
        } else {
          cb(false);
        }
      });
    } else {
      return cb(false);
    }
  },

  /* scope:server */
  $ifCurrencyUpdate: async function(data) {
    var me = this;
    if (data.id) {
      let sql = `select bank_accounts.* from bank_accounts `;
      sql = await this.buildJoinQuery(sql);
      sql += " where bank_accounts.id=$1";
      let resp = await me.src.db.query(sql, [data.id]);

      if (!!resp && resp.length && data.currency) {
        if (resp[0].currency !== data.currency) {
          return true;
        }
        return false;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
});
