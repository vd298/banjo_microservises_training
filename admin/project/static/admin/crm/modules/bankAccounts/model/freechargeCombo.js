Ext.define("Crm.modules.bankAccounts.model.freechargeCombo", {
  extend: "Crm.modules.bankAccounts.model.BankAccountsModel",

  /* scope:server */
  getData: async function(params, cb) {
    var me = this;
    try {
      const result = await this.callApi({
        service: "transaction-service",
        method: "getUpiMerchants",
        data: {},
        options: {
          realmId: me.user.profile.realm_id
        }
      });
      if (!!result && result.result && result.result.merchants) {
        let merchants = result.result.merchants;
        let filteredMerchants = [];
        for await (let merchant of merchants) {
          let exists = await me.src.db.collection("bank_accounts").findOne({
            fc_merchant_id: merchant.merchantId,
            removed: 0
          });
          if (exists) {
            merchant["alreadyInUse"] = true;
          } else {
            merchant["alreadyInUse"] = false;
          }
          let find = { short_name: merchant.ifsc.slice(0, 4), removed: 0 };
          find = await this.checkValidRealmsID(find, "banks");
          let bank_id = await me.src.db.collection("banks").findOne(find);
          if (bank_id) {
            merchant["bank_id"] = bank_id.id;
          }
          filteredMerchants.push(merchant);
        }

        return cb({
          total: filteredMerchants.length,
          list: filteredMerchants
        });
      } else {
        return cb({ total: 0, list: [] });
      }
    } catch (err) {
      console.error(`freechargeCombo. Func: getData. Caught Error:`, err);
      return cb({ total: 0, list: [] });
    }
  }
});
