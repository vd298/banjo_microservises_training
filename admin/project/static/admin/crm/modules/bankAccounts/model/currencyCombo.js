Ext.define("Crm.modules.bankAccounts.model.currencyCombo", {
  extend: "Crm.modules.bankAccounts.model.BankAccountsModel",

  getData: async function(params, cb) {
    let sql = "SELECT name,abbr,crypto from currency where removed = 0";
    if (
      params &&
      params.filters &&
      params.filters.length &&
      params.filters[0]._property
    ) {
      sql += ` AND ${params.filters[0]._property} = ${params.filters[0]._value}`;
    }

    // sql = await this.buildRealmConditionQuery(sql);
    const res = await this.src.db.query(sql, function(err, resp) {
      if (err) {
        console.error(
          "CurrencyCombo.js. Func:getData. Database Error. Error",
          err
        );
        return cb({ total: 0, list: {} });
      }
      resp.rows.map((item) => {
        item.currencyDisplay = item.name + " (" + item.abbr + ")";
      });
      return cb({ total: resp.length, list: resp.rows });
    });
  }
});
