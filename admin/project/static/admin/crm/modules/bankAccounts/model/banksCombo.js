Ext.define("Crm.modules.bankAccounts.model.banksCombo", {
  extend: "Crm.modules.bankAccounts.model.BankAccountsModel",

  getData: async function(params, cb) {
    let query = "SELECT id,short_name from banks where removed = 0 ";
    query = await this.buildRealmConditionQuery(query);
    const res = await this.src.db.query(query, function(err, resp) {
      if (err) {
        console.error(
          "banksCombo.js. Func:getData. Database Error. Error",
          err
        );
        return cb({ total: 0, list: {} });
      }
      return cb({ total: resp.length, list: resp.rows });
    });
  }
});
