Ext.define("Crm.modules.bankAccounts.model.paymentProviderCombo", {
  extend: "Crm.modules.bankAccounts.model.BankAccountsModel",

  getData: async function(params, cb) {
    let query = "SELECT id,name from payment_providers where removed = '0'";
    query = await this.buildRealmConditionQuery(query);
    await this.src.db.query(query, function(err, resp) {
      if (err) {
        console.error(
          "paymentProviderCombo.js. Func:getData. Database Error. Error",
          err
        );
        return cb({ total: 0, list: {} });
      }
      return cb({ total: resp.length, list: resp.rows });
    });
  }
});
