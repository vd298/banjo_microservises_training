Ext.define("Crm.modules.bankAccounts.model.PaymentTypeCombo", {
  extend: "Core.data.DataModel",

  collection: "vw_payment_protocols",
  
  fields: [
    {
      name: "_id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "identifier_label",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    }
  ]
});
