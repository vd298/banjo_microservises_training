Ext.define("Crm.modules.bankAccounts.model.ActiveAllocationModel", {
  extend: "Crm.modules.bankAccounts.model.AccountHistoryModel",
  removedFilter: true,

  /* scope:server */
  buildWhere: function(params, cb) {
    params.filters.push({
      _property: "active_relation",
      _value: true,
      _operator: "eq"
    });
    var me = this,
      args = arguments;
    me.callParent(args);
  },
  getData(params, callback) {
    var me = this;
    // fix for extjs >= 5

    if (params && params.filters === undefined) {
      params.filters = [];
    }
    if (
      params &&
      params._property !== undefined &&
      params._value !== undefined &&
      params._operator !== undefined
    ) {
      params.filters.push({
        _property: params._property,
        _value: params._value,
        _operator: params._operator
      });
    }

    return me.callParent(arguments);
  }
});
