Ext.define("Crm.modules.bankAccounts.model.AccountLimitModel", {
  extend: "Crm.classes.DataModel",

  collection: "vw_bank_acc_payment_protocols_ref_limit",
  idField: "id",
  removedFilter: true,
  foreignKeyFilter: [
    {
      collection: "ref_limits",
      alias: "p",
      on: "  vw_bank_acc_payment_protocols_ref_limit.id = ref_limits.id",
      type: "inner"
    }
  ],
  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "global_limit",
      type: "number",
      visible: true,
      editable: true
    },
    {
      name: "bank_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "account_name",
      type: "string",
      filterable: false,
      editable: true,
      visible: true
    },
    {
      name: "acc_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "currency",
      type: "string",
      filterable: true,
      editable: true,
      visible: true,
      sort: -1
    },
    {
      name: "payment_provider_id",
      type: "ObjectID",
      filterable: false,
      editable: true,
      visible: true
    },
    {
      name: "status",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "protocol_type",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "identifier_label",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "account_label",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "transaction_identifier_label",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "bank_account_protocol_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "payment_protocol_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "bank_account_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "min_amount",
      type: "integer",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "max_amount",
      type: "integer",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "limit_type",
      type: "integer",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "daily_deposit",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  /* scope:server */
  async write(data, cb) {
    var me = this;
    const params = {
      service: "account-service",
      method: "attachBankAccountLimits",
      data: data,
      options: {
        realmId: me.user.profile.realm_id
      }
    };
    if (!isNaN(data.min_amount)) {
      data.min_amount = Number(data.min_amount);
    }
    if (!isNaN(data.max_amount)) {
      data.max_amount = Number(data.max_amount);
    }
    if (!isNaN(data.daily_deposit)) {
      data.daily_deposit = Number(data.daily_deposit);
    }
    if (
      typeof data.min_amount == "number" &&
      typeof data.max_amount == "number" &&
      data.min_amount > data.max_amount
    ) {
      return cb({
        success: false,
        message: "Maximum amount cannot be smaller than minimum amount"
      });
    }
    const resp = await me.callApi(params);
    if (resp.result) {
      this.changeModelData(
        "Crm.modules.bankAccounts.model.AccountLimitModel",
        "ins",
        {}
      );
      return cb(data);
    } else {
      return cb({
        success: false,
        message: resp.error.message
      });
    }
  },

  /* scope:client */
  deleteAccountLimit: function(data, cb) {
    this.runOnServer("deleteAccountLimit", data, cb);
  },

  /* scope:server */
  $deleteAccountLimit: async function(data, cb) {
    var me = this;
    if (data.id) {
      const res = await this.callApi({
        service: "account-service",
        method: "deleteAccountLimit",
        data,
        writeFlag: true,
        options: {
          realmId: me.user.profile.realm_id
        }
      });
      this.changeModelData(
        "Crm.modules.bankAccounts.model.AccountLimitModel",
        "ins",
        {}
      );
      if (res.result) {
        return cb(res);
      } else {
        return cb({
          success: false,
          message: res.error.message
        });
      }
    } else {
      return cb(false);
    }
  }
});
