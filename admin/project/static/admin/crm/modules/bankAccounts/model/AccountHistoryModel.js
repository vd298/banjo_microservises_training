Ext.define("Crm.modules.bankAccounts.model.AccountHistoryModel", {
  extend: "Crm.classes.DataModel",

  collection: "vw_merchant_account_mappings",
  idField: "id",
  removedFilter: true,
  foreignKeyFilter: [
    {
      collection: "banks",
      alias: "b",
      on: "  vw_merchant_account_mappings.bank_id = banks.id ",
      type: "inner"
    }
  ],
  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "bank_account",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "merchant_id",
      type: "ObjectID",
      filterable: false,
      editable: true,
      visible: true
    },
    {
      name: "merchant_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "active_relation",
      type: "boolean",
      filterable: true,
      editable: true,
      visible: true,
      sort: -1
    },
    {
      name: "merchant_account_id",
      type: "ObjectID",
      filterable: false,
      editable: true,
      visible: true
    },
    {
      name: "merchant_account_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "merchant_account_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "start_time",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "end_time",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    }
  ]
});
