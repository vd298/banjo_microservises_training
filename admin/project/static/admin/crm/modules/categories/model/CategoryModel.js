Ext.define("Crm.modules.categories.model.CategoryModel", {
  extend: "Core.data.DataModel",

  collection: "categories",
  idField: "id",
  strongRequest: false,
  removedFilter: true,
  //removeAction: "remove",

  maxLimit: 1000,

  fields: [
    {
      name: "id",
      type: "ObjectID"
    },
    {
      name: "name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "code",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    }
  ],
  /* scope:server */
  async $checkUniqueCode(data, cb) {
    if (data.id && data.code) {
      data.code = data.code.trim();
      let sql = `select id from categories where removed=0 and code ilike $1 and id != $2`;
      sql = await this.buildRealmConditionQuery(sql);
      const duplicateCheck = await this.src.db.query(sql, [data.code, data.id]);
      if (duplicateCheck[0]) {
        return cb({
          result: false,
          message: "code parameter need to be unique."
        });
      } else {
        return cb({ result: true });
      }
    } else {
      return cb({ result: false, message: "Code field required" });
    }
  },

  /* scope:server */
  async $checkUniqueName(data, cb) {
    if (data.id && data.name) {
      data.name = data.name.trim();
      let sql = `select id from categories where removed=0 and name ilike $1 and id != $2`;
      sql = await this.buildRealmConditionQuery(sql);
      const duplicateCheck = await this.src.db.query(sql, [data.name, data.id]);
      if (duplicateCheck[0]) {
        return cb({
          result: false,
          message: "Name should be unique."
        });
      } else {
        return cb({ result: true });
      }
    } else {
      return cb({ result: false, message: "Name required." });
    }
  },
  async beforeSave(data, cb) {
    data.name = data.name.trim();
    data.code = data.code.trim();
    cb(data);
  },
  /* scope:client */
  checkAssociatedMerchantAccounts: function(data, cb) {
    this.runOnServer("checkAssociatedMerchantAccounts", data, cb);
  },

  /* scope:server */
  async $checkAssociatedMerchantAccounts(data, cb) {
    let associated_accounts_count = await this.src.db
      .collection("merchant_accounts")
      .findAll({ category: data.id, removed: 0 });

    cb(associated_accounts_count);
  },

  /* scope:server */
  beforeRemove: async function(ids, callback) {
    this.$checkAssociatedMerchantAccounts({ id: ids[0] }, (res) => {
      if (!!res && res.length) {
        callback(null);
      } else {
        callback(ids);
      }
    });
  }
});
