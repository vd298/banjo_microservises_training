Ext.define("Crm.modules.categories.view.CategoryGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Categories"),
  iconCls: "x-fa fa-users",
  controllerCls: "Crm.modules.categories.view.CategoryGridController",
  filterable: true,
  filterbar: true,

  buildColumns: function() {
    return [
      {
        text: D.t("Code"),
        flex: 1,
        filter: true,
        sortable: true,
        dataIndex: "code"
      },
      {
        text: D.t("Name"),
        flex: 1,
        filter: true,
        sortable: true,
        dataIndex: "name"
      }
    ];
  }
});
