Ext.define("Crm.modules.categories.view.CategoryGridController", {
  extend: "Core.grid.GridController",

  setControls: function() {
    var me = this;
    this.control({
      "[action=add]": {
        click: function(el) {
          me.addRecord();
        }
      },
      "[action=refresh]": {
        click: function(el) {
          me.reloadData();
        }
      },
      "[action=import]": {
        click: function(el) {
          me.importData();
        }
      },
      "[action=export]": {
        click: function(el) {
          me.exportData();
        }
      },
      grid: {
        cellkeydown: function(cell, td, i, rec, tr, rowIndex, e, eOpts) {
          if (e.keyCode == 13) {
            me.gotoRecordHash(rec.data);
          }
        },
        celldblclick: function(cell, td, i, rec) {
          me.gotoRecordHash(rec.data);
        },
        itemcontextmenu: function(vw, record, item, index, e, options) {
          e.stopEvent();
        }
      }
    });
    this.view.on("activate", function(grid, indx) {
      if (!me.view.observeObject)
        document.title = me.view.title + " " + D.t("ConsoleTitle");
    });
    this.view.on("edit", function(grid, indx) {
      me.gotoRecordHash(grid.getStore().getAt(indx).data);
    });
    this.view.on("delete", function(grid, indx) {
      me.model.runOnServer(
        "checkAssociatedMerchantAccounts",
        { id: grid.getRecord(indx).id },
        (res) => {
          if (!!res && res.length) {
            D.a(
              "Delete Error",
              `Category cannot be deleted as it is assigned to ${res.length} merchant accounts. Please de-assign before deleting.`,
              []
            );
          } else {
            me.deleteRecord(grid.getStore(), indx);
          }
        }
      );
    });
    this.initButtonsByPermissions();
    setTimeout(() => {
      me.reloadData();
    }, 500);
  }
});
