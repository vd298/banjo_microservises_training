Ext.define("Crm.modules.categories.view.CategoryFormController", {
  extend: "Core.form.FormController",

  setControls: function() {
    var me = this;
    this.control({
      "[name=code]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();

          if (formData.code.trim().length < 1) {
            me.view.down("[name=code]").setValue("");
            e.validator = function(value) {
              return "White space not allowed";
            };
          }

          this.model.runOnServer("checkUniqueCode", formData, function(res) {
            e.validator = function(value) {
              console.log(res);
              if (res.result) {
                return true;
              } else {
                return res.message;
              }
            };
            me.view.down("form").isValid();
          });
        }
      },
      "[name=name]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();

          if (formData.name.trim().length < 1) {
            me.view.down("[name=name]").setValue("");
            e.validator = function(value) {
              return "White space not allowed";
            };
            me.view.down("form").isValid();
          }

          this.model.runOnServer("checkUniqueName", formData, function(res) {
            e.validator = function(value) {
              console.log(res);
              if (res.result) {
                return true;
              } else {
                return res.message;
              }
            };
            me.view.down("form").isValid();
          });
        }
      },
      "[action=formclose]": {
        click: () => {
          this.closeView();
        }
      },
      "[action=apply]": {
        click: () => {
          this.save(false, (res) => {
            //D.a("Message","Data saved")
          });
        }
      },
      "[action=save]": {
        click: () => {
          this.save(true);
        }
      },
      "[action=remove]": {
        click: () => {
          const me = this;
          const formData = me.view.down("form").getValues();
          me.model.runOnServer(
            "checkAssociatedMerchantAccounts",
            { id: formData.id },
            (res) => {
              if (!!res && res.length) {
                let component = Ext.Msg.alert(
                  "Delete Error!",
                  `Category cannot be deleted as it is assigned to ${res.length} merchant accounts. Please de-assign before deleting.`
                );
                Ext.WindowManager.bringToFront(component);
                Ext.WindowManager.getNextZSeed();
              } else {
                me.deleteRecord_do(true);
              }
            }
          );
        }
      },
      "[action=copy]": {
        click: () => {
          this.copyRecord(true);
        }
      },
      "[action=gotolist]": {
        click: () => {
          this.gotoListView(true);
        }
      },
      "[action=exportjson]": {
        click: () => {
          this.exportJson();
        }
      },
      "[action=importjson]": {
        change: (el) => {
          this.importJson();
        }
      }
    });

    this.view.on("activate", (grid, indx) => {
      if (!this.oldDocTitle) this.oldDocTitle = document.title;
      var form = this.view.down("form").getForm();
      if (form) {
        data = form.getValues();
        this.setTitle(data);
      }
    });
    this.view.on("close", (grid, indx) => {
      if (this.oldDocTitle) document.title = this.oldDocTitle;
      try {
        if (me && me.view && me.view.currentData)
          me.setValues(me.view.currentData);
      } catch (err) {
        console.error(`FormController. Unable to reset form data. Error:`, err);
      }
    });

    this.view.on("disablePanels", (data, cmp) => {
      if (data.maker == undefined) {
        var tabPanel = Ext.getCmp(cmp);
        var items = tabPanel.items.items;

        for (var i = 1; i < items.length; i++) {
          var panel = items[i];
          panel.setDisabled(true);
        }
      }
    });

    this.view.down("form").on({
      validitychange: (th, valid, eOpts) => {
        var el = this.view.down("[action=apply]");
        if (el) el.setDisabled(!valid);
        el = this.view.down("[action=save]");
        if (el) el.setDisabled(!valid);
      }
    });
    this.checkPermissions();
  }
});
