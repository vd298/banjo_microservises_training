Ext.define("Crm.modules.categories.view.CategoryForm", {
  extend: "Core.form.FormWindow",

  titleTpl: "Category: {name}",
  controllerCls: "Crm.modules.categories.view.CategoryFormController",
  width: 500,
  height: 200,
  syncSize: function() {},
  buildItems: function() {
    return [
      {
        name: "id",
        hidden: true
      },
      {
        name: "code",
        maxLength: 10,
        minLength: 1,
        allowBlank: false,
        fieldLabel: D.t("Code"),
        maskRe: /[a-zA-Z0-9]/,
        validator: function(value) {
          return true;
        }
      },
      {
        name: "name",
        maxLength: 50,
        minLength: 1,
        allowBlank: false,
        fieldLabel: D.t("Name"),
        validator: function(value) {
          return true;
        }
      }
    ];
  }
});
