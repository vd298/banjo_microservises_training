Ext.define("Crm.modules.realm.model.ParentRealmComboModel", {
  extend: "Crm.modules.realm.model.RealmModel",
  removedFilter: true,

  /* scope:server */
  buildWhere: function(params, callback) {
    params.filters = [
      {
        initialConfig: {
          property: "pid",
          value: null,
          type: "combo",
          operator: "eq"
        },
        config: {
          property: "pid",
          value: null,
          type: "combo",
          operator: "eq"
        },
        type: "combo",
        _property: "pid",
        _value: null,
        _disableOnEmpty: null,
        _operator: "eq",
        _root: "data",
        _id: "pid"
      }
    ].concat(params.filters);
    this.db.buildWhere(params, this, callback);
  }
});
