Ext.define("Crm.modules.realm.model.RealmModel", {
  extend: "Core.data.DataModel",

  collection: "realms",
  idField: "id",
  removedFilter: true,

  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "pid",
      type: "ObjectID",
      visible: true,
      filterable: true,
      editable: true,
      bindTo: {
        collection: "realms",
        keyFieldType: "ObjectID",
        keyField: "id",
        fields: {
          name: 1,
          id: 1
        }
      }
    },
    {
      name: "name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "token",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "tariff",
      type: "jsonb",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "cors",
      type: "object",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "variables",
      type: "object",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ip",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "domain",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "permissions",
      type: "object",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "activateuserlink",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "admin_realm",
      type: "boolean",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "status",
      type: "string",
      filterable: false,
      editable: true,
      visible: true
    }
  ],

  /* scope:client */
  getApiServices(cb) {
    this.runOnServer("getApiServices", {}, cb);
  },

  /* scope:server */
  async $getApiServices(data, cb) {
    cb(
      await this.src.queue.requestOne("auth-service", {
        method: "getPublicMethods",
        data: {}
      })
    );
  },

  /* scope:client */
  generateToken(cb) {
    this.runOnServer("generateToken", {}, (res) => {
      cb(res && res.token ? res.token : null);
    });
  },

  /* scope:server */
  async $generateToken(data, cb) {
    const UIDGenerator = require("uid-generator");
    const uidgen = new UIDGenerator(256, UIDGenerator.BASE16);
    uidgen.generate((err, token) => {
      if (err) throw err;
      cb({ token });
    });
  },

  /* scope:client */
  checkUniqueToken: function(data, cb) {
    this.runOnServer("checkUniqueToken", data, cb);
  },

  /* scope:server */
  $checkUniqueToken: function(data, cb) {
    var me = this;
    if (data.id && data.token) {
      me.dbCollection.findOne(
        {
          token: data.token.trim(),
          removed: 0,
          id: {
            $ne: me.src.db.fieldTypes.ObjectID.getValueToSave(me, data.id)
          }
        },
        { id: 1 },
        function(e, d) {
          cb({ isset: !!d && d.id });
        }
      );
    }
  },

  /* scope:server */
  async beforeSave(data, cb) {
    const res = await this.dbCollection.findOne({ id: data.id }, { token: 1 });
    if (res && res.token) {
      this.oldToken = res.token;
    } else if (!res) {
      this.newRealm = 1;
    }
    data.name = data.name.trim();
    data.ip = data.ip.trim();
    data.domain = data.domain.trim();
    data.token = data.token.trim();
    data.admin_realm = data.admin_realm === "on" ? true : false;
    cb(data);
  },

  /* scope:server */
  async afterSave(data, cb) {
    if (this.newRealm && data && data.id) {
      await this.addDefaultGlobalLimit(data);
      await this.addOwnerRole(data);
    }
    if (this.oldToken) {
      await this.src.queue.requestOne("auth-service", {
        method: "resetServer",
        data: { token: this.oldToken }
      });
    }
    cb(data);
  },
  async addOwnerRole(data) {
    try {
      const uuidV4 = require("uuid/v4");

      const me = this;
      const addOwnerRole = [
        {
          ctime: new Date(),
          mtime: new Date(),
          removed: 0,
          role_name: "Owner",
          permissions: {
            role: { view: true, create: true, edit: true, delete: true }
          },
          realm_id: data.id
        }
      ];
      for (let i = 0; i < addOwnerRole.length; i++) {
        await me.src.db.query(
          `INSERT INTO roles (id,ctime, mtime, removed,role_name,permissions,realm_id) VALUES ($1,$2,$3,$4,$5,$6,$7)`,
          [
            uuidV4(),
            addOwnerRole[i].ctime,
            addOwnerRole[i].mtime,
            addOwnerRole[i].removed,
            addOwnerRole[i].role_name,
            addOwnerRole[i].permissions,
            addOwnerRole[i].realm_id
          ]
        );
      }
    } catch (error) {
      console.log(
        "Error: File: RealmModel Function: addDefaultGlobalLimit Error: " +
          error
      );
    }
  },
  async addDefaultGlobalLimit(data) {
    try {
      const uuidV4 = require("uuid/v4");

      const me = this;
      const defaultGlobalLimit = [
        {
          ctime: new Date(),
          mtime: new Date(),
          removed: 0,
          protocol_id: "3ae5f73a-716b-40ce-aaac-939d0a41a415",
          currency: "INR",
          min_amount: 1,
          max_amount: 100000,
          realm_id: data.id
        },
        {
          ctime: new Date(),
          mtime: new Date(),
          removed: 0,
          protocol_id: "ec9402e2-9153-4761-a3fc-5c92dbbcd2f5",
          currency: "INR",
          min_amount: 1,
          max_amount: 200000,
          realm_id: data.id
        },
        {
          ctime: new Date(),
          mtime: new Date(),
          removed: 0,
          protocol_id: "94f84239-8cf4-4400-85c0-42d2b60a253d",
          currency: "INR",
          min_amount: 1,
          max_amount: 200000,
          realm_id: data.id
        },
        {
          ctime: new Date(),
          mtime: new Date(),
          removed: 0,
          protocol_id: "f3f01260-d459-4724-ba1a-ea62f694c2e1",
          currency: "INR",
          min_amount: 200000,
          max_amount: null,
          realm_id: data.id
        }
      ];
      for (let i = 0; i < defaultGlobalLimit.length; i++) {
        await me.src.db.query(
          `INSERT INTO ref_limits (id,ctime, mtime, removed,protocol_id,currency,min_amount,max_amount,realm_id) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)`,
          [
            uuidV4(),
            defaultGlobalLimit[i].ctime,
            defaultGlobalLimit[i].mtime,
            defaultGlobalLimit[i].removed,
            defaultGlobalLimit[i].protocol_id,
            defaultGlobalLimit[i].currency,
            defaultGlobalLimit[i].min_amount,
            defaultGlobalLimit[i].max_amount,
            defaultGlobalLimit[i].realm_id
          ]
        );
      }
    } catch (error) {
      console.log(
        "Error: File: RealmModel Function: addDefaultGlobalLimit Error: " +
          error
      );
    }
  }
});
