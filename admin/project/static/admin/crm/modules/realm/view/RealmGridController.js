Ext.define("Crm.modules.realm.view.RealmGridController", {
  extend: "Core.grid.GridController",
  afterDataLoad: function(view, cb) {
    var me = this;
    me.view.model.runOnServer("getAdminDetails", {}, function(resp) {
      if (resp && resp.user && !resp.user.realm_id && resp.user.superuser) {
        me.view.down("[action=add]").setVisible(true);
      } else {
        me.view.down("[action=add]").setVisible(false);
      }
      return cb(view);
    });
  },
  
});
