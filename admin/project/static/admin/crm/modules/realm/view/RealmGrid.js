Ext.define("Crm.modules.realm.view.RealmGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Realms"),
  iconCls: "x-fa fa-users",
  filterable: true,
  filterbar: true,
  controllerCls: "Crm.modules.realm.view.RealmGridController",
  buildColumns: function() {
    return [
      {
        text: D.t("Parent"),
        flex: 1,
        sortable: true,
        filter: Ext.create("Crm.modules.realm.view.RealmCombo", {
          name: null,
          fieldLabel: null,
          queryMode: "all"
        }),
        dataIndex: "pid",
        renderer: (v, m, r) => {
          return v ? v.name : "-";
        }
      },
      {
        text: D.t("name"),
        flex: 1,
        filter: true,
        sortable: true,
        dataIndex: "name"
      },
      {
        text: D.t("IP adderss"),
        flex: 1,
        filter: true,
        sortable: true,
        dataIndex: "ip"
      },
      {
        text: D.t("Domain"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "domain"
      },
      {
        text: D.t("Status"),
        flex: 1,
        sortable: true,
        dataIndex: "status",
        filter: __CONFIG__.buildENMStatusCombo({
          forceSelection: false,
          fieldLabel: null,
          enum_name: "enum_realms_status"
        })
      },
      {
        text: D.t("Token"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "token"
      }
    ];
  },
  buildButtonsColumns: function() {
    var me = this;
    return [
      {
        xtype: "actioncolumn",
        width: 54,
        menuDisabled: true,
        items: [
          {
            iconCls: "x-fa fa-pencil-square-o",
            tooltip: this.buttonEditTooltip,
            isDisabled: function() {
              return !me.permis.modify && !me.permis.read;
            },
            handler: function(grid, rowIndex) {
              me.fireEvent("edit", grid, rowIndex);
            }
          }
        ]
      }
    ];
  },
  buildTbar: function() {
    var items = [];

    items.push({
      text: this.buttonAddText,
      tooltip: this.buttonAddTooltip,
      iconCls: "x-fa fa-plus",
      scale: "medium",
      action: "add",
      itemId: "add",
      hidden: true
    });

    if (this.importButton) {
      items.push("-", {
        text: this.buttonImportText,
        iconCls: "x-fa fa-cloud-download",
        action: "import"
      });
    }
    if (this.exportButton) {
      items.push("-", {
        text: this.buttonExportText,
        iconCls: "x-fa fa-cloud-upload",
        action: "export"
      });
    }

    items.push("-", {
      tooltip: this.buttonReloadText,
      iconCls: "x-fa fa-refresh",
      action: "refresh"
    });

    if (this.filterable) items.push("->", this.buildSearchField());
    return items;
  }
});
