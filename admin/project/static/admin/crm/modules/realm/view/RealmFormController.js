Ext.define("Crm.modules.realm.view.RealmFormController", {
  extend: "Core.form.FormController",

  setControls() {
    this.control({
      "[action=gentoken]": {
        click: () => {
          this.genToken();
        }
      },
      "[name=token]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();
          if (formData.token.trim().length < 1) {
            e.validator = function(value) {
              return "White space not allowed";
            };
          }
          this.model.checkUniqueToken(formData, function(res) {
            e.validator = function(value) {
              if (!res.isset) {
                return true;
              } else {
                return res.message;
              }
            };
            me.view.down("form").isValid();
          });
        }
      }
    });
    this.view.on("beforesave", (el, data) => {
      this.prepareDataBeforeSave(data);
    });
    this.callParent(arguments);
  },

  afterDataLoad(data, cb) {
    let out = [];
    const permisGrid = this.view.down("[name=permiss]");
    permisGrid.setLoading(true);
    this.model.getApiServices((res) => {
      if (res && res.result && res.result.data) {
        Object.keys(res.result.data).forEach((service) => {
          Object.keys(res.result.data[service]).forEach((method) => {
            if (res.result.data[service][method].realm) {
              out.push({
                service,
                method,
                description: res.result.data[service][method].description,
                access:
                  data.permissions &&
                  data.permissions[service] &&
                  data.permissions[service][method]
              });
            }
          });
        });
      }
      out.sort((a, b) => {
        return a.service + ":" + a.method > b.service + ":" + b.method ? 1 : -1;
      });
      //data.permissions = out;
      permisGrid.setValue(out);
      permisGrid.setLoading(false);
    });
    if (this && data && data.ctime == undefined) {
      this.view.fireEvent("disablePanels", data, "RealmFormTabPanel");
    }
    cb(data);
  },

  prepareDataBeforeSave(data) {
    let out = {};
    data.permiss.forEach((rec) => {
      if (rec.access) {
        if (!out[rec.service]) out[rec.service] = {};
        out[rec.service][rec.method] = true;
      }
    });
    data.permissions = out;
  },

  genToken() {
    this.model.generateToken((token) => {
      if (token) {
        this.view.down("[name=token]").setValue(token);
      }
    });
  },

  setValues(data) {
    if (data && data.pid && data.pid.id) data.pid = data.pid.id;
    this.callParent(arguments);
  }
});
