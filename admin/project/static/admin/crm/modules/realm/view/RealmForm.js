Ext.define("Crm.modules.realm.view.RealmForm", {
  extend: "Core.form.FormContainer",

  titleTpl: "{name}",
  requires: ["Desktop.core.widgets.GridField"],
  formLayout: "fit",
  width: 500,
  height: 280,

  controllerCls: "Crm.modules.realm.view.RealmFormController",
  allowImportExport: true,

  buildItems: function() {
    return {
      xtype: "tabpanel",
      layout: "fit",
      id: "RealmFormTabPanel",
      items: [
        this.buildGeneral(),
        this.buildCorsPanel(),
        this.buildTariffPanel(),
        this.buildPermissionsGrid()
      ]
    };
  },

  buildGeneral() {
    return {
      xtype: "panel",
      title: D.t("General"),

      padding: 5,
      layout: "anchor",
      style: "background:#ffffff",
      defaults: { xtype: "textfield", labelWidth: 150, anchor: "100%" },
      items: [
        {
          name: "id",
          hidden: true
        },
        Ext.create("Crm.modules.realm.view.RealmCombo", {
          name: "pid",
          //margin: "0 3 0 0",
          fieldLabel: D.t("Parent realm")
        }),
        {
          name: "name",
          fieldLabel: D.t("Realm name"),
          validator: true,
          allowBlank: false,
          maxLength: 40,
          minLength: 1
        },
        {
          name: "ip",
          fieldLabel: D.t("IP address"),
          maxLength: 255
        },
        {
          name: "domain",
          fieldLabel: D.t("Domain"),
          maxLength: 255
        },
        {
          xtype: "fieldcontainer",
          layout: "hbox",
          items: [
            {
              name: "token",
              xtype: "textfield",
              labelWidth: 150,
              flex: 1,
              fieldLabel: D.t("Access token"),
              validator: true,
              allowBlank: false
            },
            {
              xtype: "button",
              action: "gentoken",
              width: 150,
              text: D.t("Generate token")
            }
          ]
        },
        __CONFIG__.buildENMStatusCombo({
          forceSelection: false,
          allowBlank: false,
          enum_name: "enum_realms_status"
        }),
        {
          xtype: "checkbox",
          name: "admin_realm",
          fieldLabel: D.t("Admin realm")
        }
        /* {
          xtype: "checkbox",
          name: "def",
          fieldLabel: D.t("Default")
        }*/
      ]
    };
  },
  buildTariffPanel() {
    return Ext.create("Crm.modules.tariffs.view.TariffSettingsPanel", {
      scope: this,
      uniqueCombinationForce: true
    });
  },
  buildPermissionsGrid() {
    return {
      xtype: "panel",
      title: D.t("Permissions"),
      //region: "center",
      layout: "fit",
      items: {
        xtype: "gridfield",
        name: "permiss",
        fields: ["service", "method", "description", "access"],
        buildTbar() {
          return null;
        },
        columns: [
          {
            text: D.t("Service"),
            flex: 1,
            sortable: true,
            dataIndex: "service"
          },
          {
            text: D.t("Method"),
            flex: 1,
            sortable: true,
            dataIndex: "method"
          },
          {
            text: D.t("Description"),
            flex: 1,
            sortable: true,
            dataIndex: "description"
          },
          {
            text: D.t("Access"),
            width: 70,
            sortable: true,
            dataIndex: "access",
            editor: {
              xtype: "checkbox",
              inputValue: true,
              uncheckedValue: false
            },
            renderer(v) {
              return v ? "YES" : "";
            }
          }
        ]
      }
    };
  },

  buildCorsPanel() {
    return {
      xtype: "panel",
      title: D.t("CORS"),
      //region: "center",
      layout: "fit",
      items: {
        xtype: "gridfield",
        name: "cors",
        fields: ["option", "value"],
        columns: [
          {
            text: D.t("Option"),
            flex: 1,
            sortable: true,
            dataIndex: "option",
            editor: this.buildCorsOptions()
          },
          {
            text: D.t("Value"),
            flex: 1,
            sortable: true,
            dataIndex: "value",
            editor: { xtype: "textfield" }
          }
        ]
      }
    };
  },

  buildCorsOptions() {
    return {
      xtype: "combo",
      valueField: "option",
      displayField: "option",
      store: {
        fields: ["option"],
        data: [
          { option: "origin" },
          { option: "methods" },
          { option: "allowedHeaders" },
          { option: "exposedHeaders" },
          { option: "credentials" },
          { option: "maxAge" },
          { option: "preflightContinue" },
          { option: "optionsSuccessStatus" }
        ]
      }
    };
  }
});
