Ext.define("Crm.modules.apicredentials.model.MerchantCategoryModel", {
  extend: "Crm.classes.DataModel",

  collection: "merchant_categories",
  idField: "id",

  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "name",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    }
  ]
});
