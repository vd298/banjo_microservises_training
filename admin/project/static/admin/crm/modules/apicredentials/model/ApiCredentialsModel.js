Ext.define("Crm.modules.apicredentials.model.ApiCredentialsModel", {
  extend: "Crm.classes.DataModel",

  collection: "api_credentials",
  idField: "id",
  foreignKeyFilter: [
    {
      collection: "banks",
      alias: "b",
      on: "  api_credentials.bank = banks.id ",
      type: "inner"
    }
  ],

  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "name",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "currency",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "aggregator_id",
      type: "string",
      sort: 1,
      filterable: false,
      editable: true,
      visible: true
    },
    {
      name: "secret_key",
      sort: 1,
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "account_id",
      sort: 1,
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "merchant_account_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "category",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "bank_account",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "bank",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "payment_provider_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  /* scope:server */
  async $duplicateCheck(data, cb) {
    let sql = `select api_credentials.* from  api_credentials `;
    sql = await this.buildJoinQuery(sql);
    sql += ` where LOWER(api_credentials.aggregator_id) = LOWER($1) and api_credentials.id !=$2 and api_credentials.removed = 0`;
    const duplicateCheck = await this.src.db.query(sql, [
      data.aggregator_id,
      data.id
    ]);
    if (duplicateCheck[0]) {
      return cb({
        result: false,
        message: "Aggregator Id needs to be unique."
      });
    } else {
      return cb({ result: true });
    }
  },

  /* scope:server */
  async afterGetData(data, cb) {
    if (data && data.length) {
      let categoryId = [];
      let category = [];
      let indexData = 1;
      secretKeys = [];
      data.map((d) => {
        secretKeys.push({ secret_key: d.secret_key, id: d.id });
        if (categoryId.indexOf(d.category) == -1) {
          category.push("$" + indexData);
          indexData++;
          categoryId.push(d.category);
        }
        if (d.secret_key) {
          delete d.secret_key;
        }
      });
      let decryptSecretKeys = await this.decryptSecretKeys(secretKeys, cb);
      category = category.join(", ");
      if (categoryId && categoryId.length) {
        let sql = `SELECT id, name FROM merchant_categories WHERE id IN (${category})`;
        // sql = await this.buildRealmConditionQuery(sql);
        const category_name = await this.src.db.query(sql, categoryId);

        for (let i = 0; i < data.length; i++) {
          let index = category_name.findIndex((prop) => {
            if (prop.id == data[i].category) {
              return true;
            }
          });
          decryptSecretKeys.secretKeys.findIndex((prop) => {
            if (prop.id == data[i].id) {
              return true;
            }
          });
          if (index >= 0) {
            data[i].category_name = category_name[index]["name"];
            data[i].secret_key_masked = this.maskString(
              decryptSecretKeys.secretKeys[index]["decryptedSecret"]
            );
          }
        }
      }
    }
    cb(data);
  },

  /* scope:server*/
  async beforeSave(data, cb) {
    if (data.secret_key === "") {
      delete data.secret_key;
    }
    var me = this;
    try {
      const params = {
        service: "account-service",
        method: "createAcquirerBankAccount",
        data: data,
        options: {
          realmId: me.user.profile.realm_id
        }
      };

      const result = await me.callApi(params);
      if (result.error) {
        cb({ success: false, message: result.error.message });
      } else {
        if (result.result.bank_account_id) {
          data.bank_account = result.result.bank_account_id;
        }
        if (result.result && result.result.secret_key) {
          data.secret_key = result.result.secret_key;
        }
      }
      return cb(data);
    } catch (error) {
      return cb({
        success: false,
        message: "Could not create acquirer account."
      });
    }
  },

  maskString(str) {
    if (str.length < 4) {
      return "x".repeat(str.length);
    }

    const firstTwoChars = str.substring(0, 2);
    const lastTwoChars = str.substring(str.length - 2);
    const maskedChars = "x".repeat(str.length - 4);

    return firstTwoChars + maskedChars + lastTwoChars;
  },

  async decryptSecretKeys(data, cb) {
    var me = this;
    try {
      const params = {
        service: "account-service",
        method: "decryptSecretKeys",
        data: { secretKeys: data },
        options: {
          realmId: me.user.profile.realm_id
        }
      };

      const result = await me.callApi(params);
      if (result.error) {
        return result.result.secretKeys;
      } else {
        return result.result;
      }
    } catch (error) {
      return cb({
        success: false,
        message: "Could not create acquirer account."
      });
    }
  },

  /* scope:client */
  async checkIfExists(data, cb) {
    this.runOnServer("checkIfExists", data, cb);
  },

  /* scope:server */
  $checkIfExists: async function(data, cb) {
    var me = this;
    let sql = "select api_credentials.* from api_credentials ";
    sql = await this.buildJoinQuery(sql);
    sql += ` where api_credentials.id=$1 and api_credentials.removed = 0 `;
    if (data.id) {
      me.src.db.query(sql, [data.id], function(err, resp) {
        if (err) {
          console.error(
            `MerchantAccountsModel. Func:checkIfExists. ${me.collection} table Database Error:`,
            err
          );
          cb(false);
        } else if (!!resp && resp.length) {
          cb(true);
        } else {
          cb(false);
        }
      });
    } else {
      return cb(false);
    }
  }
});
