Ext.define("Crm.modules.apicredentials.view.ApiCredentialsForm", {
  extend: "Core.form.FormWindow",
  titleTpl: D.t("API credentials"),
  requires: ["Desktop.core.widgets.GridField"],
  formLayout: "fit",
  controllerCls: "Crm.modules.apicredentials.view.ApiCredentialsFormController",
  formMargin: 0,
  width: 450,
  height: 350,
  syncSize: function() {},
  buildItems() {
    return {
      xtype: "tabpanel",
      layout: "fit",
      items: [this.buildGeneral()],
      autoEl: { "data-test": "tab-items" }
    };
  },
  buildGeneral() {
    return {
      xtype: "panel",
      layout: "anchor",
      title: D.t("General"),
      defaults: {
        anchor: "100%",
        xtype: "textfield",
        labelWidth: 150,
        margin: 5
      },
      items: [
        {
          name: "id",
          hidden: true
        },
        {
          name: "name",
          fieldLabel: D.t("Name"),
          validator: true,
          allowBlank: false
        },
        {
          name: "aggregator_id",
          fieldLabel: D.t("Secret Id"),
          validator: true,
          allowBlank: false
        },
        {
          name: "secret_key",
          fieldLabel: D.t("Secret key"),
          validator: true,
          allowBlank: false
        },
        {
          name: "secret_key_masked",
          fieldLabel: D.t("Masked key"),
          validator: true,
          xtype: "displayfield",
          allowBlank: false,
          listeners: {
            render: function(displayField) {
              if (Ext.isEmpty(displayField.getValue())) {
                displayField.setValue("xxxxxxxx");
              }
            }
          }
        },
        this.buildPaymentProviderCombo(),
        {
          name: "category",
          xtype: "combo",
          fieldLabel: D.t("Category"),
          queryMode: "all",
          displayField: "name",
          forceSelection: true,
          editable: true,
          valueField: "id",
          validator: true,
          allowBlank: false,
          minChars: 1,
          store: Ext.create("Core.data.ComboStore", {
            storeId: "category",
            dataModel: Ext.create(
              "Crm.modules.apicredentials.model.MerchantCategoryModel"
            ),
            fieldSet: ["name", "id"],
            scope: this
          })
        },
        this.buildCurrencyCombo(),
        this.buildBankCombo()
      ]
    };
  },
  buildCurrencyCombo() {
    return {
      name: "currency",
      fieldLabel: D.t("Currency"),
      xtype: "combo",
      editable: false,
      allowBlank: false,
      validateBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "all",
      displayField: "currencyDisplay",
      flex: 1,
      valueField: "abbr",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.bankAccounts.model.currencyCombo"),
        fieldSet: ["name", "abbr"],
        scope: this
      }),
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      }
    };
  },
  buildBankCombo() {
    return {
      name: "bank",
      fieldLabel: D.t("Bank"),
      xtype: "combo",
      editable: false,
      allowBlank: false,
      validateBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "all",
      displayField: "short_name",
      flex: 1,
      valueField: "id",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create(
          "Crm.modules.bankAccounts.model.aquirerBanksCombo"
        ),
        fieldSet: ["id,short_name"],
        scope: this
      }),
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      }
    };
  },
  buildPaymentProviderCombo() {
    return {
      name: "payment_provider_id",
      fieldLabel: D.t("Payment Provider"),
      xtype: "combo",
      editable: false,
      allowBlank: false,
      validateBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "all",
      displayField: "name",
      flex: 1,
      valueField: "id",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create(
          "Crm.modules.bankAccounts.model.paymentProviderCombo"
        ),
        fieldSet: ["id,name"],
        scope: this
      }),
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      }
    };
  }
});
