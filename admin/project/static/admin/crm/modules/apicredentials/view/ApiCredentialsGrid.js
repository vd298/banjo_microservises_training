Ext.define("Crm.modules.apicredentials.view.ApiCredentialsGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("API Credentials"),

  filterable: true,
  filterbar: true,

  buildColumns: function() {
    return [
      {
        dataIndex: "id",
        hidden: true
      },
      {
        dataIndex: "secret_key",
        hidden: true
      },
      {
        dataIndex: "secret_key_masked",
        hidden: true
      },
      {
        dataIndex: "payment_provider_id",
        hidden: true
      },
      {
        text: D.t("Name"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "name"
      },
      {
        text: D.t("Aggregator Id"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "aggregator_id"
      },
      {
        text: D.t("Category"),
        flex: 1,
        sortable: true,
        filter: {
          xtype: "combo",
          valueField: "id",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create(
              "Crm.modules.apicredentials.model.MerchantCategoryModel"
            ),
            fieldSet: ["id", "name"],
            scope: this
          })
        },
        dataIndex: "category",
        renderer: function(v, m, r) {
          if (r.data.category) {
            return r.data.category_name;
          } else {
            return v;
          }
        }
      }
    ];
  }
});
