Ext.define("Crm.modules.apicredentials.view.ApiCredentialsFormController", {
  extend: "Core.form.FormController",

  setControls() {
    const me = this;
    this.control({
      "[name=aggregator_id]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();
          let name = formData.name ? formData.name.trim() : "";
          if (name.length <= 0) {
            e.validator = function(value) {
              return "White space not allowed";
            };
          } else {
            e.validator = function(value) {
              return true;
            };
          }
          me.model.runOnServer("duplicateCheck", formData, function(res) {
            e.validator = function(value) {
              if (res.result) {
                return true;
              } else {
                return res.message;
              }
            };
          });
        }
      }
    });
    this.isSecretKeyRequired(me);
    this.callParent(arguments);
  },
  isSecretKeyRequired(me) {
    this.model.checkIfExists({ id: this.view.recordId }, (resp) => {
      if (!resp) {
        me.view.down("[name=secret_key]").setConfig("allowBlank", false);
      } else {
        me.view.down("[name=secret_key]").setConfig("allowBlank", true);
      }
    });
  }
});
