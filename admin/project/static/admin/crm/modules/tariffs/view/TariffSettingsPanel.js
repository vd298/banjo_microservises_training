Ext.define("Crm.modules.tariffs.view.TariffSettingsPanel", {
  extend: "Ext.panel.Panel",
  title: D.t("Tariff"),
  height: 380,
  formLayout: "fit",
  initComponent() {
    this.items = [
      {
        xtype: "panel",
        region: "north",
        height: 50,
        layout: "hbox",
        defaults: {
          margin: "9 5 5 5"
        },
        items: [
          {
            xtype: "combo",
            name: "tariff",
            fieldLabel: D.t("Tariffs plan"),
            valueField: "id",
            displayField: "name",
            queryMode: "local",
            flex: 1,
            allowBlank: true,

            store: Ext.create("Core.data.ComboStore", {
              dataModel: Ext.create(
                "Crm.modules.tariffs.model.PlansValuesModel"
              ),
              fieldSet: ["id", "name", "variables"],
              scope: this
            })
          },
          {
            xtype: "button",
            width: 150,
            iconCls: "x-fa fa-chevron-down",
            handler: () => {
              this.importVariables();
            },
            text: D.t("Import Variables")
          }
        ]
      },
      {
        xtype: "panel",
        region: "center",
        title: D.t("Variables"),
        cls: "grayTitlePanel",
        layout: "fit",
        items: Ext.create("Crm.modules.tariffs.view.VariablesPanel", {})
      }
    ];
    this.callParent(arguments);
  },

  importVariables() {
    const tariffEl = this.down("[name=tariff]");
    const tariff = tariffEl.getValue();
    if (!tariff) {
      return D.a("", "Select tariff", []);
    }
    const variablesEl = this.down("[name=variables]");
    const f = () => {
      tariffEl.getStore().each((item) => {
        if (item.data.id == tariff) {
          variablesEl.setValue(Ext.clone(item.data.variables));
        }
      });
    };
    if (variablesEl.getValue().length) {
      D.c("", "Replace previously entered variables?", [], () => {
        f();
      });
    } else f();
  }
});
