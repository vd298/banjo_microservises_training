Ext.define("Crm.modules.tariffs.view.actions.Transfer", {
  extend: "Ext.form.FieldContainer",

  layout: "anchor",

  defaults: {
    anchor: "100%",
    labelWidth: 150
  },

  initComponent() {
    let currentView = this;
    this.items = [
      {
        fieldLabel: D.t("Action Type"),
        aname: "txtype",
        xtype: "combo",
        displayField: "type",
        valueField: "type",
        store: {
          fields: ["type"],
          data: [
            { type: "PAYOUT" },
            { type: "PAYIN" },
            { type: "COLLECTION" },
            { type: "REFUND" },
            { type: "RETURN" },
            { type: "REVERSE" },
            { type: "SETTLEMENT" },
            { type: "DIRECT_DEPOSIT" }
          ]
        }
      },
      {
        fieldLabel: D.t("Tx Sub Type"),
        aname: "txsubtype",
        xtype: "combo",
        displayField: "desc",
        valueField: "type",
        store: {
          fields: ["type", "desc"],
          data: __CONFIG__.getTxActivityType()
        }
      },
      {
        fieldLabel: D.t("Key Field"),
        aname: "parent_id",
        xtype: "treecombo",
        store: this.scope.id_store
      },
      {
        xtype: "fieldcontainer",
        layout: "anchor",
        defaults: {
          xtype: "treecombo",
          store: this.scope.acc_store,
          labelWidth: 150,
          displayField: "_id",
          valueField: "_id",
          anchor: "100%"
        },
        items: [
          {
            fieldLabel: D.t("Source Account"),
            //margin: "0 5 0 0",
            aname: "acc_src"
          },
          {
            fieldLabel: D.t("Dest Account"),
            //margin: "0 0 0 5",
            aname: "acc_dst"
          }
        ]
      },
      {
        xtype: "fieldcontainer",
        layout: "hbox",
        items: [
          {
            fieldLabel: D.t("Amount"),
            aname: "fee",
            flex: 3,
            labelWidth: 150,
            margin: "0 5 0 0",
            xtype: "textfield"
          },
          {
            emptyText: D.t("Currency"),
            flex: 2,
            aname: "currency",
            xtype: "combo",
            valueField: "key",
            margin: "0 5 0 0",
            displayField: "text",
            store: {
              fields: ["key", "text"],
              data: [
                { key: "src", text: "Currency of src account" },
                { key: "dst", text: "Currency of dest. account" }
                // { key: "EUR", text: "Fixed EUR Currency" },
                // { key: "USD", text: "Fixed USD Currency" },
                // { key: "GBP", text: "Fixed GBP Currency" },
                // { key: "RUB", text: "Fixed RUB Currency" },
                // { key: "PLN", text: "Fixed PLN Currency" }
              ]
            },
            listeners: {
              change: (el, v) => {
                if (v == "src" || v == "dst") {
                  currentView.down("[aname=amount_field]").setDisabled(false);
                } else {
                  currentView.down("[aname=amount_field]").setDisabled(true);
                }
              }
            }
          },
          {
            emptyText: D.t("Amount Type"),
            flex: 1,
            aname: "feetype",
            xtype: "combo",
            valueField: "type",
            margin: "0 5 0 0",
            displayField: "type",
            listeners: {
              change: (el, v) => {
                currentView
                  .down("[aname=amount_field]")
                  .setDisabled(v != "PERCENTS");
                currentView
                  .down("[aname=minimum_amount]")
                  .setDisabled(v != "PERCENTS");
              }
            },
            store: {
              fields: ["type"],
              data: [{ type: "ABS" }, { type: "PERCENTS" }]
            }
          },
          {
            emptyText: D.t("Amount Field"),
            flex: 3,
            aname: "amount_field",
            xtype: "treecombo",
            displayField: "_id",
            valueField: "_id",
            store: this.scope.amount_store
          }
        ]
      },
      {
        xtype: "fieldcontainer",
        layout: "hbox",
        height: 30,
        items: [
          //   {
          //   xtype: 'label',
          //   name: 'info_label_1',
          //   text: D.t('Minimum Amount only applicable for Percentage calculations'),
          //   margin: "0 5 0 0",
          //   padding: "4 0 0 0",
          // },
          {
            fieldLabel: D.t("Minimum Fee"),
            aname: "minimum_amount",
            labelWidth: 150,
            width: 600,
            emptyText: D.t(
              "Minimum Amount only applicable for Percentage calculations"
            ),
            margin: "0 5 0 0",
            xtype: "textfield"
          }
        ]
      },
      {
        xtype: "fieldcontainer",
        layout: "hbox",
        height: 30,
        items: [
          {
            fieldLabel: D.t("Hold Transaction"),
            aname: "hold",
            xtype: "checkbox",
            labelWidth: 150,
            height: 30,
            margin: "0 30 0 0"
          },
          {
            fieldLabel: D.t("Hidden"),
            height: 30,
            aname: "hidden",
            xtype: "checkbox"
          }
        ]
      },
      {
        fieldLabel: D.t("Description for sender"),
        aname: "description_src",
        xtype: "textfield"
      },
      {
        fieldLabel: D.t("Description for recipient"),
        aname: "description_dst",
        xtype: "textfield"
      }
    ];
    this.callParent(arguments);
  }
});
