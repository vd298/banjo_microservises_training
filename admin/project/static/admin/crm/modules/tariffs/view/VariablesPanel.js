Ext.define("Crm.modules.tariffs.view.VariablesPanel", {
  extend: "Desktop.core.widgets.GridField",

  title: D.t("Variables"),
  xtype: "gridfield",
  hideLabel: true,
  region: "center",
  name: "variables",
  layout: "fit",
  fields: ["key", "value", "descript"],
  constructor(cfg) {
    this.columns = [
      {
        text: D.t("Key"),
        width: 200,
        sortable: false,
        dataIndex: "key",
        menuDisabled: true,
        allowBlank: false,
        editor: true
      },
      {
        text: D.t("Param"),
        flex: 1,
        sortable: false,
        dataIndex: "descript",
        menuDisabled: true,
        allowBlank: false,
        editor: true,
        renderer: (v, m, r) => {
          return v || r.data.key;
        }
      },
      {
        text: D.t("Value"),
        width: 300,
        sortable: false,
        dataIndex: "value",
        menuDisabled: true,
        allowBlank: false,
        editor: true
      }
    ];
    this.callParent(arguments);
  }
});
