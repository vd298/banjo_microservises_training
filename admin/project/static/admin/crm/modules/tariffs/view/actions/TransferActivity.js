Ext.define("Crm.modules.tariffs.view.actions.TransferActivity", {
  extend: "Ext.form.FieldContainer",

  layout: "anchor",

  defaults: {
    anchor: "100%",
    labelWidth: 150
  },

  initComponent() {
    this.items = [
      {
        fieldLabel: D.t("Action Type"),
        aname: "txtype",
        xtype: "combo",
        displayField: "type",
        valueField: "type",
        store: {
          fields: ["type"],
          data: [
            { type: "PAYOUT" },
            { type: "PAYIN" },
            { type: "COLLECTION" },
            { type: "REFUND" },
            { type: "RETURN" },
            { type: "REVERSE" },
            { type: "SETTLEMENT" },
            { type: "DIRECT_DEPOSIT" }
          ]
        }
      },
      {
        fieldLabel: D.t("Tx Sub Type"),
        aname: "txsubtype",
        xtype: "combo",
        displayField: "desc",
        valueField: "type",
        store: {
          fields: ["type", "desc"],
          data: __CONFIG__.getTxActivityType()
        }
      },
      {
        fieldLabel: D.t("Note"),
        aname: "note",
        xtype: "textfield"
      }
    ];
    this.callParent(arguments);
  }
});
