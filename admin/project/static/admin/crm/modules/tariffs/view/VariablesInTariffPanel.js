Ext.define("Crm.modules.tariffs.view.VariablesInTariffPanel", {
  extend: "Desktop.core.widgets.GridField",

  title: D.t("Variables"),
  xtype: "gridfield",
  hideLabel: true,
  region: "center",
  name: "variables",
  layout: "fit",
  fields: ["key", "value", "descript"],
  constructor(cfg) {
    this.columns = [
      {
        text: D.t("Key"),
        width: 200,
        sortable: false,
        dataIndex: "key",
        menuDisabled: true,
        editor: true
      },
      {
        text: D.t("Value"),
        width: 200,
        sortable: false,
        dataIndex: "value",
        menuDisabled: true,
        editor: true
      },
      {
        text: D.t("Description"),
        flex: 1,
        sortable: false,
        dataIndex: "descript",
        menuDisabled: true,
        editor: true
      }
    ];
    this.callParent(arguments);
  }
});
