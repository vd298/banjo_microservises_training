Ext.define("Crm.modules.tariffs.view.TariffSettingsUserPanel", {
  extend: "Crm.modules.tariffs.view.TariffSettingsPanel",
  name: "TariffSettingsUserPanel",
  initComponent() {
    let me = this;
    let proxyFilters = {};
    this.items = [
      {
        xtype: "panel",
        region: "north",
        height: 50,
        layout: "hbox",
        defaults: {
          margin: "9 5 5 5"
        },
        items: [
          {
            xtype: "tagfield",
            name: "realm_plans",
            valueField: "id",
            displayField: "name",
            queryMode: "local",
            readOnly: true,
            flex: 1,
            labelWidth: 220,
            fieldLabel: D.t("Default Realm plan"),
            ariaHelpText:
              "Displays plans applicable for the User under current realm",
            blankText: "Currently there are no realm plans set for this User",
            emptyText: "Currently there are no realm plans set for this User",
            store: Ext.create("Core.data.ComboStore", {
              dataModel: Ext.create(
                "Crm.modules.tariffs.model.PlansValuesModel"
              ),
              fieldSet: ["id", "name", "variables"],
              scope: this
            })
          }
        ]
      },
      {
        xtype: "panel",
        region: "north",
        height: 50,
        layout: "hbox",
        defaults: {
          margin: "9 5 5 5"
        },
        items: [
          {
            xtype: "tagfield",
            name: "tariff",
            valueField: "id",
            displayField: "name",
            queryMode: "remote",
            triggerAction: "all",
            labelWidth: 220,
            flex: 1,
            fieldLabel: D.t("Tariffs plan"),
            store: Ext.create("Core.data.ComboStore", {
              dataModel: Ext.create(
                "Crm.modules.tariffs.model.PlansValuesModel"
              ),
              fieldSet: ["id", "name", "variables"],
              exProxyParams: proxyFilters,
              scope: this
            }),
            listeners: {
              afterrender: function(context, eOpts) {
                let userForm = this.up("form");
                if (userForm) {
                  let userVals = userForm.getValues();
                  if (userVals && userVals.risk_type != undefined) {
                    proxyFilters.risk_type = userVals.risk_type;
                  }
                  if (userVals && userVals.type != undefined) {
                    proxyFilters.user_type = userVals.type;
                  }
                }
              },
              change: function(el, val, oldValue) {
                let realmPlansComponent = me.down("[name=realm_plans]");
                if (val && val.length > 0) {
                  if (realmPlansComponent) {
                    realmPlansComponent.setDisabled(true);
                    realmPlansComponent.setFieldLabel(
                      "Default Ream Plan (No longer applicable due to custom plan)"
                    );
                  }
                } else {
                  if (realmPlansComponent) {
                    realmPlansComponent.setDisabled(false);
                    realmPlansComponent.setFieldLabel("Default Realm plan");
                  }
                }
              }
            }
          },
          {
            xtype: "button",
            width: 150,
            iconCls: "x-fa fa-chevron-down",
            handler: () => {
              this.importVariables();
            },
            text: D.t("Import Variables")
          }
        ]
      },
      {
        xtype: "panel",
        region: "center",
        title: D.t("Variables"),
        cls: "grayTitlePanel",
        layout: "fit",
        items: Ext.create("Crm.modules.tariffs.view.VariablesPanel", {})
      }
    ];
    this.callParent(arguments);
  },

  importVariables() {
    const tariffEl = this.down("[name=tariff]");
    const tariff = tariffEl.getValue();
    if (!tariff || (tariff && tariff.length == 0)) {
      return D.a("", "Select tariff", []);
    }
    const variablesEl = this.down("[name=variables]");
    const f = () => {
      tariffEl
        .getStore()
        .getRange()
        .forEach(item => {
          if (typeof tariff == "object" && Array.isArray(tariff)) {
            tariff.forEach(currTariff => {
              if (item.id == currTariff && item.data && item.data.variables) {
                variablesEl.setValue(Ext.clone(item.data.variables));
              }
            });
          } else if (typeof tariff == "string" && item.id == tariff) {
            variablesEl.setValue(Ext.clone(item.variables));
          }
        });
    };
    if (
      variablesEl.getValue().length &&
      typeof tariffEl.getStore == "function" &&
      typeof tariffEl.getStore().getRange == "function"
    ) {
      D.c("", "Replace previously entered variables?", [], () => {
        f();
      });
    } else f();
  }
});
