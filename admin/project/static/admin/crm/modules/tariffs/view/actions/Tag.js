Ext.define("Crm.modules.tariffs.view.actions.Tag", {
  extend: "Ext.form.FieldContainer",

  layout: "anchor",

  defaults: {
    anchor: "100%",
    labelWidth: 150
  },

  initComponent() {
    this.items = [
      {
        fieldLabel: D.t("Key Field"),
        aname: "entity",
        xtype: "treecombo",
        store: this.scope.id_store
      },
      {
        fieldLabel: D.t("Tag Name"),
        aname: "tag",
        xtype: "textfield"
      }
    ];
    this.callParent(arguments);
  }
});
