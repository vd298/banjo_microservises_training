Ext.define("Crm.modules.tariffs.model.TariffsModel", {
  extend: "Core.data.DataModel",

  collection: "tariffs",
  idField: "id",
  foreignKeyFilter: [
    {
      collection: "triggers",
      alias: "tr",
      on: " triggers.id = tariffs.trigger ",
      type: "inner"
    },
    
  ],
  //removeAction: "remove",

  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "pid",
      type: "ObjectID",
      visible: true,
      editable: true
    },
    {
      name: "name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "description",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "trigger",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "data",
      type: "object",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "variables",
      type: "object",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "actions",
      type: "object",
      filterable: false,
      editable: true,
      visible: true
    },
    {
      name: "rules",
      type: "object",
      filterable: false,
      editable: true,
      visible: true
    },
    {
      name: "stop_on_rules",
      type: "boolean",
      filterable: false,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    }
  ]
});
