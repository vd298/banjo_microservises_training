Ext.define("Crm.modules.BankConfig.model.BankConfigModel", {
  extend: "Core.data.DataModel",

  collection: "bank_config",
  idField: "id",
  removeAction: "mark",
  strongRequest: false,
  flattenBind: true,
  foreignKeyFilter: [
    {
      collection: "banks",
      alias: "b",
      on: " bank_config.bank_id = banks.id ",
      type: "inner"
    }
  ],
  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "domain_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "bank_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true,
      bindTo: {
        collection: "banks",
        keyFieldType: "ObjectID",
        keyField: "id",
        fields: {
          name: 1
        }
      }
    },
    {
      name: "brn_regex",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  /* scope:server*/
  afterSave: function(data, cb) {
    var me = this;
    const Utils = require("@lib/utils");
    if (data && data.domain_name && data.domain_name.length) {
      Utils.setMemStore(`bankConfig${data.domain_name}`, data);
    }
    return cb(data);
  },

  /* scope:server*/
  async $uniqueCheck(data, cb) {
    var me = this,
      placeHolders = [],
      ilikeFlag = false;
    if (data && data.changeCol == "domain_name") {
      ilikeFlag = true;
    } else if (data && data.changeCol) {
      ilikeFlag = false;
    } else {
      cb({
        result: false,
        message: `Invalid Filter`
      });
    }
    placeHolders.push(data.id);
    let sql = `select ${me.collection}.* from ${me.collection} `;
    sql = await this.buildJoinQuery(sql);
    sql += ` where ${me.collection}.removed=0 and ${me.collection}.id!=$${placeHolders.length} `;
    placeHolders.push(data.changeVal);
    if (ilikeFlag) {
      sql += ` and ${me.collection}.${data.changeCol} ilike $${placeHolders.length}`;
    } else {
      sql += ` and ${me.collection}.${data.changeCol} = $${placeHolders.length}`;
    }

    const res = await this.src.db.query(sql, placeHolders);
    if (res && res.length) {
      let message =
        data.changeCol === "domain_name" ? "Bank Domain Name" : data.changeCol;
      return cb({
        result: false,
        message: `${message} must be unique`
      });
    } else {
      return cb({ result: true });
    }
  }
});
