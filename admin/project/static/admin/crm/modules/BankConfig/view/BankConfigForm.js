Ext.define("Crm.modules.BankConfig.view.BankConfigForm", {
  extend: "Core.form.FormWindow",

  titleTpl: D.t("Bank Config: {domain_name}"),
  controllerCls: "Crm.modules.BankConfig.view.BankConfigFormController",
  formMargin: 5,

  width: 550,
  height: 220,

  syncSize: function() {},

  buildItems() {
    var me = this;
    return [
      {
        name: "id",
        hidden: true
      },
      {
        name: "domain_name",
        fieldLabel: D.t("Bank Domain Name"),
        flex: 1,
        maxLength: 100,
        allowBlank: false
      },
      me.buildBankIdCombo(),
      {
        name: "brn_regex",
        flex: 1,
        xtype: "textarea",
        fieldLabel: D.t("BRN regex"),
        maxLength: 500,
        allowBlank: false,
        listeners: {
          change: function(e, newVal, oldVal, eOpts) {
            let isValid = true;
            try {
              new RegExp(newVal);
            } catch (e) {
              e.validator = function(value) {
                return "Invalid regular expression";
              };
            }
            if (isValid) {
              e.validator = function(value) {
                return true;
              };
            }
          }
        }
      }
    ];
  },

  buildBankIdCombo() {
    return {
      flex: 1,
      allowBlank: false,
      fieldLabel: D.t("Select Bank Id"),
      xtype: "combo",
      name: "bank_id",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.banks.model.BanksModel"),
        exProxyParams: {
          filters: [
            {
              _property: "bank_type",
              _value: 0,
              _operator: "eq"
            }
          ]
        },
        fieldSet: ["id", "name"],
        scope: this
      }),
      valueField: "id",
      displayField: "name"
    };
  }
});
