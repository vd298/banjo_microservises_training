Ext.define("Crm.modules.BankConfig.view.BankConfigFormController", {
  extend: "Core.form.FormController",
  setControls() {
    this.control({
      "[name=domain_name]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();
          formData.changeCol = "domain_name";
          formData.changeVal = formData.domain_name;
          this.checker(formData, me, e);
        }
      },
      "[name=bank_id]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();
          formData.changeCol = "bank_id";
          formData.changeVal = formData.bank_id;

          this.checker(formData, me, e);
        }
      }
    });
    this.callParent(arguments);
  },
  checker(formData, me, e) {
    me.model.runOnServer("uniqueCheck", formData, function(res) {
      if (!res.result) {
        e.validator = function(value) {
          return res.message;
        };
      } else {
        e.validator = function(value) {
          return true;
        };
      }
      me.view.down("form").isValid();
    });
  }
});
