Ext.define("Crm.modules.BankConfig.view.BankConfigGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Account Holders"),
  iconCls: "x-fa fa-users",

  filterable: true,
  filterbar: true,

  fields: ["id", "domain_name", "bank_id", "brn_regex"],

  buildColumns: function() {
    return [
      {
        text: D.t("Domain Name"),
        width: 100,
        sortable: true,
        filter: true,
        flex: 1,
        dataIndex: "domain_name"
      },
      {
        text: D.t("Bank Name"),
        flex: 1,
        sortable: true,
        dataIndex: "bank_id",
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "id",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create("Crm.modules.banks.model.BanksModel"),
            fieldSet: ["id", "name"],
            exProxyParams: {
              filters: [
                {
                  _property: "bank_type",
                  _value: 0,
                  _operator: "eq"
                }
              ]
            },
            scope: this
          }),
          operator: "eq"
        },
        renderer: function(v, m, r) {
          if (r && r.data && r.data.bank_id && r.data.bank_id_name) {
            return (
              '<a href="#Crm.modules.banks.view.BanksForm~' +
              r.data.bank_id +
              '">' +
              r.data.bank_id_name +
              "</a>"
            );
          }
        }
      },
      {
        text: D.t("BRN Regular Expression"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "brn_regex"
      }
    ];
  }
});
