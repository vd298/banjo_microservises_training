Ext.define("Crm.modules.files.model.FileModel", {
  extend: "Core.data.DataModel",
  collection: "files",
  idField: "id",
  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "tmp_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "size",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mime_type",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  async afterSave(data, cb) {
    const fs = require("fs");
    let srcPath = this.config.uploadTmpDir + "/" + this.tmpName;
    let dstPath = this.config.uploadTmpDir + "/../../upload/" + this.fileName;
    console.log("src", srcPath);
    console.log("dst", dstPath);
    fs.copyFile(srcPath, dstPath, (err) => {
      if (err) {
        console.log("Error Found:", err);
      } else {
        // Get the current filenames
        // after the function
        console.log("\nFile copied");
      }
    });
    cb(data);
  },

  async beforeSave(data, cb) {
    let fileData = JSON.parse(data.file);
    this.tmpName = fileData.tmpName;
    this.fileName = fileData.name;
    data.name = fileData.name;
    data.tmp_name = fileData.tmpName;
    data.mime_type = fileData.mime_type;
    data.size = fileData.size;
    cb(data);
  }
});
