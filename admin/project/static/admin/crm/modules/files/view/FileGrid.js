Ext.define("Crm.modules.files.view.FileGrid", {
  extend: "Core.grid.GridContainer",
  title: D.t("Files"),
  iconCls: "x-fa fa-file",
  buildColumns: function() {
    var me = this;
    return [
      {
        dataIndex: "tmp_name",
        hidden: true
      },
      {
        text: D.t("Name"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "name",
        renderer: (v, m, r) => {
          if (r.data.mime_type.includes("image")) {
            return `<a href='/Admin.Data.getFile/?name=${encodeURIComponent(
              v
            )}&tmp=${encodeURIComponent(r.data.tmp_name)}'>${v}</a>`;
          } else {
            return v;
          }
        }
      },
      {
        text: D.t("Size"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "size",
        renderer: function(v) {
          if (v / 1024 < 1024) {
            return (v / 1024).toFixed(2) + " kB";
          }
          return (v / (1024 * 1024)).toFixed(2) + " MB";
        }
      },
      {
        text: D.t("Type"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "mime_type"
      }
    ];
  }
});
