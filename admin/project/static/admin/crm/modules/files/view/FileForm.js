Ext.define("Crm.modules.files.view.FileForm", {
  extend: "Core.form.FormWindow",
  requires: ["Ext.form.HtmlEditor"],
  titleTpl: D.t("Upload a file"),
  iconCls: "x-fa fa-file",

  formMargin: 0,
  formLayout: "fit",

  width: 700,
  height: 500,

  syncSize: function() {},
  // controllerCls: "Crm.modules.deviceTypes.view.DeviceTypeFormController",
  buildItems() {
    return {
      xtype: "panel",
      height: "90vh",
      items: [this.buildUploadForm(), this.buildSelectExistingGrid()]
    };
  },
  buildUploadForm() {
    let me = this;
    return {
      xtype: "panel",
      layout: "anchor",
      scrollable: true,
      defaults: { xtype: "textfield", labelWidth: 150, anchor: "100%" },
      items: [
        {
          name: "file",
          hidden: true
        },
        {
          xtype: "fieldset",
          title: "Select",
          items: [
            {
              xtype: "filefield",
              label: "File:",
              name: "name",
              accept: "image",
              listeners: {
                change: function(el) {
                  me.upload(el);
                }
              }
            },
            {
              xtype: "label"
            }
          ]
        }
      ]
    };
  },
  buildSelectExistingGrid() {
    this.existingFiles = Ext.create("Crm.modules.files.view.FileGrid");
    return {
      action: "resultgrid",
      region: "east",
      width: "100%",
      cls: "grayTitlePanel",
      layout: "fit",
      items: this.existingFiles
    };
  },
  upload: function(inp) {
    var me = this;
    if (inp.fileInputEl.dom.files.length > 0) {
      var fn =
        inp.fileInputEl.dom.files[0].name ||
        inp.fileInputEl.dom.files[0].fileName;
      let file = inp.fileInputEl.dom.files[0];

      Glob.Ajax.upload(
        inp.fileInputEl.dom.files[0],
        "/Admin.Data.uploadFile/",
        function(data) {
          if (data.response && data.response.name) {
            me.down("[name=file]").setValue(
              JSON.stringify({
                name: fn,
                size: file.size,
                mime_type: file.type,
                tmpName: data.response.name
              })
            );
            var o = {
              name: fn,
              tmpName: data.response.name,
              link:
                "/Admin.Data.getFile/?name= " +
                encodeURIComponent(fn) +
                "&tmp=" +
                encodeURIComponent(data.response.name)
            };
            me.fireEvent("upload", o);
            me.setValue(o);
            inp.fileInputEl.dom.value = "";
          }
        }
      );
    }
  },
  setValue: function(value) {
    var me = this,
      btn = me.down("[xtype=filefield]").button,
      label = me.down("[xtype=label]");
    this.value = value;
    if (!value) {
      label.setText("");
      btn.on("contextmenu", function(e) {
        e.preventDefault();
      });
      return;
    }
    label.setText('<a href="' + value.link + '">' + value.name + "</a>", false);
    btn.on("contextmenu", function(e) {
      e.preventDefault();
      me.contextMenu.show(btn);
    });
    label.on("contextmenu", function(e) {
      e.preventDefault();
      me.contextMenu.show(label);
    });
  }
});
