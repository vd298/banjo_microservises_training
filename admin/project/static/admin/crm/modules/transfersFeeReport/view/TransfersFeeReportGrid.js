Ext.define("Crm.modules.transfersFeeReport.view.TransfersFeeReportGrid", {
  extend: "Admin.view.main.AbstractContainer",

  layout: "border",
  autoScroll: false,
  height: 600,

  requires: [
    "Ext.tab.Panel",
    "Ext.layout.container.Border",
    "Ext.layout.container.Anchor",
    "Ext.form.field.ComboBox",
    "Ext.form.field.Number",
    "Ext.form.FieldContainer",
    "Desktop.core.widgets.TagField",
    "Core.form.DependedTags",
    "Core.form.DependedCombo"
  ],

  initComponent: function() {
    var me = this;

    this.items = this.buildItems();

    this.controller = Ext.create(
      "Crm.modules.transfersFeeReport.view.TransfersFeeReportGridController"
    );
    this.model = Ext.create(
      "Crm.modules.transfersFeeReport.model.TransfersFeeReportModel"
    );
    me.callParent(arguments);
  },

  buildItems: function() {
    return [this.buildFiltersPanel(), this.contextPanel()];
  },

  buildFiltersPanel: function() {
    var me = this;
    return {
      title: D.t("Table Filters"),
      name: "FiltersPanel",
      collapsible: true,
      collapsed: false,
      scrollable: "y",
      region: "east",
      width: 255,
      split: true,
      xtype: "form",
      action: "filtersform",
      bodyStyle: "background:#efefef;",
      style: "background:#efefef;",
      layout: "anchor",
      //padding: 5,
      defaults: {
        anchor: "100%",
        xtype: "textfield",
        margin: 5,
        hideLabel: true
      },
      items: this.buildFilters(),

      listeners: {
        render: function() {
          me.fireEvent("checkUser", localStorage.uid, function(flag) {
            if (!flag && me.up()) {
              me.up()
                .down("[name=merchant_id]")
                .setDisabled(true);
              me.up()
                .down("[name=merchant_id]")
                .hide();
              me.up()
                .down("[name=merchant_label]")
                .hide();
            }
          });
          me.syncSize();
        },
        resize: function(c, oW, oH, nW, nH) {
          me.syncSize();
        }
      }
    };
  },

  syncSize: function() {
    var width = Ext.Element.getViewportWidth(),
      height = Ext.Element.getViewportHeight();
    this.setHeight(Math.floor(height * 0.9));
  },

  buildFilters: function() {
    var me = this;
    return [
      {
        xtype: "label",
        name: "merchant_label",
        text: "Select Merchant"
      },
      me.buildMerchants(),
      {
        xtype: "label",
        name: "merchant_account_label",
        text: "Select Merchant Account"
      },
      me.buildMerchantsAccounts(),

      {
        xtype: "label",
        text: "Transactions date From and To"
      },
      me.datesFromTo(),
      {
        xtype: "button",
        text: D.t("Update grid data"),
        action: "update",
        iconCls: "x-fa fa-refresh"
      }
    ];
  },

  buildGrid() {
    return {
      xtype: "gridfield",
      region: "center",
      name: "list",

      // split: true,
      fields: [
        "txtype",
        "detail",
        "rate_per_transaction",
        "unit_of_fee",
        "count_of_respective_transactions",
        "total_fees_applicable",
        "currency"
      ],
      buildTbar() {
        return null;
      },
      buildCellEditing() {
        return null;
      },
      buildGrid: function(columns) {
        var me = this,
          uni = new Date().getTime();

        this.firstEditableColumn = null;
        for (var i = 0; i < columns.length; i++) {
          if (columns[i].editor) {
            if (this.firstEditableColumn === null) this.firstEditableColumn = i;
            if (columns[i].editor === true)
              columns[i].editor = { xtype: "textfield" };
            columns[i].editor.submitValue = false;
          }
        }

        columns.push({
          xtype: "mactioncolumn",
          width: 30,
          sortable: false,
          menuDisabled: true,
          align: "center",

          items: []
        });

        var grid = {
          xtype: "grid",
          region: "center",
          columns: columns,
          store: this.store,
          tbar: this.buildTbar(),
          border: this.border,
          bodyBorder: this.bodyBorder,
          hideHeaders: this.hideHeaders,
          viewConfig: {
            trackOver: false,
            plugins: {
              ptype: "gridviewdragdrop",
              dragGroup: uni + "dnd",
              dropGroup: uni + "dnd"
            }
          },
          listeners: {
            cellkeydown: function(th, td, cellIndex, record, tr, rowIndex, e) {
              me.keyDown(record, e);
            },
            cellclick: function(
              th,
              td,
              cellIndex,
              record,
              tr,
              rowIndex,
              e,
              eOpts
            ) {
              me.fireEvent(
                "cellclick",
                th,
                td,
                cellIndex,
                record,
                tr,
                rowIndex,
                e,
                eOpts
              );
            },
            celldblclick: function(
              th,
              td,
              cellIndex,
              record,
              tr,
              rowIndex,
              e,
              eOpts
            ) {
              me.fireEvent(
                "celldblclick",
                th,
                td,
                cellIndex,
                record,
                tr,
                rowIndex,
                e,
                eOpts
              );
            }
          }
        };

        var p = this.cellEditing;
        if (p) grid.plugins = [p];

        if (me.gridCfg) Ext.apply(grid, me.gridCfg);

        return grid;
      },

      columns: [
        {
          text: D.t("Transfer Type"),
          flex: 1,
          sortable: true,
          dataIndex: "txtype",
          menuDisabled: true,
          editor: false
        },
        {
          text: D.t("Detail"),
          flex: 1,
          sortable: true,
          dataIndex: "detail",
          menuDisabled: true,
          editor: false
        },
        {
          text: D.t("Rate Per Transaction"),
          flex: 1,
          sortable: true,
          dataIndex: "rate_per_transaction",
          menuDisabled: true,
          editor: false
        },
        {
          text: D.t("Units of Fee"),
          flex: 1,
          sortable: true,
          dataIndex: "unit_of_fee",
          menuDisabled: true,
          editor: false,
          renderer: (v) => {
            return v && v === "PERCENTS" ? "%" : v;
          }
        },
        {
          text: D.t("Count of Respective Transactions"),
          flex: 1,
          sortable: true,
          dataIndex: "count_of_respective_transactions",
          menuDisabled: true,
          editor: false
        },
        {
          text: D.t("Total Fees Applicable"),
          flex: 1,
          sortable: true,
          dataIndex: "total_fees_applicable",
          menuDisabled: true,
          editor: false
        },
        {
          text: D.t("Currency"),
          flex: 1,
          sortable: true,
          dataIndex: "currency",
          menuDisabled: true,
          editor: false
        }
      ]
    };
  },
  contextPanel: function() {
    var me = this;
    return {
      activeTab: __CONFIG__.RevenueReportsTab || 0,
      xtype: "tabpanel",
      region: "center",
      action: "contentpanel",
      layout: "fit",
      defaults: {
        xtype: "panel",
        layout: "fit"
      },
      items: [
        {
          title: D.t("Revenue Report"),
          iconCls: "fa fa-th-list",
          items: this.buildGrid(),
          action: 0
        }
      ],
      listeners: {
        tabchange: function(e, v) {
          __CONFIG__.RevenueReportsTab = v.action;
        }
      }
    };
  },

  datesFromTo: function() {
    return Ext.create("Core.form.DatesFromTo", {
      anchor: "100%",
      name: "period",
      getValueFormat: "Y-m-d",
      buildDateField: function(name) {
        var currentDate = new Date();
        return {
          xtype: "datefield",
          fname: name,
          flex: 1,
          value:
            name == "from"
              ? currentDate
              : new Date(currentDate.setDate(currentDate.getDate() + 30)),
          format: this.format,
          isFormField: false,
          emptyText: this.labels[name]
        };
      }
    });
  },

  buildMerchants: function() {
    var me = this;
    this.AccountsStore = Ext.create("Core.data.ComboStore", {
      dataModel: Ext.create("Crm.modules.accounts.model.AccountsModel"),
      fieldSet: ["id", "name"],
      forceSelection: false,
      allowBlank: false,
      scope: this
    });

    return {
      xtype: "combo",
      margin: 10,
      name: "account_id",
      fieldLabel: D.t("Merchant"),
      emptyText: "Select Merchant",
      valueField: "id",
      displayField: "name",
      queryMode: "remote",
      queryParam: "query",
      // triggerAction: "query",
      queryCaching: true,
      queryDelay: 1000,
      typeAhead: true,
      minChars: 1,
      anyMatch: true,

      store: this.AccountsStore,
      listeners: {
        change: (el, v) => {
          const merchantAccountForm = me.down("[name=FiltersPanel]");
          merchantAccountForm.down("[name=merchant_account_id]").clearValue();
          const proxyFilters = {
            filters: [
              {
                _property: "account_id",
                _value: v,
                _operator: "eq"
              }
            ]
          };
          const store = Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create(
              "Crm.modules.accounts.model.MerchantAccountsModel"
            ),
            fieldSet: ["id", "name"],
            exProxyParams: proxyFilters,
            scope: me
          });

          merchantAccountForm
            .down("[name=merchant_account_id]")
            .setStore(store);
        }
      }
    };
  },
  buildMerchantsAccounts: function() {
    var me = this;
    return {
      xtype: "combo",
      margin: 10,
      name: "merchant_account_id",
      fieldLabel: D.t("Merchant Account"),
      emptyText: "Select Merchant Account",
      valueField: "id",
      displayField: "name",
      queryMode: "remote",
      queryParam: "query",
      queryCaching: true,
      queryDelay: 1000,
      typeAhead: true,
      minChars: 1,
      anyMatch: true
    };
  }
});
