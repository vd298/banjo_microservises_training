Ext.define(
  "Crm.modules.transfersFeeReport.view.TransfersFeeReportGridController",
  {
    extend: "Core.grid.GridController",

    init: function(view) {
      var me = this;
      if (!this.compDomain) this.compDomain = new Ext.app.domain.View(this);
      this.view = view;

      this.setControls();
    },

    setControls: function() {
      var me = this;
      this.control({
        "[action=update]": {
          click: function() {
            me.onFiltersChange();
          }
        }
      });
    },

    onFiltersChange: function() {
      var me = this,
        vals = this.view.down("form").getValues(),
        searchParams;
      if (!this.view.down("form").isValid()) {
        D.a("System message", "Check if all parameters are valid!");
        return;
      } else {
        this.view.model.runOnServer(
          "_getFeeTransfers",
          {
            merchant_account_id: vals.merchant_account_id,
            from_date: vals.period.from,
            to_date: vals.period.to
          },
          async function(res) {
            if (res && res.transfers) {
              me.view.down("[name=list]").store.loadData([]);
              console.log(res);
              me.view.down("[name=list]").store.loadData(res.transfers, true);
            }
          }
        );
      }
    }
  }
);
