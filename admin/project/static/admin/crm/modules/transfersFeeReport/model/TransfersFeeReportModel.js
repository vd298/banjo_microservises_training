Ext.define("Crm.modules.transfersFeeReport.model.TransfersFeeReportModel", {
  extend: "Crm.classes.DataModel",

  /* scope:server */
  async $_getFeeTransfers(data, cb) {
    try {
      var me = this;
      console.log(
        `File:TransfersFeeReportModel.js Func:$_getFeeTransfers. Making API call to _getFeeTransfers`,
        data
      );
      let res = await this.callApi({
        service: "account-service",
        method: "_getFeeTransfers",
        data,
        options: {
          realmId: me.user.profile.realm_id
        }
      });
      console.log(
        `File:TransfersFeeReportModel.js Func:$_getFeeTransfers.Received Response`
      );
      if (res && typeof res == "object") {
        console.log(JSON.stringify(res));
      }
      return cb(res.result);
    } catch (err) {
      console.error(
        `TransfersFeeReportModel.js _getFeeTransfers, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      return cb(err);
    }
  }
});
