Ext.define("Crm.modules.countries.view.CountriesFormController", {
  extend: "Core.form.FormController",
  setControls() {
    this.control({
      "[name=abbr2]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();
          formData.changeCol = "abbr2";
          formData.changeVal = formData.abbr2;
          this.checker(formData, me, e);
        }
      },
      "[name=code]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();
          formData.changeCol = "code";
          formData.changeVal = formData.code;

          this.checker(formData, me, e);
        }
      },
      "[name=abbr3]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();
          formData.changeCol = "abbr3";
          formData.changeVal = formData.abbr3;
          this.checker(formData, me, e);
        }
      }
    });
    this.callParent(arguments);
  },
  checker(formData, me, e) {
    me.model.runOnServer("uniqueCheck", formData, function(res) {
      if (!res.result) {
        e.validator = function(value) {
          return res.message;
        };
      } else {
        e.validator = function(value) {
          return true;
        };
      }
      me.view.down("form").isValid();
    });
  }
});
