Ext.define("Crm.modules.countries.view.CountriesGrid", {
  extend: "Core.grid.GridContainer",

  filterable: true,
  filterbar: true,
  title: D.t("Countries"),
  iconCls: "x-fa fa-dollar",

  controllerCls: "Crm.modules.countries.view.CountriesGridController",

  buildColumns: function() {
    return [
      {
        text: D.t("Id"),
        sortable: true,
        dataIndex: "id",
        hidden: true
      },
      {
        text: D.t("Code"),
        flex: 1,
        sortable: true,
        dataIndex: "code",
        filter: true
      },
      {
        text: D.t("ISO 2 Country Code"),
        flex: 1,
        sortable: true,
        dataIndex: "abbr2",
        filter: true
      },
      {
        text: D.t("Name"),
        flex: 1,
        sortable: true,
        dataIndex: "name",
        filter: true
      },
      {
        text: D.t("ISO 3 Country Code"),
        flex: 1,
        sortable: true,
        dataIndex: "abbr3",
        filter: true
      }
    ];
  }
});
