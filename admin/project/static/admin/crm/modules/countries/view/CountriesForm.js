Ext.define("Crm.modules.countries.view.CountriesForm", {
  extend: "Core.form.FormWindow",

  titleTpl: D.t("Country: {abbr2}"),
  controllerCls: "Crm.modules.countries.view.CountriesFormController",
  formMargin: 5,

  width: 550,
  height: 220,

  syncSize: function() {},

  buildItems() {
    return [
      {
        name: "id",
        hidden: true
      },
      {
        name: "name",
        fieldLabel: D.t("Country Name"),
        maxLength: 50,
        allowBlank: false
      },
      {
        name: "abbr2",
        fieldLabel: D.t("ISO 2 Country Code"),
        maxLength: 2,
        allowBlank: false
      },
      {
        name: "code",
        fieldLabel: D.t("Code"),
        maxLength: 50,
        allowBlank: false,
        maskRe: /^[\-\+0-9]{1,15}$/
      },
      {
        name: "abbr3",
        fieldLabel: D.t("ISO 3 Country Code"),
        maxLength: 3,
        allowBlank: false
      }
    ];
  }
});
