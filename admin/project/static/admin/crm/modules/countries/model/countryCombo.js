Ext.define("Crm.modules.countries.model.countryCombo", {
  extend: "Crm.modules.countries.model.CountriesModel",

  getData: async function(params, cb) {
    let query = "SELECT name,abbr2 from countries where removed=0";
    // query = await this.buildRealmConditionQuery(query);

    const res = await this.src.db.query(query, function(err, resp) {
      if (err) {
        console.error(
          "CurrencyCombo.js. Func:getData. Database Error. Error",
          err
        );
        return cb({ total: 0, list: [] });
      }
      return cb({ total: resp.length, list: resp.rows });
    });
  }
});
