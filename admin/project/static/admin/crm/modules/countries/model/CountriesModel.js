Ext.define("Crm.modules.countries.model.CountriesModel", {
  extend: "Core.data.DataModel",

  collection: "countries",
  idField: "id",

  strongRequest: false,

  fields: [
    {
      name: "id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "code",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "abbr2",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },

    {
      name: "name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true,
      sort: 1
    },
    {
      name: "abbr3",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    }
  ],
  /* scope:server*/
  async $uniqueCheck(data, cb) {
    const res = await this.src.db.query(
      `select * from countries where removed=0 and LOWER(${data.changeCol})=LOWER('${data.changeVal}') and id!='${data.id}'`
    );
    if (res && res.length) {
      let message =
        data.changeCol === "abbr2"
          ? "ISO country code 2"
          : data.changeCol === "abbr3"
          ? "ISO country code 3"
          : data.changeCol;
      return cb({
        result: false,

        message: `${message} must be unique`
      });
    } else {
      return cb({ result: true });
    }
  }
});
