Ext.define("Crm.modules.lostTransactions.view.LostTransactionsForm", {
  extend: "Core.form.FormWindow",

  titleTpl: D.t("Lost Transactions"),
  iconCls: "x-fa fa-bank",
  controllerCls:
    "Crm.modules.lostTransactions.view.LostTransactionsFormController",
  model: "Crm.modules.lostTransactions.model.LostTransactionsModel",
  buildItems: function() {
    return [
      {
        name: "id",
        hidden: true
      },
      {
        name: "bank_name",
        fieldLabel: D.t("Bank Name"),
        readOnly: true,
        inputWrapCls: "",
        triggerWrapCls: "",
        fieldStyle: "background:none;padding: 0"
      },
      {
        xtype: "combo",
        queryMode: "all",
        name: "bank_account_id",
        fieldLabel: D.t("Bank Account"),
        readOnly: true,
        inputWrapCls: "",
        triggerWrapCls: "",
        fieldStyle: "background:none;padding: 0",

        store: Ext.create("Core.data.ComboStore", {
          dataModel: Ext.create(
            "Crm.modules.accountsPool.model.AccountsPoolModel"
          ),
          fieldSet: ["id", "account_name", "acc_no", "currency", "status"],
          scope: this
        }),
        valueField: "id",
        displayField: "account_name",
        tpl:
          '<tpl for="."><div class="x-boundlist-item" style="height:40px;"><b>{account_name}</b>&nbsp;&nbsp;(A/C: {acc_no})&nbsp;(Status: {status})</div><div style="clear:both"></div></tpl>'
      },
      {
        name: "acc_no",
        fieldLabel: D.t("Account Number"),
        readOnly: true,
        inputWrapCls: "",
        triggerWrapCls: "",
        fieldStyle: "background:none;padding: 0"
      },
      {
        name: "brn",
        fieldLabel: D.t("Bank Reference Number"),
        readOnly: true,
        inputWrapCls: "",
        triggerWrapCls: "",
        fieldStyle: "background:none;padding: 0"
      },
      {
        name: "transaction_date",
        fieldLabel: D.t("Transaction Date"),
        readOnly: true,
        inputWrapCls: "",
        triggerWrapCls: "",
        fieldStyle: "background:none;padding: 0"
      },
      {
        name: "processed",
        fieldLabel: D.t("Activity"),
        readOnly: true,
        inputWrapCls: "",
        triggerWrapCls: "",
        fieldStyle: "background:none;padding: 0",
        xtype: "combo",
        flex: 1,
        valueField: "key",
        displayField: "type",
        value: 0,
        store: {
          fields: ["key", "type"],
          data: [
            { key: 0, type: "Pending" },
            { key: 1, type: "Accepted" },
            { key: 2, type: "Rejected" }
          ]
        }
      },
      {
        xtype: "dependedcombo",
        name: "admin_id",

        fieldLabel: D.t("Admin"),
        editable: true,
        fieldSet: "name,_id",
        valueField: "_id",
        displayField: "name",
        parentEl: null,
        parentField: null,
        dataModel: "Crm.modules.lostTransactions.model.AdminCombo",

        readOnly: true,
        inputWrapCls: "",
        triggerWrapCls: "",
        fieldStyle: "background:none;padding: 0"
      },
      {
        flex: 1,
        xtype: "combo",
        name: "account_id",
        allowBlank: false,
        fieldLabel: D.t("Select Merchant"),
        store: Ext.create("Core.data.ComboStore", {
          dataModel: Ext.create("Crm.modules.accounts.model.AccountsModel"),
          fieldSet: ["id", "name"],
          scope: this
        }),
        valueField: "id",
        displayField: "name",
        validator: function(value) {
          return true;
        },
        listeners: {
          change: (el, v) => {
            const processed = this.down("[name=processed]").getValue();
            if (!processed) {
              this.down("[name=merchant_account_id]").clearValue();
            }
            const proxyFilters = {
              filters: [{ _property: "account_id", _value: v, _operator: "eq" }]
            };
            const store = Ext.create("Core.data.ComboStore", {
              dataModel: Ext.create(
                "Crm.modules.accounts.model.MerchantAccountsModel"
              ),
              fieldSet: ["id", "name"],
              exProxyParams: proxyFilters,
              scope: this
            });
            this.down("[name=merchant_account_id]").setStore(store);
          }
        }
      },
      {
        flex: 1,
        xtype: "combo",
        name: "merchant_account_id",
        fieldLabel: D.t("Select Merchant Account"),
        valueField: "id",
        allowBlank: false,
        displayField: "name",
        validator: function(value) {
          return true;
        }
      },
      {
        name: "amount",
        fieldLabel: D.t("Amount"),
        allowBlank: false,
        readOnly: true,
        maskRe: /[0-9]/,
        validator: function(value) {
          return true;
        }
      },
      {
        xtype: "textarea",
        name: "note",
        maxLength: 150,
        fieldLabel: D.t("Approve Note")
      }
    ];
  },
  buildButtons: function() {
    var btns = [
      "->",
      {
        text: D.t("Approve"),
        iconCls: "x-fa fa-check-square-o",
        action: "save",
        disabled: true
      },
      {
        text: D.t("Link"),
        iconCls: "x-fa fa-external-link-square",
        action: "link"
      },
      "-",
      {
        text: D.t("Reject"),
        iconCls: "x-fa fa-check-square-o",
        action: "reject"
      },

      "-",
      {
        text: D.t("Close"),
        iconCls: "x-fa fa-ban",

        action: "formclose"
      }
    ];
    return btns;
  }
});
