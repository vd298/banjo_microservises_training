Ext.define("Crm.modules.lostTransactions.view.LostTransactionsFormController", {
  extend: "Core.form.FormController",

  setControls: function() {
    this.control({
      "[action=save]": {
        click: async () => {
          const me = this;
          me.accept();
        }
      },
      "[action=reject]": {
        click: async () => {
          const me = this;
          me.reject();
        }
      },
      "[action=link]": {
        click: async () => {
          const me = this;
          me.link();
        }
      }
    });
    this.callParent(arguments);
  },
  afterDataLoad(data, cb) {
    if (data.processed && data.processed != 0) {
      this.view.down("[action=save]").setDisabled(true);
      this.view.down("[action=reject]").setDisabled(true);
      this.view.down("[action=link]").setDisabled(true);
      this.view.down("[name=account_id]").setReadOnly(true);
      this.view.down("[name=merchant_account_id]").setReadOnly(true);
    }
    cb(data);
  },
  setAcceptRejectDisabled() {
    this.view.down("[action=save]").setDisabled(true);
    this.view.down("[action=reject]").setDisabled(true);
    this.view.down("[action=link]").setDisabled(true);
    this.loadData();
  },
  async link() {
    var me = this;
    let currentData = me.view.down("form").getValues();
    var linkWindow = Ext.create("Ext.window.Window", {
      title: "Link Transfer:",
      layout: "fit",
      width: "80vh",
      //margin: 30,
      modal: true,
      items: [
        {
          xtype: "form",
          layout: "anchor",
          //margin: 10,
          defaults: {
            xtype: "textfield",
            anchor: "100%",
            labelWidth: 150,
            padding: 10
          },
          items: [
            {
              name: "admin_id",
              value: localStorage && localStorage.uid ? localStorage.uid : null,
              hidden: true,
              allowBlank: false
            },
            {
              name: "transfer_id",
              fieldLabel: D.t("Rejecting Transfer Id: *"),
              emptyText: "Transfer Id cannot be blank",
              allowBlank: false
            },
            {
              name: "brn",
              fieldLabel: D.t("Bank reference number"),
              emptyText: "Bank reference number cannot be blank",
              allowBlank: false,
              readOnly: true,
              value: currentData.brn
            },
            {
              xtype: "textarea",
              name: "note",
              maxLength: 150,
              fieldLabel: D.t("link Note")
            },
            {
              xtype: "fieldcontainer",
              anchor: "100%",
              layout: "hbox",
              align: "center",
              margin: { top: 15, bottom: 5 },
              items: [
                {
                  xtype: "button",
                  text: D.t("OK"),
                  formBind: true,
                  listeners: {
                    click: async function(e, v) {
                      var actionBtn = e;
                      if (actionBtn) {
                        actionBtn.disable();
                      }
                      var reqData = this.up("form").getValues() || {};

                      reqData.id = currentData.id;
                      me.model.runOnServer(
                        "callActivityLink",
                        reqData,
                        function(Resp) {
                          let defaultMsg = `Selected transfer`;
                          if (Resp && Resp.result) {
                            defaultMsg += ` has been successfully marked as link in the system`;
                            D.a("Transfer Link Successful", defaultMsg);
                            me.setAcceptRejectDisabled();
                          } else {
                            defaultMsg += ` has failed to be marked. Please try again or Contact Support`;
                            if (Resp && Resp.error && Resp.error.message) {
                              Resp.message = Resp.error.message;
                            }
                            D.a(
                              "Transfer Link Failed",
                              Resp.message || defaultMsg
                            );
                          }
                          linkWindow.close();
                        }
                      );
                    }
                  }
                },
                {
                  xtype: "button",
                  margin: { left: 5 },
                  //padding: 10,
                  text: D.t("Cancel"),
                  listeners: {
                    click: function() {
                      linkWindow.close();
                    }
                  }
                }
              ]
            }
          ]
        }
      ]
    }).show();
  },
  async accept() {
    const me = this;
    const reqData = me.view.down("form").getValues();
    await me.model.runOnServer("callActivityApprove", reqData, async function(
      acceptResp
    ) {
      let defaultMsg = `Selected transfer`;

      if (
        acceptResp &&
        acceptResp.result &&
        acceptResp.result.status == "APPROVED"
      ) {
        defaultMsg += ` has been successfully marked as accepted in the system`;
        D.a("Transfer Accept Successful", acceptResp.message || defaultMsg);
        me.setAcceptRejectDisabled();
      } else {
        defaultMsg += ` has failed to be marked. Please try again or Contact Support`;
        if (acceptResp && acceptResp.error && acceptResp.error.message) {
          acceptResp.message = acceptResp.error.message;
        }
        D.a("Transfer Accept Failed", acceptResp.message || defaultMsg);
      }
    });
  },
  async reject() {
    const me = this;
    const reqData = me.view.down("form").getValues();
    console.log("reqData", reqData);
    await this.model.runOnServer("callActivityReject", reqData, function(
      rejectResp
    ) {
      let defaultMsg = `Selected transfer`;
      if (rejectResp && rejectResp.result) {
        defaultMsg += ` has been successfully marked as rejected in the system`;
        D.a("Transfer Rejected Successful", defaultMsg);
        me.setAcceptRejectDisabled();
      } else {
        defaultMsg += ` has failed to be marked. Please try again or Contact Support`;
        if (rejectResp && rejectResp.error && rejectResp.error.message) {
          rejectResp.message = rejectResp.error.message;
        }
        D.a("Transfer Rejected Failed", rejectResp.message || defaultMsg);
      }
    });
  }
});
