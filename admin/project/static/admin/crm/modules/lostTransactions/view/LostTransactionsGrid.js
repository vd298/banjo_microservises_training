Ext.define("Crm.modules.lostTransactions.view.LostTransactionsGrid", {
  extend: "Core.grid.GridContainer",
  title: D.t("Lost Transactions"),
  iconCls: "x-fa fa-bank",

  filterable: true,
  filterbar: true,

  buildColumns: function() {
    const typeArray = [
      { key: 0, name: D.t("Pending") },
      { key: 1, name: D.t("Accepted") },
      { key: 2, name: D.t("Rejected") }
    ];
    this.typesStore = Ext.create("Ext.data.Store", {
      fields: ["key", "name"],
      data: typeArray
    });
    return [
      {
        name: "id",
        type: "ObjectID",
        dataIndex: "id",
        sortable: true,
        filter: true,
        hidden: true
      },
      {
        text: D.t("Bank Name"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "bank_name"
      },
      {
        text: D.t("Bank Account"),
        flex: 1,
        sortable: true,

        dataIndex: "bank_account_id",
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "id",
          displayField: "account_name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create(
              "Crm.modules.accountsPool.model.AccountsPoolModel"
            ),
            fieldSet: ["id", "account_name", "acc_no", "currency", "status"],
            scope: this
          }),
          operator: "eq"
        },
        renderer: function(v, m, r) {
          if (v) {
            return (
              '<a href="#Crm.modules.bankAccounts.view.BankAccountsForm~' +
              v +
              '">' +
              r.data.bank_account_name +
              "</a> "
            );
          }
          return v;
        }
      },
      {
        text: D.t("Account number"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "acc_no"
      },
      {
        text: D.t("Activity"),
        flex: 1,
        sortable: true,
        dataIndex: "processed",
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "key",
          displayField: "name",
          store: this.typesStore,
          operator: "eq"
        },
        renderer: function(v, m, r) {
          if (v) {
            if (typeArray[v].name) {
              return typeArray[v].name;
            }
          }
          return "Pending";
        }
      },
      {
        text: D.t("Bank Reference Number"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "brn"
      },
      {
        text: D.t("amount"),
        flex: 1,
        sortable: true,
        filter: { xtype: "numberfield" },
        dataIndex: "amount"
      }
    ];
  },

  buildButtonsColumns: function() {
    var me = this;
    return [
      {
        xtype: "actioncolumn",
        width: 30,
        menuDisabled: true,
        items: [
          {
            iconCls: "x-fa fa-pencil-square-o",
            tooltip: this.buttonEditTooltip,
            isDisabled: function() {
              return !me.permis.modify && !me.permis.read;
            },
            handler: function(grid, rowIndex) {
              me.fireEvent("edit", grid, rowIndex);
            }
          }
        ]
      }
    ];
  },
  buildTbar: function() {
    var items = [];

    if (this.importButton) {
      items.push("-", {
        text: this.buttonImportText,
        iconCls: "x-fa fa-cloud-download",
        action: "import"
      });
    }
    if (this.exportButton) {
      items.push("-", {
        text: this.buttonExportText,
        iconCls: "x-fa fa-cloud-upload",
        action: "export"
      });
    }

    items.push({
      tooltip: this.buttonReloadText,
      iconCls: "x-fa fa-refresh",
      action: "refresh"
    });

    if (this.filterable) items.push("->", this.buildSearchField());
    return items;
  }
});
