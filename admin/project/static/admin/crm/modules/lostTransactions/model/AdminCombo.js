Ext.define("Crm.modules.lostTransactions.model.AdminCombo", {
  extend: "Crm.modules.lostTransactions.model.LostTransactionsModel",

  getData: async function(params, cb) {
    let sql = "SELECT name,_id from admin_users where removed = 0";
    sql = await this.buildRealmConditionQuery(sql);
    const res = await this.src.db.query(sql, function(err, resp) {
      if (err) {
        console.error(
          "AdminCombo.js. Func:getData. Database Error. Error",
          err
        );
        return cb({ total: 0, list: {} });
      }
      return cb({ total: resp.length, list: resp.rows });
    });
  }
});
