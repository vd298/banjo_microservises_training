Ext.define("Crm.modules.lostTransactions.model.LostTransactionsModel", {
  extend: "Crm.classes.DataModel",

  collection: "lost_transactions",
  idField: "id",
  foreignKeyFilter: [
    {
      collection: "bank_accounts",
      alias: "b",
      on: " bank_accounts.id = lost_transactions.bank_account_id ",
      type: "inner"
    },
    {
      collection: "banks",
      alias: "b",
      on: " banks.id = bank_accounts.bank_id ",
      type: "inner"
    }
  ],

  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "account_id",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "merchant_account_id",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "transfer_id",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "amount",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "brn",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "bank_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "domain_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "bank_account_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "acc_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "transaction_date",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "processed",
      type: "integer",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "admin_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "transaction_data",
      type: "jsonb",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "note",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  /* scope:server */
  async afterGetData(data, cb) {
    var me = this;
    const Util = Ext.create("Crm.Util", { scope: me });
    if (!!data && data.length) {
      let merchant_account_id = [];
      data.map((item) => {
        item.id && merchant_account_id.push(`'${item.id}'`);
      });

      let sqlPlaceHolders = [];
      let sql = `SELECT lt.*,ma.name as merchant_account_name,ba.account_name as bank_account_name FROM lost_transactions lt left join merchant_accounts ma ON ma.id = lt.merchant_account_id AND ma.removed = 0 join bank_accounts ba on ba.id = lt.bank_account_id WHERE ${Util.pushArray(
        data.map((e) => {
          if (typeof e.id == "string" && e.id.length) {
            return e.id;
          } else return null;
        }),
        sqlPlaceHolders,
        { colName: "lt.id" }
      )} `;

      
      const resp = await this.src.db.query(
        `${sql} ORDER BY lt.ctime DESC`,
        sqlPlaceHolders
      );
      cb(resp);
    }
    return cb(data);
  },
  /* scope:server */
  async write(data, cb) {
    cb(null);
  },

  /* scope:server */
  async $callActivityApprove(data, cb) {
    try {
      var me = this;

      if (data && data.id) {
        if (data.processed) {
          return cb({
            result: false,
            message: "Transaction failed, this transaction is already processed"
          });
        }
        const params = {
          service: "merchant-service",
          method: "executeLostTransactions",
          data: data,
          options: {
            realmId: me.user.profile.realm_id
          }
        };
        var res = await me.callApi(params);

        if (res) {
          this.changeModelData(
            "Crm.modules.Transfers.model.TransfersModel",
            "ins",
            {}
          );
          this.changeModelData(
            "Crm.modules.lostTransactions.model.LostTransactionsModel",
            "ins",
            {}
          );

          if (res && res.result && res.result.status == "APPROVED") {
            const resp = await this.src.db.query(
              `UPDATE lost_transactions SET processed = $1,admin_id = $2,transfer_id=$3,merchant_account_id=$4,account_id=$5  WHERE id = $6;`,
              [
                1,
                me.user.id,
                res.result.transfer_id,
                data.merchant_account_id,
                data.account_id,
                data.id
              ]
            );
          }
        }
        return cb(res);
      } else {
        return cb({ result: false, message: "Transaction id missing" });
      }
    } catch (error) {
      return cb({
        result: false,
        message:
          "Something went wrong and server could not process your request"
      });
    }
  },
  async $callActivityLink(data, cb) {
    try {
      var me = this;

      if (data && data.transfer_id && data.id) {
        if (
          typeof data.transfer_id == "string" &&
          data.transfer_id.length == 36
        ) {
          let sql = `select vw_tx_transfer* from vw_tx_transfer JOIN accounts a on a.id = vw_tx_transfer.account_id  `;
          sql = await this.buildRealmConditionQuery(sql, "a.realm_id");
          sql += ` where vw_tx_transfer.transfer_id= $1 `;
          const resp = await this.src.db.query(sql, [data.transfer_id]);

          if (
            resp &&
            resp.length &&
            resp[0].account_id &&
            resp[0].merchant_account_id
          ) {
            const res = await this.src.db.query(
              `UPDATE lost_transactions SET processed = $1,admin_id = $2,transfer_id=$3,merchant_account_id=$4,account_id=$5  WHERE id = $6;`,
              [
                1,
                me.user.id,
                data.transfer_id,
                resp[0].merchant_account_id,
                resp[0].account_id,
                data.id
              ]
            );

            if (res) {
              this.changeModelData(
                "Crm.modules.Transfers.model.TransfersModel",
                "ins",
                {}
              );
              this.changeModelData(
                "Crm.modules.lostTransactions.model.LostTransactionsModel",
                "ins",
                {}
              );
              return cb({
                result: true
              });
            } else {
              return cb({ result: false, message: "Transaction missing" });
            }
          }
        }
        return cb({ result: false, message: "Invalid transfer Id" });
      } else {
        return cb({ result: false, message: "Transaction id missing" });
      }
    } catch (error) {
      return cb({
        result: false,
        message:
          "Something went wrong and server could not process your request"
      });
    }
  },
  async $callActivityReject(data, cb) {
    var me = this;

    if (data && data.id) {
      const res = await this.src.db.query(
        `UPDATE lost_transactions SET processed = $1,admin_id = $2 WHERE id = $3;`,
        [2, me.user.id, data.id]
      );

      if (res) {
        this.changeModelData(
          "Crm.modules.Transfers.model.TransfersModel",
          "ins",
          {}
        );
        this.changeModelData(
          "Crm.modules.lostTransactions.model.LostTransactionsModel",
          "ins",
          {}
        );
        return cb({
          result: true
        });
      } else {
        return cb({ result: false, message: "Transaction missing" });
      }
    } else {
      return cb({ result: false, message: "Transaction id missing" });
    }
  }
});
