Ext.define("Crm.modules.Util.model.FetchComboRecords", {
  extend: "Crm.classes.DataModel",

  collection: "",
  idField: "id",
  fields: [],

  getData: async function(params, cb) {
    let me = this,
      sql = ``,
      sqlPlaceholders = [];
    if (params && params.enum_name) {
      sql = `SELECT distinct on (e.enumsortorder) e.enumlabel as key
        FROM pg_enum e
        JOIN pg_type t ON e.enumtypid = t.oid
      WHERE t.typname = $1  order by e.enumsortorder `;
      sqlPlaceholders = [params.enum_name];
    } else if (params && params.fieldSet && params.collection) {
      sql = ` select ${params.fieldSet} from ${params.collection}`;
    } else {
      console.error(
        `FetchComboRecords.js. Func:getData. Invalid filters:`,
        params
      );
      return cb({ total: 0, list: [] });
    }
    if (params.removedCheck && sql.length) {
      sql += ` where removed=0`;
    }
    if (!params || !params.enum_name) {
      let preSql = null;
      if (params.collection === "realms") {
        preSql = "id";
        sql = await this.buildRealmConditionQuery(sql, preSql);
      } else if (me.getRealmsIDTableColumnName(params.collection)) {
        sql = await this.buildRealmConditionQuery(
          sql,
          me.getRealmsIDTableColumnName(params.collection)
        );
      }
    }
    me.src.db.query(sql, sqlPlaceholders, function(err, resp) {
      if (err) {
        console.error("FetchComboRecords.js. Func:getData. DB Error:", err);
        return cb({ total: 0, list: [] });
      } else if (resp && resp.length) {
        return cb({ total: resp.length, list: resp });
      } else {
        return cb({ total: 0, list: [] });
      }
    });
  }
});
