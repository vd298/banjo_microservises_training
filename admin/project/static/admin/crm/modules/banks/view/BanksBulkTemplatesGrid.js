Ext.define("Crm.modules.banks.view.BanksBulkTemplatesGrid", {
  extend: "Core.grid.GridContainer",

  filterable: true,
  filterbar: true,

  buildColumns: function() {
    return [
      {
        name: "id",
        hidden: true
      },
      {
        text: D.t("Name"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "template_name"
      },
      {
        text: D.t("Protocol"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "protocol_id",
        hidden: true
      },
      {
        text: D.t("Description"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "description"
      }
    ];
  }
});
