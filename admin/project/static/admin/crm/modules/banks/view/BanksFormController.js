Ext.define("Crm.modules.banks.view.BanksFormController", {
  extend: "Core.form.FormController",

  setControls: function() {
    this.control({
      "[action=formclose]": {
        click: () => {
          this.closeView();
        }
      },
      "[action=apply]": {
        click: () => {
          this.save(false, (res) => {
            //D.a("Message","Data saved")
          });
        }
      },
      "[action=save]": {
        click: () => {
          this.save(true);
        }
      },
      "[action=copy]": {
        click: () => {
          this.copyRecord(true);
        }
      },
      "[action=gotolist]": {
        click: () => {
          this.gotoListView(true);
        }
      },
      "[action=exportjson]": {
        click: () => {
          this.exportJson();
        }
      },
      "[action=importjson]": {
        change: (el) => {
          this.importJson();
        }
      },
      "[name=short_name]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();

          this.model.runOnServer("uniqueBankEntries", formData, function(res) {
            if (!res.result) {
              e.validator = function(value) {
                return res.message;
              };
            } else {
              e.validator = function(value) {
                return true;
              };
            }
            me.view.down("form").isValid();
          });
        }
      },
      "[action=remove]": {
        click: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();
          me.model.runOnServer(
            "checkAssociatedBankAccounts",
            { id: formData.id },
            (res) => {
              if (!!res && res.length) {
                let component = Ext.Msg.alert(
                  "Delete Error!",
                  `Bank cannot be deleted as it is assigned to ${res.length} bank accounts. Please de-assign before deleting.`
                );
                Ext.WindowManager.bringToFront(component);
                Ext.WindowManager.getNextZSeed();
              } else {
                this.deleteRecord_do(true);
              }
            }
          );
        }
      }
    });

    this.view.on("activate", (grid, indx) => {
      if (!this.oldDocTitle) this.oldDocTitle = document.title;
      var form = this.view.down("form").getForm();
      if (form) {
        data = form.getValues();
        this.setTitle(data);
      }
    });
    this.view.on("close", (grid, indx) => {
      if (this.oldDocTitle) document.title = this.oldDocTitle;
    });
    this.view.down("form").on({
      validitychange: (th, valid, eOpts) => {
        var el = this.view.down("[action=apply]");
        if (el) el.setDisabled(!valid);
        el = this.view.down("[action=save]");
        if (el) el.setDisabled(!valid);
      }
    });
    this.checkPermissions();
    this.view.on("disablePanels", (data, cmp) => {
      var tabPanel = Ext.getCmp(cmp);
      var items = tabPanel.items.items;

      for (var i = 1; i < items.length; i++) {
        var panel = items[i];
        panel.setDisabled(true);
      }
    });
  },
  afterDataLoad(data, cb) {
    var me = this;
    if (me && data && data.ctime == undefined) {
      me.view.fireEvent("disablePanels", data, "BanksFormTabPanel");
    }
    cb(data);
  }
});
