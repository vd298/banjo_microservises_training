Ext.define("Crm.modules.banks.view.BanksGridController", {
  extend: "Core.grid.GridController",
  setControls() {
    let me = this;
    console.log("Inside controller");
    this.view.on("delete", function(grid, indx) {
      console.log("Inside Delete");
      me.model.runOnServer(
        "checkAssociatedBankAccounts",
        { id: grid.getRecord(indx).id },
        (res) => {
          if (!!res && res.length) {
            D.a(
              "Delete Error",
              `Bank cannot be deleted as it is assigned to ${res.length} bank accounts. Please de-assign before deleting.`,
              []
            );
          }
        }
      );
    });
    this.callParent(arguments);
  }
});
