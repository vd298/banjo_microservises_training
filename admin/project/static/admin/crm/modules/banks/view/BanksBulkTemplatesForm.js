Ext.define("Crm.modules.banks.view.BanksBulkTemplatesForm", {
  extend: "Core.form.FormWindow",
  requires: ["Desktop.core.widgets.GridField"],
  formLayout: "fit",
  formMargin: 0,
  width: 450,
  height: 450,
  syncSize: function() {},
  buildItems() {
    const me = this;
    return {
      xtype: "tabpanel",
      layout: "fit",
      items: [this.buildTemplateForm(me)],
      autoEl: { "data-test": "tab-items" }
    };
  },
  buildTemplateForm(me) {
    return {
      xtype: "form",
      layout: "anchor",
      name: "bulk_template",
      title: D.t("Bulk template"),
      defaults: {
        anchor: "100%",
        xtype: "textfield",
        labelWidth: 150,
        margin: 5
      },
      items: [
        {
          name: "id",
          hidden: true
        },
        {
          name: "bank_id",
          hidden: true
        },
        {
          name: "template_name",
          fieldLabel: D.t("Template name"),
          allowBlank: false,
          listeners: {
            change: function(e, newVal, oldVal, eOpts) {
              me.validateHeaders(me);
            }
          }
        },
        this.buildProtocolCombo(),
        {
          name: "description",
          fieldLabel: D.t("Description")
        },
        {
          xtype: "checkbox",
          name: "headers",
          width: 100,
          fieldLabel: D.t("Add headers"),
          listeners: {
            change: function(e, newVal, oldVal, eOpts) {
              if (newVal) {
                me.down("[name='headers_mapping_data']").setHidden(false);
                me.down("[name='headers_data']").setHidden(true);
                me.down("[name='headers_mapping_data']").setValue(
                  me.down("[name='headers_data']").value
                );
              } else {
                me.down("[name='headers_mapping_data']").setHidden(true);
                me.down("[name='headers_data']").setHidden(false);
                me.down("[name='headers_data']").setValue(
                  me.down("[name='headers_mapping_data']").value
                );
              }
            }
          }
        },
        this.buildMappingGrid(),
        this.buildGrid()
      ]
    };
  },
  buildProtocolCombo() {
    const me = this;
    return {
      name: "protocol_id",
      fieldLabel: D.t("Payment protocol"),
      xtype: "combo",
      editable: false,
      allowBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "all",
      displayField: "name",
      flex: 1,
      valueField: "_id",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create(
          "Crm.modules.bankAccounts.model.PaymentTypeCombo"
        ),
        fieldSet: ["_id", "name"],
        scope: this
      }),
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        },
        change: function(e, newVal, oldVal, eOpts) {
          me.validateHeaders(me);
        }
      }
    };
  },
  buildMappingGrid: function() {
    const me = this;
    const headerStore = __CONFIG__.getBulkTemplateHeaders();
    return {
      xtype: "gridfield",
      hideLabel: true,
      region: "center",
      autoScroll: true,
      scrollable: true,
      hidden: true,
      name: "headers_mapping_data",
      layout: "fit",
      minHeight: 500,
      maxHeight: 501,
      fields: ["header", "field_name"],
      listeners: {
        change: function(e, newVal, oldVal, eOpts) {
          me.changeField(e, newVal, true, headerStore);
        }
      },
      columns: [
        this.buildFieldsCombo(headerStore),
        {
          text: D.t("Header"),
          flex: 1,
          sortable: false,
          dataIndex: "header",
          menuDisabled: true,
          editor: true
        }
      ]
    };
  },

  buildfieldName: function(headerStore) {
    return {
      xtype: "combo",
      name: "field_name",
      forceSelection: false,
      editable: true,
      fieldSet: "_id, name",
      valueField: "_id",
      queryModel: "local",
      displayField: "name",
      allowBlank: false,
      store: headerStore
    };
  },

  buildGrid: function() {
    const me = this;
    const headerStore = __CONFIG__.getBulkTemplateHeaders();
    return {
      xtype: "gridfield",
      hideLabel: true,
      region: "center",
      autoScroll: true,
      scrollable: true,
      name: "headers_data",
      layout: "fit",
      minHeight: 500,
      maxHeight: 501,
      fields: ["header", "field_name"],
      listeners: {
        change: function(e, newVal, oldVal, eOpts) {
          me.changeField(e, newVal, false, headerStore);
        }
      },
      columns: [this.buildFieldsCombo(headerStore)]
    };
  },
  buildFieldsCombo(headerStore) {
    const me = this;
    return {
      text: D.t("Field"),
      flex: 1,
      sortable: false,
      name: "field_name",
      dataIndex: "field_name",
      menuDisabled: true,
      forceSelection: true,
      allowNull: false,
      xtype: "combocolumn",
      model: "Crm.modules.Transfers.model.TransfersModel",
      editor: me.buildfieldName(headerStore),
      renderer: function(v, m, r) {
        if (headerStore && v != undefined) {
          let filteredStore = headerStore
            .getRange()
            .filter((e) => e.data._id == v);
          if (
            filteredStore &&
            filteredStore[0] &&
            filteredStore[0].data &&
            filteredStore[0].data.name
          ) {
            return filteredStore[0].data.name;
          }
        }
        return v;
      }
    };
  },
  changeField: function(e, newVal, mapping, headerStore) {
    const me = this;
    const save = me.down("[action=apply]");
    const form = me.down("[name=bulk_template]");
    let data = e.store.getRange().map((e) => e.data);
    let newName = newVal[0] && newVal[0].field_name.trim().toLowerCase();
    let recordExists = false;
    let uniqueRecord = true;
    let headerPresent = true;
    if (data.length > 0) {
      if (mapping) {
        if (newVal[0].header && newVal[0].header.trim() === "") {
          headerPresent = false;
          Ext.Msg.alert("Error", "Header cannot be empty");
        }
      }
      for (let j = 0; j < headerStore.data.items.length; j++) {
        el = headerStore.data.items[j];
        if (el.data._id === newName) {
          recordExists = true;
          break;
        }
      }
      if (!recordExists) {
        save.setDisabled(true);
        Ext.Msg.alert("Error", "Please select a valid value");
      }
    }

    for (let i = 0; i < data.length; i++) {
      const el = data[i];
      if (i === 0) {
        continue;
      }
      let field_name = el.field_name.trim().toLowerCase();
      if (field_name === newName) {
        uniqueRecord = false;
        Ext.Msg.alert("Error", "Field name must be unique");
        break;
      }
    }
    let disabledStatus = false;
    if (mapping) {
      if (!(recordExists && uniqueRecord && headerPresent)) {
        disabledStatus = true;
      }
    } else {
      if (!(recordExists && uniqueRecord)) {
        disabledStatus = true;
      }
    }
    if (form.isValid() && !disabledStatus) {
      save.setDisabled(false);
    } else {
      save.setDisabled(true);
    }
  },
  validateHeaders: function(me) {
    let headers_data = me.down("[name=headers_data]").value;
    const save = me.down("[action=apply]");
    const form = me.down("[name=bulk_template]");

    if (form.isValid()) {
      if (!headers_data || headers_data.length === 0) {
        save.disable();
      }
    }
  }
});
