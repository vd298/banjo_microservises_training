Ext.define("Crm.modules.banks.view.BanksGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Banks"),
  controllerCls: "Crm.modules.banks.view.BanksGridController",

  filterable: true,
  filterbar: true,

  buildColumns: function() {
    const typeArray = [
      { key: 0, name: D.t("Bank") },
      { key: 1, name: D.t("Exchange") },
      { key: 2, name: D.t("Acquirer") }
    ];
    this.typesStore = Ext.create("Ext.data.Store", {
      fields: ["key", "name"],
      data: typeArray
    });
    return [
      {
        text: D.t("Short name"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "short_name"
      },
      {
        text: D.t("Name"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "name"
      },
      {
        text: D.t("Address"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "address"
      },
      {
        text: D.t("City"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "city"
      },
      {
        text: D.t("Country"),
        flex: 1,
        sortable: true,
        dataIndex: "country",
        filter: {
          xtype: "combo",
          queryMode: "local",
          sortable: true,
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "abbr2",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create("Crm.modules.countries.model.countryCombo"),
            fieldSet: ["name", "abbr2"],
            scope: this
          }),
          operator: "eq"
        }
      },
      {
        text: D.t("Type"),
        flex: 1,
        sortable: true,
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "key",
          displayField: "name",
          store: this.typesStore,
          operator: "eq"
        },
        renderer: function(v, m, r) {
          if (v) {
            if (typeArray[v].name) {
              return typeArray[v].name;
            }
          }
          return "Bank";
        },
        dataIndex: "bank_type"
      }
    ];
  }
});
