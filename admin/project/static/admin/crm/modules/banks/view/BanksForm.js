Ext.define("Crm.modules.banks.view.BanksForm", {
  extend: "Core.form.FormWindow",
  titleTpl: D.t("Banks"),
  requires: ["Desktop.core.widgets.GridField"],
  formLayout: "fit",
  formMargin: 0,
  width: 450,
  height: 450,
  syncSize: function() {},
  controllerCls: "Crm.modules.banks.view.BanksFormController",
  buildItems() {
    const me = this;
    return {
      xtype: "tabpanel",
      id: "BanksFormTabPanel",
      layout: "fit",
      items: [this.buildGeneral(), this.buildTemplateForm(me)],
      autoEl: { "data-test": "tab-items" }
    };
  },
  buildGeneral() {
    return {
      xtype: "panel",
      layout: "anchor",
      title: D.t("General"),
      defaults: {
        anchor: "100%",
        xtype: "textfield",
        labelWidth: 150,
        margin: 5
      },
      items: [
        {
          name: "id",
          hidden: true
        },
        {
          name: "short_name",
          fieldLabel: D.t("Code"),
          validator: true,
          allowBlank: false,
          minLength: 4,
          maxLength: 4,
          regex: /^[A-Z]+$/,
          regexText: "Capital Alphabets accepted only. Max length: 4"
        },
        {
          name: "name",
          fieldLabel: D.t("Name"),
          allowBlank: false,
          maxLength: 50
        },
        {
          name: "address",
          fieldLabel: D.t("Address"),
          allowBlank: false,
          maxLength: 150
        },
        {
          name: "city",
          fieldLabel: D.t("City"),
          allowBlank: false,
          maxLength: 50
        },
        this.buildCountryCombo(),
        {
          xtype: "combo",
          name: "bank_type",
          allowBlank: false,
          flex: 1,
          fieldLabel: D.t("Type"),
          valueField: "key",
          displayField: "type",
          value: 0,
          store: {
            fields: ["key", "type"],
            data: [
              { key: 0, type: "Bank" },
              { key: 1, type: "Exchange" },
              { key: 2, type: "Acquirer" }
            ]
          }
        }
      ]
    };
  },
  buildCountryCombo() {
    return {
      name: "country",
      fieldLabel: D.t("Country"),
      xtype: "combo",
      editable: true,
      allowBlank: false,
      validateBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "local",
      displayField: "name",
      flex: 1,
      valueField: "abbr2",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.countries.model.countryCombo"),
        fieldSet: ["name", "abbr2"],
        scope: this
      }),
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      }
    };
  },
  buildTemplateForm(scope) {
    return {
      xtype: "panel",
      layout: "fit",
      title: D.t("Bulk template"),
      name: "bulk_templates_tab",
      items: Ext.create("Crm.modules.banks.view.BanksBulkTemplatesGrid", {
        scope: scope,
        observe: [{ property: "bank_id", param: "id" }]
      })
    };
  }
});
