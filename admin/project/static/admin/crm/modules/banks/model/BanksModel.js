Ext.define("Crm.modules.banks.model.BanksModel", {
  extend: "Crm.classes.DataModel",

  collection: "banks",
  idField: "id",
  removeAction: "remove",

  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "short_name",
      type: "string",
      filterable: true,
      unique: true,
      editable: true,
      visible: true
    },
    {
      name: "name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "city",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "country",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "address",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "bank_type",
      type: "integer",
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  /* scope:client */
  checkAssociatedBankAccounts: function(data, cb) {
    this.runOnServer("checkAssociatedBankAccounts", data, cb);
  },

  /* scope:server */
  async $checkAssociatedBankAccounts(data, cb) {
    let associated_accounts_count = await this.src.db
      .collection("bank_accounts")
      .findAll({ bank_id: data.id, removed: 0 });

    cb(associated_accounts_count);
  },

  /* scope:server */
  beforeRemove: async function(ids, callback) {
    this.$checkAssociatedBankAccounts({ id: ids[0] }, (res) => {
      if (!!res && res.length) {
        callback(null);
      } else {
        callback(ids);
      }
    });
  },

  /* scope:server */
  async $uniqueBankEntries(data, cb) {
    let sql = `select * from banks where removed=0 and lower(short_name)=lower($1) and id!='${data.id}'`;
    sql = await this.buildRealmConditionQuery(sql);
    const res = await this.src.db.query(sql, [data.short_name]);
    if (res && res.length) {
      return cb({
        result: false,
        message: "Short name for banks must be unique"
      });
    } else {
      return cb({ result: true });
    }
  }
});
