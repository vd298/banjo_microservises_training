Ext.define("Crm.modules.banks.model.BanksBulkTemplatesModel", {
  extend: "Crm.classes.DataModel",

  collection: "banks_bulk_templates",
  idField: "id",
  removeAction: "remove",
  foreignKeyFilter: [
    {
      collection: "banks",
      alias: "b",
      on: " banks_bulk_templates.bank_id = banks.id ",
      type: "inner"
    }
  ],

  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "template_name",
      type: "string",
      filterable: true,
      unique: true,
      editable: true,
      visible: true
    },
    {
      name: "bank_id",
      type: "ObjectID",
      filterable: true,
      unique: true,
      editable: true,
      visible: true
    },
    {
      name: "protocol_id",
      type: "ObjectID",
      filterable: true,
      unique: true,
      editable: true,
      visible: true
    },
    {
      name: "description",
      type: "string",
      filterable: true,
      unique: true,
      editable: true,
      visible: true
    },
    {
      name: "template",
      type: "jsonb",
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  /* scope:server */
  async $saveTemplate(data, cb) {
    let templateData;
    const me = this;
    if (data.headers) {
      templateData = {
        name: data.template_name,
        headers: true,
        mapping: data.headers_mapping_data
      };
    } else {
      templateData = {
        name: data.template_name,
        headers: false,
        mapping: data.headers_data
      };
    }

    var res = await this.callApi({
      service: "account-service",
      method: "addBankTemplate",
      data: {
        id: data.id,
        description: data.description,
        protocol_id: data.protocol_id,
        bank_id: data.bank_id,
        template_name: data.template_name,
        template: templateData
      },
      options: {
        realmId: me.user.profile.realm_id
      }
    });

    if (res.result) {
      this.changeModelData(
        "Crm.modules.banks.model.BanksBulkTemplatesModel",
        "ins",
        {}
      );
    }
    return cb(res);
  },

  /* scope:server */
  async afterGetData(data, cb) {
    const me = this;
    data.forEach((el) => {
      if (el.template) {
        el.headers = el.template.headers;
        el.template_name = el.template.name;
        if (el.template.headers) {
          el.headers_mapping_data = el.template.mapping;
        } else {
          el.headers_data = el.template.mapping;
        }
      }
    });
    cb(data);
  },

  /* scope:server */
  async write(data, cb) {
    let res = this.$saveTemplate(data, cb);
    cb(res);
  }
});
