Ext.define("Crm.modules.CreationMerchantTariffPlans.view.CreationTariffSettingsPanel", {
  extend: "Crm.modules.tariffs.view.TariffSettingsPanel",
  title: D.t("Tariff"),
  filterable: false,
  filterbar: false,
});
