Ext.define("Crm.modules.accountCategories.model.accountCategoriesModel", {
  extend: "Core.data.DataModel",

  collection: "account_categories",
  idField: "id",
  strongRequest: false,
  foreignKeyFilter: [
    {
      collection: "accounts",
      alias: "a",
      on: " account_categories.account_id = accounts.id",
      type: "inner"
    }
  ],
  maxLimit: 1000,

  fields: [
    {
      name: "id",
      type: "ObjectID"
    },
    {
      name: "category_id",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "account_id",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    }
  ],
  async afterGetData(data, cb) {
    let d1 = null;
    let d2 = null;
    if (data.length) {
      let categories = new Set();
      let accounts = new Set();
      let i = 0;

      while (data.length > i) {
        categories.add(data[i].category_id);
        data[i].account_id && accounts.add(data[i].account_id);
        i++;
      }
      if (categories.size) {
        await this.getCategoryByIDUsingINClause(
          Array.from(categories),
          function(res) {
            if (res) {
              d1 = res;
            }
          }
        );
      }
      if (accounts.size) {
        await this.getAccountsByIDUsingINClause(Array.from(accounts), function(
          res
        ) {
          if (res) {
            d2 = res;
          }
        });
      }

      let j = 0;
      const tempdata = data.map((e) => {
        e.category_name =
          (d1 &&
            d1.length &&
            d1[d1.findIndex((o) => o.category_id === e.category_id)]
              .category_name) ||
          "";
        e.account_name =
          (d2 &&
            d2.length &&
            d2[d2.findIndex((o) => o.account_id === e.account_id)]
              .account_name) ||
          "";
        return e;
      });
      cb(tempdata);
    } else {
      cb(data);
    }
  },
  getCategoryByIDUsingINClause: async function(params, cb) {
    try {
      let sql = `SELECT id as category_id , name as category_name  FROM categories WHERE id IN(${params.map(
        (id) => `'${id}'`
      )})`;
      const resp = await this.src.db.query(sql);
      if (resp.length) {
        return cb(resp);
      }
      return cb();
    } catch (error) {
      console.error(
        "UserRiskTypesCombo.js. Func:getData. Database Error. Error",
        error
      );
      return cb({ message: error });
    }
  },
  getAccountsByIDUsingINClause: async function(params, cb) {
    try {
      let sql = `SELECT id as account_id , name as account_name  FROM accounts WHERE id IN(${params.map(
        (id) => `'${id}'`
      )})`;
      sql = await this.buildRealmConditionQuery(sql);
      const resp = await this.src.db.query(sql);

      if (resp.length) {
        return cb(resp);
      }
      return cb();
    } catch (error) {
      console.error(
        "UserRiskTypesCombo.js. Func:getData. Database Error. Error",
        error
      );
      return cb({ message: error });
    }
  },

  async beforeSave(data, cb) {
    const me = this;
    try {
      let sql = `select account_categories.id from account_categories`;
      sql = await this.buildJoinQuery(sql);
      sql += ` where account_categories.account_id=$1 and account_categories.category_id=$2 and account_categories.removed=0`;
      const category = await me.src.db.query(sql, [
        data.account_id,
        data.category_id
      ]);
      if (category && category.length) {
        cb({ success: false, message: "Category already added for account" });
      } else {
        cb(data);
      }
    } catch (error) {
      cb({ success: false });
    }
  },

  async afterSave(data, cb) {
    const me = this;
    me.changeModelData(
      "Crm.modules.accountCategories.model.accountCategoriesModel",
      "ins",
      data
    );
    cb(data);
  }
});
