Ext.define("Crm.modules.accountCategories.view.accountCategoriesGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Account Categories"),
  iconCls: "x-fa fa-users",
  filterbar: true,

  buildColumns: function() {
    return [
      {
        dataIndex: "id",
        hidden: true
      },
      {
        text: D.t("Categories"),
        flex: 1,
        sortable: true,
        dataIndex: "category_id",
        renderer: function(v, m, r) {
          if (v && r.data.category_name) {
            return (
              '<a href="#Crm.modules.categories.view.CategoryForm~' +
              v +
              '">' +
              r.data.category_name +
              "</a>"
            );
          }
          return v;
        },
        filter: {
          xtype: "combo",
          valueField: "id",
          displayField: "name",
          minChars: 1,
          store: Ext.create("Core.data.ComboStore", {
            storeId: "id",
            dataModel: Ext.create("Crm.modules.categories.model.CategoryModel"),
            fieldSet: ["name", "id"],
            scope: this
          })
        }
      },
      {
        text: D.t("Accounts"),
        flex: 1,
        sortable: true,
        dataIndex: "account_id",

        renderer: function(v, m, r) {
          if (v && r.data.account_name) {
            return (
              '<a href="#Crm.modules.accounts.view.AccountsForm~' +
              v +
              '">' +
              r.data.account_name +
              "</a>"
            );
          }
          return v;
        },
        filter: {
          xtype: "combo",
          valueField: "id",
          displayField: "name",
          queryMode: "all",
          minChars: 1,
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create("Crm.modules.accounts.model.AccountsModel"),
            fieldSet: ["name", "id"],
            scope: this
          })
        }
      }
    ];
  }
});
