Ext.define(
  "Crm.modules.accountCategories.view.accountCategoriesFormController",
  {
    extend: "Core.form.FormController",

    setControls() {
      var me = this;
      this.control({
        "[action=removeBtn]": {
          click: function(el) {
            me.deleteRecord_do(true);
          }
        }
      });
      this.callParent(arguments);
    },

    deleteRecord_do: function(store, rec) {
      var me = this;
      D.c("Removing", "Delete the record?", [], function() {
        var data = me.view
          .down("form")
          .getForm()
          .getValues();
        me.model.remove([data[me.model.idField]], function() {
          me.view.fireEvent("remove", me.view);
          window.history.go(-1);
        });
      });
    }
  }
);
