Ext.define("Crm.modules.accountCategories.view.accountCategoriesForm", {
  extend: "Core.form.FormWindow",

  titleTpl: D.t("Account Categories: {code}{[name? '/':'']}{name}"),
  width: 500,
  height: 200,
  controllerCls:
    "Crm.modules.accountCategories.view.accountCategoriesFormController",
  syncSize: function() {},

  buildItems() {
    return [
      {
        name: "id",
        hidden: true
      },
      this.buildCategoryCombo(),
      this.buildAccountCombo()
    ];
  },
  buildAccountCombo() {
    return {
      name: "account_id",
      xtype: "combo",
      editable: false,
      fieldLabel: D.t("Account"),
      queryMode: "all",
      displayField: "name",
      editable: true,
      valueField: "id",
      validator: true,
      allowBlank: false,
      minChars: 1,
      store: Ext.create("Core.data.ComboStore", {
        storeId: "account_id",
        dataModel: Ext.create("Crm.modules.accounts.model.AccountsModel"),
        fieldSet: ["name", "id"],
        scope: this
      })
    };
  },
  buildCategoryCombo() {
    return {
      name: "category_id",
      xtype: "combo",
      fieldLabel: D.t("Category"),
      queryMode: "all",
      displayField: "name",
      editable: true,
      valueField: "id",
      validator: true,
      allowBlank: false,
      minChars: 1,
      store: Ext.create("Core.data.ComboStore", {
        storeId: "category_id",
        dataModel: Ext.create("Crm.modules.categories.model.CategoryModel"),
        fieldSet: ["name", "id"],
        scope: this
      })
    };
  },

  buildButtons: function() {
    var btns = [
      {
        tooltip: D.t("Remove this record"),
        iconCls: "x-fa fa-trash",
        action: "removeBtn"
      },
      "->",
      { text: D.t("Save"), iconCls: "x-fa fa-check", action: "apply" },
      "-",
      { text: D.t("Close"), iconCls: "x-fa fa-ban", action: "formclose" }
    ];
    if (this.allowCopy)
      btns.splice(1, 0, {
        tooltip: D.t("Make a copy"),
        iconCls: "x-fa fa-copy",
        action: "copy"
      });

    if (this.allowImportExport) {
      btns.splice(1, 0, "-", {
        text: D.t("Export/Import"),
        menu: [
          {
            text: D.t("Export to file"),
            xtype: "button",
            margin: 5,
            action: "exportjson"
          },
          {
            xtype: "filefield",
            buttonOnly: true,
            margin: 5,
            action: "importjson",
            buttonConfig: {
              text: D.t("Import from file"),
              width: "100%"
            }
          }
        ]
      });
    }

    return btns;
  }
});
