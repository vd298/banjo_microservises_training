Ext.define("Crm.modules.Transfers.view.TransfersLiteGrid", {
  extend: "Crm.modules.Transfers.view.TransfersGrid",

  buildTbar: function() {
    var items = [
      {
        text: D.t("Add"),
        tooltip: D.t("Create Settlement Payout Request"),
        iconCls: "x-fa fa-plus",
        scale: "medium",
        action: "add"
      }
    ];

    items.push(
      "-",
      {
        text: "Reports",
        tooltip: "Reports",
        iconCls: "x-fa fa-cloud-download",
        scale: "medium",
        action: "downloadReport"
      },
      "-",
      {
        tooltip: this.buttonReloadText,
        iconCls: "x-fa fa-refresh",
        action: "refresh"
      }
    );

    if (this.filterable) items.push("->", this.buildSearchField());
    return items;
  }
});
