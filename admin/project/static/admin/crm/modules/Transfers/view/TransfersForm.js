Ext.define("Crm.modules.Transfers.view.TransfersForm", {
  extend: "Core.form.FormWindow",

  titleTpl: D.t("Transfer: {id}"),

  formMargin: 0,
  formLayout: "fit",

  controllerCls: "Crm.modules.Transfers.view.TransfersFormController",

  onClose: function(me) {
    window.history.back();
  },

  templates: {
    default: Ext.create("Crm.modules.Transfers.view.templates.default")
  },

  buildItems: function() {
    return {
      xtype: "tabpanel",
      layout: "fit",
      items: [this.buildGeneral()]
    };
  },

  buildGeneral() {
    var me = this;
    this.DataPanel = Ext.create("Ext.panel.Panel", {
      region: "center",
      scrollable: true,
      bodyCls: "x-selectable",
      items: [
        {
          xtype: "textfield",
          name: "id",
          hidden: true
        }
      ]
    });
    return {
      title: D.t("Transfer Data"),
      xtype: "panel",
      layout: "border",
      scrollable: true,
      items: [this.DataPanel, this.buildTransactions()]
    };
  },

  buildTransactions() {
    return {
      xtype: "panel",
      region: "east",
      cls: "grayTitlePanel",
      width: "50%",
      layout: "fit",
      split: true,
      title: D.t("Transactions"),
      items: Ext.create("Crm.modules.Transactions.view.TransactionsGrid", {
        filterbar: false,
        filterable: false,
        scope: this,
        observe: [{ property: "transfer_id", param: "id" }]
      })
    };
  },

  buildButtons() {
    var me = this;
    let currentData = me.data;
    return [
      "->",
      "->",
      "->",
      {
        text: D.t("Sync Transfer Status"),
        iconCls: "x-fa fa-refresh",
        action: "syncTransferStatus",
        name: "syncTransferStatus",
        disabled: true
      },
      "->",
      {
        text: D.t("Change source account"),
        name: "changeSourceAccount",
        action: "changeSourceAccount",
        hidden: true,
        isDisabled: function() {
          let permission = !this.permis.modify && !this.permis.read;
          let currStatus = currentData.activity;
          if (permission == true) {
            return permission;
          } else if (
            currStatus &&
            ["APPROVED", "REJECTED", "SETTLEMENT_APPROVED"].indexOf(
              currStatus
            ) == -1
          ) {
            return false;
          } else return permission;
        }
      },
      {
        text: D.t("Accept"),
        iconCls: "x-fa fa-check",
        name: "accept",
        action: "accept",
        isDisabled: function() {
          let permission = !this.permis.modify && !this.permis.read;
          let currStatus = currentData.activity;
          if (permission == true) {
            return permission;
          } else if (
            currStatus &&
            ["APPROVED", "REJECTED", "SETTLEMENT_APPROVED"].indexOf(
              currStatus
            ) == -1
          ) {
            return false;
          } else return permission;
        }
      },
      {
        text: D.t("Reject"),
        iconCls: "x-fa fa-check",
        action: "reject",
        isDisabled: function() {
          let permission = !this.permis.modify && !this.permis.read;
          let currStatus = currentData.activity;
          if (permission == true) {
            return permission;
          } else if (
            currStatus &&
            ["APPROVED", "REJECTED"].indexOf(currStatus) == -1
          ) {
            return false;
          } else return permission;
        }
      },
      "-",
      { text: D.t("Close"), iconCls: "x-fa fa-ban", action: "formclose" }
    ];
  },

  applyEventTpl(event) {
    const tpl = this.templates.default;
    this.DataPanel.add({
      action: "datapanel",
      cls: "details-panel",
      tpl: tpl.build()
    });
  }
});
