Ext.define("Crm.modules.Transfers.view.TransfersGridController", {
  extend: "Core.grid.GridController",

  setControls: function() {
    var me = this;
    this.control({
      "[action=executePayout]": {
        click: function(el) {
          me.addUpdatePayout();
        }
      },
      "[action=addDepositDirect]": {
        click: function(el) {
          me.addDepositDirect();
        }
      },
      "[action=downloadReport]": {
        click: function(el) {
          let filter = {};
          let isDate = false;
          let filterArray = me.view.down("grid").filterBar.filterArray || [];
          if (filterArray.length) {
            for (let i = 0; i < filterArray.length; i++) {
              filter[filterArray[i].config.property] =
                filterArray[i].config.value;
              if (filterArray[i].config.property === "ctime") {
                isDate = true;
              }
            }
          }

          me.downloadReport(filter, isDate);
        }
      },
      "[action=add]": {
        click: function(el) {
          me.addRecord();
        }
      },
      "[action=refresh]": {
        click: function(el) {
          me.reloadData();
        }
      },
      "[action=import]": {
        click: function(el) {
          me.importData();
        }
      },
      "[action=export]": {
        click: (el, v) => {
          this.export({
            format: "xlsx",
            report_name: "account_report",
            fileName: "account_report"
          });
        }
      },
      grid: {
        cellkeydown: function(cell, td, i, rec, tr, rowIndex, e, eOpts) {
          if (e.keyCode == 13) {
            me.gotoRecordHash(rec.data);
          }
        },
        celldblclick: function(cell, td, i, rec) {
          me.gotoRecordHash(rec.data);
        },
        itemcontextmenu: function(vw, record, item, index, e, options) {
          e.stopEvent();
          //if(win.menuContext) {
          //    win.menuContext.record = record;
          //    win.menuContext.showAt(e.pageX, e.pageY);
          //}
        }
      }
    });

    this.view.on("gridTxReject", function(grid, indx) {
      me.txReject(grid.getStore().getAt(indx).data);
    });
    this.view.on("gridTxAccept", async function(grid, indx) {
      let data = grid.getStore().getAt(indx).data;
      if (data.transfer_type === "SETTLEMENT") {
        await me.model.runOnServer(
          "fetchSettlementDetails",
          {
            id: data.settlement_id,
            wallet_id: data.src_wallet_id
          },
          function(res) {
            if (res && res.id) {
              data.markup_rate = res.markup_rate;
              data.conversion_rate = res.conversion_rate;
              data.max_src_amount = res.max_src_amount;
              data.src_amount_settlement = res.src_amount;
              data.dst_amount_settlement = res.dst_amount;
              me.txAccept(data);
            }
          }
        );
      } else me.txAccept(grid.getStore().getAt(indx).data);
    });
    this.view.on("activate", function(grid, indx) {
      if (!me.view.observeObject)
        document.title = me.view.title + " " + D.t("ConsoleTitle");
    });
    this.view.on("edit", function(grid, indx) {
      me.gotoRecordHash(grid.getStore().getAt(indx).data);
    });
    this.view.on("delete", function(grid, indx) {
      me.deleteRecord(grid.getStore(), indx);
    });
    this.initButtonsByPermissions();
    //this.view.on('modify', function(id) {alert();me.modify(id)})
  },
  addRecord: function() {
    var me = this;
    me.view.model.getNewObjectId(function(_id) {
      var oo = {};
      oo[me.view.model.idField] = _id;
      return me.createNewPayout(oo);
    });
  },
  isShowing(param) {
    return param !== "SETTLEMENT";
  },
  async txAccept(store) {
    var me = this;
    var acceptWindow = Ext.create("Ext.window.Window", {
      title: "Transfer Approval:",
      layout: "fit",
      width: "80vh",
      //margin: 30,
      modal: true,
      items: [
        {
          xtype: "form",
          layout: "anchor",
          //margin: 10,
          defaults: {
            xtype: "textfield",
            anchor: "100%",
            labelWidth: 150,
            padding: 10
          },
          items: [
            {
              name: "admin_id",
              value: localStorage && localStorage.uid ? localStorage.uid : null,
              hidden: true,
              allowBlank: false
            },
            {
              name: "transfer_type",
              value: store.transfer_type,
              hidden: true,
              allowBlank: false
            },
            {
              readOnly: true,
              name: "transfer_id",
              fieldLabel: D.t("Approving Transfer Id: *"),
              emptyText: "Transfer Id cannot be blank",
              allowBlank: false,
              value: store.id
            },
            {
              name: "src_amount",
              xtype: "numberfield",
              minValue: 0,
              maxValue: store.max_src_amount,
              allowBlank: me.isShowing(store.transfer_type),
              decimalPrecision: 6,
              fieldLabel: D.t("Source Amount"),
              value: store.src_amount,
              hidden: me.isShowing(store.transfer_type),
              listeners: {
                afterrender(e, eOpts) {
                  __CONFIG__.setComponentEmptyText(e, eOpts);
                },
                change: (el, v) => {
                  __CONFIG__.calculateDstAmount(acceptWindow, store);
                }
              }
            },
            {
              name: "dst_amount",
              xtype: "numberfield",
              minValue: 0,
              decimalPrecision: 6,
              fieldLabel: D.t("Destination Amount"),
              value: store.dst_amount,
              allowBlank: me.isShowing(store.transfer_type),
              hidden: me.isShowing(store.transfer_type),
              listeners: {
                afterrender(e, eOpts) {
                  __CONFIG__.setComponentEmptyText(e, eOpts);
                },
                change: (el, v) => {
                  __CONFIG__.calculateSrcAmount(acceptWindow, store);
                }
              }
            },
            {
              name: "conversion_rate",
              fieldLabel: D.t("Conversion Rate"),
              value: store.conversion_rate,
              allowBlank: me.isShowing(store.transfer_type),
              hidden: me.isShowing(store.transfer_type) || !store.is_exchange,
              xtype: "numberfield",
              minValue: 0,
              decimalPrecision: 6,
              submitEmptyText: false,
              listeners: {
                afterrender(e, eOpts) {
                  __CONFIG__.setComponentEmptyText(e, eOpts);
                },
                change: (el, v) => {
                  __CONFIG__.calculateDstAmount(acceptWindow, store);
                }
              }
            },
            {
              name: "markup_rate",

              fieldLabel: D.t("Markup Rate(0-100)"),
              value: store.markup_rate,
              hidden: me.isShowing(store.transfer_type) || !store.is_exchange,
              allowBlank: me.isShowing(store.transfer_type),
              xtype: "numberfield",
              minValue: 0,
              maxValue: 100,
              submitEmptyText: false,
              listeners: {
                afterrender(e, eOpts) {
                  __CONFIG__.setComponentEmptyText(e, eOpts);
                },
                change: (el, v) => {
                  __CONFIG__.calculateDstAmount(acceptWindow, store);
                }
              }
            },
            {
              xtype: "textarea",
              name: "note",
              maxLength: 150,
              fieldLabel: D.t("Approve Note")
            },
            {
              name: "brn",
              maxLength: 200,
              fieldLabel: __CONFIG__.isCryptoCurrency(store.dst_currency)
                ? D.t("Hash Id")
                : D.t("Bank Reference Number"),
              value: store.last_brn,
              disabled: store.last_brn && store.last_brn.length ? true : false,
              readOnly: store.last_brn && store.last_brn.length ? true : false
            },
            {
              xtype: "fieldcontainer",
              anchor: "100%",
              layout: "hbox",
              align: "center",
              margin: { top: 15, bottom: 5 },
              items: [
                {
                  xtype: "button",
                  text: D.t("OK"),
                  formBind: true,
                  listeners: {
                    click: async function(e, v) {
                      var actionBtn = e;
                      if (actionBtn) {
                        actionBtn.disable();
                      }
                      var reqData = this.up("form").getValues() || {};
                      reqData.merchantId = localStorage.uid;

                      me.callActivityApprove(reqData, store, acceptWindow);
                    }
                  }
                },
                {
                  xtype: "button",
                  margin: { left: 5 },
                  //padding: 10,
                  text: D.t("Cancel"),
                  listeners: {
                    click: function() {
                      acceptWindow.close();
                    }
                  }
                }
              ]
            }
          ]
        }
      ]
    }).show();
  },
  async callActivityApprove(reqData, store, acceptWindow) {
    var me = this;
    me.model.runOnServer(
      "callActivityApprove",
      { reqData, currentData: store },
      async function(acceptResp) {
        let defaultMsg = `Selected transfer`;
        if (store.ref_num != undefined)
          defaultMsg += ` with Reference number:${store.ref_num}`;
        if (
          acceptResp &&
          acceptResp.result &&
          acceptResp.result.status == "APPROVED"
        ) {
          defaultMsg += ` has been successfully marked as accepted in the system`;
          D.a("Transfer Accept Successful", acceptResp.message || defaultMsg);
        } else {
          if (
            acceptResp &&
            acceptResp.result.error &&
            acceptResp.result.error.message
          ) {
            acceptResp.message = acceptResp.result.error.message;
          }
          defaultMsg += ` has failed to be marked. Please try again or Contact Support`;
          D.a("Transfer Accept Failed", acceptResp.message || defaultMsg);
        }
        acceptWindow.close();
      }
    );
  },
  async txReject(store) {
    var me = this;
    var rejectWindow = Ext.create("Ext.window.Window", {
      title: "Transfer Rejection:",
      layout: "fit",
      width: "80vh",
      //margin: 30,
      modal: true,
      items: [
        {
          xtype: "form",
          layout: "anchor",
          //margin: 10,
          defaults: {
            xtype: "textfield",
            anchor: "100%",
            labelWidth: 150,
            padding: 10
          },
          items: [
            {
              name: "admin_id",
              value: localStorage && localStorage.uid ? localStorage.uid : null,
              hidden: true,
              allowBlank: false
            },
            {
              readOnly: true,
              name: "transfer_id",
              fieldLabel: D.t("Rejecting Transfer Id: *"),
              emptyText: "Transfer Id cannot be blank",
              allowBlank: false,
              value: store.id
            },
            {
              xtype: "textarea",
              name: "note",
              maxLength: 150,
              fieldLabel: D.t("Reject Note")
            },
            {
              xtype: "fieldcontainer",
              anchor: "100%",
              layout: "hbox",
              align: "center",
              margin: { top: 15, bottom: 5 },
              items: [
                {
                  xtype: "button",
                  text: D.t("OK"),
                  formBind: true,
                  listeners: {
                    click: async function(e, v) {
                      var actionBtn = e;
                      if (actionBtn) {
                        actionBtn.disable();
                      }
                      var reqData = this.up("form").getValues() || {};
                      reqData.merchantId = localStorage.uid;
                      me.model.runOnServer(
                        "callActivityReject",
                        reqData,
                        function(rejectResp) {
                          let defaultMsg = `Selected transfer`;
                          if (store.ref_num != undefined)
                            defaultMsg += ` with Reference number:${store.ref_num}`;
                          if (
                            rejectResp &&
                            rejectResp.result &&
                            rejectResp.result.status == "REJECTED"
                          ) {
                            defaultMsg += ` has been successfully marked as rejected in the system`;
                            D.a(
                              "Transfer Reject Successful",
                              rejectResp.message || defaultMsg
                            );
                          } else {
                            if (
                              rejectResp &&
                              rejectResp.error &&
                              rejectResp.error.message
                            ) {
                              rejectResp.message = rejectResp.error.message;
                            }
                            defaultMsg += ` has failed to be marked. Please try again or Contact Support`;
                            D.a(
                              "Transfer Reject Failed",
                              rejectResp.message || defaultMsg
                            );
                          }
                          rejectWindow.close();
                        }
                      );
                    }
                  }
                },
                {
                  xtype: "button",
                  margin: { left: 5 },
                  //padding: 10,
                  text: D.t("Cancel"),
                  listeners: {
                    click: function() {
                      rejectWindow.close();
                    }
                  }
                }
              ]
            }
          ]
        }
      ]
    }).show();
  },

  afterDataLoad: function(view, cb) {
    var me = this;
    return cb();
  },

  createNewPayout: function(formData) {
    var me = this;
    var adjustWindow = Ext.create("Ext.window.Window", {
      scope: me,
      title: "Create New Payout",
      name: "createPayoutWindow",
      height: "80vh",
      maxHeight: 550,
      width: "70vw",
      layout: "fit",
      margin: 20,
      modal: true,
      items: [
        {
          xtype: "panel",
          width: "60vw",
          height: "80vh",
          layout: "card",
          bodyStyle: "padding:10px",
          name: "mainCreatePanel",
          margin: 20,
          modal: true,
          deferredRender: true,
          defaults: {
            border: false
          },
          bbar: [
            {
              id: "move-prev",
              text: "Back",
              xtype: "button",
              cls: "account-wizard-button",
              handler: function(btn) {
                me.navigate(adjustWindow, btn.up("panel"), "prev");
              },
              border: 5,
              style: {
                backgroundColor: "#35baf6",
                borderColor: "#35baf6",
                borderStyle: "solid",
                color: "#fff"
              },
              disabled: true,
              hidden: true
            },
            "->",
            {
              id: "move-next",
              text: "Step 2: Set Rate",
              xtype: "button",
              cls: "account-wizard-button",
              handler: function(btn) {
                me.navigate(adjustWindow, btn.up("panel"), "next");
              },
              border: 5,
              style: {
                backgroundColor: "#35baf6",
                borderColor: "#35baf6",
                borderStyle: "solid",
                color: "#fff"
              },
              disabled: true
            }
          ],
          items: [
            {
              id: "account_1",
              name: "account_1",
              xtype: "form",
              layout: "anchor",
              scrollable: true,
              submitEmptyText: false,
              defaults: {
                labelWidth: "30",
                width: "70%",
                labelStyle: "white-space: nowrap;"
              },
              defaultType: "textfield",
              items: [
                {
                  flex: 1,
                  xtype: "displayfield",
                  name: "Payout",
                  readOnly: true,
                  editable: false,
                  shrinkwrap: 3,
                  value: `Payout basic details`
                },
                {
                  name: "id",
                  hidden: true,
                  value: me.uuidv4()
                },
                {
                  flex: 1,
                  xtype: "combo",
                  name: "account",
                  allowBlank: false,
                  fieldLabel: D.t("Select Merchant"),
                  store: Ext.create("Core.data.ComboStore", {
                    dataModel: Ext.create(
                      "Crm.modules.accounts.model.AccountsModel"
                    ),
                    fieldSet: ["id", "name"],
                    scope: me
                  }),
                  valueField: "id",
                  displayField: "name",
                  listeners: {
                    change: (el, v) => {
                      adjustWindow
                        .down("[name=merchant_account_id]")
                        .clearValue();
                      adjustWindow.down("[name=src_currency]").setValue("");

                      adjustWindow
                        .down("[name=all_amount]")
                        .setValue(0)
                        .setHidden(true);

                      adjustWindow.down("[name=dst_currency]").setValue("");
                      adjustWindow
                        .down("[name=dst_currency_name]")
                        .setValue("");
                      const proxyFilters = {
                        filters: [
                          {
                            _property: "account_id",
                            _value: v,
                            _operator: "eq"
                          }
                        ]
                      };
                      const store = Ext.create("Core.data.ComboStore", {
                        dataModel: Ext.create(
                          "Crm.modules.accounts.model.MerchantAccountsModel"
                        ),
                        fieldSet: ["id", "name"],
                        exProxyParams: proxyFilters,
                        scope: this
                      });
                      adjustWindow
                        .down("[name=merchant_account_id]")
                        .setStore(store);
                    }
                  }
                },
                {
                  flex: 1,
                  xtype: "combo",
                  name: "merchant_account_id",
                  fieldLabel: D.t("Select Merchant Account"),
                  valueField: "id",
                  allowBlank: false,
                  displayField: "name",
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    },
                    change: (el, v) => {
                      if (!!v) {
                        this.model.runOnServer(
                          "fetchPayoutDetailId",
                          { merchant_account_id: v },
                          function(res) {
                            if (res && res.length) {
                              adjustWindow
                                .down("[name=src_currency]")
                                .setValue(res[0].currency);

                              const balance = __CONFIG__.formatCurrency(
                                res[0].total_available_balance,
                                0,
                                res[0].settlement_currency
                              );
                              adjustWindow
                                .down("[name=all_amount]")
                                .setValue(
                                  `${balance}(${res[0].settlement_currency})`
                                )
                                .setHidden(false);

                              let merchAccountId = adjustWindow.down(
                                "[name=merchant_account_id]"
                              );
                              let errMsg = null;
                              if (merchAccountId) {
                                if (!isNaN(balance) && Number(balance) <= 0) {
                                  errMsg = `Pick a Merchant account with greater than zero balance available for settlement payout.`;
                                  merchAccountId.markInvalid(errMsg);
                                  merchAccountId.validator = function() {
                                    return errMsg;
                                  };
                                } else {
                                  merchAccountId.clearInvalid();
                                  merchAccountId.validator = function() {
                                    return true;
                                  };
                                }
                              }
                              el.isValid();
                              adjustWindow
                                .down("[name=dst_currency]")
                                .setValue(res[0].settlement_currency);
                              adjustWindow
                                .down("[name=dst_currency_name]")
                                .setValue(res[0].settlement_currency_name);
                              var nBtn = adjustWindow.down("[id=move-next]");
                              if (errMsg) {
                                nBtn.setDisabled(true);
                              } else {
                                nBtn.setDisabled(false);
                              }
                            }
                          }
                        );
                      }
                    }
                  }
                },

                {
                  name: "dst_currency_name",
                  xtype: "displayfield",
                  fieldLabel: D.t("Merchant Account settlement Currency"),
                  allowBlank: true,
                  readOnly: true,
                  hidden: false,
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    }
                  }
                },
                {
                  name: "src_currency",
                  fieldLabel: D.t("Merchant Account Currency"),
                  allowBlank: true,
                  readOnly: true,
                  hidden: true,
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    }
                  }
                },
                {
                  name: "dst_currency",
                  allowBlank: true,
                  readOnly: true,
                  hidden: true,
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    }
                  }
                },
                {
                  name: "all_amount",
                  flex: 1,
                  xtype: "displayfield",
                  fieldLabel: D.t("Total Available For Payout"),
                  readOnly: true,
                  hidden: true,
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    }
                  }
                }
              ],
              listeners: {
                activate: function(context, eOpts) {
                  if (adjustWindow) me.checkSetButtons(adjustWindow);
                },
                validitychange: function(context, valid, eOpts) {
                  if (adjustWindow) me.checkSetButtons(adjustWindow, valid);
                },
                resize: function(c, oW, oH, nW, nH) {
                  me.syncSize(c);
                },
                afterRender: function(e, eOpts) {
                  e.isValid();
                  Ext.on("resize", function() {
                    me.syncSize(e);
                  });
                }
              }
            },
            {
              id: "account_2",
              xtype: "form",
              layout: "anchor",
              defaultType: "textfield",
              reserveScrollbar: true,
              submitEmptyText: false,
              autoScroll: true,
              fieldDefaults: {
                labelWidth: "20",
                width: "70%",
                labelStyle: "white-space: nowrap;"
              },
              items: [
                {
                  flex: 1,
                  xtype: "displayfield",
                  name: "payout",
                  readOnly: true,
                  editable: false,
                  shrinkwrap: 3,
                  value: `Select bank account and payout amount`
                },
                {
                  name: "src_amount_display",
                  xtype: "displayfield",
                  readOnly: true,
                  editable: false,

                  fieldLabel: D.t("Total Payout Amount")
                },
                {
                  name: "src_amount",
                  fieldLabel: D.t("Total Payout Amount"),
                  allowBlank: true,
                  readOnly: true,
                  value: 0,
                  hidden: true,
                  inputWrapCls: "",
                  triggerWrapCls: "",
                  fieldStyle: "background:none",
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    },
                    change: (el, v) => {
                      me.calculateDestination(v, adjustWindow);
                    }
                  }
                },
                {
                  name: "dst_amount",
                  fieldLabel: D.t("Total Payout Amount"),
                  allowBlank: true,
                  readOnly: true,
                  value: 0,
                  hidden: true,
                  inputWrapCls: "",
                  triggerWrapCls: "",
                  fieldStyle: "background:none",
                  listeners: {
                    afterrender: function(context, eOpts) {
                      context.isValid();
                    }
                  }
                },
                {
                  flex: 1,
                  name: "src_currency",
                  allowBlank: true,
                  readOnly: true,
                  hidden: true
                },
                {
                  flex: 1,
                  name: "dst_currency",
                  allowBlank: true,
                  readOnly: true,
                  hidden: true
                },
                {
                  flex: 1,
                  name: "merchant_account_id",
                  allowBlank: true,
                  readOnly: true,
                  hidden: true
                },
                this.payout_data(formData)
              ],
              listeners: {
                activate: function(context, eOpts) {
                  if (adjustWindow) {
                    let FormResp = me.checkSetButtons(adjustWindow, {
                      gridName: "payout_data"
                    });
                    if (FormResp == false) {
                      me.checkGridValidity(adjustWindow, {
                        name: "payout_data"
                      });
                    }
                  }
                },
                validitychange: function(context, valid, eOpts) {
                  if (adjustWindow) {
                    let FormResp = me.checkSetButtons(adjustWindow);
                    if (FormResp == false) {
                      me.checkGridValidity(adjustWindow, {
                        name: "payout_data"
                      });
                    }
                  }
                }
              }
            }
          ]
        }
      ],
      listeners: {
        resize: function(context, width, height, eOpts) {}
      }
    });
    adjustWindow.show();
  },

  syncSize: function(c) {
    var width = Ext.Element.getViewportWidth(),
      height = Ext.Element.getViewportHeight();
    c.setHeight(Math.floor(height * 0.9));
  },

  gridCheck: function(adjustWindow, eOpts) {
    Ext.GlobalEvents.fireEvent(`${eOpts.name}ValidCheck`);
  },

  checkGridValidity: function(adjustWindow, eOpts) {
    var me = this;
    var nBtn = adjustWindow.down("[id=move-next]"),
      currentGrid = adjustWindow.down(`[name=${eOpts.name}]`);
    if (nBtn) {
      if (
        currentGrid &&
        currentGrid.store &&
        currentGrid.store.getRange() &&
        currentGrid.store.getRange().length
      ) {
        nBtn.setDisabled(false);
        return false;
      } else if (nBtn) {
        nBtn.setDisabled(true);
        return true;
      }
    }
    return false;
  },
  checkSetButtons: function(adjustWindow, valid, name = "form") {
    var nBtn = adjustWindow.down("[id=move-next]"),
      currentForm = adjustWindow.down(name);
    if (nBtn) {
      if (valid == undefined && currentForm && currentForm.isValid) {
        valid = currentForm.isValid();
      }
      if (valid == false) {
        nBtn.setDisabled(true);
        return true;
      } else {
        nBtn.setDisabled(false);
        return false;
      }
    }
    return false;
  },
  calculateDestination: function(v, adjustWindow) {
    let val = adjustWindow.down("[name=src_amount]").getValue();
    adjustWindow.down("[name=dst_amount]").setValue(val);
  },

  navigate: async function(adjustWindow, panel, direction) {
    var me = this;
    var layout = panel.getLayout(),
      currentForm,
      currentFormContext,
      currentFormVals,
      createResp;
    //Vaibhav Vaidya, Note: Based on Panel inputs, check their input first then proceed
    if (layout && layout.activeItem && layout.activeItem.id) {
      currentForm = layout.activeItem.id;
      currentFormContext = layout.activeItem.form;
      currentFormVals = layout.activeItem.form.getValues();
    }
    let panel1Str = `Step 1: Payout Basic Info`;
    let panel2Str = `Step 2: Select Bank Account`;
    let submitStr = `Submit Payout Request`;
    switch (currentForm) {
      //Basic info
      case "account_1":
        {
          if (direction == "next") {
            let formCheckResp,
              basicInfo = panel.down("[id=account_1]"),
              formVals = { basic_info: {} };
            let account_2 = panel.down("[id=account_2]");
            if (basicInfo && basicInfo.form) {
              formVals.basic_info = basicInfo.form.getValues();
              account_2.form.setValues(formVals.basic_info);
              let label =
                "Payout Amount (" + formVals.basic_info.dst_currency + "):";
              let payoutGrid = account_2.down("[name=payout_data]");
              let payoutGridItems = payoutGrid.store.getData().items;
              if (
                (payoutGridItems && !payoutGridItems.length) ||
                (payoutGridItems &&
                  payoutGridItems.length &&
                  payoutGridItems[0].data.merchant_account_id !==
                    formVals.basic_info.merchant_account_id)
              ) {
                await this.model.runOnServer(
                  "fetchPayoutBankAmountByMerchantId",
                  {
                    merchant_account_id:
                      formVals.basic_info.merchant_account_id,
                    currency: formVals.basic_info.dst_currency
                  },
                  function(res) {
                    account_2.down("[name=payout_data]").store.loadData([]);
                    account_2.down("[name=src_amount]").setValue(0);
                    account_2
                      .down("[name=src_amount_display]")
                      .setValue(`0 (${formVals.basic_info.dst_currency})`);

                    payoutGridItems = [];
                    let src_amount = 0;

                    if (res && res.length) {
                      for (let currentBA of res) {
                        currentBA.amount = currentBA.max_amount_for_payout;
                        currentBA.payment_protocol = currentBA.payment_protocols
                          .length
                          ? currentBA.payment_protocols[0].protocol_type
                          : null;
                        src_amount += currentBA.max_amount_for_payout;

                        payoutGridItems.push(currentBA);
                      }
                      src_amount = __CONFIG__.formatCurrency(
                        src_amount,
                        0,
                        formVals.basic_info.src_currency
                      );
                      account_2
                        .down("[name=payout_data]")
                        .store.loadData(payoutGridItems, true);
                      account_2.down("[name=src_amount]").setValue(src_amount);
                      account_2
                        .down("[name=src_amount_display]")
                        .setValue(
                          `${src_amount} (${formVals.basic_info.dst_currency})`
                        );
                      account_2.down("[name=dst_amount]").setValue(src_amount);

                      if (parseFloat(src_amount) <= 0)
                        Ext.getCmp("move-next").setDisabled(true);
                      else Ext.getCmp("move-next").setDisabled(false);
                    }
                  }
                );
              }
              account_2
                .down("[name=src_amount]")
                .setConfig("fieldLabel", label);
            }
            layout[direction]();
            let bank_amount = account_2.down("[name=src_amount]").getValue();
            Ext.getCmp("move-next")
              .setHidden(false)
              .setDisabled(false)
              .setText(submitStr);

            Ext.getCmp("move-prev")
              .setDisabled(false)
              .setHidden(false)
              .setText(panel1Str);
          }
          return;
        }
        break;

      //Merchant Accounts
      case "account_2":
        {
          if (direction == "next") {
            let basicInfo = panel.down("[id=account_1]"),
              bank_amount = panel.down("[id=account_2]"),
              formVals = { basic_info: {} };

            if (basicInfo && basicInfo.form) {
              formVals.basic_info = basicInfo.form.getValues();
            }

            if (bank_amount && bank_amount.form) {
              Object.assign(formVals.basic_info, bank_amount.form.getValues());
            }
            Ext.getCmp("move-next").setDisabled(true);
            createResp = await me
              .createNewPayouts(formVals)
              .then(function(val) {
                adjustWindow.close();
                if (!!val.result) {
                  msg = "Payout request created successfully.";
                  if (val.result.status != undefined)
                    msg += `<br> Merchant Status: Inprogress <br>`;
                  D.a(
                    "Payout request created successfully.",
                    msg,
                    [],
                    () => {}
                  );
                }
                me.view.store.reload();
              })
              .catch(function(val) {
                Ext.getCmp("move-next").setDisabled(true);
                msg =
                  "Problem during payout creation, Contact support team if error persists";
                if (val && val.message) msg = val.message;
                else if (val && val.error && val.error.message)
                  msg = val.error.message;
                D.a("Payout creation failure", msg);
              });
          } else {
            layout[direction]();

            Ext.getCmp("move-next").setText(panel2Str);
            Ext.getCmp("move-prev").setHidden(true);
          }
        }
        break;
      default:
        {
          D.a(
            "Invalid Form Selected",
            "Please close form and restart the process. Contact Support Team incase problem persists."
          );
        }
        break;
    }
  },

  uuidv4: function() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
      (
        c ^
        (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
      ).toString(16)
    );
  },
  startLoadingAnim: function() {
    Ext.MessageBox.wait("Processing...");
  },
  stopLoadingAnim: function() {
    Ext.MessageBox.hide();
  },
  createNewPayouts: function(formVals) {
    var me = this,
      funcName = "createNewPayout";
    var createPromise = new Promise(function(resolve, reject) {
      // me.startLoadingAnim();
      me.model.runOnServer(funcName, formVals, function(respData) {
        me.stopLoadingAnim();
        if (respData && respData.actionStatus == true) {
          resolve(respData);
        } else {
          reject(respData);
        }
      });
    });
    return createPromise;
  },

  payout_data: function(formContext) {
    const me = this;
    let payoutDataGrid = {
      xtype: "gridfield",
      hideLabel: true,
      region: "center",
      autoScroll: true,
      scrollable: true,
      name: "payout_data",
      layout: "fit",
      minHeight: 500,
      maxHeight: 501,
      fields: [
        "bank_name",
        "bank_account_id",
        "acc_no",
        "max_amount_for_payout",
        "amount",
        "payment_protocol"
      ],
      buildTbar() {
        return [
          {
            tooltip: "Reset list",
            iconCls: "x-fa fa-refresh",
            activity: "refresh",
            handler: async function() {
              let obj = Ext.getCmp("move-next")
                .up("panel")
                .down("[id=account_2]");
              let currency = obj.down("[name=dst_currency]").getValue();
              let merchant_account_id = obj
                .down("[name=merchant_account_id]")
                .getValue();
              await me.model.runOnServer(
                "fetchPayoutBankAmountByMerchantId",
                {
                  merchant_account_id,
                  currency
                },
                function(res) {
                  obj.down("[name=payout_data]").store.loadData([]);
                  obj.down("[name=src_amount]").setValue(0);
                  obj
                    .down("[name=src_amount_display]")
                    .setValue(`0 (${currency})`);

                  payoutGridItems = [];
                  let src_amount = 0;

                  if (res && res.length) {
                    for (let currentBA of res) {
                      currentBA.amount = currentBA.max_amount_for_payout;
                      currentBA.payment_protocol = currentBA.payment_protocols
                        .length
                        ? currentBA.payment_protocols[0].protocol_type
                        : null;
                      src_amount += currentBA.max_amount_for_payout;

                      payoutGridItems.push(currentBA);
                    }
                    src_amount = __CONFIG__.formatCurrency(
                      src_amount,
                      0,
                      currency
                    );
                    obj
                      .down("[name=payout_data]")
                      .store.loadData(payoutGridItems, true);
                    obj.down("[name=src_amount]").setValue(src_amount);
                    obj
                      .down("[name=src_amount_display]")
                      .setValue(`${src_amount} (${currency})`);
                    obj.down("[name=dst_amount]").setValue(src_amount);

                    if (parseFloat(src_amount) <= 0)
                      Ext.getCmp("move-next").setDisabled(true);
                    else Ext.getCmp("move-next").setDisabled(false);
                  }
                }
              );
            }
          }
        ];
      },
      listeners: {
        beforeselect: function(context, record, index, eOpts) {
          if (record) {
            console.log(record);
          }
        },
        afterrender: function(context, eOpts) {
          context.isValid();
        },
        change: function(e, newVal, oldVal, eOpts) {
          let data = e.store.getRange().map((e) => e.data);
          let amount = 0;
          for (let i = 0; i < data.length; i++) {
            const el = data[i];

            if (
              el.bank_account_id === newVal[i].bank_account_id &&
              el.max_amount_for_payout < newVal[i].amount
            ) {
              Ext.Msg.alert(
                "Error",
                "Amount for payout must be less than or equal to max amount for payout"
              );
              newVal[i].amount = el.max_amount_for_payout;
              amount = parseFloat(amount);
              amount += parseFloat(el.max_amount_for_payout);
              amount = __CONFIG__.formatCurrency(amount, 0, newVal[i].currency);
              e.store.loadData(newVal, true);
            } else {
              amount = parseFloat(amount);
              amount += parseFloat(newVal[i].amount);
              amount = __CONFIG__.formatCurrency(amount, 0, newVal[i].currency);
            }
          }
          let obj = Ext.getCmp("move-next")
            .up("panel")
            .down("[id=account_2]");

          obj.down("[name=src_amount]").setValue(amount);
          let currency = obj.down("[name=src_currency]").getValue();
          obj
            .down("[name=src_amount_display]")
            .setValue(`${amount} (${currency})`);
          // obj
          //   .down("[name=src_amount_display]")
          //   .setValue(`${src_amount} (${newVal[0].currency})`);
          //obj.down("[name=src_amount_display]").setValue(amount);

          if (amount > 0) Ext.getCmp("move-next").setDisabled(false);
          else Ext.getCmp("move-next").setDisabled(true);
        }
      },
      columns: [
        {
          text: D.t("Bank"),
          flex: 1,
          sortable: true,
          dataIndex: "bank_name",
          menuDisabled: false,
          editor: false
        },
        {
          text: D.t("Bank Account"),
          flex: 1,
          sortable: true,
          name: "bank_account_id",
          dataIndex: "bank_account_id",
          menuDisabled: false,
          forceSelection: true,
          allowNull: false,
          guideKeyField: "id",
          guideValueField: "account_name",
          xtype: "combocolumn",
          model: "Crm.modules.accountsPool.model.AllAccountsModel",
          editor: false,
          renderer: function(v, m, r) {
            if (r && r.data && r.data.bank_account_name != undefined) {
              return r.data.bank_account_name;
            }
            return v;
          }
        },
        {
          text: D.t("Bank Account Name"),
          hidden: true,
          sortable: true,
          name: "bank_account_name",
          dataIndex: "bank_account_name",
          editor: false,
          menuDisabled: false
        },
        {
          text: D.t("Account Number"),
          flex: 1,
          sortable: true,
          dataIndex: "acc_no",
          menuDisabled: false,
          editor: false
        },
        {
          text: D.t("Max Amount For Payout"),
          flex: 1,
          sortable: true,
          dataIndex: "max_amount_for_payout",
          menuDisabled: false,
          editor: false,
          renderer: function(v, m, r) {
            if (v) {
              return __CONFIG__.formatCurrency(v, 0, r.data.currency);
            }
            return v;
          }
        },
        {
          text: D.t("Payout Amount"),
          flex: 1,
          sortable: true,
          dataIndex: "amount",
          menuDisabled: false,
          editor: {
            xtype: "numberfield",
            allowBlank: false,
            minValue: 0
          },
          renderer: function(v, m, r) {
            if (v) {
              return __CONFIG__.formatCurrency(v, 0, r.data.currency);
            }
            return v;
          }
        },
        {
          text: D.t("Bank Account Id"),
          flex: 1,
          hidden: true,
          dataIndex: "bank_account_id"
        },
        me.buildPayoutProtocol(formContext)
      ]
    };
    return payoutDataGrid;
  },

  buildDefaultProtocolStore: function() {
    var me = this;
    return Ext.create("Core.data.ComboStore", {
      dataModel: Ext.create(
        "Crm.modules.paymentProtocols.model.PaymentProtocolsModel"
      ),
      fieldSet: ["id", "protocol_type"],
      scope: this
    });
  },

  buildPayoutProtocol: function(formContext) {
    var me = this;
    let defaultProtocolStore = me.buildDefaultProtocolStore();
    return {
      text: D.t("Payment Method"),
      flex: 1,
      sortable: true,
      menuDisabled: true,
      forceSelection: true,
      allowNull: false,
      name: "payment_protocol",
      dataIndex: "payment_protocol",
      xtype: "combocolumn",
      model: "Crm.modules.Transfers.model.TransfersModel",
      editor: me.buildProtocolName(),
      renderer: function(v, m, r) {
        if (v) {
          let filteredStore = defaultProtocolStore
            .getRange()
            .filter((e) => e.data.protocol_type == v);
          if (
            filteredStore &&
            filteredStore[0] &&
            filteredStore[0].data &&
            filteredStore[0].data.protocol_type
          ) {
            return filteredStore[0].data.protocol_type;
          }
        }
        return v;
      }
    };
  },

  buildProtocolName: function() {
    var me = this;
    return {
      xtype: "combo",
      name: "payment_protocol",
      forceSelection: true,
      editable: false,
      fieldSet: "protocol_type",
      valueField: "protocol_type",
      queryModel: "local",
      displayField: "protocol_type",
      triggerAction: "all",
      repeatTriggerClick: true,
      allowBlank: false,
      guideKeyField: "protocol_type",
      guideValueField: "protocol_type",
      store: me.buildDefaultProtocolStore(),
      listeners: {
        beforeQuery: function(queryPlan, eOpts) {
          try {
            let combo = queryPlan.combo;
            let gridData,
              gridSelection = combo.up("grid").getSelection();
            if (gridSelection && gridSelection.length) {
              gridData = gridSelection[0].getData();
              if (
                gridData &&
                gridData.payment_protocols &&
                combo &&
                combo.store &&
                combo.store.loadData
              ) {
                combo.store.loadData(gridData.payment_protocols);
              }
            }
          } catch (err) {}
        }
      }
    };
  },

  buildProtocolStore: function(headerStore) {
    var me = this;
    return Ext.create("Ext.data.Store", {
      fields: ["id", "protocol_type"],
      name: "headerStore",
      data: []
    });
  },
  addUpdatePayout: function(request) {
    var me = this,
      addPayoutWin,
      message = "";
    var addBtn = me.view.down("[action=merchBankAccAddEdit]");
    if (addBtn) {
      addBtn.disable();
    }
    function validatorTxt(v) {
      return v && v.toString().trim() != "" ? true : " Field should be valid.";
    }
    addPayoutWin = Ext.create("Ext.window.Window", {
      width: "50%",

      name: "payoutWin",

      title: "Execute payout",

      iconCls: "x-fa fa-files-o",

      modal: true,

      items: [
        {
          xtype: "form",
          layout: "anchor",
          name: "notes_form",
          padding: 10,
          defaults: { xtype: "textfield", anchor: "100%", labelWidth: 100 },
          items: [
            {
              name: "id",
              hidden: true,
              allowBlank: true,
              maxLength: 36,
              value: request && request.id ? request.id : ""
            },
            {
              hidden: true,
              editable: false,
              name: "status",
              value: request && request.status ? request.status : ""
            },
            {
              name: "message",
              xtype: "label",
              text: `${message}`,
              hidden: true
            },
            {
              name: "bank_name",
              xtype: "textfield",
              hidden: true
            },
            {
              flex: 1,
              xtype: "dependedcombo",
              name: "bank_id",
              labelWidth: 150,
              allowBlank: false,
              forceSelection: true,
              fieldLabel: D.t("Select Bank"),
              dataModel: Ext.create("Crm.modules.banks.model.BanksModel"),
              modelConfig: { collection: "banks", fieldSet: "id,name" },
              value: request && request.bank_id ? request.bank_id : "",
              valueField: "id",
              displayField: "name",
              listeners: {
                change: (el, v) => {
                  addPayoutWin
                    .down("[name=bank_name]")
                    .setValue(el.valueCollection.items[0].data.name);
                  addPayoutWin.down("[name=bank_account_id]").clearValue();
                  addPayoutWin.down("[name=toggleStatus]").setHidden(true);
                  const proxyFilters = {
                    filters: [
                      { _property: "bank_id", _value: v, _operator: "eq" }
                    ]
                  };

                  const store = Ext.create("Core.data.ComboStore", {
                    dataModel: Ext.create(
                      "Crm.modules.accountsPool.model.AccountsPoolModel"
                    ),
                    fieldSet: [
                      "id",
                      "account_name",
                      "acc_no",
                      "currency",
                      "status"
                    ],
                    exProxyParams: proxyFilters,
                    scope: this
                  });

                  addPayoutWin.down("[name=bank_account_id]").setStore(store);
                }
              }
            },
            {
              name: "account_name",
              xtype: "textfield",
              hidden: true
            },
            {
              name: "acc_no",
              xtype: "textfield",
              hidden: true
            },
            {
              xtype: "fieldcontainer",
              layout: "hbox",
              align: "center",
              defaults: {
                flex: 1
              },
              style: {
                "align-item": "center",
                "justify-content": "center"
              },
              items: [
                {
                  flex: 3,
                  xtype: "combo",
                  name: "bank_account_id",
                  labelWidth: 150,
                  typeAhead: true,
                  fieldLabel: D.t("Select Bank Account"),
                  valueField: "id",
                  forceSelection: true,
                  allowBlank: false,
                  value:
                    request && request.bank_account_id
                      ? request.bank_account_id
                      : "",
                  displayField: "account_name",
                  tpl:
                    '<tpl for="."><div class="x-boundlist-item" style="height:40px;"><b>{account_name}</b>&nbsp;&nbsp;(A/C: {acc_no})&nbsp;(Status: {status})</div><div style="clear:both"></div></tpl>',
                  listeners: {
                    change: (el, v) => {
                      if (
                        el.valueCollection &&
                        el.valueCollection.items &&
                        el.valueCollection.items.length
                      ) {
                        addPayoutWin
                          .down("[name=account_name]")
                          .setValue(
                            el.valueCollection.items[0].data.account_name
                          );
                        addPayoutWin
                          .down("[name=acc_no]")
                          .setValue(el.valueCollection.items[0].data.acc_no);
                      }
                    }
                  }
                },
                {
                  xtype: "button",
                  name: "toggleStatus",
                  action: "toggleBankStatus",
                  text: "",
                  hidden: true,
                  style: {
                    "margin-left": "10px"
                  }
                }
              ]
            },
            {
              flex: 1,
              xtype: "tagfield",
              name: "merchant_id",
              labelWidth: 150,
              fieldLabel: D.t("Select Merchant"),
              store: Ext.create("Core.data.ComboStore", {
                dataModel: Ext.create(
                  "Crm.modules.accounts.model.AccountsModel"
                ),
                fieldSet: ["id", "name"],
                scope: this
              }),
              valueField: "id",
              displayField: "name",
              listeners: {
                change: (el, v) => {
                  addPayoutWin.down("[name=merchant_account_id]").clearValue();
                  const proxyFilters = {
                    filters: [
                      { _property: "account_id", _value: v, _operator: "eq" }
                    ]
                  };
                  const store = Ext.create("Core.data.ComboStore", {
                    dataModel: Ext.create(
                      "Crm.modules.accounts.model.MerchantAccountsModel"
                    ),
                    fieldSet: ["id", "name", "currency"],
                    exProxyParams: proxyFilters,
                    scope: this
                  });
                  addPayoutWin
                    .down("[name=merchant_account_id]")
                    .setStore(store);
                }
              }
            },
            {
              flex: 1,
              xtype: "tagfield",
              labelWidth: 150,
              name: "merchant_account_id",
              fieldLabel: D.t("Select Merchant Account"),
              valueField: "id",
              displayField: "name",
              store: Ext.create("Core.data.ComboStore", {
                dataModel: Ext.create(
                  "Crm.modules.accounts.model.MerchantAccountsModel"
                ),
                fieldSet: ["id", "name", "currency"],
                scope: this
              })
            },
            {
              flex: 1,
              xtype: "dependedcombo",
              labelWidth: 150,
              name: "template",
              fieldLabel: D.t("Select Template"),
              allowBlank: false,
              fieldSet: "id, template_name, bank_id",
              valueField: "id",
              parentEl: "bank_id",
              parentField: "bank_id",
              displayField: "template_name",
              dataModel: "Crm.modules.banks.model.BanksBulkTemplatesModel"
            },
            {
              flex: 1,
              labelWidth: 150,
              xtype: "datefield",
              fieldLabel: "From Date",
              name: "from_date",
              maxValue: new Date(),
              format: "Y-m-d",
              listeners: {
                change: function(field, newValue, oldValue) {
                  var toDateField = field.up("form").down("[name=to_date]"); // Get the "to" date field
                  if (newValue > toDateField.getValue()) {
                    toDateField.setValue(newValue);
                  }
                }
              }
            },
            {
              flex: 1,
              labelWidth: 150,
              xtype: "datefield",
              maxValue: new Date(),
              fieldLabel: "To Date",
              name: "to_date",
              format: "Y-m-d",
              listeners: {
                change: function(field, newValue, oldValue) {
                  var fromDateField = field.up("form").down("[name=from_date]");
                  if (newValue < fromDateField.getValue()) {
                    fromDateField.setValue(newValue);
                  }
                }
              }
            },
            {
              xtype: "fieldcontainer",
              anchor: "100%",
              layout: "hbox",
              formBind: true,
              align: "center",
              items: [
                {
                  xtype: "button",
                  text: D.t("Get qualified payouts"),
                  margin: "0 3 0 0",
                  name: "getQualifiedPayouts",
                  listeners: {
                    click: async function(e, v) {
                      var actionBtn = e;
                      if (actionBtn) {
                        actionBtn.disable();
                      }
                      var reqData = this.up("form").getValues() || {};

                      me.model.runOnServer(
                        "getQualifiedPayouts",
                        reqData,
                        function(res) {
                          let message = "Total number of transactions found: ";
                          if (res.result) {
                            addPayoutWin
                              .down("[name=message]")
                              .setHidden(false)
                              .setText(`${message} ${res.result.length}`);
                            if (res.result.length === 0) {
                              addPayoutWin
                                .down("[name=exportPayoutCSV]")
                                .disable();
                            } else {
                              addPayoutWin
                                .down("[name=exportPayoutCSV]")
                                .enable();
                            }
                            actionBtn.enable();
                          } else {
                            actionBtn.enable();
                            if (res.error) {
                              D.a(res.error.title, res.error.message);
                            }
                          }
                        }
                      );
                    }
                  }
                },
                {
                  xtype: "button",
                  text: D.t("Export CSV"),
                  name: "exportPayoutCSV",
                  disabled: true,
                  listeners: {
                    click: async function(e, v) {
                      var actionBtn = e;
                      if (actionBtn) {
                        actionBtn.disable();
                      }
                      var reqData = this.up("form").getValues() || {};

                      me.model.runOnServer("exportPayoutCSV", reqData, function(
                        res
                      ) {
                        if (res.result) {
                          var items = res.result.payoutObject;
                          let csv = "";

                          for (let row = 0; row < items.length; row++) {
                            let keysAmount = Object.keys(items[row]).length;
                            let keysCounter = 0;
                            let itemsCounter = 0;
                            if (row === 0 && res.result.headers) {
                              for (let key in items[row]) {
                                csv +=
                                  key +
                                  (keysCounter + 1 < keysAmount ? "," : "\r\n");
                                keysCounter++;
                              }

                              for (let key in items[row]) {
                                csv +=
                                  items[row][key] +
                                  (itemsCounter + 1 < keysAmount
                                    ? ","
                                    : "\r\n");
                                itemsCounter++;
                              }
                            } else {
                              for (let key in items[row]) {
                                csv +=
                                  items[row][key] +
                                  (keysCounter + 1 < keysAmount ? "," : "\r\n");
                                keysCounter++;
                              }
                            }

                            keysCounter = 0;
                          }

                          let bank_name = addPayoutWin.down("[name=bank_name]")
                            .value;
                          let account_name = addPayoutWin.down(
                            "[name=account_name]"
                          ).value;
                          let acc_no = addPayoutWin.down("[name=acc_no]").value;
                          let link = document.createElement("a");
                          link.id = "download-csv";
                          link.setAttribute(
                            "href",
                            "data:text/plain;charset=utf-8," +
                              encodeURIComponent(csv)
                          );
                          link.setAttribute(
                            "download",
                            `${bank_name} ${acc_no} (${account_name}).csv`
                          );
                          document.body.appendChild(link);
                          document.querySelector("#download-csv").click();
                          link.remove();
                          actionBtn.enable();
                        } else {
                          actionBtn.enable();
                          if (res.error) {
                            D.a(res.error.title, res.error.message);
                          }
                        }
                      });
                    }
                  }
                }
              ]
            }
          ]
        }
      ],
      listeners: {
        close: function() {
          if (addBtn) {
            addBtn.enable();
          }
          if (me && me.view && me.view.store) {
            me.view.store.reload();
          }
        }
      }
    }).show();
  },

  setTemplatesStore: function(addPayoutWin, proxyFilters) {
    const templateStore = Ext.create("Core.data.ComboStore", {
      dataModel: Ext.create("Crm.modules.banks.model.BanksBulkTemplatesModel"),
      fieldSet: ["id", "template_name"],
      exProxyParams: proxyFilters,
      scope: this
    });

    addPayoutWin.down("[name=template]").setStore(templateStore);
  },

  addDepositDirect: function(request) {
    var me = this,
      addDeposit,
      message = "";
    var addBtn = me.view.down("[action=addDepositDirect]");
    if (addBtn) {
      addBtn.disable();
    }
    addDeposit = Ext.create("Ext.window.Window", {
      width: "50%",

      name: "payoutWin",

      title: "Deposit Direct",

      iconCls: "x-fa fa-money",

      modal: true,

      items: [
        {
          xtype: "form",
          layout: "anchor",
          name: "notes_form",
          padding: 10,
          defaults: { xtype: "textfield", anchor: "100%", labelWidth: 200 },
          items: [
            {
              fieldLabel: D.t("Destination System"),
              name: "dst_system",
              xtype: "textfield",
              value: D.t("Rapidogate System"),
              readOnly: true
            },
            {
              flex: 1,
              xtype: "combo",
              name: "account",
              allowBlank: false,
              forceSelection: true,
              queryMode: "all",
              editable: true,
              minChars: 1,
              fieldLabel: D.t("Select Merchant"),
              store: Ext.create("Core.data.ComboStore", {
                dataModel: Ext.create(
                  "Crm.modules.accounts.model.AccountsModel"
                ),
                fieldSet: ["id", "name"],
                scope: me
              }),
              valueField: "id",
              displayField: "name",
              listeners: {
                change: (el, v) => {
                  addDeposit.down("[name=merchant_account_id]").clearValue();
                  const proxyFilters = {
                    filters: [
                      {
                        _property: "account_id",
                        _value: v,
                        _operator: "eq"
                      }
                    ]
                  };
                  const store = Ext.create("Core.data.ComboStore", {
                    dataModel: Ext.create(
                      "Crm.modules.accounts.model.MerchantAccountsModel"
                    ),
                    fieldSet: ["id", "name", "currency"],
                    exProxyParams: proxyFilters,
                    scope: this
                  });
                  addDeposit.down("[name=merchant_account_id]").setStore(store);
                }
              }
            },
            {
              flex: 1,
              xtype: "combo",
              name: "merchant_account_id",
              fieldLabel: D.t("Select Merchant Account"),
              valueField: "id",
              allowBlank: false,
              forceSelection: true,
              queryMode: "all",
              editable: true,
              minChars: 1,
              displayField: "name",
              listeners: {
                change: (el, v) => {
                  if (!!v) {
                    if (
                      el.valueCollection &&
                      el.valueCollection.items &&
                      el.valueCollection.items.length
                    ) {
                      addDeposit
                        .down("[name=dst_currency]")
                        .setValue(el.valueCollection.items[0].data.currency);
                      addDeposit.down("[name=dst_bank_acc_id]").clearValue();
                      const proxyFilters = {
                        filters: [
                          {
                            _property: "merchant_account_id",
                            _value: v,
                            _operator: "eq"
                          }
                        ]
                      };
                      const store = Ext.create("Core.data.ComboStore", {
                        dataModel: Ext.create(
                          "Crm.modules.accountsPool.model.AccountsPoolModel"
                        ),
                        fieldSet: [
                          "id",
                          "account_name",
                          "acc_no",
                          "currency",
                          "status"
                        ],
                        exProxyParams: proxyFilters,
                        scope: this
                      });
                      addDeposit.down("[name=dst_bank_acc_id]").setStore(store);
                    }
                  }
                }
              }
            },
            {
              flex: 1,
              xtype: "combo",
              name: "dst_bank_acc_id",
              fieldLabel: D.t("Select Merchant Bank Account"),
              valueField: "id",
              forceSelection: true,
              allowBlank: true,
              hidden: true,
              displayField: "account_name",
              tpl:
                '<tpl for="."><div class="x-boundlist-item" style="height:40px;"><b>{account_name}</b>&nbsp;&nbsp;(A/C: {acc_no})&nbsp;(Status: {status})</div><div style="clear:both"></div></tpl>',
              listeners: {
                change: (el, v) => {
                  if (
                    !!v &&
                    el.valueCollection &&
                    el.valueCollection.items &&
                    el.valueCollection.items.length
                  ) {
                    me.checkActiveStatus(
                      "dst_bank_acc_id",
                      el.valueCollection.items[0].data.status,
                      addDeposit
                    );
                  }
                }
              }
            },

            {
              fieldLabel: D.t("Destination Currency"),
              name: "dst_currency",
              xtype: "textfield",
              readOnly: true,
              listeners: {
                change: (el, v) => {
                  if (!!v) {
                    me.isChangeCurrency(addDeposit);
                  }
                }
              }
            },
            {
              flex: 1,
              xtype: "combo",
              name: "bank_id",
              allowBlank: false,
              store: Ext.create("Core.data.ComboStore", {
                dataModel: Ext.create("Crm.modules.banks.model.BanksModel"),
                fieldSet: ["id", "name"],
                exProxyParams: {
                  filters: [
                    {
                      _property: "bank_type",
                      _value: 1, // Exchange banks
                      _operator: "eq"
                    }
                  ]
                },
                scope: me
              }),
              forceSelection: true,
              fieldLabel: D.t("Select Source Bank"),
              valueField: "id",
              displayField: "name",
              listeners: {
                change: (el, v) => {
                  addDeposit.down("[name=src_bank_acc_id]").clearValue();

                  const proxyFilters = {
                    filters: [
                      { _property: "bank_id", _value: v, _operator: "eq" },
                      { _property: "status", _value: "ACTIVE", _operator: "eq" }
                    ]
                  };

                  const store = Ext.create("Core.data.ComboStore", {
                    dataModel: Ext.create(
                      "Crm.modules.accountsPool.model.AllAccountsModel"
                    ),
                    fieldSet: [
                      "id",
                      "account_name",
                      "acc_no",
                      "currency",
                      "status"
                    ],
                    exProxyParams: proxyFilters,
                    scope: this
                  });

                  addDeposit.down("[name=src_bank_acc_id]").setStore(store);
                }
              }
            },

            {
              flex: 1,
              xtype: "combo",
              name: "src_bank_acc_id",

              typeAhead: true,
              fieldLabel: D.t("Select Source Bank Account"),
              valueField: "id",
              forceSelection: true,
              allowBlank: false,
              displayField: "account_name",
              tpl:
                '<tpl for="."><div class="x-boundlist-item" style="height:40px;"><b>{account_name}</b>&nbsp;&nbsp;(A/C: {acc_no})&nbsp;(Status: {status})</div><div style="clear:both"></div></tpl>',
              listeners: {
                change: (el, v) => {
                  if (
                    el.valueCollection &&
                    el.valueCollection.items &&
                    el.valueCollection.items.length
                  ) {
                    addDeposit
                      .down("[name=src_currency]")
                      .setValue(el.valueCollection.items[0].data.currency);
                  }
                }
              }
            },
            {
              fieldLabel: D.t("Source Currency"),
              name: "src_currency",
              readOnly: true,
              listeners: {
                change: (el, v) => {
                  if (!!v) {
                    addDeposit
                      .down("[name=brn]")
                      .setConfig(
                        "fieldLabel",
                        __CONFIG__.isCryptoCurrency(v)
                          ? D.t("Hash Id")
                          : D.t("Bank Reference Number")
                      );

                    me.isChangeCurrency(addDeposit);
                  }
                }
              }
            },

            {
              flex: 1,
              fieldLabel: D.t("Source Amount"),
              xtype: "numberfield",
              name: "src_amount",
              allowBlank: false,
              minValue: 0,
              decimalPrecision: 6,
              listeners: {
                change: (el, v) => {
                  me.calculateConversionRate(v, addDeposit);
                }
              }
            },

            {
              flex: 1,
              fieldLabel: D.t("Destination Amount"),
              xtype: "numberfield",
              name: "dst_amount",
              allowBlank: false,
              minValue: 0,
              decimalPrecision: 6,
              listeners: {
                change: (el, v) => {
                  me.calculateConversionRate(v, addDeposit);
                  me.checkFee(addDeposit);
                }
              }
            },
            {
              flex: 1,
              fieldLabel: "Conversion Rate:",
              name: "conversion_rate",
              allowBlank: true,
              readOnly: true
            },
            {
              flex: 1,
              fieldLabel: D.t("Fee "),
              name: "fee",
              value: 0,
              xtype: "numberfield",
              minValue: 0,
              decimalPrecision: 6,
              listeners: {
                change: (el, v) => {
                  me.checkFee(addDeposit);
                }
              }
            },
            {
              flex: 1,
              fieldLabel: D.t("Bank Reference Number"),
              name: "brn",
              allowBlank: false
            },
            {
              xtype: "textarea",
              name: "note",
              maxLength: 150,
              fieldLabel: D.t("Deposit Note")
            },
            {
              xtype: "fieldcontainer",
              anchor: "100%",
              layout: "hbox",

              align: "center",
              items: [
                {
                  xtype: "button",
                  formBind: true,
                  text: D.t("Submit"),
                  margin: "0 3 0 0",
                  name: "createDeposit",
                  listeners: {
                    click: async function(e, v) {
                      var actionBtn = e;
                      if (actionBtn) {
                        // actionBtn.disable();
                      }
                      var reqData = this.up("form").getValues() || {};

                      me.model.runOnServer("createDeposit", reqData, function(
                        res
                      ) {
                        let defaultMsg = `Created direct deposit transfer`;
                        if (res.result) {
                          defaultMsg += ` has been successfully marked as accepted in the system`;
                          D.a(
                            "Transfer Accept Successful",
                            res.result.message || defaultMsg
                          );
                          addDeposit.close();
                        } else {
                          if (res && res.error && res.error.message) {
                            res.message = res.error.message;
                          }
                          defaultMsg += ` has failed to be marked. Please try again or Contact Support`;
                          D.a(
                            "Transfer Accept Failed",
                            res.message || defaultMsg
                          );
                        }
                      });
                    }
                  }
                },
                {
                  xtype: "button",
                  text: D.t("Close"),
                  margin: "0 3 0 0",
                  disabled: false,
                  name: "close",
                  listeners: {
                    click: async function(e, v) {
                      addDeposit.close();
                    }
                  }
                }
              ]
            }
          ]
        }
      ],
      listeners: {
        close: function() {
          if (addBtn) {
            addBtn.enable();
          }
          if (me && me.view && me.view.store) {
            me.view.store.reload();
          }
        }
      }
    }).show();
  },
  datesFromTo: function(isDate) {
    return Ext.create("Core.form.DatesFromTo", {
      anchor: "100%",
      name: "period",
      fieldLabel: "Transactions date from and to",
      getValueFormat: "Y-m-d",
      disabled: isDate,
      buildDateField: function(name) {
        var currentDate = new Date();
        return {
          xtype: "datefield",
          fname: name,
          flex: 1,
          disabled: isDate,

          format: this.format,
          isFormField: false,
          emptyText: this.labels[name]
        };
      }
    });
  },
  buildReportTypeCombo: function() {
    return {
      fieldLabel: D.t("Type of report"),
      xtype: "combo",
      queryMode: "all",
      displayField: "desc",
      valueField: "type",
      name: "type",
      allowNull: false,
      allowBlank: false,
      forceSelection: true,
      queryMode: "all",
      editable: true,
      minChars: 1,
      store: Ext.create("Ext.data.Store", {
        fields: ["type", "desc"],
        data: __CONFIG__.getReportTypeCombo()
      }),
      listeners: {
        change: function(e, newVal, oldVal, eOpts) {
          if (newVal) {
            this.up("form")
              .down("[name=download]")
              .enable();
          } else {
            this.up("form")
              .down("[name=download]")
              .disable();
          }
        }
      }
    };
  },
  downloadReport: function(filter, isDate) {
    var me = this,
      downloadReports;

    var downloadReports = me.view.down("[action=downloadReport]");

    downloadReports = Ext.create("Ext.window.Window", {
      width: "40%",

      name: "download_report",

      title: "Download Report",

      iconCls: "x-fa fa-cloud-download",

      modal: true,

      items: [
        {
          xtype: "form",
          layout: "anchor",
          name: "notes_form",
          padding: 10,
          defaults: { xtype: "textfield", anchor: "100%", labelWidth: 200 },
          items: [
            me.buildReportTypeCombo(),
            me.datesFromTo(isDate),
            {
              xtype: "button",
              text: D.t("Download"),
              name: "download",

              style: {
                width: 50
              },
              disabled: true,
              listeners: {
                click: async function(e, v) {
                  var actionBtn = e;
                  if (actionBtn) {
                    actionBtn.disable();
                  }
                  var reqData = this.up("form").getValues() || {};
                  if (!reqData || !reqData.type) {
                    actionBtn.enable();
                    return;
                  }

                  let startDate = null,
                    endDate = null;
                  if (!isDate && reqData && reqData.period) {
                    startDate = reqData.period.from;
                    endDate = reqData.period.to;
                  } else if (filter.ctime) {
                    const parts = filter.ctime.split(".");
                    startDate = parts[1] + "." + parts[0] + "." + parts[2];
                    endDate = parts[1] + "." + parts[0] + "." + parts[2];
                  }
                  try {
                    for (let key in filter) {
                      reqData[key] = filter[key];
                    }
                    reqData.startDate = startDate;
                    reqData.endDate = endDate;
                    const res = await me.model.callApi(
                      "account-service",
                      "downloadRecords",
                      reqData,
                      null,
                      null
                    );

                    if (res.code) {
                      let link_url = res.downloadUrl;
                      let link = document.createElement("a");
                      link.setAttribute("href", link_url);
                      link.click();
                      downloadReports.close();
                    } else if (res) {
                    }
                  } catch (error) {
                    D.a(error.title, error.message);
                  }

                  actionBtn.enable();
                }
              }
            }
          ]
        }
      ],
      listeners: {
        close: function() {
          if (downloadReports) {
            downloadReports.enable();
          }
          if (me && me.view && me.view.store) {
            me.view.store.reload();
          }
        }
      }
    }).show();
  },
  calculateConversionRate(v, addDeposit) {
    let conversion_rate = 0,
      src_amount = addDeposit.down("[name=src_amount]").getValue(),
      dst_amount = addDeposit.down("[name=dst_amount]").getValue(),
      src_currency = addDeposit.down("[name=src_currency]").getValue();
    dst_currency = addDeposit.down("[name=dst_currency]").getValue();
    if (!!src_currency && src_currency === dst_currency) {
      addDeposit.down("[name=dst_amount]").setValue(src_amount);
    }
    if (isNaN(dst_amount) || isNaN(src_amount) || !dst_amount || !src_amount) {
      return 0;
    }

    conversion_rate = Number(dst_amount) / Number(src_amount);
    conversion_rate = __CONFIG__.formatCurrency(
      conversion_rate,
      0,
      src_currency
    );
    addDeposit.down("[name=conversion_rate]").setValue(conversion_rate);
  },
  async isChangeCurrency(addDeposit) {
    const me = this;
    let src_currency = addDeposit.down("[name=src_currency]").getValue();
    let dst_currency = addDeposit.down("[name=dst_currency]").getValue();
    const exchangeBank = addDeposit.down("[name=bank_id]").getValue();
    const exchangeBankAccount = addDeposit.down("[name=src_bank_acc_id]");
    if (src_currency === dst_currency) {
      addDeposit.down("[name=conversion_rate]").setConfig("hidden", true);
      addDeposit.down("[name=dst_amount]").setConfig("hidden", true);
      exchangeBankAccount.clearInvalid();
      exchangeBankAccount.validator = function() {
        return true;
      };
    } else {
      addDeposit.down("[name=conversion_rate]").setConfig("hidden", false);
      addDeposit.down("[name=dst_amount]").setConfig("hidden", false);
    }
    if (!!src_currency && !!dst_currency && src_currency !== dst_currency) {
      if (exchangeBank) {
        await me.model.runOnServer(
          "checkExchangeBankAccountForCurrency",
          {
            currency: dst_currency,
            bank_id: exchangeBank
          },
          function(res) {
            if (res && res.length) {
              exchangeBankAccount.clearInvalid();
              exchangeBankAccount.validator = function() {
                return true;
              };
            } else {
              let errMsg = `Please select merchant account of ${src_currency} or create new exchange type bank account with currency ${dst_currency} under the same exchange bank`;
              exchangeBankAccount.markInvalid(errMsg);
              exchangeBankAccount.validator = function() {
                return errMsg;
              };
              D.a(
                `${src_currency} currency type bank account not available`,
                errMsg
              );
            }
          }
        );
      }
    }
  },
  checkActiveStatus(name, status, addDeposit) {
    const bankAccount = addDeposit.down(`[name=${name}]`);
    if (status === "ACTIVE") {
      bankAccount.clearInvalid();
      bankAccount.validator = function() {
        return true;
      };
    } else {
      let errMsg = `please select Active bank account`;
      bankAccount.markInvalid(errMsg);
      bankAccount.validator = function() {
        return errMsg;
      };
    }
  },
  checkFee(addDeposit) {
    let dst_amount = addDeposit.down("[name=dst_amount]").getValue();
    const fee = addDeposit.down("[name=fee]");
    let v = fee.getValue();
    if (dst_amount && v <= parseFloat(dst_amount)) {
      fee.clearInvalid();
      fee.validator = function() {
        return true;
      };
    } else {
      const errMsg = "Maximum amount cannot be smaller than destination amount";
      fee.markInvalid(errMsg);
      fee.validator = function() {
        return errMsg;
      };
    }
  },
  export: async function(prop) {
    const form = this.view
      .down("form")
      .getForm()
      .getValues();
    var period_start, period_end;
    if (form.start_date && form.end_date) {
      period_start = new Date(form.start_date).toISOString();
      period_end = new Date(form.end_date).toISOString();
    }

    const me = this;
    let opt = {
      report_name: prop.report_name,
      filename: prop.fileName
    };
    const accountsReportData = await me.model.callApi(
      "account-service",
      "getAccountsReportData",
      {
        from_date: period_start ? period_start : null,
        to_date: period_end ? period_end : null
      },
      null,
      null
    );
    opt["format"] = prop.format;
    opt["data"] = {
      table_3: [
        {
          date: accountsReportData.period
        }
      ],
      table_4: accountsReportData.reportData
    };
    const res = await me.model.callApi(
      "account-service",
      "exportUsingJasper",
      opt,
      null,
      null
    );
    let link_url = `${__CONFIG__.downloadFileLink}/${res.code}`;
    let link = document.createElement("a");
    link.setAttribute("href", link_url);
    link.click();
  }
});
