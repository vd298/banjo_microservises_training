Ext.define("Crm.modules.Transfers.view.templates.default", {
  tpl: `
    <li>Transfer ID: <span>{id}</span></li>
    <li>Order ID: <span>{order_num}</span></li>
    <li>Transfer type: <span>{transfer_type}</span></li>
    <li>Status: <span>{activity}</span></li>
    <li>Event Name: <span>{event_name}</span></li>
    <li>Source Amount: <span>{src_amount:number("0.00")} {src_currency}</span></li>
    <li>Destination Amount: <span>{dst_amount:number("0.00")} {dst_currency}</span></li>
    <li>Reference Number: <span>{ref_num}</span></li>
    <tpl if="data.original_submitted_brn">
    <li>Originally submitted BRN: <span>{data.original_submitted_brn}</span></li>
    </tpl>
    <li>Bank Reference Number: <span>{last_brn}</span></li>
    <li>Date: <span>{ctime:date("d.m.Y H:i")}</span></li>
    <tpl if="data.original_src_amount">
    <li>Original Source Amount: <span>{data.original_src_amount}</span></li>
    </tpl>
    <tpl if="data.original_dst_amount">
    <li if="data.original_dst_amount">Original Destination Amount: <span>{data.original_dst_amount}</span></li>
    </tpl>    
    <tpl if="data && !!data.user_id">
    <li>External User : <span>{data.user_id}</span></li>
    </tpl>
    <tpl if="balance_status">
    <li if="balance_status">Balance Status:
      <tpl if="balance_status==='Sufficient Balance, Can Execute'"> 
        <span  style="color:green;font-weight:bold">{balance_status}</span>
      </tpl>
      <tpl if="balance_status==='Insufficient Balance, DO NOT EXECUTE'">
        <span  style="color:red;font-weight:bold">{balance_status}</span>
      </tpl>
    </li>
    </tpl>
    
    <tpl if="counterpart_id">
      <p style="font-size:1.8vh;"><b><u> Counterpart Information</u></b></p>
      <tpl if="counter_party_name"><li>Counterpart Name: <span> {counter_party_name} </span></li></tpl>
    </tpl>
    <tpl if="action_type">
      <p style="font-size:1.8vh;"><b><u> Final Status Update Information</u></b></p>
      <tpl if="action_type"><li>Status Updated by: <span> {action_type} </span></li></tpl>
      <tpl if="activity_action === 'APPROVED'"><li>Status Action performed: <span style="color:green;font-weight:bold"> {activity_action} </span></li></tpl>
      <tpl if="activity_action === 'SETTLEMENT_APPROVED'"><li>Status Action performed: <span style="color:green;font-weight:bold"> {activity_action} </span></li></tpl>
      <tpl if="activity_action === 'REJECTED'"><li>Status Action performed: <span style="color:red;font-weight:bold"> {activity_action} </span></li></tpl>
      <tpl if="admin_name"><li>Admin Name: <span> {admin_name} </span></li></tpl>
    </tpl>
    <tpl if="src_wallet_id">
      <p style="font-size:1.8vh;"><b><u> Source Wallet Information</u></b></p>
      <tpl if="src_wallet_label"><li>Label: <span> {src_wallet_label} </span></li></tpl>
      <tpl if="src_wallet_acc_no"><li>Bank Account Number: <span> {src_bank_acc_no} </span></li></tpl>
      <tpl if="src_wallet_acc_no"><li>Wallet Account Number: <span> {src_wallet_acc_no} </span></li></tpl>
      <tpl if="src_wallet_currency"><li>Currency: <span> {src_wallet_currency} </span></li></tpl>
      <tpl if="src_wallet_ref_num"><li>Reference Number: <span> {src_wallet_ref_num} </span></li></tpl>
      <tpl if="src_wallet_type"><li>Type: <span> {src_wallet_type} </span></li></tpl>
    </tpl>
    <tpl if="dst_wallet_id">
      <p style="font-size:1.8vh;"><b><u> Destination Wallet Information</u></b></p>
      <tpl if="dst_wallet_label"><li>Label: <span> {dst_wallet_label} </span></li></tpl>
      <tpl if="dst_wallet_acc_no"><li>Bank Account Number: <span> {dst_bank_acc_no} </span></li></tpl>
      <tpl if="dst_wallet_acc_no"><li>Wallet Account Number: <span> {dst_wallet_acc_no} </span></li></tpl>
      <tpl if="dst_wallet_currency"><li>Currency: <span> {dst_wallet_currency} </span></li></tpl>
      <tpl if="dst_wallet_ref_num"><li>Reference Number: <span> {dst_wallet_ref_num} </span></li></tpl>
      <tpl if="dst_wallet_type"><li>Type: <span> {dst_wallet_type} </span></li></tpl>
    </tpl>
    <tpl if="transfer_type">
      <p style="font-size:1.8vh;"><b><u>Beneficiary Information</u></b></p>
      <tpl if="beneficiary_first_name"><li>Name: <span> {beneficiary_first_name} {beneficiary_last_name} </span></li></tpl>
      <tpl if="beneficiary_vpa"><li>VPA: <span> {beneficiary_vpa} </span></li></tpl>
      <tpl if="beneficiary_acc_no"><li>Account Number: <span> {beneficiary_acc_no} </span></li></tpl>
      <tpl if="beneficiary_ifsc"><li>Account IFSC: <span> {beneficiary_ifsc} </span></li></tpl>
    </tpl>
    <tpl if="account_id">
      <p style="font-size:1.8vh;"><b><u> Merchant Information</u></b></p>
      <tpl if="src_acc_name"><li>Name: <span><a href='#Crm.modules.accounts.view.AccountsForm~{account_id}' target='_blank'>{[values.src_acc_name]}</a></span></li></tpl>
      <tpl if="src_acc_registration_no"><li>Registration Number: <span> {src_acc_registration_no} </span></li></tpl>
      <tpl if="src_acc_status"><li>Status: <span> {src_acc_status} </span></li></tpl>
      <tpl if="src_acc_country"><li>Country Code: <span> {src_acc_country} </span></li></tpl>
      <tpl if="src_acc_type"><li>Type: <span> {src_acc_type} </span></li></tpl>
    </tpl>
    <tpl if="merchant_account_id">
      <p style="font-size:1.8vh;"><b><u> Merchant Account Information</u></b></p>
      <tpl if="src_macc_name"><li>Name: <span> {src_macc_name} </span></li></tpl>
      <tpl if="src_macc_no"><li>Account Number: <span> {src_macc_no} </span></li></tpl>
      <tpl if="src_macc_status"><li>Status: <span> {src_macc_status} </span></li></tpl>
    </tpl>
    <tpl if="src_bank_short_name">
      <p style="font-size:1.8vh;"><b><u> Bank Information</u></b></p>
      <tpl if="src_bank_short_name"><li>Short Name: <span> {src_bank_short_name} </span></li></tpl>
      <tpl if="src_bank_name"><li>Name: <span> {src_bank_name} </span></li></tpl>
      <tpl if="src_bank_address"><li>Address: <span> {src_bank_address} </span></li></tpl>
      <tpl if="src_bank_city"><li>City: <span> {src_bank_city} </span></li></tpl>
      <tpl if="src_bank_country"><li>Country: <span> {src_bank_country} </span></li></tpl>
    </tpl>
  `,
  build() {
    return `<ul>${this.tpl}</ul>`;
  }
});
