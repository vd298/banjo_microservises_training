Ext.define("Crm.modules.Transfers.view.TransfersGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Transfers"),
  iconCls: "x-fa fa-dollar",

  filterbar: true,
  filterable: true,

  controllerCls: "Crm.modules.Transfers.view.TransfersGridController",

  buildColumns: function() {
    var me = this;
    return [
      {
        text: D.t(" Transaction Id"),
        dataIndex: "id",
        hidden: true,
        filter: true
      },
      {
        text: D.t(" Settlement Id"),
        dataIndex: "settlement_id",
        hidden: true
      },
      {
        text: D.t(" Source Bank"),
        flex: 1,
        sortable: true,
        dataIndex: "src_bank_name",
        filter: true,
        hidden: true
      },
      {
        text: D.t("Source Bank Account"),
        flex: 1,
        sortable: true,
        dataIndex: "src_wallet_label",
        filter: true,
        hidden: true,
        text: D.t("Source Bank Account")
      },
      {
        text: D.t("Source Bank Account number"),
        flex: 1,
        sortable: true,
        dataIndex: "src_bank_acc_no",
        filter: true,
        hidden: true
      },
      {
        text: D.t("Destination bank"),
        flex: 1,
        sortable: true,
        dataIndex: "dst_bank_name",
        filter: true,
        hidden: true
      },
      {
        text: D.t("Destination bank account"),
        flex: 1,
        sortable: true,
        dataIndex: "dst_wallet_label",
        filter: true,
        hidden: true
      },
      {
        text: D.t("Destination bank account number"),
        flex: 1,
        sortable: true,
        dataIndex: "dst_bank_acc_no",
        filter: true,
        hidden: true
      },
      {
        //text: D.t("Source Amount"),
        text: D.t("Amount"),
        flex: 1,
        sortable: true,
        dataIndex: "src_amount",
        filter: { xtype: "numberfield" },
        renderer: function(v, m, r) {
          if (v) {
            return __CONFIG__.formatCurrency(v, 0, r.data.src_currency);
          }
          return v;
        }
      },
      {
        text: D.t("Date"),
        flex: 1,
        dataIndex: "ctime",
        xtype: "datecolumn",
        sortable: true,
        format: "d.m.Y H:i:s",
        filter: {
          xtype: "datefield",
          format: "d.m.Y"
        }
      },
      {
        //text: D.t("Source Currency"),
        text: D.t("Currency"),
        flex: 1,
        sortable: true,
        dataIndex: "src_currency",
        filter: me.buildCurrencyCombo()
      },
      {
        text: D.t("Dest Amount"),
        flex: 1,
        sortable: true,
        dataIndex: "dst_amount",
        filter: true,
        hidden: true,
        renderer: function(v, m, r) {
          if (v) {
            return __CONFIG__.formatCurrency(v, 0, r.data.dst_currency);
          }
          return v;
        }
      },
      {
        text: D.t("Dest Currency"),
        flex: 1,
        sortable: true,
        dataIndex: "dst_currency",
        filter: true,
        hidden: true
      },
      {
        text: D.t("Transfer Type"),
        flex: 1,
        sortable: true,
        dataIndex: "transfer_type",
        filter: me.buildTransferTypeCombo()
      },
      {
        text: D.t("Status"),
        flex: 1,
        sortable: true,
        dataIndex: "activity",
        filter: me.buildTransferActivityCombo()
      },
      {
        text: D.t("Reference Number"),
        flex: 1,
        sortable: true,
        dataIndex: "ref_num",
        filter: true
      },
      {
        text: D.t("Exchange"),
        flex: 1,
        sortable: true,
        dataIndex: "is_exchange",
        filter: true,
        renderer: (v, m, r) => {
          return v ? "Exchange" : "Normal";
        }
      },
      {
        text: D.t("Order Number"),
        flex: 1,
        sortable: true,
        dataIndex: "order_num",
        filter: true
      },
      {
        text: D.t("Counterpart"),
        flex: 1,
        sortable: true,
        dataIndex: "counterpart_id",
        hidden: true,
        filter: true,
        renderer: (v, m, r) => {
          return r && r.data && r.data.counter_party_name
            ? r.data.counter_party_name
            : v;
        }
      },
      {
        text: D.t("Counter Party Name"),
        hidden: true,
        dataIndex: "counter_party_name"
      },
      {
        text: D.t("Account Id"),
        hidden: true,
        dataIndex: "account_id"
      },
      {
        text: D.t("Merchant Name"),
        flex: 1,
        sortable: true,
        dataIndex: "src_acc_name",
        filter: true,
        renderer: (v, m, r) => {
          if (v && typeof v == "string" && v.length) {
            return (
              '<a href="#Crm.modules.accounts.view.AccountsForm~' +
              r.data.account_id +
              '">' +
              r.data.src_acc_name +
              "</a>"
            );
          }
        }
      },
      {
        text: D.t("Merchant Account Name"),
        flex: 1,
        sortable: true,
        dataIndex: "src_macc_name",
        filter: true,
        renderer: (v, m, r) => {
          return r && r.data && r.data.src_macc_name ? r.data.src_macc_name : v;
        }
      },
      {
        text: D.t("Bank Reference Number"),
        flex: 1,
        sortable: true,
        dataIndex: "last_brn",
        filter: true,
        renderer: (v, m, r) => {
          if (
            v &&
            typeof v == "string" &&
            v.length &&
            __CONFIG__.isCryptoCurrency(r.data.dst_currency)
          ) {
            return (
              '<a href="' +
              __CONFIG__.BlockchainUrl +
              r.data.dst_currency +
              "/" +
              v +
              '" target="_blank" >' +
              v +
              "</a>"
            );
          }
          return v;
        }
      },
      {
        text: D.t("Src Wallet Id"),
        dataIndex: "src_wallet_id",
        hidden: true
      },
      {
        text: D.t("Exchange Id"),
        dataIndex: "exchange_id",
        hidden: true
      }
    ];
  },

  buildCurrencyCombo: function() {
    return {
      xtype: "combo",
      queryMode: "all",
      displayField: "currencyDisplay",
      valueField: "abbr",
      editable: false,
      forceSelection: true,
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.bankAccounts.model.currencyCombo"),
        fieldSet: ["name", "abbr"],
        scope: this
      })
    };
  },

  buildTransferTypeCombo: function() {
    if (__CONFIG__) {
      return {
        xtype: "combo",
        queryMode: "all",
        displayField: "type",
        valueField: "type",
        store: Ext.create("Ext.data.Store", {
          fields: ["type"],
          data: __CONFIG__.getTxTransferType()
        })
      };
    } else return true;
  },

  buildTransferActivityCombo: function() {
    if (__CONFIG__) {
      return {
        xtype: "combo",
        queryMode: "all",
        displayField: "type",
        valueField: "type",
        store: Ext.create("Ext.data.Store", {
          fields: ["type"],
          data: __CONFIG__.getTxActivityTypeGrid()
        })
      };
    } else return true;
  },

  buildButtonsColumns() {
    var me = this;
    return [
      {
        xtype: "actioncolumn",
        width: 90,
        menuDisabled: true,
        items: [
          {
            iconCls: "x-fa fa-search",
            tooltip: D.t("Show transfer"),
            isDisabled: () => {
              return !this.permis.modify && !this.permis.read;
            },
            handler: (grid, rowIndex) => {
              this.fireEvent("edit", grid, rowIndex);
            }
          },
          {
            iconCls: "x-fa fa-check-circle",
            name: "txAccept",
            tooltip: D.t("Accept Transfer."),
            isDisabled: (view, rowIndex, colIndex, item, record) => {
              let permission = !this.permis.modify && !this.permis.read;
              let currStatus = record.get("activity");
              if (permission == true) {
                return permission;
              } else if (
                currStatus &&
                ["APPROVED", "REJECTED"].indexOf(currStatus) == -1
              ) {
                return false;
              } else return permission;
            },
            handler: function(grid, rowIndex) {
              me.fireEvent("gridTxAccept", grid, rowIndex);
            }
          },
          {
            iconCls: "x-fa fa-times-circle",
            name: "txReject",
            tooltip: D.t("Reject Transfer."),
            isDisabled: (view, rowIndex, colIndex, item, record) => {
              let permission = !this.permis.modify && !this.permis.read;
              let currStatus = record.get("status");
              if (permission == true) {
                return permission;
              } else if (
                currStatus &&
                ["APPROVED", "REJECTED", "SETTLEMENT_APPROVED"].indexOf(
                  currStatus
                ) == -1
              ) {
                return false;
              } else return permission;
            },
            handler: function(grid, rowIndex) {
              me.fireEvent("gridTxReject", grid, rowIndex);
            }
          }
        ]
      }
    ];
  },

  buildTbar: function() {
    var items = [
      {
        text: D.t("Add"),
        tooltip: D.t("Create Settlement Payout Request"),
        iconCls: "x-fa fa-plus",
        scale: "medium",
        action: "add"
      }
    ];

    if (this.importButton) {
      items.push("-", {
        text: this.buttonImportText,
        iconCls: "x-fa fa-cloud-download",
        action: "import"
      });
    }
    if (this.exportButton) {
      items.push("-", {
        text: this.buttonExportText,
        iconCls: "x-fa fa-cloud-upload",
        action: "export"
      });
    }

    items.push(
      "-",
      {
        text: "Execute payout",
        tooltip: "Execute payout",
        iconCls: "x-fa fa-dollar",
        scale: "medium",
        action: "executePayout"
      },
      "-",
      {
        text: "Direct Deposit",
        tooltip: "Direct Deposit",
        iconCls: "x-fa fa-money",
        scale: "medium",
        action: "addDepositDirect"
      },
      "-",
      {
        text: "Reports",
        tooltip: "Reports",
        iconCls: "x-fa fa-cloud-download",
        scale: "medium",
        action: "downloadReport"
      },
      "-",
      {
        tooltip: this.buttonReloadText,
        iconCls: "x-fa fa-refresh",
        action: "refresh"
      }
    );

    if (this.filterable) items.push("->", this.buildSearchField());
    return items;
  }
});
