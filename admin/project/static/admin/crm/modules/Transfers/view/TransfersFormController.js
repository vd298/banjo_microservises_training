Ext.define("Crm.modules.Transfers.view.TransfersFormController", {
  extend: "Core.form.FormController",

  setControls() {
    this.control({
      "[action=accept]": {
        click: async () => {
          const me = this;
          if (me.data.transfer_type === "SETTLEMENT") {
            await me.model.runOnServer(
              "fetchSettlementDetails",
              {
                id: me.data.settlement_id,
                wallet_id: me.data.src_wallet_id
              },
              function(res) {
                if (res && res.id) {
                  me.data.markup_rate = res.markup_rate;
                  me.data.conversion_rate = res.conversion_rate;
                  me.data.max_src_amount = res.max_src_amount;
                  me.data.src_amount_settlement = res.src_amount;
                  me.data.dst_amount_settlement = res.dst_amount;
                  me.accept();
                }
              }
            );
          } else me.accept();
        }
      },
      "[action=syncTransferStatus]": {
        click: () => {
          this.syncTransferStatus();
        }
      },
      "[action=reject]": {
        click: () => {
          this.reject();
        }
      },
      "[action=changeSourceAccount]": {
        click: () => {
          this.changeSourceAccount();
        }
      }
    });
    this.callParent(arguments);
  },
  afterDataLoad(data, cb) {
    const me = this;
    if (data.activity && data.activity === "PENDING") {
      this.view.down("[name=syncTransferStatus]").enable();
    } else {
      this.view.down("[name=syncTransferStatus]").disable();
      this.view.down("[name=changeSourceAccount]").disable();
    }

    if (data.transfer_type && data.transfer_type === "PAYOUT") {
      this.view.down("[name=changeSourceAccount]").setConfig("hidden", false);
    } else {
      this.view.down("[name=changeSourceAccount]").setConfig("hidden", true);
    }

    if (
      data.activity &&
      [
        "APPROVED",
        "REJECTED",
        "SETTLEMENT_APPROVED",
        "INTERNAL_FUNDS_SHIFT",
        "EXCHANGE_WITHDRAW"
      ].indexOf(data.activity) >= 0
    ) {
      this.view.down("[action=accept]").setDisabled(true);
      this.view.down("[action=reject]").setDisabled(true);
    } else {
      this.view.down("[action=accept]").setDisabled(false);
      this.view.down("[action=reject]").setDisabled(false);
    }
    if (
      data.transfer_type &&
      data.transfer_type === "PAYOUT" &&
      data.activity &&
      data.activity === "REQUEST" &&
      data.account_id &&
      data.merchant_account_id &&
      data.src_wallet_id
    ) {
      me.model.runOnServer("checkSufficientBalance", data, function(resp) {
        if (resp) {
          data.balance_status = "Sufficient Balance, Can Execute";
          me.view.down("[name=accept]").enable();
        } else {
          data.balance_status = "Insufficient Balance, DO NOT EXECUTE";
          me.view.down("[name=accept]").disable();
        }
        cb(data);
      });
    } else {
      cb(data);
    }
  },
  async syncTransferStatus(store) {
    var me = this;
    let currentData = me.data;
    me.model.runOnServer("syncTransferStatus", currentData, function(
      acceptResp
    ) {
      if (acceptResp && acceptResp.error && acceptResp.error.message) {
        let defaultMsg = `Selected transfer`;
        defaultMsg += ` with Reference number:${currentData.ref_num}`;
        D.a("Transfer Sync Failed", acceptResp.error.message || defaultMsg);
      } else if (acceptResp && acceptResp.result && acceptResp.result.message) {
        D.a(acceptResp.result.message);
      }
    });
  },
  isShowing(param) {
    return param !== "SETTLEMENT";
  },
  async accept(store) {
    var me = this;
    let currentData = me.data;
    var acceptWindow = Ext.create("Ext.window.Window", {
      title: "Transfer Approval:",
      layout: "fit",
      width: "80vh",
      //margin: 30,
      modal: true,
      items: [
        {
          xtype: "form",
          layout: "anchor",
          //margin: 10,
          defaults: {
            xtype: "textfield",
            anchor: "100%",
            labelWidth: 150,
            padding: 10
          },
          items: [
            {
              name: "admin_id",
              value: localStorage && localStorage.uid ? localStorage.uid : null,
              hidden: true,
              allowBlank: false
            },
            {
              name: "transfer_type",
              value: currentData.transfer_type,
              hidden: true,
              allowBlank: false
            },
            {
              readOnly: true,
              name: "transfer_id",
              fieldLabel: D.t("Approving Transfer Id: *"),
              emptyText: "Transfer Id cannot be blank",
              allowBlank: false,
              value: currentData.id
            },

            {
              name: "src_amount",
              xtype: "numberfield",
              minValue: 0,
              maxValue: currentData.max_src_amount,
              allowBlank: me.isShowing(currentData.transfer_type),
              decimalPrecision: 6,
              fieldLabel: D.t("Source Amount"),
              value: currentData.src_amount,
              hidden: me.isShowing(currentData.transfer_type),
              listeners: {
                afterrender(e, eOpts) {
                  __CONFIG__.setComponentEmptyText(e, eOpts);
                },
                change: (el, v) => {
                  __CONFIG__.calculateDstAmount(acceptWindow, currentData);
                }
              }
            },
            {
              name: "dst_amount",
              xtype: "numberfield",
              minValue: 0,
              decimalPrecision: 6,
              fieldLabel: D.t("Destination Amount"),
              value: currentData.dst_amount,
              allowBlank: me.isShowing(currentData.transfer_type),
              hidden: me.isShowing(currentData.transfer_type),
              listeners: {
                afterrender(e, eOpts) {
                  __CONFIG__.setComponentEmptyText(e, eOpts);
                },
                change: (el, v) => {
                  __CONFIG__.calculateSrcAmount(acceptWindow, currentData);
                }
              }
            },
            {
              name: "conversion_rate",
              fieldLabel: D.t("Conversion Rate"),
              value: currentData.conversion_rate,
              allowBlank: me.isShowing(currentData.transfer_type),
              hidden:
                me.isShowing(currentData.transfer_type) ||
                !currentData.is_exchange,
              xtype: "numberfield",
              minValue: 0,
              decimalPrecision: 6,
              submitEmptyText: false,
              listeners: {
                afterrender(e, eOpts) {
                  __CONFIG__.setComponentEmptyText(e, eOpts);
                },
                change: (el, v) => {
                  __CONFIG__.calculateDstAmount(acceptWindow, currentData);
                }
              }
            },
            {
              name: "markup_rate",

              fieldLabel: D.t("Markup Rate(0-100)"),
              value: currentData.markup_rate,
              hidden:
                me.isShowing(currentData.transfer_type) ||
                !currentData.is_exchange,
              allowBlank: me.isShowing(currentData.transfer_type),
              xtype: "numberfield",
              minValue: 0,
              maxValue: 100,
              submitEmptyText: false,
              listeners: {
                afterrender(e, eOpts) {
                  __CONFIG__.setComponentEmptyText(e, eOpts);
                },
                change: (el, v) => {
                  __CONFIG__.calculateDstAmount(acceptWindow, currentData);
                }
              }
            },
            {
              xtype: "textarea",
              name: "note",
              maxLength: 150,
              fieldLabel: D.t("Approve Note")
            },
            {
              name: "brn",
              maxLength: 200,
              fieldLabel: __CONFIG__.isCryptoCurrency(currentData.dst_currency)
                ? D.t("Hash Id")
                : D.t("Bank Reference Number"),
              value: currentData.last_brn
            },
            {
              fieldLabel: D.t("Amount"),
              name: "actual_approval_amount",
              type: "integer",
              filterable: true,
              editable: true,
              visible: true
            },
            {
              xtype: "fieldcontainer",
              anchor: "100%",
              layout: "hbox",
              align: "center",
              margin: { top: 15, bottom: 5 },
              items: [
                {
                  xtype: "button",
                  text: D.t("OK"),
                  formBind: true,
                  listeners: {
                    click: async function(e, v) {
                      var actionBtn = e;

                      if (actionBtn) {
                        actionBtn.disable();
                      }
                      var reqData = this.up("form").getValues() || {};
                      reqData.merchantId = localStorage.uid;

                      me.callActivityApprove(
                        reqData,
                        acceptWindow,
                        currentData
                      );
                    }
                  }
                },
                {
                  xtype: "button",
                  margin: { left: 5 },
                  //padding: 10,
                  text: D.t("Cancel"),
                  listeners: {
                    click: function() {
                      acceptWindow.close();
                    }
                  }
                }
              ]
            }
          ]
        }
      ]
    }).show();
  },
  async callActivityApprove(reqData, acceptWindow, currentData) {
    var me = this;
    await me.model.runOnServer(
      "callActivityApprove",
      { reqData, currentData },
      async function(acceptResp) {
        let defaultMsg = `Selected transfer`;
        if (currentData.ref_num != undefined)
          defaultMsg += ` with Reference number:${currentData.ref_num}`;
        if (
          acceptResp &&
          acceptResp.result &&
          acceptResp.result.status == "APPROVED"
        ) {
          defaultMsg += ` has been successfully marked as accepted in the system`;
          D.a("Transfer Accept Successful", acceptResp.message || defaultMsg);
          me.setAcceptRejectDisabled();
        } else {
          defaultMsg += ` has failed to be marked. Please try again or Contact Support`;
          if (
            acceptResp &&
            acceptResp.result &&
            acceptResp.result.error &&
            acceptResp.result.error.message
          ) {
            acceptResp.message = acceptResp.result.error.message;
          }
          D.a("Transfer Accept Failed", acceptResp.message || defaultMsg);
        }
        acceptWindow.close();
      }
    );
  },
  async reject(store) {
    var me = this;
    let currentData = me.data;
    var rejectWindow = Ext.create("Ext.window.Window", {
      title: "Transfer Rejection:",
      layout: "fit",
      width: "80vh",
      //margin: 30,
      modal: true,
      items: [
        {
          xtype: "form",
          layout: "anchor",
          //margin: 10,
          defaults: {
            xtype: "textfield",
            anchor: "100%",
            labelWidth: 150,
            padding: 10
          },
          items: [
            {
              name: "admin_id",
              value: localStorage && localStorage.uid ? localStorage.uid : null,
              hidden: true,
              allowBlank: false
            },
            {
              readOnly: true,
              name: "transfer_id",
              fieldLabel: D.t("Rejecting Transfer Id: *"),
              emptyText: "Transfer Id cannot be blank",
              allowBlank: false,
              value: currentData.id
            },
            {
              xtype: "textarea",
              name: "note",
              maxLength: 150,
              fieldLabel: D.t("Reject Note")
            },
            {
              xtype: "fieldcontainer",
              anchor: "100%",
              layout: "hbox",
              align: "center",
              margin: { top: 15, bottom: 5 },
              items: [
                {
                  xtype: "button",
                  text: D.t("OK"),
                  formBind: true,
                  listeners: {
                    click: async function(e, v) {
                      var actionBtn = e;
                      if (actionBtn) {
                        actionBtn.disable();
                      }
                      var reqData = this.up("form").getValues() || {};
                      reqData.merchantId = localStorage.uid;
                      me.model.runOnServer(
                        "callActivityReject",
                        reqData,
                        function(rejectResp) {
                          let defaultMsg = `Selected transfer`;
                          if (currentData.ref_num != undefined)
                            defaultMsg += ` with Reference number:${currentData.ref_num}`;
                          if (
                            rejectResp &&
                            rejectResp.result &&
                            rejectResp.result.status == "REJECTED"
                          ) {
                            defaultMsg += ` has been successfully marked as rejected in the system`;
                            if (
                              rejectResp &&
                              rejectResp.error &&
                              rejectResp.error.message
                            ) {
                              rejectResp.message = rejectResp.error.message;
                            }
                            D.a(
                              "Transfer Reject Successful",
                              rejectResp.message || defaultMsg
                            );
                            me.setAcceptRejectDisabled();
                          } else {
                            defaultMsg += ` has failed to be marked. Please try again or Contact Support`;
                            D.a(
                              "Transfer Reject Failed",
                              rejectResp.message || defaultMsg
                            );
                          }
                          rejectWindow.close();
                        }
                      );
                    }
                  }
                },
                {
                  xtype: "button",
                  margin: { left: 5 },
                  //padding: 10,
                  text: D.t("Cancel"),
                  listeners: {
                    click: function() {
                      rejectWindow.close();
                    }
                  }
                }
              ]
            }
          ]
        }
      ]
    }).show();
  },

  async changeSourceAccount(store) {
    var me = this;
    let currentData = me.data;
    var changeAccountWindow = Ext.create("Ext.window.Window", {
      title: "Change bank account:",
      layout: "fit",
      width: "80vh",
      //margin: 30,
      modal: true,
      items: [
        {
          xtype: "form",
          layout: "anchor",
          //margin: 10,
          defaults: {
            xtype: "textfield",
            anchor: "100%",
            labelWidth: 150,
            padding: 10
          },
          items: [
            {
              name: "admin_id",
              value: localStorage && localStorage.uid ? localStorage.uid : null,
              hidden: true,
              allowBlank: false
            },
            {
              readOnly: true,
              name: "dst_currency",
              hidden: true,
              allowBlank: false,
              value: currentData.dst_currency
            },
            {
              readOnly: true,
              name: "merchant_account_id",
              hidden: true,
              allowBlank: false,
              value: currentData.merchant_account_id
            },
            {
              readOnly: true,
              name: "transfer_id",
              hidden: true,
              fieldLabel: D.t("Transfer Id: *"),
              emptyText: "Transfer Id cannot be blank",
              allowBlank: false,
              value: currentData.id
            },
            {
              flex: 1,
              allowBlank: false,
              xtype: "combo",
              name: "bank_id",
              fieldLabel: D.t("Select Bank"),
              store: Ext.create("Core.data.ComboStore", {
                dataModel: Ext.create("Crm.modules.banks.model.BanksModel"),
                exProxyParams: {
                  filters: [
                    {
                      _property: "bank_type",
                      _value: 0,
                      _operator: "eq"
                    }
                  ]
                },
                fieldSet: ["id", "name"],
                scope: this
              }),
              valueField: "id",
              displayField: "name",
              listeners: {
                change: (el, v) => {
                  changeAccountWindow
                    .down("[name=bank_account_id]")
                    .clearValue();
                  const proxyFilters = {
                    filters: [
                      { _property: "bank_id", _value: v, _operator: "eq" }
                    ]
                  };
                  if (changeAccountWindow.down("[name=dst_currency]").value) {
                    proxyFilters.filters.push({
                      _property: "currency",
                      _value: changeAccountWindow.down("[name=dst_currency]")
                        .value,
                      _operator: "eq"
                    });
                  }
                  const store = Ext.create("Core.data.ComboStore", {
                    dataModel: Ext.create(
                      "Crm.modules.accountsPool.model.AccountsPoolModel"
                    ),
                    fieldSet: ["id", "account_name", "acc_no", "currency"],
                    exProxyParams: proxyFilters,
                    scope: this
                  });
                  changeAccountWindow
                    .down("[name=bank_account_id]")
                    .setStore(store);
                }
              }
            },
            {
              flex: 3,
              xtype: "combo",
              labelWidth: 150,
              name: "bank_account_id",
              fieldLabel: D.t("Select Bank Account"),
              valueField: "id",
              allowBlank: false,
              displayField: "account_name",
              tpl:
                '<tpl for="."><div class="x-boundlist-item" style="height:40px;"><b>{account_name}</b>&nbsp;&nbsp;(A/C: {acc_no})&nbsp;(Status: {status})</div><div style="clear:both"></div></tpl>',
              listeners: {
                change: (e, newVal, oldVal) => {
                  const me = this;
                  if (typeof newVal == "string" && newVal.length) {
                    console.log("new value: ", newVal);
                  }
                }
              }
            },
            {
              xtype: "fieldcontainer",
              anchor: "100%",
              layout: "hbox",
              align: "center",
              margin: { top: 15, bottom: 5 },
              items: [
                {
                  xtype: "button",
                  text: D.t("OK"),
                  formBind: true,
                  listeners: {
                    click: async function(e, v) {
                      var actionBtn = e;
                      if (actionBtn) {
                        actionBtn.disable();
                      }
                      var reqData = this.up("form").getValues() || {};
                      reqData.merchantId = localStorage.uid;
                      me.model.runOnServer(
                        "changePayoutSourceBankAccount",
                        reqData,
                        function(res) {
                          console.log("response: ", res);
                          if (res.error) {
                            let defaultMsg = `Could not update bank account`;
                            D.a(
                              res.error.title || "Something Went Wrong",
                              res.error.message || defaultMsg
                            );
                          } else if (res.result.success) {
                            changeAccountWindow.close();
                            let defaultMsg = `Source Bank Account for payout was updated successfully`;
                            D.a(
                              res.result.body.title ||
                                "Account updated successfully",
                              res.result.body.message || defaultMsg
                            );
                            me.loadData();
                            me.view.templates.default.build();
                          }
                        }
                      );
                    }
                  }
                },
                {
                  xtype: "button",
                  margin: { left: 5 },
                  //padding: 10,
                  text: D.t("Cancel"),
                  listeners: {
                    click: function() {
                      rejectWindow.close();
                    }
                  }
                }
              ]
            }
          ]
        }
      ]
    }).show();
  },

  setAcceptRejectDisabled() {
    this.view.down("[action=accept]").setDisabled(true);
    this.view.down("[action=reject]").setDisabled(true);
    if (
      this.view &&
      this.view.templates &&
      this.view.templates.default &&
      typeof this.view.templates.default.build == "function"
    ) {
      this.loadData();
      this.view.templates.default.build();
    }
  },

  async setValues(data) {
    if (window.__CB_REC__) {
      Ext.apply(data, __CB_REC__);
      window.__CB_REC__ = null;
      this.view.s = true;
    }
    this.view.currentData = data;
    var form = this.view.down("form");
    this.view.fireEvent("beforesetvalues", form, data);
    form.getForm().setValues(data);
    this.view.fireEvent("setvalues", form, data);
    form.getForm().isValid();

    this.data = data;
    this.view.down("[name=id]").setValue(data.id);
    this.view.applyEventTpl(data.event_name);
    this.view.down("[action=datapanel]").setData(data);
  }
});
