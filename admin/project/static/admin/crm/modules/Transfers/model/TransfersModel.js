Ext.define("Crm.modules.Transfers.model.TransfersModel", {
  extend: "Crm.classes.DataModel",

  collection: "vw_tx_transfer",
  idField: "id",
  foreignKeyFilter: [
    {
      collection: "accounts",
      alias: "a",
      on: " accounts.id = vw_tx_transfer.account_id ",
      type: "inner"
    }
  ],

  strongRequest: false,

  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "transfer_id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "src_currency",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "status",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_amount",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },

    {
      name: "dst_currency",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_amount",
      type: "float",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "transfer_type",
      type: "enumstring",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ref_num",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "order_num",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "counterpart_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true,
      hidden: true
    },
    {
      name: "is_exchange",
      type: "boolean",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "exchange_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "data",
      type: "jsonb",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "account_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "merchant_account_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "order_num",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "counter_party_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "counter_party_bank_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "counter_party_bank_code",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_macc_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_acc_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_acc_registration_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_acc_status",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_acc_country",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_acc_type",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_macc_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_macc_status",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_macc_category",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "last_brn",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "event_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "event_name",
      type: "description",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true,
      sort: -1
    },
    {
      name: "removed",
      type: "int",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "signobject",
      type: "json",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_wallet_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_wallet_label",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_wallet_acc_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_wallet_currency",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_wallet_ref_num",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_wallet_type",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_wallet_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_wallet_label",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_wallet_acc_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_wallet_currency",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_wallet_ref_num",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_wallet_type",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_wallet_type",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_bank_short_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_bank_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_bank_address",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_bank_city",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_bank_country",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_bank_short_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_bank_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_bank_address",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_bank_city",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_bank_country",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "activity",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "action_type",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "activity_action",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "admin_id",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "settlement_id",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "beneficiary_vpa",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "beneficiary_ifsc",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "beneficiary_acc_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "beneficiary_first_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "beneficiary_last_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_bank_acc_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_bank_acc_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  /* scope:server */
  async $syncTransferStatus(data, cb) {
    try {
      var me = this;
      if (data && typeof data == "object") {
      }
      const params = {
        service: "merchant-service",
        method: "syncAutoPayout",
        data: data,
        options: {
          realmId: me.user.profile.realm_id
        }
      };
      console.log(`Func:$syncTransferStatus. Send Request Data:`);
      console.log(data);
      var res = await me.callApi(params);
      console.log(
        `File:AccountsModel.js Func:$syncTransferStatus.Received Response`
      );
      me.changeModelData(
        "Crm.modules.Transactions.model.TransactionsModel",
        "ins",
        {}
      );
      me.changeModelData(
        "Crm.modules.Transfers.model.TransfersModel",
        "ins",
        {}
      );
      return cb(res);
    } catch (err) {
      console.error(
        `AccountsModel.js Func:syncTransferStatus, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      return cb(err);
    }
  },

  /* scope:server */
  async $callActivityApprove(data, cb) {
    try {
      var me = this;
      if (data && typeof data == "object") {
        data.reqData.activity_action = "APPROVED";
        data.reqData.action_type = "ADMIN";
      }
      if (data.reqData.transfer_type === "SETTLEMENT") {
        let src_amount =
          data.currentData.src_amount_settlement - data.currentData.src_amount;
        src_amount += parseFloat(data.reqData.src_amount);

        let dst_amount =
          data.currentData.dst_amount_settlement - data.currentData.dst_amount;
        dst_amount += parseFloat(data.reqData.dst_amount);

        const updateRes = await me.updateSettlementDetails(
          {
            reqData: data.reqData,
            src_amount,
            dst_amount,
            oldVal: data.currentData
          },
          cb
        );
        if (!updateRes.result) {
          return cb(updateRes);
        }
      }
      const params = {
        service: "transaction-service",
        method: "txActivityApprove",
        data: data.reqData,
        options: {
          realmId: me.user.profile.realm_id
        },
        writeFlag: true
      };
      console.log(`Func:$callActivityApprove. Send Request Data:`);
      console.log(data);
      var res = await me.callApi(params);
      console.log(
        `File:AccountsModel.js Func:$callActivityApprove.Received Response`
      );
      me.changeModelData(
        "Crm.modules.Transactions.model.TransactionsModel",
        "ins",
        {}
      );
      me.changeModelData(
        "Crm.modules.Transfers.model.TransfersModel",
        "ins",
        {}
      );
      me.changeModelData(
        "Crm.modules.settlements.model.SettlementsModel",
        "ins",
        {}
      );
      if (
        data.reqData.transfer_type === "SETTLEMENT" &&
        res &&
        res.result &&
        res.result.error &&
        res.result.error.message
      ) {
        await me.updateSettlementDetails(
          {
            reqData: data.currentData,
            src_amount: data.currentData.src_amount,
            dst_amount: data.currentData.dst_amount,
            oldVal: data.currentData
          },
          cb
        );
      }
      return cb(res);
    } catch (err) {
      console.error(
        `AccountsModel.js Func:callActivityApprove, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      return cb(err);
    }
  },

  /* scope:server */
  async afterGetData(data, cb) {
    var me = this;
    //Only set admin_name for from
    if (data && data.length == 1 && data[0].admin_id != undefined) {
      me.src.db.query(
        `SELECT _id, name, email from admin_users where _id=$1;`,
        [data[0].admin_id],
        function(err, resp) {
          if (err) {
            console.error(
              "TransfersModel.js. Func:getData. Database Error. Error",
              err
            );
            return cb(data);
          } else if (resp && resp.length) {
            data[0].admin_name = resp[0].name;
          }
          return cb(data);
        }
      );
    } else {
      return cb(data);
    }
  },

  /* scope:server */
  async $callActivityReject(data, cb) {
    try {
      var me = this;
      if (data && typeof data == "object") {
        data.activity_action = "REJECTED";
        data.action_type = "ADMIN";
      }
      const params = {
        service: "transaction-service",
        method: "txActivityReject",
        data: data,
        options: {
          realmId: me.user.profile.realm_id
        },
        writeFlag: true
      };
      console.log(`Func:$callActivityReject. Send Request Data:`);
      console.log(data);
      var res = await me.callApi(params);
      console.log(
        `File:AccountsModel.js Func:$callActivityReject.Received Response`
      );
      me.changeModelData(
        "Crm.modules.Transactions.model.TransactionsModel",
        "ins",
        {}
      );
      me.changeModelData(
        "Crm.modules.Transfers.model.TransfersModel",
        "ins",
        {}
      );
      return cb(res);
    } catch (err) {
      console.error(
        `AccountsModel.js Func:callActivityApprove, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      return cb(err);
    }
  },
  /* scope:server */
  async $fetchSettlementDetails(data, cb) {
    try {
      var me = this;
      if (data && data.id) {
        const res = await this.src.db.query(
          `select * from settlements where removed=0 and id = $1`,
          [data.id]
        );
        if (res && res.length && data.wallet_id) {
          const balance = await this.src.db.query(
            `select balance as max_src_amount from wallets where removed=0 and id = $1`,
            [data.wallet_id]
          );
          if (balance && balance.length)
            res[0].max_src_amount = balance[0].max_src_amount;
          return cb(res[0]);
        }
        return cb([]);
      }
      return cb([]);
    } catch (err) {
      console.error(
        `AccountsModel.js Func:fetchSettlementDetails, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      return cb(err);
    }
  },

  /* scope:server */
  async $fetchPayoutDetailId(data, callbackFunction) {
    try {
      var me = this;
      const Util = Ext.create("Crm.Util", { scope: me });
      if (!data.merchant_account_id) {
        return callbackFunction([]);
      } else if (
        data &&
        typeof data.merchant_account_id == "string" &&
        data.merchant_account_id.length == 36
      ) {
        const merchantAccounts = await this.src.db.query(
          `select ma.*,c.name as settlement_currency_name from merchant_accounts ma inner join currency c on c.abbr = ma.settlement_currency where ma.removed=0 and ma.id = $1`,
          [data.merchant_account_id]
        );
        if (merchantAccounts && merchantAccounts.length) {
          const wallets = await this.callApi({
            service: "account-service",
            method: "fetchMerchantAccountWalletBalance",
            data: {
              src_currency: merchantAccounts[0].settlement_currency,
              merchant_account_id: data.merchant_account_id,
              w_type: "WALLET"
            },
            options: {
              realmId: me.user.profile.realm_id
            }
          });
          if (
            wallets &&
            wallets.result &&
            wallets.result.total_available_balance
          ) {
            merchantAccounts[0].total_available_balance =
              wallets.result.total_available_balance;
          } else {
            merchantAccounts[0].total_available_balance = 0;
          }
        }

        if (merchantAccounts && merchantAccounts.length) {
          return callbackFunction(merchantAccounts);
        } else {
          return callbackFunction([]);
        }
      } else {
        return callbackFunction([]);
      }
    } catch (err) {
      console.error(
        `Func:fetchSettlementCurrencyId, File: SettlementsModel.js, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      callbackFunction(err);
    }
  },

  async updateSettlementDetails(data, cb) {
    try {
      var me = this;
      console.log(
        `File:TransfersModel.js Func:$updateSettlementDetails. Making API call to updateSettlementDetails`,
        data
      );
      const params = {
        src_amount: data.src_amount,
        dst_amount: data.dst_amount,
        markup_rate: data.reqData.markup_rate,
        conversion_rate: data.reqData.conversion_rate,
        settlement_id: data.oldVal.settlement_id,
        tr_src_amount: data.reqData.src_amount,
        tr_dst_amount: data.reqData.dst_amount,
        id: data.oldVal.id
      };
      if (data.oldVal.exchange_id) params.exchange_id = data.oldVal.exchange_id;
      var res = await me.callApi({
        service: "account-service",
        method: "updateSettlementDetails",
        data: params,
        options: {
          realmId: me.user.profile.realm_id
        }
      });
      console.log(`File:TransfersModel.js Func:$params.Received Response`);

      if (
        res &&
        res.result &&
        res.result.message == "Settlement details update successfully"
      ) {
        this.changeModelData(
          "Crm.modules.Transfers.model.TransfersModel",
          "ins",
          {}
        );
        return { result: true, message: res.result.message };
      } else {
        return { result: false, message: res.error.title };
      }
    } catch (err) {
      console.error(
        `AccountsModel.js Func:fetchSettlementDetails, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      return err;
    }
  },

  /* scope:server */
  async $exportPayoutCSV(data, cb) {
    const me = this;
    try {
      const params = {
        service: "account-service",
        method: "exportPayoutCSV",
        data: data,
        options: {
          realmId: me.user.profile.realm_id
        }
      };
      var res = await me.callApi(params);
      cb(res);
    } catch (error) {
      cb({ success: false, message: error.title });
    }
  },

  /* scope:server */
  async $fetchPayoutBankAmountByMerchantId(data, cb) {
    try {
      var me = this;
      console.log(
        `File:TransfersModel.js Func:$fetchPayoutBankAmountByMerchantId`,
        data
      );
      let realmConditionSql = " ";
      realmConditionSql = await this.buildRealmConditionQuery(
        realmConditionSql,
        "b.realm_id"
      );
      const res = await this.src.db.query(
        `with payout_info as (
          SELECT coalesce(sum(w.balance),0) as max_amount_for_payout, w.bank_account_id, ma.merchant_account_id, ba.currency as bank_account_currency, ba.acc_no, ba.account_name as bank_account_name, b.name as bank_name, w.currency FROM  wallets w 
                join merchant_account_mappings ma on ma.id = w.merchant_account_mapping_id and ma.merchant_account_id = $1
                join bank_accounts ba on ba.id = w.bank_account_id
                join banks b on b.id = ba.bank_id ${realmConditionSql}
                where w.removed = 0 and w.type = 'WALLET' and w.currency= $2 and w.balance > 0 
                GROUP BY w.bank_account_id,ba.currency , ma.merchant_account_id,ba.acc_no,b.name,w.currency, ba.account_name 
          )
          select pai.*, (
          select json_agg(json_build_object('id', pp.id,'protocol_type', pp.protocol_type)) as payment_protocols
          from bank_accounts_protocols bap
          left join payment_protocols pp on (bap.payment_protocol_id=pp.id and pp.removed=0)
          where bap.bank_account_id =pai.bank_account_id and bap.removed=0
          )
          from payout_info pai; `,
        [data.merchant_account_id, data.currency]
      );
      return cb(res);
    } catch (err) {
      console.error(
        `SettlementModel.js Func:fetchSettlementBankAmountByMerchantId, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      return cb(err);
    }
  },
  /* scope:server */
  async $createNewPayout(data, cb) {
    try {
      var me = this;
      console.log(
        `File:TransfersModel.js Func:$createNewPayout. Making API call to createNewPayout`,
        data
      );
      var res = await this.callApi({
        service: "merchant-service",
        method: "createPayoutRequest",
        data: {
          basic_info: data.basic_info,
          is_settlement_payout_trigger: true,
          action_type: "ADMIN",
          admin_id: this.user.id || ""
        },

        options: {
          realmId: me.user.profile.realm_id,
          maId: data.basic_info.merchant_account_id,
          accountId: data.basic_info.account
        }
      });

      if (res && typeof res == "object") {
        console.log(
          "File:TransfersModel.js Func:$createNewPayout.Received Response",
          JSON.stringify(res)
        );
      }
      if (
        res &&
        res.result &&
        res.result.message == "Settlement payout request created successfully"
      ) {
        res.actionStatus = true;
        this.changeModelData(
          "Crm.modules.Transfers.model.TransfersModel",
          "ins",
          {}
        );
      } else {
        res.actionStatus = false;
      }

      return cb(res);
    } catch (err) {
      console.error(
        `TransfersModel.js Func:createNewPayout, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      return cb(err);
    }
  },
  async $getQualifiedPayouts(data, cb) {
    const me = this;
    try {
      const params = {
        service: "account-service",
        method: "getQualifiedPayouts",
        data: data,
        options: {
          realmId: me.user.profile.realm_id
        }
      };
      var res = await me.callApi(params);
      cb(res);
    } catch (error) {
      cb({ success: false, message: error.title });
    }
  },

  /* scope:server */
  async $checkSufficientBalance(data, cb) {
    const me = this;
    try {
      let Data = {
        trigger: data.event_name,
        variables: [],
        data: {
          action: "APPROVE_TX",
          amount: data.src_amount
        }
      };
      let service = {
        realmId: me.user.profile.realm_id,
        accountId: data.account_id,
        maId: data.merchant_account_id
      };
      const params = {
        service: "tariff-service",
        method: "calculateFee",
        data: Data,
        options: service
      };
      var fee = await me.callApi(params);

      let amount = 0;
      if (
        fee &&
        fee.result &&
        fee.result.transactions &&
        fee.result.transactions.length
      ) {
        for (let i = 0; i < fee.result.transactions.length; i++) {
          if (
            "FEE" === fee.result.transactions[i].txsubtype &&
            !isNaN(fee.result.transactions[i].amount)
          ) {
            amount += fee.result.transactions[i].amount;
          }
        }
      }
      const res = await this.src.db.query(
        `select balance from wallets where id = $1`,
        [data.src_wallet_id]
      );
      console.log(res);
      if (res && res.length) {
        let totalBalance = amount + parseFloat(data.src_amount);
        if (totalBalance <= res[0].balance) {
          return cb(true);
        }
      }

      return cb(false);
    } catch (error) {
      cb(false);
    }
  },
  /* scope:server */
  async $createDeposit(data, cb) {
    const me = this;
    try {
      const params = {
        service: "merchant-service",
        method: "createDeposit",
        data: data,
        options: {
          realmId: me.user.profile.realm_id
        }
      };
      var res = await me.callApi(params);
      cb(res);
    } catch (error) {
      cb({ success: false, message: error.title });
    }
  },
  /* scope:server */
  async $checkExchangeBankAccountForCurrency(data, cb) {
    try {
      var me = this;
      console.log(
        `File:SettlementModel.js Func:$checkExchangeBankAccountForCurrency.`,
        data
      );
      if (data.currency) {
        let parameter = [data.currency];
        let query = `SELECT ba.id FROM bank_accounts ba join banks b ON b.id = ba.bank_id AND b.removed = 0 AND bank_type = 1 where ba.removed = 0  and ba.currency= $1`;
        if (data.bank_id) {
          parameter.push(data.bank_id);
          query += ` and ba.bank_id=$2`;
        }
        query = await this.buildRealmConditionQuery(query, "b.realm_id");
        const res = await this.src.db.query(query, parameter);
        me.changeModelData(
          "Crm.modules.Transfers.model.TransfersModel",
          "ins",
          {}
        );
        return cb(res);
      }
      return cb([]);
    } catch (err) {
      console.error(
        `SettlementModel.js Func:checkExchangeBankAccountForCurrency, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      return cb(err);
    }
  },
  /* scope:server */
  async $changePayoutSourceBankAccount(data, cb) {
    try {
      var me = this;
      if (data && typeof data == "object") {
        data.activity_action = "REJECTED";
        data.action_type = "ADMIN";
      }
      const params = {
        service: "transaction-service",
        method: "changePayoutSourceBankAccount",
        data: data,
        options: null,
        writeFlag: true
      };
      console.log(`Func:$changePayoutSourceBankAccount. Send Request Data:`);
      console.log(data);
      var res = await me.callApi(params);
      console.log(
        `File:AccountsModel.js Func:$changePayoutSourceBankAccount.Received Response`
      );
      me.changeModelData(
        "Crm.modules.Transactions.model.TransactionsModel",
        "ins",
        {}
      );
      me.changeModelData(
        "Crm.modules.Transfers.model.TransfersModel",
        "ins",
        {}
      );
      return cb(res);
    } catch (err) {
      console.error(
        `AccountsModel.js Func:callActivityApprove, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      return cb(err);
    }
  }
});
