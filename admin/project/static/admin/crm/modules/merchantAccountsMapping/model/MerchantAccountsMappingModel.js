Ext.define(
  "Crm.modules.merchantAccountsMapping.model.MerchantAccountsMappingModel",
  {
    extend: "Core.data.DataModel",
    foreignKeyFilter: [
      {
        collection: "merchant_accounts",
        alias: "ma",
        on:
          " merchant_accounts.id = merchant_account_mappings.merchant_account_id ",
        type: "inner"
      },
      {
        collection: "accounts",
        alias: "a",
        on: " accounts.id = merchant_accounts.account_id ",
        type: "inner"
      }
    ],
    collection: "merchant_account_mappings",
    idField: "id",

    fields: [
      {
        name: "id",
        type: "ObjectID",
        visible: true
      },
      {
        name: "merchant_account_id",
        type: "string",
        visible: true,
        editable: true
      },
      {
        name: "bank_account",
        type: "string",
        filterable: true,
        editable: true,
        visible: true
      },
      {
        name: "start_time",
        type: "date",
        filterable: true,
        editable: true,
        visible: true
      },
      {
        name: "end_time",
        type: "date",
        filterable: true,
        editable: true,
        visible: true
      },
      {
        name: "removed",
        type: "number",
        filterable: true,
        editable: true,
        visible: true
      },
      {
        name: "ctime",
        type: "date",
        filterable: true,
        editable: true,
        visible: true
      },
      {
        name: "mtime",
        type: "date",
        sort: -1,
        filterable: true,
        editable: true,
        visible: true
      }
    ],
    async afterGetData(data, cb) {
      let d1 = null;
      let d2 = null;
      if (data.length) {
        let merchantAccounts = new Set();
        let bankAccounts = new Set();
        let i = 0;

        while (data.length > i) {
          merchantAccounts.add(data[i].merchant_account_id);
          bankAccounts.add(data[i].bank_account);
          i++;
        }
        if (merchantAccounts.size) {
          await this.getCategoryByIDUsingINClause(
            Array.from(merchantAccounts),
            function(res) {
              if (res) {
                d1 = res;
              }
            }
          );
        }
        if (bankAccounts.size) {
          await this.getAccountsByIDUsingINClause(
            Array.from(bankAccounts),
            function(res) {
              if (res) {
                d2 = res;
              }
            }
          );
        }

        const tempdata = data.map((e) => {
          e.merchant_account_name =
            (d1 &&
              d1.length &&
              d1[
                d1.findIndex(
                  (o) => o.merchant_account_id === e.merchant_account_id
                )
              ].merchant_account_name) ||
            "";
          e.bank_account_name =
            (d2 &&
              d2.length &&
              d2[d2.findIndex((o) => o.bank_account === e.bank_account)]
                .bank_account_name) ||
            "";
          return e;
        });
        cb(tempdata);
      } else {
        cb(data);
      }
    },
    getCategoryByIDUsingINClause: async function(params, cb) {
      try {
        let sql = `SELECT id as merchant_account_id , name as merchant_account_name  FROM merchant_accounts WHERE id IN(${params.map(
          (id) => `'${id}'`
        )})`;
        sql = await this.buildRealmConditionQuery(sql);
        const resp = await this.src.db.query(sql);
        if (resp.length) {
          return cb(resp);
        }
        return cb();
      } catch (error) {
        console.error(
          "MerchantAccountsMappingModel.js. Func:getCategoryByIDUsingINClause. Database Error. Error",
          error
        );
        return cb({ message: error });
      }
    },
    getAccountsByIDUsingINClause: async function(params, cb) {
      try {
        let sql = `SELECT id as bank_account , account_name as bank_account_name  FROM bank_accounts WHERE id IN(${params.map(
          (id) => `'${id}'`
        )})`;
        sql = await this.buildRealmConditionQuery(sql);
        const resp = await this.src.db.query(sql);

        if (resp.length) {
          return cb(resp);
        }
        return cb();
      } catch (error) {
        console.error(
          "MerchantAccountsMappingModel.js. Func:getAccountsByIDUsingINClause. Database Error. Error",
          error
        );
        return cb({ message: error });
      }
    }
  }
);
