Ext.define("Crm.modules.merchantAccountsMapping.model.MerchantAccountCombo", {
  extend:
    "Crm.modules.merchantAccountsMapping.model.MerchantAccountsMappingModel",

 
  getData: async function(params, cb) {
    let sql = "SELECT ma.name, ma.id FROM merchant_accounts ma join accounts a on a.id = ma.account_id where ma.removed=0 ";
    if (
      params &&
      params._filters &&
      params._filters.length &&
      params._filters[0]._value
    ) {
      sql += ` AND name ILIKE '${params._filters[0]._value}%'`;
    }
    sql = await this.buildRealmConditionQuery(sql,"a.realm_id");
    const res = await this.src.db.query(sql, function(err, resp) {
      if (err) {
        console.error(
          "MerchantAccountCombo.js. Func:getData. Database Error. Error",
          err
        );
        return cb({ total: 0, list: [] });
      }
      if (resp) {
        return cb({ total: resp.length, list: resp.rows });
      }
      return cb({ total: 0, list: [] });
    });
  }
});
