Ext.define(
  "Crm.modules.merchantAccountsMapping.view.MerchantAccountsMappingGrid",
  {
    extend: "Core.grid.GridContainer",

    title: D.t("Merchant Accounts"),
    iconCls: "x-fa fa-credit-card",
    filterbar: true,

    buildColumns: function() {
      return [
        {
          text: D.t("Merchant Account"),
          flex: 1,
          sortable: true,
          dataIndex: "merchant_account_id",
          renderer: function(v, m, r) {
            if (r.data.merchant_account_name)
              return r.data.merchant_account_name;
            return "-";
          },
          filter: {
            xtype: "combo",
            valueField: "id",
            displayField: "name",
            minChars: 1,
            store: Ext.create("Core.data.ComboStore", {
              storeId: "id",
              dataModel: Ext.create(
                "Crm.modules.merchantAccountsMapping.model.MerchantAccountCombo"
              ),
              fieldSet: ["name", "id"],
              scope: this,
            }),
          },
        },
        {
          text: D.t("Bank Account"),
          flex: 1,
          sortable: true,
          dataIndex: "bank_account",
          renderer: function(v, m, r) {
            if (r.data.bank_account_name) return r.data.bank_account_name;
            return "-";
          },
          filter: {
            xtype: "combo",
            valueField: "id",
            displayField: "account_name",
            minChars: 1,
            store: Ext.create("Core.data.ComboStore", {
              storeId: "id",
              dataModel: Ext.create(
                "Crm.modules.bankAccounts.model.BankAccountsModel"
              ),
              fieldSet: ["account_name", "id"],
              scope: this,
            }),
          },
        },
        {
          text: D.t("Start Date"),
          flex: 1,
          sortable: true,
          xtype: "datecolumn",
          format: "d.m.Y H:i:s",
          dataIndex: "start_time",
          filter: {
            xtype: "datefield",
            format: "d.m.Y",
          },
        },
        {
          text: D.t("End Date"),
          flex: 1,
          sortable: true,
          xtype: "datecolumn",
          format: "d.m.Y H:i:s",
          dataIndex: "end_time",
          filter: {
            xtype: "datefield",
            format: "d.m.Y",
          },
        },
      ];
    },
  }
);
