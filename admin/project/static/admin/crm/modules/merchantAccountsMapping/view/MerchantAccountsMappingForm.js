Ext.define(
  "Crm.modules.merchantAccountsMapping.view.MerchantAccountsMappingForm",
  {
    extend: "Core.form.FormWindow",
    requires: ["Ext.form.HtmlEditor"],
    titleTpl: D.t("Merchant Accounts"),
    iconCls: "x-fa fa-credit-card",
    width: 500,
    height: 300,
    syncSize: function() {},
    controllerCls:
      "Crm.modules.merchantAccountsMapping.view.MerchantAccountsMappingFormController",

    buildItems: function() {
      return [
        {
          name: "id",
          hidden: true,
        },
        this.buildMerchantAccountCombo(),
        this.buildBankAccountCombo(),
        {
          xtype: "xdatefield",
          format: "d/m/y",
          anchor: "100%",
          submitFormat: "Y.m.d",
          allowBlank: false,
          editable: false,
          name: "start_time",
          fieldLabel: D.t("Start Date"),
        },
        {
          xtype: "xdatefield",
          format: "d/m/y",
          anchor: "100%",
          submitFormat: "Y.m.d",
          editable: false,
          name: "end_time",
          fieldLabel: D.t("End Date"),
        },
      ];
    },
    buildMerchantAccountCombo() {
      return {
        name: "merchant_account_id",
        xtype: "combo",
        fieldLabel: D.t("Merchant Account"),
        queryMode: "all",
        displayField: "name",
        editable: true,
        valueField: "id",
        validator: true,
        allowBlank: false,
        minChars: 1,
        store: Ext.create("Core.data.ComboStore", {
          storeId: "account_id",
          dataModel: Ext.create(
            "Crm.modules.merchantAccountsMapping.model.MerchantAccountCombo"
          ),
          fieldSet: ["name", "id"],
          scope: this,
        }),
      };
    },
    buildBankAccountCombo() {
      return {
        name: "bank_account",
        xtype: "combo",
        fieldLabel: D.t("Bank Account"),
        queryMode: "all",
        displayField: "account_name",
        editable: true,
        valueField: "id",
        validator: true,
        allowBlank: false,
        minChars: 1,
        store: Ext.create("Core.data.ComboStore", {
          storeId: "id",
          dataModel: Ext.create(
            "Crm.modules.bankAccounts.model.BankAccountsModel"
          ),
          fieldSet: ["account_name", "id"],
          scope: this,
        }),
      };
    },
  }
);
