Ext.define("Crm.modules.currency.model.CurrencyModel", {
  extend: "Core.data.DataModel",

  collection: "currency",
  idField: "id",

  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "code",
      type: "int",
      type: "string",
      visible: true,
      editable: true,
    },
    {
      name: "code",
      type: "integer",
      filterable: true,
      editable: true,
      visible: true,
    },
    {
      name: "abbr",
      sort: 1, 
      type: "string",
      filterable: true,
      editable: true,
      visible: true,
    },
    {
      name: "name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true,
    },
    {
      name: "fraction_points",
      type: "number",
      filterable: true,
      editable: true,
      visible: true,
    },
    {
      name: "crypto",
      type: "boolean",
      filterable: true,
      editable: true,
      visible: true,
    },
  ],
  /* scope:server */
  async $checkUniqueCode(data, cb) {
    if (data.id) {
      const duplicateCheck = await this.src.db.query(
        `select id from currency where removed=0 and code = $1 and id != $2`,
        [Number(data.code), data.id]
      );
      if (duplicateCheck[0]) {
        return cb({
          result: false,
          message: "Code parameter need to be unique.",
        });
      } else {
        return cb({ result: true });
      }
    } else {
      return cb({ result: true });
    }
  },
  async $checkUniqueABBR(data, cb) {
    if (data.id) {
      const duplicateCheck = await this.src.db.query(
        `select id from currency where removed=0 and abbr ilike $1 and id != $2`,
        [data.abbr, data.id]
      );
      if (duplicateCheck[0]) {
        return cb({
          result: false,
          message: "Name parameter need to be unique.",
        });
      } else {
        return cb({ result: true });
      }
    } else {
      return cb({ result: true });
    }
  },
  async beforeSave(data, cb) {
    data.abbr = data.abbr.trim();
    data.name = data.name.trim();
    cb(data);
  }
});
