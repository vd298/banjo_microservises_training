Ext.define("Crm.modules.currency.view.CurrencyGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Currency"),
  iconCls: "x-fa fa-usd",
  filterable: true,
  filterbar: true,

  buildColumns: function() {
    return [
      {
        text: D.t("Code"),
        flex: 1,
        filter: true,
        sortable: true,
        dataIndex: "code",
      },
      {
        text: D.t("Abbreviation ISO 3"),
        flex: 1,
        filter: true,
        sortable: true,
        dataIndex: "abbr",
      },
      {
        text: D.t("Name"),
        flex: 1,
        filter: true,
        sortable: true,
        dataIndex: "name",
      },
      {
        text: D.t("Currency type"),
        flex: 1,
        sortable: true,
        dataIndex: "crypto",
        renderer: function(v, m, r) {
          if (r.data.crypto) return "Crypto";
          return "Fiat";
        },
        filter: {
          xtype: "combo",
          valueField: "key",
          displayField: "name",
          store: {
            fields: ["key", "name"],
            data: [
              { key: false, name: D.t("Fiat") },
              { key: true, name: D.t("Crypto") },
            ],
          },
        },
      },
      {
        text: D.t("Fraction points"),
        flex: 1,
        filter: true,
        sortable: true,
        dataIndex: "fraction_points",
      },
    ];
  },
});
