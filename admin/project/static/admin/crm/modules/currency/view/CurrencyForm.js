Ext.define("Crm.modules.currency.view.CurrencyForm", {
  extend: "Core.form.FormWindow",
  requires: ["Ext.form.HtmlEditor"],
  titleTpl: D.t("Currency"),
  iconCls: "x-fa fa-usd",

  controllerCls: "Crm.modules.currency.view.CurrencyFormController",
  width: 500,
  height: 300,
  syncSize: function() {},
  buildItems: function() {
    return [
      {
        name: "id",
        hidden: true,
      },
      {
        name: "code",
        fieldLabel: D.t("Code"),
        allowBlank: false,
        minValue: 1,
        hideTrigger: true,
        allowNegative: false,
        maxLength: 3,
        maskRe: /[0-9]/,
        allowPureDecimal: true,
        validator: function(value) {
          return true;
        },
      },
      {
        name: "abbr",
        fieldLabel: D.t("Abbreviation ISO 3"),
        allowBlank: false,
        maxLength: 4,
        minLength: 3,
        maskRe: /[a-zA-Z]/,
        validator: function(value) {
          return true;
        },
      },
      {
        name: "name",
        fieldLabel: D.t("Name"),
        allowBlank: false,
        maxLength: 50,
        minLength: 1,
        validator: function(value) {
          return true;
        },
      },
      this.buildCryptoCombo(),
      {
        name: "fraction_points",
        xtype: "numberfield",
        fieldLabel: D.t("Fraction points"),
        allowBlank: false,
        minValue: 1,
        hideTrigger: true,
        allowNegative: false,
        // maxValue: 9,
        validator: function(value) {
          return true;
        },
      },
    ];
  },
  buildCryptoCombo() {
    return {
      name: "crypto",
      xtype: "combo",
      fieldLabel: D.t("Currency type"),
      value: false,
      displayField: "name",
      editable: false,
      valueField: "key",
      validator: true,
      allowBlank: false,
      store: {
        fields: ["key", "name"],
        data: [
          { key: false, name: D.t("Fiat") },
          { key: true, name: D.t("Crypto") },
        ],
      },
    };
  },
});
