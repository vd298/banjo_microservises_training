Ext.define("Crm.modules.currency.view.CurrencyFormController", {
  extend: "Core.form.FormController",
  setControls() {
    this.control({
      "[name=name]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();
          if (formData.name.trim().length < 1) {
            me.view.down("[name=name]").setValue("");
            e.validator = function(value) {
              return "White space not allowed";
            };
          } else {
            e.validator = function(value) {
              return true;
            };
          }
          me.view.down("form").isValid();
        },
      },
      "[name=abbr]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();
          this.model.runOnServer("checkUniqueABBR", formData, function(res) {
            e.validator = function(value) {
              if (res.result) {
                return true;
              } else {
                return res.message;
              }
            };
            me.view.down("form").isValid();
          });
        },
      },
      "[name=code]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();
          this.model.runOnServer("checkUniqueCode", formData, function(res) {
            e.validator = function(value) {
              if (res.result) {
                return true;
              } else {
                return res.message;
              }
            };
            me.view.down("form").isValid();
          });
        },
      },
    });
    this.callParent(arguments);
  },
});
