Ext.define("Crm.modules.accountsPool.model.ExchangeAccountsModel", {
  extend: "Crm.modules.accountsPool.model.AllAccountsModel",

  strongRequest: true,

  /* scope:server */
  buildWhere: function(params, cb) {
    params.filters.push({
      _property: "bank_type",
      _value: 1,
      _operator: "eq"
    });
    var me = this,
      args = arguments;
    me.callParent(args);
  }
});
