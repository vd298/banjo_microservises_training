Ext.define("Crm.modules.accountsPool.model.AccountsPoolModel", {
  extend: "Crm.classes.DataModel",

  collection: "vw_bank_accounts",
  idField: "id",
  strongRequest: true,
  removedFilter: true,
  foreignKeyFilter: [
    {
      collection: "banks",
      alias: "b",
      on: "  vw_bank_accounts.bank_id = banks.id",
      type: "inner"
    }
  ],
  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "account_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "acc_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "currency",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "bank_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "bank_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "merchant_id",
      type: "ObjectID",
      filterable: false,
      editable: true,
      visible: true
    },
    {
      name: "mapping_id",
      type: "ObjectID",
      filterable: false,
      editable: false,
      visible: true
    },
    {
      name: "merchant_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "active_relation",
      type: "boolean",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "merchant_account_id",
      type: "ObjectID",
      filterable: false,
      editable: true,
      visible: true
    },
    {
      name: "merchant_account_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "merchant_account_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "active_relation",
      type: "boolean",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "status",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "allocation_count",
      type: "integer",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "bank_type",
      type: "integer",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    }
    // {
    //   name: "start_time",
    //   type: "date",
    //   filterable: true,
    //   editable: true,
    //   visible: true
    // },
    // {
    //   name: "end_time",
    //   type: "date",
    //   filterable: true,
    //   editable: true,
    //   visible: true
    // }
  ],
  /* scope:server */
  buildWhere: function(params, cb) {
    params.filters.push({
      _property: "bank_type",
      _value: 0,
      _operator: "eq"
    });
    var me = this,
      args = arguments;
    me.callParent(args);
  },
  /* scope:server */
  async $unassignAccount(data, cb) {
    let me = this;
    const res = await this.callApi({
      service: "account-service",
      method: "releaseBankAccount",
      data,
      writeFlag: true,
      options: {
        realmId: me.user.profile.realm_id
      }
    });
    if (res.result) {
      this.changeModelData(
        "Crm.modules.accountsPool.model.AssignedAccountsModel",
        "ins",
        {}
      );
      this.changeModelData(
        "Crm.modules.accountsPool.model.UnassignedAccountsModel",
        "ins",
        {}
      );
    }
    return cb(res);
  },

  /* scope:server*/
  async $assignAccount(data, cb) {
    let me = this;
    const res = await this.callApi({
      service: "account-service",
      method: "associateBankAccount",
      data,
      writeFlag: true,
      options: {
        realmId: me.user.profile.realm_id
      }
    });
    if (res.result) {
      this.changeModelData(
        "Crm.modules.accountsPool.model.AssignedAccountsModel",
        "ins",
        {}
      );
      this.changeModelData(
        "Crm.modules.accountsPool.model.UnassignedAccountsModel",
        "ins",
        {}
      );
    }
    return cb(res);
  },
  async $fetchAssignedAccountsById(data, callbackFunction) {
    try {
      if (!data.id) {
        return callbackFunction([]);
      } else if (data && typeof data.id == "string" && data.id.length == 36) {
        let sql = `select vw_bank_accounts.id, vw_bank_accounts.merchant_name, vw_bank_accounts.merchant_id,vw_bank_accounts.status,vw_bank_accounts.active_relation from vw_bank_accounts `;
        sql = await this.buildJoinQuery(sql);
        sql += ` where vw_bank_accounts.removed=0 and vw_bank_accounts.id = $1 `;
        const assignedAccounts = await this.src.db.query(sql, [data.id]);

        if (assignedAccounts && assignedAccounts.length) {
          return callbackFunction(assignedAccounts);
        } else {
          return callbackFunction([]);
        }
      } else {
        return callbackFunction([]);
      }
    } catch (err) {
      console.error(
        `Func:fetchAssignedAccountsById, File: MerchantAccountsModel.js, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      callbackFunction(err);
    }
  },
  async $toggleBankStatusById(data, callbackFunction) {
    try {
      if (!data.id) {
        return callbackFunction([]);
      } else if (data && typeof data.id == "string" && data.id.length == 36) {
        const update = await this.src.db.query(
          `UPDATE bank_accounts SET status  = $2 WHERE id = $1 RETURNING *`,
          [data.id, data.status]
        );
        return callbackFunction(update);
      } else {
        return callbackFunction(0);
      }
    } catch (err) {
      console.error(
        `Func:toggleBankStatusById, File: AccountsPoolModel.js, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      callbackFunction(err);
    }
  },
  async $fetchMappingId(data, callbackFunction) {
    try {
      if (!data.id && !data.merchant_id && data.merchant_account_id) {
        return callbackFunction([]);
      }
      let sql = `select vw_bank_accounts.mapping_id  from vw_bank_accounts `;
      sql = await this.buildJoinQuery(sql);
      sql += ` where vw_bank_accounts.removed=0 and vw_bank_accounts.id = $1 and vw_bank_accounts.merchant_id =$2 and vw_bank_accounts.merchant_account_id  = $3`;
      const mappingId = await this.src.db.query(sql, [
        data.id,
        data.merchant_id,
        data.merchant_account_id
      ]);

      if (mappingId) {
        return callbackFunction(mappingId);
      } else {
        return callbackFunction({ message: "Account not found" });
      }
    } catch (err) {
      console.error(
        `Func:fetchMappingId, File: AccountsPoolModel.js, Caught error:`,
        typeof err == "object" ? JSON.stringify(err) : err
      );
      callbackFunction(err);
    }
  }
});
