Ext.define("Crm.modules.accountsPool.model.AssignedAccountsModel", {
  extend: "Crm.modules.accountsPool.model.AccountsPoolModel",
  removedFilter: true,
  collection: "vw_bank_accounts_per_merchant_account",
  idField: "id",
  foreignKeyFilter: [
    {
      collection: "banks",
      alias: "b",
      on: "  vw_bank_accounts_per_merchant_account.bank_id = banks.id ",
      type: "inner"
    }
  ],
  /* scope:server */
  buildWhere: function(params, cb) {
    params.filters.push({
      _property: "active_relation",
      _value: true,
      _operator: "eq"
    });
    var me = this,
      args = arguments;
    me.callParent(args);
  }
});
