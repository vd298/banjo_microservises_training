Ext.define("Crm.modules.accountsPool.model.UnassignedAccountsModel", {
  extend: "Crm.modules.accountsPool.model.AccountsPoolModel",
  removedFilter: true,
  /* scope:server */
  buildWhere: function(params, cb) {
    params.filters.push(
      {
        _property: "active_relation",
        _value: false,
        _operator: "eq"
      },
      { _property: "bank_type", _value: 0, _operator: "eq" }
    );
    var me = this,
      args = arguments;
    me.callParent(args);
  }
});
