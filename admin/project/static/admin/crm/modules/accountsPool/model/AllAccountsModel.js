Ext.define("Crm.modules.accountsPool.model.AllAccountsModel", {
  extend: "Crm.classes.DataModel",

  collection: "vw_bank_accounts",
  idField: "id",
  strongRequest: true,
  foreignKeyFilter: [
    {
      collection: "banks",
      alias: "b",
      on: "   vw_bank_accounts.bank_id = banks.id ",
      type: "inner"
    }
  ],
  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "account_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "acc_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "currency",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "bank_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "bank_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "merchant_id",
      type: "ObjectID",
      filterable: false,
      editable: true,
      visible: true
    },
    {
      name: "mapping_id",
      type: "ObjectID",
      filterable: false,
      editable: false,
      visible: true
    },
    {
      name: "merchant_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "active_relation",
      type: "boolean",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "merchant_account_id",
      type: "ObjectID",
      filterable: false,
      editable: true,
      visible: true
    },
    {
      name: "merchant_account_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "merchant_account_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "active_relation",
      type: "boolean",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "status",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "allocation_count",
      type: "integer",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "bank_type",
      type: "integer",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    }
  ]
});
