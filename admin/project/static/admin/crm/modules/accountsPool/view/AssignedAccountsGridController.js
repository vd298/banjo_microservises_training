Ext.define("Crm.modules.accountsPool.view.AssignedAccountsGridController", {
  extend: "Core.grid.GridController",
  setControls() {
    let me = this;

    this.control({
      "[action=add]": {
        click: function(el) {
          me.addRecord();
        }
      },
      "[action=refresh]": {
        click: function(el) {
          me.reloadData();
        }
      },
      "[action=import]": {
        click: function(el) {
          me.importData();
        }
      },
      "[action=export]": {
        click: function(el) {
          me.exportData();
        }
      },
      grid: {
        cellkeydown: function(cell, td, i, rec, tr, rowIndex, e, eOpts) {
          if (e.keyCode == 13) {
            me.gotoRecordHash(rec.data);
          }
        },
        celldblclick: function(cell, td, i, rec) {
          me.gotoRecordHash(rec.data);
        },
        itemcontextmenu: function(vw, record, item, index, e, options) {
          e.stopEvent();
          //if(win.menuContext) {
          //    win.menuContext.record = record;
          //    win.menuContext.showAt(e.pageX, e.pageY);
          //}
        }
      }
    });
    this.view.on("activate", function(grid, indx) {
      if (!me.view.observeObject)
        document.title = me.view.title + " " + D.t("ConsoleTitle");
    });
    this.view.on("edit", function(grid, indx) {
      me.gotoRecordHash(grid.getStore().getAt(indx).data);
    });
    this.view.on("delete", function(grid, indx) {
      me.deleteRecord(grid.getStore(), indx);
    });
    this.initButtonsByPermissions();

    this.view.on("unassignAccount", function(grid, data) {
      // me.unassignAccount(data);
      me.unassignAccount(data);
    });

    //yc274 temporary fix for dialog
    setTimeout(() => {
      me.reloadData();
    }, 500);
    //
  },
  async unassignAccount(record) {
    var me = this;
    var acceptWindow = Ext.create("Ext.window.Window", {
      title: "Unassign bank account?",
      layout: "fit",
      width: "40%",
      name: "unassignAccount",
      //margin: 30,
      modal: true,
      items: [
        {
          xtype: "form",
          layout: "anchor",
          name: "notes_form",
          //margin: 10,
          defaults: {
            xtype: "textfield",
            anchor: "100%",
            labelWidth: 150,
            padding: 10
          },
          items: [
            {
              name: "id",
              hidden: true,
              allowBlank: true,
              maxLength: 36,
              value: record && record.id ? record.id : ""
            },
            {
              name: "mapping_id",
              hidden: true,
              allowBlank: true,
              maxLength: 36,
              value: record && record.mapping_id ? record.mapping_id : ""
            },
            {
              flex: 1,
              xtype: "combo",
              name: "merchant_id",
              allowBlank: false,
              fieldLabel: D.t("Select Merchant"),
              value: record.merchant_id,
              store: Ext.create("Core.data.ComboStore", {
                dataModel: Ext.create(
                  "Crm.modules.bankAccounts.model.ActiveAllocationModel"
                ),
                fieldSet: ["merchant_id", "merchant_name"],
                exProxyParams: {
                  _property: "bank_account",
                  _value: record.id,
                  _operator: "eq"
                },
                scope: this
              }),
              valueField: "merchant_id",
              displayField: "merchant_name",
              listeners: {
                change: (el, v) => {
                  const proxyFilters = {
                    filters: [
                      { _property: "merchant_id", _value: v, _operator: "eq" },
                      {
                        _property: "bank_account",
                        _value: record.id,
                        _operator: "eq"
                      }
                    ]
                  };
                  const store = Ext.create("Core.data.ComboStore", {
                    dataModel: Ext.create(
                      "Crm.modules.bankAccounts.model.ActiveAllocationModel"
                    ),
                    fieldSet: ["merchant_account_id", "merchant_account_name"],
                    exProxyParams: proxyFilters,
                    scope: this
                  });
                  acceptWindow
                    .down("[name=merchant_account_id]")
                    .setStore(store);
                  acceptWindow.down("[name=merchant_account_id]").setValue("");
                  acceptWindow.down("[name=mapping_id]").setValue("");
                  acceptWindow.down("[name=unassign_account]").disable();
                }
              }
            },
            {
              flex: 1,
              xtype: "combo",
              name: "merchant_account_id",
              fieldLabel: D.t("Select Merchant Account"),
              value: record.merchant_account_id,
              valueField: "merchant_account_id",
              allowBlank: false,
              displayField: "merchant_account_name",
              store: Ext.create("Core.data.ComboStore", {
                dataModel: Ext.create(
                  "Crm.modules.bankAccounts.model.ActiveAllocationModel"
                ),
                fieldSet: ["merchant_account_id", "merchant_account_name"],
                exProxyParams: {
                  filters: [
                    {
                      _property: "merchant_id",
                      _value: record.merchant_id,
                      _operator: "eq"
                    },
                    {
                      _property: "bank_account",
                      _value: record.id,
                      _operator: "eq"
                    }
                  ]
                },
                scope: this
              }),
              listeners: {
                change: (el, v) => {
                  let merchant_id = acceptWindow
                    .down("[name=merchant_id]")
                    .getValue();
                  if (record.id && merchant_id && v) {
                    this.model.runOnServer(
                      "fetchMappingId",
                      { id: record.id, merchant_id, merchant_account_id: v },
                      function(res) {
                        if (res && res.length) {
                          acceptWindow
                            .down("[name=mapping_id]")
                            .setValue(res[0].mapping_id);
                          acceptWindow.down("[name=unassign_account]").enable();
                        }
                      }
                    );
                  }
                }
              }
            },

            {
              xtype: "fieldcontainer",
              anchor: "100%",
              layout: "hbox",
              align: "center",
              margin: { top: 15, bottom: 5 },
              items: [
                {
                  xtype: "button",
                  text: D.t("Unassign Account"),
                  name: "unassign_account",
                  listeners: {
                    click: async function(e, v) {
                      var actionBtn = e;
                      if (actionBtn) {
                        actionBtn.disable();
                      }
                      var reqData = this.up("form").getValues() || {};
                      me.model.runOnServer(
                        "unassignAccount",
                        { mapping_id: reqData.mapping_id },
                        function(res) {
                          if (res.result) {
                            me.view.store.reload();
                            acceptWindow.close();
                          } else {
                            D.a(res.error.title, res.error.message);
                          }
                        }
                      );
                    }
                  }
                },
                {
                  xtype: "button",
                  margin: { left: 5 },
                  //padding: 10,
                  text: D.t("Cancel"),
                  listeners: {
                    click: function() {
                      acceptWindow.close();
                    }
                  }
                }
              ]
            }
          ]
        }
      ]
    }).show();
  }
});
