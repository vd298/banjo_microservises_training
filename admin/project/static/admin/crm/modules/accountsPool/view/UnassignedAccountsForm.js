Ext.define("Crm.modules.accountsPool.view.UnassignedAccountsForm", {
  extend: "Core.form.FormWindow",

  titleTpl: D.t("Allocate Bank Account"),
  requires: ["Desktop.core.widgets.GridField"],
  controllerCls:
    "Crm.modules.accountsPool.view.UnassignedAccountsFormController",
  width: "60%",
  height: 250,
  syncSize: function() {},

  buildItems() {
    let firstTime = true;
    return [
      {
        hidden: true,
        editable: false,
        name: "id"
      },
      {
        hidden: true,
        editable: false,
        name: "status"
      },
      {
        hidden: true,
        editable: false,
        name: "currency"
      },
      {
        name: "message",
        xtype: "label",
        hidden: true
      },
      {
        hidden: true,
        editable: false,
        name: "ctime"
      },
      {
        flex: 1,
        xtype: "combo",
        name: "bank_id",
        allowBlank: false,
        fieldLabel: D.t("Select Bank"),
        store: Ext.create("Core.data.ComboStore", {
          dataModel: Ext.create("Crm.modules.banks.model.BanksModel"),
          exProxyParams: {
            filters: [
              {
                _property: "bank_type",
                _value: 0,
                _operator: "eq"
              }
            ]
          },
          fieldSet: ["id", "name"],
          scope: this
        }),
        valueField: "id",
        displayField: "name",
        listeners: {
          change: (el, v) => {
            if (!firstTime) {
              this.down("[name=bank_account_id]").clearValue();
              this.down("[name=toggleStatus]").setHidden(true);
              firstTime = false;
            } else {
              const id = this.down("[name=id]").getValue();
              const ctime = this.down("[name=ctime]").getValue();
              if (id && ctime) {
                this.down("[name=bank_account_id]").setValue(id);
              }
              firstTime = false;
            }
            const proxyFilters = {
              filters: [{ _property: "bank_id", _value: v, _operator: "eq" }]
            };
            const store = Ext.create("Core.data.ComboStore", {
              dataModel: Ext.create(
                "Crm.modules.accountsPool.model.AccountsPoolModel"
              ),
              fieldSet: ["id", "account_name", "acc_no", "currency", "status"],
              exProxyParams: proxyFilters,
              scope: this
            });
            this.down("[name=bank_account_id]").setStore(store);
          }
        }
      },
      {
        xtype: "fieldcontainer",
        layout: "hbox",
        align: "center",
        defaults: {
          flex: 1
        },
        style: {
          "align-item": "center",
          "justify-content": "center"
        },
        items: [
          {
            flex: 3,
            labelWidth: 150,
            width: "70%",
            xtype: "combo",
            name: "bank_account_id",
            fieldLabel: D.t("Select Bank Account"),
            valueField: "id",
            allowBlank: false,
            displayField: "account_name",
            tpl:
              '<tpl for="."><div class="x-boundlist-item" style="height:40px;"><b>{account_name}</b>&nbsp;&nbsp;(A/C: {acc_no})&nbsp;(Status: {status})</div><div style="clear:both"></div></tpl>',
            listeners: {
              change: (el, v) => {
                this.down("[name=currency]").setValue(
                  el.valueCollection.items[0].data.currency
                );
                const currentSelected = this.down("[name=account]").value;
                this.down("[name=account]").setValue("");
                this.down("[name=account]").setValue(currentSelected);
                this.down("[name=merchant_account_id]").clearValue();
              }
            }
          },
          {
            xtype: "button",
            name: "toggleStatus",
            action: "toggleBankStatus",
            text: "",
            hidden: true,
            style: {
              "margin-left": "10px"
            }
          }
        ]
      },
      {
        flex: 1,
        xtype: "combo",
        name: "account",
        allowBlank: false,
        fieldLabel: D.t("Select Merchant"),
        store: Ext.create("Core.data.ComboStore", {
          dataModel: Ext.create("Crm.modules.accounts.model.AccountsModel"),
          fieldSet: ["id", "name"],
          scope: this
        }),
        valueField: "id",
        displayField: "name",
        listeners: {
          change: (el, v) => {
            this.down("[name=merchant_account_id]").clearValue();
            const proxyFilters = {
              filters: [
                { _property: "account_id", _value: v, _operator: "eq" },
                {
                  _property: "currency",
                  _value: this.down("[name=currency]").value,
                  _operator: "eq"
                }
              ]
            };
            const store = Ext.create("Core.data.ComboStore", {
              dataModel: Ext.create(
                "Crm.modules.accounts.model.MerchantAccountsModel"
              ),
              fieldSet: ["id", "name"],
              exProxyParams: proxyFilters,
              scope: this
            });
            this.down("[name=merchant_account_id]").setStore(store);
          }
        }
      },
      {
        flex: 1,
        xtype: "combo",
        name: "merchant_account_id",
        fieldLabel: D.t("Select Merchant Account"),
        valueField: "id",
        allowBlank: false,
        displayField: "name"
      }
    ];
  },
  buildButtons: function() {
    var btns = [
      "->",
      {
        text: D.t("Assign Account"),
        iconCls: "x-fa fa-link",
        scale: "small",
        action: "assignAccount",
        disabled: true
      },

      "-",
      { text: D.t("Close"), iconCls: "x-fa fa-ban", action: "formclose" }
    ];
    return btns;
  }
});
