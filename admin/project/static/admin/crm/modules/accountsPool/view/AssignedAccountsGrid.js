Ext.define("Crm.modules.accountsPool.view.AssignedAccountsGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Assigned Bank Accounts"),

  filterable: true,
  filterbar: true,
  controllerCls: "Crm.modules.accountsPool.view.AssignedAccountsGridController",
  buildColumns: function() {
    return [
      {
        hidden: true,
        dataIndex: "mapping_id"
      },
      {
        hidden: true,
        dataIndex: "id"
      },
      {
        hidden: true,
        dataIndex: "bank_name"
      },
      {
        dataIndex: "bank_account",
        hidden: true
      },
      {
        hidden: true,
        dataIndex: "merchant_name"
      },
      {
        hidden: true,
        dataIndex: "merchant_account_name"
      },
      {
        text: D.t("Account Holder"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "account_name"
      },
      {
        text: D.t("Account Number"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "acc_no"
      },
      {
        text: D.t("Currency"),
        flex: 1,
        sortable: true,
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "abbr",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create("Crm.modules.currency.model.CurrencyModel"),
            fieldSet: ["name", "abbr"],
            scope: this
          }),
          operator: "eq"
        },
        dataIndex: "currency"
      },
      {
        text: D.t("Bank"),
        flex: 1,
        sortable: true,
        dataIndex: "bank_id",
        renderer: function(v, m, r) {
          if (v) {
            return (
              '<a href="#Crm.modules.banks.view.BanksForm~' +
              v +
              '">' +
              r.data.bank_name +
              "</a>"
            );
          }
          return v;
        },
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "id",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create("Crm.modules.banks.model.BanksModel"),
            fieldSet: ["id", "name"],
            exProxyParams: {
              filters: [
                {
                  _property: "bank_type",
                  _value: 0,
                  _operator: "eq"
                }
              ]
            },
            scope: this
          }),
          operator: "eq"
        }
      },
      {
        text: D.t("Associated Merchant"),
        flex: 1,
        hidden: true,
        sortable: true,
        dataIndex: "merchant_id",
        renderer: function(v, m, r) {
          if (v) {
            return (
              '<a href="#Crm.modules.accounts.view.AccountsForm~' +
              v +
              '">' +
              r.data.merchant_name +
              "</a>"
            );
          }
          return v;
        },
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "id",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create("Crm.modules.accounts.model.AccountsModel"),
            fieldSet: ["id", "name"],
            scope: this
          }),
          operator: "eq"
        }
      },
      {
        text: D.t("Associated Merchant Account"),
        flex: 1,
        hidden: true,
        sortable: true,
        dataIndex: "merchant_account_id",
        renderer: function(v, m, r) {
          if (v) {
            return (
              '<a href="#Crm.modules.accounts.view.MerchantAccountsForm~' +
              v +
              '">' +
              r.data.merchant_account_name +
              "</a> "
            );
          }
          return v;
        },
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "id",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create(
              "Crm.modules.accounts.model.MerchantAccountsModel"
            ),
            fieldSet: ["id", "name"],
            scope: this
          }),
          operator: "eq"
        }
      },
      {
        text: D.t("Associated Merchant Accounts"),
        dataIndex: "allocation_count",
        sortable: true,
        filter: true
      },
      {
        text: D.t("bank_account"),
        hidden: true,
        sortable: true,
        filter: true,
        dataIndex: "bank_account"
      },
      this.buildButtonsColumns()
    ];
  },
  buildButtonsColumns() {
    var me = this;
    return [
      {
        xtype: "actioncolumn",
        width: 54,
        items: [
          {
            tooltip: D.t("Allocate this bank account to merchant account"),
            text: D.t("Assign Account"),
            iconCls: "x-fa fa-link",
            action: "assignAccount",
            handler: function(grid, index) {
              me.fireEvent("edit", grid, index);
            }
          },
          {
            tooltip: D.t("Release this bank account from merchant account"),
            text: D.t("Unassign Account"),
            iconCls: "x-fa fa-unlink",
            action: "unassignAccount",
            handler: function(grid, index) {
              me.fireEvent(
                "unassignAccount",
                grid,
                grid.store.getAt(index).data
              );
            }
          }
        ]
      }
    ];
  }
  // buildTbar() {
  //   return [
  //     {
  //       tooltip: "Reload list",
  //       iconCls: "x-fa fa-refresh",
  //       action: "refresh"
  //     }
  //   ];
  // }
});
