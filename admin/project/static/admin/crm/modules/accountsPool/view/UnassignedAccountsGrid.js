Ext.define("Crm.modules.accountsPool.view.UnassignedAccountsGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Unassigned Bank Accounts"),

  filterable: true,
  filterbar: true,
  detailsInDialogWindow: true,
  buildColumns: function() {
    return [
      {
        text: D.t("Id"),
        hidden: true,
        sortable: true,
        filter: true,
        dataIndex: "id"
      },
      {
        hidden: true,
        sortable: true,
        filter: true,
        dataIndex: "bank_name"
      },
      {
        hidden: true,
        sortable: true,
        filter: true,
        dataIndex: "merchant_name"
      },
      {
        hidden: true,
        sortable: true,
        filter: true,
        dataIndex: "merchant_account_name"
      },
      {
        text: D.t("Account Holder"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "account_name"
      },
      {
        text: D.t("Account Number"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "acc_no"
      },
      {
        text: D.t("Currency"),
        flex: 1,
        sortable: true,
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "abbr",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create("Crm.modules.currency.model.CurrencyModel"),
            fieldSet: ["name", "abbr"],
            scope: this
          }),
          operator: "eq"
        },
        dataIndex: "currency"
      },
      {
        text: D.t("Bank"),
        flex: 1,
        sortable: true,
        dataIndex: "bank_id",
        renderer: function(v, m, r) {
          if (v) {
            return (
              '<a href="#Crm.modules.banks.view.BanksForm~' +
              v +
              '">' +
              r.data.bank_name +
              "</a>"
            );
          }
          return v;
        },
        filter: {
          xtype: "combo",
          queryMode: "local",
          forceSelection: true,
          triggerAction: "all",
          editable: true,
          valueField: "id",
          displayField: "name",
          store: Ext.create("Core.data.ComboStore", {
            dataModel: Ext.create("Crm.modules.banks.model.BanksModel"),
            fieldSet: ["id", "name"],
            exProxyParams: {
              filters: [
                {
                  _property: "bank_type",
                  _value: 0,
                  _operator: "eq"
                }
              ]
            },
            scope: this
          }),
          operator: "eq"
        }
      }
    ];
  },
  buildButtonsColumns() {
    var me = this;
    return [
      {
        xtype: "actioncolumn",
        items: [
          {
            tooltip: D.t("Allocate this bank account to merchant account"),
            text: D.t("Assign Account"),
            iconCls: "x-fa fa-link",
            action: "assignAccount",
            handler: function(grid, index) {
              me.fireEvent("edit", grid, index);
            }
          }
        ]
      }
    ];
  },
  buildTbar() {
    return [
      {
        text: "Allocate Account",
        tooltip: "Allocate bank accounts to merchant accounts",
        iconCls: "x-fa fa-plus",
        scale: "medium",
        action: "add"
      },
      "-",
      {
        tooltip: "Reload list",
        iconCls: "x-fa fa-refresh",
        action: "refresh"
      }
    ];
  }
});
