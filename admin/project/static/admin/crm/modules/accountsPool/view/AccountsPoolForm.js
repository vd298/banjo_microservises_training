Ext.define("Crm.modules.accountsPool.view.AccountsPoolForm", {
  extend: "Core.form.FormContainer",
  titleTpl: D.t("Bank Accounts Pool"),
  iconCls: "x-fa fa-user",
  requires: [
    "Desktop.core.widgets.GridField",
    "Core.form.DateField",
    "Core.form.DependedCombo",
    "Ext.form.field.Tag"
  ],
  onClose: function(me) {
    window.history.back();
  },
  formLayout: "fit",

  formMargin: 0,
  buildButtons: function() {
    return [];
  },
  buildItems() {
    return {
      xtype: "tabpanel",
      layout: "fit",
      items: [this.buildAssignedAccounts(), this.buildUnassignedAccounts()],
      autoEl: { "data-test": "tab-items" }
    };
  },
  buildAssignedAccounts() {
    return {
      xtype: "panel",
      layout: "fit",
      title: D.t("Assigned Accounts"),
      items: Ext.create("Crm.modules.accountsPool.view.AssignedAccountsGrid", {
        scope: this
      })
    };
  },
  buildUnassignedAccounts() {
    return {
      xtype: "panel",
      layout: "fit",
      title: D.t("Unassigned Accounts"),
      items: Ext.create(
        "Crm.modules.accountsPool.view.UnassignedAccountsGrid",
        {
          scope: this
        }
      )
    };
  }
});
