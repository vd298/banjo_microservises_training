Ext.define("Crm.modules.accountsPool.view.UnassignedAccountsFormController", {
  extend: "Core.form.FormController",

  setControls() {
    var me = this;
    let status = "";
    let message = "",
      selectedmerchName = "";
    this.control({
      "[name=bank_account_id]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();

          if (formData.bank_account_id) {
            this.model.runOnServer(
              "fetchAssignedAccountsById",
              { id: formData.bank_account_id },
              async function(res) {
                if (res && res.length) {
                  status = res[0].status;
                  message = __CONFIG__.setAssignedAccountMessage(
                    e,
                    me.view.down("form"),
                    res
                  );
                } else {
                  message = "";
                  selectedmerchName = "";
                  me.view
                    .down("form")
                    .down("[name=message]")
                    .setHidden(true)
                    .setText("");
                }
                me.view
                  .down("form")
                  .down("[name=toggleStatus]")
                  .setText(
                    status === "ACTIVE"
                      ? "Update Status: Inactive"
                      : "Update Status: Active"
                  )
                  .setHidden(false);
                me.view.down("form").isValid();
              }
            );
          } else {
            me.view
              .down("form")
              .down("[name=toggleStatus]")
              .setHidden(true);
            message = "";
            selectedmerchName = "";
            me.view
              .down("form")
              .down("[name=message]")
              .setHidden(true)
              .setText("");
          }
        }
      },
      "[name=account]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          selectedmerchName = me.view
            .down("form")
            .down("[name=account]")
            .getDisplayValue();
          if (message && selectedmerchName) {
            selectedmerchName = ` do you want to share with`;
            selectedmerchName += ` ${me.view
              .down("form")
              .down("[name=account]")
              .getDisplayValue()}?`;
            me.view
              .down("form")
              .down("[name=message]")
              .setHidden(false)
              .setText(`${message} ${selectedmerchName}`);
          }
        }
      },
      "[action=assignAccount]": {
        click: (el, v) => {
          this.assignAccount();
        }
      },
      "[action=toggleBankStatus]": {
        click: (el, v) => {
          const me = this;
          const formData = me.view.down("form").getValues();
          this.model.runOnServer(
            "toggleBankStatusById",
            {
              id: formData.bank_account_id,
              status: status === "ACTIVE" ? "INACTIVE" : "ACTIVE"
            },
            function(res) {
              status = __CONFIG__.toggleBankStatusById(
                el,
                me.view.down("form"),
                res
              );
              me.view.down("form").isValid();
            }
          );
        }
      }
    });

    this.view.down("form").on({
      validitychange: (th, valid, eOpts) => {
        var el = this.view.down("[action=assignAccount]");
        if (el) el.setDisabled(!valid);
        el = this.view.down("[action=save]");
        if (el) el.setDisabled(!valid);
      }
    });
    this.callParent(arguments);
  },

  async assignAccount() {
    let me = this;
    let data = this.view
      .down("form")
      .getForm()
      .getValues();
    let mes = me.view.down("form").down("[name=message]").text;

    const isAssigned = await __CONFIG__.checkAlreadyAssigned(me, mes);

    if (!isAssigned) {
      return;
    } else {
      me.model.runOnServer(
        "assignAccount",
        {
          merchant_account_id: data.merchant_account_id,
          bank_account_id: data.bank_account_id
        },
        function(res) {
          if (res.result) {
            me.closeView();
          } else {
            D.a(res.error.title, res.error.message);
          }
        }
      );
    }
  }
});
