Ext.define("Crm.modules.Transactions.view.TransactionsGridController", {
  extend: "Core.grid.GridController",

  setControls: function() {
    var me = this;
    this.control({
      "[action=refresh]": {
        click: function(el) {
          me.reloadData();
        }
      },
      "[action=import]": {
        click: function(el) {
          me.importData();
        }
      },
      "[action=export]": {
        click: function(el) {
          me.exportData();
        }
      },
      grid: {
        cellkeydown: function(cell, td, i, rec, tr, rowIndex, e, eOpts) {
          if (e.keyCode == 13) {
            //me.gotoRecordHash(rec.data);
          }
        },
        celldblclick: function(cell, td, i, rec) {
          //me.gotoRecordHash(rec.data);
        },
        itemcontextmenu: function(vw, record, item, index, e, options) {
          e.stopEvent();
          //if(win.menuContext) {
          //    win.menuContext.record = record;
          //    win.menuContext.showAt(e.pageX, e.pageY);
          //}
        }
      }
    });
    this.view.on("activate", function(grid, indx) {
      if (!me.view.observeObject)
        document.title = me.view.title + " " + D.t("ConsoleTitle");
    });
    this.view.on("edit", function(grid, indx) {
      //me.gotoRecordHash(grid.getStore().getAt(indx).data);
    });
    this.view.on("delete", function(grid, indx) {
      //me.deleteRecord(grid.getStore(), indx);
    });
    this.initButtonsByPermissions();
    //this.view.on('modify', function(id) {alert();me.modify(id)})
  },

  afterDataLoad: function(view, cb) {
    var me = this;
    return cb();
  }
});
