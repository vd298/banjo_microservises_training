Ext.define("Crm.modules.Transactions.view.TransactionsGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Transactions"),
  iconCls: "x-fa fa-dollar",

  requires: ["Ext.grid.column.Date", "Ext.grid.column.Number"],

  controllerCls: "Crm.modules.Transactions.view.TransactionsGridController",

  buildColumns: function() {
    return [
      {
        xtype: "datecolumn",
        text: D.t("Date"),
        flex: 2,
        sortable: true,
        format: "d.m.Y H:i:s",
        filter: {
          xtype: "datefield",
          format: "d.m.Y"
        },
        dataIndex: "ctime"
      },
      {
        text: D.t("Activity"),
        flex: 1,
        sortable: true,
        dataIndex: "activity",
        filter: true
      },
      {
        text: D.t("Type"),
        flex: 1,
        sortable: true,
        dataIndex: "txtype",
        filter: true
      },
      {
        text: D.t("Source Balance"),
        flex: 1,
        sortable: true,
        dataIndex: "src_balance",
        filter: true,
        value: 0,
        hidden: true,
        renderer: function(v, m, r) {
          if (v) {
           return __CONFIG__.formatCurrency(v,0,r.data.currency)
          }
          return v;
        },
      },
      {
        text: D.t("Destination Balance"),
        flex: 1,
        sortable: true,
        dataIndex: "dst_balance",
        filter: true,
        value: 0,
        hidden: true,
        renderer: function(v, m, r) {
          if (v) {
           return __CONFIG__.formatCurrency(v,0,r.data.currency)
          }
          return v;
        },
      },
      {
        text: D.t("Amount"),
        flex: 1,
        sortable: true,
        dataIndex: "amount",
        filter: true
      },
      {
        text: D.t("Currency"),
        flex: 1,
        sortable: true,
        dataIndex: "currency",
        filter: true
      },
      {
        text: D.t("BRN"),
        flex: 1,
        sortable: true,
        dataIndex: "brn",
        filter: true
      },
      {
        text: D.t("Tariff Name"),
        dataIndex: "tariff_name",
        hidden: true
      },
      {
        text: D.t("Plan Name"),
        dataIndex: "plan_name",
        hidden: true
      },
      {
        text: D.t("data"),
        dataIndex: "data",
        hidden: true
      }
    ];
  },

  detailsTpl() {
    return new Ext.XTemplate(
      "<table width='100%'><tr valign=top>",
      "<td width='50%'><p><b>ID:</b> {id}</p>",
      "<p><b>Tariff Name:</b> {tariff_name}</p>",
      "<p><b>Plane Name:</b> {plan_name}</p>",
      "<p><b>Date:</b> {ctime}</p>",
      `<p><tpl if="tr_ip"><b>Request IP:</b> {tr_ip}</p></tpl>`,
      `<p><tpl if="tr_user_ip"><b>User IP:</b> {tr_user_ip}</p></tpl>`,
      `<p><tpl if="tr_ua"><b>User Agent:</b> {tr_ua}</p></tpl>`,
      `<p><tpl if="tr_note"><b>Note:</b> {tr_note}</p></tpl>`,
      "</td></tr></table>",
      {
        formatNum: function(val) {
          if (val && typeof val == "string") {
            val = Number(val);
          }
          if (val && typeof val == "number") {
            return Number(val.toFixed(2));
          } else return val;
        }
      }
    );
  },

  buildPlugins() {
    let plugins = this.callParent();
    plugins.push({
      ptype: "rowexpander",
      rowBodyTpl: this.detailsTpl()
    });

    return plugins;
  },

  buildButtonsColumns() {
    return [];
  },

  buildTbar: function() {
    var items = [];

    if (this.importButton) {
      items.push("-", {
        text: this.buttonImportText,
        iconCls: "x-fa fa-cloud-download",
        action: "import"
      });
    }
    if (this.exportButton) {
      items.push("-", {
        text: this.buttonExportText,
        iconCls: "x-fa fa-cloud-upload",
        action: "export"
      });
    }

    items.push("-", {
      tooltip: this.buttonReloadText,
      iconCls: "x-fa fa-refresh",
      action: "refresh"
    });

    if (this.filterable) items.push("->", this.buildSearchField());
    return items;
  }
});
