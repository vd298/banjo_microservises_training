Ext.define("Crm.modules.Transactions.model.TransactionsModel", {
  extend: "Core.data.DataModel",

  collection: "vw_transactions",
  idField: "id",
  foreignKeyFilter: [
    {
      collection: "transfers",
      alias: "t",
      on: " transfers.id = vw_transactions.transfer_id ",
      type: "inner"
    },
    {
      collection: "accounts",
      alias: "a",
      on: " accounts.id = transfers.account_id ",
      type: "inner"
    }
  ],

  strongRequest: false,

  fields: [
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true,
      sort: -1
    },
    {
      name: "mtime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "int",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "signobject",
      type: "json",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_macc_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_macc_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_macc_status",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_macc_category",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_macc_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_macc_no",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_macc_status",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_macc_category",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "tariff_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "tariff_description",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "plan_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "plan_description",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "transfer_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "statement_note",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "internal_note",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_acc_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_acc_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "amount",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "src_balance",
      type: "float",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "dst_balance",
      type: "float",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "currency",
      type: "float",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "tariff_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "plan_id",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "status",
      type: "enumstring",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "brn",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "activity",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "txtype",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "data",
      type: "jsonb",
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  /* scope:server */
  afterGetData: function(data, cb) {
    var me = this;
    if (data && data.length) {
      for (let i = 0; i < data.length; i++) {
        if (data[i].data) {
          if (data[i].data.note) {
            data[i].tr_note = data[i].data.note;
          }
          if (data[i].data.ua && data[i].data.ua.ua) {
            data[i].tr_ua = data[i].data.ua.ua;
          }
          if (data[i].data.ip) {
            data[i].tr_ip = data[i].data.ip;
          }
          if (data[i].data.user_ip) {
            data[i].tr_user_ip = data[i].data.user_ip;
          }
        }
      }
    }
    return cb(data);
  }
});
