Ext.define("Crm.modules.users.controller.UsersFormController", {
  extend: "Core.form.FormController",
  setControls() {
    this.control({
      "[name=login]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();
          if (formData.login.trim().length < 1) {
            e.validator = function(value) {
              return "White space not allowed";
            };
          }
          this.model.checkUnicLogin(formData, function(res) {
            e.validator = function(value) {
              if (!res.isset) {
                return true;
              } else {
                return res.message;
              }
            };
            me.view.down("form").isValid();
          });
        }
      },
      "[name=superuser]": {
        change: function(v) {
          const me = this;
          const formData = me.view.down("form").getValues();

          if (!v.value) {
            me.view
              .down("form")
              .down("[name=groupid]")
              .setConfig("allowBlank", false)
              .enable();
            me.view
              .down("form")
              .down("[name=xgroups]")
              .enable();
          } else {
            me.view
              .down("form")
              .down("[name=groupid]")
              .setConfig("allowBlank", true)
              .setValue(null)
              .disable();
            me.view
              .down("form")
              .down("[name=xgroups]")
              .setValue(null)
              .disable();
          }
        }
      }
      // "[name=groupid]": {
      //   change: function(v) {
      //     const me = this;
      //     if (v.value) {
      //       me.view
      //         .down("form")
      //         .down("[name=groupid]")
      //         .enable();
      //       me.view
      //         .down("form")
      //         .down("[name=xgroups]")
      //         .enable();
      //     } else {
      //       me.view
      //         .down("form")
      //         .down("[name=groupid]")
      //         .setValue(null)
      //         .disable();
      //       me.view
      //         .down("form")
      //         .down("[name=xgroups]")
      //         .setValue(null)
      //         .disable();
      //     }
      //   },
      // },
      // "[name=xgroups]": {
      //   change: function(v) {
      //     const me = this;
      //     if (v.value) {
      //       me.view
      //         .down("form")
      //         .down("[name=groupid]")
      //         .enable();
      //       me.view
      //         .down("form")
      //         .down("[name=xgroups]")
      //         .enable();
      //     } else {
      //       me.view
      //         .down("form")
      //         .down("[name=groupid]")
      //         .setValue(null)
      //         .disable();
      //       me.view
      //         .down("form")
      //         .down("[name=xgroups]")
      //         .setValue(null)
      //         .disable();
      //     }
      //   },
      // },
    });
    this.callParent(arguments);
  },
  afterDataLoad(data, cb) {
    const me = this;
    me.model.runOnServer("getAdminDetails", data, function(resp) {
      if (resp && resp.user && !resp.user.realm_id && resp.user.superuser) {
        me.view
          .down("form")
          .down("[name=realm_id]")
          .setConfig("allowBlank", false)
          .enable();
      } else if (
        resp &&
        resp.user &&
        resp.user.realm_id &&
        data &&
        !data.ctime
      ) {
        me.view
          .down("form")
          .down("[name=realm_id]")
          .setConfig("allowBlank", false)
          .setValue(resp.user.realm_id)
          .disable();
      } else {
        me.view
          .down("form")
          .down("[name=realm_id]")
          .setConfig("allowBlank", false)
          .disable();
      }
      cb(data);
    });
  }
});
