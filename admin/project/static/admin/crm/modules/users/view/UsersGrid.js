Ext.define("Crm.modules.users.view.UsersGrid", {
  extend: "main.GridContainer",

  title: D.t("Admins"),
  iconCls: "x-fa fa-users",

  filterbar: true,
  filterable: true,

  requires: ["Core.grid.ComboColumn"],

  buildColumns: function() {
    return [
      {
        text: D.t("Login"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "login"
      },
      {
        xtype: "combocolumn",
        text: D.t("Group"),
        flex: 1,
        filter: true,
        // model: "Crm.modules.users.model.GroupsModel",
        dataIndex: "groupid",
        renderer: function(v, m, r) {
          if (v && r.data.groupName) {
            return (
              '<a href="#Crm.modules.users.view.GroupsForm~' +
              v +
              '">' +
              r.data.groupName +
              "</a>"
            );
          }
          return v;
        },
        filter: {
          xtype: "combo",
          valueField: "_id",
          displayField: "name",
          minChars: 1,
          store: Ext.create("Core.data.ComboStore", {
            storeId: "_id",
            dataModel: Ext.create("Crm.modules.users.model.GroupsModel"),
            fieldSet: ["name", "_id"],
            scope: this
          })
        }
      },
      {
        text: D.t("name"),
        flex: 1,
        filter: true,
        sortable: true,
        dataIndex: "name"
      },
      {
        text: D.t("Email"),
        flex: 1,
        filter: true,
        sortable: true,
        dataIndex: "email"
      },
      {
        text: D.t("Phone"),
        flex: 1,
        filter: true,
        sortable: true,
        dataIndex: "tel"
      },
      {
        text: D.t("Status"),
        flex: 1,
        filter: true,
        sortable: true,
        dataIndex: "status",
        filter: {
          xtype: "combo",
          valueField: "key",
          displayField: "label",
          minChars: 1,
          store: Ext.create("Ext.data.Store", {
            fields: ["key", "label"],
            data: [
              { key: "ACTIVE", label: "ACTIVE" },
              { key: "INACTIVE", label: "INACTIVE" },
              { key: "BLOCKED", label: "BLOCKED" }
            ]
          })
        }
      } /*,{
            text: D.t("Session password"),
            flex: 1,
            sortable: true,
            dataIndex: 'dblauth',
            renderer: function(v,m) {
                return D.t(v? 'on':'off')    
            }
        }*/
    ];
  }
});
