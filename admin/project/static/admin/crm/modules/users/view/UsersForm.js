Ext.define("Crm.modules.users.view.UsersForm", {
  extend: "Core.form.FormWindow",

  titleTpl: "User: {login}",

  requires: ["Ext.ux.form.ItemSelector", "Ext.form.field.Tag"],
  controllerCls: "Crm.modules.users.controller.UsersFormController",
  buildItems: function() {
    return [
      {
        name: "id",
        hidden: true
      },
      {
        name: "user_realm_id",
        hidden: true
      },
      {
        name: "login",
        fieldLabel: D.t("Username (login) *"),
        allowBlank: false,
        maxLength: 20,
        minLength: 1,
        maskRe: /[a-zA-Z0-9]/
      },
      {
        name: "name",
        fieldLabel: D.t("Full Name"),
        maxLength: 20,
        minLength: 1,
        allowBlank: false
      },
      {
        name: "tel",
        fieldLabel: D.t("Phone *"),
        allowBlank: false,
        maxLength: 15,
        minLength: 1
      },
      {
        name: "email",
        fieldLabel: D.t("Email *"),
        allowBlank: false,
        vtype: "email",
        maxLength: 255
      },
      {
        name: "pass",
        inputType: "password",
        fieldLabel: D.t("Password *"),
        allowBlank: false,
        maxLength: 80,
        minLength: 1
      },
      {
        xtype: "checkbox",
        name: "superuser",
        fieldLabel: D.t("Is Superuser")
      },
      {
        xtype: "combo",
        name: "status",
        fieldLabel: D.t("Status"),
        valueField: "key",
        displayField: "label",
        queryMode: "local",
        store: Ext.create("Ext.data.Store", {
          fields: ["key", "label"],
          data: [
            { key: "ACTIVE", label: "ACTIVE" },
            { key: "INACTIVE", label: "INACTIVE" },
            { key: "BLOCKED", label: "BLOCKED" }
          ]
        })
      },
      this.buildGroupCombo(),
      this.buildXGroups(),
      this.buildRealmCombo()
    ];
  },

  buildGroupCombo: function() {
    var me = this;
    return {
      xtype: "combo",
      name: "groupid",
      fieldLabel: D.t("Main Role"),
      valueField: "_id",
      displayField: "name",
      queryMode: "local",
      allowBlank: false,
      disabled: false,
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.users.model.GroupsModel"),
        fieldSet: ["_id", "name"],
        scope: me
      })
    };
  },
  buildRealmCombo: function() {
    var me = this;
    return {
      xtype: "combo",
      name: "realm_id",
      fieldLabel: D.t("Realm"),
      valueField: "id",
      displayField: "name",
      queryMode: "local",
      disabled: true,
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create("Crm.modules.realm.model.RealmModel"),
        fieldSet: ["id", "name"],
        scope: me
      })
    };
  },

  buildXGroups: function() {
    return {
      xtype: "tagfield",
      fieldLabel: D.t("Extended Permission Role"),
      store: Ext.create("Core.data.Store", {
        dataModel: "Crm.modules.users.model.GroupsModel",
        fieldSet: "_id,name"
      }),
      displayField: "name",
      valueField: "_id",
      queryMode: "local",
      name: "xgroups",
      disabled: false,
      filterPickList: true
    };
  },

  buildButtons: function() {
    var btns = [
      {
        text: D.t("Save and close"),
        iconCls: "x-fa fa-check-square-o",
        scale: "medium",
        action: "save",
        disabled: true
      },
      {
        text: D.t("Save"),
        iconCls: "x-fa fa-check",
        action: "apply",
        disabled: true
      },
      "-",
      { text: D.t("Close"), iconCls: "x-fa fa-ban", action: "formclose" }
    ];
    if (this.allowCopy)
      btns.splice(1, 0, {
        tooltip: D.t("Make a copy"),
        iconCls: "x-fa fa-copy",
        action: "copy"
      });
    return btns;
  }
});
