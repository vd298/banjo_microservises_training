Ext.define("Crm.modules.merchantCategories.view.MerchantCategoriesGrid", {
  extend: "Crm.modules.accountCategories.view.accountCategoriesGrid",

  title: D.t("Categories"),
  iconCls: "x-fa fa-users",
  filterable: false,
  filterbar: true,

  buildColumns: function() {
    return [
      {
        name: "id",
        hidden: true
      },
      {
        text: D.t("Categories"),
        flex: 1,
        sortable: true,
        dataIndex: "category_id",
        renderer: function(v, m, r) {
          if (r.data.category_name) return r.data.category_name;
          return "-";
        },
        filter: {
          xtype: "combo",
          valueField: "id",
          displayField: "name",
          minChars: 1,
          store: Ext.create("Core.data.ComboStore", {
            storeId: "id",
            dataModel: Ext.create("Crm.modules.categories.model.CategoryModel"),
            fieldSet: ["name", "id"],
            scope: this
          })
        }
      },
      {
        name: "account_id",
        dataIndex: "account_id",
        hidden: true
      }
    ];
  }
});
