Ext.define(
  "Crm.modules.merchantCategories.view.MerchantCategoriesFormController",
  {
    extend:
      "Crm.modules.accountCategories.view.accountCategoriesFormController",

    setControls: function() {
      this.control({
        "[action=formclose]": {
          click: () => {
            this.closeView();
          }
        },
        "[action=apply]": {
          click: () => {
            this.save(false, (res) => {
              this.closeView();
            });
          }
        },
        "[action=save]": {
          click: () => {
            this.save(true);
          }
        },
        "[action=remove]": {
          click: () => {
            this.deleteRecord_do(true);
          }
        },
        "[action=copy]": {
          click: () => {
            this.copyRecord(true);
          }
        },
        "[action=gotolist]": {
          click: () => {
            this.gotoListView(true);
          }
        },
        "[action=exportjson]": {
          click: () => {
            this.exportJson();
          }
        },
        "[action=importjson]": {
          change: (el) => {
            this.importJson();
          }
        }
      });

      this.view.on("activate", (grid, indx) => {
        if (!this.oldDocTitle) this.oldDocTitle = document.title;
        var form = this.view.down("form").getForm();
        if (form) {
          data = form.getValues();
          this.setTitle(data);
        }
      });
      this.view.on("close", (grid, indx) => {
        if (this.oldDocTitle) document.title = this.oldDocTitle;
      });

      this.view.on("disablePanels", (data, cmp) => {
        if (data.maker == undefined) {
          var tabPanel = Ext.getCmp(cmp);
          var items = tabPanel.items.items;

          for (var i = 1; i < items.length; i++) {
            var panel = items[i];
            panel.setDisabled(true);
          }
        }
      });

      this.view.down("form").on({
        validitychange: (th, valid, eOpts) => {
          var el = this.view.down("[action=apply]");
          if (el) el.setDisabled(!valid);
          el = this.view.down("[action=save]");
          if (el) el.setDisabled(!valid);
        }
      });
      this.checkPermissions();
    }
  }
);
