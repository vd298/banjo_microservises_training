Ext.define("Crm.modules.merchantCategories.view.MerchantCategoriesForm", {
  extend: "Crm.modules.accountCategories.view.accountCategoriesForm",

  titleTpl: "Category: {category_name}",
  controllerCls:
    "Crm.modules.merchantCategories.view.MerchantCategoriesFormController",

  buildItems: function() {
    return [
      {
        name: "id",
        hidden: true,
      },
      this.buildCategoryCombo(),
      {
        name: "account_id",
        hidden: true,
      },
    ];
  },
  buildCategoryCombo() {
    return {
      name: "category_id",
      xtype: "combo",
      fieldLabel: D.t("Category"),
      queryMode: "all",
      displayField: "name",
      editable: true,
      valueField: "id",
      validator: true,
      allowBlank: false,
      minChars: 1,
      store: Ext.create("Core.data.ComboStore", {
        storeId: "category_id",
        dataModel: Ext.create("Crm.modules.categories.model.CategoryModel"),
        fieldSet: ["name", "id"],
        scope: this,
      }),
    };
  },
  buildButtons: function() {
    var btns = [
      {
        tooltip: D.t("Remove this record"),
        iconCls: "x-fa fa-trash",
        action: "remove"
      },
      "->",
      { text: D.t("Save"), iconCls: "x-fa fa-check", action: "apply" },
      "-",
      { text: D.t("Close"), iconCls: "x-fa fa-ban", action: "formclose" }
    ];

    return btns;
  }
});
