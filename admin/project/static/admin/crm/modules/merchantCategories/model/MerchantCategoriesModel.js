Ext.define("Crm.modules.merchantCategories.model.MerchantCategoriesModel", {
  extend: "Crm.modules.accountCategories.model.accountCategoriesModel",
  async afterSave(data, cb) {
    const me = this;
    me.changeModelData(
      "Crm.modules.merchantCategories.model.MerchantCategoriesModel",
      "ins",
      data
    );
    cb(data);
  }
});
