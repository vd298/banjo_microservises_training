const config = require("@lib/config");

const URL = config.admin_url;

module.exports = {
  before(browser) {},

  after(browser) {},

  async Login(browser) {
    await browser.url(URL + "/admin/#authentication.login");

    await browser.waitForElementVisible("body");

    await browser.assert.visible("input[placeholder='User Name']");
    await browser.setValue("input[placeholder='User Name']", "yeti");

    await browser.assert.visible("input[type=password]");
    await browser.setValue("input[type=password]", "Passw0rd");

    await browser.assert.visible("a[id='button-1024']");

    await browser.pause(1000);
    await browser.click("a[id='button-1024']");
  }
};
