const config = require("@lib/config");

Ext.define("Crm.modules.letterTemplates.model.letterTemplatesModel", {
  extend: "Crm.classes.DataModel",
  type: "email",
  collection: "vw_letters",
  idField: "id",
  useTags: true,
  removedFilter: true,
  removeAction: "remove",
  foreignKeyFilter: [
    {
      collection: "letters",
      alias: "l",
      on: " letters.id = vw_letters.id ",
      type: "inner"
    }
  ],

  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "transporter",
      type: "ObjectID",
      sort: 1,
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "realm",
      type: "ObjectID",
      filterable: true,
      editable: true,
      visible: true,
      bindTo: {
        collection: "realms",
        keyFieldType: "ObjectID",
        keyField: "id",
        fields: {
          name: 1,
          id: 1
        }
      }
    },
    {
      name: "code",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "letter_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "subject",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "lang",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "text",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "from_email",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "to_email",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "html",
      type: "text",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "data",
      type: "text",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "sms_gateway",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "to_cc",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "to_bcc",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "send_email",
      type: "boolean",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "send_sms",
      type: "boolean",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "send_push",
      type: "boolean",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "to_mobile",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "sms_text",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "push_title",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "push_message",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "push_action",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "target_group",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "category",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "sub_category",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "channel",
      type: "integer",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    }
  ],
  async afterGetData(data, callback) {
    if (!data || !data.length) return callback(data);

    const transporters_id = data.map((item) => item.transporter).flat();
    if (!transporters_id || !transporters_id.length) return callback(data);
    const res = await this.src.db
      .collection("transporters")
      .findAll({ id: { $in: transporters_id } }, {});

    data.map((element) => {
      switch (element.channel) {
        case 0:
          element.channel = null;
          break;
        case 10:
          element.channel = config.notificationChannel["email"];
          break;
        case 7:
          element.channel = config.notificationChannel["sms"];
          break;
        case 15:
          element.channel = config.notificationChannel["email"];
          break;
        case 17:
          element.channel = config.notificationChannel["emailSms"];
          break;
        case 22:
          element.channel = config.notificationChannel["smsPush"];
          break;
        case 25:
          element.channel = config.notificationChannel["emailPush"];
          break;
        default:
          element.channel = config.notificationChannel["emailSmsPush"];
      }
    });
    for (transport of res) {
      data = data.map((item) => {
        if (item.transporter == transport.id)
          item.transporter_name = transport.name;
        return item;
      });
    }
    callback(data);
  },

  /* scope:server */
  buildWhere: function(params, callback) {
    params.filters = [
      {
        initialConfig: {
          property: "letter_type",
          value: this.type,
          type: "combo",
          operator: "eq"
        },
        config: {
          property: "letter_type",
          value: this.type,
          type: "combo",
          operator: "eq"
        },
        type: "combo",
        _property: "letter_type",
        _value: this.type,
        _disableOnEmpty: this.type,
        _operator: "eq",
        _root: "data",
        _id: "letter_type"
      }
    ].concat(params.filters);
    this.db.buildWhere(params, this, callback);
  },

  async beforeSave(data, cb) {
    const channels = {
      send_email: 10,
      send_sms: 7,
      send_push: 15
    };
    Object.keys(channels).forEach((item) => {
      data[item] = data[item] && data[item] === "on" ? channels[item] : 0;
    });
    cb(data);
  }
  // async beforeSave(data, cb) {
  //   if (!data.merchant) return cb(data);
  //   await this.src.db
  //     .collection("merchants")
  //     .update({ id: data.merchant }, { $set: { id_template: data.id } });
  //   cb(data);
  // }
});
