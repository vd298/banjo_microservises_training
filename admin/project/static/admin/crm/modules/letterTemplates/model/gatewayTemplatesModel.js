const config = require("@lib/config");

Ext.define("Crm.modules.letterTemplates.model.gatewayTemplatesModel", {
  extend: "Crm.modules.letterTemplates.model.smsTemplatesModel",
  type: "sms",

  /* scope:server */
  async getData(data, callback) {
    const me = this;
    let res = await this.callApi({
      service: "mail-service",
      method: "getSMSGatewayList",
      data: {},
      options: {
        realmId: me.user.profile.realm_id
      }
    });
    if (res) {
      res = [
        {
          key: config.automaticGateway["automaticKey"],
          value: config.automaticGateway["automaticValue"]
        }
      ].concat(res);
      callback({ count: res.length, list: res });
    } else {
      callback(res);
    }
  }
});
