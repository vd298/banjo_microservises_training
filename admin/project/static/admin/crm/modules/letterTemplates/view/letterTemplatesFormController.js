Ext.define("Crm.modules.letterTemplates.view.letterTemplatesFormController", {
  extend: "Core.form.FormController",
  fields: ["html", "text", "subject", "sms_text", "push_title", "push_message"],
  setControls() {
    const controls = {};
    this.fields.map((key) => {
      controls[`[name=${key}]`] = {
        change: () => {
          this.changeHtml();
        }
      };
    });
    this.control(controls);
    this.fields.map((type) =>
      this.view[type + "PreviewPanel"].on("activate", () => {
        setTimeout(() => {
          this.buildPreview();
        }, 1000);
      })
    );
    this.fields.map((type) =>
      this.view[type + "PreviewPanel"].on("afterrender", () => {
        setTimeout(() => {
          this.buildPreview();
        }, 1000);
      })
    );
    this.callParent(arguments);
  },

  changeHtml() {
    if (this.tout) clearTimeout(this.tout);
    this.tout = setTimeout(() => {
      this.buildPreview();
    }, 2000);
  },

  async buildPreview() {
    const data = this.view.down("form").getValues();
    const sourceField = {
      html: data.html,
      text: data.text,
      subject: data.subject,
      sms_text: data.sms_text,
      push_title: data.push_title,
      push_message: data.push_message
    };
    if (!Object.keys(sourceField).map((item) => sourceField[item])) return;
    try {
      JSON.parse(data.data);
    } catch (e) {
      this.view.htmlPreviewPanel
        .getEl()
        .down("iframe", true).contentWindow.document.body.innerHTML = e.message;
      return;
    }
    const d = JSON.parse(data.data);
    let mailServiceObject = { data: d};
    Object.assign(mailServiceObject, sourceField)
    const res = await this.model.callApi("mail-service", "preview", mailServiceObject);
    this.fields.forEach((type) => {
      try {
        if (res && res[type]) {
          this.view[type + "PreviewPanel"]
            .getEl()
            .down("iframe", true).contentWindow.document.body.innerHTML =
            res[type];
        }
      } catch (error) {
        console.log(error);
      }
    });
  },

  setValues(data) {
    if (data && data.realm) data.realm = data.realm.id;
    this.callParent(arguments);
  }
});
