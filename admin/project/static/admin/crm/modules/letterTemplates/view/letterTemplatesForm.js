Ext.define("Crm.modules.letterTemplates.view.letterTemplatesForm", {
  extend: "Core.form.FormWindow",

  titleTpl: D.t("Template: {letter_name}"),
  iconCls: "x-fa fa-envelope-o",
  requires: ["Desktop.core.widgets.GridField"],
  formLayout: "fit",
  allowClone: true,
  formMargin: 0,
  allowImportExport: true,
  fetchSubCategory(category) {
    const subcategory = {
      1000: [
        { code: "100", name: "Registration & Verification (100)" },
        { code: "200", name: "Access the App (200)" }
      ],
      2000: [
        { code: "100", name: "Top Up (100)" },
        { code: "200", name: "Transaction Statuses (200)" },
        { code: "300", name: "OTP Confirmation (300)" }
      ],
      3000: [{ code: "100", name: "Tickets (100)" }],
      4000: [
        { code: "100", name: "Product (100)" },
        { code: "200", name: "Terms (200)" },
        { code: "300", name: "Company News (300)" },
        {
          code: "400",
          name: "Promotion Campaigns & Marketing Activities (400)"
        }
      ],
      5000: [
        { code: "100", name: "Accountant (100)" },
        { code: "200", name: "MLRO (200)" },
        { code: "300", name: "Tech Team (300)" },
        { code: "400", name: "Management (400)" },
        { code: "900", name: "CRM salesforce (900)" }
      ]
    };
    return subcategory[category];
  },
  controllerCls:
    "Crm.modules.letterTemplates.view.letterTemplatesFormController",

  buildItems() {
    return {
      xtype: "tabpanel",
      layout: "fit",
      items: [
        this.buildGeneral(),
        this.emailGeneral(),
        this.smsGeneral(),
        this.pushGeneral()
      ]
    };
  },

  pushGeneral() {
    this.push_titlePreviewPanel = Ext.create("Ext.panel.Panel", {
      html:
        '<iframe id="htmls" width="100%" height="100%" frameborder="0" style="border: null"></iframe>'
    });
    this.push_messagePreviewPanel = Ext.create("Ext.panel.Panel", {
      html:
        '<iframe width="100%" id="texts" height="100%" frameborder="0" style="border: null"></iframe>'
    });

    return {
      xtype: "panel",
      title: D.t("Push Notification"),
      layout: "border",
      bodyStyle: "background: #ffffff",
      items: [
        {
          xtype: "fieldcontainer",
          layout: "anchor",
          region: "center",
          items: [
            {
              xtype: "panel",
              layout: "anchor",
              split: true,
              region: "north",
              height: 100,
              scrollable: true,
              defaults: {
                anchor: "100%",
                xtype: "textfield",
                margin: 5,
                labelWidth: 110
              },
              items: [
                {
                  xtype: "checkbox",
                  name: "send_push",
                  width: 100,
                  fieldLabel: D.t("Send Push Notification"),
                  listeners: {
                    change: (el, v) => {
                      this.down("[name=push_action]").setVisible(v == 1);
                      this.down("[name=push_action]").setDisabled(!v);
                    }
                  }
                },
                {
                  name: "push_action",
                  disabled: true,
                  fieldLabel: D.t("Action")
                }
              ]
            },
            {
              xtype: "tabpanel",
              layout: "fit",
              tabBarPosition: "bottom",
              region: "center",
              items: [
                {
                  xtype: "panel",
                  title: D.t("Title"),
                  layout: "fit",
                  items: {
                    xtype: "textarea",
                    name: "push_title",
                    labelWidth: 150,
                    height: 500,
                    style: "background:#ffffff",
                    emptyText: D.t("Template (PUG format)")
                  }
                },
                {
                  xtype: "panel",
                  title: D.t("Message"),
                  layout: "fit",
                  items: {
                    xtype: "textarea",
                    name: "push_message",
                    labelWidth: 150,
                    height: 500,
                    style: "background:#ffffff",
                    emptyText: D.t("Template (PUG format)")
                  }
                }
              ]
            }
          ]
        },
        {
          xtype: "tabpanel",
          region: "east",
          width: "50%",
          split: true,
          items: [
            {
              xtype: "panel",
              title: D.t("Title Preview"),
              layout: "fit",
              items: this.push_titlePreviewPanel
            },
            {
              xtype: "panel",
              title: D.t("Message Preview"),
              layout: "fit",
              items: this.push_messagePreviewPanel
            }
          ]
        }
      ]
    };
  },

  smsGeneral() {
    this.sms_textPreviewPanel = Ext.create("Ext.panel.Panel", {
      html:
        '<iframe id="htmls" width="100%" height="100%" frameborder="0" style="border: null"></iframe>'
    });
    return {
      xtype: "panel",
      title: D.t("SMS"),
      layout: "border",
      bodyStyle: "background: #ffffff",
      items: [
        {
          xtype: "fieldcontainer",
          layout: "anchor",
          region: "center",
          items: [
            {
              xtype: "panel",
              layout: "anchor",
              split: true,
              region: "north",
              height: 100,
              scrollable: true,
              defaults: {
                anchor: "100%",
                xtype: "textfield",
                margin: 5,
                labelWidth: 110
              },
              items: [
                {
                  xtype: "checkbox",
                  name: "send_sms",
                  width: 100,
                  fieldLabel: D.t("Send SMS"),
                  listeners: {
                    change: (el, v) => {
                      this.down("[name=sms_gateway]").setDisabled(!v);
                      this.down("[name=to_mobile]").setDisabled(!v);
                    }
                  }
                },
                {
                  name: "to_mobile",
                  disabled: true,
                  fieldLabel: D.t("Mobile")
                },
                {
                  xtype: "combo",
                  name: "sms_gateway",
                  fieldLabel: D.t("Gateways"),
                  valueField: "key",
                  disabled: true,
                  displayField: "value",
                  queryMode: "local",
                  flex: 1,
                  store: Ext.create("Core.data.ComboStore", {
                    dataModel: Ext.create(
                      "Crm.modules.letterTemplates.model.gatewayTemplatesModel"
                    ),
                    fieldSet: ["key", "value"],
                    scope: this
                  })
                }
              ]
            },
            {
              xtype: "tabpanel",
              layout: "fit",
              tabBarPosition: "bottom",
              region: "center",
              items: [
                {
                  xtype: "panel",
                  title: D.t("Text"),
                  layout: "fit",
                  items: {
                    xtype: "textarea",
                    name: "sms_text",
                    labelWidth: 150,
                    height: 600,
                    style: "background:#ffffff",
                    emptyText: D.t("Template (PUG format)")
                  }
                }
              ]
            }
          ]
        },
        {
          xtype: "tabpanel",
          region: "east",
          width: "50%",
          split: true,
          items: [
            {
              xtype: "panel",
              title: D.t("Text Preview"),
              layout: "fit",
              items: this.sms_textPreviewPanel
            }
          ]
        }
      ]
    };
  },

  emailGeneral() {
    this.htmlPreviewPanel = Ext.create("Ext.panel.Panel", {
      name: "htmlPreviewPanel1",
      html:
        '<iframe id="htmls" width="100%" height="100%" frameborder="0" style="border: null"></iframe>'
    });
    this.textPreviewPanel = Ext.create("Ext.panel.Panel", {
      html:
        '<iframe width="100%" id="texts" height="100%" frameborder="0" style="border: null"></iframe>'
    });
    this.subjectPreviewPanel = Ext.create("Ext.panel.Panel", {
      html:
        '<iframe width="100%" id="subjects" height="100%" frameborder="0" style="border: null"></iframe>'
    });

    return {
      xtype: "panel",
      title: D.t("Email"),
      layout: "border",
      scrollable: true,
      bodyStyle: "background: #ffffff",
      items: [
        {
          xtype: "fieldcontainer",
          layout: "anchor",
          region: "center",
          items: [
            {
              xtype: "panel",
              layout: "anchor",
              split: true,
              region: "north",
              height: 200,
              defaults: {
                anchor: "100%",
                xtype: "textfield",
                margin: 5,
                labelWidth: 110
              },
              items: [
                {
                  xtype: "checkbox",
                  name: "send_email",
                  width: 100,
                  fieldLabel: D.t("Send Email"),
                  listeners: {
                    change: (el, v) => {
                      this.down("[name=subject]").setDisabled(!v);
                      this.down("[name=from_email]").setDisabled(!v);
                      this.down("[name=to_email]").setDisabled(!v);
                      this.down("[name=to_cc]").setDisabled(!v);
                      this.down("[name=to_bcc]").setDisabled(!v);
                      this.down("[name=transporter]").setDisabled(!v);
                    }
                  }
                },
                {
                  name: "subject",
                  disabled: true,
                  fieldLabel: D.t("Subject")
                },
                {
                  name: "from_email",
                  disabled: true,
                  fieldLabel: D.t("From email")
                },
                {
                  name: "to_email",
                  disabled: true,
                  fieldLabel: D.t("To email")
                },
                {
                  name: "to_cc",
                  disabled: true,
                  fieldLabel: D.t("CC")
                },
                {
                  name: "to_bcc",
                  disabled: true,
                  fieldLabel: D.t("BCC")
                },
                {
                  xtype: "combo",
                  name: "transporter",
                  fieldLabel: D.t("Transporter"),
                  valueField: "id",
                  disabled: true,
                  displayField: "name",
                  queryMode: "local",
                  flex: 1,
                  store: Ext.create("Core.data.ComboStore", {
                    dataModel: Ext.create(
                      "Crm.modules.transporters.model.TransporterModel"
                    ),
                    fieldSet: ["id", "name"],
                    scope: this
                  })
                }
              ]
            },
            {
              xtype: "tabpanel",
              layout: "fit",
              tabBarPosition: "bottom",
              region: "center",
              items: [
                {
                  xtype: "panel",
                  title: D.t("HTML"),
                  layout: "fit",
                  items: {
                    xtype: "textarea",
                    name: "html",
                    labelWidth: 150,
                    height: 500,
                    style: "background:#ffffff",
                    emptyText: D.t("Template (PUG format)")
                  }
                },
                {
                  xtype: "panel",
                  title: D.t("Text"),
                  layout: "fit",
                  items: {
                    xtype: "textarea",
                    name: "text",
                    labelWidth: 150,
                    height: 500,
                    style: "background:#ffffff",
                    emptyText: D.t("Template (PUG format)")
                  }
                }
              ]
            }
          ]
        },
        {
          xtype: "tabpanel",
          region: "east",
          width: "50%",
          split: true,
          items: [
            {
              xtype: "panel",
              title: D.t("HTML Preview"),
              layout: "fit",
              items: this.htmlPreviewPanel
            },
            {
              xtype: "panel",
              title: D.t("Text Preview"),
              layout: "fit",
              items: this.textPreviewPanel
            },
            {
              xtype: "panel",
              title: D.t("Subject Preview"),
              layout: "fit",
              items: this.subjectPreviewPanel
            }
          ]
        }
      ]
    };
  },

  buildGeneral() {
    var me = this;
    return {
      title: D.t("General"),
      xtype: "panel",
      padding: 5,
      scrollable: true,
      layout: "anchor",
      defaults: { xtype: "textfield", labelWidth: 150, anchor: "100%" },
      items: [
        {
          name: "id",
          hidden: true
        },
        {
          xtype: "fieldcontainer",
          layout: "hbox",
          items: [
            {
              flex: 1,
              xtype: "textfield",
              name: "letter_name",
              labelWidth: 150,
              margin: "0 10 0 0",
              fieldLabel: D.t("Event Name")
            },
            {
              flex: 1,
              xtype: "textfield",
              name: "code",
              labelWidth: 100,
              fieldLabel: D.t("Event Code")
            }
          ]
        },
        {
          xtype: "fieldcontainer",
          layout: "hbox",
          items: [
            Ext.create("Crm.modules.realm.view.ParentRealmCombo", {
              name: "realm",
              flex: 1,
              width: 400,
              labelWidth: 150,
              margin: "0 20 0 0"
            }),
            {
              xtype: "combo",
              width: 250,
              margin: "0 20 0 0",
              labelWidth: 80,
              name: "lang",
              fieldLabel: D.t("Language"),
              displayField: "code",
              valueField: "code",
              value: "en",
              store: {
                fields: ["code"],
                data: [{ code: "en" }, { code: "ru" }]
              }
            },
            {
              xtype: "combo",
              width: 350,
              labelWidth: 80,
              name: "target_group",
              fieldLabel: D.t("Target Group"),
              displayField: "target_group",
              valueField: "target_group",
              value: "",
              store: {
                fields: ["target_group"],
                data: [{ target_group: "CUSTOMER" }, { target_group: "AGENT" }]
              }
            }
          ]
        },

        {
          xtype: "fieldcontainer",
          layout: "hbox",
          items: [
            {
              flex: 1,
              xtype: "combo",
              name: "category",
              labelWidth: 150,
              margin: "0 10 0 0",
              fieldLabel: D.t("Category"),
              store: {
                fields: ["code", "name"],
                data: [
                  { code: "1000", name: "User Profile (1000)" },
                  { code: "2000", name: "Payment Accounts (2000)" },
                  { code: "3000", name: "Support (3000)" },
                  { code: "4000", name: "News and Announcements (4000)" },
                  { code: "5000", name: "Support Team (5000)" }
                ]
              },
              valueField: "code",
              displayField: "name",
              listeners: {
                change: (el, v) => {
                  const store = Ext.create("Ext.data.Store", {
                    fields: ["code", "name"],
                    data: me.fetchSubCategory(v)
                  });
                  me.down("[name=sub_category]").setStore(store);
                }
              }
            },
            {
              flex: 1,
              xtype: "combo",
              name: "sub_category",
              labelWidth: 150,
              margin: "0 10 0 0",
              fieldLabel: D.t("Sub Category"),
              valueField: "code",
              displayField: "name"
            }
          ]
        },
        {
          xtype: "textarea",
          width: "100%",
          height: 500,
          name: "data",
          style: "background:#ffffff",
          emptyText: D.t("Data example")
        }
      ]
    };
  }
});
