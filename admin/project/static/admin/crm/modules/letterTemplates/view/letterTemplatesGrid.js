Ext.define("Crm.modules.letterTemplates.view.letterTemplatesGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Letter Templates"),
  iconCls: "x-fa fa-envelope-o",

  filterable: true,
  filterbar: true,

  buildColumns: function() {
    return [
      {
        text: D.t("Code"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "code"
      },
      {
        text: D.t("Language"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "lang"
      },
      {
        text: D.t("Event Name"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "letter_name"
      },
      {
        text: D.t("Subject"),
        flex: 1,
        sortable: true,
        hidden: this.type === "email" ? false : true,
        filter: true,
        dataIndex: "subject"
      },

      {
        text: D.t("Transporter"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "transporter",
        renderer: (v, m, r) => {
          return r.data ? r.data.transporter_name : "";
        }
      },
      {
        text: D.t("Realm"),
        flex: 1,
        sortable: true,
        filter: Ext.create("Crm.modules.realm.view.RealmCombo", {
          name: null,
          fieldLabel: null
        }),
        dataIndex: "realm",
        renderer: (v, m, r) => {
          return v ? v.name : "";
        }
      },
      {
        text: D.t("Channel"),
        flex: 1,
        sortable: true,
        filter: {
          xtype: "combo",
          store: {
            fields: ["name", "code"],
            data: [
              { code: 10, name: "Email" },
              { code: 7, name: "SMS" },
              { code: 15, name: "Push" },
              { code: 17, name: "Email & SMS" },
              { code: 25, name: "Email & Push" },
              { code: 22, name: "SMS & Push" },
              { code: 32, name: "Email & SMS & Push" }
            ]
          },
          valueField: "code",
          displayField: "name"
        },
        dataIndex: "channel"
      },
      {
        text: D.t("send email"),
        flex: 1,
        sortable: true,
        filter: true,
        hidden: true,
        dataIndex: "send_email"
      },
      {
        text: D.t("send sms"),
        flex: 1,
        sortable: true,
        filter: true,
        hidden: true,
        dataIndex: "send_sms"
      },
      {
        text: D.t("send push"),
        flex: 1,
        sortable: true,
        filter: true,
        hidden: true,
        dataIndex: "send_push"
      },
      {
        text: D.t("Category"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "category"
      },
      {
        text: D.t("Sub Category"),
        flex: 1,
        sortable: true,
        filter: true,
        hidden: true,
        dataIndex: "sub_category"
      }
    ];
  }
});
