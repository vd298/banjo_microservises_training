Ext.define("Crm.modules.paymentProtocols.view.PaymentProtocolsGrid", {
    extend: "Core.grid.GridContainer",
  
    title: D.t("Payment Protocols"),
  
    filterable: true,
    filterbar: true,
  
    buildColumns: function() {
      return [
        {
          text: D.t("Type"),
          flex: 1,
          sortable: true,
          filter: {
            xtype: "combo",
            queryMode: "local",
            forceSelection: true,
            triggerAction: "all",
            editable: true,
            valueField: "protocol_type",
            displayField: "protocol_type",
            store:  Ext.create("Core.data.ComboStore", {
              dataModel: Ext.create(
                "Crm.modules.paymentProtocols.model.protocolTypeCombo"
              ),
              fieldSet: ["protocol_type","idprotocol_type"],
              scope: this
            }),
            operator: "eq"
          },  
          dataIndex: "protocol_type"
        },
        {
          text: D.t("Identifier Label "),
          flex: 1,
          sortable: true,
          filter: true,
          dataIndex: "identifier_label"
        },
        {
          text: D.t("Account Label"),
          flex: 1,
          sortable: true,
          filter: true,
          dataIndex: "account_label"
        },
      ];
    }
  });
  