Ext.define("Crm.modules.paymentProtocols.view.PaymentProtocolsForm", {
  extend: "Core.form.FormWindow",

  titleTpl: D.t("Payment Protocols"),
  requires: ["Desktop.core.widgets.GridField", "Ext.form.field.HtmlEditor"],
  formLayout: "fit",

  formMargin: 0,

  width: 450,
  height: 450,
  syncSize: function() {},
  controllerCls:
    "Crm.modules.paymentProtocols.view.PaymentProtocolsFormController",
  buildItems() {
    return {
      xtype: "panel",
      layout: "anchor",
      title: D.t("General"),
      defaults: {
        anchor: "100%",
        xtype: "textfield",
        labelWidth: 150,
        margin: 5
      },
      items: [
        {
          name: "id",
          hidden: true
        },
        {
          name: "account_label",
          fieldLabel: D.t("Account Label"),
          allowBlank: false,
          maxLength: 20
        },
        {
          name: "identifier_label",
          fieldLabel: D.t("Identifier Label"),
          allowBlank: false,
          maxLength: 20
        },
        {
          name: "transaction_identifier_label",
          fieldLabel: D.t("Transaction Identifier Label"),
          allowBlank: false,
          maxLength: 20
        },
        this.buildUserRiskTypeCombo(),
        {
          xtype: "htmleditor",
          name: "instruction",
          fieldLabel: D.t("Instruction"),
          allowBlank: false
        }
      ]
    };
  },
  buildUserRiskTypeCombo() {
    return {
      name: "protocol_type",
      fieldLabel: D.t("Protocol Type"),
      xtype: "combo",
      editable: true,
      allowBlank: false,
      validateBlank: true,
      validateOnChange: true,
      forceSelection: true,
      queryMode: "local",
      displayField: "protocol_type",
      flex: 1,
      valueField: "protocol_type",
      store: Ext.create("Core.data.ComboStore", {
        dataModel: Ext.create(
          "Crm.modules.paymentProtocols.model.protocolTypeCombo"
        ),
        fieldSet: ["protocol_type"],
        scope: this
      }),
      listeners: {
        afterrender: function(context, eOpts) {
          context.isValid();
        }
      }
    };
  }
});
