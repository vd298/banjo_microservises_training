Ext.define("Crm.modules.paymentProtocols.view.PaymentProtocolsFormController", {
  extend: "Core.form.FormController",
  setControls() {
    this.control({
      "[name=identifier_label]": {
        change: (e, newVal, oldVal) => {
          const me = this;
          const formData = me.view.down("form").getValues();
          if (formData.identifier_label.trim().length < 1) {
            me.view.down("[name=identifier_label]").setValue("");
            e.validator = function(value) {
              return "White space not allowed";
            };
          }

          this.model.runOnServer("uniqueProtocolEntries", formData, function(
            res
          ) {
            if (!res.result) {
              e.validator = function(value) {
                // save.setConfig("disabled", true);
                return res.message;
              };
            } else {
              e.validator = function(value) {
                // save.setConfig("disabled", false);
                return true;
              };
            }
            me.view.down("form").isValid();
          });
        }
      }
    });
    this.callParent(arguments);
  }
});
