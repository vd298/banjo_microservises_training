Ext.define("Crm.modules.paymentProtocols.model.protocolTypeCombo", {
    extend: "Crm.modules.paymentProtocols.model.PaymentProtocolsModel",
  
    getData: async function(params, cb) {
      const res =await this.src.db.query(
               "SELECT unnest(enum_range(NULL::enum_payment_protocols_protocol_type)) as protocol_type ;",
        function(err, resp) {
          if (err) {
            console.error(
              "UserRiskTypesCombo.js. Func:getData. Database Error. Error",
              err
            );
            return cb({ total: 0, list: {} });
          }
          return cb({ total: resp.length, list: resp.rows });
        }
      );
    }
  });
  