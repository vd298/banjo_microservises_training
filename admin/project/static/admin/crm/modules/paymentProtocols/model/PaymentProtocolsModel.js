Ext.define("Crm.modules.paymentProtocols.model.PaymentProtocolsModel", {
  extend: "Core.data.DataModel",

  collection: "payment_protocols",
  idField: "id",
  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "protocol_type",
      type: "enumstring",
      sort: 1,
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "identifier_label",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "transaction_identifier_label",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "account_label",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "instruction",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    }
  ],
  /* scope:server */
  async beforeSave(data, cb) {
    data.identifier_label = data.identifier_label.trim();
    cb(data);
  },
  /* scope:server */
  async $uniqueProtocolEntries(data, cb) {
    data.identifier_label =
      data.identifier_label && data.identifier_label.trim();
    let sql = `select * from payment_protocols where removed=0 and lower(identifier_label)=lower($1) and id!=($2)`;
    sql = await this.buildRealmConditionQuery(sql);
    const res = await this.src.db.query(sql, [data.identifier_label, data.id]);
    if (res && res.length) {
      return cb({
        result: false,
        message: "Identifier Code for payment protocol must be unique"
      });
    } else {
      return cb({ result: true });
    }
  }
});
