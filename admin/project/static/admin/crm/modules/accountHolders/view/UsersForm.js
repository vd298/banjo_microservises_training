Ext.define("Crm.modules.accountHolders.view.UsersForm", {
  extend: "Core.form.FormWindow",

  titleTpl: D.t(
    "User: {first_name} {last_name} ({[values.active? 'Enabled':'Disabled']})"
  ),
  iconCls: "x-fa fa-user",
  requires: [
    "Desktop.core.widgets.GridField",
    "Core.form.DateField",
    "Core.form.DependedCombo"
  ],
  formLayout: "fit",

  formMargin: 0,

  controllerCls: "Crm.modules.accountHolders.view.UsersFormController",

  buildItems() {
    return {
      xtype: "tabpanel",
      layout: "fit",
      items: [
        this.buildGeneral(),
        this.buildMerchants(),
        this.buildTariffPanel(),
        //this.buildKyc(),
        //this.buildIBANS(),
        this.buildNotifications(),
        this.buildNotes(),
        this.buildIPs()
      ]
    };
  },

  buildGeneral() {
    this.accountsGrid = Ext.create(
      "Crm.modules.accounts.view.UserAccountsGrid",
      {
        title: null,
        iconCls: null,
        scope: this,
        observe: [{ property: "owner", param: "id" }]
      }
    );
    return {
      xtype: "panel",
      title: D.t("General"),
      layout: "border",
      items: [
        {
          xtype: "panel",
          region: "center",
          layout: "border",
          items: [
            {
              xtype: "panel",
              layout: "anchor",
              split: true,
              region: "north",
              height: 295,

              defaults: {
                anchor: "100%",
                xtype: "textfield",
                margin: 5,
                labelWidth: 110
              },
              items: this.buildUserProfileFields()
            },
            {
              xtype: "panel",
              layout: "fit",
              region: "center",
              cls: "grayTitlePanel",
              title: D.t("Accounts"),
              items: this.accountsGrid
            }
          ]
        },
        {
          xtype: "panel",
          region: "east",
          cls: "grayTitlePanel",
          width: "50%",
          layout: "fit",
          split: true,
          title: D.t("Transactions"),
          items: Ext.create(
            "Crm.modules.transactions.view.UserTransactionsGrid",
            {
              scope: this,
              observe: [{ property: "user_id", param: "id" }]
            }
          )
        }
      ]
    };
  },

  buildUserProfileFields() {
    return [
      {
        name: "id",
        hidden: true
      },

      {
        xtype: "fieldcontainer",
        layout: "hbox",
        defaults: {
          flex: 1
        },
        items: [
          this.buildTypeCombo(),
          {
            xtype: "textfield",
            name: "legalname",
            //margin: "0 3 0 0",
            fieldLabel: D.t("Legal Name")
          }
        ]
      },
      {
        xtype: "fieldcontainer",
        layout: "hbox",
        defaults: {
          flex: 1
        },
        items: [this.buildRealmCombo(), this.buildCountryCombo()]
      },
      {
        xtype: "fieldcontainer",
        layout: "hbox",
        defaults: {
          xtype: "textfield",
          flex: 1
        },
        items: [
          {
            name: "login",
            margin: "0 3 0 0",
            fieldLabel: D.t("Login")
          },
          {
            name: "keyword",
            margin: "0 0 0 3",
            fieldLabel: D.t("Keyword")
          }
        ]
      },
      {
        xtype: "fieldcontainer",
        layout: "hbox",
        defaults: {
          xtype: "textfield",
          flex: 1
        },
        items: [
          {
            name: "first_name",
            margin: "0 3 0 0",
            fieldLabel: D.t("First name")
          },
          {
            name: "last_name",
            margin: "0 0 0 3",
            fieldLabel: D.t("Last name")
          }
        ]
      },
      {
        xtype: "fieldcontainer",
        layout: "hbox",
        defaults: {
          xtype: "textfield",
          flex: 1
        },
        items: [
          {
            name: "email",
            margin: "0 3 0 0",
            fieldLabel: D.t("E-mail")
          },
          {
            name: "phone",
            margin: "0 0 0 3",
            fieldLabel: D.t("Phone")
          }
        ]
      },
      {
        xtype: "fieldcontainer",
        layout: "hbox",
        defaults: {
          xtype: "xdatefield",
          submitFormat: "Y-m-d",
          format: D.t("m/d/Y"),
          flex: 1
        },
        items: [
          {
            name: "birthday",
            margin: "0 3 0 0",
            fieldLabel: D.t("Birth Day")
          },
          {
            name: "ctime",
            margin: "0 0 0 3",
            fieldLabel: D.t("Reg. date")
          }
        ]
      },
      this.buildAddress("addr1", "Address 1"),
      this.buildAddress("addr2", "Address 2"),
      this.buildInvoiceCombo(),
      {
        xtype: "fieldcontainer",
        layout: "hbox",
        items: [
          {
            xtype: "combo",
            name: "otp_transport",
            flex: 1,
            labelWidth: 110,
            fieldLabel: D.t("Notification chanel"),
            valueField: "name",
            displayField: "name",
            store: {
              fields: ["name"],
              data: [{ name: "telegram" }, { name: "email" }, { name: "test" }]
            }
          },
          {
            xtype: "checkbox",
            name: "kyc",
            width: 100,
            margin: "0 0 0 10",
            labelWidth: 70,
            fieldLabel: D.t("KYC")
          },
          {
            xtype: "checkbox",
            name: "activated",
            width: 100,
            margin: "0 0 0 10",
            labelWidth: 70,
            fieldLabel: D.t("Activated")
          }
        ]
      }
    ];
  },

  buildNotes() {
    return {
      xtype: "panel",
      title: D.t("Notes"),
      layout: "anchor",
      padding: 5,
      items: [
        {
          xtype: "textfield",
          anchor: "100%",
          name: "notes"
        }
      ]
    };
  },

  buildKyc() {
    return {
      xtype: "panel",
      title: D.t("KYC"),
      layout: "border",
      items: [
        {
          xtype: "panel",
          region: "center",
          layout: "border",
          items: [
            {
              xtype: "panel",
              layout: "anchor",
              region: "north",
              height: 325,
              cls: "grayTitlePanel",
              items: Ext.create("Crm.modules.kyc.view.ProfileGrid", {
                scope: this,
                observe: [{ property: "user_id", param: "id" }]
              })
            },
            {
              xtype: "panel",
              layout: "fit",
              region: "center",
              cls: "grayTitlePanel",
              items: Ext.create("Crm.modules.kyc.view.AddressGrid", {
                scope: this,
                observe: [{ property: "user_id", param: "id" }]
              })
            }
          ]
        },
        {
          xtype: "panel",
          region: "east",
          cls: "grayTitlePanel",
          width: "50%",
          layout: "fit",
          split: true,
          items: Ext.create("Crm.modules.kyc.view.CompanyGrid", {
            scope: this,
            observe: [{ property: "user_id", param: "id" }]
          })
        }
      ]
    };
  },

  buildAddress(name, text) {
    return {
      xtype: "fieldcontainer",
      layout: "hbox",
      fieldLabel: D.t(text),
      defaults: {
        xtype: "textfield"
      },
      items: [
        {
          name: `${name}_zip`,
          margin: "0 3 0 0",
          width: 100,
          emptyText: D.t("ZIP")
        },
        {
          name: `${name}_address`,
          margin: "0 0 0 3",
          flex: 1,
          emptyText: D.t("city, street, bld.")
        }
      ]
    };
  },

  buildTypeCombo() {
    return {
      fieldLabel: D.t("User type"),
      xtype: "combo",
      name: "type",
      margin: "0 3 0 0",
      store: {
        fields: ["code", "name"],
        data: [
          { code: 0, name: D.t("Personal user") },
          { code: 1, name: D.t("Company") }
        ]
      },
      valueField: "code",
      displayField: "name",
      listeners: {
        change: (el, v) => {
          this.down("[name=legalname]").setDisabled(v != 1);
        }
      }
    };
  },

  buildRealmCombo() {
    return Ext.create("Crm.modules.realm.view.RealmCombo", {
      name: "realm",
      margin: "0 3 0 0"
    });
  },

  buildInvoiceCombo() {
    return {
      xtype: "dependedcombo",
      name: "invoice_tpl",
      fieldLabel: D.t("Invoice type"),
      valueField: "id",
      margin: "0 5 0 5",
      displayField: "name",
      dataModel: "Crm.modules.invoiceTemplates.model.InvoiceTemplatesModel",
      fieldSet: "id,name"
    };
  },

  buildCountryCombo() {
    return {
      xtype: "dependedcombo",
      name: "country",
      fieldLabel: D.t("Country"),
      valueField: "abbr2",
      margin: "0 0 0 3",
      displayField: "name",
      dataModel: "Crm.modules.settings.model.CountriesModel",
      fieldSet: "abbr2,name"
    };
  },
  buildTariffPanel() {
    return Ext.create("Crm.modules.tariffs.view.TariffSettingsPanel");
  },

  buildIBANS() {
    return {
      xtype: "panel",
      layout: "fit",
      title: D.t("IBANs"),
      items: Ext.create("Crm.modules.banks.view.IBANGrid", {
        scope: this,
        observe: [{ property: "owner", param: "id" }]
      })
    };
  },

  buildNotifications() {
    return {
      xtype: "panel",
      layout: "fit",
      title: D.t("Notifications"),
      items: Ext.create("Crm.modules.notifications.view.NotificationsGrid", {
        scope: this,
        observe: [{ property: "user_id", param: "id" }]
      })
    };
  },

  buildMerchants() {
    return {
      xtype: "panel",
      layout: "fit",
      title: D.t("Merchants"),
      items: Ext.create("Crm.modules.merchants.view.MerchantsGrid", {
        scope: this,
        observe: [{ property: "user_id", param: "id" }]
      })
    };
  },

  buildIPs() {
    return {
      xtype: "panel",
      title: D.t("IP address"),

      xtype: "gridfield",
      hideLabel: true,
      region: "center",
      name: "ip",
      layout: "fit",
      fields: ["ip"],
      columns: [
        {
          text: D.t("Address"),
          flex: 1,
          sortable: false,
          dataIndex: "ip",
          menuDisabled: true,
          editor: true
        }
      ]
    };
  }
});
