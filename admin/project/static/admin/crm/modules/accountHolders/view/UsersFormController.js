Ext.define("Crm.modules.accountHolders.view.UsersFormController", {
  extend: "Core.form.FormController",

  setControls() {
    this.control({
      "[name=trigger]": {
        change: (el, v) => {
          this.onTriggerChange(v);
        }
      },
      "[action=balance_currency]": {
        change: (el, v) => {
          this.showBalance(v);
        }
      }
    });

    this.callParent(arguments);
  },

  setValues(data) {
    if (data && data.realm) data.realm = data.realm.id;
    this.callParent(arguments);
    this.showBalance("USD");
  },

  showBalance(currency) {
    this.model.getBalance(
      this.view.down("[name=id]").getValue(),
      currency,
      res => {
        this.view
          .down("[action=total_balance]")
          .setText(Ext.util.Format.number(res, "0.00"));
      }
    );
  }
});
