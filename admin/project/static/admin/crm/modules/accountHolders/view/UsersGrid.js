Ext.define("Crm.modules.accountHolders.view.UsersGrid", {
  extend: "Core.grid.GridContainer",

  title: D.t("Account Holders"),
  iconCls: "x-fa fa-users",

  filterable: true,
  filterbar: true,

  fields: [
    "id",
    "first_name",
    "last_name",
    "email",
    "phone",
    "realm",
    "active",
    "activated",
    "kyc",
    "legalname",
    "type"
  ],

  gridCfg: {
    viewConfig: {
      getRowClass: record => {
        if (!record.data.activated) return "disabled";
        else if (!record.data.kyc) return "pending";
      }
    }
  },

  buildColumns: function() {
    return [
      {
        text: D.t("Type"),
        width: 100,
        sortable: true,
        filter: {
          xtype: "combo",
          valueField: "code",
          displayField: "name",
          store: {
            fields: ["code", "name"],
            data: [
              { code: 0, name: D.t("Personal") },
              { code: 1, name: D.t("Company") }
            ]
          }
        },
        dataIndex: "type",
        renderer: v => {
          return v == 1 ? D.t("Company") : D.t("Personal");
        }
      },
      {
        text: D.t("Organisation"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "legalname"
      },
      {
        text: D.t("First name"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "first_name"
      },
      {
        text: D.t("Last name"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "last_name"
      },
      {
        text: D.t("E-mail"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "email"
      },
      {
        text: D.t("Phone"),
        flex: 1,
        sortable: true,
        filter: true,
        dataIndex: "phone"
      },
      {
        text: D.t("Realm"),
        flex: 1,
        sortable: true,
        filter: Ext.create("Crm.modules.realm.view.RealmCombo", {
          name: null,
          fieldLabel: null
        }),
        dataIndex: "realm",
        renderer: (v, m, r) => {
          return v ? v.name : "";
        }
      },
      {
        text: D.t("KYC"),
        width: 100,
        sortable: true,
        filter: {
          xtype: "combo",
          valueField: "key",
          displayField: "val",
          store: {
            fields: ["key", "val"],
            data: [
              { key: "true", val: D.t("YES") },
              { key: "false", val: D.t("NO") }
            ]
          }
        },
        dataIndex: "kyc",
        renderer: (v, m, r) => {
          return v ? D.t("YES") : "";
        }
      }
      /*{
        text: D.t("Status"),
        width: 100,
        sortable: true,
        filter: {
          xtype: "combo",
          valueField: "key",
          displayField: "val",
          store: {
            fields: ["key", "val"],
            data: [
              { key: "true", val: D.t("Activated") },
              { key: "false", val: D.t("Disabled") }
            ]
          }
        },
        dataIndex: "active",
        renderer: (v, m, r) => {
          return renderer(v ? D.t("Activated") : "", m, r);
        }
      }*/
    ];
  }
});
