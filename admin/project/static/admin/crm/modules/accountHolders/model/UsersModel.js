Ext.define("Crm.modules.accountHolders.model.UsersModel", {
  extend: "Core.data.DataModel",

  collection: "users",
  idField: "id",
  removeAction: "remove",
  strongRequest: true,
  foreignKeyFilter: [
    {
      collection: "user_accounts",
      alias: "ua",
      on: " users.id = user_accounts.user_id",
      type: "inner"
    },
    {
      collection: "accounts",
      alias: "a",
      on: " accounts.id = user_accounts.account_id ",
      type: "inner"
    }
  ],
  fields: [
    {
      name: "id",
      type: "ObjectID",
      visible: true
    },
    {
      name: "first_name",
      type: "string",
      filterable: true,
      unique: true,
      editable: true,
      visible: true
    },
    {
      name: "last_name",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "email",
      type: "string",
      filterable: true,
      vtype: "email",
      editable: true,
      visible: true
    },
    {
      name: "mobile",
      type: "string",
      editable: true,
      visible: false
    },
    {
      name: "password",
      type: "password",
      editable: true,
      visible: true
    },
    {
      name: "username",
      type: "string",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mobile_verified",
      type: "boolean",
      filterable: true,
      editable: true,
      visible: true,
      itemsType: "string"
    },
    {
      name: "email_verified",
      type: "boolean",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "status",
      type: "string",
      filterable: false,
      editable: true,
      visible: true
    },
    {
      name: "removed",
      type: "number",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "ctime",
      type: "date",
      filterable: true,
      editable: true,
      visible: true
    },
    {
      name: "mtime",
      type: "date",
      sort: -1,
      filterable: true,
      editable: true,
      visible: true
    }
  ],

  /* scope:client */
  async getCount(cb) {
    this.runOnServer("getCount", {}, cb);
  },

  /* scope:server */
  async $getCount(data, cb) {
    let sql = "SELECT count(*) FROM users ";
    sql = await this.buildJoinQuery(sql);
    sql += " WHERE users.removed=0 ";

    const res = await this.src.db.query(sql);
    cb({
      count: res[0].count
    });
  },

  /* scope:server */
  insertDataToDb: function(data, cb) {
    data.ctime = new Date();
    data.mtime = new Date();
    this.dbCollection.insert(data, cb);
  },

  /* scope:server */
  updateDataInDb: function(_id, data, cb) {
    var oo = {};
    oo[this.idField] = _id;
    data.mtime = new Date();
    this.dbCollection.update(oo, { $set: data }, cb);
  },

  /* scope:client */
  getBalance(user_id, currency, cb) {
    this.runOnServer("getBalance", { user_id, currency }, (res) => {
      cb(res ? res.balance : 0);
    });
  },

  /* scope:server */
  async $getBalance(data, cb) {
    let find = {
      id: data.user_id
    };
    // find = await this.checkValidRealmsID(find);
    const realm = await this.src.db
      .collection("users")
      .findOne(find, { realm: 1 });
    if (!realm) return cb({ balance: 0 });

    const { result } = await this.src.queue.requestOne("account-service", {
      method: "getUserBalance",
      data: {
        currency: data.currency
      },
      realmId: realm.realm,
      userId: data.user_id
    });
    cb(result);
  }
});
