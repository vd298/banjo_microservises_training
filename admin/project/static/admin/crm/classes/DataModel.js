Ext.define("Crm.classes.DataModel", {
  extend: "Core.data.DataModel",

  callApi(service, method, data, options) {
    return new Promise(async (resolve, reject) => {
      this.do_callApi(service, method, data, options, resolve, reject);
    });
  },

  async do_callApi(service, method, data, options, resolve, reject) {
    if (!Glob.ws) {
      setTimeout(() => {
        this.do_callApi(service, method, data, options, resolve, reject);
      }, 1000);
      return;
    }

    if (data.files) {
      data.files = await this.filesToBase64(data.files);
    }

    this.runOnServer("callApi", { service, method, data, options }, (res) => {
      if (res.result) resolve(res.result);
      else reject(res.error);
    });
  },

  async filesToBase64(files) {
    let out = [];
    for (let file of files) {
      out.push(await this.fileToBase64(file));
    }
    return out;
  },

  fileToBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () =>
        resolve({
          name: file.name,
          data: reader.result
        });
      reader.onerror = (error) => reject(error);
    });
  }
});
