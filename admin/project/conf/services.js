let pgsql = {
  use_env_variable: "DB_CONN_STRING",
  schema: process.env.DB_SCHEMA || "public",
  port: 5432,
  max: 10,
  idleTimeoutMillis: 30000,
  debugQuery: false,
  debug: false
};

let memcached = {
  servers: "localhost:11211",
  options: {}
};

let nats = {
  servers: ["nats://localhost:4222"],
  json: true
};

if (process.env.NODE_ENV == "staging") {
  pgsql = {
    use_env_variable: "DB_CONN_STRING",
    schema: process.env.DB_SCHEMA || "public",
    port: 5323,
    max: 10,
    idleTimeoutMillis: 30000,
    debugQuery: false,
    debug: false
  };

  memcached = {
    servers: "memcached:11211",
    options: {}
  };

  nats = {
    servers: ["nats://nats:4222"],
    json: true
  };
} else if (process.env.NODE_ENV == "production") {
  pgsql = {
    use_env_variable: "DB_CONN_STRING",
    schema: process.env.DB_SCHEMA || "public",
    port: 5432,
    max: 10,
    idleTimeoutMillis: 30000,
    debugQuery: false,
    debug: false
  };

  memcached = {
    servers: "memcached:11211",
    options: {}
  };

  nats = {
    servers: ["nats://nats:4222"],
    json: true
  };
} else if (process.env.NODE_ENV == "docker") {
  pgsql = {
    use_env_variable: "DB_CONN_STRING",
    schema: process.env.DB_SCHEMA || "public",
    port: 5432,
    max: 10,
    idleTimeoutMillis: 30000,
    debugQuery: false,
    debug: false
  };

  memcached = {
    servers: "memcached:11211",
    options: {}
  };

  nats = {
    servers: ["nats://nats:4222"],
    json: true
  };
}

exports.pgsql = pgsql;
exports.memcached = memcached;
exports.nats = nats;
