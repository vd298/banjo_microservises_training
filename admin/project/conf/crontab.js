/**
 * include cans be a js file
 * ATTENTION!!!
 * crontab should be activated just on one application node
 */

exports.crontab = [
  //"1 1 * * * Crm.Reports.DebtCalculator.dailyCalculate",
];

exports.filesTmpDir = "/app/admin/tmp/";
exports.userFilesDir = "/app/admin/tmp";
exports.userDocsDir = "/app/admin/tmp";
exports.adminModulesDir = __dirname + "/../static/admin/crm/modules";
