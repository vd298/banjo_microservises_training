const Queue = require("@lib/queue");

_TABPREFIX = "";
_LAST_MESSAGE_TIME_ = new Date();
_LAST_READ_MESSAGE_TIME_ = new Date();

Ext.define("Crm.Core.ProjectServer", {
  extend: "Core.ProjectServer",
  dbConnect: function(callback) {
    var opt = Ext.clone(this.config.pgsql);

    _TABPREFIX = this.config.tablePrefix || "";

    opt.callback = (conn) => {
      this.connectNats(() => {
        callback();
      });
    };
    this.sources.db = Ext.create("Database.drivers.Postgresql.Database", opt);
  },

  connectNats: function(cb) {
    this.sources.queue = Ext.create("Crm.Core.Queue", this.config);

    this.sources.db
      .collection("admin_users")
      .findOne({ superuser: 1 }, {}, (e, user) => {
        Queue.subscribe("broadcast-request", (data) => {
          if (data.method == "call-admin" && data.data && data.data.model) {
            try {
              const obj = Ext.create(data.data.model, {
                src: this.sources,
                config: this.config,
                user: {
                  id: user._id,
                  profile: user
                }
              });
              if (obj && !!obj[data.data.method])
                obj[data.data.method](data.data.data);
            } catch (err) {
              console.error(
                `ProjectServer.js. Func:connectNats. Caught Exception:`,
                err
              );
              console.error(
                `ProjectServer.js. Func:connectNats. Attempted Creation of Model:`,
                data.data.model
              );
              console.error(
                `ProjectServer.js. Func:connectNats. Data Object:`,
                data
              );
            }
          }
        });
      });
    cb();
  }
});
