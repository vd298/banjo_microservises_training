const FileProvider = require("@lib/fileprovider").default;
const config = require("@lib/config");

Ext.define("Crm.classes.DataModel", {
  extend: "Core.data.DataModel",

  async $callApi(data, cb) {
    const permis = await this.getPermissions();

    if (!permis.add && !permis.modify)
      return cb({ success: false, message: "Access denied" });

    const res = await this.callApi(data);
    if (res) cb(res);
    else cb({ success: false });
  },

  async callApi(data) {
    var me = this;
    if (data.data && data.data.files && Ext.isArray(data.data.files)) {
      data.data.files = await this.prepareFiles(data.data.files);
    }
    let resultData;
    try {
      resultData = await this.doJob(
        data.service,
        data.method,
        data.data,
        data.options
      );
    } catch (err) {
      console.error(
        "DataModel.js. Func: callApi. Caught Error:",
        JSON.stringify(err, null, 2)
      );
      if (data && data.service && data.method) {
        console.error(
          `DataModel.js. Func: callApi. Service called:${data.service} for method ${data.method}`
        );
      }
      if (data && data.data) {
        console.error(
          `DataModel.js. Func: callApi. Data Object:`,
          JSON.stringify(data.data, null, 2)
        );
      }
      return err;
    }

    if (resultData) {
      if (data.data && data.data.files)
        await FileProvider.accept(data.data.files);
      if (data && data.writeFlag) {
        me.changeModelData(
          Object.getPrototypeOf(me).$className,
          "ins",
          resultData
        );
      }
      return resultData;
    } else {
      if (data.data && data.data.files)
        await this.removeTemplatedFiles(data.data.files);
    }
    return null;
  },

  async prepareFiles(files) {
    let fileData,
      out = [];
    try {
      for (let i = 0; i < files.length; i++) {
        fileData = await FileProvider.push(
          files[i],
          config.new_file_hold_timeout || 300
        );
        if (fileData && fileData.success) {
          out.push({
            name: files[i].name,
            code: fileData.code,
            size: fileData.size
          });
        }
      }
    } catch (e) {
      console.log("func: prepareFiles Caught Error:", e);
      this.error("FILEUPLOADERROR");
    }
    return out;
  },

  async removeTemplatedFiles(files) {
    for (let i = 0; i < files.length; i++) {
      await FileProvider.del(files[i].data);
    }
  },

  async doJob(service, method, data, options) {
    const Queue = require("@lib/queue");
    const res = await Queue.newJob(service, {
      method,
      data,
      options: { ...options, scope: "admin", header: { lang: "en" } }
    });

    if (res.error) {
      throw res;
    } else {
      return res;
    }
  }
});
