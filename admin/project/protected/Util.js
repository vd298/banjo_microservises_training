Ext.define("Crm.Util", {
  extend: "Core.Controller",

  /**
   * @method getRandomPassword
   * @author Aditya Tapaswi
   * @param {Number} charLen Contains length of string
   * @since 2 NOV 2022
   * @summary return random alpha numeric string of desired length
   */
  getRandomPassword: function(charLen) {
    var result = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < charLen; i++) {
      result += characters.charAt(
        Math.floor(Math.random() * characters.length)
      );
    }
    return result;
  },

  /**
   * @method generateHash
   * @author Aditya Tapaswi
   * @param {String} password Contains password to be hashed
   * @since 2 NOV 2022
   * @summary Returns hash of string
   */
  generateHash: function(password) {
    const crypto = require("crypto");
    const passwordHashed = crypto
      .createHash("sha256")
      .update(password)
      .digest("hex");
    return passwordHashed;
  },

  /**
   * @method pushArray
   * @author Vaibhav Vaidya
   * @param {array} values Contains Array of values
   * @param {array} sqlPlaceHolders Array of placeholders
   * @param {object} cond Conditions of sql
   * @since 5 April 2023
   * @summary
   */
  pushArray: function(values, sqlPlaceHolders, cond) {
    var me = this;
    var buildStr = "",
      inString = "",
      validFlag = false;
    if (
      cond &&
      cond.colName &&
      values &&
      Array.isArray(values) &&
      values.length > 0 &&
      typeof values[0] != "object"
    ) {
      if (cond && cond.and == true) buildStr += " and ";
      if (cond && cond.or == true) buildStr += " or ";
      buildStr += ` ${cond.colName} `;
      if (cond && cond.not == true) buildStr += " not ";
      buildStr += ` in (`;

      for (var k = 0; k < values.length; k++) {
        if (typeof values[k] != undefined && values[k] !== "") {
          if (k > 0 && validFlag) buildStr += " ,";
          validFlag = true;
          buildStr += me.pushVal(values[k], sqlPlaceHolders);
        }
      }
      buildStr += " ) ";
    }
    if (validFlag) return buildStr;
    else return "";
  },

  /**
   * @method pushVal
   * @author Vaibhav Vaidya
   * @param {string} val value to push
   * @param {array} sqlPlaceHolders Array of placeholders
   * @since 5 April 2023
   * @summary
   */
  pushVal: function(val, sqlPlaceHolders) {
    if (
      val != undefined &&
      val !== "" &&
      sqlPlaceHolders &&
      Array.isArray(sqlPlaceHolders)
    ) {
      sqlPlaceHolders.push(val);
      return `$${sqlPlaceHolders.length}`;
    }
    return "";
  }
});
