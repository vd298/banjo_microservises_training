require("nightwatch/bin/runner.js");

module.exports = {
  src_folders: [
    "./project/static/admin/crm/modules/accountHolders/tests",
    "./project/static/admin/crm/modules/accounts/tests",
    "./project/static/admin/crm/modules/banks/tests",
    "./project/static/admin/crm/modules/businessTypes/tests",
    "./project/static/admin/crm/modules/currency/tests",
    "./project/static/admin/crm/modules/invoiceTemplates/tests",
    "./project/static/admin/crm/modules/kyc/tests",
    "./project/static/admin/crm/modules/letterTemplates/tests",
    "./project/static/admin/crm/modules/logs/tests",
    "./project/static/admin/crm/modules/merchants/tests",
    "./project/static/admin/crm/modules/messages/tests",
    "./project/static/admin/crm/modules/notifications/tests",
    "./project/static/admin/crm/modules/realm/tests",
    "./project/static/admin/crm/modules/settings/tests",
    "./project/static/admin/crm/modules/support/tests",
    "./project/static/admin/crm/modules/tariffs/tests",
    "./project/static/admin/crm/modules/transactions/tests",
    "./project/static/admin/crm/modules/transfers/tests",
    "./project/static/admin/crm/modules/transporters/tests"
  ],

  webdriver: {
    start_process: true,
    port: 4444,
    server_path: require("geckodriver").path,
    cli_args: []
  },

  test_settings: {
    default: {
      launch_url: "http://localhost:8080/",
      desiredCapabilities: {
        browserName: "firefox"
      }
    },
    localtest: {
      launch_url: "http://localhost:8080/",
      browserName: "firefox",
      alwaysMatch: {
        "moz:firefoxOptions": {
          args: []
        }
      }
    }
  }
};
