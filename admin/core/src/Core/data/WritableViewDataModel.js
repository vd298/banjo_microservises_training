/**
 * @class Core.data.WritableViewDataModel
 * @extend Core.AbstractModel
 *
 * Server
 *
 *
 * Server side data model
 */

/*
var excelbuilder = require('msexcel-builder'),
    crypto = require('crypto'),
    BSON = require('bson').pure().BSON;
 */
Ext.define("Core.data.WritableViewDataModel", {
  extend: "Core.data.DataModel",
  // write: function(name) {
  //   console.log(this.tableName, "tabel");
  // }

  constructor: function(cfg) {
    var me = this;

    if (cfg) {
      if (cfg.skipInit) {
        me.callParent(arguments);
        return;
      }

      if (me.dbType && cfg.src[me.dbType]) {
        me.db = cfg.src[me.dbType];
      } else if (cfg.src && cfg.src.db) {
        me.db = cfg.src.db;
        if (me.collection)
          me.dbCollection = cfg.src.db.collection(me.collection);
      }

      // if (!me.notCheckCollection && cfg && cfg.config && cfg.config.debug) {
      // me.db.checkCollection(me);
      // }

      if (cfg.xPermissions) me.accessSet = cfg.xPermissions;
    }
    me.callParent(arguments);
  },

  /**
   * @method
   * Server and client method
   *
   * Saving data
   * @param {Object} data record to save
   * @param {Function} callback
   * @private
   */
  write: function(data, callback, permissions, fields, runInBackground) {
    console.log(data, "data");
    console.log(this.tableName, "tabel");
    var me = this,
      insdata = {},
      isNew = false,
      //,db = tx? tx.db : me.src.db
      actionType;

    [
      function(call) {
        var isValid = me.isValid(data);
        if (isValid === true) {
          call();
        } else {
          me.error(415, { mess: isValid.join("; ") });
        }
      },
      function(call) {
        if (!!me.beforeSave) {
          me.beforeSave(data, call);
        } else call(data);
      },

      function(data, call) {
        if (!data || data.success === false) {
          callback(data);
          return;
        }
        call(data);
      },

      function(data, call) {
        if (
          data &&
          data[me.idField] &&
          data[me.idField] != "-" &&
          !me.insertStrong
        ) {
          call("upd", data);
        } else {
          //if(data[me.idField]) delete data[me.idField];
          call("ins", data);
        }
        //}, null, data)
      },

      function(type, data, call) {
        // Updating

        actionType = type;
        if (type == "ins") {
          call(true, data);
          return;
        }
        me.db.fieldTypes[me.fields[0].type].getValueToSave(
          me,
          data[me.idField] + "",
          null,
          null,
          null,
          function(_id) {
            var oo = {};
            oo[me.idField] = _id;
            me.db.collection(me.tableName).findOne(oo, function(err, cur_data) {
              if (cur_data) {
                var fff = function(permis) {
                  if (permis.modify) {
                    me.createDataRecord(
                      data,
                      cur_data,
                      function(data) {
                        if (!me.strongRequest) {
                          data.mtime = new Date();
                          data = me.setModifyTime(data);
                        }

                        me.updateDataInDb(_id, data, function(e, d) {
                          data[me.idField] = _id;
                          //if(!) {
                          me.isNeedSign(function(res) {
                            if (!!res) {
                              if (cur_data.signobject)
                                data.signobject = cur_data.signobject;
                              else data.signobject = {};
                              data.signobject.shouldSign = true;
                            }
                            call(false, data);
                          }, data);

                          //} else
                          //    call(false, data)
                        });
                      },
                      fields
                    );
                  } else me.error(401);
                };

                if (permissions) fff(permissions);
                else me.getPermissions(fff, null, cur_data);
              } else {
                data[me.idField] = _id;
                actionType = "ins";
                call(true, data);
              }
            });
          }
        );
      },

      function(ins, data, call) {
        // Inserting
        if (ins) {
          var _id = data[me.idField];
          me.createDataRecord(
            data,
            null,
            function(data) {
              if (!me.strongRequest) {
                if (!data.ctime) data.ctime = new Date();
                if (!data.mtime) data.mtime = new Date();
                if (me.user) data.maker = me.user.id;
                data = me.setModifyTime(data);
                if (!me.removeAction || me.removeAction == "mark")
                  data.removed = 0;
              }
              if (_id) data[me.idField] = _id;
              else if (!!me.db.fieldTypes[me.fields[0].type].createNew) {
                data[me.idField] = me.db.fieldTypes[
                  me.fields[0].type
                ].createNew(me.src.db);
              }
              var fff = function(permis) {
                if (permis.add) {
                  me.insertDataToDb(data, function(e, d) {
                    isNew = true;
                    if (!e) {
                      me.isNeedSign(function(res) {
                        if (!!res) {
                          data.signobject = { shouldSign: true };
                          if (me.autoSign) {
                            me.AutoSignDo(data, function() {
                              call(data);
                            });
                          } else call(data);
                        } else call(data);
                      }, data);
                    } else {
                      console.log("Insert error:", e, data);
                      callback({ success: false, error: e });
                      return;
                    }
                  });
                } else me.error(401);
              };

              if (permissions) fff(permissions);
              else me.getPermissions(fff);
            },
            fields
          );
        } else {
          call(data);
        }
      },

      function(data, call) {
        if (!!me.afterSave) {
          me.afterSave(
            data,
            function(data) {
              call(data);
            },
            isNew
          );
        } else {
          call(data);
        }
      },

      function(data, call) {
        if (runInBackground) callback({ success: true, record: data });
        else
          me.builData([data], function(data) {
            me.addDataToSearchIndex(data, function() {
              me.changeModelData(
                Object.getPrototypeOf(me).$className,
                actionType,
                data[0]
              );
              callback({ success: true, record: data[0] });
            });
          });
      }
    ].runEach();
  },

  insertDataToDb: function(data, cb) {
    console.log(this.tableName, "tablen");
    console.log(this.dbCollection, "dbCollection");

    this.dbCollection.insert(data, cb);
  },

  updateDataInDb: function(_id, data, cb) {
    var oo = {};
    oo[this.idField] = _id;
    data.updater = this.user.id;
    this.dbCollection.update(oo, { $set: data }, cb);
  }
});
