Ext.define("Core.data.actions.Constant", {
  getRealmsIDTableColumnName: function(find) {
    const tableNames = [
      "accounts",
      "admin_users",
      "banks",
      "categories",
      "counterparts",
      "exchanges",
      "files",
      "groups",
      "messages",
      "payment_providers",
      "permissions",
      "ref_limits",
      "resources",
      "roles",
      "tariffplans",
      "tariffs_es",
      "transporters",
      "triggers",
      "tests",
      "viewset"
    ];
    const realmDataFieldTableName = ["letters", "signset"];
    if (tableNames.indexOf(find) >= 0) {
      return "realm_id";
    } else if (realmDataFieldTableName.indexOf(find) >= 0) {
      return "realm";
    }
    return false;
  }
});
