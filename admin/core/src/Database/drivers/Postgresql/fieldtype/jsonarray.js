
Ext.define('Database.drivers.Postgresql.fieldtype.jsonarray', {
    extend: 'Database.drivers.Postgresql.fieldtype.object'

    , getValueToSave: function (model, value, newRecord, oldRecord, name, callback) {
        value = JSON.stringify(value || [])
        arguments[1] = value
        this.callParent(arguments)
    }

    , getDisplayValue: function (model, record, name, callback) {
        var value = null;
        if (record && record[name]) {
            value = record[name]
        }
        callback(value)
    }
})