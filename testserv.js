const Net = require("net");

const server = new Net.Server();
server.listen(9011, () => {
  console.log(`Driver listening port: 9011`);
});
server.on("connection", (socket) => {
  console.log("connection");
  socket.on("data", (chunk) => {
    onData(chunk, socket);
  });
  socket.on("end", () => {
    console.log("end");
  });
  socket.on("error", (err) => {
    console.log("error");
  });
});

function onData(chunk, socket) {
  //console.log(chunk.map(itm => itm.toString("hex")).join(" "));
  let o = chunk.toString("hex");
  let s = "";
  for (let i = 0; i < o.length; i++) {
    if (!(i % 2)) s += " ";
    s += o.charAt(i);
  }
  console.log(s);
  for (let i = 0; i < chunk.length; i++) {
    //     console.log(getByte(chunk[i]))
  }

  let cmd;
  if (chunk.length == 17) {
    cmd = Buffer([0x01]);
  } else {
    cmd = Buffer([0, 0, 0, 0, 0, 0, 0, 0x01]);
  }
  socket.write(cmd);
}

function getByte(buf) {
  if (!buf) return "00000000";
  let out = buf.toString(2);
  while (out.length < 8) out = "0" + out;
  return out;
}
