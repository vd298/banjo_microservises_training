FROM mhart/alpine-node:12 As build

WORKDIR /app

COPY package.json /app/package.json
COPY lerna.json /app/lerna.json

COPY src/drivers-service/dist /app/src/drivers-service/dist
COPY src/drivers-service/package.json /app/src/drivers-service/package.json

COPY src/mapping-service/dist /app/src/mapping-service/dist
COPY src/mapping-service/package.json /app/src/mapping-service/package.json

COPY src/tariff-service/dist /app/src/tariff-service/dist
COPY src/tariff-service/package.json /app/src/tariff-service/package.json

COPY src/gate-service/dist /app/src/gate-service/dist
COPY src/gate-service/package.json /app/src/gate-service/package.json

COPY src/entity-service/dist /app/src/entity-service/dist
COPY src/entity-service/package.json /app/src/entity-service/package.json

COPY src/auth-service/dist /app/src/auth-service/dist
COPY src/auth-service/package.json /app/src/auth-service/package.json

COPY packages /app/packages

COPY admin /app/admin
COPY admin/package.json /app/admin/package.json

RUN apk add --update git python build-base curl bash && \
    echo "Fixing PhantomJS" && \
    curl -Ls "https://github.com/dustinblackman/phantomized/releases/download/2.1.1/dockerized-phantomjs.tar.gz" | tar xz -C / && \
    echo "Installing node modules" && \
    sed -i '/postinstall/d' package.json && \
    npm cache clean --force && \
    npm install --production && \
    npm cache clean --force && \
    apk del git python build-base curl && \
    rm -rf /usr/share/man /tmp/* /var/tmp/* /var/cache/apk/* /root/.npm /root/.node-gyp \
    echo "CD APP" && cd /app && echo "INSTALL LERNA" && npm i lerna@3.19.0 -g && echo "INSTALL YARN" && yarn --ignore-scripts --production --no-optional && npx lerna bootstrap

RUN apk add --no-cache python make gcc g++
RUN apk --no-cache add msttcorefonts-installer 
RUN apk add fontconfig && update-ms-fonts

ENV NODE_ENV=production

ENV TZ Europe/Moscow

CMD ["npx", "lerna", "run", "dstart", "--parallel"]