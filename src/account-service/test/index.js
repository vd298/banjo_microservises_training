import chai from "chai";
import db from "@lib/db";
import pretest from "@lib/pretest";
import uuid from "chai-uuid";
chai.use(uuid);
const should = chai.should();
const expect = chai.expect;
import tcpclient from "./lib/tcpclient.js";

import Service from "../src/Service.js";
const service = new Service({
  name: "entity-service"
});

let ENV;
let acc1, acc2, user1;
describe("Account service", async () => {
  before(async () => {
    ENV = await pretest.before();
  });
  after(async () => {});

  describe("Account methods", () => {
    describe("Onboarding flow", () => {
      it("Onboard merchant", async () => {
        let fakeName = "Raju" + Date.now();
        const res = await service.runServiceMethod({
          method: "onboardMerchant",
          data: {
            basic_info: {
              name: fakeName,
              address: "heaven",
              city: "Jaipur",
              country: "IN",
              legal_entity: true,
              registration_no: "1231231",
              plan_id: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4",
              type: "MERCHANT",
              status: "ACTIVE",
              realm_id: "046cce25-f407-45c7-8be9-3bf198093408"
            },
            merchant_accounts: [
              {
                name: "at213@enovate-it.com",
                currency: "INR",
                settlement_currency: "INR",
                category: "facdfaad-fded-4d40-b4b5-8bb8a44d9845",
                status: "ACTIVE",
                account_id: ENV.acc1.id,
                realm_id: "046cce25-f407-45c7-8be9-3bf198093408"
              },
              {
                id: "67af7aa8-23d9-4e5a-8963-eb7b97346555",
                name: "yj298@enovate-it.com",
                currency: "USD",
                settlement_currency: "EUR",
                category: "facdfaad-fded-4d40-b4b5-8bb8a44d9845",
                status: "INACTIVE",
                account_id: ENV.acc1.id,
                realm_id: "046cce25-f407-45c7-8be9-3bf198093408"
              }
            ],
            invited_users: [
              {
                email: "pm261@enovate-it.com",
                role: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4",
                account_id: ENV.acc1.id,
                merchant_account_id: "67af7aa8-23d9-4e5a-8963-eb7b97346555",
                user_type: "PORTAL",
                status: "INACTIVE",
                realm_id: "046cce25-f407-45c7-8be9-3bf198093408",
                owner_id: "3df4c72a-600a-11ed-9b6a-0242ac120001"
              }
            ]
          }
        });
        res.message.should.equal("Merchant onboarded successfully");
      });
    });
  });

  describe("User methods", () => {
    describe("Onboarding flow", () => {
      it("Get Invite Data", async () => {
        const invite = await db.user_account_invitations.findOne({
          where: {
            removed: 0
          }
        });

        const res = await service.runServiceMethod({
          method: "getInviteData",
          data: {
            token: invite.dataValues.token
          }
        });

        res.should.have.deep.property("id");
      });

      it("get all invitations", async () => {
        const res = await service.runServiceMethod({
          method: "getAllInvitations",
          data: {
            page: 1,
            page_size: 2
          },
          options: {
            accountId: ENV.acc1.id,
            userId: ENV.user1.id
          }
        });

        res.should.have.deep.property("success", true);
        res.should.have.deep.property("invitations");
      });
      it("Accept user invitation", async () => {
        const res = await service.runServiceMethod({
          method: "acceptUserInvitation",
          data: {
            token: "1234561",
            accepted: true,
            accountId: ENV.acc1.id
          },
          options: {
            accountId: ENV.acc1.id,
            userId: ENV.user1.id
          }
        });
        res.should.have.deep.property("success", true);
        res.should.have.deep.property(
          "message",
          "Invitation accepted successfully"
        );
      });
      it("Decline user invitation", async () => {
        const res = await service.runServiceMethod({
          method: "acceptUserInvitation",
          data: {
            token: "1234562",
            accepted: false,
            accountId: ENV.acc1.id
          },
          options: {
            accountId: ENV.acc1.id,
            userId: ENV.user1.id
          }
        });
        res.should.have.deep.property("success", true);
        res.should.have.deep.property(
          "message",
          "Invitation decline successfully"
        );
      });
      it("Create Account with basic info", async () => {
        let fakeName = "Raju" + Date.now();
        const res = await service.runServiceMethod({
          method: "createAccount",
          data: {
            name: fakeName,
            address: "heaven",
            city: "Jaipur",
            country: "IN",
            legal_entity: false,
            registration_no: "123123123123123123",
            plan_id: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4",
            type: "MERCHANT",
            status: "ACTIVE",
            realm_id: "046cce25-f407-45c7-8be9-3bf198093408"
          }
        });
        res.message.should.equal("Account created successfully");
      });
      it("Create Merchant Account", async () => {
        const res = await service.runServiceMethod({
          method: "createMerchantAccount",
          data: {
            name: "at213@enovate-it.com",
            currency: "INR",
            settlement_currency: "INR",
            category: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4",
            status: "ACTIVE",
            account_id: ENV.acc1.id
          },
          options: {
            account_id: ENV.acc1.id
          }
        });

        res.message.should.equal("Account created successfully");
      });
    });
    describe("User Accounts", () => {
      it("Get user accounts", async () => {
        const res = await service.runServiceMethod({
          method: "getMerchants",
          data: {},
          options: {
            userId: "3df4c34c-600a-11ed-9b6a-0242ac120002",
            header: { lan: "en" }
          }
        });

        res.should.have.deep.property("success", true);
        res.userAccounts[0].should.have.deep.property("account_id");
      });
      it("Upsert role to associated accounts", async () => {
        const res = await service.runServiceMethod({
          method: "upsertAccountRole",
          data: {
            account_id: "63af7aa8-23d9-4e5a-8963-eb7b9734644b",
            type: 1,
            role_name: "demo1",
            permissions: {
              role: {
                read: true,
                write: true,
                delete: true,
                view: true
              }
            }
          },
          options: {
            userId: "3df4c34c-600a-11ed-9b6a-0242ac120002",
            header: { lan: "en" }
          }
        });
        res.should.have.deep.property("success", true);
        res.message.should.equal("Role created successfully");

        ENV.roleId = { id: res.id };
      });
      it("Upsert role to associated accounts", async () => {
        const res = await service.runServiceMethod({
          method: "upsertAccountRole",
          data: {
            account_id: "63af7aa8-23d9-4e5a-8963-eb7b9734644b",
            type: 1,
            role_name: "demo2",
            permissions: {
              role: {
                read: true,
                write: true,
                delete: true,
                view: true
              }
            },
            id: ENV.roleId.id
          },
          options: {
            userId: "3df4c34c-600a-11ed-9b6a-0242ac120002",
            header: { lan: "en" }
          }
        });
        res.should.have.deep.property("success", true);
        res.message.should.equal("Role updated successfully");
      });
      it("Return Roles List for particular Merchant", async () => {
        const res = await service.runServiceMethod({
          method: "fetchAccountRole",
          data: {
            account_id: "63af7aa8-23d9-4e5a-8963-eb7b9734644b",
            page: 1,
            page_size: 10,
            read: false,
            write: false,
            view: false,
            delete: false
          },
          options: {
            userId: "3df4c34c-600a-11ed-9b6a-0242ac120002",

            header: { lan: "en" }
          }
        });
        res.should.have.deep.property("success", true);
        res.roles[0].should.have.deep.property("id");
      });
      it("Remove Role Merchant Account", async () => {
        const res = await service.runServiceMethod({
          method: "removeAccountRole",
          data: {
            account_id: "63af7aa8-23d9-4e5a-8963-eb7b9734644b",

            id: ENV.roleId.id
          },
          options: {
            userId: "3df4c34c-600a-11ed-9b6a-0242ac120002",
            header: { lan: "en" }
          }
        });
        res.should.have.deep.property("success", true);
        res.message.should.equal("Role removed successfully");
      });
    });
    describe("Account Users", () => {
      it("Get account users", async () => {
        const res = await service.runServiceMethod({
          method: "getAccountUsers",
          options: {
            userId: ENV.user1.id,
            header: { lan: "en" }
          },
          data: {
            account_id: ENV.acc1.id
          }
        });
        res.should.have.deep.property("success", true);
        res.users[0].should.have.deep.property("account_id");
      });
    });
    describe("Account Users", () => {
      it("Get account users", async () => {
        const res = await service.runServiceMethod({
          method: "getAccountUsers",
          options: {
            userId: ENV.user1.id,
            header: { lan: "en" }
          },
          data: {
            account_id: ENV.acc1.id
          }
        });
        res.should.have.deep.property("success", true);
        res.users[0].should.have.deep.property("account_id");
      });
    });
    describe("Account Users", () => {
      it("Get account users", async () => {
        const res = await service.runServiceMethod({
          method: "getAccountUsers",
          options: {
            userId: ENV.user1.id,
            header: { lan: "en" }
          },
          data: {
            account_id: ENV.acc1.id
          }
        });
        res.should.have.deep.property("success", true);
        res.users[0].should.have.deep.property("account_id");
      });
    });
    describe("Remove Account User", () => {
      it("Remove account users", async () => {
        const res = await service.runServiceMethod({
          method: "removeAccountUser",
          options: {
            userId: ENV.user1.id,
            header: { lan: "en" }
          },
          data: {
            account_id: ENV.acc1.id,
            user_account_id: "3df4c72a-600a-11ed-9b6a-0242ac120001"
          }
        });
        res.should.have.deep.property("success", true);
        res.message.should.equal("User account remove successfully");
      });
    });
    describe("Account Users", () => {
      it("Get account users", async () => {
        const res = await service.runServiceMethod({
          method: "getAccountUsers",
          options: {
            userId: ENV.user1.id,
            header: { lan: "en" }
          },
          data: {
            account_id: ENV.acc1.id
          }
        });
        res.should.have.deep.property("success", true);
        res.users[0].should.have.deep.property("account_id");
        ENV.user_account_id = res.users[0].user_account_id;
      });
    });
    describe("update Account User", () => {
      it("Update account users", async () => {
        const res = await service.runServiceMethod({
          method: "updateUserAccount",
          options: {
            userId: ENV.user1.id,
            header: { lan: "en" }
          },
          data: {
            account_id: ENV.acc1.id,
            user_account_id: ENV.user_account_id
          }
        });
        res.should.have.deep.property("success", true);
        res.message.should.equal("User account update successfully");
      });
    });

    describe("Users account role verification", () => {
      it("Users account role verification", async () => {
        const res = await service.runServiceMethod({
          method: "usersAccountRoleVerification",
          options: {
            userId: ENV.user1.id,
            header: { lan: "en" }
          },
          data: {
            account_id: ENV.acc1.id
          }
        });
        res.should.have.deep.property("success", true);
        res.should.have.deep.property("role_id");
      });
    });
    describe("Add/update beneficiary", () => {
      it("Add/update beneficiary", async () => {
        const res = await service.runServiceMethod({
          method: "addUpdateBeneficiary",
          options: {
            userId: ENV.user1.id,
            header: { lan: "en" }
          },
          data: {
            account_id: "63af7aa8-23d9-4e5a-8963-eb7b9734644b",
            merchant_account_id: "1e50984c-8cd1-11ed-a1eb-0242ac121112",
            protocols: ["44e1ec76-a196-4793-b2f4-a7e569cda66c"],
            first_name: "ew",
            last_name: "we",
            email: "er@gmil.com",
            country_code: "91",
            mobile: "12324234242",
            vpa: "uyugj",
            country_code: "91"
          }
        });
        res.should.have.deep.property("success", true);
      });
    });
    describe("getbeneficiary", () => {
      it("get Beneficiaries", async () => {
        const res = await service.runServiceMethod({
          method: "getBeneficiaries",
          options: {
            userId: ENV.user1.id,
            maId: "1e50984c-8cd1-11ed-a1eb-0242ac121112",
            header: { lan: "en" }
          },
          data: {}
        });
        res.should.have.deep.property("success", true);
      });
    });
  });
});
