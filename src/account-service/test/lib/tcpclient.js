import net from "net";

export default class TCPclient {
  constructor() {
    this.client = new net.Socket();
    this.client.on("data", (data) => {
      this.responseData = this.decodeData(data);
    });
  }

  connect(port, host) {
    return new Promise((res) => {
      this.client.connect(port, host, () => {
        res();
      });
    });
  }
  send(data) {
    return new Promise((res) => {
      this.responseData = "";
      this.client.write(this.encodeData(data));
      setTimeout(() => {
        res(this.responseData);
      }, 500);
    });
  }

  encodeData(data) {
    let d = data.trim().split(" ");
    let buf = new Buffer.alloc(d.length);
    d.forEach((itm, i) => {
      buf[i] = parseInt("0x" + itm);
    });
    return buf;
  }

  decodeData(chunk) {
    let o = chunk.toString("hex");
    let s = "";
    for (let i = 0; i < o.length; i++) {
      if (!(i % 2)) s += " ";
      s += o.charAt(i);
    }
    return s;
  }
}
