import Base from "@lib/base";
import Users from "./lib/Users";
import Account from "./lib/Account";
import Transfer from "./lib/Transfer";
import Wallets from "./lib/Wallets";
import jasperExport from "./lib/jasperExport";
import Aggregate from "./lib/Aggregate";
import microB from "./lib/MicroserviceB";
import curr from "./lib/currencyFind";

//import microB from "./lib/MicroserviceB";
// import Acquirer from "./lib/Acquirer";
export default class Service extends Base {
  publicMethods() {
    return {
      getCountries: {
        method: curr.getCountries,
        description: "All currencies",
        realm: true,
        // account: true,
        // user: true,
        schema: {
          type: "object",
          properties: {
            code: { type: "number" },
          },
        },
      },
      aggregateDataForStudent: {
        method: Aggregate.aggregateDataForStudent,
        description: "Student's Topper",
        realm: true,
        // account: true,
        // user: true,
        schema: {
          type: "object",
          properties: {
            studentId: { type: "number" },
          },
        },
      },
      aggregateDataForStudentMicroB: {
        method: microB.aggregateDataForStudentMicroB,
        description: "Details of a student in Microservice B",
        realm: true,
        schema: {
          type: "object",
          properties: {
            studentId: { type: "number" },
          },
        },
      },
      createWallet: {
        private: true,
        method: Wallets.createWallet,
        description: "Create wallet",
      },
      userList: {
        account: true,
        user: true,
        method: Users.list,
        description: "List all users",
      },
      getOneUser: {
        account: true,
        user: true,
        method: Users.getOne,
        schema: {
          type: "object",
          properties: {
            id: { type: "string", format: "uuid" },
          },
          required: ["email", "role"],
        },
        description: "Getting one user",
      },

      getInviteData: {
        realm: true,
        method: Users.getInviteData,
        schema: {
          type: "object",
          properties: {
            token: { type: "string", maxLength: 80 },
          },
          required: ["token"],
        },
        description: "Fetch Invitation data for User",
      },
      getChildAccounts: {
        description: "Get child account hierarchy",
        account: true,
        user: true,
        method: Account.getChildAccounts,
        schema: {
          type: "object",
          properties: {},
        },
      },
      getMerchants: {
        realm: true,
        user: true,
        method: Account.getMerchants,
        description: "Get associated accounts to user",
        schema: {
          type: "object",
          properties: {
            page_size: {
              type: "integer",
            },
            page: {
              type: "integer",
            },
          },
        },
        responseSchema: {
          type: Array,
          items: {
            properties: {
              account_id: {
                type: "string",
                description: "user account id",
              },
              id: {
                type: "string",
                description: "user id",
              },
              email: {
                type: "string",
                description: "user email address",
              },
              account_name: {
                type: "string",
                description: "user account name",
              },
              u_a_ctime: {
                type: "string",
                description: "user account created time",
              },
              legal_entity: {
                type: "boolean",
                description: "legal entity",
              },
              role: {
                type: "string",
                description: "user role",
              },
              status: {
                type: "string",
                description: "account active status",
              },
            },
          },
        },
      },
      getAccountUsers: {
        realm: true,
        user: true,
        method: Account.getAccountUsers,
        description: "Get associated  user to accounts",
        schema: {
          type: "object",
          properties: {
            page_size: {
              type: "integer",
            },
            page: {
              type: "integer",
            },
            account_id: {
              type: "string",
            },
            search_text: {
              type: "string",
            },
            merchant_account_ids: {
              type: "array",
              items: {
                type: "string",
              },
            },
            user_id: { type: "string", format: "uuid" },
          },
          required: ["account_id"],
        },
      },
      removeAccountUser: {
        realm: true,
        user: true,
        method: Account.removeAccountUser,
        description: "Remove associated  user to accounts",
        schema: {
          type: "object",
          properties: {
            account_id: {
              type: "string",
            },
            user_account_id: { type: "string", format: "uuid" },
          },
          required: ["account_id", "user_account_id"],
        },
      },
      fetchMerchantAccountDetails: {
        description: "Fetch main account basic details",
        user: true,
        method: Account.fetchMerchantAccountDetails,
        schema: {
          type: "object",
          properties: {
            page_size: {
              type: "integer",
            },
            page: {
              type: "integer",
            },
            merchant_account_id: {
              type: "string",
            },
          },
        },
      },
      fetchMerchantAccounts: {
        description: "Fetch merchant accounts",
        user: true,
        method: Account.fetchMerchantAccounts,
        schema: {
          type: "object",
          properties: {
            page_size: {
              type: "integer",
            },
            page: {
              type: "integer",
            },
            account_id: { type: "string", format: "uuid" },
          },
        },
      },
      getAllInvitations: {
        description: "Get all pending invitations",
        account: true,
        user: true,
        method: Users.getAllInvitations,
        schema: {
          type: "object",
          properties: {
            page: {
              type: "number",
              format: "integer",
              description: "Page number starts from 1, Default 1",
            },
            page_size: {
              type: "number",
              format: "integer",
              description: "Default 100, Max 200 records per API call",
            },
          },
        },
      },
      createAccount: {
        admin: true,
        method: Account.createAccount,
        schema: {
          type: "object",
          properties: {
            name: { type: "string", maxLength: 50 },
            address: { type: "string", maxLength: 150 },
            city: { type: "string", maxLength: 50 },
            country: { type: "string", maxLength: 2 },
            registration_no: { type: "string", maxLength: 30 },
            plan_id: { type: "string" },
            type: { type: "string" },
            status: { type: "string" },
          },
        },
        description: "Create account with basic info",
      },
      createMerchantAccount: {
        description: "Create merchant account with unique account no",
        required: ["name"],
        admin: true,
        method: Users.createMerchantAccount,
        schema: {
          type: "object",
          properties: {
            account_id: { type: "string" },
            acc_no: {
              type: "string",
            },
            name: { type: "string", maxLength: 50 },
            currency: { type: "string", maxLength: 4 },
            settlement_currency: { type: "string", maxLength: 4 },
            category: { type: "string", format: "uuid" },
            status: { type: "string" },
            auto_payouts: { type: "integer" },
            api_credential_id: { type: "string", format: "uuid" },
            expiry_hours: { type: ["number", "string"] },
            payout_expiry_hours: { type: ["number", "string"] },
          },
        },
      },
      acceptUserInvitation: {
        description: "Accept user invitations",
        user: true,
        method: Users.acceptUserInvitation,
        schema: {
          type: "object",
          properties: {
            token: { type: "string", maxLength: 80 },
            accepted: { type: "boolean", format: "boolean" },
            accountId: { type: "string", format: "uuid" },
          },
          required: ["token", "accepted", "accountId"],
        },
      },
      associateBankAccount: {
        admin: true,
        method: Account.associateBankAccount,
        schema: {
          type: "object",
          properties: {
            merchant_account_id: { type: "string", format: "uuid" },
            bank_account_id: { type: "string", format: "uuid" },
          },
          required: ["merchant_account_id", "bank_account_id"],
        },
        description: "Associate bank account to merchant account",
      },
      releaseBankAccount: {
        private: true,
        method: Account.releaseBankAccount,
        schema: {
          type: "object",
          properties: {
            mapping_id: { type: "string", format: "uuid" },
          },
          required: ["mapping_id"],
        },
        description: "De associate allocate bank account from merchant account",
      },
      onboardMerchant: {
        description: "Onboard new merchant using multi-step form",
        account: true,
        user: true,
        method: Account.onboardMerchant,
        schema: {
          type: "object",
          properties: {
            basic_info: {
              type: "object",
              description: "Merchant basic info",
              properties: {
                name: {
                  type: "string",
                  description: "Name of merchant",
                },
                address: {
                  type: "string",
                  description: "Merchant address",
                },
                city: {
                  type: "string",
                  description: "Merchant city",
                },
                country: {
                  type: "string",
                  description: "Merchant country",
                },
                registration_no: {
                  type: "string",
                },
                plan_id: { type: "string" },
                type: { type: "string" },
                status: { type: "string" },
                realm_id: { type: "string" },
              },
            },
            merchant_accounts: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  name: { type: "string" },
                  currency: { type: "string" },
                  settlement_currency: { type: "string" },
                  category: { type: "string" },
                  status: { type: "string" },
                  account_id: { type: "string" },
                  realm_id: { type: "string" },
                },
              },
            },
            invited_users: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  email: { type: "string" },
                  role: { type: "string" },
                  account_id: { type: "string" },
                  user_type: { type: "string" },
                  status: { type: "string" },
                  realm_id: { type: "string" },
                },
              },
              description: "Invited users array",
            },
          },
        },
      },
      checkBasicMerchant: {
        description: "Check for uniqueness for Basic Merchant info",
        account: true,
        user: true,
        method: Account.checkBasicMerchant,
        schema: {
          type: "object",
          properties: {
            registration_no: {
              type: "string",
            },
            name: {
              type: "string",
            },
          },
        },
      },
      "transaction-list": {
        description: "Return Transaction List for particular Merchant Account",
        user: true,
        method: Transfer.fetchTransferList,
        schema: {
          type: "object",
          properties: {
            page: {
              type: "number",
              format: "integer",
              description: "Page number starts from 1, Default 1",
            },
            page_size: {
              type: "number",
              format: "integer",
              description: "Default 100, Max 200 records per API call",
            },
            filters: {
              type: "object",
            },
          },
          required: ["page", "page_size"],
        },
      },
      attachBankAccountProtocols: {
        description: "attach protocols to bank accounts",
        method: Account.attachBankAccountProtocols,
        schema: {
          type: "object",
          properties: {
            bank_account_id: {
              type: "string",
              format: "uuid",
            },
            payment_data: {
              type: "array",
            },
          },
          required: ["payment_data"],
        },
      },
      attachBankAccountLimits: {
        description: "Attach limits to bank accounts",
        method: Account.attachBankAccountLimits,
        schema: {
          type: "object",
          properties: {
            min_amount: {
              type: "number",
            },
            max_amount: {
              type: "number",
            },
            daily_deposit: {
              type: "number",
            },
            payment_protocol_id: {
              type: "string",
              format: "uuid",
            },
            currency: {
              type: "string",
            },
          },
        },
      },
      logBankAccountActivity: {
        description: "Log bank account activity",
        method: Account.logBankAccountActivity,
        schema: {
          bank_account_id: { type: "string", format: "uuid" },
          new_status: {
            type: "enum",
            enum: ["ACTIVE", "INACTIVE"],
          },
          note: { type: "string" },
          old_status: {
            type: "enum",
            enum: ["ACTIVE", "INACTIVE"],
          },
        },
      },
      usersAccountRoleVerification: {
        realm: true,
        user: true,
        method: Account.usersAccountRoleVerification,
        description: "Users account role verification",
        schema: {
          type: "object",
          properties: {
            account_id: {
              type: "string",
              format: "uuid",
            },
            role_id: {
              type: "string",
              format: "uuid",
            },
          },
          required: ["account_id"],
        },
      },
      upsertAccountRole: {
        // realm: true,
        user: true,
        method: Account.upsertAccountRole,
        description: "Upsert role to associated accounts",
        schema: {
          type: "object",
          properties: {
            account_id: {
              type: "string",
              format: "uuid",
            },
            id: { type: "string", format: "uuid" },
            role_name: { type: "string" },
            type: { type: "integer" },
            permissions: { type: "object" },
          },
          required: ["account_id", "type", "role_name", "permissions"],
        },
      },
      fetchAccountRole: {
        description: "Return Roles List for particular Merchant",
        user: true,
        method: Account.fetchAccountRole,
        schema: {
          type: "object",
          properties: {
            page: {
              type: "number",
              format: "integer",
              description: "Page number starts from 1, Default 1",
            },
            page_size: {
              type: "number",
              format: "integer",
              description: "Default 100, Max 200 records per API call",
            },
            read: { type: "boolean" },
            write: { type: "boolean" },
            view: { type: "boolean" },
            delete: { type: "boolean" },
            account_id: {
              type: "string",
              format: "uuid",
            },
          },
          required: ["account_id"],
        },
      },
      removeAccountRole: {
        description: "Remove Role Merchant Account",
        user: true,
        method: Account.removeAccountRole,
        schema: {
          type: "object",
          properties: {
            id: { type: "string", format: "uuid" },
            account_id: {
              type: "string",
              format: "uuid",
            },
          },
          required: ["account_id", "id"],
        },
      },
      updateUserAccount: {
        realm: true,
        user: true,
        method: Account.updateUserAccount,
        description: "Update associated  user to accounts",
        schema: {
          type: "object",
          properties: {
            account_id: {
              type: "string",
            },
            user_account_id: { type: "string", format: "uuid" },
            main_role: { type: "string", format: "uuid" },
            merchant_account_id: { type: "string", format: "uuid" },
            status: { type: "string" },
            user_id: { type: "string", format: "uuid" },
          },
          required: ["account_id", "user_account_id"],
        },
      },
      deleteAccountLimit: {
        description: "Delete account limit",
        method: Account.deleteAccountLimit,
        schema: {
          bank_account_protocol_id: {
            type: "string",
          },
          payment_protocol_id: {
            type: "string",
          },
        },
        required: ["bank_account_protocol_id", "payment_protocol_id"],
      },
      addBankTemplate: {
        description: "Add bank template",
        method: Account.addBankTemplate,
      },
      exportPayoutCSV: {
        description: "Export payout CSV",
        method: Account.exportPayoutCSV,
        schema: {
          bank_id: {
            type: "string",
            format: "uuid",
          },
          bank_account_id: {
            type: "string",
            format: "uuid",
          },
          merchant_id: {
            type: "array",
            items: [
              {
                type: "string",
              },
            ],
          },
          merchant_account_id: {
            type: "array",
            items: [
              {
                type: "string",
              },
            ],
          },
        },
        required: ["bank_id", "bank_account_id"],
      },
      getQualifiedPayouts: {
        description: "Export payout CSV",
        method: Account.getQualifiedPayouts,
        schema: {
          bank_id: {
            type: "string",
            format: "uuid",
          },
          bank_account_id: {
            type: "string",
            format: "uuid",
          },
          merchant_id: {
            type: "array",
            items: [
              {
                type: "string",
              },
            ],
          },
          merchant_account_id: {
            type: "array",
            items: [
              {
                type: "string",
              },
            ],
          },
        },
        required: ["bank_id", "bank_account_id"],
      },
      addUpdateBeneficiary: {
        description: "Add/update beneficiary",
        method: Account.addUpdateBeneficiary,
        schema: {
          type: "object",
          properties: {
            merchant_account_id: {
              type: "string",
              format: "uuid",
            },
            protocols: {
              type: "array",
              items: [
                {
                  type: "string",
                  format: "uuid",
                },
              ],
            },
            first_name: {
              type: "string",
              minLength: 1,
            },
            last_name: {
              type: "string",
              minLength: 1,
            },
            email: {
              type: "string",
            },
            country_code: {
              type: "string",
            },
            mobile: {
              type: "string",
            },
            acc_no: {
              type: "string",
            },
            ifsc: {
              type: "string",
            },
            bank_name: {
              type: "string",
            },
            network_type: {
              type: "string",
            },
            crypto_wallet_address: {
              type: "string",
            },
            description: {
              type: "string",
            },
            vpa: {
              type: "string",
            },
          },
          required: [
            "merchant_account_id",
            "first_name",
            "last_name",
            "protocols",
            "email",
            "mobile",
            "country_code",
          ],
        },
      },
      upsertBeneficiary: {
        merchant: true,
        description: "Add/update beneficiary",
        method: Account.upsertBeneficiary,
        schema: {
          type: "object",
          properties: {
            protocols: {
              type: "array",
              items: [
                {
                  type: "string",
                },
              ],
            },
            first_name: {
              type: "string",
              minLength: 1,
            },
            last_name: {
              type: "string",
              minLength: 1,
            },
            email: {
              type: "string",
            },
            country_code: {
              type: "string",
            },
            mobile: {
              type: "string",
            },
            acc_no: {
              type: "string",
            },
            ifsc: {
              type: "string",
            },
            bank_name: {
              type: "string",
            },
            network_type: {
              type: "string",
            },
            crypto_wallet_address: {
              type: "string",
            },
            description: {
              type: "string",
            },
            vpa: {
              type: "string",
            },
          },
          required: [
            "first_name",
            "last_name",
            "protocols",
            "country_code",
            "acc_no",
            "ifsc",
          ],
        },
      },
      getBeneficiaries: {
        merchant: true,
        description: "Get beneficiaries",
        method: Account.getBeneficiaries,
        schema: {
          merchant_account_id: {
            type: "string",
            format: "uuid",
          },
        },
        required: [""],
      },
      removeBeneficiariesAccount: {
        description: "remove beneficiaries",
        method: Account.removeBeneficiariesAccount,
        schema: {
          type: "object",
          properties: {
            beneficiary_id: {
              type: "string",
              format: "uuid",
            },
          },
          required: ["beneficiary_id"],
        },
      },
      createAcquirerBankAccount: {
        description: "Create acquirer bank account",
        method: Account.createAcquirerBankAccount,
      },
      assignAPICreds: {
        description: "Assign API Creds",
        method: Account.assignAPICreds,
        schema: {
          api_credentials: { type: "string" },
          merchant_account_id: { type: "string" },
        },
        required: ["api_credentials", "merchant_account_id"],
      },
      decryptSecretKeys: {
        description: "Decrypt secret keys",
        method: Account.decryptSecretKeys,
        schema: {
          secretKeys: {
            type: "array",
            items: {
              type: "string",
            },
          },
        },
      },
      createNewSettlement: {
        description: "create new settlement ",
        user: true,
        method: Account.createNewSettlement,
        schema: {
          type: "object",
          properties: {
            merchant_account_id: {
              type: "string",
            },
            src_currency: {
              type: "string",
            },
            dst_currency: {
              type: "string",
            },
            src_amount: {
              type: "string",
            },
            dst_amount: {
              type: "string",
            },
            markup_rate: {
              type: "string",
            },
            conversion_rate: {
              type: "string",
            },
          },
          required: [
            "merchant_account_id",
            "markup_rate",
            "dst_amount",
            "dst_currency",
            "src_currency",
            "src_amount",
            "conversion_rate",
          ],
        },
      },
      updateSettlementDetails: {
        description: "update settlement details",
        method: Account.updateSettlementDetails,
        schema: {
          type: "object",
          properties: {
            id: {
              type: "string",
            },
            settlement_id: {
              type: "string",
            },
            tr_src_amount: {
              type: ["string", "number"],
            },
            tr_dst_amount: {
              type: ["string", "number"],
            },
            src_amount: {
              type: ["string", "number"],
            },
            dst_amount: {
              type: ["string", "number"],
            },
            markup_rate: {
              type: ["string", "number"],
            },
            conversion_rate: {
              type: ["string", "number"],
            },
            exchange_id: {
              type: "string",
            },
          },
          required: [
            "conversion_rate",
            "markup_rate",
            "dst_amount",
            "id",
            "settlement_id",
            "tr_src_amount",
            "tr_dst_amount",
            "src_amount",
          ],
        },
      },
      exportSettlementReport: {
        description: "create new settlement ",
        method: Account.exportSettlementReport,
        schema: {
          type: "object",
          properties: {
            merchant_account_id: {
              type: "string",
            },
          },
        },
      },
      fetchMerchantAccountWalletBalance: {
        description: "fetch merchant account total wallet balance",
        method: Account.fetchMerchantAccountWalletBalance,
        schema: {
          src_currency: {
            type: "string",
          },
          merchant_account_id: {
            type: "string",
          },
          w_type: {
            type: "string",
          },
        },
        required: ["merchant_account_id", "w_type", "src_currency"],
      },
      _getFeeTransfers: {
        description: "fetch total fee transfers",
        method: Account._getFeeTransfers,
        schema: {
          from_date: {
            type: "string",
          },
          merchant_account_id: {
            type: "date",
          },
          to_date: {
            type: "date",
          },
        },
        required: [],
      },
      exportUsingJasper: {
        realm: true,
        user: true,
        description: "Report generated as per jasper template",
        method: jasperExport.exportUsingJasper,
        schema: {
          type: "object",
          properties: {
            report_data: {
              type: "object",
              description: "pass data ",
              properties: {
                data: {
                  type: "object",
                  description: "table data",
                  items: {
                    type: "object",
                  },
                },
              },
            },
            filename: {
              type: "string",
              description: "filename user required after downloading the file",
            },
            report_name: {
              type: "string",
              description:
                "report_name is the template name present in jasper server",
            },
            report_format: {
              type: "string",
              description: "report_format should be pdf or xlsx ",
            },
          },
          required: ["report_data", "filename", "report_name", "report_format"],
        },
      },
      downloadRecords: {
        realm: true,
        user: true,
        description: "Report generated as per jasper template",
        method: jasperExport.downloadRecords,
        schema: {
          type: "object",
        },
      },
      checkValidRealmId: {
        description: "Check valid realm by id",
        method: Account.checkValidRealmId,
        schema: {
          type: "object",
          properties: {},
          required: [],
        },
      },
    };
  }
}
