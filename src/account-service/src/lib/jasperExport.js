// import jasperReport from "./index";
import config from "@lib/config";
import FileProvider from "@lib/fileprovider";
import db from "@lib/db";

const axios = require("axios");

async function downloadRecords(data, service) {
  const transaction = await db.sequelize.transaction();
  try {
    let sqlQuery = "";
    const Filters = [];

    if (data.activity) {
      sqlQuery += sqlQuery
        ? ` AND t.activity = '${data.activity}'`
        : ` WHERE t.activity = '${data.activity}'`;
      Filters.push({
        name: "Status",
        value: data.activity
      });
    }
    if (data.merchant_account_id) {
      sqlQuery += sqlQuery
        ? ` AND t.merchant_account_id = '${data.merchant_account_id}'`
        : ` WHERE t.merchant_account_id = '${data.merchant_account_id}'`;
      Filters.push({
        name: "Merchant Account",
        value: data.merchant_account_id
      });
    }

    if ((data.startDate && data.endDate) || data.ctime) {
      let startDate, endDate;
      if (data.startDate && data.endDate) {
        startDate = new Date(data.startDate);
        endDate = new Date(data.endDate);
      } else if (data.ctime) {
        startDate = new Date(data.ctime);
        endDate = new Date(data.ctime);
      }
      startDate.setHours(0, 0, 0, 0);
      endDate.setHours(23, 59, 59, 999);
      const maxDuration = 3 * 30 * 24 * 60 * 60 * 1000; // 3 months in milliseconds
      const duration = endDate.getTime() - startDate.getTime();
      if (duration > maxDuration) {
        throw new service.ServiceError("BANJO_ERR_MAX_DATE_FILTER_LIMIT");
      }
      const startDateString = startDate
        .toISOString()
        .replace("T", " ")
        .replace("Z", " +0530");
      const endDateString = endDate
        .toISOString()
        .replace("T", " ")
        .replace("Z", " +0530");

      sqlQuery += sqlQuery
        ? ` AND t.ctime>= '${startDateString}' AND ctime<= '${endDateString}' AND t.mtime>= '${startDateString}'`
        : ` WHERE t.ctime>= '${startDateString}' AND ctime<= '${endDateString}' AND t.mtime>= '${startDateString}'`;
      Filters.push(
        {
          name: "Start Time",
          value: startDateString
        },
        {
          name: "End Time",
          value: endDateString
        }
      );
    }

    if (data.last_brn) {
      sqlQuery += sqlQuery
        ? ` AND t.last_brn = '${data.last_brn}'`
        : ` WHERE t.last_brn = '${data.last_brn}'`;
      Filters.push({
        name: "BRN",
        value: data.last_brn
      });
    }
    if (data.src_amount) {
      sqlQuery += sqlQuery
        ? ` AND t.src_amount = ${data.src_amount}`
        : ` WHERE t.src_amount = ${data.src_amount}`;
      Filters.push({
        name: "Source Amount",
        value: data.src_amount
      });
    }
    if (data.src_currency) {
      sqlQuery += sqlQuery
        ? ` AND t.src_currency = '${data.src_currency}'`
        : ` WHERE t.src_currency = '${data.src_currency}'`;
      Filters.push({
        name: "Source Currency",
        value: data.src_currency
      });
    }
    if (data.transfer_type) {
      sqlQuery += sqlQuery
        ? ` AND t.transfer_type = '${data.transfer_type}'`
        : ` WHERE t.transfer_type = ${data.transfer_type}`;
      Filters.push({
        name: "Transaction Type",
        value: data.transfer_type
      });
    }
    if (data.order_num) {
      sqlQuery += sqlQuery
        ? ` AND t.order_num = '${data.order_num}'`
        : ` WHERE t.order_num = '${data.order_num}'`;
      Filters.push({
        name: "Order Num",
        value: data.order_num
      });
    }
    if (data.ref_num) {
      sqlQuery += sqlQuery
        ? ` AND t.ref_num = '${data.ref_num}'`
        : ` WHERE t.ref_num = '${data.ref_num}'`;
      Filters.push({
        name: "Ref Num",
        value: data.ref_num
      });
    }
    if (data.src_acc_name) {
      sqlQuery += sqlQuery
        ? ` AND t.src_acc_name = '${data.src_acc_name}'`
        : ` WHERE t.src_acc_name = '${data.src_acc_name}'`;
      Filters.push({
        name: "Account Name",
        value: data.src_acc_name
      });
    }
    if (data.src_macc_name) {
      sqlQuery += sqlQuery
        ? ` AND t.src_macc_name = '${data.src_macc_name}'`
        : ` WHERE t.src_macc_name = '${data.src_macc_name}'`;
      Filters.push({
        name: "Merchant Account Name",
        value: data.src_macc_name
      });
    }
    let transactionListQuery = `select t.transfer_id as "TRANSACTION_ID", mtime as "MTIME", src_amount as "SOURCE_AMOUNT", src_currency as "SOURCE_CURRENCY", ref_num as "REF_NUM", account_id as "ACCOUNT_ID", merchant_account_id as "MERCHANT_ACCOUNT_ID", order_num as "ORDER_NUM", transfer_type as "TRANSACTION_TYPE", description as "DESCRIPTION", src_macc_name as "MACC_NAME", src_acc_name as "ACC_NAME", activity as "STATUS", last_brn as "BRN"
   from ${db.schema}.vw_tx_transfer t ${sqlQuery}`;

    let summaryListQuery = `select count(t.id) as "COUNT",
        t.transfer_type as "TRANSACTION_TYPE",
t.activity as "RESULT",
sum(t.src_amount) as "AMOUNT",
t.src_currency as "CURRENCY" 
from
${db.schema}.vw_tx_transfer t ${sqlQuery} group by
t.activity,
t.src_currency,
t.transfer_type `;
    let overviewQuery = `SELECT
    t.transfer_type AS "TRANSACTION_TYPE",
    'Total' AS "Result",
    SUM(t.src_amount) AS "TOTAL_AMOUNT",
    t.src_currency,
    COUNT(CASE WHEN t.activity IN ('APPROVED', 'SETTLEMENT_APPROVED') THEN 1 END) AS "Completed Count",
    COUNT(t.transfer_id) AS "TOTAL",
    ROUND(COUNT(CASE WHEN t.activity IN ('APPROVED', 'SETTLEMENT_APPROVED') THEN 1 END) * 100.0 / COUNT(t.transfer_id), 2) AS "CONVERSION_RATIO"
    FROM
    ${db.schema}.vw_tx_transfer t ${sqlQuery} GROUP BY
    t.src_currency,
    t.transfer_type`;

    const TransactionList = await db.sequelize.query(
      `${transactionListQuery}`,
      {
        replacements: {},
        raw: true
      }
    );
    const summary = await db.sequelize.query(`${summaryListQuery}`, {
      replacements: {},
      raw: true
    });
    const ConversionRatio = await db.sequelize.query(`${overviewQuery}`, {
      replacements: {},
      raw: true
    });
    if (!TransactionList) {
      throw new service.ServiceError("BANJO_ERR_TRANSACTION_LIST_IS_EMPTY");
    }
    if (TransactionList && !TransactionList[0].length) {
      throw new service.ServiceError("BANJO_ERR_TRANSACTION_LIST_IS_EMPTY");
    }
    if (!Filters.length && TransactionList[0].length > config.reportListCount) {
      throw new service.ServiceError("BANJO_ERR_NEED_TO_ADD_DATE_FILTER");
    }
    if (!data.startDate && TransactionList[0].length > config.reportListCount) {
      throw new service.ServiceError("BANJO_ERR_MAX_DATE_FILTER_LIMIT");
    }

    let reportData = {
      report_format: data.type,
      report_name: "rashipay_transaction_report_excel",
      filename: data.filename || "",
      report_data: {
        LABELS: {
          TRANSACTION_TYPE: "Transaction Type",
          RESULT: "Result",
          AMOUNT: "Amount",
          CURRENCY: "Currency",
          SOURCE_AMOUNT: "Source Amount",
          SOURCE_CURRENCY: "Source Currency",
          START_TIME: "Start Time",
          FINISH_TIME: "Finish Time",
          TRANSACTION_ID: "Transaction Id",
          MTIME: "Date & Time",
          REF_NUM: "Ref Num",
          ACCOUNT_ID: "Account Id",
          MERCHANT_ACCOUNT_ID: "Merchant Account Id",
          ORDER_NUM: "Order Num",
          DESCRIPTION: "Description",
          MACC_NAME: "Merchant Account Name",
          ACC_NAME: "Account Name",
          STATUS: "Status",
          BRN: "BRN",
          CONVERSION_RATIO: "Conversion Ratio",
          TOTAL: "Total",
          COUNT: "Count",
          SUMMARY: "Summary",
          TRANSACTION_LIST: "Transaction List",
          STATISTICS: "Statistics",
          FILTERS: "Filters"
        },
        data: {
          summary: summary[0],
          TransactionList: TransactionList[0],
          ConversionRatio: ConversionRatio[0],
          Filters: Filters
        }
      }
    };
    return exportUsingJasper(reportData);
  } catch (error) {
    console.log("Users.js acceptUserInvitation error: " + error);
    await transaction.rollback();
    throw error;
  }
}
async function exportUsingJasper(report) {
  const resReport = await _getReport(report);
  if (!resReport) return { success: false };
  if (resReport.error || !resReport.success) {
    return {
      success: false,
      error: resReport.error || ""
    };
  }

  return {
    success: true,
    code: resReport.code,
    data: resReport.data, //base64 data
    name: resReport.name,
    downloadUrl: config.file_url.baseUrl + "" + resReport.code
  };
}

async function _getReport(report, realmId, userId) {
  let filename = report.filename ? report.filename : "Rashipay_Report";
  let requestParams = {
    report_name: report.report_name,
    report_data: report.report_data,
    report_format: report.report_format
  };
  const res = await _getJasperReport(
    { filename, requestParams: requestParams },
    realmId,
    userId
  );
  return res ? res : null;
}
async function makeFile(data, type, filename) {
  type = type.toLowerCase();
  const file = {};
  file.data = `data:application/${type};base64,${data}`;
  file.name = `${filename}.${type}`;
  return await FileProvider.push(file, 3000);
}
async function _getJasperReport(data, realmId, userId) {
  try {
    const response = await axios.post(
      config.jasperServiceUrl,
      data.requestParams,
      {
        httpsAgent: {
          rejectUnauthorized: false
        }
      }
    );

    if (response.data && !response.data.file && response.data.code) {
      return { error: response.data.code };
    }

    if (response.data) {
      const myFile = await makeFile(
        response.data.file,
        response.data.file_extension,
        data.filename
      );
      return {
        ...myFile,
        data: response.data //base64 data
      };
    }
    return null;
  } catch (error) {
    return {
      success: false,
      error: error.code
    };
  }
}
export default {
  exportUsingJasper,
  downloadRecords
};
