import db from "@lib/db";
import Util from "@lib/shared";
import config from "@lib/config";
import Queue from "@lib/queue";
import crypto from "crypto";

/**
 * @method fetchUserInviteList
 * @author Vaibhav Vaidya
 * @since 13 Jan 2022
 * @summary Fetch List of all transfers from Transfers/Transactions table
 */
async function fetchTransferList(data, service) {
  try {
    if (!service || !service.userId) {
      console.log(
        `INFO. func:fetchUserInviteList. API requires User logged in for access.`
      );
      throw "BANJO_ERR_ACCESSTOKEN";
    }
    let where,
      whereCond = ["tt.removed=0"],
      sqlPlaceholders = [];
    if (data.filters) {
      if (data.filters.ctime || data.filters.date_time) {
        let date1 = new Date(data.filters.ctime || data.filters.date_time);
        date1.setHours(0, 0, 0, 0);
        let date2 = new Date(data.filters.ctime || data.filters.date_time);
        date2.setHours(23, 59, 59, 0);
        sqlPlaceholders.push(date1);
        whereCond.push(`tt.ctime>=$${sqlPlaceholders.length}`);
        sqlPlaceholders.push(date2);
        whereCond.push(`tt.ctime<=$${sqlPlaceholders.length}`);
      }
      if (data.filters.order_id) {
        sqlPlaceholders.push(data.filters.order_id);
        whereCond.push(`tt.order_num=$${sqlPlaceholders.length}`);
      }
      if (data.filters.transaction_id) {
        sqlPlaceholders.push(data.filters.transaction_id);
        whereCond.push(`tt.transfer_id=$${sqlPlaceholders.length}`);
      }
      if (data.filters.amount != undefined && data.filters.amount) {
        sqlPlaceholders.push(data.filters.amount);
        whereCond.push(`tt.src_amount=$${sqlPlaceholders.length}`);
      }
      if (data.filters.status) {
        sqlPlaceholders.push(data.filters.status);
        whereCond.push(`tt.activity=$${sqlPlaceholders.length}`);
      }
      if (data.filters.type) {
        sqlPlaceholders.push(data.filters.type);
        whereCond.push(`tt.type=$${sqlPlaceholders.length}`);
      }
      if (data.filters.account_id) {
        sqlPlaceholders.push(data.filters.account_id);
        whereCond.push(`tt.account_id=$${sqlPlaceholders.length}`);
      }
      if (data.filters.merchant_account_id) {
        sqlPlaceholders.push(data.filters.merchant_account_id);
        whereCond.push(`tt.merchant_account_id=$${sqlPlaceholders.length}`);
      }
    }
    if (service.realmId) {
      sqlPlaceholders.push(service.realmId);
      whereCond.push(`tt.realm_id=$${sqlPlaceholders.length}`);
    }
    let count = 0;
    where = `where ${whereCond.join(" AND ")}`;
    sqlPlaceholders.push(service.userId);
    if (data.sort && data.sort.column) {
      let column = data.sort.column || "ctime";
      let type = data.sort.type || "desc";
      if (column === "order_id") {
        where += ` order by tt.order_num ${type}`;
      } else {
        where += ` order by tt.${column} ${type}`;
      }
    } else {
      where += " order by tt.ctime desc";
    }
    //Vaibhav Vaidya, 18 Jan 2023, Remove inner join with permission matrix
    //inner join ${db.schema}.vw_user_macc_access acc on (tt.src_acc_id=acc.merchant_account_id and acc.user_id=$${sqlPlaceholders.length})
    let sqlQuery = `select tt.*, tt.order_num as order_id
    from ${db.schema}.vw_tx_transfer tt
    inner join ${db.schema}.user_accounts ua on (tt.merchant_account_id = ua.merchant_account_id and  tt.account_id=ua.account_id and ua.removed=0 and ua.end_date is null and ua.user_id=$${sqlPlaceholders.length})
    ${where}`;
    console.log("sqlQuery", sqlQuery);
    let countQuery = `select count(*) from (${sqlQuery}) data`;
    const countResp = await db.sequelize.query(countQuery, {
      bind: sqlPlaceholders,
      raw: true,
      type: db.sequelize.QueryTypes.SELECT
    });
    if (countResp && countResp[0] && !isNaN(countResp[0].count)) {
      count = countResp[0].count;
    }
    let limit = data.page_size || 10,
      pageNumber = data.page || 1;

    sqlPlaceholders.push(limit * (pageNumber - 1));
    let limitQuery = ` offset $${sqlPlaceholders.length}`;
    sqlPlaceholders.push(limit);
    limitQuery += ` limit $${sqlPlaceholders.length}`;

    const transferList = await db.sequelize.query(sqlQuery + limitQuery, {
      bind: sqlPlaceholders,
      raw: true,
      type: db.sequelize.QueryTypes.SELECT
    });
    return { success: true, count, list: transferList };
  } catch (error) {
    throw error;
  }
}

export default {
  fetchTransferList
};
