// require("dotenv").config({ path: "../../../../.env" });
// import { Sequelize, DataTypes, fn, col, Op } from "sequelize";
// import queue from "@lib/queue";

// const sequelize = new Sequelize(process.env.DB_CONN_STRING, {
//   define: {
//     schema: process.env.DB_SCHEMA,
//   },
// });

// const Student = sequelize.define("Student", {
//   id: {
//     type: DataTypes.INTEGER,
//     primaryKey: true,
//     autoIncrement: true,
//   },
//   name: {
//     type: DataTypes.STRING,
//   },
// });

// const StudentMarks = sequelize.define("StudentMarks", {
//   id: {
//     type: DataTypes.INTEGER,
//     primaryKey: true,
//     autoIncrement: true,
//   },
//   studentId: {
//     type: DataTypes.INTEGER,
//     references: {
//       model: "Student",
//       key: "id",
//     },
//   },
//   marks: {
//     type: DataTypes.INTEGER,
//   },
// });

// // Subscribe to the 'studentId' queue
// queue.subscribe("studentId", async (msg) => {
//   const studentId = msg.studentId;

//   const result = await aggregateDataForStudentMicroB({ studentId }, null);
//   console.log("Sending combined result to microservice A:", result);

//   queue.publish("combinedResult", result);
// });

// async function aggregateDataForStudentMicroB(data, service) {
//   try {
//     const studentId = data.studentId;

//     const studentInfo = await Student.findByPk(studentId);

//     const studentMarks = await StudentMarks.findAll({
//       where: {
//         studentId: studentId,
//       },
//       attributes: [[fn("sum", col("marks")), "totalMarks"]],
//     });

//     const studentTotalMarks = studentMarks[0].dataValues.totalMarks;

//     const otherStudentMarks = await StudentMarks.findAll({
//       where: {
//         studentId: {
//           [Op.not]: studentId,
//         },
//       },
//       attributes: [[fn("sum", col("marks")), "totalMarks"]],
//     });

//     const otherStudentTotalMarks = otherStudentMarks[0].dataValues.totalMarks;

//     const topper = studentTotalMarks > otherStudentTotalMarks;

//     const combinedResult = {
//       studentInfo,
//       isTopper: topper,
//     };

//     return combinedResult;
//   } catch (err) {
//     console.error(err);
//     throw err;
//   }
// }

// export default { aggregateDataForStudentMicroB };
//import PassStud from "../../gate-service/lib/Request.js";

import Queue from "@lib/queue";
import Users from "./Users";

//const Crypto = shared.Crypto;

require("dotenv").config({ path: "../../../../.env" });
import { Sequelize, DataTypes, fn, col, Op } from "sequelize";

const sequelize = new Sequelize(process.env.DB_CONN_STRING, {
  define: {
    schema: process.env.DB_SCHEMA,
  },
});

const Student = sequelize.define("Student", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name: {
    type: DataTypes.STRING,
  },
});

const StudentMarks = sequelize.define("StudentMarks", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  studentId: {
    type: DataTypes.INTEGER,
    references: {
      model: "Student",
      key: "id",
    },
  },
  marks: {
    type: DataTypes.INTEGER,
  },
});

async function aggregateDataForStudentMicroB(data, service) {
  try {
    // async function doJob(service, method, data, options = {}) {
    //   const res = await Queue.newJob(service, {
    //     method,
    //     data,
    //     options: { ...options, scope: "gate", ua: this.ua },
    //   });

    //   if (res.error) {
    //     this.error(res.error);
    //     return null;
    //   } else {
    //     return res.result;
    //   }
    // }
    // async function passStudentId(studentId) {
    //   const resultData = await this.doJob(
    //     "merchant-service",
    //     "DetailsOfStudent",
    //     data.data,
    //     {
    //       realmId,
    //       header: data.header,
    //       httpHeaders: this.request.headers,
    //       ip: this.getIp(),
    //     }
    //   );
    //   return studentId;
    // }
    //-----------------------------------------------------------------
    // identifier: `${camelCasedServiceName}.${data.options.header.method}`,
    // token: data.options.header.token,

    var student = await Queue.newJob("merchant-service", {
      method: "DetailsOfStudent",
      data: {
        filters: {
          student_id1: data.studentId,
        },
      },
      options: {
        //userId: service.userId,
        realmId: service.realmId,
      },
    });
    console.log("--------------------------------------------->", student);
    //const studentId = await passStudentId(data.studentId);
    let studentId = student.studentId;
    const studentInfo = await Student.findByPk(studentId);

    const studentMarks = await StudentMarks.findAll({
      where: {
        studentId: studentId,
      },
      attributes: [[fn("sum", col("marks")), "totalMarks"]],
    });

    const studentTotalMarks = studentMarks[0].dataValues.totalMarks;

    const otherStudentMarks = await StudentMarks.findAll({
      where: {
        studentId: {
          [Op.not]: studentId,
        },
      },
      attributes: [[fn("sum", col("marks")), "totalMarks"]],
    });

    const otherStudentTotalMarks = otherStudentMarks[0].dataValues.totalMarks;

    const topper = studentTotalMarks > otherStudentTotalMarks;

    const combinedResult = {
      studentInfo,
      isTopper: topper,
    };

    return combinedResult;
  } catch (err) {
    console.error(err);
    throw err;
  }
}

export default { aggregateDataForStudentMicroB };
