import db from "@lib/db";
import accounts from "@lib/db/models/accounts";
import Queue from "@lib/queue";
import config from "@lib/config";
import Users from "./Users";
import Wallets from "./Wallets";
import crypto from "crypto";
// import Acquirer from "./Acquirer";
// import createMerchantAccount from "./Users";

// import createMerchantAccount from "./Users";
const Op = db.Sequelize.Op;

async function _getChildAccounts(accountId) {
  const list = await childAccountsInArray(accountId);
  for (const account of list) {
    account.self = account.id === accountId;
    account.children = [];
    for (const child of list) {
      if (child.pid === account.id) {
        child.is_child = true;
        account.children.push(child);
      }
    }
  }
  return list.filter((item) => !item.is_child);
}
async function getChildAccounts(data, service) {
  return await _getChildAccounts(service.accountId);
}
async function childAccountsInArray(accountId) {
  const [
    list,
  ] = await db.sequelize.query(
    `SELECT id,name,type,pid FROM ${db.schema}.vw_children_accounts WHERE :accountId=ANY(path)`,
    { replacements: { accountId } }
  );
  return list;
}
function getChildAccountsInArray(data, service) {
  return childAccountsInArray(service.accountId);
}
async function getMerchants(data, service) {
  try {
    let userAccountsQuery = `SELECT distinct on (account_id) id,account_id, email,account_name,u_a_ctime,legal_entity,primary_role as role,type,status, acc_status,start_date FROM ${db.schema}.vw_user_accounts WHERE id= :userId and merchant_account_id IS NOT NULL`;
    let filters = { userId: service.userId };
    if (service.realmId) {
      filters.realm_id = service.realmId;
      userAccountsQuery += ` AND realm_id= :realm_id`;
    }
    let limit = 6;
    let pageNumber = 1;
    if (data.page) {
      pageNumber = data.page;
    }

    if (data.page_size) {
      limit = data.page_size;
    }
    const userAccountsTotalCount = await db.sequelize.query(
      `SELECT COUNT(*) FROM (${userAccountsQuery}) AS user_accounts`,
      {
        replacements: filters,
        raw: true,
      }
    );
    const userAccounts = await db.sequelize.query(
      `${userAccountsQuery} limit ${limit} offset ${limit * (pageNumber - 1)};`,
      {
        replacements: filters,
        raw: true,
      }
    );
    const returnData = {
      success: true,
      count: userAccountsTotalCount[0][0].count,
    };
    if (userAccounts && userAccounts.length) {
      returnData.userAccounts = userAccounts[0];
    } else {
      returnData.userAccounts = [];
    }
    return returnData;
  } catch (error) {
    throw error;
  }
}
async function checkValidRealmId(data, service) {
  try {
    if (service.realmId) {
      const realm = await db.realm.findOne({
        where: { id: service.realmId, removed: 0 },
        attributes: ["status"],
        raw: true,
      });
      if (!realm) {
        throw new service.ServiceError("BANJO_ERR_INVALID_REALM");
      } else if (realm.status !== "ACTIVE") {
        throw new service.ServiceError("BANJO_ERR_REALM_IS_INACTIVE");
      }
    }
    return true;
  } catch (error) {
    throw error;
  }
}
async function fetchMerchantAccountDetails(data, service) {
  //Input:
  // merchant_account_id
  // For transactions list: page: 1 indexed (default 0 if not provided), page_size: (default 10 if not provided)
  try {
    let limit = 5;
    let pageNumber = 1;
    if (data.page) {
      pageNumber = data.page;
    }

    if (data.page_size) {
      limit = data.page_size;
    }

    if (!data.merchant_account_id) {
      throw new service.ServiceError("BANJO_ERR_INVALID_REQUEST");
    }

    //Get merchant account details:
    const merchant_account_details = await db.sequelize.query(
      `select ma.name as merchant_name, ma.acc_no as account_number, ma.status as status, ma.currency, ma.settlement_currency from ${db.schema}.merchant_accounts ma
      where ma.id = '${data.merchant_account_id}' and ma.removed=0;`,
      { raw: true }
    );
    if (merchant_account_details[0] && !merchant_account_details[0].length) {
      throw new service.ServiceError("BANJO_ERR_INVALID_MERCHANT_ACCOUNT");
    }
    const balance = await db.sequelize.query(
      `select coalesce(sum(w.balance),0) as balance, w.type, w.currency from ${db.schema}.wallets w where w.merchant_account_mapping_id in (select mam.id from ${db.schema}.merchant_account_mappings mam where mam.merchant_account_id = '${data.merchant_account_id}' and mam.removed=0 and start_time is not null ) and (w.type='WALLET' or w.type='COLLECTION') and w.removed = 0 and (w.currency='${merchant_account_details[0][0].currency}' or w.currency='${merchant_account_details[0][0].settlement_currency}') group by w.type,w.currency;`,
      { raw: true }
    );

    //Get transactions list:
    var transactionList = await Queue.newJob("account-service", {
      method: "transaction-list",
      data: {
        page: pageNumber,
        page_size: limit,
        filters: {
          merchant_account_id: data.merchant_account_id,
        },
      },
      options: {
        userId: service.userId,
        realmId: service.realmId,
      },
    });
    if (transactionList.error) {
      throw transactionList.error;
    }

    let responseObject = {
      ...merchant_account_details[0][0],
      balance: balance[0],
      transaction_list_count: transactionList.result
        ? transactionList.result.count
        : 0,
      transaction_list: transactionList.result
        ? transactionList.result.list
        : [],
    };

    return responseObject;
  } catch (error) {
    throw error;
  }
}

async function fetchMerchantAccounts(data, service) {
  //Input: page: 1 indexed (default 0 if not provided), page_size: (default 10 if not provided)
  try {
    let limit = 10;
    let pageNumber = 1;
    let realmConditionalSQL = ``;
    if (data.page) {
      pageNumber = data.page;
    }

    if (data.page_size) {
      limit = data.page_size;
    }
    if (service.realmId) {
      realmConditionalSQL = ` and a.realm_id = '${service.realmId}' `;
    }

    let get_merchant_accounts_list_query = `select
	ma.id as merchant_account_id,
	ma.name as merchant_name,
	ma.acc_no as account_number,
	ma.status,
	a.name as account_name,
  (select
		count(id)
	from
		${db.schema}.merchant_account_mappings mam2 
	where
		mam2.merchant_account_id = ma.id and mam2.start_time is not null
	and mam2.end_time is null
	and mam2.removed = 0) as bank_account_count,
	SUM(w.balance) as balance,
	MAX(w.currency) as currency
from
	${db.schema}.merchant_accounts ma
inner join ${db.schema}.accounts a on
	a.id = ma.account_id and a.removed=0 ${realmConditionalSQL}
left join ${db.schema}.merchant_account_mappings mam on
	mam.merchant_account_id = ma.id
	and mam.start_time is not null
  and mam.removed=0
left join ${db.schema}.wallets w on
	w.merchant_account_mapping_id = mam.id
	and w.type = 'COLLECTION'
  and w.removed=0
where
    ma.removed=0 and
	ma.account_id in (
	select
		account_id
	from
		${db.schema}.vw_user_accounts
	where
		id = '${service.userId}' and ma.id = merchant_account_id ) `;
    if (data.account_id) {
      get_merchant_accounts_list_query += `and account_id ='${data.account_id}' `;
    }
    get_merchant_accounts_list_query += "group by ma.id, a.name";
    const merchant_accounts_count = await db.sequelize.query(
      `select count(*) from (${get_merchant_accounts_list_query}) as merchant_accounts;`,
      { raw: true }
    );

    const merchant_accounts = await db.sequelize.query(
      `${get_merchant_accounts_list_query} limit ${limit} offset ${limit *
        (pageNumber - 1)};`,
      {
        raw: true,
      }
    );

    if (merchant_accounts.length && merchant_accounts[0].length) {
      let respData = merchant_accounts[0].map((ma) => {
        if (!ma.balance) {
          ma.balance = 0.0;
        }

        if (parseInt(ma.bank_account_count) < 1) {
          ma.bank_account_associated = false;
        } else {
          ma.bank_account_associated = true;
        }

        return ma;
      });
      return { count: merchant_accounts_count[0][0].count, list: respData };
    } else {
      console.log(
        "File: Account.js Func: fetchMerchantAccounts, Err: Merchant account does not exist"
      );
      throw new service.ServiceError("POLY_ERR_ACCOUNT_DOES_NOT_EXIST");
    }
  } catch (error) {
    console.log(
      "File: Account.js Func: fetchMerchantAccounts, Err: Something went wrong"
    );
    throw error;
  }
}

async function createAccount(data, service) {
  var txControl;
  if (data.transactionObject) {
    txControl = data.transactionObject;
  } else {
    txControl = await db.sequelize.transaction();
  }
  let filter = {};
  if (service.realmId) {
    await checkValidRealmId({ realm_id: service.realmId }, service);
    filter.realm_id = service.realmId;
  }
  let accounts_type = await db.sequelize.query(
    `SELECT enum_range(NULL::${db.schema}.enum_accounts_type) as accounts_type;`
  );
  if (!!accounts_type[1].rowCount) {
    accounts_type = accounts_type[1].rows[0].accounts_type;
  }

  let accounts_status = await db.sequelize.query(
    `SELECT enum_range(NULL::${db.schema}.enum_accounts_status) as accounts_status;`
  );
  if (!!accounts_status[1].rowCount) {
    accounts_status = accounts_status[1].rows[0].accounts_status;
  }

  if (data.status.length > 0 && !accounts_status.includes(data.status)) {
    throw new service.ServiceError("INVALID_USER_DATA");
  }
  if (data.type.length > 0 && !accounts_type.includes(data.type)) {
    throw new service.ServiceError("INVALID_USER_DATA");
  }

  const userData = {
    name: data.name,
    address: data.address,
    city: data.city,
    country: data.country,
    legal_entity: data.legal_entity,
    registration_no: data.registration_no,
    type: data.type,
    status: data.status,
    variables: { _arr: data.variables },
    realm_id: data.realm_id,
  };
  if (data.plan_id) {
    userData.plan_id = data.plan_id;
  } else {
    const res = await db.realm.findOne({
      where: { id: data.realm_id },
      attributes: ["tariff"],
      raw: true,
    });
    userData.plan_id = res && res.tariff ? res.tariff : null;
  }
  let dbresp = null;
  if (data.id) {
    dbresp = await db.account.findOne({ where: { id: data.id } });
  }

  let nameExists = await db.account.findOne({
    where: { name: data.name, removed: 0, ...filter },
  });

  if (data.id && dbresp) {
    //Edit/update flow

    userData.mtime = new Date().toISOString();
    userData.updater = !!data.owner ? data.owner : null;

    if (nameExists && nameExists.id != data.id) {
      console.error(
        "Account-service: createAccount, File: Account.js, Update Error: Name already in use."
      );

      await txControl.rollback();
      throw new service.ServiceError("INVALID_USER_DATA");
    }

    try {
      await db.account.update(userData, {
        where: { id: data.id },
        transaction: txControl,
      });
      await txControl.commit();
      return { id: data.id, message: "Account updated successfully" };
    } catch (error) {
      console.error(
        "Account-service: createAccount, File: Account.js, Update Error:",
        typeof error == "object" ? JSON.stringify(error) : error
      );
      await txControl.rollback();
      throw new service.ServiceError("INVALID_USER_DATA");
    }
  } else {
    //Create flow
    userData.ctime = new Date().toISOString();
    userData.mtime = new Date().toISOString();
    userData.maker = !!data.owner ? data.owner : null;

    if (nameExists) {
      console.error(
        "Account-service: createAccount, File: Account.js, Update Error: Name already in use."
      );

      throw new service.ServiceError("INVALID_USER_DATA");
    }

    try {
      //saving in db
      let createResp = await db.account.create(userData, {
        transaction: txControl,
      });
      return { id: createResp.id, message: "Account created successfully" };
    } catch (error) {
      console.error(
        "Account-service: createAccount, File: Account.js, Create Error:",
        typeof error == "object" ? JSON.stringify(error) : error
      );
      throw new service.ServiceError("INVALID_USER_DATA");
    }
  }
}

async function associateBankAccount(data, service) {
  const Op = db.Sequelize.Op;
  const transaction = await db.sequelize.transaction();
  const now = new Date();
  const where = {
    start_time: { [Op.lt]: now },
    removed: 0,
    [Op.or]: [{ end_time: { [Op.eq]: null } }, { end_time: { [Op.gt]: now } }],
  };
  const realmFilter = {};
  try {
    let query = `select ba.status from ${db.schema}.bank_accounts ba`;
    if (service.realmId) {
      await checkValidRealmId({ realm_id: service.realmId }, service);
      realmFilter.realm_id = service.realmId;
      query += ` join ${db.schema}.banks b on ba.bank_id = b.id and b.removed=0 and b.realm_id = :realm_id `;
    }
    const checkActiveStatus = await db.sequelize.query(
      ` ${query} where ba.id=:bank_account_id and ba.removed=0 `,
      {
        replacements: { bank_account_id: data.bank_account_id, ...realmFilter },
        raw: true,
      }
    );
    if (!checkActiveStatus) {
      throw new service.ServiceError("BANJO_ERR_INVALID_BANK_ACCOUNT");
    }
    if (
      checkActiveStatus &&
      checkActiveStatus[0].length &&
      checkActiveStatus[0][0].status !== "ACTIVE"
    ) {
      throw new service.ServiceError(
        "BANJO_ERR_INVALID_BANK_ACCOUNT_STATUS_SHOULD_ACTIVE"
      );
    }
    const checkMercAccalReadyToBankAcc = await db.merchant_account_mappings.findOne(
      {
        where: {
          merchant_account_id: data.merchant_account_id,
          bank_account: data.bank_account_id,
          ...where,
        },
        attributes: ["id"],
        transaction,
        lock: true,
      }
    );
    if (checkMercAccalReadyToBankAcc) {
      throw new service.ServiceError(
        "BANJO_ERR_MERCHANT_ACCOUNT_IS_AlREADY_ALLCOCATE_TO_THIS_BANK_ACCOUNT"
      );
    }

    const maMapping = await db.merchant_account_mappings.create(
      {
        merchant_account_id: data.merchant_account_id,
        bank_account: data.bank_account_id,
        start_time: now,
        end_time: null,
      },
      { transaction, raw: true }
    );
    //Vaibhav Vaidya, 12 Jan 2023, Add creation of Wallets for MerchantAccount-BankAccount linking
    service.transaction = transaction;
    if (maMapping && maMapping.id) {
      //Vaibhav Vaidya note, DB view "vw_merchant_account_mappings" cannot be used since mapping id is not comitted
      let fetchMerchAcc = await db.sequelize.query(
        `select map.id, map.account_name, map.currency, map.bank_name
        from ${db.schema}.vw_bank_accounts map
        where map.removed=0 and map.id=$1`,
        {
          bind: [data.bank_account_id],
          type: db.sequelize.QueryTypes.SELECT,
          raw: true,
        }
      );
      if (fetchMerchAcc && fetchMerchAcc[0] && fetchMerchAcc[0].id) {
        data.currency = fetchMerchAcc[0].currency;
        data.bank_account_name = fetchMerchAcc[0].account_name;
        data.merchant_account_mapping_id = maMapping.id;
        await Wallets.attachWallets(data, service);
      } else {
        console.error(
          `Account.js. Func:associateBankAccount. Failed to fetch bank details by Id:${data.bank_account_id}`
        );
        console.error(
          `Account.js. Func:associateBankAccount. Failed to create Attached walelt for Merchant Account Mapping Id:${maMapping.id}`
        );
      }
    }
    await transaction.commit();
    return { id: maMapping.id };
  } catch (err) {
    await transaction.rollback();
    console.error(
      `Account.js. Func:associateBankAccount. Caught Exception:`,
      err
    );
    throw err;
  }
}
async function releaseBankAccount(data, service) {
  const now = new Date();
  const maMapping = await db.merchant_account_mappings.update(
    {
      end_time: now,
    },
    { where: { id: data.mapping_id } }
  );
  if (maMapping[0]) {
    return { success: true };
  } else {
    throw new service.ServiceError("BANJO_ERR_INVALID_ACC_MAPPING");
  }
}

async function onboardMerchant(data, service) {
  if (!!data) {
    //Create transaction object
    const txControl = await db.sequelize.transaction();
    data.transactionObject = txControl;
    try {
      if (service.realmId) {
        await checkValidRealmId({ realm_id: service.realmId }, service);
      }
      console.log("Merchant registration started.");
      //Plan is part of basic_info
      data.basic_info.transactionObject = txControl;
      var createResp = await createAccount(data.basic_info, service);
      console.log(
        "Merchant account created. Creating merchant sub-accounts.",
        createResp.id
      );

      let acc_nos = [];
      for await (var merchant_account of data.merchant_accounts) {
        merchant_account.transactionObject = txControl;
        merchant_account.account_id = createResp.id;
        var createMerchantResp = await Users.createMerchantAccount(
          merchant_account,
          service
        );

        acc_nos.push({
          id: createMerchantResp.id,
          acc_no: createMerchantResp.acc_no,
          bank_account_id: merchant_account.bank_account_id,
        });
      }
      console.log("Sub-account creation completed. Inviting owner.");

      for await (let user_invite of data.invited_users) {
        user_invite.account_id = createResp.id;
        user_invite.merchant_account_ids = acc_nos.map((e) => e.id);
        var resp = await Queue.newJob("auth-service", {
          method: "inviteUser",
          data: user_invite,
          options: {
            realmId: service.realmId,
          },
        });
        if (resp.error) {
          throw resp.error;
        }
      }
      console.log("Owner invitation completed.");
      await txControl.commit();
      console.log("Merchant onboarded successfully");

      console.log("Assigning bank accounts to merchants");
      let allBanksAssigned = true;
      try {
        for await (let acc_data of acc_nos) {
          //Vaibhav Vaidya, Add condition to only call associate Bank account if bank_account_id present
          if (acc_data.bank_account_id != undefined) {
            var maResp = await db.merchant_accounts.findOne({
              where: {
                acc_no: acc_data.acc_no,
              },
            });
            if (maResp) {
              await associateBankAccount(
                {
                  merchant_account_id: maResp.dataValues.id,
                  bank_account_id: acc_data.bank_account_id,
                },
                service
              );
            }
          }
        }
      } catch (error) {
        console.log("Bank account assignment error:", error);
        allBanksAssigned = false;
      }
      console.log("Bank account assignment successful");
      return {
        id: createResp.id,
        message: "Merchant onboarded successfully",
        allBanksAssigned: allBanksAssigned,
      };
    } catch (error) {
      console.log("Merchant onboarding unsuccessful");
      await txControl.rollback();
      console.error(
        "Func: onboardMerchant, File: account.js, Error: ",
        typeof error == "object" ? JSON.stringify(error) : error
      );
      throw "BANJO_ERR_MERCHANT_ONBOARD";
    }
  } else {
    console.log(`Func: onboardMerchant. Missing data. Data:`, data);
    throw "BANJO_ERR_MERCHANT_DATA_MISSING";
  }
}

/**
 * @method checkBasicMerchant
 * @author Vaibhav Vaidya
 * @since 23 Dec 2022
 * @summary Check for uniqueness for name, registration_no values during Merchant creation
 */
async function checkBasicMerchant(data, service) {
  let fieldName;
  if (!!data) {
    let sqlReplacements = [];
    let sql = ` select u.id, u.name, u.registration_no from ${db.schema}.accounts u
    where u.removed=0`;
    if (service.realmId) {
      sqlReplacements.push(service.realmId);
      sql += ` and u.realm_id = $${sqlReplacements.length}`;
    }
    if (typeof data.name == "string" && data.name.length) {
      sqlReplacements.push(data.name);
      sql += ` and u.name ilike $${sqlReplacements.length}`;
      fieldName = `name`;
    } else if (
      typeof data.registration_no == "string" &&
      data.registration_no.length
    ) {
      sqlReplacements.push(data.registration_no);
      sql += ` and u.registration_no = $${sqlReplacements.length}`;
      fieldName = `registration_no`;
    } else {
      console.error(
        "Account.js. Func:checkBasicMerchant. InsufficientParameters, data:",
        data
      );
      throw "BANJO_ERR_INCOMPLETE_PARAMS";
    }
    let checkResp = await db.sequelize.query(sql, {
      bind: sqlReplacements,
      type: db.sequelize.QueryTypes.SELECT,
      raw: true,
    });
    if (checkResp && checkResp.length) {
      return {
        error: [
          {
            field: fieldName,
            message: "Entry already exists, Update value to be unique",
          },
        ],
      };
    } else {
      return { success: 1 };
    }
  } else {
    console.log(`Func: onboardMerchant. Missing data. Data:`, data);
    throw "BANJO_ERR_MERCHANT_DATA_MISSING";
  }
}
async function upsertAccountRole(data, service) {
  try {
    let filter = {};
    let upsertData = {
      role_name: data.role_name,
      permissions: data.permissions,
      account_id: data.account_id,
      type: data.type,
    };
    if (service.realmId) {
      await checkValidRealmId({ realm_id: service.realmId }, service);
      filter.realm_id = service.realmId;
      upsertData.realm_id = service.realmId;
    }
    const Op = db.Sequelize.Op;
    data.role_name = data.role_name.trim();
    await checkIsExistUsersAccount(data, service);
    let roles = await db.role.findOne({
      attributes: ["id", "role_name", "permissions"],
      where: {
        [Op.or]: [{ account_id: data.account_id }, { type: 0 }],
        removed: 0,
        role_name: {
          [Op.iLike]: data.role_name,
        },
        ...filter,
      },
    });
    if (roles) {
      if (!data.id || (data.id && data.id !== roles.id)) {
        throw new service.ServiceError("BANJO_ERR_ROLE_ALREADY_EXIST");
      }
    }
    if (data.id) {
      const roles = await db.role.findOne({
        attributes: ["id", "role_name", "type"],
        where: {
          account_id: data.account_id,
          removed: 0,
          id: data.id,
          ...filter,
        },
      });
      if (!roles) {
        throw new service.ServiceError("BANJO_ERR_INVALID_ROLE");
      } else if (roles.type === 0) {
        throw new service.ServiceError(
          "BANJO_ERR_LOGIN_USER_DOES_NOT_ACC_TO_UPDATE_ADMIN_CREATED_ROLES"
        );
      }
      const role = await db.role.update(upsertData, {
        where: { id: data.id },
        returning: true,
      });
      return { success: true, message: "Role updated successfully" };
    } else {
      const role = await db.role.create(upsertData);
      return {
        success: true,
        message: "Role created successfully",
        id: role.id,
      };
    }
  } catch (err) {
    console.error(`Account.js. Func:upsertAccountRole. Caught Exception:`, err);
    throw err;
  }
}
async function removeAccountRole(data, service) {
  try {
    const Op = db.Sequelize.Op;
    let filter = { removed: 0, account_id: data.account_id };
    if (service.realmId) {
      bank_accounts_protocols;
      await checkValidRealmId({ realm_id: service.realmId }, service);
      filter.realm_id = service.realmId;
    }
    await checkIsExistUsersAccount(data, service);
    const roles = await db.role.findOne({
      attributes: ["id", "role_name"],
      where: {
        ...filter,
        id: data.id,
      },
    });
    if (!roles) {
      throw "BANJO_ERR_LOGIN_USER_DOES_NOT_ACC_TO_REMOVE_USER_ACCOUNT";
    }
    const checkRoleAssigenToAnyUser = await db.user_accounts.findAll({
      where: {
        account_id: data.account_id,
        main_role: data.id,
        removed: 0,
      },
    });
    if (checkRoleAssigenToAnyUser.length) {
      throw "BANJO_ERR_ROLE_ALREADY_ASSIGNED_TO_USER_ACCOUNT";
    }
    const role = await db.role.update(
      { removed: 1 },
      {
        where: { id: data.id },
      }
    );
    return { success: true, message: "Role removed successfully" };
  } catch (err) {
    console.error(`Account.js. Func:removeAccountRole. Caught Exception:`, err);
    throw err;
  }
}
async function fetchAccountRole(data, service) {
  try {
    const Op = db.Sequelize.Op;
    const limit = Math.min(parseInt(data.page_size) || 100, 200);
    let page = Math.max(parseInt(data.page) || 1);
    const offset = (page - 1) * limit;

    const filter = { account_id: data.account_id };
    let query = `select id,role_name , account_id,type, coalesce((permissions -> 'role'->'view')::text,false::text) as view, coalesce((permissions -> 'role'->'create')::text,false::text) as create, coalesce((permissions -> 'role'->'edit')::text,false::text) as edit, coalesce((permissions -> 'role'->'delete')::text,false::text) as delete from ${db.schema}.roles where removed = 0 and (type = 0 or (type = 1 and account_id = :account_id)) `;
    let filterQuery = "";
    if (service.realmId) {
      filter.realm_id = service.realmId;
      query += ` and realm_id = :realm_id `;
    }
    const temp = ["view", "create", "delete", "edit"];
    for (let i = 0; i < temp.length; i++) {
      if (data[temp[i]]) {
        if (filterQuery.length) {
          filterQuery += " or ";
        } else if (filterQuery.length === 0) {
          filterQuery += "and ( ";
        }
        filterQuery += `(permissions -> 'role'->'${temp[i]}')::text = 'true'`;
      }
    }
    if (filterQuery.length) {
      filterQuery += " )";
      query += filterQuery;
    }
    query += " ORDER BY role_name";
    const count = await db.sequelize.query(
      `SELECT COUNT(*) FROM (${query}) AS count_roles`,
      {
        replacements: filter,
        raw: true,
      }
    );

    const roles = await db.sequelize.query(
      `${query} limit ${limit} offset ${offset};`,
      {
        replacements: filter,
        raw: true,
      }
    );
    const returnData = {
      success: true,
      count: count[0][0].count,
    };
    if (roles && roles.length) {
      returnData.roles = roles[0];
    } else {
      returnData.roles = [];
    }
    return { ...returnData, has_next: count > limit + offset };
  } catch (err) {
    console.error(`Account.js. Func:fetchAccountRole. Caught Exception:`, err);
    throw err;
  }
}
async function attachBankAccountProtocols(data, service) {
  try {
    if (service.realmId) {
      await checkValidRealmId({ realm_id: service.realmId }, service);
    }
    for (let i = 0; i < data.payment_data.length; i++) {
      const exists = await db.bank_accounts_protocols.findOne({
        where: {
          bank_account_id: data.bank_account_id,
          payment_protocol_id: data.payment_data[i].payment_protocol,
        },
      });
      if (exists) {
        const bank_accounts_protocols = await db.bank_accounts_protocols.update(
          {
            identifier: data.payment_data[i].identifier,
          },
          {
            where: {
              bank_account_id: data.bank_account_id,
              payment_protocol_id: data.payment_data[i].payment_protocol,
            },
          }
        );
      } else {
        const bank_accounts_protocols = await db.bank_accounts_protocols.create(
          {
            bank_account_id: data.bank_account_id,
            payment_protocol_id: data.payment_data[i].payment_protocol,
            identifier: data.payment_data[i].identifier,
          }
        );
      }
    }
  } catch (error) {
    console.log(
      "File: Account.js, Func: attachBankAccountProtocols, error: ",
      error
    );
    throw error;
  }
  return true;
}

async function attachBankAccountLimits(data, service) {
  try {
    let limitData = {
      currency: data.currency,
      min_amount: data.min_amount,
      max_amount: data.max_amount,
      limit_type: data.limit_type,
      daily_deposit: data.daily_deposit,
    };
    if (service.realmId) {
      await checkValidRealmId({ realm_id: service.realmId }, service);
      limitData.realm_id = service.realmId;
    }
    let { limit, bankAccountProtocol } = await _checkIfLimitExists(
      data,
      service
    );
    if (limit) {
      limit = await db.ref_limits.update(limitData, {
        where: { id: limit },
      });
    } else {
      if (bankAccountProtocol) {
        limit = await db.ref_limits.create({
          ...limitData,
          protocol_id: data.payment_protocol_id,
          bank_account_protocol_id: bankAccountProtocol.id,
        });
      } else {
        console.log(
          "File: Account.js, Func: attachBankAccountLimits, error: bank_account_protocol does not exist, bank doesn't support protocol"
        );
        throw new service.ServiceError(
          "BANJO_ERR_ACCOUNT_DOES_NOT_SUPPORT_PROTOCOL"
        );
      }
    }
    return { success: !!limit };
  } catch (error) {
    console.log(
      "File: Account.js, Func: attachBankAccountLimits, error: ",
      error
    );
    throw error;
  }
}

async function _checkIfLimitExists(data, service) {
  let filter = { removed: 0 };
  if (service.realmId) {
    filter.realm_id = service.realmId;
  }
  const bankAccountProtocol = await db.bank_accounts_protocols.findOne({
    where: {
      bank_account_id: data.bank_account_id,
      removed: 0,
      payment_protocol_id: data.payment_protocol_id,
    },
  });
  let limit;
  if (bankAccountProtocol) {
    limit = await db.ref_limits.findOne({
      where: {
        ...filter,
        bank_account_protocol_id: bankAccountProtocol.id,
        limit_type: data.limit_type,
      },
    });
  }
  return { limit: limit ? limit.id : null, bankAccountProtocol };
}

async function getAccountUsers(data, service) {
  const limit = Math.min(parseInt(data.page_size) || 100, 200);
  let page = Math.max(parseInt(data.page) || 1);
  const offset = (page - 1) * limit;
  const filter = {
    account_id: data.account_id,
    removed: 0,
    // acc_status: db.account.STATUS.ACTIVE
  };
  let userAccountsQuery = "";
  let userAccountSubQuery = "";
  let userIdSubQuery = "";
  if (data.user_id) {
    filter.id = data.user_id;
    userAccountsQuery += "SELECT vua.id,  ";
    userAccountSubQuery += " and vua.id = :id";
  } else {
    //Ajit note: Remove distinct on clause when user -> Merchant account relationship mapping is added
    filter.id = service.userId;
    userAccountsQuery += "SELECT distinct on (vua.id)  ";
    userIdSubQuery += "and vua.id != :id";
  }
  userAccountsQuery += `vua.username,vua.user_type,vua.id,vua.account_id,vua.first_name,vua.last_name,vua.email,vua.account_name,vua.user_account_id,vua.legal_entity,vua.primary_role as role,vua.type,vua.status,vua.start_date,vua.main_role,vua.user_account_status, ma.id as merchant_acc_id, ma.name as merchant_name FROM ${db.schema}.vw_user_accounts vua inner join ${db.schema}.merchant_accounts ma on (vua.merchant_account_id=ma.id and ma.removed=0) WHERE vua.account_id= :account_id and vua.removed= 0  and 
  ma.account_id in (select account_id from ${db.schema}.vw_user_accounts where
    id = '${service.userId}' and ma.id = merchant_account_id )`;

  if (service.realmId) {
    filter.realm_id = service.realmId;
    userAccountsQuery += ` and vua.realm_id= :realm_id `;
  }
  if (data.search_text) {
    filter.search_text = `%${data.search_text}%`;
    userAccountsQuery +=
      " and (email ILIKE :search_text or first_name ILIKE :search_text or last_name ILIKE :search_text)";
  }
  if (data.merchant_account_ids && data.merchant_account_ids.length) {
    userAccountsQuery +=
      " AND ma.id IN (" +
      data.merchant_account_ids.map((id) => `'${id}'`) +
      ")";
  }
  userAccountsQuery += userAccountSubQuery;
  userAccountsQuery += userIdSubQuery;
  const count = await db.sequelize.query(
    `SELECT COUNT(*) FROM (${userAccountsQuery}) AS user_accounts`,
    {
      replacements: filter,
      raw: true,
    }
  );

  const users = await db.sequelize.query(
    `${userAccountsQuery} limit ${limit} offset ${offset};`,
    {
      replacements: filter,
      raw: true,
    }
  );
  const returnData = {
    success: true,
    count: count[0][0].count,
  };
  if (users && users.length) {
    returnData.users = users[0];
  } else {
    returnData.users = [];
  }
  return {
    ...returnData,
    has_next: count > limit + offset,
  };
}
async function removeAccountUser(data, service) {
  try {
    const filter = {
      account_id: data.account_id,
      removed: 0,
      // acc_status: db.account.STATUS.ACTIVE
    };
    let userAccountsQuery = `SELECT user_type,id,account_id,first_name,last_name,email,account_name,user_account_id,legal_entity,primary_role as role,type,status,start_date,main_role FROM ${db.schema}.vw_user_accounts WHERE account_id= :account_id and removed= 0 `;

    if (service.realmId) {
      await checkValidRealmId({ realm_id: service.realmId }, service);
    }
    const checkUsersAccountRole = await db.sequelize.query(
      `${userAccountsQuery} and id=:id`,
      {
        replacements: { ...filter, id: service.userId },
        raw: true,
      }
    );
    if (
      (checkUsersAccountRole &&
        checkUsersAccountRole.length &&
        !checkUsersAccountRole[0][0]) ||
      !checkUsersAccountRole[0][0].role
    ) {
      throw "BANJO_ERR_INVALID_USER";
    }
    if (
      (checkUsersAccountRole[0][0] &&
        checkUsersAccountRole[0][0].role &&
        checkUsersAccountRole[0][0].role !== "Owner") ||
      checkUsersAccountRole[0][0].user_type !== "PORTAL"
    ) {
      throw "BANJO_ERR_LOGIN_USER_DOES_NOT_ACC_TO_REMOVE_USER_ACCOUNT";
    }
    const users = await db.sequelize.query(
      `${userAccountsQuery} and user_account_id=:user_account_id`,
      {
        replacements: { ...filter, user_account_id: data.user_account_id },
        raw: true,
      }
    );

    if (users && users.length && users[0].length) {
      await db.user_accounts.update(
        { removed: 1, end_date: new Date() },
        {
          where: {
            id: data.user_account_id,
          },
        }
      );
      return { success: true, message: "User account remove successfully" };
    } else {
      throw "BANJO_ERR_INVALID_USER_ACCOUNT_ID";
    }
  } catch (err) {
    console.error(`Account.js. Func:removeAccountUser. Caught Exception:`, err);
    throw err;
  }
}
async function checkIsExistUsersAccount(data, service) {
  try {
    const Op = db.Sequelize.Op;
    const filter = {
      account_id: data.account_id,
      removed: 0,
      // acc_status: db.account.STATUS.ACTIVE
    };
    let userAccountsQuery = `SELECT user_type,id,account_id,first_name,last_name,email,account_name,user_account_id,legal_entity,primary_role as role,type,status,start_date,main_role FROM ${db.schema}.vw_user_accounts WHERE account_id= :account_id and removed= 0 `;

    const checkUsersAccountRole = await db.sequelize.query(
      `${userAccountsQuery} and id=:id`,
      {
        replacements: { ...filter, id: service.userId },
        raw: true,
      }
    );
    if (
      (checkUsersAccountRole &&
        checkUsersAccountRole.length &&
        !checkUsersAccountRole[0][0]) ||
      !checkUsersAccountRole[0][0].role
    ) {
      throw "BANJO_ERR_INVALID_USER";
    }
    if (
      (checkUsersAccountRole[0][0] &&
        checkUsersAccountRole[0][0].role &&
        checkUsersAccountRole[0][0].role !== "Owner") ||
      checkUsersAccountRole[0][0].user_type !== "PORTAL"
    ) {
      throw "BANJO_ERR_LOGIN_USER_DOES_NOT_ACC_TO_UPDATE_USER_ACCOUNT";
    }
  } catch (err) {
    console.error(
      `Account.js. Func:checkIsExistUsersAccount. Caught Exception:`,
      err
    );
    throw err;
  }
}
async function updateUserAccount(data, service) {
  try {
    const filter = {
      account_id: data.account_id,
      removed: 0,
    };
    let userAccountsQuery = `SELECT vua.id , ma.id as merchant_acc_id, ma.name as merchant_name FROM ${db.schema}.vw_user_accounts vua inner join ${db.schema}.merchant_accounts ma on (vua.account_id=ma.account_id and ma.removed=0) WHERE vua.account_id= :account_id and vua.removed= 0`;

    if (data.merchant_account_id) {
      filter.merchant_account_id = data.merchant_account_id;
      userAccountsQuery += ` and ma.id= :merchant_account_id`;
    }
    if (service.realmId) {
      await checkValidRealmId({ realm_id: service.realmId }, service);
    }
    await checkIsExistUsersAccount(data, service);
    const users = await db.sequelize.query(
      `${userAccountsQuery} and user_account_id=:user_account_id`,
      {
        replacements: { ...filter, user_account_id: data.user_account_id },
        raw: true,
      }
    );
    const updateData = {};

    if (users && users.length && users[0].length) {
      if (data.main_role) updateData.main_role = data.main_role;
      if (data.status) updateData.status = data.status;
      await db.user_accounts.update(updateData, {
        where: {
          id: data.user_account_id,
        },
      });

      return { success: true, message: "User account update successfully" };
    } else {
      throw "BANJO_ERR_INVALID_USER_ACCOUNT_ID";
    }
  } catch (err) {
    console.error(
      `Account.js. Func:checkIsExistUsersAccount. Caught Exception:`,
      err
    );
    throw err;
  }
}
async function usersAccountRoleVerification(data, service) {
  try {
    const filter = {
      account_id: data.account_id,
      removed: 0,
      // acc_status: db.account.STATUS.ACTIVE
    };
    let userAccountsQuery = `SELECT primary_role as role,main_role FROM ${db.schema}.vw_user_accounts WHERE account_id= :account_id and removed= 0 `;

    const checkUsersAccountRole = await db.sequelize.query(
      `${userAccountsQuery} and id=:id`,
      {
        replacements: { ...filter, id: service.userId },
        raw: true,
      }
    );
    if (
      checkUsersAccountRole &&
      checkUsersAccountRole.length &&
      !checkUsersAccountRole[0][0]
    ) {
      throw "BANJO_ERR_INVALID_USER";
    }
    if (data.role_id) {
      if (
        checkUsersAccountRole[0][0] &&
        checkUsersAccountRole[0][0] &&
        checkUsersAccountRole[0][0].main_role === data.role_id
      ) {
        return {
          success: true,
          role_verified: true,
          role_id: checkUsersAccountRole[0][0].main_role,
          role_name: checkUsersAccountRole[0][0].role,
        };
      } else
        return {
          success: true,
          role_verified: false,
          role_id: checkUsersAccountRole[0][0].main_role,
          role_name: checkUsersAccountRole[0][0].role,
        };
    } else if (
      checkUsersAccountRole[0][0] &&
      checkUsersAccountRole[0][0].role &&
      checkUsersAccountRole[0][0].role === "Owner"
    ) {
      return {
        success: true,
        role_verified: true,
        role_id: checkUsersAccountRole[0][0].main_role,
        role_name: checkUsersAccountRole[0][0].role,
      };
    }
    return {
      success: true,
      role_verified: false,
      role_id: checkUsersAccountRole[0][0].main_role,
      role_name: checkUsersAccountRole[0][0].role,
    };
  } catch (err) {
    console.error(
      `Account.js. Func:usersAccountRoleVerification. Caught Exception:`,
      err
    );
    throw err;
  }
}

async function logBankAccountActivity(data, service) {
  try {
    if (service.realmId) {
      await checkValidRealmId({ realm_id: service.realmId }, service);
    }
    const activityLog = await db.bank_account_activity_logs.create(data);
    return { success: !!activityLog };
  } catch (error) {
    console.log(`Func: logBankAccountActivity. Error: `, error);
    throw error;
  }
}

async function deleteAccountLimit(data, service) {
  try {
    const filter = {
      bank_account_protocol_id: data.bank_account_protocol_id,
      removed: 0,
    };
    if (service.realmId) {
      await checkValidRealmId({ realm_id: service.realmId }, service);
      filter.realm_id = service.realmId;
    }
    const limitExists = await db.ref_limits.findOne({
      where: filter,
    });
    if (!limitExists) {
      throw new service.ServiceError("BANJO_ERR_CANNOT_DELETE_GLOBAL_LIMITS");
    }

    let limit = await db.ref_limits.destroy({
      where: {
        ...filter,
        protocol_id: data.payment_protocol_id,
      },
    });
    return { success: !!limit };
  } catch (error) {
    console.log(`Func: deleteAccountLimit. Error: `, error);
    throw error;
  }
}

async function addBankTemplate(data, service) {
  let templateExists, bank_template;
  try {
    let templateObject = {
      bank_id: data.bank_id,
      template_name: data.template_name,
      template: data.template,
      description: data.description,
    };
    if (service.realmId) {
      await checkValidRealmId({ realm_id: service.realmId }, service);
    }
    if (data.protocol_id && data.protocol_id !== "") {
      templateObject.protocol_id = data.protocol_id;
    }
    if (data.id && data.id.length) {
      templateExists = await db.banks_bulk_templates.findOne({
        where: { id: data.id },
      });
    }
    if (templateExists) {
      bank_template = await db.banks_bulk_templates.update(templateObject, {
        where: { id: data.id },
      });
    } else {
      bank_template = await db.banks_bulk_templates.create(templateObject);
    }
    if (!!bank_template) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log("File: Account.js, Func: addBankTemplate. Error: ", error);
    throw error;
  }
}

async function exportPayoutCSV(data, service) {
  try {
    let { transfers, template } = await _getTransfers(data, service);

    return await _createDataObject(
      template.template.headers,
      template.template.mapping,
      transfers[0]
    );
  } catch (error) {
    console.log("File: Account.js, Func: exportPayoutCSV. Error: ", error);
    throw error;
  }
}

async function getQualifiedPayouts(data, service) {
  try {
    let { transfers } = await _getTransfers(data, service);
    return transfers[0];
  } catch (error) {
    throw error;
  }
}

async function _getTransfers(data, service) {
  let query,
    subQuery = "";
  let replacements = {
    bank_account_id: data.bank_account_id,
  };
  if (service.realmId) {
    subQuery = ` vtt.realm_id = :realm_id `;
    replacements.realm_id = service.realmId;
  }
  let from_date, to_date;
  const template = await db.banks_bulk_templates.findOne({
    where: { id: data.template },
  });

  if (template.protocol_id) {
    let protocol = await db.payment_protocols.findOne({
      where: { id: template.protocol_id },
      attributes: ["protocol_type"],
    });
    query = `select * from ${db.schema}.vw_tx_transfer vtt
      join ${db.schema}.transfers_meta tm on tm.transfer_id = vtt.transfer_id and tm.transfer_request->'order'->>'protocol' = :protocol_type
      where vtt.src_bank_acc_id=:bank_account_id and vtt.removed = 0 and vtt.transfer_type = 'PAYOUT'`;
    replacements.protocol_type = protocol.protocol_type;
  } else {
    query = `select * from ${db.schema}.vw_tx_transfer vtt where vtt.src_bank_acc_id=:bank_account_id and vtt.removed = 0`;
  }

  if (data.merchant_account_id && data.merchant_account_id !== "") {
    query += ` and vtt.merchant_account_id in :merchant_account_ids `;
    replacements.merchant_account_ids = [data.merchant_account_id];
  }

  if (data.from_date) {
    from_date = new Date(data.from_date);
    query += ` and vtt.ctime >= :from_date`;
    replacements.from_date = from_date;
  }

  if (data.to_date) {
    to_date = new Date(data.to_date);
    if ((from_date && from_date <= to_date) || !from_date) {
      query += ` and vtt.ctime <= :to_date`;
      replacements.to_date = to_date;
    } else {
      throw new service.ServiceError(
        "BANJO_ERR_FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE"
      );
    }
  }

  query += subQuery;
  const transfers = await db.sequelize.query(query, {
    replacements,
    logging: console.log,
  });
  return { transfers, template };
}

async function _createDataObject(headers, template, transferData) {
  let payoutObject = [];
  if (headers) {
    transferData.map((el) => {
      let tempObject = {};
      template.map((key) => {
        let header = key.header;
        let value = el[key.field_name];
        tempObject[header] = value;
      });
      payoutObject.push(tempObject);
    });
  } else {
    transferData.map((el) => {
      let tempObject = {};
      template.map((key) => {
        let value = el[key.field_name];
        tempObject[`${key.field_name}`] = value;
      });
      payoutObject.push(tempObject);
    });
  }
  return { headers, payoutObject };
}
async function checkProtocolValidation(data, service) {
  try {
    const Op = db.Sequelize.Op;
    if (service.realmId) {
      await checkValidRealmId({ realm_id: service.realmId }, service);
    }
    if (data && data.protocols && data.protocols.length) {
      let payment_protocols = await db.payment_protocols.findAll({
        where: { id: { [Op.in]: data.protocols }, removed: 0 },
      });

      if (!payment_protocols) {
        throw new service.ServiceError("BANJO_ERR_INVALID_PROTOCOL");
      }

      if (payment_protocols.length) {
        const protocolName = payment_protocols.map(
          (e) => e.dataValues.protocol_type
        );
        if (protocolName.indexOf("UPI") >= 0 && !data.vpa) {
          throw new service.ServiceError("BANJO_ERR_PROTOCOL_UPI_VPI_REQUIRED");
        }
        if (protocolName.indexOf("USDT") >= 0) {
          if (!data.network_type)
            throw new service.ServiceError(
              "BANJO_ERR_PROTOCOL_USDT_NETWORK_TYPE_REQUIRED"
            );
          if (!data.crypto_wallet_address)
            throw new service.ServiceError(
              "BANJO_ERR_PROTOCOL_USDT_WALLET_ADDRESS_REQUIRED"
            );
        }
        if (
          protocolName.indexOf("IMPS") >= 0 ||
          protocolName.indexOf("RTGS") >= 0 ||
          protocolName.indexOf("NEFT") >= 0
        ) {
          if (!data.bank_name)
            throw new service.ServiceError(
              "BANJO_ERR_PROTOCOL_BANK_NAME_REQUIRED"
            );
          if (!data.acc_no)
            throw new service.ServiceError(
              "BANJO_ERR_PROTOCOL_ACCOUNT_NUMBER_REQUIRED"
            );
          if (!data.ifsc)
            throw new service.ServiceError("BANJO_ERR_PROTOCOL_IFSC_REQUIRED");
        }
        return payment_protocols;
      } else {
        throw new service.ServiceError("BANJO_ERR_INVALID_PROTOCOL");
      }
    } else {
      throw new service.ServiceError("BANJO_ERR_PROTOCOL_REQUIRED");
    }
  } catch (error) {
    console.log("File: Account.js, Func: addUpdateBeneficiary. Error: ", error);
    throw error;
  }
}
async function fetchProtocolsID(data, service) {
  try {
    const Op = db.Sequelize.Op;
    let protocols = [];
    if (data && data.protocols && data.protocols.length) {
      const protocolsUpperCase = data.protocols.map((protocol) =>
        protocol.toUpperCase()
      );

      let payment_protocols = await db.payment_protocols.findAll({
        where: {
          identifier_label: { [Op.in]: protocolsUpperCase },
          removed: 0,
        },
      });

      if (!payment_protocols) {
        throw new service.ServiceError("BANJO_ERR_INVALID_PROTOCOL");
      }

      if (payment_protocols.length) {
        protocols = payment_protocols.map((e) => e.dataValues.id);
        return protocols;
      }
    }
    return protocols;
  } catch (error) {
    console.log("File: Account.js, Func: fetchProtocolsID. Error: ", error);
    throw error;
  }
}
async function upsertBeneficiary(data, service) {
  try {
    data.protocols = await fetchProtocolsID(data, service);
    data.account_id = service.accountId;
    data.merchant_account_id = service.maId;

    return addUpdateBeneficiary(data, service);
  } catch (error) {
    console.log("File: Account.js, Func: upsertBeneficiary. Error: ", error);
    throw error;
  }
}
async function addUpdateBeneficiary(data, service) {
  let beneficiaryExists, beneficiary;

  try {
    if (service.realmId) {
      await checkValidRealmId({ realm_id: service.realmId }, service);
    }
    const payment_protocols = await checkProtocolValidation(data, service);
    if (!!payment_protocols) {
      if (data.id) {
        beneficiaryExists = await db.beneficiaries.findOne({
          where: { id: data.id },
        });
      }
      let message = "Beneficiaries ";
      if (beneficiaryExists) {
        message += "updated ";
        beneficiary = await db.beneficiaries.update(data, {
          where: { id: data.id },
          returning: true,
        });
      } else {
        data.ctime = Date.now();
        message += "created ";
        beneficiary = await db.beneficiaries.create(data);
      }
      if (!!beneficiary) {
        message += " successfully";
        let returnData = { success: true, message };
        if (data.id) {
          returnData.id = data.id;
        } else if (beneficiary.id) returnData.id = beneficiary.id;
        return returnData;
      } else {
        message += "failed";
        return { success: false, message };
      }
    }
  } catch (error) {
    console.log("File: Account.js, Func: addUpdateBeneficiary. Error: ", error);
    throw error;
  }
}

async function getBeneficiaries(data, service) {
  try {
    const Op = db.Sequelize.Op;
    let filter = { removed: 0, merchant_account_id: service.maId };
    let filterVpa = "";
    let filterSqlProtocol = "";
    let protocolReplacements = {}; // Object to hold replacements for protocol filter
    data.protocols = await fetchProtocolsID(data, service);
    if (data && data.protocols && Array.isArray(data.protocols)) {
      protocolReplacements.protocolFilter = data.protocols; // Add protocol filter to replacements
    }

    if (data.vpa) {
      filter.vpa = data.vpa;
      filterVpa = " and bene.vpa = :vpa";
    }

    if (
      protocolReplacements &&
      protocolReplacements.protocolFilter &&
      protocolReplacements.protocolFilter.length
    ) {
      filterSqlProtocol += ` and bene.protocols && array[:protocolFilter]::uuid[]`; // Use array literal with ::uuid[]
    }
    let query = `select bene.id, bene.account_id, bene.merchant_account_id,bene.protocols,bene.last_name, bene.first_name, bene.email,bene.country_code,bene.mobile, bene.acc_no, bene.ifsc,bene.bank_name, bene.network_type,bene.crypto_wallet_address,bene.description,bene.vpa, a.name as account_name,ma.name as merchant_account_name from ${db.schema}.beneficiaries bene 
    join ${db.schema}.accounts a on a.id = bene.account_id 
    join ${db.schema}.merchant_accounts ma on ma.id = bene.merchant_account_id
    where bene.removed = 0 and bene.merchant_account_id = :merchant_account_id ${filterVpa} ${filterSqlProtocol}`;
    const beneficiaries = await db.sequelize.query(query, {
      replacements: { ...filter, ...protocolReplacements }, // Merge both filter and protocolReplacements
      type: db.sequelize.QueryTypes.SELECT,
      raw: true,
    });
    for (let i = 0; i < beneficiaries.length; i++) {
      let protocols = await db.payment_protocols.findAll({
        attributes: ["id", "identifier_label"],
        where: { id: { [Op.in]: beneficiaries[i].protocols }, removed: 0 },
      });
      beneficiaries[i].protocols = protocols;
    }
    return { success: true, beneficiaries };
  } catch (error) {
    console.log("File: Account.js, Func: getBeneficiaries. Error: ", error);
    throw error;
  }
}

async function removeBeneficiariesAccount(data, service) {
  try {
    if (service.realmId) {
      await checkValidRealmId({ realm_id: service.realmId }, service);
    }
    const beneficiaries = await db.beneficiaries.findOne({
      attributes: ["id"],
      where: {
        id: data.beneficiary_id,
        removed: 0,
      },
    });
    if (!beneficiaries) {
      throw new service.ServiceError(
        "BANJO_ERR_BENEFICIARIES_ACCOUNT_NOT_FOUND"
      );
    }

    let acc = await db.beneficiaries.update(
      { removed: 1 },
      {
        where: { id: data.beneficiary_id },
      }
    );
    return {
      success: true,
      message: "Beneficiary account removed successfully",
    };
  } catch (error) {
    console.log(
      "File: Account.js, Func: removeBeneficiariesAccount. Error: ",
      error
    );
    throw error;
  }
}

async function createNewSettlement(data, service) {
  if (!!data) {
    //Create transaction object
    const txControl = await db.sequelize.transaction();
    try {
      console.log(`Func: createNew settlement ` + data);
      if (service.realmId) {
        await checkValidRealmId({ realm_id: service.realmId }, service);
        data.realm_id = service.realmId;
      }
      let calculateAccountBalance = `SELECT coalesce(sum(w.balance),0) as total_available_balance FROM  ${db.schema}.wallets w 
            join ${db.schema}.vw_merchant_account_mappings mam on mam.id = w.merchant_account_mapping_id and mam.merchant_account_id = :merchant_account_id
            where w.removed = 0 and w.type = 'COLLECTION' and w.currency= :src_currency`;

      const balance = await db.sequelize.query(calculateAccountBalance, {
        replacements: {
          src_currency: data.src_currency,
          merchant_account_id: data.merchant_account_id,
        },
        raw: true,
      });
      if (
        balance &&
        balance.length &&
        balance[0][0].total_available_balance &&
        balance[0][0].total_available_balance < data.src_amount
      ) {
        throw "BANJO_ERR_INVALID_REQUESTED_AMOUNT";
      }
      const merchant = await db.sequelize.query(
        `select a.realm_id, ma.account_id FROM  ${db.schema}.merchant_accounts ma 
        join ${db.schema}.accounts a on a.id = ma.account_id 
         where ma.removed = 0 and ma.id= :merchant_account_id`,
        {
          replacements: {
            merchant_account_id: data.merchant_account_id,
          },
          raw: true,
        }
      );
      if (merchant && merchant.length && !merchant[0][0].realm_id) {
        throw "BANJO_ERR_INVALID_MERCHANT_ACCOUNT";
      }
      let exchange_id = null;
      if (data.src_currency !== data.dst_currency) {
        const srcLedgerAccount = await checkSrcLedgerAccount(data, service);
        const [ledgerAccount, ledgerSrcAccount] = await Promise.all([
          Wallets.getCreateLedgerAccount(
            data.exchange_account_id,
            data.dst_currency,
            service
          ),
          Wallets.getCreateLedgerAccount(
            srcLedgerAccount.id,
            data.src_currency,
            service
          ),
        ]);
        const exchanges = await db.exchange.create(
          {
            ...data,
            applied_rate: data.markup_rate,
            quoted_rate: data.markup_rate,
            actual_rate: data.conversion_rate,
            ledger_wallet_id: ledgerAccount.id,
          },
          {
            transaction: txControl,
          }
        );
        exchange_id = exchanges.id;
      } else {
        delete data.exchange_account_id;
      }
      const settlement = await db.settlements.create(data, {
        transaction: txControl,
      });

      const [walletAccounts] = await Promise.all([
        Wallets.getFetchWalletsAccount(
          data.settlement_data,
          data.src_currency,
          service
        ),
      ]);

      let total_amount = 0;
      for (const walletAccount of walletAccounts) {
        let total_bank_amount = walletAccount.amount;
        if (total_amount === data.src_amount) break;
        for (const wallet of walletAccount.walletRecord) {
          if (total_bank_amount <= 0) break;

          let amount = wallet.balance - total_bank_amount;
          let d = {
            dst_wallet_id: wallet.dst_wallet_id,
            src_wallet_id: wallet.src_wallet_id,
            src_currency: wallet.src_wallet_currency,
            src_amount: amount ? total_bank_amount : wallet.balance,
            dst_currency: wallet.dst_wallet_currency,
            dst_amount: amount ? total_bank_amount : wallet.balance,
            merchant_account_id: data.merchant_account_id,
            bank_account_id: walletAccount.bank_account_id,
            conversion_rate: data.conversion_rate,
            markup_rate: data.markup_rate,
            settlement_id: settlement.id,
            realm_id: merchant[0][0].realm_id,
            account_id: merchant[0][0].account_id,
            exchange_id,
            is_exchange: !!exchange_id,
            data: {
              ua: service.ua,
              ip: service.ip,
              user_ip: data.user_ip,
              return_url: data.return_url,
            },
          };

          const res = await Queue.newJob("transaction-service", {
            method: "settlementTrigger",
            data: {
              action: "SETTLEMENT_REQUEST",
              ...d,
            },
            options: {
              ...service,
              maId: data.merchant_account_id,
              accountId: merchant[0][0].account_id,
              realmId: merchant[0][0].realm_id,
            },
          });
          if (res.result) {
            if (!res.result.transfer_id) {
              await txControl.rollback();
              throw new service.ServiceError("BANJO_ERR_TARIFF_CONFIG");
            }
            Queue.newJob("transaction-service", {
              method: "callTelegramBot",
              data: {
                transfer_id: res.result.transfer_id,
                action_type: data.action_type || "ADMIN",
                admin_id: data.admin_id,
                status: "created",
                activity: "request",
              },
            });
            total_amount += amount ? total_bank_amount : wallet.balance;
            total_bank_amount -= wallet.balance;
            d.transfer_id = res.result.transfer_id;
            // await updateSettlementActivity(d, data, service);
          } else {
            if (
              res.error &&
              res.error.name == "SequelizeUniqueConstraintError" &&
              res.error.errors.filter((e) => e.path == "order_num").length
            ) {
              await txControl.rollback();
              throw new service.ServiceError("BANJO_ERR_UNIQUE_ORDER_ID");
            } else {
              await txControl.rollback();
              throw res.error;
            }
          }
        }
      }

      await txControl.commit();
      return {
        id: settlement.id,
        message: "Settlement created successfully",
        settlement: settlement,
      };
    } catch (error) {
      console.log("Settlement created unsuccessfully");
      await txControl.rollback();
      console.error(
        "Func: createNewSettlement, File: account.js, Error: ",
        typeof error == "object" ? JSON.stringify(error) : error
      );
      throw "BANJO_ERR_SETTLEMENT";
    }
  } else {
    console.log(`Func: createNewSettlement. Missing data. Data:`, data);
    throw "BANJO_ERR_SETTLEMENT_DATA_MISSING";
  }
}
async function checkSrcLedgerAccount(data, service) {
  const bank = await db.bank_accounts.findOne({
    where: {
      id: data.exchange_account_id,
    },
    attributes: ["bank_id"],
  });
  if (!bank) {
    throw new service.ServiceError("BANJO_ERR_EXCHANGE_ACCOUNT_NOT_FOUND");
  }
  const srcLedgerAccount = await db.bank_accounts.findOne({
    where: {
      bank_id: bank.bank_id,
      currency: data.src_currency,
    },
    attributes: ["id"],
  });
  if (!srcLedgerAccount) {
    throw new service.ServiceError("BANJO_ERR_EXCHANGE_SRC_ACCOUNT_NOT_FOUND");
  }
  return srcLedgerAccount;
}
async function updateSettlementActivity(tx, data, service) {
  const res = await Queue.newJob("transaction-service", {
    method: "settlementTrigger",
    data: {
      brn: data.brn,
      transfer_id: tx.transfer_id,
      action: "APPROVE_SETTLEMENT",
      description: tx.description,
      order_id: tx.order_num,
      bank_account_id: tx.bank_account_id,
      dst_wallet_id: tx.dst_wallet_id,
      src_wallet_id: tx.src_wallet_id,
      src_currency: tx.src_currency,
      src_amount: tx.src_amount,
      dst_currency: tx.dst_currency,
      dst_amount: tx.dst_amount,
      settlement_id: tx.settlement_id,
      merchant_account_id: tx.merchant_account_id,
      data: {
        ua: service.ua,
        ip: service.ip,
        user_ip: data.user_ip,
        activity_action: "APPROVE_SETTLEMENT",
      },
    },
    options: {
      ...service,
      realmId: tx.realm_id,
      accountId: tx.account_id,
      maId: tx.merchant_account_id,
    },
  });
  if (res.result) {
    if (!res.result.transfer_id) {
      throw new service.ServiceError("BANJO_ERR_TARIFF_CONFIG");
    }
    return {
      transfer_id: res.result.transfer_id,
    };
  } else {
    if (
      res.error &&
      res.error.name == "SequelizeDatabaseError" &&
      res.error.original &&
      res.error.original.where &&
      res.error.original.where.indexOf("balance_check") >= 0
    ) {
      return {
        success: false,
        error: {
          title: "Insufficient Balance",
          message: "Insufficient balance in source account",
        },
        paymentDetails: tx,
      };
    } else if (
      res.error &&
      res.error.name == "SequelizeUniqueConstraintError" &&
      res.error.original &&
      res.error.original.constraint == "transactions_brn"
    ) {
      return {
        success: false,
        error: {
          title: "BRN already exists into our records",
          message:
            "Duplicate (BRN) bank reference number detected, please contact support",
        },
        paymentDetails: tx,
      };
    } else {
      throw res.error;
    }
  }
}
async function exportSettlementReport(data, service) {
  try {
    let settlements = await db.settlements.findAll({
      where: {
        merchant_account_id: data.merchant_account_id,
        [Op.or]: [{ status: 1 }, { status: 4 }],
        removed: 0,
      },
      attributes: ["id"],
    });

    let settlement_ids = [];
    settlements.forEach((el) => {
      settlement_ids.push(el.id);
    });

    let transfers = await db.transfer.findAll({
      where: { settlement_id: { [Op.in]: settlement_ids } },
      attributes: ["id"],
    });
    let transfer_ids = [];
    transfers.forEach((el) => {
      transfer_ids.push(el.id);
    });

    let transactions = await db.transaction.findAll({
      where: { transfer_id: { [Op.in]: transfer_ids } },
      group: ["activity", "id"],
    });

    return transactions;
  } catch (error) {
    throw error;
  }
}
async function createAcquirerBankAccount(data, service) {
  try {
    let returnObj = {};
    const filter = {};
    if (service.realmId) {
      await checkValidRealmId({ realm_id: service.realmId }, service);
      filter.realm_id = service.realmId;
    }
    let bank = await db.banks.findOne({
      where: { ...filter, id: data.bank },
      attributes: ["bank_type"],
    });

    if (!bank || bank.bank_type !== 2) {
      throw new service.ServiceError("BANJO_ERR_INVALID_REQUEST");
    }
    let exists = await db.api_credentials.findOne({
      where: { id: data.id },
    });
    if (!exists) {
      var account_no = Math.round((Date.now() + Math.random()) * 1000);
      var account_name =
        "Acquirer" + Math.round((Date.now() + Math.random()) * 100);
      const bankAccountData = {
        bank_id: data.bank,
        account_name: account_name,
        acc_no: account_no,
        status: "ACTIVE",
        currency: data.currency,
        payment_provider_id: data.payment_provider_id,
      };
      const acquirerBankAccount = await db.bank_accounts.create(
        bankAccountData
      );
      returnObj.bank_account_id = acquirerBankAccount.id;
    } else {
      returnObj.bank_account_id = exists.bank_account;
    }
    if (data.secret_key) {
      let encryptedSecret = encrypt(data.secret_key);
      returnObj.secret_key = encryptedSecret;
    }

    return returnObj;
  } catch (error) {
    throw error;
  }
}

async function assignAPICreds(data, service) {
  try {
    let api_credentials = await db.api_credentials.findOne({
      where: { id: data.api_credentials },
    });
    if (api_credentials) {
      let associateAccount = await associateBankAccount(
        {
          merchant_account_id: data.merchant_account_id,
          bank_account_id: api_credentials.bank_account,
        },
        service
      );

      let wallet = await db.wallet.findOne({
        where: {
          merchant_account_mapping_id: associateAccount.id,
          type: db.wallet.TYPES.COLLECTION,
        },
      });

      let balance = await getSyncWalletBalance(api_credentials);

      if (balance && balance.amount) {
        await db.wallet.update(
          { balance: balance.amount },
          {
            where: { id: wallet.id },
          }
        );
      }

      return api_credentials;
    } else {
      throw new service.ServiceError("BANJO_ERR_INVALID_REQUEST");
    }
  } catch (error) {
    throw error;
  }
}

function generateRequestData(tree_map) {
  const hashAlgorithm = "sha256";
  let jsonString = JSON.stringify(tree_map);

  let index = jsonString.indexOf("amount");
  if (index !== -1) {
    index = jsonString.indexOf(":", index);
    jsonString = replaceChar(jsonString, "", index + 1);
    index = jsonString.indexOf('"', index);
    jsonString = replaceChar(jsonString, "", index);
  }
  console.log("jsonString", jsonString);
  const hash = generateHashString(jsonString);
  console.log("***********************Hash = ", hash);

  tree_map.hash = hash;
  delete tree_map.secretKey;

  const jsonStringNew = JSON.stringify(tree_map);
  console.log(jsonStringNew + "*********************" + hash);

  return jsonStringNew;
}

function generateHashString(input) {
  const hash = crypto
    .createHash("sha256")
    .update(input)
    .digest("hex");
  const paddedHash = hash.padStart(64, "0");
  return paddedHash;
}

function getLocalIPAddress() {
  return new Promise((resolve, reject) => {
    const http = require("http");

    http
      .get("http://api.ipify.org", (res) => {
        let data = "";

        res.on("data", (chunk) => {
          data += chunk;
        });

        res.on("end", () => {
          console.log(`Your public IP address is: ${data}`);
          resolve(data);
        });
      })
      .on("error", (error) => {
        console.error(
          "File: Account.js, Func: getLocalIPAddress.  Error:",
          error
        );
        reject(error);
      });
  });
}

function decryptSecretKeys(data, service) {
  try {
    data.secretKeys.map((el) => {
      if (el.secret_key) {
        let decryptedSecret = decrypt(el.secret_key);
        el.decryptedSecret = decryptedSecret;
      }
    });
    return data;
  } catch (error) {
    throw error;
  }
}

function decrypt(encryptedText) {
  try {
    const { iv, derivedKey } = getEncryptionConfiguration();
    const decipher = crypto.createDecipheriv("aes-256-cbc", derivedKey, iv);
    let decrypted = decipher.update(encryptedText, "hex", "utf8");
    decrypted += decipher.final("utf8");
    return decrypted;
  } catch (error) {
    console.log("File: Account.js, Func: getLocalIPAddress.  Error:", error);
    return encryptedText;
  }
}

function encrypt(text) {
  try {
    const { iv, derivedKey } = getEncryptionConfiguration();

    const cipher = crypto.createCipheriv("aes-256-cbc", derivedKey, iv);
    let encrypted = cipher.update(text, "utf8", "hex");
    encrypted += cipher.final("hex");
    return encrypted;
  } catch (error) {
    throw error;
  }
}

async function getWalletBalanceDtl(data, service) {
  const secretKey = await decrypt(data.secretKey);

  const tree_map = {
    aggregatorId: data.aggregatorId,
    secretKey,
  };
  const requestData = generateRequestData(tree_map);
  const IPIMEI = await getLocalIPAddress();
  const options = {
    method: "POST",
    url: process.env.BANJOPAY_PAYOUT_BASE_URL + "getWalletBalanceDtl",
    headers: {
      "Content-Type": "application/json",
      IPIMEI,
      "User-Agent": "web",
      SecretSalt: "99m4mhb3cEGuIwIQ2we2Sw==",
    },
    body: requestData,
  };
  return new Promise((resolve, reject) => {
    var request = require("request");

    request(options, function(error, response) {
      if (error) reject(error);
      console.log(response.body);
      if (response.body) resolve(JSON.parse(response.body));
      else resolve(response.body);
    });
  });
}

function getEncryptionConfiguration() {
  const iv = Buffer.from(process.env.API_CREDENTIALS_ENCRYPTION_IV, "hex");
  const salt = Buffer.from(process.env.API_CREDENTIALS_ENCRYPTION_SALT, "hex");
  const derivedKey = crypto.pbkdf2Sync(
    process.env.API_CREDENTIALS_ENCRYPTION_SECRET_KEY,
    salt,
    100000,
    32,
    "sha256"
  );
  return {
    iv,
    derivedKey,
  };
}

async function fetchMerchantAccountWalletBalance(data, service) {
  try {
    let calculateAccountBalance = `SELECT coalesce(sum(w.balance),0) as total_available_balance FROM  ${db.schema}.wallets w 
            join ${db.schema}.vw_merchant_account_mappings mam on mam.id = w.merchant_account_mapping_id and mam.merchant_account_id = :merchant_account_id
            where w.removed = 0 and w.type = :w_type and w.currency= :src_currency`;
    const balance = await db.sequelize.query(calculateAccountBalance, {
      replacements: {
        src_currency: data.src_currency,
        merchant_account_id: data.merchant_account_id,
        w_type: data.w_type,
      },
      raw: true,
    });
    return balance[0][0];
  } catch (error) {
    console.error(
      "Func: fetchMerchantAccountWalletBalance, File: account.js, Error: ",
      typeof error == "object" ? JSON.stringify(error) : error
    );
    throw error;
  }
}

async function updateSettlementDetails(data, service) {
  if (!!data) {
    const txControl = await db.sequelize.transaction();
    try {
      if (service.realmId) {
        await checkValidRealmId({ realm_id: service.realmId }, service);
      }
      await db.settlements.update(
        {
          src_amount: data.src_amount,
          dst_amount: data.dst_amount,
          markup_rate: data.markup_rate,
          conversion_rate: data.conversion_rate,
        },
        {
          where: { id: data.settlement_id },
          transaction: txControl,
        }
      );

      if (data.exchange_id) {
        await db.exchange.update(
          {
            src_amount: data.src_amount,
            dst_amount: data.dst_amount,
            actual_rate: data.markup_rate,
            applied_rate: data.markup_rate,
            quoted_rate: data.conversion_rate,
          },
          {
            where: { id: data.exchange_id },
            transaction: txControl,
          }
        );
      }
      await db.transfer.update(
        {
          src_amount: data.tr_src_amount,
          dst_amount: data.tr_dst_amount,
        },
        {
          where: { id: data.id },
          transaction: txControl,
        }
      );

      await txControl.commit();
      return {
        message: "Settlement details update successfully",
        success: true,
      };
    } catch (error) {
      console.log("Settlement details updation failed");
      await txControl.rollback();
      console.error(
        "Func: updateSettlementDetails, File: account.js, Error: ",
        typeof error == "object" ? JSON.stringify(error) : error
      );
      throw error;
    }
  } else {
    console.log(`Func: updateSettlementDetails. Missing data. Data:`, data);
    throw "BANJO_ERR_SETTLEMENT_DATA_MISSING";
  }
}

async function _getFeeTransfers(data, service) {
  let query,
    replacements = {},
    from_date,
    to_date;

  query = `select sum(trn.amount) as total_fees_applicable ,t.src_currency as currency,
      t.merchant_account_id, coalesce((trn.data ->> 'rate_of_fee')::text,''::text) as rate_per_transaction,coalesce((trn.data ->> 'feetype')::text,''::text) as unit_of_fee,
      count(trn.id) as count_of_respective_transactions ,trn.activity as detail, trn.txtype 
      from ${db.schema}.transactions trn  
      join ${db.schema}.transfers t on t.id = trn.transfer_id
      join ${db.schema}.accounts a on t.account_id = a.id
      where trn.activity in ('FEE','REVERSE_FEE') and trn.removed = 0 
      `;

  if (service.realmId) {
    query += ` and a.realm_id =  :realm_id `;
    replacements.realm_id = service.realmId;
  }
  if (data.merchant_account_id && data.merchant_account_id !== "") {
    query += ` and t.merchant_account_id =  :merchant_account_id `;
    replacements.merchant_account_id = data.merchant_account_id;
  }

  if (data.from_date) {
    from_date = new Date(data.from_date);
    query += ` and trn.ctime >= :from_date`;
    replacements.from_date = from_date;
  }

  if (data.to_date) {
    to_date = new Date(data.to_date);
    if ((from_date && from_date < to_date) || !from_date) {
      query += ` and trn.ctime <= :to_date`;
      replacements.to_date = to_date;
    } else {
      throw new service.ServiceError(
        "BANJO_ERR_FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE"
      );
    }
  }
  query += ` group by t.src_currency,trn.activity,trn.txtype,t.merchant_account_id,trn.data ->> 'rate_of_fee',trn.data ->> 'feetype'`;
  const transfers = await db.sequelize.query(query, {
    replacements,
    type: db.sequelize.QueryTypes.SELECT,
    raw: true,
    logging: console.log,
  });
  return { success: true, transfers };
}

async function getSyncWalletBalance(api_credentials) {
  let balance;
  let category_name = await db.sequelize.query(
    `SELECT mc.name
    FROM ${db.schema}.api_credentials AS ac
    JOIN ${db.schema}.merchant_categories AS mc ON ac.category = mc.id
    WHERE ac.id = :api_credentials_id;
  `,
    { replacements: { api_credentials_id: api_credentials.id } }
  );
  switch (category_name[0][0].name) {
    case "Banjo Pay":
      balance = await getWalletBalanceDtl(
        {
          aggregatorId: api_credentials.aggregator_id,
          secretKey: api_credentials.secret_key,
        },
        service
      );
      break;

    default:
      break;
  }
  return balance;
}

export default {
  _getChildAccounts,
  getChildAccounts,
  getMerchants,
  getChildAccountsInArray,
  fetchMerchantAccountDetails,
  fetchMerchantAccounts,
  createAccount,
  associateBankAccount,
  releaseBankAccount,
  onboardMerchant,
  checkBasicMerchant,
  attachBankAccountProtocols,
  attachBankAccountLimits,
  logBankAccountActivity,
  getAccountUsers,
  removeAccountUser,
  upsertAccountRole,
  removeAccountRole,
  fetchAccountRole,
  usersAccountRoleVerification,
  updateUserAccount,
  deleteAccountLimit,
  exportSettlementReport,
  _getFeeTransfers,
  updateSettlementDetails,
  fetchMerchantAccountWalletBalance,
  addBankTemplate,
  exportPayoutCSV,
  getQualifiedPayouts,
  addUpdateBeneficiary,
  upsertBeneficiary,
  getBeneficiaries,
  removeBeneficiariesAccount,
  createNewSettlement,
  createAcquirerBankAccount,
  assignAPICreds,
  decryptSecretKeys,
  checkValidRealmId,
};
