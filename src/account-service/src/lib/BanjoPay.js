class BanjoPay extends AcquirerBase {
  constructor(aggregator_id, secretKey) {
    super(aggregator_id, secretKey);
  }
  async _hasSufficientBalance(data, service) {
    try {
      const res = await getWalletBalanceDtl(data, service);
      if (res && res.response && res.response === "ERROR") {
        return { success: false, res };
      } else if (res && res.code && res.code === "300") {
        if (res.amount && res.amount >= data.amount) {
          return { success: true, res };
        } else {
          return { success: false, res };
        }
      }
      return { success: false, res: [] };
    } catch (error) {
      console.log(
        "File: Acquirer.js Function: hasSufficientBalance Error: " + error
      );
      throw error;
    }
  }
  async executeAcquirerPayment(data, service, beneficiary, transfer) {
    try {
      if (
        beneficiary &&
        beneficiary.acc_no &&
        beneficiary.bank_name &&
        beneficiary.ifsc &&
        beneficiary.first_name
      ) {
        let res = await this._hasSufficientBalance(
          {
            aggregatorId: this.aggregatorId,
            amount: transfer.src_amount,
            src_currency: transfer.src_currency,
            secretKey: this.secretKey
          },
          service
        );
        let collectionWalletRes = await this._hasCollectionWalletResSufficientBalance(
          {
            merchant_account_id: transfer.merchant_account_id,
            amount: transfer.src_amount,
            src_currency: transfer.src_currency
          },
          service
        );
        if (res.success && collectionWalletRes.success) {
          let transferRequest = {
            accountNo: beneficiary.acc_no,
            aggregatorId: this.aggregatorId,
            amount: transfer.src_amount,
            bankName: beneficiary.bank_name,
            beneName: beneficiary.first_name + " " + beneficiary.last_name,
            ifscCode: beneficiary.ifsc,
            merchantTransId: transfer.transfer_id,
            transferType: transfer.transfer_request.order.protocol,
            secretKey: this.secretKey
          };
          let fundTransferResponse = await fundTransferRequest(
            transferRequest,
            service
          );

          data.activity_action = "PENDING_TX";
          data.transfer_id = transfer.transfer_id;
          data.auto_payout = true;
          if (
            fundTransferResponse &&
            ["300", "005", "006", "007"].indexOf(fundTransferResponse.code) >= 0
          ) {
            let transaction = await Queue.newJob("transaction-service", {
              method: "txActivityPending",
              data,
              options: service
            });
            if (transaction.error && transaction.error.message) {
              console.log(
                `File: Acquirer.js Func:executeAcquirerPayment ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
              );
            }
          } else {
            let transaction = await Queue.newJob("transaction-service", {
              method: "txActivityReject",
              data,
              options: service
            });
            if (transaction.error && transaction.error.message) {
              console.log(
                `File: Acquirer.js Func:executeAcquirerPayment ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
              );
            }
          }
        }
      } else {
        console.log(
          `File: Acquirer.js Func:executeAcquirerPayment ${transfer.transfer_id} beneficiary details missing`
        );
        let transaction = await Queue.newJob("transaction-service", {
          method: "txActivityReject",
          data,
          options: service
        });
        if (transaction.error && transaction.error.message) {
          console.log(
            `File: Acquirer.js Func:executeAcquirerPayment ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
          );
        }
      }
    } catch (error) {
      console.log(
        "File: Acquirer.js Function: executeAcquirerPayment Error: " + error
      );
      throw error;
    }
  }

  async syncAutoPayoutAcquirer(data, service, transfer) {
    try {
      let transferRequest = {
        transid: transfer.transfer_id,
        aggregatorId: this.aggregatorId,
        secretKey: this.secretKey
      };
      let transStatusResponse = await getTransStatus(transferRequest, service);

      data.transfer_id = transfer.transfer_id;
      data.auto_payout = true;
      if (transStatusResponse.status === "PENDING") {
        return {
          success: true,
          message: `Transfer is in pending state please try again later`
        };
      } else if (
        transStatusResponse &&
        ["SUCCESS", "ACCEPTED"].indexOf(transStatusResponse.status) >= 0
      ) {
        await db.transaction.update(
          { activity: "APPROVED" },
          {
            where: {
              transfer_id: transfer.transfer_id,
              activity: "PENDING"
            }
          }
        );
        setTimeout(async () => {
          try {
            await Queue.newJob("transaction-service", {
              method: "sendWebhook",
              data: {
                transfer_id: transfer.transfer_id,
                type: db.tx_webhooks.TYPE.COLLECTION_UPDATES
              },
              options: service
            });
          } catch (err) {
            console.error("Error while sending transaction webhook", err);
          }
        });
      } else {
        data.activity_action = "REVERSE_TX";
        let transaction = await Queue.newJob("transaction-service", {
          method: "txActivityReverse",
          data,
          options: service
        });

        if (transaction.error && transaction.error.message) {
          console.log(
            `File: Acquirer.js Func:executeAutoPayout ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
          );
          return;
        }
        await db.transaction.update(
          { activity: "REJECTED" },
          {
            where: {
              transfer_id: transfer.transfer_id,
              activity: "PENDING"
            }
          }
        );
        setTimeout(async () => {
          try {
            await Queue.newJob("transaction-service", {
              method: "sendWebhook",
              data: {
                transfer_id: transfer.transfer_id,
                type: db.tx_webhooks.TYPE.COLLECTION_UPDATES
              },
              options: service
            });
          } catch (err) {
            console.error("Error while sending transaction webhook", err);
          }
        });
      }
    } catch (error) {
      console.log(
        "File: Acquirer.js Function: syncAutoPayoutAcquirer Error: " + error
      );
      throw error;
    }
  }
}
