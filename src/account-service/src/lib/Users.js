import db from "@lib/db";
import Util from "@lib/shared";
import config from "@lib/config";
import Queue from "@lib/queue";
import Account from "./Account";
const { Op } = require("sequelize");

async function getAllInvitations(data, service) {
  try {
    const limit = Math.min(parseInt(data.page_size) || 100, 200);
    let page = Math.max(parseInt(data.page) || 1);
    const offset = (page - 1) * limit;
    let countQuery = "SELECT COUNT(uai.user_id)  ";
    let normalQuery = ` FROM ${db.schema}.user_account_invitations uai
       left join ${db.schema}.user_accounts ua on 
       (
	      uai.user_id = ua.user_id and ua.account_id = uai.account_id and
	      (	
		      (ua.removed=1 or ua.end_date != null)
	 	      or (ua.removed = 0 and ua.end_date = null and ua.start_date = null)
	      )
       )
       join ${db.schema}.accounts a on uai.account_id = a.id
    where uai.user_id = :userId and uai.removed = 0 and 
    uai.accepted IS NULL`;
    const filters = {};
    if (service.realmId) {
      filters.realm_id = service.realmId;
      normalQuery += ` and a.realm_id= :realm_id`;
    }
    const count = await db.sequelize.query(countQuery + normalQuery, {
      replacements: {
        ...filters,
        userId: service.userId
      },
      raw: true,
      type: db.sequelize.QueryTypes.SELECT
    });
    const pendingInvitations = await db.sequelize.query(
      `SELECT uai.user_id, uai.account_id,uai.expiry, uai.id ` +
        normalQuery +
        ` LIMIT :limit OFFSET :offset`,
      {
        replacements: {
          ...filters,
          userId: service.userId,
          limit: limit,
          offset: offset
        },
        raw: true,
        type: db.sequelize.QueryTypes.SELECT
      }
    );
    let i = 0;

    while (pendingInvitations && pendingInvitations.length > i) {
      const accountDetail = await db.account.findOne({
        where: { id: pendingInvitations[i].account_id, removed: 0 }
      });
      pendingInvitations[i].name =
        (accountDetail && accountDetail.dataValues.name) || "";
      pendingInvitations[i].registration_no =
        (accountDetail && accountDetail.dataValues.registration_no) || "";

      i++;
    }
    return {
      success: true,
      total: count[0].count,
      invitations: pendingInvitations
    };
  } catch (error) {
    console.log("Users.js getAllInvitations error: " + error);
    throw error;
  }
}
async function acceptUserInvitation(data, service) {
  const transaction = await db.sequelize.transaction();
  try {
    if (service.realmId) {
      await Account.checkValidRealmId({ realm_id: service.realmId }, service);
    }
    const invitation = await db.user_account_invitations.findOne({
      where: { token: data.token, removed: 0, accepted: { [Op.not]: true } }
    });

    if (!invitation) {
      throw new service.ServiceError("BANJO_ERR_INVALID_INVITATION_ID");
    }
    if (data.accepted) {
      await db.user_accounts.create(
        {
          account_id: data.accountId,
          user_id: service.userId,
          start_date: new Date(),
          main_role: invitation.dataValues.role,
          merchant_account_id: invitation.dataValues.merchant_account_id
        },
        {
          transaction
        }
      );
      await db.user_account_invitations.update(
        { accepted: true },
        {
          where: {
            token: data.token
          },
          transaction
        }
      );

      await transaction.commit();
      return { success: true, message: "Invitation accepted successfully" };
    } else {
      await db.user_account_invitations.update(
        { accepted: false },
        {
          where: {
            token: data.token
          },
          transaction
        }
      );
      await transaction.commit();
      return { success: true, message: "Invitation decline successfully" };
    }
  } catch (error) {
    console.log("Users.js acceptUserInvitation error: " + error);
    await transaction.rollback();
    throw error;
  }
}
async function createMerchantAccount(data, service) {
  if (service.realmId) {
    await Account.checkValidRealmId({ realm_id: service.realmId }, service);
  }
  const userData = {
    id: data.id ? data.id : undefined,
    account_id: data.account_id,
    name: data.name,
    acc_no: !!data.acc_no ? data.acc_no : null,
    currency: data.currency,
    settlement_currency: data.settlement_currency,
    category: data.category,
    status: data.status,
    auto_payouts: data.auto_payouts,
    api_credential_id: data.api_credentials ? data.api_credentials : null,
    expiry_hours: data.expiry_hours || undefined,
    payout_expiry_hours: data.payout_expiry_hours || undefined
  };

  var txControl;
  if (data.transactionObject) {
    txControl = data.transactionObject;
  } else {
    txControl = await db.sequelize.transaction();
  }

  if (data.payment_page_theme && data.payment_page_theme.length) {
    userData.payment_page_theme = data.payment_page_theme;
  }

  let merchant_accounts_status = await db.sequelize.query(
    `SELECT enum_range(NULL::${db.schema}.enum_merchant_accounts_status) as merchant_accounts_status;`
  );
  if (!!merchant_accounts_status[1].rowCount) {
    merchant_accounts_status =
      merchant_accounts_status[1].rows[0].merchant_accounts_status;
  }

  if (
    data.status.length > 0 &&
    !merchant_accounts_status.includes(data.status)
  ) {
    throw new service.ServiceError("INVALID_USER_DATA");
  }

  let dbresp = null;
  if (data.id) {
    try {
      dbresp = await db.merchant_accounts.findOne({ where: { id: data.id } });
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  if (data.id && dbresp) {
    //Update data in db
    if (data.currency) {
      if (data.currency !== dbresp.currency) {
        throw new service.ServiceError("BANJO_ERR_CURRENCY_CANNOT_BE_UPDATED");
      }
    }
    if (data.settlement_currency) {
      if (data.settlement_currency !== dbresp.settlement_currency) {
        throw new service.ServiceError("BANJO_ERR_CURRENCY_CANNOT_BE_UPDATED");
      }
    }

    userData.mtime = new Date().toISOString();
    userData.updater = !!data.owner ? data.owner : null;

    if (!userData.acc_no) {
      var account_no = Math.round((Date.now() + Math.random()) * 1000);

      var account_nos = await db.merchant_accounts.findOne({
        where: {
          acc_no: `${account_no}`
        }
      });
      while (account_nos !== null) {
        account_no = Math.round((Date.now() + Math.random()) * 1000);
        account_nos = await db.merchant_accounts.findOne({
          where: {
            acc_no: `${account_no}`
          }
        });
      }
      userData.acc_no = `${account_no}`;
    }

    try {
      await db.merchant_accounts.update(
        { ...userData },
        { where: { id: data.id }, transaction: txControl }
      );
      await txControl.commit();
      return {
        id: data.id,
        acc_no: userData.acc_no,
        message: "Merchant Account updated successfully"
      };
    } catch (error) {
      console.error(
        "Account-service: createMerchantAccount, File: Users.js, Update Error:",
        typeof error == "object" ? JSON.stringify(error) : error
      );
      await txControl.rollback();
      throw new service.ServiceError("INVALID_USER_DATA");
    }
  } else {
    var account_no = Math.round((Date.now() + Math.random()) * 1000);

    var account_nos = await db.merchant_accounts.findOne({
      where: {
        acc_no: `${account_no}`
      }
    });
    while (account_nos !== null) {
      account_no = Math.round((Date.now() + Math.random()) * 1000);
      account_nos = await db.merchant_accounts.findOne({
        where: {
          acc_no: `${account_no}`
        }
      });
    }
    userData.acc_no = `${account_no}`;

    //Create new account in db
    userData.ctime = new Date().toISOString();
    userData.mtime = new Date().toISOString();
    userData.maker = !!data.owner ? data.owner : null;

    try {
      //saving in db
      const ma = await db.merchant_accounts.create(userData, {
        transaction: txControl
      });
      if (!data.transactionObject) {
        await txControl.commit();
      }
      return {
        id: ma.id,
        acc_no: userData.acc_no,
        message: "Account created successfully"
      };
    } catch (error) {
      console.error(
        "Account-service: createMerchantAccount, File: Users.js, Create Error:",
        typeof error == "object" ? JSON.stringify(error) : error
      );
      if (!data.transactionObject) {
        await txControl.rollback();
      }
      throw new service.ServiceError("INVALID_USER_DATA");
    }
  }
}

/**
 * Get invite data invite link page
 * Author: Aditya Tapaswi 14 NOV 2022
 * @param {Object} data
 * @param {Object} service
 * @returns {Promise}
 */
async function getInviteData(data, service, transaction) {
  let filters = {
    removed: 0
  };
  if (service.realmId) {
    filters.realm_id = service.realmId;
  }
  const invite = await db.user_account_invitations.findOne({
    attributes: [
      "id",
      "email",
      "account_id",
      "expiry",
      "role",
      "is_registered_user",
      "token",
      "accepted",
      "merchant_account_id"
    ],
    where: {
      token: data.token,
      removed: 0
    },
    include: [
      {
        model: db.account,
        attributes: ["name"],
        as: "accounts",
        where: filters
      },
      {
        model: db.role,
        attributes: ["role_name"],
        as: "main_role",
        where: filters
      }
    ],
    transaction
  });
  if (!invite) {
    throw new service.ServiceError("BANJO_ERR_INVALID_INVITE_ERROR");
  }
  if (invite.accepted) {
    throw new service.ServiceError("BANJO_ERR_INVITE_ALREADY_ACCEPTED_ERROR");
  }
  if (invite.accepted === false) {
    throw new service.ServiceError("BANJO_ERR_INVITE_ALREADY_DECLINE_ERROR");
  }
  const diff = Util.getDateDifference(new Date(), new Date(invite.expiry));
  if (diff < 0) {
    throw new service.ServiceError("BANJO_ERR_EXPIRED_INVITE_ERROR");
  }
  return invite;
}

export default {
  getAllInvitations,
  acceptUserInvitation,
  createMerchantAccount,
  getInviteData
};
