import db from "@lib/db";
import Util from "@lib/shared";
import config from "@lib/config";
import Queue from "@lib/queue";
const randomize = require("randomatic");
import crypto from "crypto";
import Account from "./Account";

const MAX_UNIQUE_GENERATION = 10;

async function attachWallets(data, service) {
  try {
    let createWalletResp = await createDefaultWallets(data, service);
    return createWalletResp;
  } catch (err) {
    console.error(`Wallets.js. Func:attachWallets. Caught Exception: `, err);
    throw err;
  }
}

async function createDefaultWallets(data, service) {
  let dbQueryOptions = {},
    walletTypes = [config.walletsTypes.WALLET, config.walletsTypes.COLLECTION];
  if (service && service.transaction && service.transaction.commit) {
    dbQueryOptions = { transaction: service.transaction };
  }
  try {
    if (service.realmId) {
      await Account.checkValidRealmId({ realm_id: service.realmId }, service);
    }
    let walletWriteData = [];
    for (let i = 0; i < walletTypes.length; i++) {
      let accNo,
        accResp = await generateWalletAccNo(data, service);
      let refNo,
        refResp = await generateRefNum(data, service);
      let balanceHash;
      if (accResp && accResp.accNo) {
        accNo = accResp.accNo;
        balanceHash = crypto
          .createHash("sha256")
          .update(accResp.accNo)
          .digest("hex");
      }
      if (refResp && refResp.refNum) {
        refNo = refResp.refNum;
      }
      walletWriteData.push({
        acc_label: data.bank_account_name,
        acc_no: accNo,
        currency: data.currency,
        balance: 0,
        balance_hash: balanceHash,
        ref_num: refNo,
        type: walletTypes[i],
        merchant_account_mapping_id: data.merchant_account_mapping_id,
        bank_account_id: data.bank_account_id
      });
    }
    let walletsWriteResp = await db.wallet.bulkCreate(
      walletWriteData,
      dbQueryOptions
    );
    return { success: true, wallets_list: walletsWriteResp };
  } catch (err) {
    console.error(`Wallets.js. Func:createDefaultWallets. Caught Error:`, err);
    throw err;
  }
}

async function generateWalletAccNo(data, service) {
  try {
    let accNo = randomize("0", 12);
    let accSearch = await db.wallet.findOne({
      attributes: ["id", "removed"],
      where: {
        acc_no: accNo
      },
      raw: true
    });
    if (accSearch && accSearch.id) {
      checkRetryCounter("refNum", data, service);
      return generateWalletAccNo(data, service);
    } else {
      return { success: true, accNo };
    }
  } catch (err) {
    console.error(`Wallets.js. Func:generateWalletAccNo. Caught Error:`, err);
    throw err;
  }
}
async function createWallet(data, service) {
  const refNo = await generateWalletAccNo();
  const accResp = await generateWalletAccNo(data, service);
  return await db.wallet.create({
    ...data,
    ref_num: refNo.accNo,
    acc_no: accResp.accNo,
    balance: 0,
    balance_hash: 0
  });
}
async function generateRefNum(data, service) {
  try {
    let refNum = randomize("A0", 12);
    let refNumSearch = await db.wallet.findOne({
      attributes: ["id", "removed"],
      where: {
        removed: 0,
        ref_num: refNum
      },
      raw: true
    });
    if (refNumSearch && refNumSearch.id) {
      checkRetryCounter("refNum", data, service);
      return generateRefNum(data, service);
    } else {
      return { success: true, refNum };
    }
  } catch (err) {
    console.error(`Wallets.js. Func:generateRefNum. Caught Error:`, err);
    throw err;
  }
}

function checkRetryCounter(prefix, data, service) {
  if (
    data &&
    typeof prefix == "string" &&
    !isNaN(data[prefix]) &&
    data[prefix] > MAX_UNIQUE_GENERATION
  ) {
    console.error(
      `Wallets. Func:checkRetryCounter. Failed to generate Unique Wallet Acc no`
    );
    throw "BANJO_ERR_UNIQUE_SEQ_FAIL";
  }
  if (data[prefix] == undefined) data[prefix] = 0;
  else data[prefix]++;
}
async function createWalletAccount(data, currency) {
  try {
    let query = `SELECT ba.account_name, mam.id as merchant_account_mapping_id FROM  ${db.schema}.merchant_account_mappings mam 
            join ${db.schema}.bank_accounts ba on mam.bank_account = ba.id
            where mam.removed = 0 and mam.merchant_account_id = :merchant_account_id and mam.bank_account = :bank_account_id AND mam.start_time < now() AND (mam.end_time is null OR mam.end_time > now())`;
    const merchantMapping = await db.sequelize.query(query, {
      replacements: {
        bank_account_id: data.bank_account_id,
        merchant_account_id: data.merchant_account_id
      },
      raw: true
    });
    if (
      merchantMapping &&
      merchantMapping.length &&
      merchantMapping[0].length
    ) {
      const collectionAcc = await fetchCollectionAccount(
        merchantMapping[0][0].merchant_account_mapping_id
      );
      if (collectionAcc && collectionAcc.id) {
        const res = await Queue.newJob("account-service", {
          method: "createWallet",
          data: {
            currency,
            acc_label: merchantMapping[0][0].account_name,
            type: db.wallet.TYPES.WALLET,
            bank_account_id: data.bank_account_id,
            merchant_account_mapping_id:
              merchantMapping[0][0].merchant_account_mapping_id
          },
          options: {
            realmId: service.realmId
          }
        });
        if (res.result) {
          return {
            ...res.result,
            src_wallet_id: collectionAcc.id,
            src_wallet_currency: collectionAcc.currency,
            balance: collectionAcc.balance
          };
        } else {
          throw res.error;
        }
      }
      return null;
    }
    throw "BANJO_ERR_INVALID_MERCHANT_ACCOUNT";
  } catch (error) {
    throw error;
  }
}
async function getFetchWalletsAccount(bankIds, currency) {
  let query = `SELECT w.id as dst_wallet_id, w.currency as dst_wallet_currency, w.merchant_account_mapping_id  FROM  ${db.schema}.wallets w 
            join ${db.schema}.vw_merchant_account_mappings mam on mam.id = w.merchant_account_mapping_id and mam.merchant_account_id = :merchant_account_id
            where w.removed = 0 and w.type = :type and w.currency= :currency and w.bank_account_id = :bank_account_id order by w.ctime`;

  const settlement_wallet_records = [];
  for (let currentBA of bankIds) {
    try {
      if (currentBA.amount <= 0) {
        continue;
      }
      let replacements = {
        currency,
        bank_account_id: currentBA.bank_account_id,
        merchant_account_id: currentBA.merchant_account_id
      };
      const acc = await db.sequelize.query(query, {
        replacements: {
          ...replacements,
          type: db.wallet.TYPES.WALLET
        },
        raw: true
      });
      if (acc && acc.length && acc[0].length && acc[0][0].dst_wallet_id) {
        const walletRecord = [];
        for (let i = 0; i < acc[0].length; i++) {
          const collectionAcc = await fetchCollectionAccount(
            acc[0][i].merchant_account_mapping_id
          );

          if (collectionAcc && collectionAcc.id) {
            walletRecord.push({
              ...acc[0][i],
              src_wallet_id: collectionAcc.id,
              src_wallet_currency: collectionAcc.currency,
              balance: collectionAcc.balance
            });
          }
        }
        if (walletRecord.length)
          settlement_wallet_records.push({
            walletRecord: walletRecord,
            ...currentBA
          });
      } else {
        const acc = await createWalletAccount(currentBA, currency);
        if (acc)
          settlement_wallet_records.push({
            walletRecord: [
              {
                dst_wallet_id: acc.id,
                src_wallet_id: acc.src_wallet_id,
                dst_wallet_currency: acc.currency,
                src_wallet_currency: acc.src_wallet_currency,
                balance: acc.balance
              }
            ],
            ...currentBA
          });
      }
    } catch (error) {
      throw error;
    }
  }
  return settlement_wallet_records;
}

async function fetchCollectionAccount(merchant_account_mapping_id) {
  try {
    const Op = db.Sequelize.Op;
    const acc = await db.wallet.findOne({
      where: {
        merchant_account_mapping_id,
        type: db.wallet.TYPES.COLLECTION,
        removed: 0,
        balance: { [Op.gt]: 0 }
      },
      attributes: ["id", "currency", "balance"],
      raw: true,
      type: db.sequelize.QueryTypes.SELECT
    });
    return acc;
  } catch (error) {
    console.log("file:wallet.js function:fetchCollectionAccount", error);
    throw error;
  }
}
async function getCreateLedgerAccount(bankAccId, currency, service) {
  const acc = await db.wallet.findOne({
    where: {
      bank_account_id: bankAccId,
      currency,
      type: db.wallet.TYPES.LEDGER
    },
    attributes: ["id", "currency"]
  });
  if (acc) {
    return acc;
  } else {
    const acc = await createWallet(
      {
        negative: true,
        currency,
        acc_label: "Exchange Ledger",
        type: db.wallet.TYPES.LEDGER,
        bank_account_id: bankAccId
      },
      service
    );
    return {
      id: acc.id,
      currency: acc.currency
    };
  }
}
export default {
  createWallet,
  attachWallets,
  createDefaultWallets,
  getFetchWalletsAccount,
  getCreateLedgerAccount
};
