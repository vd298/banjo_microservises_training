// require("dotenv").config({ path: "../../../../.env" });

// import { Sequelize, DataTypes, fn, col, Op } from "sequelize";

// const sequelize = new Sequelize(process.env.DB_CONN_STRING, {
//   define: {
//     schema: process.env.DB_SCHEMA,
//   },
// });

// const Student = sequelize.define("Student", {
//   id: {
//     type: DataTypes.INTEGER,
//     primaryKey: true,
//     autoIncrement: true,
//   },
//   name: {
//     type: DataTypes.STRING,
//   },
// });

// const StudentMarks = sequelize.define("StudentMarks", {
//   id: {
//     type: DataTypes.INTEGER,
//     primaryKey: true,
//     autoIncrement: true,
//   },
//   studentId: {
//     type: DataTypes.INTEGER,
//     references: {
//       model: "Student",
//       key: "id",
//     },
//   },
//   marks: {
//     type: DataTypes.INTEGER,
//   },
// });

// async function aggregateDataForStudent(data, service) {
//   try {
//     const studentId = data.studentId;

//     const studentInfo = await Student.findByPk(studentId);

//     const studentMarks = await StudentMarks.findAll({
//       where: {
//         studentId: studentId,
//       },
//       attributes: [[fn("sum", col("marks")), "totalMarks"]],
//     });

//     const studentTotalMarks = studentMarks[0].dataValues.totalMarks;

//     const otherStudentMarks = await StudentMarks.findAll({
//       where: {
//         studentId: {
//           [Op.not]: studentId,
//         },
//       },
//       attributes: [[fn("sum", col("marks")), "totalMarks"]],
//     });

//     const otherStudentTotalMarks = otherStudentMarks[0].dataValues.totalMarks;

//     const topper = studentTotalMarks > otherStudentTotalMarks;

//     const combinedResult = {
//       studentInfo,
//       isTopper: topper,
//     };

//     return combinedResult;
//   } catch (err) {
//     console.error(err);
//     throw err;
//   }
// }
// export default { aggregateDataForStudent };
//----------------------------------------------------------------------------

// // require("dotenv").config({ path: "../../../../.env" });
// import config from "@lib/db/config/config";
// import student from "@lib/db/models/student";
// import { Sequelize, DataTypes, fn, col, Op } from "sequelize";

// // const sequelize = new Sequelize(process.env.DB_CONN_STRING, {
// //   define: {
// //     schema: process.env.DB_SCHEMA,
// //   },
// // });

// const Student = student;

// const StudentMarks = sequelize.define("StudentMarks", {
//   id: {
//     type: DataTypes.INTEGER,
//     primaryKey: true,
//     autoIncrement: true,
//   },
//   studentId: {
//     type: DataTypes.INTEGER,
//     references: {
//       model: "Student",
//       key: "id",
//     },
//   },
//   marks: {
//     type: DataTypes.INTEGER,
//   },
// });

// async function aggregateDataForStudent(data, service) {
//   try {
//     const studentId = data.studentId;

//     const studentInfo = await Student.findByPk(studentId);

//     const studentMarks = await StudentMarks.findAll({
//       where: {
//         studentId: studentId,
//       },
//       attributes: [[fn("sum", col("marks")), "totalMarks"]],
//     });

//     const studentTotalMarks = studentMarks[0].dataValues.totalMarks;

//     const otherStudentMarks = await StudentMarks.findAll({
//       where: {
//         studentId: {
//           [Op.not]: studentId,
//         },
//       },
//       attributes: [[fn("sum", col("marks")), "totalMarks"]],
//     });

//     const otherStudentTotalMarks = otherStudentMarks[0].dataValues.totalMarks;

//     const topper = studentTotalMarks > otherStudentTotalMarks;

//     const combinedResult = {
//       studentInfo,
//       isTopper: topper,
//     };

//     return combinedResult;
//   } catch (err) {
//     console.error(err);
//     throw err;
//   }
// }

// export default { aggregateDataForStudent };
// require("dotenv").config({ path: "../../../../.env" });

// import { Sequelize, DataTypes, fn, col, Op } from "sequelize";

// const sequelize = new Sequelize(process.env.DB_CONN_STRING, {
//   define: {
//     schema: process.env.DB_SCHEMA,
//   },
// });

// const Student = sequelize.define("Student", {
//   id: {
//     type: DataTypes.INTEGER,
//     primaryKey: true,
//     autoIncrement: true,
//   },
//   name: {
//     type: DataTypes.STRING,
//   },
// });

// const StudentMarks = sequelize.define("StudentMarks", {
//   id: {
//     type: DataTypes.INTEGER,
//     primaryKey: true,
//     autoIncrement: true,
//   },
//   studentId: {
//     type: DataTypes.INTEGER,
//     references: {
//       model: "Student",
//       key: "id",
//     },
//   },
//   marks: {
//     type: DataTypes.INTEGER,
//   },
// });
//-------------------------------------------------------------------
require("dotenv").config({ path: "../../../../.env" });

import { Sequelize, DataTypes, fn, col, Op } from "sequelize";

const sequelize = new Sequelize(process.env.DB_CONN_STRING, {
  define: {
    schema: process.env.DB_SCHEMA,
  },
});

const Student = sequelize.define("Student", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name: {
    type: DataTypes.STRING,
  },
});

const StudentMarks = sequelize.define("StudentMarks", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  studentId: {
    type: DataTypes.INTEGER,
    references: {
      model: "Student",
      key: "id",
    },
  },
  marks: {
    type: DataTypes.INTEGER,
  },
});

StudentMarks.belongsTo(Student, { foreignKey: "studentId" });

Student.hasMany(StudentMarks, { foreignKey: "studentId" });

async function aggregateDataForStudent(data, service) {
  try {
    const studentId = data.studentId;

    const studentMarks = await StudentMarks.findOne({
      where: { studentId },
      attributes: [[fn("sum", col("marks")), "totalMarks"]],
    });
    const studentTotalMarks = studentMarks?.dataValues.totalMarks || 0;

    const allStudentsMarks = await StudentMarks.findAll({
      attributes: [
        ["studentId", "studentId"],
        [fn("sum", col("marks")), "totalMarks"],
      ],
      group: ["studentId"],
      where: {
        studentId: { [Op.ne]: studentId },
      },
    });

    // max marks
    const maxTotalMarks = Math.max(
      ...allStudentsMarks.map((marks) => marks.dataValues.totalMarks)
    );

    const isTopper = studentTotalMarks >= maxTotalMarks;

    const studentInfo = await Student.findByPk(studentId);

    const combinedResult = {
      studentInfo,
      isTopper,
    };

    return combinedResult;
  } catch (err) {
    console.error(err);
    //throw err;
    throw new service.ServiceError("Vinay_Error_Test");
  }
}

export default { aggregateDataForStudent };
