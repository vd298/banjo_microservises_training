/////////////////////////////////////////sequelize function////////////////////////////
// import db from "@lib/db";
// import currency from "@lib/db/models/currency";
// async function getCountries(data, currency) {
//   try {
//     const currencies = await db.currency.findAll({
//       where: {
//         code: data.code,
//       },
//     });

//     return currencies;
//   } catch (error) {
//     console.error("Error retrieving currencies:", error);
//     throw error;
//   }
// }
// //////////////////////////////////////////query//////////////////////////////////////////////
// import db from "@lib/db";
// import currency from "@lib/db/models/currency";
// async function getCountries(data, currency) {
//   try {
//     const [
//       list,
//     ] = await db.sequelize.query(
//       `SELECT id,code,name,abbr FROM ${db.schema}.currency where code = '${data.code}';`,
//       { raw: true }
//     );
//     return list;

//     //return currencies;
//   } catch (error) {
//     console.error("Error retrieving currencies:", error);
//     throw error;
//   }
// }
///////////////////////////////////////join query/////////////////////////////////////////
import db from "@lib/db";
import { ServiceError } from "@lib/error";
//import currency from "@lib/db/models/currency";
async function getCountries(data, currency) {
  try {
    // let userAccountsQuery = "";
    // userAccountsQuery += `A.name,ma.name FROM ${db.schema}.accounts a inner join ${db.schema}.merchant_accounts ma on (ma.merchant_account_id=a.id and ma.removed=0) where code = '${data.status}'`;
    // const x = await db.sequelize.query(
    //   `SELECT count(*) FROM (${userAccountsQuery}) AS number_of_user_accounts`,
    //   {
    //     raw: true,
    //   }
    // );
    let limit = 5;
    let pageNumber = 1;
    if (data.page) {
      pageNumber = data.page;
    }

    if (data.page_size) {
      limit = data.page_size;
    }

    const [list] = await db.sequelize.query(
      `SELECT * FROM ${db.schema}.accounts AS a INNER JOIN ${
        db.schema
      }.merchant_accounts AS ma ON ma.account_id = a.id WHERE ma.status = '${
        data.status
      }' limit ${limit} offset ${limit * (pageNumber - 1)}`,
      {
        raw: true,
      }
    );
    // if (3 === 3) throw new service.ServiceError("BANJO_ERR_REALM_IS_INACTIVE");
    return list;
  } catch (error) {
    console.error("Error retrieving currencies:", error);
    //throw error;
    //throw new service.ServiceError("Vinay_Error_Test");
  }
}
// getCountries(data, currency)
//   .then((currencies) => {
//     console.log("Countries:", currencies);
//   })
//   .catch((error) => {
//     countries;
//     console.error("Error:", error);
//   });
export default { getCountries };
