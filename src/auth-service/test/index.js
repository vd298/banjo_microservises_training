import chai from "chai";
import db from "@lib/db";
let should = chai.should();
import pretest from "@lib/pretest";
const expect = chai.expect;
import { v4 as uuidv4 } from "uuid";
import crypto from "crypto";
import MemStore from "@lib/memstore";

import Service from "../src/Service.js";
const service = new Service({
  name: "auth-service"
});

const profile_pic =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWIAAAE2CAYAAABbQ5ibAAAAAXNSR0IArs4c6QAAIABJREFUeAHsnQd4JUeR==";

let userId;
let accountId = "93332248-0144-42b8-96a8-422963bb65d2";
let uuid = uuidv4();
let uuidUserAccount = uuidv4();
let ENV;
let token;
let emailVerificationToken, code;

describe("auth-service", function() {
  before(async () => {
    ENV = await pretest.before();

    const user = await db.user.create({
      id: uuid,
      ctime: new Date(),
      mtime: new Date(),
      removed: 0,
      maker: uuid,
      first_name: "test",
      last_name: "user",
      email: "test@gmail.com",
      mobile: "+919623850000",
      username: "test123",
      password: crypto
        .createHash("sha256")
        .update("Passw0rd")
        .digest("hex"),
      mobile_verified: false,
      email_verified: false,
      settings: null,
      status: "ACTIVE",
      profile_pic: null,
      timezone: null,
      user_type: "PORTAL"
    });
    userId = user.id;
    await db.user_accounts.create({
      id: uuidUserAccount,
      removed: 0,
      account_id: accountId,
      user_id: userId,
      main_role: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4",
      start_date: "2022-11-09T08:54:31.400Z"
    });
  });

  it("Change password", async () => {
    const res = await service.runServiceMethod({
      method: "changePassword",
      data: {
        old_password: "Passw0rd",
        new_password: "Test@123"
      },
      options: {
        userId: userId,
        header: {
          lang: "en"
        }
      }
    });
    res.should.have.deep.property("success", true);
  });

  it("Reset password request", async () => {
    const res = await service.runServiceMethod({
      method: "resetPasswordRequest",
      data: {
        email: "test@gmail.com"
      }
    });
    res.should.have.deep.property("success", true);
  });
  it.skip("Reset password ", async () => {
    const res = await service.runServiceMethod({
      method: "resetPassword",
      data: {
        step: 1,
        code: "q2jcBleNEr",
        new_password: "Passw0rd@123"
      }
    });
    res.should.have.deep.property("success", true);
  });
  it("Get user profile", async () => {
    const res = await service.runServiceMethod({
      method: "getUserProfile",
      options: { userId, header: { lan: "en" } }
    });
    res.user.should.have.deep.property("first_name", "test");
  });
  it("Get merchant accounts by user", async () => {
    const res = await service.runServiceMethod({
      method: "getMerchantAccountsByUser",
      options: { userId, header: { lan: "en" } }
    });
    res.should.have.deep.property("success", true);
  });
  it("Get user accounts", async () => {
    const res = await service.runServiceMethod({
      method: "getAccounts",
      options: { userId, header: { lan: "en" } }
    });
    res.should.have.deep.property("success", true);
  });

  it("Update user profile", async () => {
    const res = await service.runServiceMethod({
      method: "updateUserProfile",
      data: {
        first_name: "Aditi",
        last_name: "Phalke",
        email: "ap269@enovate-it.com",
        mobile: "+919075201623",
        username: "ap269",
        timezone: "Etc/GMT+12"
      },
      options: { userId, header: { lan: "en" } }
    });
    res.should.have.deep.property("success", true);
  });
  it("Send email verification mail", async () => {
    const res = await service.runServiceMethod({
      method: "verifyEmailRequest",
      data: {
        email: "ap269@enovate-it.com"
      },
      options: { userId, header: { lan: "en" } }
    });
    emailVerificationToken = res.token;
    code = res.code;
    res.should.have.deep.property("success", true);
  });

  it("Verify mail", async () => {
    const res = await service.runServiceMethod({
      method: "verifyEmail",
      data: {
        token: emailVerificationToken,
        code: code
      },
      options: { userId, header: { lan: "en" } }
    });
    res.should.have.deep.property("success", true);
  });

  it("Upload user profile pic", async () => {
    const res = await service.runServiceMethod({
      method: "updateProfilePic",
      data: {
        file_name: "image.png",
        profile_pic: profile_pic
      },
      options: { userId, header: { lan: "en" } }
    });
    res.should.have.deep.property("success", true);
    res.should.have.property("profilePicUrl");
    res.should.have.property("profile_pic_code");
  });
  it("sign up user", async () => {
    const invite = await db.user_account_invitations.findOne({
      where: {
        is_registered_user: false,
        removed: 0
      }
    });
    const res = await service.runServiceMethod({
      method: "signup",
      data: {
        token: invite.token,
        first_name: "Prashin",
        last_name: "More",
        email: invite.email,
        mobile: "9324115782",
        username: "pm261",
        password: "P@ssw0rd"
      }
    });
    res.result.should.have.deep.property("user_token");
    res.result.should.have.deep.property("user_id");
  });
  it("fetch assignable roles", async () => {
    const role = await db.role.findOne({
      where: {
        role_name: "Owner"
      }
    });
    const res = await service.runServiceMethod({
      method: "fetchAssignableRoles",
      data: {
        role_id: role.id,
        weight: role.weight
      }
    });
    res.should.have.deep.property("success", true);
    res.should.have.deep.property("roles");
  });
  it("Resend user invitation user account", async () => {
    const res = await service.runServiceMethod({
      method: "resendInvitation",
      data: {
        email: "as263@enovate-it.com",
        account_id: "93332248-0144-42b8-96a8-422963bb65d2",
        user_type: "PORTAL",
        role: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4",
        merchant_account_id: "1e50984c-8cd1-11ed-a1eb-0242ac121112"
      },
      options: {
        userId: "3df4c34c-600a-11ed-9b6a-0242ac120002",
        header: {
          lang: "en"
        }
      }
    });
    console.log("resendInvitation", res);
    res.should.equal("Invite sent successfully");
  });
});
