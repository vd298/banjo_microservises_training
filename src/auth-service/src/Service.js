import Base from "@lib/base";
import Captcha from "./lib/Captcha";
import Otp from "./lib/Otp";
import Server from "./lib/Server";
import Signin from "./lib/Signin";
import Signup from "./lib/Signup";
import GoogleAuth from "./lib/GoogleAuth";
import Documents from "./lib/Documents";
import UserToken from "./lib/UserToken";
import Account from "./lib/Account";
import User from "./lib/User";
import UserInvite from "./lib/UserInvite";
import SyncPermissions from "./lib/SyncPermissions";

export default class Service extends Base {
  publicMethods() {
    return {
      resetServer: {
        realm: true,
        method: Server.reset,
      },
      signin: {
        realm: true,
        method: Signin.signin,
        description: "Login",
        schema: {
          type: "object",
          properties: {
            username: { type: "string", maxLength: 50 },
            password: { type: "string", secure: true },
          },
          required: ["username", "password"],
        },
        log: true,
        secureResponse: {
          user_token: true,
        },
      },
      validateUserToken: {
        private: true,
        method: UserToken.validateUserToken,
        description: "Validate User Access Token",
        schema: {
          type: "object",
          properties: {},
        },
      },
      validateMerchantToken: {
        private: true,
        method: UserToken.validateMerchantToken,
        description: "Generate Merchant Access Token",
        schema: {
          type: "object",
          properties: {},
        },
      },
      getUserDetailByToken: {
        private: true,
        method: UserToken.getUserDetailByToken,
        description: "get user detail by token",
        schema: {
          type: "object",
          properties: {},
        },
      },
      generateUserToken: {
        private: true,
        method: UserToken.generateUserToken,
        description: "Generate User Access Token",
        schema: {
          type: "object",
          properties: {
            login: { type: "string" },
            realm_id: { type: "string" },
            user_id: { type: ["string", "null"] },
            valid_attempt: { type: "boolean" },
            source: { type: "string", enum: "password" },
          },
          required: ["login", "realm_id", "valid_attempt"],
        },
      },
      getAccounts: {
        realm: true,
        user: true,
        method: Account.getUserAccounts,
        description: "Get associated accounts to user",
        schema: {
          type: "object",
          properties: {},
        },
        responseSchema: {
          type: "array",
          items: {
            id: { type: "string" },
            account_id: { type: "string" },
            account_name: { type: "string" },
            status: { type: "string" },
            role: { type: "string" },
            legal_entity: { type: "boolean" },
            u_a_ctime: { type: "string" },
            email: { type: "string" },
          },
          required: ["success"],
        },
      },
      getAccountDetails: {
        realm: true,
        user: true,
        account: false,
        method: Account.getAccountDetails,
        description: "Get Account Details",
        schema: {
          type: "object",
          properties: {},
        },
      },
      changePassword: {
        realm: true,
        user: true,
        method: User.changePassword,
        description: "change password",
        schema: {
          type: "object",
          properties: {
            old_password: { type: "string", secure: true },
            new_password: { type: "string", secure: true },
          },
          required: ["old_password", "new_password"],
        },
        log: true,
      },
      resetPasswordRequest: {
        realm: true,
        method: User.resetPasswordRequest,
        description: "send link to restore password",
        schema: {
          type: "object",
          properties: {
            email: { type: "string" },
          },
          required: ["email"],
        },
      },
      resetPassword: {
        realm: true,
        method: User.resetPassword,
        description: "reset password",
        schema: {
          type: "object",
          properties: {
            code: { type: "string", secure: true },
            step: { type: "integer" },
            new_password: { type: "string", secure: true },
          },
          required: ["code", "step"],
        },
        log: true,
      },
      checkGoogleAuth: {
        method: Signin.checkGoogleAuth,
        description: "checkGoogleAuth",
      },
      getCaptcha: {
        realm: true,
        method: Captcha.getCaptcha,
        description: "Getting captcha svg",
        schema: {
          type: "object",
          properties: {
            token: { type: "string" },
          },
          required: ["token"],
        },
      },
      checkCaptcha: {
        realm: true,
        method: Captcha.checkCaptcha,
        description: "Checking captcha text",
        schema: {
          type: "object",
          properties: {
            captcha: { type: "string" },
            token: { type: "string" },
          },
          required: ["token", "captcha"],
        },
      },
      permissedMethod: {
        private: true,
        realm: true,
        method: Server.permissedMethod,
        description: "Test permissions",
      },
      getServerByToken: {
        private: true,
        method: Server.getServerByToken,
        description: "Getting ServerId by authorization token",
      },
      getServerPermissions: {
        private: true,
        realm: true,
        method: Server.getServerPermissions,
        description: "Getting Server Permissions",
      },
      getPublicMethods: {
        private: true,
        method: Server.getPublicMethods,
        description: "Test public methods",
      },
      otp: {
        realm: true,
        user: false,
        method: Otp.setOtp,
        description: "Set OTP",
        schema: {
          type: "object",
          properties: {
            userId: { type: "string" },
          },
          required: ["userId"],
        },
      },
      checkOtp: {
        realm: true,
        user: true,
        method: Otp.check,
        description: "Checking one time password",
        schema: {
          type: "object",
          properties: {
            otp: { type: "string" },
          },
          required: ["otp"],
        },
      },
      resendOtp: {
        realm: true,
        user: true,
        method: Otp.resendOtp,
        description: "Resend one time password",
      },

      activate: {
        realm: true,
        method: Signup.activate,
        description: "activate profile",
        schema: {
          type: "object",
          properties: {
            user_id: { type: "string" },
            code: { type: "string" },
          },
          required: ["user_id", "code"],
        },
      },

      verifyPhone: {
        realm: true,
        user: false,
        method: Signup.verifyPhone,
        description: "Phone verification: send code",
        schema: {
          type: "object",
          properties: {
            login: { type: "string" },
            captcha: { type: "string" },
            token: { type: "string", secure: true },
          },
          required: ["login", "captcha", "token"],
        },
        log: true,
      },
      checkPhoneCode: {
        realm: true,
        user: false,
        method: Signup.checkPhoneCode,
        description: "Phone verification: check code",
        schema: {
          type: "object",
          properties: {
            code: { type: "string", secure: true },
            user_id: { type: "string" },
            token: { type: "string", secure: true },
          },
          required: ["code", "user_id", "token"],
        },
        log: true,
      },
      verifyPassword: {
        realm: true,
        user: false,
        method: Signup.verifyPassword,
        description: "Password verification",
        schema: {
          type: "object",
          properties: {
            password: { type: "string", secure: true },
            user_id: { type: "string" },
            token: { type: "string", secure: true },
          },
          required: ["password", "user_id", "token"],
        },
        log: true,
      },

      uploadDocument: {
        realm: true,
        user: true,
        method: Documents.uploadDocument,
        description: "Upload document",
        schema: {
          type: "object",
          properties: {
            type: { type: "string" },
            status: { type: "integer" },
            files: {
              type: "array",
              items: {
                name: { type: "string" },
                code: { type: "string" },
              },
            },
          },
          required: ["type"],
        },
      },
      getUserDocuments: {
        realm: true,
        user: true,
        method: Documents.getUserDocuments,
        description: "Get user documents",
      },
      updateDocument: {
        realm: true,
        user: true,
        method: Documents.updateDocument,
        description: "Update document from admin portal",
        schema: {
          type: "object",
          properties: {
            id: { type: "string" },
            type: { type: "string" },
            status: { type: "integer" },
            files: {
              type: "array",
              items: {
                name: { type: "string" },
                code: { type: "string" },
              },
            },
          },
          required: ["id", "type", "status", "files"],
        },
      },

      signout: {
        realm: false,
        user: true,
        method: Signin.signout,
        description: "Sign out from UI",
        schema: {
          type: "object",
          properties: {
            token: { type: "string" },
          },
          required: ["token"],
        },
        log: true,
      },
      sendInvite: {
        realm: true,
        method: User.sendInvite,
        description: "Send invitation",
        schema: {
          type: "object",
          properties: {
            email: { type: "string" },
            user_id: { type: "string" },
          },
          required: ["email", "user_id"],
        },
        log: true,
      },
      getIP: {
        realm: true,
        user: true,
        method: User.getIP,
        description: "Getting Client IP",
      },
      changeCodeWord: {
        realm: true,
        user: true,
        method: User.changeCodeWord,
        description: "changeCodeWord",
        schema: {
          type: "object",
          properties: {
            code_word: { type: "string" },
          },
          required: ["code_word"],
        },
      },
      getRealmDepartments: {
        realm: true,
        method: User.getRealmDepartments,
        description: "Returns list of realm departments",
        schema: {
          type: "object",
          properties: {},
          required: [],
        },
      },
      resendActivationCode: {
        realm: true,
        method: Signup.resendActivationCode,
        description: "Resend activation code",
        schema: {
          type: "object",
          properties: {
            user_id: { type: "string" },
          },
          required: ["user_id"],
        },
      },

      createUser: {
        realm: true,
        user: true,
        method: Signup.createUser,
        description: "Create user throught merchant issuer",
        schema: {
          type: "object",
          properties: {
            user_type: { type: "integer" },
            legalname: { type: "string" },
            first_name: { type: "string" },
            last_name: { type: "string" },
            realm_department: { type: "string" },
            login: { type: "string" },
            email: { type: "string" },
          },
          required: ["realm_department", "user_type", "login", "email"],
        },
      },
      getUserProfile: {
        realm: true,
        user: true,
        method: User.getUserProfile,
        description: "get_user_profile",
        schema: {
          type: "object",
          properties: {},
        },
      },
      getMerchantAccountsByUser: {
        realm: true,
        user: true,
        method: User.getMerchantAccountsByUser,
        description: "Get merchant accounts by user",
        schema: {
          type: "object",
          properties: {},
        },
      },
      updateUserProfile: {
        realm: true,
        user: true,
        method: User.updateUserProfile,
        description: "update user profile",
        schema: {
          type: "object",
          properties: {
            first_name: { type: "string", minLength: 3, maxLength: 50 },
            last_name: { type: "string", minLength: 3, maxLength: 50 },
            email: { type: "string", maxLength: 100, format: "email" },
            username: { type: "string", minLength: 3, maxLength: 20 },
            mobile: { type: "string", maxLength: 15, format: "phone" },
            profile_pic: { type: ["string", "null"] },
            timezone: { type: "string", maxLength: 50 },
            status: { type: "string", enum: ["ACTIVE", "INACTIVE", "BLOCKED"] },
          },
        },
      },
      updateProfilePic: {
        realm: true,
        user: true,
        method: User.updateProfilePic,
        description: "update user's profile pic",
        schema: {
          type: "object",
          properties: {
            file_name: {
              type: "string",
            },
            profile_pic: {
              type: "string",
              contentEncoding: "base64",
              contentMediaType: "image/png",
            },
          },
        },
      },
      getProfilePic: {
        realm: true,
        user: true,
        method: User.getProfilePic,
        description: "get user profile pic",
        schema: {
          type: "object",
          properties: {
            owner: { type: "string" },
          },
        },
        required: ["owner"],
      },
      verifyEmailRequest: {
        method: Signup.verifyEmailRequest,
        description: "Email verification",
        schema: {
          type: "object",
          properties: {
            email: { type: "string", maxLength: 100, format: "email" },
          },
          required: ["email"],
        },
      },
      verifyEmail: {
        realm: true,
        method: Signup.verifyEmail,
        description: "Confirm a user's email",
        schema: {
          type: "object",
          properties: {
            token: { type: "string", secure: true },
            code: { type: "string", secure: true },
          },
          required: ["token", "code"],
        },
        log: true,
      },
      cancelInvitation: {
        method: User.cancelInvitation,
        description: "Cancel invitation sent to user",
        schema: {
          type: "object",
          properties: {
            id: { type: "string", format: "uuid" },
          },
          required: ["id"],
        },
      },
      inviteUser: {
        user: true,
        method: UserInvite.inviteUser,
        schema: {
          type: "object",
          properties: {
            email: { type: "string", maxLength: 50, format: "email" },
            role: { type: ["string", "null"] },
            account_id: { type: "string" },
            anyOf: [
              { merchant_account_id: { type: "string" } },
              {
                merchant_account_ids: {
                  type: "array",
                  items: { type: "string" },
                },
              },
            ],
            user_type: {
              type: "enum",
              enum: ["API", "PORTAL"],
            },
            additional_roles: {
              type: { type: ["array", "null"] },
              items: {
                type: "string",
                format: "uuid",
              },
            },
          },
          required: ["email", "account_id", "user_type"],
        },
        description: "Inviting user to account",
      },
      resendInvitation: {
        user: true,
        method: UserInvite.resendInvitation,
        schema: {
          type: "object",
          properties: {
            email: { type: "string", maxLength: 50, format: "email" },
            role: { type: ["string", "null"] },
            account_id: { type: "string" },
            user_type: {
              type: "enum",
              enum: ["API", "PORTAL"],
            },
          },
          required: ["email", "account_id", "user_type", "role"],
        },
        description: "Resend user invitation user account",
      },
      checkUserPermissions: {
        user: true,
        account: true,
        method: User.checkUserPermissions,
        description: "Confirm user permissions",
        schema: {
          type: "object",
          properties: {
            identifier: { type: "string" },
          },
          required: ["identifier"],
        },
      },
      syncPermissions: {
        method: SyncPermissions.syncPermissions,
        description: "sync API permissions",
        schema: {
          type: "object",
          properties: {},
        },
      },
      fetchAssignableRoles: {
        method: User.fetchAssignableRoles,
        description: "Fetch all roles with less weight than the user role",
        schema: {
          type: "object",
          properties: {
            role_id: { type: "string", format: "uuid" },
            weight: { type: "integer" },
          },
          anyOf: [
            {
              required: ["role_id"],
            },
            {
              required: ["weight"],
            },
          ],
        },
        log: true,
      },
      signup: {
        method: Signup.signup,
        schema: {
          type: "object",
          properties: {
            token: { type: "string", maxLength: 80 },
            first_name: { type: "string", maxLength: 50 },
            last_name: { type: "string", maxLength: 50 },
            email: { type: "string", maxLength: 50 },
            mobile: { type: "string", maxLength: 15 },
            username: { type: "string", maxLength: 20 },
            password: { type: "string", maxLength: 20 },
            telegram_id: { type: "string", maxLength: 200 },
          },
          required: ["token", "first_name", "email", "username", "password"],
        },
        description:
          "Registered user and join to a account via invitation link",
      },
      generateApiKeys: {
        user: true,
        method: UserInvite.generateApiKeys,
        description: "Generate API keys",
        schema: {
          type: "object",
          properties: {
            merchant_account_id: { type: "string", format: "uuid" },
            key_name: { type: "string" },
            webhook_url: { type: "string" },
            ips: { type: "array" },
            permissions: {
              type: "array",
              items: {
                type: "string",
              },
            },
          },
          required: ["merchant_account_id", "key_name", "permissions"],
        },
      },
      fetchApiKeys: {
        user: true,
        method: UserInvite.fetchApiKeys,
        description: "Fetch API keys generated by User",
        schema: {
          type: "object",
          properties: {
            merchant_account_id: { type: "string", format: "uuid" },
          },
          required: ["merchant_account_id"],
        },
      },
      fetchUserInviteList: {
        user: true,
        method: UserInvite.fetchUserInviteList,
        schema: {
          type: "object",
          properties: {
            account_id: { type: "string", format: "uuid" },
            page: {
              type: "number",
              format: "integer",
              description: "Page number starts from 1, Default 1",
            },
            page_size: {
              type: "number",
              format: "integer",
              description: "Default 100, Max 200 records per API call",
            },
            merchant_account_ids: {
              type: "array",
              items: {
                type: "string",
              },
            },
          },
          required: ["account_id"],
        },
        description: "Fetch List of Users that were invited",
      },
      removeAPIkey: {
        user: true,
        method: UserInvite.removeAPIkey,
        schema: {
          type: "object",
          properties: {
            id: {
              type: "string",
              format: "uuid",
              description: "UUID of the API key to delete",
            },
          },
          required: ["id"],
        },
        description: "Delete particular API keys",
      },
      updateUser: {
        private: true,
        account: true,
        admin: true,
        method: User.updateUser,
        schema: {
          type: "object",
          properties: {
            id: { type: "string", format: "uuid" },
            first_name: { type: "string", maxLength: 50 },
            last_name: { type: "string", maxLength: 50 },
            mobile: { type: "string", maxLength: 15 },
            status: { type: "string", enum: ["ACTIVE", "INACTIVE", "BLOCKED"] },
            telegram_id: { type: "string" },
            webhook_url: { type: "string" },
          },
          required: ["id"],
        },
        description: "Update user to account",
      },
      validateAdminToken: {
        private: true,
        method: UserToken.validateAdminToken,
        description: "Validate User Access Token",
        schema: {
          type: "object",
          properties: {},
        },
      },
      adminSignin: {
        realm: true,
        method: Signin.adminSignin,
        description: "Admin Login",
        schema: {
          type: "object",
          properties: {
            username: { type: "string", maxLength: 50 },
            password: { type: "string", secure: true },
          },
          required: ["username", "password"],
        },
        log: true,
        secureResponse: {
          user_token: true,
        },
      },
      adminSignout: {
        adminGet: true,
        realm: true,
        method: Signin.adminSignout,
        description: "Admin Logout",
        log: true,
      },
      getAdminProfile: {
        adminGet: true,
        realm: true,
        method: User.getAdminProfile,
        description: "Admin Profile",
        log: true,
      },
      sign: {
        method: Signup.sign,
        description: "Create signature",
        schema: {
          type: "object",
          properties: {
            privateKey: { type: "string", secure: true },
          },
        },
        log: true,
      },
    };
  }
}
