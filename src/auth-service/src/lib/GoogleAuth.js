import QRCode from "qrcode";
import speakeasy from "speakeasy";
import db from "@lib/db";
import crypto from "crypto";
import MemStore from "@lib/memstore";

async function googleAuthGenerateQR(data, realmId) {
  const res = await db.user.findOne({
    where: { login: data.login, realm: realmId },
    raw: true,
    attributes: ["google_auth"]
  });
  if (res && res && res.google_auth == true) return;

  let secret = await speakeasy.generateSecret({ length: 6 });
  var url = speakeasy.otpauthURL({
    secret: secret.ascii,
    label: "Falcon B2C",
    algorithm: "sha512"
  });
  let qr = await QRCode.toDataURL(url);
  await db.user.update(
    { base32secret: secret.base32 },
    {
      where: { login: data.login, realm: realmId }
    }
  );
  return { success: true, qr_code: qr, secret_key: secret.base32 };
}

async function googleAuthVerify(data, realmId) {
  const res = await db.user.findOne({
    where: { login: data.login, realm: realmId },
    raw: true,
    attributes: ["id", "base32secret"]
  });

  if (data.user_token == "") throw "USERTOKENISEMPTY";

  if (res && res.base32secret != undefined) {
    let base32secret = res.base32secret;
    let verified = speakeasy.totp.verify({
      secret: base32secret,
      encoding: "base32",
      token: data.user_token
    });
    const userRec = await MemStore.get("usr" + res.id);
    if (!!userRec) {
      const userToken = await MemStore.get("usr" + userRec);
      await MemStore.del("usr" + res.id);
      if (!!userToken) {
        await MemStore.del("usr" + userRec);
      }
    }

    const token = crypto.randomBytes(42).toString("hex");
    await MemStore.set("usr" + res.id, token, config.user_token_lifetime);
    await MemStore.set("usr" + token, res.id, config.user_token_lifetime);
    if (verified) return { success: true, token: token, verified: verified };

    throw "NOTVERIFIED";
  }
  throw "ERROR";
}

async function enableDisableGoogleAuth(data, realmId, userId) {
  let isVerified = await googleAuthVerify(data, realmId);
  if (isVerified == undefined || !isVerified.verified) throw "NOTVERIFIED";
  if (isVerified.verified) {
    await db.user.update(
      data.status == "enable"
        ? { google_auth: true }
        : { google_auth: false, base32secret: null },
      {
        where: { id: userId, realm: realmId }
      }
    );

    return { success: true };
  }
}

export default {
  googleAuthGenerateQR,
  googleAuthVerify,
  enableDisableGoogleAuth
};
