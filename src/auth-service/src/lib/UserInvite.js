import db from "@lib/db";
import Util from "@lib/shared";
import config from "@lib/config";
import Queue from "@lib/queue";
import moment from "moment";
import isIP from "is-ip";
import isUrl from "valid-url";
import shared from "@lib/shared";
const Crypto = shared.Crypto;
const randomize = require("randomatic");
const Op = db.Sequelize.Op;
import User from "./User";

async function inviteUser(data, service) {
  if (service.realmId) {
    var resp = await Queue.newJob("account-service", {
      method: "checkValidRealmId",
      data: { realm_id: service.realmId },
      options: {
        realmId: service.realmId
      }
    });
  }
  let inviteResp;
  if (data && data.merchant_account_ids == undefined) {
    data.merchant_account_ids = [];
  }
  if (
    data &&
    typeof data.merchant_account_id == "string" &&
    data.merchant_account_id.length
  ) {
    data.merchant_account_ids.push(data.merchant_account_id);
  }
  console.log(
    `INFO. UserInvite.js Func:inviteUser. received request with data:`,
    data
  );
  if (data.user_type === "API") {
    inviteResp = await _inviteApiUser(data, service);
  } else {
    for (let i = 0; i < data.merchant_account_ids.length; i++) {
      data.merchant_account_id = data.merchant_account_ids[i];
      inviteResp = await _invitePortalUser(data, service);
    }
  }
  console.log(`returning`);
  return inviteResp;
}

function _makeUsername(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function _generatePassword(passwordLength) {
  var chars =
    "0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  if (passwordLength == undefined) passwordLength = 12;
  var password = "";
  for (var i = 0; i <= passwordLength; i++) {
    var randomNumber = Math.floor(Math.random() * chars.length);
    password += chars.substring(randomNumber, randomNumber + 1);
  }
  return password;
}

async function checkAtLeastOneOwner(data, service) {
  const filter = {};
  if (service.realmId) {
    filter.realm_id = service.realmId;
  }
  const ownerRole = await db.role.findOne({
    where: { ...filter, role_name: "Owner", removed: 0 },
    raw: true
  });
  const user = await db.user.findOne({
    where: { email: data.email, removed: 0 },
    include: [
      {
        model: db.account,
        attributes: ["id"],
        as: "accounts"
      }
    ],
    raw: true
  });
  let invitations = null;
  if (ownerRole) {
    invitations = await db.user_accounts.findAll({
      where: {
        account_id: data.account_id,
        main_role: ownerRole.id,
        removed: 0
      },
      raw: true,
      type: db.Sequelize.QueryTypes.SELECT
    });

    if (!invitations.length) {
      invitations = await db.user_account_invitations.findAll({
        where: {
          account_id: data.account_id,
          role: ownerRole.id,
          removed: 0
        },
        raw: true,
        type: db.Sequelize.QueryTypes.SELECT
      });
    }
    if (data.id) {
      if (
        invitations &&
        invitations.length === 1 &&
        ((invitations[0].role &&
          invitations[0].id === data.id &&
          invitations[0].role !== data.role) ||
          (invitations[0].main_role &&
            user &&
            invitations[0].user_id === user.id &&
            invitations[0].main_role !== data.role))
      ) {
        console.log(
          `INFO. UserInvite.js Func:inviteUser. account have at list one owner`
        );
        throw new service.ServiceError(
          "BANJO_ERR_ATLEST_ONE_OWNER_IN_ACCOUNT_ERROR"
        );
      }
    }
  }
}
async function upsertUser(data, service, txControl) {
  try {
    const user = await db.user.findOne({
      where: { email: data.email, removed: 0 },
      include: [
        {
          model: db.account,
          attributes: ["id"],
          as: "accounts"
        }
      ],
      raw: true
    });
    let userData = {
      ...data,
      email: data.email,
      status: "INACTIVE",
      user_type: data.user_type
    };
    delete userData.id;
    var createdUser = null;
    if (!user) {
      createdUser = await db.user.create(userData, {
        transaction: txControl
      });
    } else if (user) {
      if (
        !data.status ||
        [db.user.ACTIVE, db.user.INACTIVE, db.user.BLOCKED].indexOf(
          data.status
        ) == -1
      ) {
        data.status = user.status;
      }
      await db.user.update(
        { ...data, id: user.id },
        {
          where: { id: user.id },
          transaction: txControl
        }
      );
    }
    return createdUser;
  } catch (error) {
    console.log(`error: upsertUser.js, func: _invitePortalUser, update user `);
    await txControl.rollback();
    throw error;
  }
}
async function updateUserInvitation(data, service, txControl) {
  try {
    await db.user_account_invitations.update(
      { role: data.role, token: data.token },
      {
        where: {
          email: data.email,
          account_id: data.account_id,
          merchant_account_id: data.merchant_account_id,
          removed: 0
        },
        transaction: txControl
      }
    );
    await db.user_accounts.update(
      { main_role: data.role, status: data.status },
      {
        where: {
          account_id: data.account_id,
          user_id: data.userId,
          merchant_account_id: data.merchant_account_id
        },
        transaction: txControl
      }
    );
    await txControl.commit();
    return "Changes update successfully";
  } catch (error) {
    console.log(
      "error: UserInvite.js, func: _invitePortalUser, update user failed error: ",
      JSON.stringify(error)
    );
    await txControl.rollback();
    throw error;
  }
}
async function _invitePortalUser(data, service) {
  console.log(
    `INFO. UserInvite.js Func:_invitePortalUser. received portal user request with data: ${data}`
  );
  const txControl = await db.sequelize.transaction();
  const filter = {};
  console.log("Finding user account invitations.");
  if (service.realmId) {
    filter.realm_id = service.realmId;
  }
  const invite = await db.user_account_invitations.findAll({
    where: {
      email: data.email,
      account_id: data.account_id,
      merchant_account_id: data.merchant_account_id,
      removed: 0
    },
    order: [["ctime", "DESC"]]
  });

  console.log("Finding user by email:", data.email);
  const user = await db.user.findOne({
    where: { email: data.email, removed: 0 },
    include: [
      {
        model: db.account,
        attributes: ["id"],
        as: "accounts"
      }
    ],
    raw: true
  });

  let invitation = null;
  if (data.id) {
    invitation = await db.user_account_invitations.findOne({
      where: {
        email: data.email,
        account_id: data.account_id,
        merchant_account_id: data.merchant_account_id,
        removed: 0
      }
    });
    await checkAtLeastOneOwner(data, service);
  }
  const role = await db.role.findOne({
    where: { ...filter, id: data.role, removed: 0 },
    raw: true
  });
  if (!role) {
    console.log(
      `INFO. UserInvite.js Func:inviteUser. Role by id not found:${data.role}`
    );
    throw new service.ServiceError("BANJO_ERR_INVALID_ROLE_ERROR");
  }

  var date = new Date();
  date.setDate(date.getDate() + config.invite_token_expiry);
  let token = Util.generateHash(data.email + new Date().getTime());

  let createdUser = await upsertUser(data, service, txControl);
  let userId = !!createdUser ? createdUser.dataValues.id : user.id;

  if (invitation && invitation.accepted && data.id) {
    return await updateUserInvitation(
      { ...data, userId: userId, token: token },
      service,
      txControl
    );
  }

  const { userExists, accepted } = await _checkIfAlreadyInAccount(
    invite,
    data,
    userId
  );
  if (userExists) {
    console.log(
      `UserInvite.js, func: _invitePortalUser, user already in account, userExists`
    );
    throw new service.ServiceError("BANJO_ERR_USER_ALREADY_IN_ACCOUNT");
  }
  //Vaibhav Vaidya, 3 Feb 2023, Update logic to add resouce/permission records

  let inviteData = {
    ...data,
    token: token,
    is_registered_user: false,
    expiry: date,
    user_id: userId,
    counter: 1,
    merchant_account_id: data.merchant_account_id
  };
  let userName = "";
  if (user) {
    let isAlreadyInAccount = false;
    if (user.accounts) {
      user.accounts.forEach((acc) => {
        if (acc.id === data.account_id) {
          isAlreadyInAccount = true;
        }
      });
    }
    if (isAlreadyInAccount) {
      throw new service.ServiceError("BANJO_ERR_USER_ALREADY_IN_ACCOUNT");
    }
    //Vaibhav Vaidya, 20 April 2023, Update logic
    if (accepted) inviteData.is_registered_user = true;
    if (user.first_name && user.last_name) {
      userName = `${user.first_name.charAt(0).toUpperCase() +
        user.first_name.slice(1)} ${user.last_name
        .charAt(0)
        .toUpperCase()}${user.last_name.slice(1)}`;
    } else if (user.first_name) {
      userName = `${user.first_name.charAt(0).toUpperCase() +
        user.first_name.slice(1)}`;
    }
  }
  try {
    if (invitation && data.id) {
      await db.user_account_invitations.update(
        { role: data.role, token },
        {
          where: { id: invitation.id },
          transaction: txControl
        }
      );
    } else if (invite.length === 0) {
      await db.user_account_invitations.create(inviteData, {
        transaction: txControl
      });
    } else {
      await db.user_account_invitations.update(
        {
          counter: invite[0].dataValues.counter + 1,
          token,
          expiry: date
        },
        {
          where: { id: invite[0].dataValues.id },
          transaction: txControl
        }
      );
    }
    await txControl.commit();
  } catch (error) {
    console.log("User creation failed");
    await txControl.rollback();
    throw error;
  }
  try {
    await sendEmailInvite({ ...data, userName, token }, service);
  } catch (error) {
    console.log(
      `For registering user email ${data.email} got email failure: ` + error
    );
  }
  return "Invite sent successfully";
}

async function sendEmailInvite(data, service) {
  try {
    console.log("Calling notify.invite-email API");
    Queue.publish(`NOTIFY.INVITE-EMAIL`, {
      user_id: null,
      options: { send_sms: false, send_push: false, to_email: data.email },
      payload: {
        invite_user_name: data.userName,
        token: data.token,
        base_url: config.app_url,
        email: data.email
      }
    });
    console.log("Invite sent successfully");
  } catch (error) {
    console.log(
      `For registering user email ${data.email} got email failure: ` + error
    );
  }
}
async function _inviteApiUser(data, service) {
  console.log(
    `INFO. UserInvite.js Func:_inviteApiUser. received API user request with data: ${data}`
  );
  let apiKeysResp = await generateApiKeys(data, service);
  let username, genPassword;
  if (apiKeysResp && apiKeysResp.success) {
    username = apiKeysResp.apiKey;
    genPassword = apiKeysResp.secretKey;
  } else {
    console.log(
      `UserInvite.js. Func:_inviteApiUser. Failed to generate API credentials. Resp:`,
      apiKeysResp
    );
    throw "BANJO_ERR_GENERATE_KEY_FAIL";
  }
  //Send email with credentials to owners email id:
  let owner_user = await db.sequelize.query(
    `select u.email
        from ${db.schema}.users u
        join ${db.schema}.user_accounts ua on u.id = ua.user_id and ua.user_id = $1 and ua.account_id=$2 and ua.end_date is null`,
    {
      bind: [data.owner_id, data.account_id],
      raw: true,
      type: db.sequelize.QueryTypes.SELECT
    }
  );
  if (!(owner_user && owner_user[0])) {
    throw new service.ServiceError("BANJO_ERR_OWNER_ACCOUNT_NOT_EXISTENT");
  }

  console.log(
    `UserInvite.js, func: _inviteApiUser, owner_user: `,
    owner_user[0]
  );
  console.log("calling notify.api-user-credential-email");
  Queue.publish(`NOTIFY.API-USER-CREDENTIALS-EMAIL`, {
    options: {
      send_sms: false,
      send_push: false,
      to_email: owner_user[0].email
    },
    payload: {
      username: username,
      password: genPassword
    }
  });
  return "Invite sent successful";
}

async function _checkIfOwnerExists(data, service) {
  console.log(
    `INFO. UserInvite.js Func:_checkIfOwnerExists. checking if owner exists for data: ${data}`
  );
  const owner_user = await db.user_accounts.findOne({
    attributes: ["id"],
    where: { removed: 0, account_id: data.account_id },
    include: [
      {
        model: db.role,
        as: "role",
        attributes: ["id", "role_name"],
        where: {
          role_name: "owner",
          removed: 0
        }
      }
    ],
    raw: true
  });
  console.log(
    `INFO. UserInvite.js Func:_checkIfOwnerExists. owner_user: ${owner_user}`
  );
  return !!owner_user;
}

async function _checkIfAlreadyInAccount(invite, data, userId) {
  console.log(
    `UserInvite.js, func: _checkIfAlreadyInAccount, checking if already in account with data: ${JSON.stringify(
      data
    )} and invite: ${invite} from user: ${userId}`
  );
  const invitations = await db.user_account_invitations.findAll({
    where: {
      email: data.email,
      removed: 0
    },
    order: [["ctime", "DESC"]]
  });
  let accepted_invite = invitations.filter((el) => {
    return el.dataValues.accepted === true;
  });
  var body = {
    userExists: false,
    accepted: null
  };
  if (accepted_invite.length > 0) {
    body.accepted = true;
    const user_accounts = await db.user_accounts.findOne({
      where: {
        removed: 0,
        account_id: data.account_id,
        merchant_account_id: data.merchant_account_id,
        end_date: null,
        main_role: data.role,
        user_id: userId
      }
    });
    if (!!user_accounts) {
      body.userExists = true;
    }
  }
  console.log(
    `UserInvite.js, func: _checkIfAlreadyInAccount, return value: ${body}`
  );
  return body;
}
/**
 * @method generateApiKeys
 * @author Datta Bhise
 * @since 2 Feb
 * @summary Generate unique key
 */
async function generateUniqueApiKey(length = 18, transaction) {
  let apiKey = randomize("A0", length);
  const u = await db.user.findOne({
    where: db.Sequelize.where(
      db.Sequelize.fn("UPPER", db.Sequelize.col("username")),
      "=",
      apiKey
    ),
    transaction,
    attributes: ["id"]
  });
  if (u) {
    return generateUniqueApiKey(length);
  }
  return apiKey;
}
/**
 * @method generateApiKeys
 * @author Vaibhav Vaidya
 * @since 11 Jan 2022
 * @summary Generate new API key for storing on BE side
 */
async function generateApiKeys(data, service) {
  if (data && typeof data == "object") {
    //Vaibhav Vaidya, 31 Jan 2023. Allow
    data.permissions = ["allow", "write"];
  }

  if (data && typeof data.webhook_url == "string") {
    if (!isUrl.isUri(data.webhook_url)) {
      console.log(
        `INFO. Func:generateApiKeys. Passed webhook url not a valid url:`,
        data.webhook_url
      );
      throw new service.ServiceError("BANJO_ERR_INVALID_URL");
    }
  }
  if (data && Array.isArray(data.ips)) {
    if (data.ips.length > 0) {
      for (let i = 0; i < data.ips.length; i++) {
        if (data.ips[i] != undefined && !isIP(data.ips[i])) {
          console.log(
            `INFO. Func:generateApiKeys. Tested IP value:${data.ips[i]} is invalid`
          );
          throw new service.ServiceError("BANJO_ERR_INVALID_IPS");
        }
      }
    }
  } else {
    console.log(
      `INFO. Func:generateApiKeys. Invalid datatype for IPs:${typeof data.ips}. Array check:${Array.isArray(
        data.ips
      )}`
    );
    throw new service.ServiceError("BANJO_ERR_INVALID_TYPE_IPS");
  }
  let realmFilter = {};
  if (service.realmId) {
    var resp = await Queue.newJob("account-service", {
      method: "checkValidRealmId",
      data: { realm_id: service.realmId },
      options: {
        realmId: service.realmId
      }
    });
    realmFilter.realm_id = service.realmId;
  }
  let checkSql = `
    select count(ua.id) from
    ${db.schema}.user_accounts ua
    inner join ${db.schema}.users u on (u.id=ua.user_id and u.removed=0 and u.status in ('ACTIVE'))
    inner join ${db.schema}.accounts ac on (ac.id=ua.account_id and ac.removed=0 and ac.status in ('ACTIVE'))
    inner join ${db.schema}.merchant_accounts ma on (ma.account_id=ac.id and ma.removed=0 and ma.status in ('ACTIVE'))
    where ua.start_date is not null and ua.end_date is null and ma.id=$1
  `;
  let bind = [data.merchant_account_id];
  if (service && service.userId) {
    checkSql += `and ua.user_id=$2`;
    bind.push(service.userId);
  }
  let fetchMerchAcc = await db.sequelize.query(checkSql, {
    bind: bind,
    type: db.sequelize.QueryTypes.SELECT,
    raw: true
  });
  if (fetchMerchAcc && fetchMerchAcc[0] && fetchMerchAcc[0].count < 1) {
    throw new service.ServiceError("BANJO_ERR_OWNER_API_ACCESS_DENIED");
  }
  const { privateKey, publicKey } = Crypto.generateKeyPair();
  const txControl = await db.sequelize.transaction();
  let apiKey = await generateUniqueApiKey(18, txControl);
  try {
    let userData = {
      first_name: data.key_name,
      username: apiKey,
      password: publicKey,
      webhook_url: data.webhook_url,
      ips: data.ips,
      user_type: "API",
      removed: 0,
      mobile_verified: false,
      email_verified: false,
      maker: service.userId,
      status: db.user.STATUS.ACTIVE
    };
    let resourceData = {
      ...realmFilter,
      alias: config.resourceIdentifier.API,
      identifier: data.merchant_account_id,
      resource_type: config.resourceType.API,
      object_type: config.resourceObjectType.MERCHANT_ACCOUNT,
      object_id: data.merchant_account_id,
      maker: service.userId
    };
    const mAcc = await db.merchant_accounts.findOne({
      where: { id: data.merchant_account_id, removed: 0 },
      attributes: ["id", "name", "account_id"],
      raw: true,
      logging: console.log
    });
    if (!mAcc) {
      console.error(
        `Func:generateAPIKeys. Didn't find Merchant for Merchant Account id:${data.merchant_account_id}`
      );
      throw new service.ServiceError("BANJO_ERR_ACCOUNT_DOES_NOT_EXIST");
    }
    //Step 1
    const user = await db.user.create(userData, {
      transaction: txControl,
      raw: true
    });
    if (user && user.id) {
      let userAccData = {
        account_id: mAcc.account_id,
        user_id: user.id,
        maker: service.userId,
        mail_role: null,
        additional_roles: null,
        start_date: new Date(),
        end_date: null,
        merchant_account_id: mAcc.id
      };
      //Step 2
      let userAccWrite = await db.user_accounts.create(userAccData, {
        transaction: txControl,
        raw: true
      });
      if (userAccWrite && userAccWrite.id) {
        //Step 3
        let resourceWrite;
        const resourceRecord = await db.resource.findOne({
          where: {
            ...realmFilter,
            object_id: data.merchant_account_id,
            removed: 0
          },
          raw: true
        });
        if (!(resourceRecord && resourceRecord.id)) {
          resourceWrite = await db.resource.create(resourceData, {
            transaction: txControl,
            raw: true
          });
        } else {
          resourceWrite = resourceRecord;
        }
        if (resourceWrite && resourceWrite.id) {
          if (
            data.permissions &&
            Array.isArray(data.permissions) &&
            data.permissions.length
          ) {
            let allowFlag = false,
              writeFlag = false,
              denyFlag = false;
            for (let i = 0; i < data.permissions.length; i++) {
              switch (data.permissions[i]) {
                case "allow":
                  allowFlag = true;
                  break;
                case "write":
                  writeFlag = true;
                  break;
                case "deny":
                  denyFlag = true;
                  break;
                default:
                  break;
              }
            }
            let permissionData = {
              ...realmFilter,
              maker: service.userId,
              removed: 0,
              resource_id: resourceWrite.id,
              role_id: null,
              user_id: user.id,
              allow: allowFlag,
              deny: writeFlag,
              write: denyFlag
            };
            //Step 4
            let permissionWrite = await db.permission.create(permissionData, {
              transaction: txControl,
              raw: true
            });
          }
          txControl.commit();
          return {
            success: true,
            apiKey,
            secretKey: Crypto.createPrivateKey(privateKey).export({
              format: "pem",
              type: "pkcs1"
            })
          };
        } else {
          console.error(
            `ERR. UserInvite. Func:generateAPIKeys, Failed to write record in User table`
          );
          throw new service.ServiceError("BANJO_ERR_DB_WRITE_FAIL");
        }
      } else {
        console.log(
          `INFO. Func:generateApiKeys. Invalid datatype for IPs:${typeof data.ips}. Array check:${Array.isArray(
            data.ips
          )}`
        );
        throw new service.ServiceError("BANJO_ERR_DB_WRITE_FAIL");
      }
    } else {
      console.error(
        `ERR. UserInvite. Func:generateAPIKeys, Failed to write record in User table`
      );
      throw new service.ServiceError("BANJO_ERR_DB_WRITE_FAIL");
    }
  } catch (err) {
    console.log(
      `UserInvite. Func:generateAPIKeys. Caught Error:`,
      JSON.stringify(err)
    );
    throw err;
  }
}

/**
 * @method fetchApiKeys
 * @author Vaibhav Vaidya
 * @since 11 Jan 2022
 * @summary Generate new API key for storing on BE side
 */
async function fetchApiKeys(data, service) {
  const userAccs = await db.sequelize.query(
    `with user_list as
    (
    select u.id, u.first_name, u.ips, u.webhook_url
    from  ${db.schema}.users u where (u.maker=$1) and u.removed=0
    )
    select ul.* from user_list ul
    inner join ${db.schema}.user_accounts ua on (ua.user_id=ul.id  and ua.removed=0 and ua.end_date is null)
    inner join ${db.schema}.accounts ac on (ua.account_id=ac.id and ac.removed=0)
    inner join ${db.schema}.merchant_accounts macc on (ac.id=macc.account_id and macc.removed=0 and macc.id=$2)`,
    {
      bind: [service.userId, data.merchant_account_id],
      raw: true,
      type: db.sequelize.QueryTypes.SELECT
    }
  );
  return { success: true, api_keys: userAccs };
}
/**
 * @method fetchUserInviteList
 * @author Vaibhav Vaidya
 * @since 13 Jan 2022
 * @summary Fetch List of all Portal users that were invited
 */
async function fetchUserInviteList(data, service) {
  if (!service || !service.userId) {
    console.log(
      `INFO. func:fetchUserInviteList. API requires User logged in for access.`
    );
    throw new service.ServiceError("BANJO_ERR_ACCESSTOKEN");
  }
  let sqlQuery = `
  select ui.id,coalesce(ui.first_name || ' ' || ui.last_name, ui.email) as name, ui.account_id, ui.user_id, ui.role, 
	  ui.removed, ui.maker, ui.start_date, ui.first_name, ui.last_name, ui.user_type, ui.status, ui.email,r.role_name,a2.name as account_name,ui.expiry,merchant_account_id,ma.name as merchant_account_name 
	  from ${db.schema}.vw_user_invitations ui
    left join ${db.schema}.roles r on ui.role= r.id
	  left join ${db.schema}.accounts a2 on ui.account_id= a2.id 
    left join ${db.schema}.merchant_accounts ma on ui.merchant_account_id= ma.id
	  where ui.removed=0 and ui.account_id=$1 and accepted is not true  and 
    ma.account_id in (select account_id from
    ${db.schema}.vw_user_accounts
    where
    id = '${service.userId}' and ma.id = vw_user_accounts.merchant_account_id )`;

  if (data.merchant_account_ids && data.merchant_account_ids.length) {
    sqlQuery +=
      " AND ma.id IN (" +
      data.merchant_account_ids.map((id) => `'${id}'`) +
      ")";
  }
  sqlQuery += " order by ui.ctime desc";
  const limit = Math.min(parseInt(data.page_size) || 100, 200);
  let page = Math.max(parseInt(data.page) || 1);
  const offset = (page - 1) * limit;

  let countQuery = `select count(*) from (${sqlQuery}) data`;
  let limitQuery = ` offset $2 limit $3`;
  let count = 0;
  const countResp = await db.sequelize.query(countQuery, {
    bind: [data.account_id],
    raw: true,
    type: db.sequelize.QueryTypes.SELECT
  });
  if (countResp && countResp[0] && !isNaN(countResp[0].count)) {
    count = countResp[0].count;
  }
  const userInvitesList = await db.sequelize.query(sqlQuery + limitQuery, {
    bind: [data.account_id, offset, limit],
    raw: true,
    type: db.sequelize.QueryTypes.SELECT
  });
  return { success: true, count, inviteList: userInvitesList };
}

/**
 * @method removeAPIkey
 * @author Vaibhav Vaidya
 * @since 30 Jan 2022
 * @summary Delete particular API key from current User list
 */
async function removeAPIkey(data, service) {
  if (!service || !service.userId) {
    console.log(
      `INFO. func:removeAPIKey. API requires User logged in for access.`
    );
    throw new service.ServiceError("BANJO_ERR_ACCESSTOKEN");
  }
  if (service.realmId) {
    await checkValidUser(
      { realm_id: service.realmId, user_id: service.userId },
      service
    );
  }
  let sqlQuery = `
  select u.id, u.first_name, u.ips, u.webhook_url
    from  ${db.schema}.users u where (u.maker=$1) and u.removed=0 and u.id=$2 and user_type=$3
  `;
  const sqlResp = await db.sequelize.query(sqlQuery, {
    bind: [service.userId, data.id, "API"],
    raw: true,
    type: db.sequelize.QueryTypes.SELECT
  });
  if (sqlResp && sqlResp.length) {
    const txControl = await db.sequelize.transaction();
    try {
      await db.user.destroy({
        where: { id: data.id, maker: service.userId },
        transaction: txControl
      });
      await db.user_accounts.destroy({
        where: { id: data.id, maker: service.userId },
        transaction: txControl
      });
      await db.permission.destroy({
        where: { user_id: data.id, maker: service.userId },
        transaction: txControl
      });
      await txControl.commit();
      return {
        success: true,
        message: "API key has been successfully removed."
      };
    } catch (err) {
      await txControl.rollback();
      console.log("INFO. Func: removeAPIkey. Caught Exception, err:", err);
      throw err;
    }
  } else {
    console.log(
      `INFO. func:removeAPIkey. User record with id:${data.id} and Maker id:${service.userId} not found.`
    );
    throw "BANJO_ERR_API_KEY_NOT_FOUND";
  }
}

async function resendInvitation(data, service) {
  try {
    if (service.realmId) {
      await Queue.newJob("account-service", {
        method: "checkValidRealmId",
        data: { realm_id: service.realmId },
        options: {
          realmId: service.realmId
        }
      });
    }
    const res = await Queue.newJob("account-service", {
      method: "usersAccountRoleVerification",
      data: data,
      options: service
    });
    if (!res) {
      throw "BANJO_ERR_INVALID_USER";
    }
    if (res && !res.result.role_verified) {
      throw "BANJO_ERR_ACCESS_DENIED_TO_RESEND_INVITATION";
    }
    return await inviteUser(data, service);
  } catch (err) {
    console.error(
      `UserInvite.js. Func:resendInvitation. Caught Exception:`,
      err
    );
    throw err;
  }
}

export default {
  inviteUser,
  generateApiKeys,
  fetchApiKeys,
  fetchUserInviteList,
  removeAPIkey,
  resendInvitation
};
