import db from "@lib/db";
import config from "@lib/config";
import { ServiceError } from "@lib/error";
import crypto from "crypto";
import sha1 from "sha1";
import MemStore from "@lib/memstore";
import Queue from "@lib/queue";
import UserToken from "./UserToken";
import Util from "@lib/shared";

const Op = db.Sequelize.Op;

async function signin(data, service) {
  const user = await db.user.findOne({
    //Vaibhav Vaidya, 10 Jan 2023, username can contain both email/username from FE.
    where: {
      [Op.or]: [
        db.Sequelize.where(
          db.Sequelize.fn("LOWER", db.Sequelize.col("username")),
          "=",
          data.username.toLowerCase()
        ),
        db.Sequelize.where(
          db.Sequelize.fn("LOWER", db.Sequelize.col("email")),
          "=",
          data.username.toLowerCase()
        )
      ],
      removed: 0,
      //email_verified: true,
      user_type: db.user.USER_TYPE.PORTAL
    },
    attributes: ["id", "password", "status"],
    raw: true,
    type: db.sequelize.QueryTypes.SELECT
  });
  if (user && user.status != db.user.STATUS.ACTIVE) {
    console.error(
      `Auth-service. Func: signin. Non-Active User status found for User:`,
      user
    );
    throw new service.ServiceError("BANJO_ERR_USER_STATUS_INACTIVE");
  }
  const isValidPassword = () => {
    return (
      user &&
      user.password &&
      user.password === Util.generateHash(data.password)
    );
  };
  return UserToken.generateUserToken(
    {
      login: data.username,
      user_id: user ? user.id : null,
      realm_id: service.realmId,
      valid_attempt: isValidPassword(),
      source: "password"
    },
    service
  );
}
async function signout(data, service) {
  return await UserToken.deregisterUserToken(data, service);
}
function _hashPassword(password, uid) {
  return crypto
    .createHash("sha256")
    .update(password)
    .digest("hex");
}
// Signin Using Admin (For BOT)
async function adminSignin(data, service) {
  const user = await db.admin_user.findOne({
    where: {
      [Op.or]: [
        db.Sequelize.where(
          db.Sequelize.fn("LOWER", db.Sequelize.col("login")),
          "=",
          data.username.toLowerCase()
        )
      ],
      removed: 0
    },
    logging: console.log,
    attributes: ["_id", "pass", "status"],
    raw: true,
    type: db.sequelize.QueryTypes.SELECT
  });
  if (user && user.status != db.admin_user.STATUS.ACTIVE) {
    console.error(
      `Auth-service. Func: adminSignin. Non-Active User status found for User:`,
      user
    );
    throw new service.ServiceError("BANJO_ERR_USER_STATUS_INACTIVE");
  }
  const isValidPassword = () => {
    return (
      user &&
      user.pass &&
      user.pass === Util.generateHash(data.password, "sha1")
    );
  };
  return UserToken.generateAdminUserToken(
    {
      login: data.username,
      user_id: user ? user._id : null,
      valid_attempt: isValidPassword(),
      source: "bot"
    },
    service
  );
}
async function adminSignout(data, service) {
  return await UserToken.deregisterAdminToken(data, service);
}
export default {
  signin,
  signout,
  _hashPassword,
  adminSignin,
  adminSignout
};
