import db from "@lib/db";
import config from "@lib/config";
import MemStore from "@lib/memstore";
import Queue from "@lib/queue";
import uuid from "uuid/v4";
import randomize from "randomatic";
import User from "./User";

async function resendOtp(data, realm_id, user_id) {
  const otpJsonString = await MemStore.get(`otp${user_id}`);
  if (!otpJsonString) throw "OTPNOTFOUND";

  const user = await db.user.findOne({ where: { id: user_id } });
  if (!user) throw "USERNOTFOUND";
  await User.checkValidUser({ realm_id, user_id: user.id });
  const otpObject = JSON.parse(otpJsonString);
  await sentOtpByTransport(user, otpObject.otp, data, realm_id);
  return { success: true };
}

async function check(data, realm_id, user_id) {
  if (!data.otp) throw "INVALIDOTPREQUEST";

  const otpJsonString = await MemStore.get(`otp${user_id}`);
  if (!otpJsonString) throw "OTPNOTFOUND";

  const otpObject = JSON.parse(otpJsonString);

  if (data.type == 1) {
    const user = await db.user.findOne({
      where: { id: user_id },
      attributes: ["id", "login"]
    });
    const ga = await Queue.newJob("auth-service", {
      method: "googleAuthVerify",
      data: {
        login: user.dataValues.login,
        user_token: data.otp
      },
      realmId: otpObject.data.realmId,
      userId: otpObject.data.userId
    });

    if (!ga.result.verified) {
      await incrementAttemps(otpObject, user_id);
      throw "WRONGGOOGLEAUTH";
    }
  } else if (data.type != 1 && data.otp != otpObject.otp) {
    await incrementAttemps(otpObject, user_id);
    throw "WRONGOTP";
  }

  const res = await Queue.newJob(otpObject.data.header.service, {
    method: otpObject.data.header.method,
    data: otpObject.data.data,
    realmId: otpObject.data.realmId,
    userId: otpObject.data.userId
  });

  if (res.error) throw res.error;

  return res.result;
}
async function incrementAttemps(otpObject, user_id) {
  otpObject.attempt++;
  if (otpObject.attempt >= config.otp_attempts) {
    // await MemStore.del(`otp${operationId}`);
    throw "OTPATTEMPTSREACHED";
  }
  await MemStore.set(
    `otp${user_id}`,
    JSON.stringify(otpObject),
    config.otp_timeout || 300
  );
  return;
}

async function checkPreviouseAttempts(user_id) {
  const otpJsonString = await MemStore.get(`otp${user_id}`);
  if (otpJsonString) {
    const prevOtpSet = JSON.parse(otpJsonString);
    if (prevOtpSet && prevOtpSet.attempt >= config.otp_attempts) {
      throw "OTPATTEMPTSREACHED";
    }
    return prevOtpSet.attempt;
  }
  return 0;
}

async function setOtp(data, service) {
  const user = await db.user.findOne({ where: { id: data.userId } });
  if (!user) throw "USERNOTFOUND";
  await User.checkValidUser(
    { realm_id: service.realmId, user_id: user.id },
    service
  );
  const operationId = uuid();
  const otp = user.otp_transport == "test" ? "111111" : randomize("0", 6);

  const attempt = await checkPreviouseAttempts(data.userId);

  await MemStore.set(
    `otp${user.id}`,
    JSON.stringify({
      otp,
      attempt,
      data
    }),
    config.otp_timeout || 300
  );

  await sentOtpByTransport(user, otp, data, realm);

  return {
    otpRequred: true,
    operationId,
    transport: user.get("otp_transport")
  };
}

async function sentOtpByTransport(user, otp, data, realm) {
  switch (user.get("otp_transport")) {
    case "telegram":
      await sentOtpByTelegram(user, otp);
      break;
    case "email":
      await sentOtpByEmail(user, otp, data, realm);
      break;
    case "sms":
      await sentOtpBySms(user, otp, data, realm);
      break;
    case "test":
      await sentOtpForTest(otp);
      break;
    default:
      throw "OTPTRANSPORTNOTDEFINED";
  }
}

async function sentOtpByTelegram(user, otp) {
  // send by telegram
  const message = config.otp_message.replace("{otp}", otp);
}

async function sentOtpByEmail(user, otp, data, realmId) {
  // send by email
  Queue.newJob("mail-service", {
    method: "send",
    data: {
      lang: arguments[7] ? arguments[7].lang || "en" : "en",
      code: "otp",
      to: user.get("email"),
      body: {
        otp,
        data
      }
    },
    realmId: data.realmId || realmId
  });
}

async function sentOtpBySms(user, otp, data, realmId) {
  // send by sms
  let to = user.get("phone");
  if (data.data) {
    if (data.data.new_phone) to = data.data.new_phone;
  }
  if (data.operation) {
    if (data.operation.new_phone) to = data.operation.new_phone;
  }

  Queue.newJob("mail-service", {
    method: "sms",
    data: {
      lang: arguments[7] ? arguments[7].lang || "en" : "en",
      code: "otp",
      to,
      body: {
        otp,
        data
      }
    },
    realmId: data.realmId || realmId
  });
}
async function sentOtpForTest(otp) {
  // send for test
  await MemStore.set(`testotp`, otp, config.otp_timeout || 300);
}

export default {
  check,
  resendOtp,
  setOtp
};
