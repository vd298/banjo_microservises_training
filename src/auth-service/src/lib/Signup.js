import MemStore from "@lib/memstore";
import db from "@lib/db";
import sha1 from "sha1";
import Queue from "@lib/queue";
import randomize from "randomatic";
import config from "@lib/config";
import Signin from "./Signin";
import Util from "@lib/shared";
import uuid from "uuid/v4";
import User from "./User";
import crypto from "crypto";

const Op = db.Sequelize.Op;

async function verifyEmailRequest(data, service) {
  const userData = await db.user.findOne({
    where: { email: data.email }
  });
  if (!userData) {
    throw new service.ServiceError("BANJO_ERR_USER_NOT_FOUND_ERROR");
  }
  let code = await MemStore.get("ver" + userData.dataValues.id);
  if (code) {
    throw "BANJO_ERR_VERIFY_EMAIL_ALREADY_SENT";
  }
  MemStore.set("ver" + userData.dataValues.id, true, 60);
  let token;
  let res;
  let tokenLifetime;
  let emailConfirmed = userData.email_verified;
  if (!emailConfirmed) {
    token = uuid();
    res = await _sendEmailVerification(data, token);
    tokenLifetime = res.tokenLifetime;
  } else {
    throw new service.ServiceError("BANJO_ERR_EMAIL_VERIFY");
  }
  if (config.isTestEnv()) {
    return { success: true, token, code: res.code };
  } else {
    return { success: true };
  }
}
async function _sendEmailVerification(data, token) {
  const tokenLifetime = config.limits.auth.otpLifetime.emailVerification;
  const emailCode = "1110-EMAIL-CONFIRM";
  let storing = data.email;
  const code = await _createCode(token, storing, tokenLifetime);
  Queue.publish(`NOTIFY.${emailCode}`, {
    user_id: null,
    options: { to_email: data.email },
    payload: {
      body: {
        code,
        token,
        userId: data.email.replace("+", "%2b"),
        expiryTime: Math.floor(tokenLifetime / 3600)
      }
    }
  });
  return { tokenLifetime, token, code };
}

async function _createCode(token, item, expiryTime = 300) {
  let code = await MemStore.get("wcd" + token);
  if (code) code = code.split(":")[0];
  else {
    code = config.testAuthOtp ? "000000" : randomize("0", 6);
  }
  await MemStore.set(
    "wcd" + token,
    code + ":" + item,
    expiryTime,
    config.limits.auth.storeType || "EX"
  );
  config.isTestEnv() ? (code = "056179") : code;
  return code;
}

async function verifyEmail(data, service) {
  if (!data.code || !data.token)
    throw new service.ServiceError("BANJO_ERR_DATA_VALIDATION");
  let stored = await _checkCode(data.token, data.code);
  if (!stored) throw new service.ServiceError("BANJO_ERR_WRONG_CODE");
  const email = stored;
  let userData = await db.user.findOne({
    where: { email }
  });
  if (!userData) {
    throw new service.ServiceError("BANJO_ERR_USER_NOT_FOUND_ERROR");
  }
  let invite = await db.user_account_invitations.findOne({
    where: { user_id: userData.id }
  });
  if (!userData.email_verified) {
    await db.user.update(
      {
        email_verified: true
      },
      {
        where: { email }
      }
    );
  }
  if (invite && !invite.accepted) {
    await db.user_account_invitations.update(
      {
        accepted: true
      },
      {
        where: { user_id: userData.id }
      }
    );
  }
  await _delCode(data.token);
  return { success: true };
}

async function _checkCode(token, code, service) {
  let stored;
  config.isTestEnv()
    ? (stored = "056179:ap269@enovate-it.com")
    : (stored = await MemStore.get("wcd" + token));

  if (!stored) return false;
  const res = stored.split(":");
  if (res[0] !== code) {
    throw new service.ServiceError("BANJO_ERR_WRONG_CODE");
  }
  return res;
}

async function _delCode(token) {
  try {
    await MemStore.del(token);
  } catch (e) {}
  try {
    await MemStore.del("chk-" + token);
  } catch (e) {}
  try {
    await MemStore.del("wcd" + token);
  } catch (e) {}
}
async function _isOWASPFormat(data, service) {
  try {
    const regExp = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*()]).{8,}/;
    const validPassword = regExp.test(data.password);
    if (!validPassword) {
      throw new service.ServiceError("BANJO_ERR_INVALID_PASSWORD_ERROR");
    } else {
      return validPassword;
    }
  } catch (error) {
    console.log("File: Signup.js, Func: _isOWASPFormat, Err:", error);
    throw error;
  }
}
async function signup(data, service) {
  const transaction = await db.sequelize.transaction();
  const invite = await getInviteData(data, service, transaction);
  if (data && typeof data.email == "string") {
    data.email = data.email.toLowerCase();
  }
  if (data && typeof data.username == "string") {
    data.username = data.username.toLowerCase();
  }
  if (
    data &&
    typeof data.username == "string" &&
    typeof data.password == "string" &&
    data.username.toLowerCase() === data.password.toLowerCase()
  ) {
    throw new service.ServiceError(
      "BANJO_ERR_USER_NAME_AND_PASSWORD_SHOULD_BE_DIFFERENT_ERROR"
    );
  }
  await _isOWASPFormat(data, service);
  let userData = {
    first_name: data.first_name,
    user_type: data.user_type,
    last_name: data.last_name,
    email: data.email,
    username: data.username,
    password: Util.generateHash(data.password),
    email_verified: true,
    status: db.user.STATUS.ACTIVE
  };
  if (data.telegram_id) {
    userData.telegram_id = data.telegram_id;
  }

  let user = await db.user.findOne({
    where: {
      [db.Sequelize.Op.or]: [
        { email: data.email },

        { username: data.username }
      ],
      removed: 0,
      email_verified: true
    },
    transaction
  });
  if (user) {
    throw new service.ServiceError("BANJO_ERR_USER_ALREADY_REGISTERED_ERROR");
  }
  try {
    await db.user.update(userData, {
      where: { email: data.email },
      transaction
    });
    data.invite = invite;
    await acceptInvite(data, service, transaction);
    await transaction.commit();
    const res = await Queue.newJob("auth-service", {
      method: "signin",
      data: data,
      options: service
    });

    return res;
  } catch (err) {
    await transaction.rollback();
    console.log("File: Signup.js, Func: signup, Err:", err);
    throw err;
  }
}

async function acceptInvite(data, service, transaction) {
  const invite = data.invite;
  const user = await db.user.findOne({
    where: {
      email: invite.email,
      removed: 0
    },
    transaction
  });

  if (!user) {
    console.log(
      "File: Signup.js Func: acceptInvite, Err: user with given invite not found, Email:",
      invite.email
    );
    throw new service.ServiceError("BANJO_ERR_INVALID_USER_INVITE_ERROR");
  }
  if (service.realmId) {
    await User.checkValidInviteUser(
      { realm_id: service.realmId, user_id: user.id },
      service
    );
  }
  await db.user_accounts.create(
    {
      account_id: invite.account_id,
      user_id: user.id,
      main_role: invite.role,
      merchant_account_id: invite.merchant_account_id,
      start_date: new Date()
    },
    {
      transaction
    }
  );
  await db.user_account_invitations.update(
    { accepted: true, is_registered_user: true },
    {
      where: {
        id: invite.id
      },
      transaction
    }
  );
  await db.user_account_invitations.update(
    { is_registered_user: true },
    {
      where: {
        email: invite.email,
        removed: 0
      },
      transaction
    }
  );
  return "Invite accepted successfully";
}

async function getInviteData(data, service, transaction) {
  const invite = await db.user_account_invitations.findOne({
    attributes: [
      "id",
      "email",
      "account_id",
      "expiry",
      "role",
      "is_registered_user",
      "token",
      "accepted",
      "merchant_account_id"
    ],
    where: {
      token: data.token,
      removed: 0
    },
    include: [
      {
        model: db.account,
        attributes: ["name"],
        as: "accounts",
        where: { removed: 0 }
      },
      {
        model: db.role,
        attributes: ["role_name"],
        as: "main_role",
        where: { removed: 0 }
      }
    ],
    transaction
  });
  if (!invite) {
    throw new service.ServiceError("BANJO_ERR_INVALID_INVITE_ERROR");
  }
  if (invite.accepted) {
    throw new service.ServiceError("BANJO_ERR_INVITE_ALREADY_ACCEPTED_ERROR");
  }
  if (invite.accepted === false) {
    throw new service.ServiceError("BANJO_ERR_INVITE_ALREADY_DECLINE_ERROR");
  }
  const diff = Util.getDateDifference(new Date(), new Date(invite.expiry));
  if (diff < 0) {
    throw new service.ServiceError("BANJO_ERR_EXPIRED_INVITE_ERROR");
  }
  return invite;
}
function sign(data, service) {
  try {
    let privateKey;
    if (data.privateKey) {
      privateKey = data.privateKey.split("\\n").join("\n");
      privateKey = crypto.createPrivateKey({
        key: Buffer.from(privateKey),
        type: "pkcs8",
        format: "pem"
      });
    }
    // Sign the data and returned signature in buffer
    delete data.privateKey;
    let d = JSON.stringify(data);
    return crypto.sign("SHA256", d, privateKey).toString("base64");
  } catch (err) {
    console.log("Error while signing", err);
    throw err;
  }
}

export default {
  verifyEmailRequest,
  verifyEmail,
  signup,
  _isOWASPFormat,
  sign
};
