import config from "@lib/config";
import db from "@lib/db";
import shared from "@lib/shared";
import sha1 from "sha1";
import crypto from "crypto";
import MemStore from "@lib/memstore";
import Signin from "./Signin";
const Op = db.Sequelize.Op;
import ipLib from "ip";
const Crypto = shared.Crypto;
// blocking time with erroneous login, sec
const BLOCK_TIMEOUT = 3600;

// number of password attempts
const MAX_USER_LOGIN_ATTEMPTS = config.max_user_login_attempts || 4;

async function generateUserToken(data, service) {
  if (await _checkLoginBlocked(data.login, data.realm_id)) {
    throw new service.ServiceError("BANJO_ERR_LOGIN_BLOCKED", {
      data: { time: parseInt(BLOCK_TIMEOUT / 60) }
    });
  }
  if (data.valid_attempt) {
    const userRec = await MemStore.get("usr" + data.user_id);
    if (!!userRec) {
      const userToken = await MemStore.get("usr" + userRec);
      await MemStore.del("usr" + data.user_id);
      if (!!userToken) {
        await MemStore.del("usr" + userRec);
      }
    }
    const token = crypto.randomBytes(42).toString("hex");

    await MemStore.set("usr" + data.user_id, token, config.user_token_lifetime);
    await MemStore.set(
      "usr" + token,
      `${data.user_id}#${data.realm_id}#${data.source}`,
      config.user_token_lifetime
    );
    return { user_token: token, user_id: data.user_id };
  }
  _registerWrongLoginAttempt(data.login, data.realm_id);
  throw new service.ServiceError("BANJO_ERR_LOGIN_ERROR");
}
async function getUserDetailByToken(data, service) {
  const key = `usr${service.header.token}`;
  const userRec = await MemStore.get(key);
  if (!userRec) return null;
  const rec = userRec.split("#");
  // if (service.realmId === rec[1]) {
  //   throw new service.ServiceError("BANJO_ERR_INVALID_REALM");
  // }
  return {
    user_id: rec[0],
    realm_id: service.realmId
  };
}
async function validateUserToken(data, service) {
  // check authorized user by token
  const key = `usr${service.header.token}`;
  const userRec = await MemStore.get(key);
  if (!userRec) return null;
  const rec = userRec.split("#");
  if (service.realmId === rec[1]) {
    await MemStore.set(key, userRec, config.user_token_lifetime);
    if (data.account_check || service.header.account) {
      if (data.account_check && !service.header.account) {
        throw new service.ServiceError("BANJO_ERR_ACCOUNT_MISSING");
      }
      let user;
      if (service.header.account) {
        user = await db.user.findOne({
          where: { id: rec[0] },
          attributes: ["id"],
          include: [
            {
              association: "accounts",
              attributes: ["id"],
              where: { realm_id: rec[1] },
              through: {
                attributes: [],
                where: { account_id: service.header.account }
              }
            }
          ]
        });
      }
      if (!user || !(user && user.accounts.length)) {
        throw new service.ServiceError("BANJO_ERR_ACCOUNT_ACCESS_ERROR");
      }
    } else if (service.header.user && isSkipMethod(service.header.method)) {
      await checkValidUser({ user_id: rec[0], realm_id: rec[1] }, service);
    }
    return {
      user_id: rec[0],
      realm_id: rec[1],
      account_id: service.header.account
    };
  }
  return null;
}
function isSkipMethod(method) {
  if (["signin", "signup", "acceptUserInvitation"].indexOf(method) >= 0) {
    return false;
  }
  return true;
}
async function checkValidUser(data, service) {
  const validUser = await db.sequelize.query(data.query, { raw: true });
  if (validUser[0] && !validUser[0].length) {
    throw new service.ServiceError("BANJO_ERR_INVALID_USER");
  }
}
async function deregisterUserToken(data, service) {
  const userRec = await MemStore.get("usr" + service.header.token);

  if (!!userRec) {
    const rec = userRec.split("#");

    await MemStore.del("usr" + rec[0]);
  }

  await MemStore.del("usr" + service.header.token);
  return { success: true };
}

async function _checkLoginBlocked(userId, realmId) {
  const key = "blk" + sha1(userId + "-" + realmId);
  let count = await MemStore.get(key);
  if (count && parseInt(count) >= MAX_USER_LOGIN_ATTEMPTS) return true;
  return false;
}
async function _registerWrongLoginAttempt(userId, realmId) {
  const key = "blk" + sha1(userId + "-" + realmId);
  let count = await MemStore.get(key);
  if (count) count = parseInt(count);
  else count = 0;
  await MemStore.set(key, count + 1, BLOCK_TIMEOUT);
}
function validateSignature(srcData, publicKey, service) {
  if (service.sign) {
    if (!service.header.sign) {
      throw new service.ServiceError("BANJO_ERR_SIGN_MISSING");
    }
    return Crypto.validateSignature(
      JSON.stringify(srcData),
      service.header.sign,
      publicKey
    );
  } else {
    return true;
  }
}
async function validateMerchantToken(data, service) {
  const APIKey = service.header.api_key;
  const APISecret = service.header.api_secret;
  const user = await db.user.findOne({
    where: {
      username: {
        [Op.iLike]: APIKey
      },
      removed: 0,
      user_type: db.user.USER_TYPE.API
    },
    attributes: ["id", "status", "password", "ips"],
    raw: true,
    type: db.sequelize.QueryTypes.SELECT
  });
  if (!user) {
    throw new service.ServiceError("BANJO_ERR_INVALID_API_KEY");
  }
  if (!_validateIPRange(user.ips, service.ip)) {
    throw new service.ServiceError("BANJO_ERR_API_IP_RESTRICTED");
  }
  if (service.sign && !validateSignature(data, user.password, service)) {
    throw new service.ServiceError("BANJO_ERR_INVALID_SIGN");
  }
  // if (!(user.password && user.password === Signin._hashPassword(APISecret))) {
  //   throw new service.ServiceError("BANJO_ERR_INVALID_API_KEY");
  // }
  let subQuery = ``;
  if (service.realmId) {
    subQuery = ` and a.realm_id = '${service.realmId}' `;
  }
  if (user && user.status != db.user.STATUS.ACTIVE) {
    console.error(
      `Auth-service. Func: validateMerchantToken. Non-Active User status found for User:`,
      user
    );
    throw new service.ServiceError("BANJO_ERR_API_STATUS_INACTIVE");
  }
  const res = await db.sequelize.query(
    `SELECT ma.id as maid, ma.account_id, a.realm_id, ma.status as ma_status,a.status as a_status
        FROM ${db.schema}.vw_resources_permissions rp
                INNER JOIN ${db.schema}.merchant_accounts ma ON ma.id = rp.object_id AND ma.removed = 0
                INNER JOIN ${db.schema}.accounts a ON a.id = ma.account_id AND a.removed = 0 ${subQuery} 
        WHERE rp.user_id = :userId
          AND rp.resource_type = 'API'
          AND rp.object_type = 'MERCHANT_ACCOUNT'`,
    {
      raw: true,
      type: db.sequelize.QueryTypes.SELECT,
      replacements: {
        userId: user.id
      }
    }
  );
  if (res && res.length) {
    if (res[0].ma_status != db.merchant_accounts.STATUS.ACTIVE) {
      throw new service.ServiceError("BANJO_ERR_MACCOUNT_INACTIVE");
    }
    if (res[0].a_status != db.account.STATUS.ACTIVE) {
      throw new service.ServiceError("BANJO_ERR_ACCOUNT_INACTIVE");
    }
    return {
      user_id: user.id,
      realm_id: res[0].realm_id,
      account_id: res[0].account_id,
      ma_id: res[0].maid,
      publicKey: service.sign ? user.password : undefined
    };
  } else {
    throw new service.ServiceError("BANJO_ERR_ACCOUNT_INACTIVE");
  }
}
function _validateIPRange(ips, requestIP) {
  if (ips && ips._arr && ips._arr.length) {
    let inRange = false;
    for (let ip of ips._arr) {
      try {
        if (ipLib.isEqual(ip, requestIP)) {
          inRange = true;
          break;
        } else if (ipLib.cidrSubnet(ip).contains(requestIP)) {
          inRange = true;
          break;
        }
      } catch (e) {}
    }
    return inRange;
  } else {
    return true;
  }
}
async function generateAdminUserToken(data, service) {
  if (await _checkLoginBlocked(data.login, data.realm_id)) {
    throw new service.ServiceError("BANJO_ERR_LOGIN_BLOCKED", {
      data: { time: parseInt(BLOCK_TIMEOUT / 60) }
    });
  }
  if (data.valid_attempt) {
    const token = crypto.randomBytes(42).toString("hex");
    await MemStore.set(
      "admin" + token,
      `${data.user_id}#${data.source}`,
      config.admin_token_lifetime
    );
    return { user_token: token, user_id: data.user_id, is_admin: true };
  }
  _registerWrongLoginAttempt(data.login, data.realm_id);
  throw new service.ServiceError("BANJO_ERR_LOGIN_ERROR");
}
async function validateAdminToken(data, service) {
  // check authorized user by token
  const key = `admin${service.header.token}`;
  const userRec = await MemStore.get(key);
  if (!userRec) return null;
  const rec = userRec.split("#");
  if (rec[0]) {
    if (service.realmId) {
      const res = await db.admin_user.findOne({
        where: {
          [Op.or]: [{ realm_id: service.realmId }, { realm_id: null }],
          _id: rec[0]
        }
      });
      if (!res) {
        throw new service.ServiceError("BANJO_ERR_INVALID_REALM");
      }
    }
    await MemStore.set(key, userRec, config.admin_token_lifetime);
    return {
      user_id: rec[0],
      is_admin: true
    };
  }
  return null;
}
async function deregisterAdminToken(data, service) {
  await MemStore.del("admin" + service.header.token);
  return { success: true };
}
export default {
  generateUserToken,
  validateUserToken,
  getUserDetailByToken,
  deregisterUserToken,
  validateMerchantToken,
  generateAdminUserToken,
  validateAdminToken,
  deregisterAdminToken
};
