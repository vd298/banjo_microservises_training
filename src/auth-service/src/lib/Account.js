import db from "@lib/db";
async function getUserAccounts(data, service) {
  let filter = {};
  if (service.realmId) {
    filter.realm_id = service.realmId;
  }
  const user = await db.user.findOne({
    where: { id: service.userId },
    attributes: ["id"],
    include: [
      {
        association: "accounts",
        where: filter,
        attributes: [
          "name",
          "id",
          "address",
          "country",
          "legal_entity",
          "registration_no",
          "status"
        ],
        through: {
          attributes: []
        }
      }
    ]
  });
  return { success: true, accounts: user.accounts };
}
async function getAccountDetails(data, service) {
  const account = await db.account.findOne({
    where: { id: service.accountId },
    attributes: [
      "name",
      "id",
      "address",
      "country",
      "legal_entity",
      "registration_no",
      "billing_currency",
      "status"
    ]
  });
  return account;
}
export default {
  getUserAccounts,
  getAccountDetails
};
