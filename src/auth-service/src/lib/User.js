import db from "@lib/db";
import Queue from "@lib/queue";
import config from "@lib/config";
const randomize = require("randomatic");
import MemStore from "@lib/memstore";
import FileProvider from "@lib/fileprovider";
const Op = db.Sequelize.Op;
import crypto from "crypto";
import Signin from "./Signin";
import Signup from "./Signup";
import { log } from "@lib/log";
import UserToken from "./UserToken";
async function getUserProfile(data, service) {
  const user = await db.user.findOne({
    where: { id: service.userId },
    raw: true,
    attributes: [
      "first_name",
      "last_name",
      "email",
      "username",
      "mobile",
      "profile_pic",
      "timezone",
      "email_verified",
      "mobile_verified",
      "telegram_id"
    ]
  });
  if (!user) throw new service.ServiceError("BANJO_ERR_USER_NOT_FOUND_ERROR");
  const accountsResp = await getMerchantAccountsByUser(data, service);
  user.accounts = [];
  if (accountsResp && Array.isArray(accountsResp.accounts)) {
    user.accounts = accountsResp.accounts;
  }
  return { success: true, user: user };
}

async function getMerchantAccountsByUser(data, service) {
  try {
    const userAccounts = await db.user_accounts.findAll({
      where: {
        user_id: service.userId,
        removed: 0,
        end_date: { [Op.eq]: null }
      },
      attributes: ["account_id"]
    });
    if (userAccounts && userAccounts.length) {
      let filter = {};
      if (service.realmId) {
        filter.realm_id = service.realmId;
      }
      let i = 0;
      let accountIds = [];
      while (userAccounts.length > i) {
        userAccounts[i].account_id &&
          accountIds.push(userAccounts[i].account_id);
        i++;
      }
      const accounts = await db.account.findAll({
        raw: true,
        where: {
          ...filter,
          id: {
            [Op.in]: accountIds
          },
          removed: 0
        },
        attributes: [
          "id",
          "name",
          "address",
          "city",
          "country",
          "legal_entity",
          "registration_no",
          "plan_id",
          "type",
          "status",
          "realm_id",
          "variables"
        ]
      });
      return { success: true, accounts: accounts };
    }
    return { success: true, accounts: [] };
  } catch (error) {
    console.error(
      "User.js auth-service getMerchantAccountsByUser error: " + error
    );
    throw error;
  }
}
async function updateUserProfile(data, service) {
  const userData = await db.user.findOne({
    attributes: ["id", "email", "mobile", "username"],
    where: {
      id: service.userId
    }
  });
  if (service.realmId) {
    await checkValidUser(
      { realm_id: service.realmId, user_id: userData.id },
      service
    );
  }
  let updateData = {
    first_name: data.first_name,
    last_name: data.last_name,
    email: data.email,
    mobile: data.mobile,
    username: data.username,
    status: data.status,
    timezone: data.timezone
  };
  if (data.profile_pic) {
    updateData.profile_pic = data.profile_pic;
  }
  if (!userData || userData.id === service.userId) {
    await db.user.update(updateData, { where: { id: service.userId } });
    return { success: true };
  } else {
    throw new service.ServiceError("BANJO_ERR_UPDATE_PROFILE");
  }
}
async function changePassword(data, service) {
  try {
    if (service.realmId) {
      await checkValidUser(
        { realm_id: service.realmId, user_id: service.userId },
        service
      );
    }
    const user = await db.user.findOne({
      where: { id: service.userId, removed: 0 },
      attributes: ["id", "password", "username"]
    });
    if (user) {
      if (
        user.username == "string" &&
        typeof data.new_password == "string" &&
        user.username.toLowerCase() === data.new_password.toLowerCase()
      ) {
        throw new service.ServiceError(
          "BANJO_ERR_USER_NAME_AND_PASSWORD_SHOULD_BE_DIFFERENT_ERROR"
        );
      }
      await Signup._isOWASPFormat(
        { ...data, password: data.new_password },
        service
      );
      if (user.password === null && data.new_password) {
        await updatePassword(data, service.userId);
        return { success: true };
      } else if (user.password === Signin._hashPassword(data.old_password)) {
        await updatePassword(data, service.userId);
        return { success: true };
      } else {
        throw new service.ServiceError("BANJO_ERR_PASS_MATCH");
      }
    } else {
      throw new service.ServiceError("BANJO_ERR_USER_NOT_FOUND");
    }
  } catch (error) {
    throw error;
  }
}
async function updatePassword(data, userId) {
  try {
    return await db.user.update(
      {
        password: Signin._hashPassword(data.new_password)
      },
      {
        where: { id: userId }
      }
    );
  } catch (error) {
    console.error("updatePassword:" + error);
    throw error;
  }
}

async function resetPasswordRequest(data, service) {
  let restoreCode;
  const res = await db.user.findOne({
    where: { email: data.email }
  });
  if (!res) {
    throw new service.ServiceError("BANJO_ERR_USER_NOT_FOUND_ERROR");
  }
  if (service.realmId) {
    await checkValidUser(
      { realm_id: service.realmId, user_id: res.id },
      service
    );
  }
  let previousAttempt = await MemStore.get(`fp${res.id}`);
  if (previousAttempt) {
    throw new service.ServiceError("BANJO_ERR_FORGOT_PASSWORD_ALREADY_EXISTS");
  }
  config.isTestEnv()
    ? (restoreCode = "q2jcBleNEr")
    : (restoreCode = randomize("Aa0", 10));
  const out = {
    restoreCode
  };
  if (res) {
    out.code = "1211-PASSW0RD-RESTORE";
    out.to = data.email;
    out.first_name = res && res.first_name ? res.first_name : "";
    out.last_name = res && res.last_name ? res.last_name : "";
    out.base_url = config.app_url;
    await MemStore.set(`fp${res.id}`, restoreCode, 120);

    Queue.publish("NOTIFY.1211-PASSW0RD-RESTORE", {
      user_id: res.id, // Optional
      payload: out, // Optional
      options: {
        send_sms: false,
        send_push: false,
        to_email: data.email
      } // Default is true for all
    });

    return { success: true };
  } else {
    throw new service.ServiceError("BANJO_ERR_USER_NOT_FOUND");
  }
}

async function resetPassword(data, service) {
  const res = await db.user.findOne({
    where: { email: data.email }
  });
  if (service.realmId) {
    await checkValidUser(
      { realm_id: service.realmId, user_id: res.id },
      service
    );
  }
  // validate code
  let code = await MemStore.get(`fp${res.id}`);
  if (data.step === 1) {
    if (code) {
      return { success: true };
    } else {
      throw new service.ServiceError("BANJO_ERR_INVALID_CODE");
    }
  } else {
    if (!data.new_password) {
      throw new service.ServiceError("BANJO_ERR_PASSWORD_REQUIRED");
    }
    await Signup._isOWASPFormat(
      { ...data, password: data.new_password },
      service
    );
    if (
      res.username == "string" &&
      typeof data.new_password == "string" &&
      res.username.toLowerCase() === data.new_password.toLowerCase()
    ) {
      throw new service.ServiceError(
        "BANJO_ERR_USER_NAME_AND_PASSWORD_SHOULD_BE_DIFFERENT_ERROR"
      );
    }
    if (code === data.code) {
      await db.user.update(
        {
          password: Signin._hashPassword(data.new_password)
        },
        {
          where: { id: res.id }
        }
      );
      await MemStore.del(`fp${res.id}`);
      return { success: true };
    } else {
      throw new service.ServiceError("BANJO_ERR_LINK_ACTIVATED");
    }
  }
}

async function updateProfilePic(data, service) {
  let file = {};
  file.data = `${data.profile_pic}`;
  file.name = Date.now() + "-" + data.file_name || Date.now() + "File.png";
  file.owner = service.userId;
  try {
    let fileProvider = await FileProvider.push(file, 300);
    let profilePicUrl = await FileProvider.pull(fileProvider);
    return {
      success: true,
      profilePicUrl,
      profile_pic_code: fileProvider.code
    };
  } catch (error) {
    throw new service.ServiceError("BANJO_ERR_UPLOAD_PROFILE_PIC_FAILED");
  }
}

async function getProfilePic(data, service) {
  let file = await FileProvider.pullByOwner(data);
  return { file };
}

async function checkUserPermissions(data, service) {
  return true;
  const userData = await UserToken.validateUserToken(data, data.options);
  if (!userData) {
    console.log(
      "File: User.js, Func: checkUserPermissions, Err: User token invalid, data: ",
      data
    );
    throw new service.ServiceError("BANJO_ERR_ACCESSTOKEN");
  }

  try {
    const user_account = await db.user_accounts.findOne({
      where: {
        user_id: userData.user_id,
        account_id: userData.account_id
      },
      attributes: ["main_role"]
    });
    const permissions = await db.vw_resources_permissions.findOne({
      where: {
        identifier: data.identifier,
        user_id: userData.user_id,
        role_id: user_account.dataValues.main_role
      },
      attributes: ["allow", "deny", "write"]
    });
    if (permissions) {
      if (permissions.allow) {
        return true;
      } else {
        throw "POLY_ERR_ACCESS_RIGHT";
      }
    } else {
      throw "BANJO_ERR_ACCESS_RIGHT";
    }
  } catch (error) {
    console.log("File: User.js, Func: checkUserPermissions, Err:", error);
    throw error;
  }
}
async function fetchAssignableRoles(data, service) {
  let filter = {},
    weight;
  if (service.realmId) {
    filter.realm_id = service.realmId;
  }
  if (data.role_id) {
    const userRole = await db.role.findOne({
      where: { ...filter, id: data.role_id },
      attributes: ["weight"]
    });
    if (!(userRole && userRole.dataValues)) {
      return { success: true, roles: [] };
    } else {
      weight = userRole.dataValues.weight;
    }
  } else if (data.weight) {
    weight = data.weight;
  }
  if (isNaN(weight)) {
    return { success: true, roles: [] };
  }
  try {
    const roles = await db.role.findAll({
      where: { ...filter, weight: { [Op.lt]: weight } },
      attributes: ["id", "role_name", "weight"]
    });
    return { success: true, roles };
  } catch (error) {
    console.log(
      "File: auth-service/src/lib/User.js Func: fetchAssignableRoles, role: ",
      data.role_id,
      "weight: ",
      data.weight,
      " err: ",
      error
    );
    throw error;
  }
}
async function cancelInvitation(data, service) {
  if (service.realmId) {
    const realm = await db.realm.findOne({
      where: { id: service.realmId },
      attributes: ["id", "status"]
    });
    if (realm.status !== "ACTIVE") {
      throw new service.ServiceError("BANJO_ERR_REALM_IS_INACTIVE");
    }
  }
  const invite_user = await db.user_account_invitations.findOne({
    where: { id: data.id },
    attributes: ["email", "token", "id", "account_id"]
  });
  const account_name = await db.account.findOne({
    where: { id: invite_user.dataValues.account_id },
    attributes: ["name"]
  });
  if (!invite_user) {
    throw new service.ServiceError("BANJO_ERR_INVITE_NOT_FOUND");
  }
  if (!invite_user.dataValues.token) {
    throw new service.ServiceError("BANJO_ERR_INVITE_TOKEN_NOT_FOUND");
  }
  invite_user.token = null;
  invite_user.save();

  Queue.publish(`NOTIFY.INVITE-CANCEL`, {
    user_id: null,
    options: {
      send_sms: false,
      send_push: false,
      to_email: invite_user.dataValues.email
    },
    payload: {
      email: invite_user.dataValues.email,
      account_name: account_name.dataValues.name
    }
  });
  return { success: true, message: "Invitation cancelled" };
}
async function updateUser(data, service) {
  try {
    if (service.realmId) {
      await checkValidUser({ realm_id: service.realmId, user_id: data.id });
    }
    const checkUserExist = await db.user.findOne({
      where: { id: data.id, removed: 0 },
      raw: true
    });
    if (!checkUserExist) {
      throw new service.ServiceError("BANJO_ERR_USER_NOT_FOUND_ERROR");
    }
    const updateUser = await db.user.update(
      { ...data },
      { where: { id: data.id } }
    );
    return {
      success: true,
      message: "user updated successfully"
    };
  } catch (error) {
    console.error(
      "Account-service: updateUser, File: Users.js, Create Error:",
      typeof error == "object" ? JSON.stringify(error) : error
    );

    throw error;
  }
}

async function getAdminProfile(data, service) {
  const filter = {};
  if (service.realmId) {
    filter[Op.or] = [{ realm_id: service.realmId }, { realm_id: null }];
  } else {
    filter.realm_id = null;
  }
  const user = await db.admin_user.findOne({
    where: { ...filter, _id: service.userId },
    raw: true,
    attributes: ["name", "email", "tel", "status", "login", "superuser"]
  });
  if (!user) throw new service.ServiceError("BANJO_ERR_USER_NOT_FOUND_ERROR");
  return { success: true, user: user };
}
async function checkValidUser(data, service) {
  try {
    let query = `select u.id,r.status from ${db.schema}.users u 
      join ${db.schema}.user_accounts ua on u.id = ua.user_id and ua.removed=0 
      join ${db.schema}.accounts a on ua.account_id = a.id and a.removed=0 and a.realm_id = '${data.realm_id}'
      join ${db.schema}.realms r on a.realm_id = r.id and r.removed=0  
      where u.id = '${data.user_id}' `;
    await executeRealmValidationQuery({ query }, service);
  } catch (error) {
    throw error;
  }
}
async function executeRealmValidationQuery(data, service) {
  try {
    const validUser = await db.sequelize.query(data.query, { raw: true });
    if (validUser[0] && !validUser[0].length) {
      throw new service.ServiceError("BANJO_ERR_INVALID_USER");
    } else if (validUser[0] && validUser[0][0].status !== "ACTIVE") {
      throw new service.ServiceError("BANJO_ERR_REALM_IS_INACTIVE");
    }
  } catch (error) {
    throw error;
  }
}
async function checkValidInviteUser(data, service) {
  try {
    let query = `select u.id,r.status from ${db.schema}.users u 
      join ${db.schema}.user_account_invitations ua on u.id = ua.user_id and ua.removed=0 
      join ${db.schema}.accounts a on ua.account_id = a.id and a.removed=0 and a.realm_id = '${data.realm_id}'
      join ${db.schema}.realms r on a.realm_id = r.id and r.removed=0  
      where u.id = '${data.user_id}' `;
    await executeRealmValidationQuery({ query }, service);
  } catch (error) {
    throw error;
  }
}
export default {
  changePassword,
  resetPasswordRequest,
  updateUserProfile,
  resetPassword,
  getUserProfile,
  updateProfilePic,
  getProfilePic,
  getMerchantAccountsByUser,
  checkUserPermissions,
  fetchAssignableRoles,
  cancelInvitation,
  updateUser,
  getAdminProfile,
  checkValidUser,
  checkValidInviteUser
};
