import db from "@lib/db";
import { AUTH_PERMISSIONS } from "../../../../packages/permissions";
const Op = db.Sequelize.Op;

async function syncPermissions() {
  for (const permissions in AUTH_PERMISSIONS) {
    for (const permission in AUTH_PERMISSIONS[permissions]) {
      let identifier = `${permissions}.${Object.keys(
        AUTH_PERMISSIONS[permissions]
      )}`;

      let allowedRoles = await db.role.findAll({
        where: {
          role_name: {
            [Op.in]: AUTH_PERMISSIONS[permissions][permission].allowRoles
          }
        },
        attributes: ["id"]
      });
      let allowedRolesArray = [];
      for (let i = 0; i < allowedRoles.length; i++) {
        allowedRolesArray.push(allowedRoles[i].id);
      }

      let deniedRoles = await db.role.findAll({
        where: {
          role_name: {
            [Op.in]: AUTH_PERMISSIONS[permissions][permission].denyRoles
          }
        },
        attributes: ["id"]
      });

      let deniedRolesArray = [];
      for (let i = 0; i < deniedRoles.length; i++) {
        deniedRolesArray.push(deniedRoles[i].id);
      }

      let allowedUsers = await db.user_accounts.findAll({
        where: { main_role: { [Op.in]: allowedRolesArray } },
        attributes: ["user_id", "main_role"]
      });

      let deniedUsers = await db.user_accounts.findAll({
        where: { main_role: { [Op.in]: deniedRolesArray } },
        attributes: ["user_id", "main_role"]
      });

      try {
        let resourceData = {
          identifier,
          alias: identifier
        };
        let resource = await db.resource.create(resourceData);
        for (let i = 0; i < allowedUsers.length; i++) {
          try {
            let permissionData = {
              user_id: allowedUsers[i].user_id,
              role_id: allowedUsers[i].main_role,
              resource_id: resource.id,
              allow: true
            };
            await db.permission.create(permissionData);
          } catch (error) {
            console.log("Func: syncPermissions: save permission error", error);
          }
        }

        for (let i = 0; i < deniedUsers.length; i++) {
          try {
            let permissionData = {
              user_id: deniedUsers[i].user_id,
              role_id: deniedUsers[i].main_role,
              resource_id: resource.id,
              deny: true
            };
            await db.permission.create(permissionData);
          } catch (error) {
            console.log("Func: syncPermissions: save permission error", error);
          }
        }
      } catch (error) {
        throw error;
      }
    }
  }
}

export default { syncPermissions };
