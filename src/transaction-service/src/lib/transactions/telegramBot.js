import axios from "axios";
import db from "@lib/db";
import activity from "./activity";
import config from "@lib/config";

async function callTelegramBot(data, service) {
  try {
    const t = await activity.getTxDetails(data, service);
    sendMessage(t, data, data.status, data.activity);
  } catch (error) {
    console.log("File:telegramBot.js Fun:callTelegramBot Error:" + error);
  }
}
async function getAdminDetailsById(_id) {
  try {
    const admin_user = await db.admin_user.findOne({
      where: { _id },
      raw: true,
      attributes: ["name", "login"]
    });
    if (admin_user) {
      return admin_user.name || admin_user.login || "";
    } else {
      return "";
    }
  } catch (error) {
    console.log("File:telegramBot.js Fun:getAdminDetailsById Error:" + error);
  }
}
async function sendMessage(t, data, type, activity) {
  try {
    let transfer_type =
      t.transfer_type?.charAt(0).toUpperCase() +
      t.transfer_type.slice(1).toLowerCase();
    let admin_name = "";
    if (data && data.action_type === "ADMIN" && data.admin_id) {
      admin_name = await getAdminDetailsById(data.admin_id);
    }
    let action_type =
      (data.action_type && data.action_type?.toLowerCase()) || "";

    let message = `${transfer_type} of ${t.src_amount} ${t.src_currency} with Reference number: '${t.ref_num}' has been ${type} as ${activity}`;
    if (action_type) {
      message += ` by ${action_type}`;
    }
    if (admin_name) {
      message += ` '${admin_name}'`;
    }
    message += ` under the '${t.merchant_account_name}' merchant account for '${t.merchant_name}' merchant.`;
    await sendTelegramBotApi(message);
  } catch (error) {
    console.log("File:telegramBot.js Fun:sendMessage Error:" + error);
  }
}

async function sendTelegramBotApi(message) {
  try {
    const botToken = process.env.TELEGRAM_TOKEN;
    const chatId = config.telegramBotId;

    const apiUrl = `https://api.telegram.org/bot${botToken}/sendMessage`;

    const params = {
      chat_id: chatId,
      text: message
    };
    axios
      .post(apiUrl, params)
      .then((response) => {
        console.log(
          "File:telegramBot.js Fun:sendTelegramBotApi Message sent:",
          response.data
        );
      })
      .catch((error) => {
        console.error(
          "File:telegramBot.js Fun:sendTelegramBotApi Error: sending message:",
          error
        );
      });
  } catch (error) {
    console.log("File:telegramBot.js Fun:sendTelegramBotApi Error:" + error);
  }
}

export default {
  sendTelegramBotApi,
  sendMessage,
  callTelegramBot
};
