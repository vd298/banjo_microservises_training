import db from "@lib/db";
import Queue from "@lib/queue";
async function getCreateLedgerAccount(bankAccId, currency) {
  const acc = await db.wallet.findOne({
    where: {
      bank_account_id: bankAccId,
      currency,
      type: db.wallet.TYPES.LEDGER
    },
    attributes: ["id", "currency"]
  });
  if (acc) {
    return acc;
  } else {
    const acc = await createLedgerAccount(bankAccId, currency);
    return {
      id: acc.id,
      currency: acc.currency
    };
  }
}
async function getCreateWalletAccount(bankAccId, currency, merchant_account) {
  let query = `SELECT w.id , w.currency , w.merchant_account_mapping_id  FROM  ${db.schema}.wallets w 
            join ${db.schema}.vw_merchant_account_mappings mam on mam.id = w.merchant_account_mapping_id and mam.merchant_account_id = :merchant_account
            where w.removed = 0 and w.type = :type and w.currency= :currency and w.bank_account_id = :bank_account_id order by w.ctime`;
  let replacements = {
    currency,
    bank_account_id: bankAccId,
    merchant_account,
    type: db.wallet.TYPES.WALLET
  };
  const acc = await db.sequelize.query(query, {
    replacements,
    raw: true
  });
  if (acc && acc.length && acc[0].length && acc[0][0].id) {
    return acc[0][0];
  } else {
    const now = new Date();
    const maMapping = await db.merchant_account_mappings.create(
      {
        merchant_account_id: merchant_account,
        bank_account: bankAccId,
        start_time: now,
        end_time: null
      },
      { raw: true }
    );
    const res = await Queue.newJob("account-service", {
      method: "createWallet",
      data: {
        currency,
        acc_label: "Bank Exchange account",
        type: db.wallet.TYPES.WALLET,
        bank_account_id: bankAccId,
        merchant_account_mapping_id: maMapping.id
      }
    });
    return res;
  }
}
async function getCreateLedgerFeeAccount(bankAccId, currency) {
  const acc = await db.wallet.findOne({
    where: {
      bank_account_id: bankAccId,
      currency,
      type: db.wallet.TYPES.FEE
    },
    attributes: ["id", "currency"]
  });
  if (acc) {
    return acc;
  } else {
    const acc = await createLedgerFeeAccount(bankAccId, currency);
    return {
      id: acc.id,
      currency: acc.currency
    };
  }
}

async function createLedgerAccount(bankAccId, currency) {
  const res = await Queue.newJob("account-service", {
    method: "createWallet",
    data: {
      negative: true,
      currency,
      acc_label: "Bank Ledger",
      type: db.wallet.TYPES.LEDGER,
      bank_account_id: bankAccId
    }
  });
  if (res.result) {
    return res.result;
  } else {
    throw res.error;
  }
}
async function createLedgerFeeAccount(bankAccId, currency) {
  const res = await Queue.newJob("account-service", {
    method: "createWallet",
    data: {
      currency,
      acc_label: "Bank Ledger",
      type: db.wallet.TYPES.FEE,
      bank_account_id: bankAccId
    }
  });
  if (res.result) {
    return res.result;
  } else {
    throw res.error;
  }
}
async function getDstCreateLedgerAccount(data, service) {
  try {
    if (!data.bank_id) {
      throw new service.ServiceError("BANJO_ERR_EXCHANGE_ACCOUNT_NOT_FOUND");
    }
    const srcLedgerAccount = await db.bank_accounts.findOne({
      where: {
        bank_id: data.bank_id,
        currency: data.currency
      },
      attributes: ["id"]
    });
    if (!srcLedgerAccount) {
      throw new service.ServiceError(
        "BANJO_ERR_EXCHANGE_SRC_ACCOUNT_NOT_FOUND"
      );
    }
    return getCreateLedgerAccount(srcLedgerAccount.id, data.currency);
  } catch (error) {}
}
async function getBankAccount(data, service) {
  try {
    if (!data.wallet_id) {
      throw new service.ServiceError("BANJO_ERR_EXCHANGE_ACCOUNT_NOT_FOUND");
    }
    const acc = await db.wallet.findOne({
      where: {
        id: data.wallet_id
      },
      attributes: ["bank_account_id"]
    });
    if (!acc) {
      throw new service.ServiceError(
        "BANJO_ERR_EXCHANGE_DST_ACCOUNT_NOT_FOUND"
      );
    }

    return acc;
  } catch (error) {
    throw error;
  }
}
export default {
  getCreateLedgerAccount,
  getCreateLedgerFeeAccount,
  getCreateWalletAccount,
  getDstCreateLedgerAccount,
  getBankAccount
};
