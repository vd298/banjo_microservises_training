import db from "@lib/db";
import config from "@lib/config";
import shared from "@lib/shared";
import axios from "axios";
const Crypto = shared.Crypto;
async function _getTransactionDetails(transferId) {
  let t = await db.sequelize.query(
    `SELECT u.id           as user_id,
          u.password as public_key,
          u.webhook_url,
          tx.id          as transaction_id,
          tx.activity as status,
          t.dst_amount   as amount,
          t.dst_currency as currency,
          t.transfer_type,
          t.order_num,
          t.description,
          tx.transfer_id
    FROM ${db.schema}.transactions tx
            INNER JOIN ${db.schema}.transfers t on tx.transfer_id = t.id
            INNER JOIN ${db.schema}.users u ON u.id = t.user_id AND u.removed = 0 AND u.status = 'ACTIVE' AND u.user_type = 'API'
    WHERE tx.id = (SELECT id from ${db.schema}.transactions WHERE transfer_id=:transferId AND activity not in ('FEE','REVERSE_FEE') ORDER BY ctime DESC LIMIT 1)`,
    {
      raw: true,
      type: db.sequelize.QueryTypes.SELECT,
      replacements: {
        transferId
      }
    }
  );
  return t[0];
}
async function _send(webhookUrl, type, data, publicKey) {
  data = {
    time: new Date(),
    type,
    payload: data
  };
  const dataBody = {
    header: {
      sign: Crypto.createSignatureByPublicKey(JSON.stringify(data), publicKey)
    },
    data
  };
  if (config.transactionWebhooks.debug) {
    console.info(
      `Sending webhooks (${JSON.stringify(dataBody)}) to ${webhookUrl}`
    );
  }
  try {
    const res = await axios({
      method: "post",
      url: webhookUrl,
      data: dataBody,
      timeout: 1000 * config.transactionWebhooks.timeout
    });
    if (res.statusCode == 200) {
      if (config.transactionWebhooks.debug) {
        console.info(
          `Webhook successfully posted (${JSON.stringify(
            dataBody
          )}) to ${webhookUrl}`
        );
      }
      return {
        message: `Webhook successfully posted to ${webhookUrl}`,
        time: data.time,
        status_code: res.statusCode,
        status: db.tx_webhooks.STATUS.COMPLETED
      };
    } else {
      if (config.transactionWebhooks.debug) {
        console.info(
          `NON 200 status code received from webhook URL ${webhookUrl} (${JSON.stringify(
            dataBody
          )})`
        );
      }
      return {
        time: data.time,
        message: "NON 200 status code received from webhook URL",
        status_code: res.statusCode,
        status: db.tx_webhooks.STATUS.FAILED
      };
    }
  } catch (err) {
    if (config.transactionWebhooks.debug) {
      console.error(
        "Error while posting webhook message to",
        webhookUrl,
        err.message ? err.message : err
      );
    }
    return {
      time: data.time,
      message: err.message ? err.message : err,
      status_code: err.response ? err.response.status : 0,
      status: db.tx_webhooks.STATUS.FAILED
    };
  }
}
async function sendWebhook(data, service) {
  const tx = await _getTransactionDetails(data.transfer_id);
  if (!tx) {
    throw new service.ServiceError("BANJO_ERR_TX_NOT_EXIST");
  }
  if (!db.transfer.WEBHOOK_UPDATES.includes(tx.status)) {
    if (config.transactionWebhooks.debug) {
      console.info(
        `Ignoring transaction status webhook for ${data.transfer_id} status is ${tx.status}`
      );
    }
    return;
  }
  const t = await db.sequelize.transaction();
  try {
    let webhookRecord = await db.tx_webhooks.findOne({
      where: {
        user_id: tx.user_id,
        transaction_id: tx.transaction_id
      },
      lock: true,
      transaction: t
    });
    if (webhookRecord) {
      webhookRecord.counter = webhookRecord.counter + 1;
      if (!webhookRecord.logs._arr) {
        webhookRecord.logs._arr = [];
      }
    } else {
      webhookRecord = new db.tx_webhooks({
        user_id: tx.user_id,
        transaction_id: tx.transaction_id,
        logs: { _arr: [] },
        counter: 0,
        status: db.tx_webhooks.CREATED
      });
    }
    const res = await _send(
      tx.webhook_url,
      data.type,
      {
        transfer_type: t.transfer_type,
        transaction_id: tx.transfer_id,
        order_id: tx.order_num,
        description: tx.description,
        status: tx.status,
        amount: tx.amount,
        currency: tx.currency
        /*
      tx.id          as transaction_id,
          tx.activity as status,
          t.dst_amount   as amount,
          t.dst_currency as currency,
          t.transfer_type,
          tx.transfer_id
      */
      },
      tx.public_key
    );
    webhookRecord.status = res.status;
    webhookRecord.logs._arr.push(res);
    await webhookRecord.save({ transaction: t });
    await t.commit();
  } catch (err) {
    console.log("Error while sending webhook", err);
    t.rollback();
  }
}
export default {
  sendWebhook
};
