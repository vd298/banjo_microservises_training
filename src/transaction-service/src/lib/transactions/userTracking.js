import db from "@lib/db";
import Queue from "@lib/queue";
import axios from "axios";
import crypto, { SHA256 } from "crypto-js";

function createSignature(sigString) {
  const signatureSigned = crypto.SHA256(sigString);
  return crypto.enc.Base64.stringify(signatureSigned);
}

async function request({
  url,
  method = "GET",
  qs = {},
  data,
  headers = {},
  cert = null
}) {
  const requestData = await formatRequestBody({
    url,
    method,
    data,
    qs,
    headers,
    cert
  });
  console.log(
    "File: userTracking.js, Func: request, requestData: ",
    JSON.stringify(requestData)
  );
  try {
    const result = await axios({
      method: requestData.method,
      url: requestData.url,
      params: requestData.qs,
      data: requestData.data,
      headers: requestData.headers,
      httpsAgent: requestData.httpsAgent
    });
    console.log(
      "File: userTracking.js, Func: request, result from axios: ",
      JSON.stringify(result.data)
    );

    return await formatResponseBody(result);
  } catch (err) {
    console.error("UIS API Error:", err.response.data);
    throw err.response.data;
  }
}
async function formatRequestBody({ url, headers, qs, data, method }) {
  return {
    method,
    url: process.env.VPA_URL + url,
    headers,
    qs,
    data
  };
}
async function formatResponseBody(result) {
  if (result.status >= 200 && result.status <= 210) {
    console.log(
      "File: userTracking.js, Func: formatResponseBody, result ok: ",
      result.data
    );
    return result.data;
  } else {
    console.log(
      "File: userTracking.js, Func: formatResponseBody, result failed: ",
      result.data
    );
    throw result.data;
  }
}

async function saveVPAv2(data, service) {
  const url = "api/v2/vpa/save";
  const headers = {
    AUTHORIZATION: `Basic ${process.env.USER_TRACK_AUTH}`
  };
  const requestData = {
    merchantId: data.merchantId,
    merchantAccountId: data.merchantAccountId,
    merchantUserId: data.merchantAccountId,
    paymentData: {
      orangeId: data.paymentData.orangeId,
      merchantData: {
        name: data.paymentData.merchantData.name
      },
      ppageData: {
        vpa: data.paymentData.ppageData.vpa
      },
      bankData: {
        name: data.paymentData.bankData.name,
        vpa: data.paymentData.bankData.vpa
      }
    }
  };
  console.log(
    "File: userTracking.js, Func: saveVPAv2, requestData: ",
    requestData
  );
  const result = await request({
    url,
    headers,
    data: requestData,
    method: "POST"
  });
  if (!!result) {
    if (result && result.result.requestStatus == "COMPLETED") {
      return result;
    } else {
      console.log(
        "File: userTracking.js, Func: saveVPAv2, Got Error in Result response: ",
        result
      );
      return result;
    }
  } else {
    console.error(`UIS. Func:saveVPAv2. Invalid Response`);
    throw new service.ServiceError("BANJO_ERR_USERTRACK_INVALID_RESPONSE");
  }
}

async function getVPAv2(data, service) {
  let url = "api/v2/vpa/get?";
  const headers = {
    AUTHORIZATION: `Basic ${process.env.USER_TRACK_AUTH}`
  };
  const qs = {};
  if (data && data.orangeId) {
    qs.orangeId = data.orangeId;
  }
  if (data && data.vpa) {
    qs.vpa = data.vpa;
    url += `vpa=${data.vpa}`;
  }
  if (data && data.name) {
    qs.name = data.name;
  }
  if (data && data.email) {
    qs.email = data.email;
  }
  if (data && data.phone) {
    qs.phone = data.phone;
  }
  const result = await request({ url, headers, method: "GET" });
  if (!!result) {
    if (result && result.result.requestStatus == "COMPLETED") {
      return result;
    } else {
      console.error(`UIS. Func:getVPA. Invalid Response`, result);
      throw new service.ServiceError("BANJO_ERR_USERTRACK_GETVPA");
    }
  } else {
    console.error(`UIS. Func:getVPA. Invalid Response`);
    throw new service.ServiceError("BANJO_ERR_USERTRACK_INVALID_RESPONSE");
  }
}

export default {
  saveVPAv2,
  getVPAv2
};
