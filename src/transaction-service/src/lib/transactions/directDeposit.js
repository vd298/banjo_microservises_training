import { v4 as uuid } from "uuid";
import db from "@lib/db";
import Wallet from "./wallet";
import Webhook from "./webhook";
const randomize = require("randomatic");

async function directDepositTrigger(data, service) {
  const [ledgerAccount, feeAccount] = await Promise.all([
    Wallet.getCreateLedgerAccount(data.bank_account_id, data.dst_currency),
    Wallet.getCreateLedgerFeeAccount(data.bank_account_id, data.dst_currency)
  ]);
  service.hooks.beforeTransfer = async function(transferData, dbTransaction) {
    const refNum = await generateUniqueTransferRef(dbTransaction);
    transferData.ref_num = refNum;
    return transferData;
  };
  service.hooks.beforeSendResult = async function(result, transfer) {
    if (transfer) {
      result.transfer_id = transfer.id;
      setTimeout(async () => {
        try {
          await Webhook.sendWebhook(
            {
              transfer_id: result.transfer_id,
              type: db.tx_webhooks.TYPE.directDeposit_UPDATES
            },
            service
          );
        } catch (err) {
          console.error("Error while sending transaction webhook", err);
        }
      });
    }
    return result;
  };
  service.hooks.beforeTransaction = async function(transactionData) {
    if (
      data.brn &&
      typeof transactionData == "object" &&
      [
        db.transfer.TRANSFER_STATUSES.APPROVED,
        db.transfer.TRANSFER_STATUSES.BRN_SUBMIT,
        db.transfer.TRANSFER_STATUSES.SETTLEMENT,
        db.transfer.TRANSFER_STATUSES.INTERNAL_FUNDS_SHIFT
      ].indexOf(transactionData.data.activity) >= 0
    ) {
      transactionData.data.brn = data.brn;
      transactionData.query = transactionData.query
        .replace("ctime", "ctime,brn")
        .replace("now()", "now(),:brn");
    } else if (!transactionData) {
      console.error(
        `Transaction-service. Func:directDepositTrigger. No Transaction Data Received.`
      );
      if (data.brn)
        console.error(
          `Transaction-service. Func:directDepositTrigger. BRN:${data.brn}`
        );
    }
  };

  return {
    fee_wallet_id: feeAccount.id,
    dst_wallet_id: data.dst_wallet_id,
    src_wallet_id: data.src_wallet_id,
    dst_currency: data.dst_currency,
    dst_amount: data.dst_amount,
    src_currency: data.src_currency,
    src_amount: data.src_amount,
    settlement_id: data.settlement_id,
    brn: data.brn,
    is_exchange: data.is_exchange,
    exchange_id: data.exchange_id,
    fee: data.fee
  };
}

async function generateUniqueTransferRef(dbTransaction, length = 5) {
  let number = randomize("A0", length);
  const t = await db.transfer.findOne({
    where: { ref_num: number },
    attributes: ["id"],
    raw: true,
    type: db.sequelize.QueryTypes.SELECT,
    transaction: dbTransaction
  });
  if (t && t.id) {
    return generateUniqueTransferRef(dbTransaction, length);
  }
  return number;
}
export default {
  directDepositTrigger
};
