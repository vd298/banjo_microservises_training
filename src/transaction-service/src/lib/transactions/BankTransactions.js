import db from "@lib/db";
import Queue from "@lib/queue";
import config from "@lib/config";
import activity from "./activity";
import utils from "@lib/utils";
import UISTracking from "./userTracking";

/**
 * @method syncBankTransactions
 * @author Vaibhav Vaidya
 * @param {object} data Request body data
 * @param {object} service Holding details pertaining to request
 * @since 13 March 2023
 * @summary For Transactions Sync: Validate request data
 */
async function syncBankTransactions(data, service) {
  //Secondary validation of request structure
  await Queue.newJob("account-service", {
    method: "checkValidRealmId",
    data: {},
    options: {
      realmId: service.realmId
    }
  });
  if (data == undefined) {
    console.error(
      `SyncBankTransactions. Func: SyncBankTransactions. Received no Data.`
    );
    throw new service.ServiceError("BANJO_ERR_INVALID_REQUEST");
  } else if (data && data.acc == undefined) {
    console.error(
      `SyncBankTransactions. Func: SyncBankTransactions. Received no Account object.`
    );
    throw new service.ServiceError("BANJO_ERR_ACC");
  } else if (
    (data && data.acc && data.acc.bank_host == undefined) ||
    data.acc.name == undefined ||
    data.acc.acc_no == undefined
  ) {
    console.error(
      `SyncBankTransactions. Func: SyncBankTransactions. Received no identifying Account Details.`
    );
    throw new service.ServiceError("BANJO_ERR_INCOMPLETE_PARAMS");
  } else if ((data && data.txs == undefined) || !Array.isArray(data.txs)) {
    console.error(
      `SyncBankTransactions. Func: SyncBankTransactions. Received no Transactions.`
    );
    throw new service.ServiceError("BANJO_ERR_TXS");
  }

  //Fetching of bank account
  let dbFetch = false,
    bankAccRecords,
    bankAccSql = `select ba.id,ba.status, ba.currency, ba.acc_no, ba.account_name, ba.removed from ${db.schema}.bank_accounts ba
  where ba.removed=0 and (ba.acc_no=$1 or ba.account_name=$1)`;
  let memFetchBankAcc = await utils.fetchMemStore(`bankAcc${data.acc.acc_no}`);
  if (
    memFetchBankAcc &&
    typeof memFetchBankAcc == "object" &&
    memFetchBankAcc.acc_no &&
    memFetchBankAcc.acc_no == data.acc.acc_no
  ) {
    bankAccRecords = [memFetchBankAcc];
  } else {
    dbFetch = true;
    bankAccRecords = await db.sequelize.query(bankAccSql, {
      bind: [data.acc.acc_no],
      type: db.sequelize.QueryTypes.SELECT,
      raw: true
    });
  }
  if (bankAccRecords && bankAccRecords.length) {
    if (
      bankAccRecords[0] &&
      bankAccRecords[0].status != db.bank_accounts.STATUS.ACTIVE
    ) {
      console.error(
        `SyncBankTransactions. Func: SyncBankTransactions. Bank Account: ${data.acc.acc_no} has status:${bankAccRecords[0].status} hence skipping.`
      );
      throw new service.ServiceError(
        "BANJO_ERR_INVALID_BANK_ACCOUNT_STATUS_SHOULD_ACTIVE"
      );
    }
    data.acc = { ...bankAccRecords[0], ...data.acc };
    if (dbFetch) {
      await utils.setMemStore(`bankAcc${data.acc.acc_no}`, data.acc);
    }
    if (data && data.txs && data.txs.length) {
      let processResp = await processBankTransactions(data, service);
      if (processResp && processResp.txs) {
        let extensionResp = processResp.txs.map((ele) => ele.result || {});
        return extensionResp;
      }
    } else {
      console.error(
        `BankTransactions. Func: SyncBankTransactions. Received no Transactions.`
      );
      throw new service.ServiceError("BANJO_ERR_TXS_EMPTY");
    }
  } else {
    console.error(
      `Transaction-service. Func:syncankTransactions. No matching bank account found for acc no: ${data.acc.acc_no}`
    );
    throw new service.ServiceError("BANJO_ERR_BANK_ACC_NOT_FOUND");
  }
}

async function processBankTransactions(data, service) {
  let dbFetch = false,
    bankConfigRecords,
    bankConfigSql = `
  select bac.* from  ${db.schema}.bank_config bac
  left join ${db.schema}.banks b on (bac.bank_id =b.id and b.removed=0)
  where bac.removed=0 and (bac.domain_name=$1 or b.name=$2)`;
  let memFetchConfig = await utils.fetchMemStore(
    `bankConfig${data.acc.bank_host}`
  );
  if (
    memFetchConfig &&
    typeof memFetchConfig == "object" &&
    memFetchConfig.domain_name &&
    memFetchConfig.domain_name == data.acc.bank_host
  ) {
    bankConfigRecords = [memFetchConfig];
  } else {
    dbFetch = true;
    bankConfigRecords = await db.sequelize.query(bankConfigSql, {
      bind: [data.acc.bank_host, data.acc.name],
      type: db.sequelize.QueryTypes.SELECT,
      raw: true
    });
  }
  if (bankConfigRecords && bankConfigRecords.length) {
    let bankProcessorInfo = bankConfigRecords[0];

    if (dbFetch) {
      await utils.setMemStore(
        `bankConfig${data.acc.bank_host}`,
        bankProcessorInfo
      );
    }
    let overallFoundFlag = false;
    for (let i = 0; i < data.txs.length; i++) {
      let currentRecord = data.txs[i];
      if (
        bankProcessorInfo.brn_regex == "" ||
        bankProcessorInfo.brn_regex == undefined
      ) {
        console.error(
          `Func: processBankTransctions. Regex missing for Bank Config record id:${bankConfigRecords[0].id}`
        );
        throw new service.ServiceError("BANJO_REGEX_MISSING");
      }
      if (currentRecord && currentRecord.narration) {
        let foundBrn = currentRecord.narration.match(
          new RegExp(bankProcessorInfo.brn_regex)
        );
        if (foundBrn && foundBrn.length) {
          overallFoundFlag = true;
          await markTransaction(
            foundBrn[0],
            data.txs[i],
            currentRecord,
            null,
            data,
            service
          );
        } else {
          const vpaRegex = /([\w.-]+)@([\w.-]+)/;

          let foundVpa = currentRecord.narration.match(vpaRegex);
          if (foundVpa && foundVpa.length) {
            let txRecords = await findTransaction(
              foundVpa[0],
              data.txs[i],
              data,
              service
            );
            if (txRecords && txRecords.length) {
              await markTransaction(
                null,
                data.txs[i],
                currentRecord,
                txRecords,
                data,
                service
              );
            }
          } else {
            console.error(
              `SyncBankTransactions. Func: processBankTransactions. BRN Extraction failed for string: ${currentRecord.narration}.`
            );
            data.txs[i].result = {
              rowId: data.txs[i].row_id,
              status: 4,
              message: "BRN Extraction failed"
            };
          }
        }
      }
    }
    if (overallFoundFlag == false) {
      //If unable to extract a single BRN, send an email alert.
      Queue.publish(`NOTIFY.BOTS-BRN-ALERT`, {
        user_id: null,
        options: {
          send_sms: false,
          send_push: false
        },
        payload: {
          ...data.acc
        }
      });
    }
    return data;
  } else {
    throw new service.ServiceError("BANJO_ERR_BANK_CONFIG_MISSING");
  }
}

async function markTransaction(
  foundBRN,
  txInfo,
  currentRecord = null,
  foundTx = null,
  data,
  service
) {
  let brnExists = true;
  let message, txRecords;
  if (txInfo && isNaN(txInfo.amount)) {
    txInfo.result = {
      rowId: txInfo.row_id,
      status: 4,
      message: "Invalid amount"
    };
    return txInfo;
  }
  if (foundBRN) {
    let txFetchSql = `
  select vw.* from  ${db.schema}.vw_tx_transfer vw
  where vw.removed=0 and vw.last_brn=$1 and vw.src_currency=$2
  and (vw.src_amount>=ROUND($3,4) and vw.src_amount<=ROUND($3,4))
  and ((coalesce(vw.ctime, vw.mtime) - INTERVAL '1 day') <=$4 and (coalesce(vw.ctime, vw.mtime) + INTERVAL '1 day')>=$4)
  order by ctime desc;`;
    txRecords = await db.sequelize.query(txFetchSql, {
      bind: [foundBRN, data.acc.currency, txInfo.amount, txInfo.date],
      type: db.sequelize.QueryTypes.SELECT,
      raw: true
    });
  } else {
    if (foundTx && foundTx.length && currentRecord) {
      const vpaRegex = /([\w.-]+)@([\w.-]+)/;

      let foundVpa = currentRecord.narration.match(vpaRegex);
      if (foundVpa && foundVpa.length) {
        txRecords = await findTransaction(foundVpa[0], txInfo, data, service);
        brnExists = false;
      } else {
        console.log("The narration does not contain a VPA.");
      }
    }
  }

  if (txRecords && txRecords.length) {
    try {
      let txData = {
        transfer_id: txRecords[0].id,
        activity_action: "APPROVED",
        action_type: "BOT",
        admin_id: service.userId
      };
      if (!brnExists) {
        txData.uis_approval = true;
      }
      const txApproveResult = await activity.txActivityApprove(txData, service);
      if (
        txApproveResult &&
        txApproveResult.status == db.transfer.TRANSFER_STATUSES.APPROVED
      ) {
        txInfo.result = {
          rowId: txInfo.row_id,
          status: 1,
          message: db.transfer.TRANSFER_STATUSES.APPROVED
        };
      } else {
        txInfo.result = {
          rowId: txInfo.row_id,
          status: 5,
          message: "SYSTEM ERROR"
        };
      }
    } catch (err) {
      if (err && err.code != undefined) {
        let serviceErr = new service.ServiceError(err.code);
        switch (err.code) {
          case "BANJO_ERR_TRANSACTION_FAILED":
            {
              txInfo.result = {
                rowId: txInfo.row_id,
                status: 3,
                message: serviceErr.message
              };
            }
            break;
          default:
            {
              txInfo.result = {
                rowId: txInfo.row_id,
                status: 6,
                message: message
              };
            }
            break;
        }
      }
    } finally {
      return txInfo;
    }
  } else {
    message = `No matching TX record found for BRN: ${foundBRN}`;
    console.log(`Func:markTransaction. ${message}`);
    txInfo.result = {
      rowId: txInfo.row_id,
      status: 2,
      message: message
    };
  }
  let lost_transaction = await db.lost_transactions.findOne({
    where: { brn: foundBRN },
    attributes: ["id"]
  });
  if (!lost_transaction) {
    await db.lost_transactions.create({
      brn: foundBRN,
      amount: txInfo.amount,
      acc_no: data.acc.acc_no,
      bank_name: data.acc.name,
      domain_name: data.acc.bank_host,
      bank_account_id: data.acc.id,
      transaction_date: txInfo.date,
      transaction_data: txInfo
    });
  }
  return txInfo;
}

async function findTransaction(foundVpa, txInfo, data, service) {
  const res = await UISTracking.getVPAv2(
    {
      vpa: foundVpa
    },
    service
  );
  if (res && res.vpaList) {
    let matchingEntryCount = 0;
    let matchingEntry = null;

    for (const entry of res.vpaList) {
      if (entry.paymentData && entry.paymentData.ppageData) {
        if (entry.paymentData.ppageData.matchScore === 100) {
          matchingEntryCount++;

          matchingEntry = entry;

          if (matchingEntryCount > 1) {
            console.log(
              "File: BankTransactions.js, Func: findTransaction. Found more than one merchant for given VPA"
            );
            return;
          }
        }
      }
    }

    if (matchingEntry && matchingEntryCount === 1) {
      let txRecords = await db.sequelize.query(
        `SELECT *
        FROM ${db.schema}.vw_tx_transfer vtt
        WHERE vtt.merchant_account_id = :merchant_account_id
        and vtt.src_currency=:currency
        and (vtt.src_amount>=ROUND(:amount,4) and vtt.src_amount<=ROUND(:amount,4))
        and ((coalesce(vtt.ctime, vtt.mtime) - INTERVAL '1 day') <=:date and (coalesce(vtt.ctime, vtt.mtime) + INTERVAL '1 day')>=:date)
        and vtt.activity NOT IN ('APPROVED', 'REJECTED')
        order by ctime desc;
        `,
        {
          replacements: {
            amount: txInfo.amount,
            merchant_account_id: matchingEntry.merchantAccountId,
            currency: data.acc.currency,
            date: txInfo.date
          }
        }
      );
      if (txRecords && txRecords.length) {
        return txRecords[0];
      } else {
        return null;
      }
    } else {
      console.log(
        "There are no matching entries or more than one matching entry."
      );
    }
  }
}

export default {
  syncBankTransactions
};
