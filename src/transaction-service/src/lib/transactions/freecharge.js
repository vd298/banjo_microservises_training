import db from "@lib/db";
import Queue from "@lib/queue";
import axios from "axios";
import crypto, { SHA256 } from "crypto-js";

function createSignature(sigString) {
  const signatureSigned = crypto.SHA256(sigString);
  return crypto.enc.Base64.stringify(signatureSigned);
}

async function request({
  url,
  method = "GET",
  qs = {},
  data,
  headers = {},
  cert = null
}) {
  const requestData = await formatRequestBody({
    url,
    method,
    data,
    qs,
    headers,
    cert
  });
  console.log("File: freecharge.js, Func: request, requestData: ", requestData);
  try {
    const result = await axios({
      method: requestData.method,
      url: requestData.url,
      params: requestData.qs,
      data: requestData.data,
      headers: requestData.headers,
      httpsAgent: requestData.httpsAgent
    });
    console.log(
      "File: freecharge.js, Func: request, result from axios: ",
      result
    );

    return await formatResponseBody(result);
  } catch (err) {
    console.error("Freecharge API Error:", err.response.data);
    throw err.response.data;
  }
}
async function formatRequestBody({ url, headers, qs, data, method }) {
  return {
    method,
    url: "https://ps.certus.finance" + url,
    headers,
    qs,
    data
  };
}
async function formatResponseBody(result) {
  if (result.status >= 200 && result.status <= 210) {
    console.log(
      "File: freecharge.js, Func: formatResponseBody, result ok: ",
      result.data
    );
    return result.data;
  } else {
    console.log(
      "File: freecharge.js, Func: formatResponseBody, result failed: ",
      result.data
    );
    throw result.data;
  }
}

async function upiPayment(data, service) {
  const url = "/freecharge/api/v1/upi/payment";
  const headers = {
    "X-API-KEY": process.env.FREECHARGE_API_KEY
  };
  let keysArr = ["merchantId", "requestId", "orderId", "amount", "payerVpa"];
  let requestSignString = buildSignString(data, keysArr);
  const sign = createSignature(requestSignString);
  console.log("File: freecharge.js, Func: upiPayment, sign: ", sign);

  const requestData = {
    merchantId: data.merchantId,
    requestId: data.requestId,
    orderId: data.orderId,
    orderDesc: !!data.orderDesc ? data.orderDesc : "",
    amount: data.amount,
    payerVpa: data.payerVpa,
    sign: sign
  };
  console.log(
    "File: freecharge.js, Func: upiPayment, requestData: ",
    requestData
  );

  const result = await request({
    url,
    headers,
    data: requestData,
    method: "POST"
  });
  console.log("File: freecharge.js, Func: upiPayment, result: ", result);
  if (!!result && !!result.sign) {
    let keysArr = [
      "merchantId",
      "txId",
      "txType",
      "txMethod",
      "requestId",
      "orderId",
      "amount",
      "currency",
      "payerVpa",
      "status",
      "time"
    ];
    let respSignString = buildSignString(result, keysArr);
    const responseSign = createSignature(respSignString);
    if (responseSign == result.sign) {
      return result;
    } else {
      console.error(`freecharge. Func:upiPayment. Signature Mismatch`);
      throw new service.ServiceError("BANJO_ERR_FREECHARGE_SIGNATURE_MISMATCH");
    }
  } else {
    console.error(`freecharge. Func:upiPayment. Invalid Response`);
    throw new service.ServiceError("BANJO_ERR_FREECHARGE_INVALID_RESPONSE");
  }
}

async function upiRefund(data, service) {
  const url = "/freecharge/api/v1/upi/refund";
  const headers = {
    "X-API-KEY": process.env.FREECHARGE_API_KEY
  };
  let keysArr = ["merchantId", "requestId", "paymentTxId", "amount"];
  const sign = createSignature(buildSignString(data, keysArr));
  const requestData = {
    merchantId: data.merchantId,
    requestId: data.requestId,
    paymentTxId: data.paymentTxId,
    amount: data.amount,
    payerVpa: data.payerVpa,
    sign: sign
  };
  const result = await request({
    url,
    headers,
    data: requestData,
    method: "POST"
  });
  if (!!result && !!result.sign) {
    let keysArr = [
      "merchantId",
      "txId",
      "txType",
      "requestId",
      "amount",
      "currency",
      "status",
      "time"
    ];
    const responseSign = createSignature(buildSignString(result, keysArr));
    if (responseSign == result.sign) {
      return result;
    } else {
      console.error(`freecharge. Func:upiRefund. Signature Mismatch`);
    }
  } else {
    console.error(`freecharge. Func:upiRefund. Invalid Response`);
    throw new service.ServiceError("BANJO_ERR_FREECHARGE_INVALID_RESPONSE");
  }
}

async function getUpiTxDetails(data, service) {
  const url = "/freecharge/api/v1/upi/getTxDetails";
  const headers = {
    "X-API-KEY": process.env.FREECHARGE_API_KEY
  };
  const qs = { requestId: data.requestId, txId: data.txId };
  const result = await request({ url, qs, headers, method: "GET" });
  if (!!result && !!result.sign) {
    let keysArr = [
      "id",
      "txType",
      "txMethod",
      "requestId",
      "orderId",
      "amount",
      "currency",
      "payerVpa",
      "status",
      "time"
    ];
    let responseSign = createSignature(buildSignString(result, keysArr));
    if (responseSign == result.sign) {
      return result;
    } else {
      console.error(`freecharge. Func:getUpiTxDetails. Signature Mismatch`);
      throw new service.ServiceError("BANJO_ERR_FREECHARGE_SIGNATURE_MISMATCH");
    }
  } else {
    console.error(`freecharge. Func:getUpiTxDetails. Invalid Response`);
    throw new service.ServiceError("BANJO_ERR_FREECHARGE_INVALID_RESPONSE");
  }
}

async function getUpiMerchants(data, service) {
  const url = "/freecharge/api/v1/user/getMerchants";
  const headers = {
    "X-API-KEY": process.env.FREECHARGE_API_KEY
  };
  const result = await request({ url, headers, method: "GET" });
  return result;
}

function buildSignString(data, keysArr) {
  let fcSecretKey = process.env.FREECHARGE_SECRET_KEY;
  let signString = ``;
  if (keysArr && Array.isArray(keysArr) && keysArr.length) {
    for (let i = 0; i < keysArr.length; i++) {
      if (
        data &&
        keysArr[i] &&
        data[keysArr[i]] != undefined &&
        data[keysArr[i]] != null &&
        data[keysArr[i]] != ""
      ) {
        signString += data[keysArr[i]];
      }
    }
  }
  signString += fcSecretKey;
  return signString;
}

async function paymentStatus(data, service) {
  console.log("file: freechargejs, Func: paymentStatus, data: ", data);
  if (!!data && !!data.sign) {
    let keysArr = [
      "merchantId",
      "txId",
      "txType",
      "txMethod",
      "requestId",
      "orderId",
      "amount",
      "currency",
      "payerVpa",
      "status",
      "time"
    ];
    let responseSignString = buildSignString(data, keysArr);
    const responseSign = createSignature(responseSignString);
    console.log(
      "file: freechargejs, Func: paymentStatus, responseSign: ",
      responseSign
    );

    if (true) {
      const transaction = await db.transaction.findOne({
        where: { external_id: data.txId, external_type: data.txType },
        order: [["ctime", "DESC"]]
      });
      if (transaction && transaction.id) {
        console.log(
          "file: freechargejs, Func: paymentStatus, Found  transaction: ",
          transaction.id
        );
      } else {
        console.log(
          "file: freechargejs, Func: paymentStatus, Transaction not found for External Id: ",
          data.txId
        );
      }
      if (data.status == "COMPLETED" && transaction && transaction.id) {
        Queue.newJob("transaction-service", {
          method: "txActivityApprove",
          data: {
            transfer_id: transaction.transfer_id,
            brn: data.bankTxId,
            activity_action: "APPROVED",
            action_type: "WEBHOOK"
          },
          options: {
            ...service
          }
        });
      } else if (transaction && transaction.id) {
        Queue.newJob("transaction-service", {
          method: "txActivityReject",
          data: {
            transfer_id: transaction.transfer_id,
            activity_action: "REJECTED",
            action_type: "WEBHOOK"
          },
          options: {
            ...service
          }
        });
      }
      console.error(`freecharge. Func:paymentStatus. Returning Success`);
      return "Success";
    } else {
      console.error(`freecharge. Func:paymentStatus. Signature Mismatch`);
      throw new service.ServiceError("BANJO_ERR_FREECHARGE_SIGNATURE_MISMATCH");
    }
  } else {
    console.error(`freecharge. Func:paymentStatus. Signature Missing`);
    throw new service.ServiceError("BANJO_ERR_FREECHARGE_SIGNATURE_MISSING");
  }
}

export default {
  upiPayment,
  upiRefund,
  getUpiTxDetails,
  getUpiMerchants,
  paymentStatus
};
