import { v4 as uuid } from "uuid";
import db from "@lib/db";
async function transfer(data, service) {
  if (!data.list || !data.list.length) return {};
  let out = { list: [] };

  if (!data.data.transfer_id && data.list.length) {
    let transfer_id = data.data.payout_request_transfer_id || uuid();
    data.data.transfer_id = transfer_id;
    out.transfer = {
      action: "create",
      data: {
        user_id: service.userId,
        id: transfer_id,
        description: data.data.description,
        transfer_type: data.list[0].txtype,
        order_num: data.data.order_id,
        ref_num: data.data.ref_num,
        src_amount: data.result.src_amount,
        src_currency: data.result.src_currency,
        dst_amount: data.result.dst_amount,
        dst_currency: data.result.dst_currency,
        src_wallet_id: data.result.src_wallet_id,
        dst_wallet_id: data.result.dst_wallet_id,
        counterpart_id: data.result.counterpart_id,
        is_exchange: data.result.is_exchange,
        exchange_id: data.result.exchange_id,
        data: data.data.data,
        account_id: service.accountId,
        merchant_account_id: service.maId,
        event_name: `${data.service}:${data.method}`,
        settlement_id: data.result.settlement_id
      }
    };
  }
  let accounts = [];

  for (let transaction of data.list) {
    if (transaction.rate_of_fee) {
      data.data.data.rate_of_fee = transaction.rate_of_fee;
      data.data.data.feetype = transaction.feetype;
    }

    await addQueriesByAbstractTransfer(
      transaction,
      data,
      accounts,
      out.list,
      service,
      data.data.transfer_id,
      {}
    );
  }

  // the first element is a query for blocking accounts
  if (accounts.length) out.list.splice(0, 0, getAccountBlockQuery(accounts));

  return out;
}
function getAccountBlockQuery(accounts) {
  return {
    query: `SELECT id, balance FROM ${db.schema}.wallets WHERE id IN (:accounts) FOR NO KEY UPDATE`,
    type: "SELECT",
    data: {
      accounts
    }
  };
}
async function addQueriesByAbstractTransfer(
  transaction,
  data,
  accounts,
  queries,
  service,
  transfer_id,
  accountsCurrency
) {
  const $Op = db.Sequelize.Op;
  let acc_src = transaction.acc_src;
  let acc_dst = transaction.acc_dst;

  if (transaction.hold) {
    acc_dst = await account.getTransitAccount(acc_dst); // getting depended account for holded payment
  }

  if (!accounts.includes(acc_src)) accounts.push(acc_src);
  if (!accounts.includes(acc_dst)) accounts.push(acc_dst);

  if (transaction.amount) {
    // Credit
    queries.push({
      model: "wallet",
      action: "decrement",
      data: {
        balance: transaction.amount || 0
      },
      where: { id: acc_src }
    });
    // Debit
    queries.push({
      model: "wallet",
      action: "increment",
      data: {
        balance: transaction.amount
      },
      where: { id: acc_dst }
    });
  }
  queries.push(
    await buildInsertTxQuery(
      {
        // realm_id,
        // user_id,
        transfer_id,
        tariff_id: transaction.tariff_id,
        plan_id: transaction.tariff_plan_id,
        activity: transaction.txsubtype,
        txtype: transaction.txtype,
        amount: transaction.amount || 0,
        currency: accountsCurrency[transaction.acc_dst] || null,
        src_acc_id: acc_src || null,
        dst_acc_id: acc_dst || null,
        internal_note: transaction.description || null,
        data:
          data.data && data.data.data
            ? JSON.stringify(data.data.data || {})
            : "{}"
      },
      data
    )
  );
}
async function buildInsertTxQuery(data, originalData) {
  data.id = uuid();
  let cols = `
      id,
      transfer_id,
      tariff_id,
      plan_id,
      activity,
      amount,
      currency,
      src_acc_id,
      dst_acc_id,
      src_balance,
      dst_balance,
      internal_note,
      txtype,
      data,
      ctime
`;
  let vals = `
      :id,
      :transfer_id,
      :tariff_id,
      :plan_id,
      :activity,
      :amount,
      :currency,
      :src_acc_id,
      :dst_acc_id,
      (SELECT balance from ${db.schema}.wallets WHERE id=:src_acc_id),
      (SELECT balance from ${db.schema}.wallets WHERE id=:dst_acc_id),
      :internal_note,
      :txtype,
      :data,
      now()`;
  return {
    query: `INSERT INTO ${db.schema}.transactions (${cols}) VALUES(${vals})`,
    type: "INSERT",
    data
  };
}
export default {
  transfer
};
