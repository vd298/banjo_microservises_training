import db from "@lib/db";
import Queue from "@lib/queue";
import Wallet from "./wallet";
import UISTracking from "./userTracking";
import telegramBot from "./telegramBot";

async function _getTransferDetails(transferId, service) {
  console.log("_getTransferDetails" + transferId);
  let t = await db.sequelize.query(
    `SELECT a.name  as merchant_name,
                        ma.name  as merchant_account_name,
                        ma.auto_payouts,
                        ma.id as merchant_account_id,
                        ma.payment_page_theme,
                        t.order_num,
                        t.ref_num,
                        t.description,
                        b.name,
                        ba.fc_merchant_id,
                        t.dst_amount,
                        t.src_amount,
                        t.src_currency,
                        t.dst_currency,
                        t.src_wallet_id,
                        t.dst_wallet_id,
                        a.realm_id,
                        t.id,
                        t.transfer_type,
                        t.event_name,
                        t.account_id,
                        t.merchant_account_id,
                        t.settlement_id,
                        t.is_exchange,
                        t.exchange_id,
                        w.bank_account_id,
                        t."data",
                        t.ext_user_id,
                        t.event_name,
                        ba.bank_id,
                        COALESCE((
                          SELECT json_agg(json_build_object('protocol_type', pp.protocol_type, 'identifier_label',
                          pp.identifier_label, 'account_name', ba.account_name, 'instruction',
                          pp.instruction, 'identifier', bap.identifier,'bank_name',b.name,'acc_no',ba.acc_no))
                          FROM ${db.schema}.bank_accounts ba
                                      inner join ${db.schema}.bank_accounts_protocols bap on (ba.id=bap.bank_account_id and bap.removed=0)
                                              INNER JOIN ${db.schema}.payment_protocols pp
                                                          on (pp.id=bap.payment_protocol_id  AND pp.removed = 0)
                                              INNER JOIN ${db.schema}.banks b on b.id = ba.bank_id AND b.removed=0
                                      WHERE ba.removed = 0
                                        AND ba.id = w.bank_account_id
                                  ),
                                  '[]'::json)                                                                                  payment_options,
                        (SELECT tx.activity FROM ${db.schema}.transactions tx WHERE transfer_id = t.id and tx.activity not in ('FEE','REVERSE_FEE') ORDER BY tx.ctime DESC LIMIT 1) as status,
                        (SELECT tx.brn FROM ${db.schema}.transactions tx WHERE transfer_id = t.id and tx.activity not in ('FEE','REVERSE_FEE') ORDER BY tx.ctime DESC LIMIT 1) as brn
                  FROM ${db.schema}.transfers t
                          INNER JOIN ${db.schema}.accounts a on a.id = t.account_id
                          INNER JOIN ${db.schema}.merchant_accounts ma on ma.id = t.merchant_account_id
                          INNER JOIN ${db.schema}.wallets w on w.id = t.src_wallet_id
                          INNER JOIN ${db.schema}.bank_accounts ba on ba.id = w.bank_account_id
                          INNER JOIN ${db.schema}.banks b on b.id = ba.bank_id
                  WHERE t.id = :transferId`,
    {
      raw: true,
      // logging: console.log,
      type: db.sequelize.QueryTypes.SELECT,
      replacements: {
        transferId
      }
    }
  );
  const transfer_meta = await db.transfers_meta.findOne({
    where: { transfer_id: transferId },
    attributes: ["transfer_request"]
  });
  if (transfer_meta && transfer_meta.transfer_request.protocols) {
    let pp = await t[0].payment_options.filter((el) =>
      transfer_meta.transfer_request.protocols.some((protocol) => {
        return el.identifier_label === protocol;
      })
    );
    t[0].payment_options = pp;
  }
  console.log("_getTransferDetails  transfer " + JSON.stringify(t[0]));
  return t[0];
}
async function getTxDetails(data, service) {
  const tx = await _getTransferDetails(data.transfer_id, service);
  if (tx) {
    return tx;
  } else {
    throw new service.ServiceError("BANJO_ERR_TX_NOT_FOUND");
  }
}
async function txActivityPageOpened(data, service) {
  const t = await _getTransferDetails(data.transfer_id, service);
  if (t) {
    if (
      [
        db.transfer.TRANSFER_STATUSES.APPROVED,
        db.transfer.TRANSFER_STATUSES.BRN_SUBMIT,
        db.transfer.TRANSFER_STATUSES.REJECTED,
        db.transfer.TRANSFER_STATUSES.REVERSE
      ].indexOf(t.status) >= 0
    ) {
      if (t.data && !t.data.system_reject) {
        return {
          success: false,
          error: {
            title: "Transaction Failed",
            message: "Transaction failed, this transaction is already processed"
          },
          paymentDetails: t,
          data: t.data
        };
      }
    }
    const res = await updateActivity(t, "PAYMENT_PAGE_OPENED", data, service);
    if (res.error) {
      return res;
    }
    t.success = true;
    return t;
  } else {
    throw new service.ServiceError("BANJO_ERR_TX_NOT_FOUND");
  }
}
async function txActivitySubmitVPA(data, service) {
  const t = await _getTransferDetails(data.transfer_id, service);
  if (t) {
    if (
      [
        db.transfer.TRANSFER_STATUSES.APPROVED,
        db.transfer.TRANSFER_STATUSES.BRN_SUBMIT,
        db.transfer.TRANSFER_STATUSES.REJECTED,
        db.transfer.TRANSFER_STATUSES.REVERSE
      ].indexOf(t.status) >= 0
    ) {
      if (t.data && !t.data.system_reject) {
        throw new service.ServiceError("BANJO_ERR_TRANSACTION_FAILED");
      }
    }
    const res = await updateActivity(t, "SUBMIT_VPA", data, service);
    //Update transactions table
    const transaction = await db.transaction.findOne({
      where: { transfer_id: data.transfer_id, activity: "VPA_SUBMIT" },
      order: [["ctime", "DESC"]]
    });
    if (transaction) {
      //call freecharge UPI payment service
      const upiResponse = await Queue.newJob("transaction-service", {
        method: "upiPayment",
        data: {
          merchantId: t.fc_merchant_id,
          requestId: transaction.id,
          orderId: t.order_num,
          orderDesc: "UPI Transaction",
          amount: t.dst_amount.toString(),
          payerVpa: data.vpa
        }
      });
      if (upiResponse.result.result.requestStatus === "COMPLETED") {
        if (upiResponse.result.status === "FAILED") {
          return {
            success: false,
            error: {
              title: "Transaction Failed",
              message: upiResponse.result.statusDesc
            },
            paymentDetails: t,
            data: t.data,
            validationError: upiResponse.result.statusDesc
          };
        }
      }

      await db.transaction.update(
        {
          external_id: upiResponse.result.txId,
          external_type: upiResponse.result.txType
        },
        { where: { id: transaction.dataValues.id } }
      );
      return t;
    } else {
      throw new service.ServiceError("BANJO_ERR_TX_NOT_FOUND");
    }
  } else {
    throw new service.ServiceError("BANJO_ERR_TX_NOT_FOUND");
  }
}
async function txActivitySubmitBRN(data, service) {
  const t = await _getTransferDetails(data.transfer_id, service);
  console.log("txActivitySubmitBRN before processing" + t.auto_payouts);
  if (t) {
    if (data.payer_vpa) {
      t.data.payer_vpa = data.payer_vpa;
      let newVpa = data.payer_vpa;
      const transfer = await db.transfer.findByPk(data.transfer_id);

      if (transfer) {
        transfer.data.vpa = newVpa;
        await transfer.update({ data: transfer.data });
      }
    }
    if (
      [
        db.transfer.TRANSFER_STATUSES.APPROVED,
        db.transfer.TRANSFER_STATUSES.BRN_SUBMIT,
        db.transfer.TRANSFER_STATUSES.REJECTED,
        db.transfer.TRANSFER_STATUSES.REVERSE,
        db.transfer.TRANSFER_STATUSES.SETTLEMENT
      ].indexOf(t.status) >= 0
    ) {
      if (t.data && !t.data.system_reject) {
        return {
          success: false,
          error: {
            title: "Transaction Failed",
            message: "Transaction failed, this transaction is already processed"
          },
          paymentDetails: t,
          data: t.data
        };
      }
    }
    const res = await updateActivity(t, "SUBMIT_BRN", data, service);
    if (res.error) {
      return res;
    }
    console.log(
      "digiPaymentBankAcc " +
        JSON.stringify(t.data) +
        "auto_payouts " +
        t.auto_payouts
    );
    console.log("digiPaymentBankAcc " + t.auto_payouts);
    if (
      t.auto_payouts === 1 &&
      t.transfer_type === db.transfer.TRANSFER_TYPE.COLLECTION &&
      t.data &&
      t.data.digiPaymentBankAcc
    ) {
      console.log(
        "digiPaymentBankAcc inside if " +
          JSON.stringify(t.data) +
          "auto_payouts " +
          t.auto_payouts
      );
      const executeAutoCollection = await Queue.newJob("merchant-service", {
        method: "executeAutoCollection",
        data: {
          merchant_account_id: t.merchant_account_id,
          brn: data.brn,
          transfer_id: data.transfer_id
        }
      });
    }
    telegramBot.sendMessage(t, data, "updated", "brn submit");
    return {
      success: true,
      status: db.transfer.TRANSFER_STATUSES.BRN_SUBMIT,
      merchant_name: t.merchant_name,
      merchant_acc_name: t.merchant_account_name,
      merchant_account_name: t.merchant_account_name,
      order_num: t.order_num,
      description: t.description,
      data: t.data,
      id: t.id,
      amount: t.src_amount,
      currency: t.src_currency,
      payment_page_theme: t.payment_page_theme
    };
  } else {
    throw new service.ServiceError("BANJO_ERR_TX_NOT_FOUND");
  }
}
async function txActivityApprove(data, service) {
  let UISTrackingFlag = false;
  console.log(`Func:txActivityApprove. Received Data in request`, data);
  if (data.actual_approval_amount) {
    await db.transfer.update(
      {
        src_amount: data.actual_approval_amount,
        dst_amount: data.actual_approval_amount,
        data: db.sequelize.literal(`jsonb_set(
      jsonb_set(data, '{original_src_amount}', '"${data.src_amount}"', true),
      '{original_dst_amount}', '"${data.dst_amount}"', true
    )`)
      },
      {
        where: {
          id: data.transfer_id
        }
      }
    );
  }

  const t = await _getTransferDetails(data.transfer_id, service);
  console.log("transfer's details: ", t);
  if (t) {
    if (
      [
        db.transfer.TRANSFER_STATUSES.APPROVED,
        db.transfer.TRANSFER_STATUSES.REJECTED,
        db.transfer.TRANSFER_STATUSES.PENDING,
        db.transfer.TRANSFER_STATUSES.REVERSE,
        db.transfer.TRANSFER_STATUSES.SETTLEMENT
      ].indexOf(t.status) >= 0
    ) {
      if (t.data && !t.data.system_reject) {
        throw new service.ServiceError("BANJO_ERR_TRANSACTION_FAILED");
      }
    }
    if (
      t.status != db.transfer.TRANSFER_STATUSES.BRN_SUBMIT &&
      !data.brn &&
      !data.auto_payout &&
      !data.uis_approval
    ) {
      throw new service.ServiceError("BANJO_ERR_BRN_MISSING");
    }
    if (t.brn !== data.brn) {
      await db.transfer.update(
        {
          data: db.sequelize.literal(`jsonb_set(
      data, '{original_submitted_brn}', '"${t.brn}"', true
    )`)
        },
        {
          where: {
            id: data.transfer_id
          }
        }
      );
    } else if (t.brn === data.brn) {
      delete data.brn;
    }
    const res = await updateActivity(t, "APPROVE_TX", data, service);
    if (res.error) {
      return res;
    }
    // Queue.publish(`UPI-Payment-Status`, {
    //   transfer_id: data.transfer_id,
    //   status: "COMPLETED"
    // });
    if (t.transfer_type === "SETTLEMENT") {
      _sendOwnerSettlementsReport(t);
    }
    // return { status: db.transfer.TRANSFER_STATUSES.APPROVED };
  } else {
    throw new service.ServiceError("BANJO_ERR_TX_NOT_FOUND");
  }
  try {
    switch (t.event_name) {
      case "transaction-service:collectionTrigger":
        {
          Queue.publish(`UPI-Payment-Status`, {
            transfer_id: data.transfer_id,
            status: "COMPLETED"
          });
          UISTrackingFlag = true;
        }
        break;
      default:
        break;
    }

    telegramBot.sendMessage(t, data, "updated", "approved");
    return { status: db.transfer.TRANSFER_STATUSES.APPROVED };
  } catch (err) {
    console.error(
      `transaction-service. Func: txActivityApprove. Error while publishing mail:`,
      err
    );
  } finally {
    if (UISTrackingFlag) {
      let bankAccountInfo = t.payment_options.filter(
        (el) => el.protocol_type === "UPI"
      );
      const savedVpa = await UISTracking.saveVPAv2(
        {
          merchantId: t.account_id,
          merchantAccountId: t.merchant_account_id,
          merchantUserId: t.ext_user_id,
          paymentData: {
            orangeId: t.order_num,
            merchantData: {
              name: t.merchant_name
            },
            ppageData: {
              vpa: t.data.vpa
            },
            bankData: {
              name: bankAccountInfo.length
                ? bankAccountInfo[0].account_name
                : t.payment_options[0].account_name,
              vpa: bankAccountInfo.length && bankAccountInfo[0].identifier_label
            }
          }
        },
        service
      );
      console.log("saved vpa: ", savedVpa);
    }
  }
}

async function txActivityPending(data, service) {
  console.log(`Func:txActivityPending. Received Data in request`, data);
  const t = await _getTransferDetails(data.transfer_id, service);
  if (t) {
    if (
      [
        db.transfer.TRANSFER_STATUSES.APPROVED,
        db.transfer.TRANSFER_STATUSES.REJECTED,
        db.transfer.TRANSFER_STATUSES.PENDING,
        db.transfer.TRANSFER_STATUSES.REVERSE
      ].indexOf(t.status) >= 0
    ) {
      if (t.data && !t.data.system_reject) {
        throw new service.ServiceError("BANJO_ERR_TRANSACTION_FAILED");
      }
    }
    if (
      t.status != db.transfer.TRANSFER_STATUSES.BRN_SUBMIT &&
      !data.brn &&
      !data.auto_payout
    ) {
      throw new service.ServiceError("BANJO_ERR_BRN_MISSING");
    }
    const res = await updateActivity(t, "PENDING_TX", data, service);
    if (res.error) {
      return res;
    }
    Queue.publish(`UPI-Payment-Status`, {
      transfer_id: data.transfer_id,
      status: "COMPLETED"
    });
    telegramBot.sendMessage(
      t,
      { action_type: "SYSTEM", ...data },
      "updated",
      "pending"
    );
    return { status: db.transfer.TRANSFER_STATUSES.PENDING };
  } else {
    throw new service.ServiceError("BANJO_ERR_TX_NOT_FOUND");
  }
}
async function txActivityReverse(data, service) {
  console.log(`Func:txActivityApprove. Received Data in request`, data);
  const t = await _getTransferDetails(data.transfer_id, service);
  if (t) {
    if (
      [
        db.transfer.TRANSFER_STATUSES.APPROVED,
        db.transfer.TRANSFER_STATUSES.REJECTED,
        db.transfer.TRANSFER_STATUSES.REVERSE
      ].indexOf(t.status) >= 0
    ) {
      if (t.data && !t.data.system_reject) {
        throw new service.ServiceError("BANJO_ERR_TRANSACTION_FAILED");
      }
    }
    if (
      t.status != db.transfer.TRANSFER_STATUSES.BRN_SUBMIT &&
      !data.brn &&
      !data.auto_payout
    ) {
      throw new service.ServiceError("BANJO_ERR_BRN_MISSING");
    }
    const transaction = await db.transaction.findOne({
      where: { transfer_id: data.transfer_id, activity: "FEE" },
      order: [["ctime", "DESC"]]
    });
    if (!transaction) {
      throw new service.ServiceError("BANJO_ERR_TRANSACTION_FAILED");
    }
    let amount = 0;
    if (transaction.data.rate_of_fee) {
      amount =
        (parseFloat(t.dst_amount) * parseFloat(transaction.data.rate_of_fee)) /
        100;
    }
    if (isNaN(amount))
      throw new service.ServiceError(
        "BANJO_ERR_INVALID_AMOUNT_TRANSACTION_FAILED"
      );

    const fee_res = await updateActivity(
      { ...t, dst_amount: amount },
      "REVERSE_FEE_TX",
      data,
      service
    );
    if (fee_res.error) {
      return fee_res;
    }
    const res = await updateActivity(t, "REVERSE_TX", data, service);
    if (res.error) {
      return res;
    }
    Queue.publish(`UPI-Payment-Status`, {
      transfer_id: data.transfer_id,
      status: "COMPLETED"
    });
    telegramBot.sendMessage(
      t,
      { action_type: "SYSTEM", ...data },
      "updated",
      "transfer reverse"
    );
    return { status: db.transfer.TRANSFER_STATUSES.REVERSE };
  } else {
    throw new service.ServiceError("BANJO_ERR_TX_NOT_FOUND");
  }
}

async function txActivityReject(data, service) {
  const t = await _getTransferDetails(data.transfer_id, service);
  if (t) {
    if (
      !data.brn &&
      [
        db.transfer.TRANSFER_STATUSES.BRN_SUBMIT,
        db.transfer.TRANSFER_STATUSES.VPA_SUBMIT,
        db.transfer.TRANSFER_STATUSES.REQUEST
      ].indexOf(t.status) == -1
    ) {
      throw new service.ServiceError("BANJO_ERR_TX_ALREADY_PROCESSING");
    }
    const res = await updateActivity(t, "REJECT_TX", data, service);
    if (res && res.error) {
      return res;
    }
    if (data && data.system_reject) {
      db.transfer
        .update(
          {
            data: db.sequelize.literal(
              `data || '{"system_reject": ${JSON.stringify(
                data.system_reject
              )}}'::jsonb`
            )
          },
          { where: { id: data.transfer_id } }
        )
        .then(() => {
          console.log("Data updated successfully");
        })
        .catch((error) => {
          console.error("Error updating data:", error);
        });
    }
    Queue.publish(`UPI-Payment-Status.${data.transfer_id}`, {
      transfer_id: data.transfer_id,
      status: "REJECTED"
    });
    telegramBot.sendMessage(
      t,
      { action_type: "SYSTEM", ...data },
      "updated",
      "rejected"
    );
    return {
      status: db.transfer.TRANSFER_STATUSES.REJECTED,
      paymentDetails: t,
      data: t.data
    };
  } else {
    throw new service.ServiceError("BANJO_ERR_TX_NOT_FOUND");
  }
}
async function updateSettlementActivity(tx, action, method, data, service) {
  const res = await Queue.newJob("transaction-service", {
    method,
    data: {
      fee: data.fee,
      brn: data.brn,
      transfer_id: tx.id,
      action: action,
      description: tx.description,
      order_id: tx.order_num,
      bank_account_id: tx.bank_account_id,
      dst_wallet_id: tx.dst_wallet_id,
      src_wallet_id: tx.src_wallet_id,
      src_currency: tx.src_currency,
      src_amount: tx.src_amount,
      dst_currency: tx.dst_currency,
      dst_amount: tx.dst_amount,
      settlement_id: tx.settlement_id,
      merchant_account_id: tx.merchant_account_id,
      exchange_id: tx.exchange_id,
      is_exchange: tx.is_exchange,
      data: {
        ua: service.ua,
        ip: service.ip,
        user_ip: data.user_ip,
        note: data.note,
        admin_action: data.admin_action,
        admin_id: data.admin_id,
        activity_action: data.activity_action,
        action_type: data.action_type,
        exchange_bank_account: data.src_bank_acc_id,
        exchange_src_currency: data.src_currency,
        exchange_src_amount: data.src_amount
      }
    },
    options: {
      ...service,
      realmId: tx.realm_id,
      accountId: tx.account_id,
      maId: tx.merchant_account_id
    }
  });
  if (res.result) {
    if (!res.result.transfer_id) {
      console.error(
        "transaction-service. Func: updateActivity. No Transfer Id generated"
      );
      throw new service.ServiceError("BANJO_ERR_TARIFF_CONFIG");
    }
    return {
      transfer_id: res.result.transfer_id
    };
  } else {
    if (
      res.error &&
      res.error.name == "SequelizeDatabaseError" &&
      res.error.original &&
      res.error.original.where &&
      res.error.original.where.indexOf("balance_check") >= 0
    ) {
      return {
        success: false,
        error: {
          title: "Insufficient Balance",
          message: "Insufficient balance in source account"
        },
        paymentDetails: tx
      };
    } else if (
      res.error &&
      res.error.name == "SequelizeUniqueConstraintError" &&
      res.error.original &&
      res.error.original.constraint == "transactions_brn"
    ) {
      return {
        success: false,
        error: {
          title: "BRN already exists into our records",
          message:
            "Duplicate (BRN) bank reference number detected, please contact support"
        },
        paymentDetails: tx
      };
    } else {
      throw res.error;
    }
  }
}
async function checkSrcLedgerAccount(data, service) {
  const bank = await db.bank_accounts.findOne({
    where: {
      id: data.exchange_account_id
    },
    attributes: ["bank_id"]
  });
  if (!bank) {
    throw new service.ServiceError("BANJO_ERR_EXCHANGE_ACCOUNT_NOT_FOUND");
  }
  const srcLedgerAccount = await db.bank_accounts.findOne({
    where: {
      bank_id: bank.bank_id,
      currency: data.src_currency
    },
    attributes: ["id"]
  });
  if (!srcLedgerAccount) {
    throw new service.ServiceError("BANJO_ERR_EXCHANGE_SRC_ACCOUNT_NOT_FOUND");
  }
  return srcLedgerAccount;
}
async function fetchExchangeRecord(id) {
  const exchange = await db.exchange.findOne({
    where: {
      id
    }
  });
  return exchange;
}
async function fetchSettlementRecord(id) {
  const settlement = await db.settlements.findOne({
    where: {
      id
    }
  });
  return settlement;
}
async function updateActivity(tx, action, data, service) {
  switch (tx.transfer_type) {
    case "SETTLEMENT":
      return updateSettlementAction(tx, action, data, service);
      break;
    case "COLLECTION":
      return updateTransferActivity(
        tx,
        action,
        "collectionTrigger",
        data,
        service
      );
      break;
    case "PAYOUT":
      let method = "payoutTrigger";

      if (tx.event_name === "transaction-service:settlementPayoutTrigger") {
        method = "settlementPayoutTrigger";
      }
      return updateTransferActivity(tx, action, method, data, service);
      break;
    case "DIRECT_DEPOSIT":
      return updateDirectDepositAction(tx, action, data, service);
      break;
  }
}
async function txSubmitBRN(data, service) {
  const t = await _getTransferDetails(data.transfer_id, service);
  if (t) {
    if (t.merchant_account_id !== service.maId) {
      throw new service.ServiceError(
        "BANJO_ERR_CURRENT_MERCHANT_ACCOUNT_DOES_NOT_ACCESS_TO_SUBMIT_BRN"
      );
    }
    const res = await txActivitySubmitBRN(data, service);
    return res;
  } else {
    throw new service.ServiceError("BANJO_ERR_TX_NOT_FOUND");
  }
}

async function updateSettlementAction(tx, action, data, service) {
  switch (action) {
    case "APPROVE_TX":
      let response;
      if (tx.is_exchange) {
        response = await updateSettlementActivity(
          {
            ...tx,
            dst_currency: tx.src_currency, //same amount transfer from source to dst_amount
            dst_amount: tx.src_amount // both Currency type wallet
          },
          "INTERNAL_FUNDS_SHIFT",
          "settlementTrigger",
          data,
          service
        );
        if (response.error) {
          return response;
        }
        const exchange = await fetchExchangeRecord(tx.exchange_id);
        const settlement = await fetchSettlementRecord(tx.settlement_id);
        if (exchange && settlement) {
          delete data.brn;
          const srcLedgerAccount = await checkSrcLedgerAccount(
            { ...data, ...settlement.dataValues },
            service
          );
          const [ledgerSrcAccount, walletDstAccount] = await Promise.all([
            Wallet.getCreateLedgerAccount(
              srcLedgerAccount.id,
              tx.src_currency,
              service
            ),
            Wallet.getCreateWalletAccount(
              srcLedgerAccount.id,
              settlement.dst_currency,
              settlement.merchant_account_id,
              service
            )
          ]);
          // transfer wallet  balance to the Exchange bank account same source currency
          const res = await updateSettlementActivity(
            {
              ...tx,
              src_wallet_id: tx.dst_wallet_id,
              dst_wallet_id: ledgerSrcAccount.id,
              dst_currency: tx.src_currency, // both Currency type wallet
              dst_amount: tx.src_amount
            },
            "EXCHANGE_WITHDRAW",
            "settlementTrigger",
            data,
            service
          );
          if (res.error) {
            return res;
          }
          let converted = exchange.quoted_rate
            ? exchange.quoted_rate * tx.src_amount
            : tx.src_amount;
          let src_amount =
            converted - converted * (exchange.applied_rate / 100);

          //Exchange balance
          const resp = await updateSettlementActivity(
            {
              ...tx,
              src_wallet_id: exchange.ledger_wallet_id,
              dst_wallet_id: walletDstAccount.id,
              src_currency: exchange.dst_currency,
              src_amount: src_amount,
              dst_currency: exchange.dst_currency,
              dst_amount: src_amount
            },
            "APPROVE_SETTLEMENT",
            "settlementTrigger",
            data,
            service
          );
          if (resp.error) {
            return resp;
          }
        }
      } else {
        response = await updateSettlementActivity(
          {
            ...tx,
            dst_currency: tx.src_currency, //same amount transfer from source to dst_amount
            dst_amount: tx.src_amount // both Currency type wallet
          },
          "APPROVE_SETTLEMENT",
          "settlementTrigger",
          data,
          service
        );
      }
      updateSettlementStatus(tx.settlement_id);
      return response;
      break;
    case "REJECT_TX":
      const res = await updateSettlementActivity(
        {
          ...tx,
          dst_currency: tx.src_currency,
          dst_amount: tx.src_amount
        },
        "REJECT_SETTLEMENT",
        "settlementTrigger",
        data,
        service
      );
      updateSettlementStatus(tx.settlement_id);
      return res;

      break;
    default:
      return {
        success: false,
        error: {
          title: "Incorrect action",
          message: `${action} not type on settlement`
        },
        paymentDetails: tx
      };
  }
}

async function updateDirectDepositAction(tx, action, data, service) {
  switch (action) {
    case "APPROVE_TX":
      let bankAccountId = await Wallet.getBankAccount({
        wallet_id: tx.dst_wallet_id
      });
      const src_ledger_account = await Wallet.getCreateLedgerAccount(
        bankAccountId.bank_account_id,
        tx.dst_currency
      );

      const approveRes = updateSettlementActivity(
        {
          ...tx,
          bank_account_id: bankAccountId.bank_account_id,
          src_wallet_id: src_ledger_account.id,
          src_amount: tx.dst_amount,
          src_currency: tx.dst_currency
        },
        "APPROVE_TX",
        "directDepositTrigger",
        data,
        service
      );
      return approveRes;

      break;
    case "REJECT_TX":
      const res = await updateSettlementActivity(
        {
          ...tx,
          dst_currency: tx.src_currency,
          dst_amount: tx.src_amount
        },
        "REJECT_TX",
        "directDepositTrigger",
        data,
        service
      );

      return res;

      break;
    default:
      return {
        success: false,
        error: {
          title: "Incorrect action",
          message: `${action} not type on direct deposit trigger`
        },
        paymentDetails: tx
      };
  }
}
async function updateTransferActivity(tx, action, method, data, service) {
  let wallet_id = tx.dst_wallet_id;
  if (method === "payoutTrigger" || method === "settlementPayoutTrigger") {
    wallet_id = tx.src_wallet_id;
  }
  const res = await Queue.newJob("transaction-service", {
    method,
    data: {
      brn: data.brn,
      transfer_id: tx.id,
      action: action,
      description: tx.description,
      currency: tx.dst_currency,
      amount: tx.dst_amount,
      order_id: tx.order_num,
      bank_account_id: tx.bank_account_id,
      wallet_id,
      data: {
        ua: service.ua,
        ip: service.ip,
        user_ip: data.user_ip,
        note: data.note,
        admin_action: data.admin_action,
        admin_id: data.admin_id,
        activity_action: data.activity_action,
        action_type: data.action_type
      }
    },
    options: {
      ...service,
      realmId: tx.realm_id,
      accountId: tx.account_id,
      maId: tx.merchant_account_id
    }
  });
  if (res.result) {
    if (!res.result.transfer_id) {
      throw new service.ServiceError("BANJO_ERR_TARIFF_CONFIG");
    }
    return {
      transfer_id: res.result.transfer_id
    };
  } else {
    if (
      res.error &&
      res.error.name == "SequelizeDatabaseError" &&
      res.error.original &&
      res.error.original.where &&
      res.error.original.where.indexOf("balance_check") >= 0
    ) {
      return {
        success: false,
        error: {
          title: "Insufficient Balance",
          message: "Insufficient balance in source account"
        },
        paymentDetails: tx
      };
    } else if (
      res.error &&
      res.error.name == "SequelizeUniqueConstraintError" &&
      res.error.original &&
      res.error.original.constraint == "transactions_brn"
    ) {
      return {
        success: false,
        error: {
          title: "BRN already exists into our records",
          message:
            "Duplicate (BRN) bank reference number detected, please contact support"
        },
        paymentDetails: tx
      };
    } else {
      throw res.error;
    }
  }
}

async function updateSettlementStatus(settlement_id) {
  let t = await db.sequelize.query(
    `SELECT 
  (SELECT tx.activity FROM ${db.schema}.transactions tx WHERE 
  tx.transfer_id = t.id and tx.activity not in ('FEE') 
  ORDER BY tx.ctime DESC LIMIT 1) as status
  FROM ${db.schema}.transfers t  
  WHERE t.settlement_id  = :settlement_id`,
    {
      raw: true,
      // logging: console.log,
      type: db.sequelize.QueryTypes.SELECT,
      replacements: {
        settlement_id
      }
    }
  );
  let status = 2;
  if (t && t.length) {
    const count = {};
    for (const element of t) {
      if (count[element.status]) {
        count[element.status] += 1;
      } else {
        count[element.status] = 1;
      }
    }
    if (count && count.SETTLEMENT_APPROVED) {
      if (count.SETTLEMENT_APPROVED === t.length) {
        status = 1;
      } else status = 4;
    } else if (count && count.REJECTED) {
      if (count.REJECTED === t.length) {
        status = 3;
      } else status = 4;
    }
    await db.settlements.update(
      {
        status
      },
      {
        where: { id: settlement_id }
      }
    );
    return { status, t };
  }
}

async function txActivitySavePayerDetails(data, service) {
  try {
    const existingRecord = await db.transfer.findByPk(data.data.transfer_id);

    if (!existingRecord) {
      throw new Error("Record not found");
    }

    let existingData = existingRecord.data;
    if (typeof existingData === "string") {
      existingData = JSON.parse(existingData || "{}");
    }
    for (const key in data.data) {
      existingData[key] = data.data[key];
    }

    await existingRecord.update({ data: existingData });
    const t = await getTxDetails(data.data, service);
    t.success = true;
    return t;
  } catch (error) {
    throw error;
  }
}

async function changePayoutSourceBankAccount(data, service) {
  try {
    const tx = await _getTransferDetails(data.transfer_id, service);
    const wallet = await Wallet.getCreateWalletAccount(
      data.bank_account_id,
      data.dst_currency,
      data.merchant_account_id,
      service
    );

    let feeAmount = await getRateOfFee({
      maId: data.merchant_account_id,
      amount: tx.src_amount,
      accountId: tx.account_id,
      realmId: service
    });

    let totalAmount = feeAmount + tx.src_amount;

    let hasSufficientBalance = await _hasSufficientBalance(
      {
        amount: totalAmount,
        merchant_account_id: data.merchant_account_id,
        currency: tx.src_currency
      },
      service
    );
    if (!hasSufficientBalance) {
      console.error(
        "Func:startProcessPayouts. Caught Error: Insufficient balance for transfer Id: " +
          tx.id
      );
      throw new ServiceError("BANJO_ERR_INSUFFICIENT_BALANCE");
    }

    await db.transfer.update(
      {
        src_wallet_id: wallet.id
      },
      {
        where: { id: data.transfer_id }
      }
    );
    return {
      success: true,
      body: {
        title: "Account updated successfully",
        message: "Source Bank Account for payout was updated successfully"
      }
    };
  } catch (error) {
    throw error;
  }
}

async function getRateOfFee(data) {
  try {
    let Data = {
      trigger: "transaction-service:payoutTrigger",
      variables: [],
      data: {
        action: "APPROVE_TX",
        amount: data.amount
      }
    };
    let service1 = {
      realmId: data.realmId,
      accountId: data.accountId,
      maId: data.maId
    };
    var fee = await Queue.newJob("tariff-service", {
      method: "calculateFee",
      data: Data,
      options: {
        ...service1
      }
    });
    let amount = 0;
    if (
      fee &&
      fee.result &&
      fee.result.transactions &&
      fee.result.transactions.length
    ) {
      for (let i = 0; i < fee.result.transactions.length; i++) {
        if (
          "FEE" === fee.result.transactions[i].txsubtype &&
          !isNaN(fee.result.transactions[i].amount)
        ) {
          amount += fee.result.transactions[i].amount;
        }
      }
    }
    return amount;
  } catch (error) {
    throw error;
  }
}
async function _hasSufficientBalance(data, service) {
  let collectionBalance = await db.sequelize.query(
    `SELECT w.bank_account_id, w.balance FROM  ${db.schema}.wallets w 
            join ${db.schema}.vw_merchant_account_mappings mam on mam.id = w.merchant_account_mapping_id and mam.merchant_account_id = :merchant_account_id
            where w.removed = 0 and w.type = 'COLLECTION' and w.currency= :src_currency and w.balance>=:amount order by w.balance asc`,
    {
      raw: true,
      type: db.sequelize.QueryTypes.SELECT,
      replacements: {
        merchant_account_id: data.merchant_account_id,
        src_currency: data.currency,
        amount: data.amount
      }
    }
  );
  if (collectionBalance && collectionBalance.length) {
    return collectionBalance[0].balance >= data.amount;
  }
  return false;
}
async function _sendOwnerSettlementsReport(tx) {
  let settlement_transfers = await db.sequelize.query(
    `select * from ${db.schema}.vw_tx_transfer vtt
     left join ${db.schema}.settlements s on s.id = vtt.settlement_id 
      where vtt.transfer_type = 'SETTLEMENT' and vtt.settlement_id = :settlement_id and vtt.activity not in ('APPROVED', 'REJECTED') and (s.settlement_data->0->>'reported' IS NULL)
    `,
    {
      replacements: {
        settlement_id: tx.settlement_id
      },
      raw: true
    }
  );
  settlement_transfers = settlement_transfers[0];
  if (!settlement_transfers || !settlement_transfers.length) {
    console.log(
      "File: activity.js, Func: _sendOwnerSettlementsReport. No settlements found"
    );
    return;
  }
  const pending_settlements = settlement_transfers.filter((el) => {
    return (
      el.activity_action !== "APPROVED" && el.activity_action !== "REJECTED"
    );
  });

  if (!pending_settlements || !pending_settlements.length) {
    const owner_emails = await db.sequelize.query(
      `select u.email from ${db.schema}.user_accounts ua 
      left join ${db.schema}.users u on ua.user_id = u.id
      left join ${db.schema}.roles r on r.id = ua.main_role
      where ua.account_id = :merchant_id and r.role_name = 'Owner'
      `,
      {
        replacements: {
          merchant_id: tx.account_id
        },
        raw: true
      }
    );
    const ctime = settlement_transfers[0].ctime;
    const dateObject = new Date(ctime);
    const options = {
      year: "numeric",
      month: "long",
      day: "numeric"
    };

    // Format the date using the options
    const formattedDate = dateObject.toLocaleDateString("en-US", options);
    var totalAmount = 0;
    await Promise.all(
      settlement_transfers.map(async (settlement) => {
        totalAmount += settlement.dst_amount;
        if (settlement.settlement_data) {
          settlement.settlement_data[0].reported = true;
          await db.settlements.update(
            {
              settlement_data: settlement.settlement_data
            },
            {
              where: {
                id: settlement.id
              }
            }
          );
        }
      })
    );
    if (owner_emails && owner_emails.length) {
      for (let email of owner_emails[0]) {
        Queue.publish(`NOTIFY.SETTLEMENT-REPORT`, {
          user_id: null,
          options: {
            send_sms: false,
            send_push: false,
            to_email: email.email
          },
          payload: {
            settlements: settlement_transfers,
            createdAt: formattedDate,
            totalAmount
          }
        });
      }
    }
  } else {
    console.log(
      "File: activity.js, Func: _sendOwnerSettlementsReport. Pending settlements found"
    );
    return;
  }
}

async function txActivitySaveReceipt(data, service) {
  try {
    await db.transfer.update(
      {
        data: db.sequelize.literal(`(
      SELECT jsonb_object_agg(key, value) 
      FROM jsonb_each(data) 
      ) || '{"receipt_url": "${data.url}"}'::jsonb`)
      },
      {
        where: {
          id: data.transfer_id
        }
      }
    );
  } catch (error) {
    console.log(
      "File: activity.js, Func: txActivitySaveReceipt, error: ",
      error
    );
    return error;
  }
}

export default {
  txActivityPageOpened,
  txActivitySubmitVPA,
  txActivitySubmitBRN,
  txActivityApprove,
  txActivityReject,
  getTxDetails,
  txSubmitBRN,
  txActivityPending,
  txActivityReverse,
  txActivitySavePayerDetails,
  changePayoutSourceBankAccount,
  txActivitySaveReceipt
};
