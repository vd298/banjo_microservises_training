import Base from "@lib/base";
import Schema from "@lib/schema";
import Transfer from "./lib/transactions/transfer";
import Collection from "./lib/transactions/collection";
import Payout from "./lib/transactions/payout";
import Activity from "./lib/transactions/activity";
import Freecharge from "./lib/transactions/freecharge";
import Webhook from "./lib/transactions/webhook";
import BankTx from "./lib/transactions/BankTransactions";
import directDeposit from "./lib/transactions/directDeposit";

import Wallet from "./lib/transactions/wallet";
import activity from "./lib/transactions/activity";
import telegramBot from "./lib/transactions/telegramBot";
export default class Service extends Base {
  publicMethods() {
    return {
      serviceDescription: {},
      ping: {
        description: "Test ping-pong method",
        schema: {
          type: "object",
          properties: {
            text: { type: "string" },
            num: { type: "number" }
          },
          required: ["text"]
        }
      },
      getPublicMethods: {
        realm: true,
        description: "getPublicMethods"
      },
      doTransfer: {
        private: true,
        realm: true,
        user: false,
        description:
          "Do transfer for inner request (do not add permissions for it to realms)"
      },
      collectionTrigger: {
        method: Collection.collectionTrigger,
        schema: Schema.TRANSACTION_SCHEMA.COLLECTION_TRIGGER,
        description: "Process transaction"
      },
      payoutTrigger: {
        method: Payout.payoutTrigger,
        schema: Schema.TRANSACTION_SCHEMA.PAYOUT_TRIGGER,
        description: "Process transaction"
      },
      settlementPayoutTrigger: {
        method: Payout.payoutTrigger,
        schema: Schema.TRANSACTION_SCHEMA.SETTLEMENT_PAYOUT_TRIGGER,
        description: "Process transaction"
      },
      txActivityPageOpened: {
        private: true, //@ToDo Make it private
        method: Activity.txActivityPageOpened,
        description: "Payment page opened",
        schema: Schema.MERCHANT_SCHEMA.TX_ACTIVITY_PAGE_OPENED
      },
      txActivitySubmitBRN: {
        private: true, //@ToDo Make it private
        method: Activity.txActivitySubmitBRN,
        description: "Submit BRN",
        schema: Schema.TRANSACTION_SCHEMA.TX_ACTIVITY_SUBMIT_BRN
      },
      txSubmitBRN: {
        merchant: true,
        realm: true,
        // sign: true,
        method: Activity.txSubmitBRN,
        description: "Submit BRN",
        schema: Schema.TRANSACTION_SCHEMA.TX_ACTIVITY_SUBMIT_BRN
      },
      txActivitySubmitVPA: {
        private: true, //@ToDo Make it private
        method: Activity.txActivitySubmitVPA,
        description: "Submit VPA",
        schema: Schema.TRANSACTION_SCHEMA.TX_ACTIVITY_SUBMIT_VPA
      },
      txActivityApprove: {
        private: true, //@ToDo Make it private
        method: Activity.txActivityApprove,
        description: "Approve Transaction",
        schema: Schema.TRANSACTION_SCHEMA.TX_ACTIVITY_APPROVE
      },
      txActivityPending: {
        private: true, //@ToDo Make it private
        method: Activity.txActivityPending,
        description: "Pending Transaction",
        schema: Schema.TRANSACTION_SCHEMA.TX_ACTIVITY_APPROVE
      },
      txActivityReverse: {
        private: true, //@ToDo Make it private
        method: Activity.txActivityReverse,
        description: "Pending Transaction",
        schema: Schema.TRANSACTION_SCHEMA.TX_ACTIVITY_APPROVE
      },
      txActivityReject: {
        private: true, //@ToDo Make it private
        method: Activity.txActivityReject,
        description: "Reject Transaction",
        schema: Schema.TRANSACTION_SCHEMA.TX_ACTIVITY_REJECT
      },
      getTxDetails: {
        private: true,
        method: Activity.getTxDetails,
        description: "Get Transaction",
        schema: Schema.TRANSACTION_SCHEMA.GET_TX_DETAILS
      },
      upiPayment: {
        method: Freecharge.upiPayment,
        schema: {
          type: "object",
          properties: {
            merchantId: {
              type: "string",
              minLength: 12,
              maxLength: 20
            },
            requestId: {
              type: "string",
              minLength: 1,
              maxLength: 36
            },
            orderId: {
              type: "string",
              minLength: 1,
              maxLength: 36
            },
            orderDesc: {
              type: "string",
              maxLength: 150
            },
            amount: {
              type: "string"
            },
            payerVpa: {
              type: "string",
              minLength: 1,
              maxLength: 50
            }
          },
          required: ["merchantId", "requestId", "orderId", "amount", "payerVpa"]
        },
        description: "Initiate freecharge UPI Payment"
      },
      upiRefund: {
        method: Freecharge.upiRefund,
        schema: {
          type: "object",
          properties: {
            merchantId: {
              type: "string",
              minLength: 12,
              maxLength: 20
            },
            requestId: {
              type: "string",
              minLength: 1,
              maxLength: 36
            },
            paymentTxId: {
              type: "string",
              minLength: 1,
              maxLength: 36
            },
            amount: { type: "string" }
          },
          required: ["merchantId", "requestId", "paymentTxId", "amount"]
        },
        description: "Initiate freecharge UPI Refund"
      },
      getUpiTxDetails: {
        method: Freecharge.getUpiTxDetails,
        schema: {
          type: "object",
          properties: {
            requestId: { type: "string" },
            txId: { type: "string" }
          }
        },
        description: "Get freecharge UPI transaction details"
      },
      getUpiMerchants: {
        method: Freecharge.getUpiMerchants,
        description: "Get freecharge bank accounts"
      },
      paymentStatus: {
        method: Freecharge.paymentStatus,
        description: "Post payment status"
      },
      sendWebhook: {
        method: Webhook.sendWebhook,
        description: "call Webhook",
        schema: {
          type: "object",
          properties: {
            transfer_id: {
              type: "string",
              format: "uuid"
            },
            type: {
              type: "string"
            }
          },
          required: ["transfer_id", "type"]
        }
      },
      syncBankTransactions: {
        realm: true,
        adminGet: true,
        method: BankTx.syncBankTransactions,
        description: "Sync Bank Transactions with System",
        schema: {
          type: "object",
          properties: {
            acc_no: {
              type: "object",
              properties: {
                acc_no: { type: "string" },
                bank_host: { type: "string" },
                name: { type: "string" }
              },
              required: ["acc_no", "bank_host", "name"]
            },
            txs: {
              type: "array",
              items: {
                type: "object",
                properties: {
                  row_id: { type: "number" },
                  date: { type: "string" },
                  narration: { type: "string" },
                  amount: { type: "number" }
                }
              }
            }
          },
          required: ["txs", "acc"]
        }
      },
      directDepositTrigger: {
        method: directDeposit.directDepositTrigger,
        schema: Schema.TRANSACTION_SCHEMA.DIRECT_DEPOSIT_TRIGGER,
        description: "Process transaction"
      },
      settlementTrigger: {
        private: true,
        method: Collection.settlementTrigger,
        schema: {
          type: "object",
          properties: {
            merchant_account_id: {
              type: "string"
            },
            src_currency: {
              type: "string"
            },
            dst_currency: {
              type: "string"
            },
            src_amount: {
              type: "number"
            },
            dst_amount: {
              type: "number"
            },
            markup_rate: {
              type: "string"
            },
            conversion_rate: {
              type: "string"
            }
          },
          required: [
            "merchant_account_id",
            "src_currency",
            "dst_currency",
            "src_amount",
            "dst_amount"
          ]
        },
        description: "Process settlement transaction"
      },
      txActivitySavePayerDetails: {
        method: activity.txActivitySavePayerDetails,
        schema: {
          name: {
            type: "string"
          },
          phone: {
            type: "string"
          },
          email: {
            type: "string"
          },
          vpa: {
            type: "string"
          }
        },
        description: "Process transaction"
      },
      callTelegramBot: {
        method: telegramBot.callTelegramBot,
        schema: {
          type: "object",
          properties: {
            transfer_id: {
              type: "string"
            },
            action_type: {
              type: "string"
            },
            status: {
              type: "string"
            },
            activity: {
              type: "string"
            }
          },
          required: ["transfer_id", "action_type", "status", "activity"]
        },
        description: "Send message on telegram"
      },
      changePayoutSourceBankAccount: {
        method: activity.changePayoutSourceBankAccount,
        description: "change payout source bank account"
      },
      txActivitySaveReceipt: {
        method: activity.txActivitySaveReceipt,
        schema: {
          type: "object",
          properties: {
            transfer_id: {
              type: "string",
              format: "uuid"
            },
            url: {
              type: "string"
            }
          },
          required: ["transfer_id", "url"]
        }
      }
    };
  }

  async ping() {
    console.log("ping");
    return { "test-pong": true };
  }
  async doTransfer(data, service) {
    return await Transfer.transfer(data, service);
  }
}
