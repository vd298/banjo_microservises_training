import express from "express";
import FileProvider from "@lib/fileprovider";
import bodyParser from "body-parser";
import Queue from "@lib/queue";
import MemStore from "@lib/memstore";
import cors from "cors";
import config from "@lib/config";
import { capture } from "@lib/log";
import multer from "multer";
import multerMinIOStorage from "multer-minio-storage";
import { v4 as uuidv4 } from "uuid";
import s3 from "./s3Config";
capture();

const app = express();

var limits = {
  files: 1,
  fileSize: 12 * 1024 * 1024
};

const fileUploadErrorHandler = (err, req, res, next) => {
  if (err) {
    res.json({ success: false, err });
  } else {
    next();
  }
};

const upload = multer({
  storage: multer.memoryStorage(),
  limits
});

// var upload = multer({
//   storage: multerMinIOStorage({
//     minioClient: FileProvider.minioClient,
//     bucket: "upload",
//     metadata: function(req, file, cb) {
//       cb(null, { fieldName: fi          le.fieldname });
//     },
//     key: function(req, file, cb) {
//       cb(null, uuidv4());
//     }
//   }),
//   limits: limits
// });

app.use(cors());

app.use(bodyParser.json({ limit: "12mb" }));
app.use(
  bodyParser.urlencoded({ limit: "12mb", extended: true, parameterLimit: 8000 })
);

app.get("/download/:code", async (req, res) => {
  downloadFile(req, res, true);
});

app.get("/client/download/:code/:userToken", async (req, res) => {
  userDownloadFile(req, res);
});

app.get("/download/:code/:userToken", async (req, res) => {
  downloadFile(req, res, false);
});

app.post(
  "/upload",
  upload.single("file"),
  fileUploadErrorHandler,
  validateRealm,
  validateUserToken,
  async function(req, res) {
    req.file.owner = req.validUserToken.result.user_id;
    if (req.file) {
      req.file.isPushed = true;
      try {
        let fileUploaded = await FileProvider.pushMultipart(req.file);
        res.json(fileUploaded);
      } catch (error) {
        res.json({ success: false, message: error });
      }
    } else {
      res.json({ success: false, message: "File could not be uploaded" });
    }
  }
);

async function validateRealm(req, res, next) {
  try {
    const token = req.headers.authorization.replace(/bearer/i, "").trim();
    const realm = await Queue.newJob("auth-service", {
      method: "getServerByToken",
      data: {
        token
      }
    });
    if (realm.result) {
      req.realmId = realm.result.id;
    } else {
      res.json({ success: false, message: "Invalid realm" });
    }
  } catch (error) {
    console.log(
      "File: file-gate/src/index.js, Func: validateRealm, err:",
      error,
      "token: ",
      req.headers.authorization
    );
    res.json({ success: false, message: error });
  }
  next();
}

async function validateUserToken(req, res, next) {
  try {
    const validUserToken = await Queue.newJob("auth-service", {
      method: "validateUserToken",
      data: {},
      options: {
        realmId: req.realmId,
        header: {
          token: req.headers.token,
          account: req.headers.account
        }
      }
    });
    if (validUserToken.result) {
      req.validUserToken = validUserToken;
    } else {
      res.json({ success: false, message: "Invalid user" });
    }
  } catch (error) {
    console.log(
      "File: file-gate/src/index.js, Func: validateUserToken, err:",
      err,
      "token: ",
      req.headers.token,
      "account: ",
      req.headers.account
    );
    res.json({ success: false, message: error });
  }
  next();
}

async function downloadFile(req, res, withoutUserToken) {
  if (!withoutUserToken) {
    // const userId = await checkUserPermissions(req.params.userToken);
    // if (!userId) {
    //   res.send({ error: "ACCESSDENIEDFORUSER" });
    // }
  }

  let file = await FileProvider.getContent(req.params);
  res.set(
    "Content-Disposition",
    `attachment; filename=${encodeURI(file.meta.filename)}`
  );
  file.data.stream.pipe(res);
  file.data.stream.on("end", () => {
    res.end();
  });
}
async function userDownloadFile(req, res) {
  const userId = await MemStore.get(`usr-pdf${req.params.userToken}`);
  if (!userId) {
    return res.send({ error: "ACCESSDENIEDFORUSER" });
  }
  let file = await FileProvider.getContent(req.params);
  res.set(
    "Content-Disposition",
    `attachment; filename=${encodeURI(file.meta.filename)}`
  );
  file.data.stream.pipe(res);
  file.data.stream.on("end", () => {
    MemStore.del(`usr-pdf${req.params.userToken}`);
    res.end();
  });
}

app.post(
  "/upload-receipt",
  upload.single("file"),
  fileUploadErrorHandler,
  validateRealm,
  async (req, res) => {
    try {
      const params = {
        Bucket: process.env.BUCKET_NAME,
        Key: req.file.originalname,
        Body: req.file.buffer
      };

      const data = await s3.upload(params).promise();
      const logoUrl = data.Location;

      res.json({ success: true, logoUrl });
    } catch (error) {
      console.error(error);
      res.json({ success: false, message: error.message });
    }
  }
);

async function checkUserPermissions(userToken) {
  const userId = await MemStore.get(`usr${userToken}`);
  if (userId) {
    await MemStore.set(`usr${userToken}`, userId, config.user_token_lifetime);
  }
  return userId;
}

if (
  !process.env.NODE_ENV ||
  !["test", "localtest"].includes(process.env.NODE_ENV)
) {
  const server = app.listen(8012, () => {
    console.log("File-gate 1 is running at %s", server.address().port);
  });
}

export default app;
