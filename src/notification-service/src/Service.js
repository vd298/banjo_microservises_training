import Base from "@lib/base";
import index from "./lib";

export default class Service extends Base {
  publicMethods() {
    return {
      serviceDescription: {},
      ping: {
        description: "Test ping-pong method",
        schema: {
          type: "object",
          properties: {
            text: { type: "string" },
            num: { type: "number" },
          },
          required: ["text"],
        },
        transform(data) {
          return data;
        },
      },
      publishMethodsForTest: {
        method: index.publishMethodsForTest,
      },
      getPublicMethods: {
        realm: true,
        description: "getPublicMethods",
      },
    };
  }

  async ping() {
    console.log("ping");
    return { "test-pong": true };
  }
}

index.subscribeEvents();
//index.subscribeTelegram();
