import Queue from "@lib/queue";
import db from "@lib/db";
import config from "@lib/config";
import { Op } from "sequelize";
import TelegramBot from "node-telegram-bot-api";
const pug = require("pug-async");
const pugOptions = {
  base_url: config.app_url,
  admin_url: config.adminBaseUrl,
  plugins: [
    {
      resolve: (filename) => filename,
      read: async (filename) => {
        const letterDetails = await db.letter.findOne({
          where: {
            code: filename.split(".pug")[0]
          },
          attributes: ["html"]
        });
        if (letterDetails) {
          return letterDetails.html;
        }
        throw new Error("Unknown template" + filename);
      }
    }
  ]
};
const DEFAULT_OPTIONS = {
  send_email: true,
  send_sms: true,
  send_push: true,
  email_subject: null,
  to_email: null,
  to_phone: null
};
async function subscribeEvents() {
  Queue.subscribe(
    "NOTIFY.*",
    async ({ user_id = null, payload = {}, options = {} }, p1, topic) => {
      topic = topic.split("NOTIFY.").pop();
      handleTemplatedSend({ payload, options, topic }, null, user_id);
    }
  );
}
async function handleTemplatedSend(
  { payload = {}, options = {}, topic },
  realmId,
  userId
) {
  options = { ...DEFAULT_OPTIONS, ...options };
  let user, templates;
  if (userId || options.credentialId) {
    user = await fetchUserData(userId, options.credentialId);
  }
  try {
    templates = await fetchTemplateData(topic, payload, options, user);
  } catch (err) {
    console.error(
      "Error while getting template data",
      err,
      topic,
      payload,
      user
    );
    return;
  }
  templates.forEach((template) => {
    // Send Email
    if (template.template.send_email && options.send_email) {
      let to,
        bcc = template.template.to_bcc,
        cc = template.template.to_cc;
      if (template.template.category == "5000") {
        to = template.template.to_email || options.to_email;
      } else {
        to = user ? user.email : options.to_email;
      }
      if (!(to || bcc || cc)) {
        console.log("Empty to,cc,bcc", template.template.id);
        return;
      }
      Queue.newJob("mail-service", {
        method: "sendV2",
        data: {
          html: template.email_html,
          text: template.email_text,
          subject: template.email_subject,
          from: template.template.from_email,
          transporter: template.template.transporter,
          to,
          cc,
          bcc,
          attachments: payload.attachments || undefined
        },
        realmId,
        userId: user ? user.id : null
      });
    }
    if (template.template.send_sms && options.send_sms) {
      let mobile;
      if (template.template.category == "5000") {
        mobile = template.template.to_mobile;
      } else {
        mobile = user ? user.phone : options.to_phone;
      }
      if (!mobile) {
        console.log("Empty mobile", template.template.id);
        return;
      }
      Queue.newJob("sms-service", {
        method: "sendV2",
        data: {
          text: template.sms_text,
          mobile
        },
        realmId,
        userId: user ? user.id : null
      });
    }
    if (user && template.template.send_push && options.send_push) {
      Queue.newJob("push-service", {
        method: "sendV2",
        data: {
          push_title: template.push_title,
          push_message: template.push_message,
          push_action: template.template.push_action,
          user,
          payload
        },
        realmId,
        userId: user.id
      });
    }
  });
}

async function fetchUserData(userId, credentialId = null) {
  let user;
  if (credentialId && !userId) {
    user = await db.credential.findOne({
      where: { id: credentialId },
      attributes: ["email", "phone"]
    });
  } else {
    user = await db.user.findOne({
      where: { id: userId },
      attributes: ["id", "email", "mobile", "first_name", "last_name"]
    });
  }
  if (user && !credentialId) {
    user = user.toJSON();
    user.name =
      user.type == 0 ? `${user.first_name} ${user.last_name}` : user.legalname;
    user.business_account = user.type !== 0;
  } else {
    user = user.toJSON();
  }
  return user;
}

async function fetchTemplateData(code, payload, options, user) {
  const templates = await getTemplates(code, payload, options);
  const out = [];
  const render = async function(text, isText = true) {
    if (!text) {
      return "";
    }
    let templatedText;
    if (isText) {
      templatedText = await pug.render("p " + text, {
        ...pugOptions,
        ...payload,
        user: user || {}
      });
      templatedText = templatedText.replace(/<[^>]{1,}>/g, "");
    } else {
      templatedText = await pug.render(text, {
        ...pugOptions,
        ...payload,
        user
      });
    }
    return templatedText;
  };
  for (const template of templates) {
    const [
      email_html,
      email_text,
      email_subject,
      sms_text,
      push_title,
      push_message
    ] = await Promise.all([
      render(template.html, false),
      render(template.text),
      render(options.email_subject || template.subject),
      render(template.sms_text),
      render(template.push_title),
      render(template.push_message)
    ]);
    out.push({
      email_html,
      email_text,
      email_subject,
      sms_text,
      push_title,
      push_message,
      template
    });
  }
  return out;
}

async function getTemplates(code, payload, options) {
  const attributes = [
    "id",
    "from_email",
    "to_email",
    "subject",
    "text",
    "html",
    "transporter",
    "data",
    "sms_gateway",
    "to_cc",
    "to_bcc",
    "to_mobile",
    "send_push",
    "send_email",
    "send_sms",
    "sms_text",
    "push_title",
    "push_message",
    "push_action",
    "category"
  ];
  const lagOrCondition = [{ lang: "en" }];
  if (options && options.lang && options.lang != "en") {
    lagOrCondition.push({ lang: options.lang });
  }
  const letters = await db.letter.findAll({
    where: {
      code,
      lang: {
        [Op.or]: lagOrCondition.map((item) => item["lang"])
      }
    },
    attributes
  });
  if (!letters.length) {
    throw "MAILTEMPLATENOTFOUND";
  }
  for (const letter of letters) {
    if (!letter.data) {
      await db.letter.update(
        { data: JSON.stringify(payload, null, 4) },
        { where: { id: letter.id } }
      );
    }
  }

  return letters;
}
async function fetchData(data) {
  try {
    let whereCondition = "";
    if (data.startDate && data.endDate) {
      let startDate, endDate;
      if (data.startDate && data.endDate) {
        startDate = new Date(data.startDate);
        endDate = new Date(data.endDate);
      }
      startDate.setHours(0, 0, 0, 0);
      endDate.setHours(23, 59, 59, 999);

      const startDateString = startDate
        .toISOString()
        .replace("T", " ")
        .replace("Z", " +0530");
      const endDateString = endDate
        .toISOString()
        .replace("T", " ")
        .replace("Z", " +0530");

      whereCondition = ` WHERE t.ctime>= '${startDateString}' AND ctime<= '${endDateString}' AND t.mtime>= '${startDateString}' `;
    }
    if (whereCondition) {
      whereCondition += ` AND t.src_currency in('INR') `;
    } else {
      whereCondition = ` WHERE t.src_currency in('INR') `;
    }

    let query = `select
        t.transfer_type AS "Transaction Type",
        COUNT(*) AS "Count",
        t.activity AS "Activity",
        ROUND(CAST(SUM(t.src_amount) AS numeric), 2) as "Total",
        ROUND(CAST(SUM(t.src_amount) / ${config.usdRate} AS numeric), 2) AS "Total(USD)"
      from
        ${db.schema}.vw_tx_transfer t
      ${whereCondition} 
      group by
        t.activity,
        t.transfer_type
      order by
        t.transfer_type;`;

    let transitions = await db.sequelize.query(query, {
      raw: true,
      type: db.sequelize.QueryTypes.SELECT
    });
    if (transitions && transitions.length) {
      const result = [];
      for (const obj of transitions) {
        const innerArray = [];
        for (const value of Object.values(obj)) {
          innerArray.push(value);
        }
        result.push(innerArray);
      }
      return result;
    }
  } catch (error) {
    console.error("File:notification-service.js Fun:fetchData Error:", error);
  }
}

async function fetchMaterializedViedData(viewName) {
  try {
    let query = `select * from ${db.schema}.${viewName}`;

    let transitions = await db.sequelize.query(query, {
      raw: true,
      type: db.sequelize.QueryTypes.SELECT
    });
    if (transitions && transitions.length) {
      const result = [];
      for (const obj of transitions) {
        const innerArray = [];
        for (const value of Object.values(obj)) {
          innerArray.push(value);
        }
        result.push(innerArray);
      }
      return result;
    }
  } catch (error) {
    console.error(
      "File:notification-service.js Fun:fetchMaterializedViedData Error:",
      error
    );
  }
}
function tableStructure(data) {
  try {
    if (data && data.length) {
      const tableData = [
        ["Type", "Count", "Status", "Total(INR)", "Converted(USD)"]
      ].concat(data);

      const addPadding = (cell, maxLength) => {
        const padding = " ".repeat(maxLength - cell.length);
        return cell + padding;
      };

      const maxLengths = tableData[0].map((_, colIndex) =>
        Math.max(...tableData.map((row) => row[colIndex].toString().length))
      );

      const tableMarkdown = tableData
        .map(
          (row) =>
            "|" +
            row
              .map((cell, index) => {
                if (index === row.length - 1) {
                  return addPadding(cell.toString(), maxLengths[index]);
                } else {
                  return addPadding(cell.toString(), maxLengths[index]) + " ";
                }
              })
              .join("|") +
            "|"
        )
        .join("\n");
      return `Statistics: \n\`\`\`\n${tableMarkdown}\n\`\`\``;
    } else {
      return `No transaction activity found`;
    }
  } catch (error) {
    console.error(
      "File:notification-service.js Fun:tableStructure Error:",
      error
    );
  }
}
function subscribeTelegram() {
  try {
    console.error("File:notification-service.js Fun:subscribeTelegram ");
    const token = process.env.TELEGRAM_TOKEN;
    const bot = new TelegramBot(token, { polling: true, interval: 1000 });
    bot.onText(/\/year@EnovateIT_Bot/, async (msg) => {
      const chatId = msg.chat.id;
      if (chatId === config.telegramBotId) {
        let message = `**Command:** 'year@EnovateITBot' \n`;
        const now = new Date();
        const hours = String(now.getHours()).padStart(2, "0");
        const minutes = String(now.getMinutes()).padStart(2, "0");
        const currentTime = `${hours}:${minutes}`;
        message += `Command Timestamp: ${currentTime} \n`;
        // message += `Statistics: \n`;

        const data = await fetchMaterializedViedData(
          `mvw_fetch_one_year_transactions_activity`
        );
        const tableMarkdown = await tableStructure(data);

        message += tableMarkdown;

        bot.sendMessage(chatId, message, {
          parse_mode: "Markdown"
        });
      }
    });
    bot.onText(/\/today@EnovateIT_Bot/, async (msg) => {
      const chatId = msg.chat.id;
      if (chatId === config.telegramBotId) {
        let message = `**Command:** 'today@EnovateITBot' \n`;
        const now = new Date();
        const hours = String(now.getHours()).padStart(2, "0");
        const minutes = String(now.getMinutes()).padStart(2, "0");
        const currentTime = `${hours}:${minutes}`;
        message += `Command Timestamp: ${currentTime} \n`;

        const endDate = new Date();
        const startDate = new Date();

        const data = await fetchData({ startDate, endDate });
        const tableMarkdown = await tableStructure(data);

        message += tableMarkdown;

        bot.sendMessage(chatId, message, {
          parse_mode: "Markdown"
        });
      }
    });
    bot.onText(/\/yday@EnovateIT_Bot/, async (msg) => {
      const chatId = msg.chat.id;
      if (chatId === config.telegramBotId) {
        let message = `**Command:** 'yday@EnovateITBot' \n`;
        const now = new Date();
        const hours = String(now.getHours()).padStart(2, "0");
        const minutes = String(now.getMinutes()).padStart(2, "0");
        const currentTime = `${hours}:${minutes}`;
        message += `Command Timestamp: ${currentTime} \n`;
        // message += `Statistics: \n`;

        const data = await fetchMaterializedViedData(
          `mvw_fetch_yesterday_transactions_activity`
        );

        const tableMarkdown = await tableStructure(data);
        console.log("tableMarkdown", tableMarkdown);
        console.log("data", data);
        message += tableMarkdown;

        bot.sendMessage(chatId, message, {
          parse_mode: "Markdown"
        });
      }
    });
    bot.onText(/\/week@EnovateIT_Bot/, async (msg) => {
      const chatId = msg.chat.id;
      if (chatId === config.telegramBotId) {
        let message = `**Command:** 'week@EnovateITBot' \n`;
        const now = new Date();
        const hours = String(now.getHours()).padStart(2, "0");
        const minutes = String(now.getMinutes()).padStart(2, "0");
        const currentTime = `${hours}:${minutes}`;
        message += `Command Timestamp: ${currentTime} \n`;
        // message += `Statistics: \n`;

        const data = await fetchMaterializedViedData(
          `mvw_fetch_one_week_transactions_activity`
        );
        const tableMarkdown = await tableStructure(data);

        message += tableMarkdown;

        bot.sendMessage(chatId, message, {
          parse_mode: "Markdown"
        });
      }
    });
    bot.onText(/\/month@EnovateIT_Bot/, async (msg) => {
      const chatId = msg.chat.id;
      if (chatId === config.telegramBotId) {
        let message = `**Command:** 'month@EnovateITBot' \n`;
        const now = new Date();
        const hours = String(now.getHours()).padStart(2, "0");
        const minutes = String(now.getMinutes()).padStart(2, "0");
        const currentTime = `${hours}:${minutes}`;
        message += `Command Timestamp: ${currentTime} \n`;
        // message += `Statistics: \n`;

        const data = await fetchMaterializedViedData(
          `mvw_fetch_one_month_transactions_activity`
        );
        const tableMarkdown = await tableStructure(data);

        message += tableMarkdown;

        bot.sendMessage(chatId, message, {
          parse_mode: "Markdown"
        });
      }
    });
  } catch (error) {
    console.error(
      "File:notification-service.js Fun:subscribeTelegram Error:",
      error
    );
  }
}
export default {
  subscribeEvents,
  handleTemplatedSend,
  subscribeTelegram
};
