import Queue from "@lib/queue";

export default class WsConnection {
  constructor(ws, req, user_id, scope) {
    this.userId = user_id;
    this.ws = ws;
    this.subscribeMQ();
    ws.on("close", () => {
      ws.close();
      delete this;
    });
  }

  subscribeMQ() {
    Queue.subscribe("broadcast-request", (data) => {
      if (data.method == "runOnClient") {
        this.onJob(data);
      }
    });
  }

  onJob(data) {
    if (data.data.userId == this.userId) {
      this.ws.send(JSON.stringify(data));
    }
  }
}
