/**
 * Class Request incapsules separate request
 */

console.log("NODE_ENV", process.env.NODE_ENV);

import config from "@lib/config";
import Queue from "@lib/queue";
import MemStore from "@lib/memstore";
import ipLib from "ip";
import FileProvider from "@lib/fileprovider";
import { ValidationError } from "@lib/error";
import { log } from "@lib/log";
import UAParser from "ua-parser-js";
import shared from "@lib/shared";
const Crypto = shared.Crypto;

const MAX_SIZE = config.limit_size || 1024 * 1024 * 5;
const MAX_QUEUE_SIZE = 1024 * 500;

export default class Request {
  constructor(request, response, next, server) {
    const parser = new UAParser(request.headers["user-agent"]);
    this.request = request;
    this.response = response;
    this.requestId = null;
    this.next = next;
    this.server = server;
    this.ua = parser.getResult();
  }

  error(error) {
    if (this.isSent) return;
    this.isSent = true;
    //if (!!errorReport) console.error(errorReport);
    let err;
    if (error && error.toJSON) {
      err = error.toJSON();
    } else {
      err = {
        code: error.code,
        message: error.message,
        title: error.title,
        dev_message: error.dev_message,
        category: error.category,
      };
    }
    const responseBody = {
      header: {
        id: this.requestId,
        status: "ERROR",
      },
      error: err,
    };
    this.response.send(responseBody);
    this.log(true, responseBody);
    Queue.publish(`NOTIFY.ERROR-EMAIL`, {
      user_id: null,
      options: {
        send_sms: false,
        send_push: false,
      },
      payload: error,
    });
  }
  getIp() {
    return (
      this.request.headers["x-real-ip"] ||
      this.request.headers["x-forwarded-for"] ||
      this.request.connection.remoteAddress
    );
  }
  send(data) {
    if (this.isSent) return;
    this.isSent = true;
    let sign,
      defaultStatus = `OK`;
    if (this.signPublicKey) {
      sign = this.signResponse({ ...data }, this.signPublicKey);
    }
    //Vaibhav Vaidya, if data has success flag that is false or negative then update status accordingly.
    if (
      data &&
      data.success != undefined &&
      (data.success === false || (!isNaN(data.success) && data.success <= 0))
    ) {
      defaultStatus = `ERROR`;
    }
    this.response.send({
      header: {
        sign,
        id: this.requestId,
        status: defaultStatus,
      },
      data,
    });
  }

  checkSize(data) {
    if (!data) return true;
    const size = JSON.stringify(data).length;
    return size <= MAX_QUEUE_SIZE;
  }

  async checkNonce(nonce) {
    const res = await MemStore.get(`nonce:${nonce}`);
    if (res) return true;
    await MemStore.set(`nonce:${nonce}`, 1, 3);
    return false;
  }

  async run() {
    let size = this.request.headers["content-length"];

    if (size > MAX_SIZE) throw "STACKOVERFLOW";
    if (/\/rest\//.test(this.request.path))
      return await this.do(this.transformRestRequest());

    return await this.do(this.request.body);
  }

  transformRestRequest() {
    const path = this.request.path.split("/");

    if (path[path.length - 1] && path[path.length - 2]) {
      if (this.request.query.bearer) {
        this.request.headers.authorization =
          "bearer" + this.request.query.bearer;
      }
      return {
        header: {
          id: this.request.query.id,
          lang: this.request.query.lang || "eng",
          version: this.request.query.version,
          service: path[path.length - 2],
          method: path[path.length - 1],
          nonce: this.request.query.nonce || "",
          token: this.request.query.token || null,
        },
        data: this.request.body,
      };
    }
    throw "RESTREQUESTFORMATERROR";
  }

  async do(data) {
    let server, userId, accountId, maId, isAdminUser, userDetail;
    console.log(`Received Requests`, this.request.headers.authorization);
    server = await this.checkServerToken(
      this.request.headers.authorization || ""
    );
    data.realm = server.id;

    if (server.cors && server.cors._arr) {
      await this.server.cors(server.cors._arr, this.request, this.response);
    } else {
      await this.server.cors(
        [{ option: "origin", value: "*" }],
        this.request,
        this.response
      );
    }

    try {
      this.checkInputData(data);
    } catch (err) {
      this.error(err);
      return;
    }

    if (data.header.nonce && (await this.checkNonce(data.header.nonce))) {
      return this.error(
        new ValidationError("BANJO_ERR_NONCE", {
          lang: data.header.lang,
        })
      );
    }

    if (!this.server.services[data.header.service]) {
      return this.error(
        new ValidationError("BANJO_ERR_SERVICE", {
          lang: data.header.lang,
          data: { service: data.header.service },
        })
      );
    }
    const serviceMethod = this.server.services[data.header.service][
      data.header.method
    ];
    if (!serviceMethod) {
      return this.error(
        new ValidationError("BANJO_ERR_METHOD", {
          lang: data.header.lang,
          data: { method: data.header.method },
        })
      );
    }

    if (serviceMethod.realm) {
      server = await this.checkServerPermissions(
        server,
        this.request.headers.authorization,
        data.header
      );
      console.log("printing server permission");
      console.log(server.permissions);
      if (!server) {
        return this.error(
          new ValidationError("BANJO_ERR_ACCESSDINED", {
            lang: data.header.lang,
            data: {
              service: data.header.service,
              method: data.header.method,
            },
          })
        );
      }

      if (server.ip && !this.checkRequestIp(server.ip, this.request)) {
        return this.error(
          new ValidationError("BANJO_ERR_IP_DENIED", {
            lang: data.header.lang,
          })
        );
      }

      if (
        server.domain &&
        !this.checkRequestDomain(server.domain, this.request)
      ) {
        return this.error(
          new ValidationError("BANJO_ERR_HOST_DENIED", {
            lang: data.header.lang,
          })
        );
      }
    }
    if (serviceMethod.merchant) {
      const result = await this.checkMerchantPermissions(
        data,
        server.id,
        serviceMethod.sign
      );
      if (!(result && result.user_id)) {
        return this.error(
          new ValidationError("BANJO_ERR_ACCESSTOKEN", {
            lang: data.header.lang,
          })
        );
      }
      userId = result.user_id;
      accountId = result.account_id;
      maId = result.ma_id;
    }

    if (
      this.server.services[data.header.service][data.header.method].adminGet
    ) {
      const result = await this.checkAdminPermissions(data, server.id);
      if (!(result && result.user_id)) {
        return this.error(
          new ValidationError("BANJO_ERR_ACCESSTOKEN", {
            lang: data.header.lang,
          })
        );
      }
      userId = result.user_id;
      isAdminUser = true;
    }
    if (
      this.server.services[data.header.service][data.header.method].user ||
      this.server.services[data.header.service][data.header.method].account
    ) {
      data.data.account_check = !!this.server.services[data.header.service][
        data.header.method
      ].account;
      const result = await this.checkUserPermissions(data, server.id);
      if (!(result && result.user_id)) {
        return this.error(
          new ValidationError("BANJO_ERR_ACCESSTOKEN", {
            lang: data.header.lang,
          })
        );
      }
      userId = result.user_id;
      accountId = result.account_id;
      if (
        this.server.services[data.header.service][data.header.method].kyc &&
        !(await this.checkUserKyc(userId))
      ) {
        return this.error(
          new ValidationError("BANJO_ERR_ACCOUNT_DISABLED", {
            lang: data.header.lang,
          })
        );
      }
    }

    if (data.data && data.data.files && Array.isArray(data.data.files)) {
      data.data.files = await this.prepareFiles(data.data.files);
    }

    if (!this.checkSize(data.data)) {
      return this.error(
        new ValidationError("BANJO_ERR_STKOVERFLOW", {
          lang: data.header.lang,
        })
      );
    }

    if (
      this.server.services[data.header.service][data.header.method].otp &&
      !data.data.test
    ) {
      data.userId = userId;
      data.realmId = server.id;
      data.accountId = accountId;
      data.isAdminUser = !!isAdminUser;
      data.maId = maId;
      data.parentRealmId = server.pid || server.id;
      return await this.sendOtp(data);
    }

    if (!data) data = {};
    if (!data.data) data.data = {};
    data.httpHeaders = this.request.headers;
    data.ip = this.getIp();

    const resultData = await this.doJob(
      data.header.service,
      data.header.method,
      data.data,
      {
        isAdminUser: data.isAdminUser,
        header: data.header,
        httpHeaders: this.request.headers,
        userId,
        accountId: accountId,
        maId,
        realmId: server ? server.id : null,
        parentRealmId: server ? server.pid || server.id : null,
      }
    );
    if (resultData) {
      if (data.data && data.data.files) {
        await FileProvider.accept(data.data.files);
      }
      this.send(resultData);
      this.log(false, resultData);
    } else {
      if (data.data && data.data.files)
        await this.removeTemplatedFiles(data.data.files);

      this.send(resultData);
    }
    this.send(resultData);
  }

  checkRequestIp(allowedIP, request) {
    const ip = this.getIp();

    if (!ip) return false;
    return ipLib.isEqual(ip, allowedIP);
  }

  checkRequestDomain(allowedDomain, request) {
    let host = request.headers["origin"];
    if (!host) return false;
    host = host.split("/")[2];
    return host == allowedDomain;
  }

  checkInputData(data) {
    if (!data || typeof data !== "object" || !this.checkHeader(data.header)) {
      return this.error(new ValidationError("BANJO_ERR_INVALID_REQUEST", {}));
    }

    if (!this.checkVersion(data.header.version)) {
      throw new ValidationError("BANJO_ERR_VERSION", {
        data: {
          version: data.header.version,
          current_version: config.apiVersion,
        },
        lang: data.header.lang,
      });
    }
    if (data.header.lang && !this.checkLanguage(data.header.lang)) {
      throw new ValidationError("BANJO_ERR_LANGUAGE_SUPPORT", {
        data: { codes: config.locales.join() },
      });
    }

    return true;
  }

  checkHeader(header) {
    if (!header) return false;
    if (!header.version) return false;
    if (!header.service) return false;
    if (!header.method) return false;
    this.requestId = header.id;
    return true;
  }

  checkVersion(version) {
    return config.apiVersion == version;
  }
  checkLanguage(lang) {
    return config.locales.includes(lang);
  }

  async checkServerPermissions(serverObject, token, header) {
    if (
      serverObject &&
      this.checkServicePermissions(serverObject, header.service, header.method)
    ) {
      return serverObject;
    }
    return false;
  }

  checkServicePermissions(server, service, method) {
    return (
      server.permissions &&
      server.permissions[service] &&
      server.permissions[service][method]
    );
  }

  async checkServerToken(token) {
    let serverObject, res;
    const tokenCode = token.replace(/bearer/i, "").trim();
    try {
      res = await MemStore.get(`srv${tokenCode}`);
      serverObject = JSON.parse(res);
    } catch (e) {
      return false;
    }

    if (!serverObject) {
      console.log(`Searching by `, tokenCode);
      serverObject = await this.getServerByToken(tokenCode);
      console.log(`Logging ServerOjb`, serverObject);
      if (serverObject) {
        await MemStore.set(`srv${tokenCode}`, JSON.stringify(serverObject));
      }
    }
    return serverObject || false;
  }

  async checkUserKyc(id) {
    const resultData = await this.doJob("auth-service", "getUserKycStatus", {
      id,
    });
    return !!resultData.kyc;
  }

  // get server id by token in db
  async getServerByToken(token) {
    const resultData = await this.doJob("auth-service", "getServerByToken", {
      token,
    });

    if (!resultData || !resultData.id) return null;
    return resultData;
  }

  checkUserIP(IPs) {
    if (!IPs || IPs == "undefined") return true;
    if (!IPs || !this.request.headers["x-real-ip"]) return true;
    for (let ip of IPs.split(",")) {
      if (ipLib.isEqual(this.getIp(), ip)) return true;
    }
    return false;
  }

  async checkAdminPermissions(data, realmId) {
    // check authorized user by token
    const resultData = await this.doJob(
      "auth-service",
      "validateAdminToken",
      data.data,
      {
        realmId,
        header: data.header,
        httpHeaders: this.request.headers,
        ip: this.getIp(),
      }
    );
    return resultData;
  }
  async checkUserPermissions(data, realmId) {
    // check authorized user by token
    const resultData = await this.doJob(
      "auth-service",
      "validateUserToken",
      data.data,
      {
        realmId,
        header: data.header,
        httpHeaders: this.request.headers,
        ip: this.getIp(),
      }
    );
    return resultData;
  }

  async checkMerchantPermissions(data, realmId, sign) {
    // check authorized merchant by token
    const resultData = await this.doJob(
      "auth-service",
      "validateMerchantToken",
      data.data,
      {
        sign,
        realmId,
        header: data.header,
        httpHeaders: this.request.headers,
        ip: this.getIp(),
      }
    );
    if (sign) {
      this.signPublicKey = resultData ? resultData.publicKey : undefined;
    }
    return resultData;
  }

  async doJob(service, method, data, options = {}) {
    const res = await Queue.newJob(service, {
      method,
      data,
      options: { ...options, scope: "gate", ua: this.ua },
    });

    if (res.error) {
      this.error(res.error);
      return null;
    } else {
      return res.result;
    }
  }

  signResponse(responseJson, publicKey) {
    return Crypto.createSignatureByPublicKey(
      JSON.stringify(responseJson),
      publicKey
    );
  }
  async prepareFiles(files) {
    let fileData,
      out = [];
    try {
      for (let i = 0; i < files.length; i++) {
        fileData = await FileProvider.push(
          files[i],
          config.new_file_hold_timeout || 300
        );
        if (fileData && fileData.success) {
          out.push({
            name: files[i].name,
            code: fileData.code,
            size: fileData.size,
          });
        }
      }
    } catch (e) {
      this.error("FILEUPLOADERROR1");
    }
    return out;
  }

  async removeTemplatedFiles(files) {
    for (let i = 0; i < files.length; i++) {
      await FileProvider.del(files[i].data);
    }
  }

  async sendOtp(data) {
    const resultData = await this.doJob("auth-service", "otp", data.data, {
      header: data.header,
      httpHeaders: this.request.headers,
      userId: data.userId,
      accountId: data.accountId,
      realmId: data.realmId,
      parentRealmId: data.parentRealmId,
    });
    this.send(resultData);
  }

  async log(error, response) {
    const { headers, body } = this.secureBodyAndHeaders();
    const userId = await this.checkUserPermissions(this.request.body);
    const securedResponse = this.secureResponse(response);
    const reqHeader = body ? body.header : null;
    const reqService = reqHeader ? reqHeader.service : null;
    const reqMethod = reqHeader ? reqHeader.method : null;
    const serviceExists = this.server.services[reqService] ? reqService : null;
    const methodExists = serviceExists
      ? this.server.services[reqService][reqMethod]
        ? reqMethod
        : null
      : null;
    // const level = error ? "error" : "http";

    const method =
      reqService &&
      reqMethod &&
      this.server.services[reqService] &&
      this.server.services[reqService][reqMethod]
        ? this.server.services[reqService][reqMethod]
        : false;

    const errorCode = response
      ? response.error
        ? response.error.code
        : null
      : null;

    const message = error
      ? `Error: ${
          method ? method.description : "Wrong http request"
        } [${errorCode}]`
      : `Success: ${method ? method.description : null}`;

    if (error || (method && method.log))
      log(message, null, {
        level: "http",
        details: {
          requestId: this.requestId,
          request: {
            headers,
            body,
          },
          response: securedResponse,
        },
        profile: userId,
        process: serviceExists,
        method: methodExists,
      });
  }

  secureBodyAndHeaders() {
    const body = JSON.parse(JSON.stringify(this.request.body));
    const headers = JSON.parse(JSON.stringify(this.request.headers));

    if (headers.authorization) headers.authorization = "Bearer***";

    if (
      body.data &&
      this.server &&
      this.server.services &&
      this.server.services[body.header.service] &&
      this.server.services[body.header.service][body.header.method] &&
      this.server.services[body.header.service][body.header.method].schema &&
      this.server.services[body.header.service][body.header.method].schema
        .properties
    ) {
      const props = this.server.services[body.header.service][
        body.header.method
      ].schema.properties;

      maskBody(body.data);

      function maskBody(data) {
        for (const [key, value] of Object.entries(props)) {
          if (value.secure && data[key]) data[key] = "***";
        }
      }
    }
    return { headers, body };
  }

  secureResponse(resBody) {
    const res = JSON.parse(JSON.stringify(resBody));
    const body = this.request.body;
    if (
      body &&
      body.header &&
      this.server &&
      this.server.services &&
      this.server.services[body.header.service] &&
      this.server.services[body.header.service][body.header.method] &&
      this.server.services[body.header.service][body.header.method]
        .secureResponse
    ) {
      const resSchema = this.server.services[body.header.service][
        body.header.method
      ].secureResponse;

      maskResponse(res, resSchema);

      function maskResponse(data, schema) {
        for (const [key, value] of Object.entries(schema)) {
          if (
            typeof value === "object" &&
            data[key] &&
            typeof data[key] === "object"
          )
            maskResponse(data[key], value);
          else if (data[key] && value === true) data[key] = "***";
        }
      }
    }
    return res;
  }

  helloVinay() {
    const responseBody = {
      header: {
        id: this.requestId,
        status: "SUCCESS",
      },
      message: "Hello Vinay!",
    };
    this.response.send(responseBody);
  }
}

//export default { passStudentId };
