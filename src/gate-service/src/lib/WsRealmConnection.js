import Queue from "@lib/queue";

export default class WsRealmConnection {
  constructor(ws, req, realmToken, scope) {
    this.ws = ws;

    this.init(realmToken);
  }

  async init(token) {
    const { result: realm } = await Queue.newJob("auth-service", {
      method: "getServerByToken",
      data: {
        token
      }
    });

    if (!realm) {
      this.ws.close();
      delete this;
      return;
    }

    this.realm = realm;

    this.subscribeMQ();
    this.ws.on("close", () => {
      delete this;
    });
  }

  subscribeMQ() {
    Queue.subscribe("broadcast-request", (data) => {
      if (data.method == "runOnRealm") {
        this.onJob(data);
      }
    });
  }

  onJob(data) {
    if (data.data.realmId == this.realm.id) {
      this.ws.send(data.data.data);
    }
  }
}
