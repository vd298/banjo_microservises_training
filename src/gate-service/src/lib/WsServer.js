import WebSocket from "ws";
import WsConnection from "./WsConnection";
import WsRealmConnection from "./WsRealmConnection";
import querystring from "querystring";
import MemStore from "@lib/memstore";

export default class WsServer {
  constructor(server) {
    this.server = server;
    this.connection();
  }

  connection() {
    this.wss = new WebSocket.Server({
      server: this.server,
      clientTracking: true
    });

    this.wss.on("connection", async (ws, req) => {
      if (
        !!req.headers.authorization &&
        req.headers.authorization.substr(0, 6) == "bearer"
      ) {
        const realmToken = req.headers.authorization.substr(6).trim();
        new WsRealmConnection(ws, req, realmToken, this);
        return;
      }

      req.url = req.url.split("?")[1];
      let incomingParams = querystring.parse(req.url);
      if (!incomingParams["token"]) {
        ws.close();
        return;
      }
      const user_id = await this.getUserIdFromRequest(incomingParams["token"]);

      if (user_id && user_id.length > 0) {
        new WsConnection(ws, req, user_id, this);
      } else {
        ws.close();
      }
    });
  }
  async getRealm(tokenCode) {
    try {
      res = await MemStore.get(`srv${tokenCode}`);
      return JSON.parse(res);
    } catch (e) {
      return false;
    }
  }

  async getUserIdFromRequest(auth_token) {
    return await MemStore.get("usr" + auth_token);
  }
}
