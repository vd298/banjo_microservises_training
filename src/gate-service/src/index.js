import config from "@lib/config";
import express from "express";
import bodyParser from "body-parser";
import Server from "./lib/Server";
import Middleware from "./middleware";
import WsServer from "./lib/WsServer";
import MemStore from "@lib/memstore";
import Request from "./lib/Request";

const app = express();

app.use(bodyParser.json({ limit: "8mb" }));
app.use(
  bodyParser.urlencoded({ limit: "8mb", extended: true, parameterLimit: 8000 })
);

for (let mw in Middleware) {
  app.use(Middleware[mw].run);
}
///////////////////////////////////////////////////////////
app.get("/hello-vinay", (req, res, next) => {
  // Creating an instance of the Request class
  const requestHandler = new Request(req, res, next);
  requestHandler.helloVinay();
});
////////////////////////////////////////////////////////////////
app.use(new Server(app));
if (!config.isTestEnv()) {
  const server = app.listen(config.port, async () => {
    console.log("server is running at %s", server.address().port);
    const perms = await MemStore.keys(`srv*`);
    if (perms && perms.length) {
      MemStore.delAll("srv*");
    }

    new WsServer(server);
  });
}

export default app;
