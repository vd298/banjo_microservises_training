import chai from "chai";
import chaiHttp from "chai-http";
import pretest from "@lib/pretest";
import uuid from "uuid/v4";
import db from "@lib/db";
import { Request, Service } from "../utils";
import account from "../services/account-service";

chai.use(require("chai-uuid"));
chai.use(require("chai-like"));
chai.use(require("chai-things"));

const expect = chai.expect;
let should = chai.should();
let userToken;
let userToken2;
let adminToken;
let userId;
let emailVerificationToken, code;
let ENV;
global.userToken = null;
global.realmToken = uuid();
global.realmHost = "test.realm";

global.refUserSender = null;
global.refUserDepended = null;
let captcha;
chai.use(chaiHttp);

let service, botService;

const profile_pic =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWIAAAE2CAYAAABbQ5ibAAAAAXNSR0IArs4c6QAAIABJREFUeAHsnQd4JUeR==";

describe("Auth Service Methods", async () => {
  before(async () => {
    if (!global.ENV) {
      ENV = await pretest.before();
    } else {
      ENV = global.ENV;
    }
    global.ENV = ENV;
    service = new Service("auth-service", global.realmHost, ENV.realm.token, {
      lang: "en",
      version: "0.0.1"
    });
    botService = new Service("auth-service", "bot-realm", ENV.botRealm.token, {
      lang: "en",
      version: "0.0.1"
    });
    userToken2 = await account.loginAPI({
      email: "ap2691",
      password: "Passw0rd"
    });
  });
  after(async () => {});

  describe("Before Authorization", async () => {
    it("Signin User - Wrong Password", async () => {
      const data = {
        username: "db154",
        password: "Passw0rd2"
      };
      const res = await new Request(service).method("signin").send(data);
      res.body.header.should.have.deep.property("status", "ERROR");
    });
    it("Signin user", async () => {
      const data = {
        username: "db154",
        password: "Passw0rd"
      };
      const res = await new Request(service)
        .document(`Signin`, `Get user access token`)
        .method("signin")
        .send(data);
      userToken = res.body.data.user_token;
      res.body.header.should.have.deep.property("status", "OK");
      res.body.data.should.have.deep.property("user_token");
    });
    it("Reset password request", async () => {
      const data = {
        email: "db154+autotests@enovate-it.com"
      };
      const res = await new Request(service)
        .document(
          `Get reset password token`,
          `Get token on email for password token, incase user forgot password`
        )
        .method("resetPasswordRequest")
        .send(data);
      res.body.header.should.have.deep.property("status", "OK");
    });
    it.skip("Reset password", async () => {
      const data = {
        step: 1,
        code: "q2jcBleNEr",
        new_password: "Passw0rd@123"
      };
      const res = await new Request(service)
        .document(`Reset Password`, `Reset password using reset token`)
        .method("resetPassword")
        .send(data);
      res.body.header.should.have.deep.property("status", "OK");
    });
  });

  describe("Post Authorization", async () => {
    it("Change Password - Wrong Old Password", async () => {
      const data = {
        old_password: "Passw0rd!",
        new_password: "test@123"
      };
      const res = await new Request(service)
        .setHeader("token", userToken)
        .method("changePassword")
        .send(data);
      res.body.header.should.have.deep.property("status", "ERROR");
    });

    it("Change Password", async () => {
      const data = {
        old_password: "Passw0rd",
        new_password: "Passw0rd@123"
      };
      const res = await new Request(service)
        .setHeader("token", userToken)
        .document(`Change Password`, `Change user password`)
        .method("changePassword")
        .send(data);
      res.body.header.should.have.deep.property("status", "OK");
    });

    it("Get User Profile", async () => {
      const data = {};
      const res = await new Request(service)
        .setHeader("token", userToken)
        .method("getUserProfile")
        .send(data);
      res.body.header.should.have.deep.property("status", "OK");
    });

    it("Update User Profile", async () => {
      const data = {
        first_name: "Aditi",
        last_name: "Phalke",
        email: "ap269@enovate-it.com",
        mobile: "+9675201612",
        username: "ap269",
        timezone: "Etc/GMT+12"
      };
      const res = await new Request(service)
        .setHeader("token", userToken2)
        .method("updateUserProfile")
        .send(data);
      res.body.header.should.have.deep.property("status", "OK");
    });

    it("Email verification request", async () => {
      const data = {
        email: "ap269@enovate-it.com"
      };
      const res = await new Request(service)
        .setHeader("token", userToken2)
        .document(
          `Get email verification token`,
          `Get token on email for email verification`
        )
        .method("verifyEmailRequest")
        .send(data);
      emailVerificationToken = res.body.data.token;
      code = res.body.data.code;
      res.body.header.should.have.deep.property("status", "OK");
    });

    it("Verify Email", async () => {
      const data = {
        token: emailVerificationToken,
        code: code
      };
      const res = await new Request(service)
        .setHeader("token", userToken2)
        .document(`Verify Email`, `Verify email using verify email token`)
        .method("verifyEmail")
        .send(data);
      res.body.header.should.have.deep.property("status", "OK");
    });

    it("Upload user profile pic", async () => {
      const data = {
        file_name: "image.png",
        profile_pic: profile_pic
      };
      const res = await new Request(service)
        .setHeader("token", userToken2)
        .method("updateProfilePic")
        .send(data);
      res.body.header.should.have.deep.property("status", "OK");
      res.body.data.should.have.deep.property("success", true);
      res.body.data.should.have.property("profilePicUrl");
      res.body.data.should.have.property("profile_pic_code");
    });

    // Signup user using invite link
    it("Signup new user using invite", async () => {
      let invite = await db.user_account_invitations.findOne({
        where: {
          email: "pm261@enovate-it.com"
        }
      });

      let data = {
        token: invite.token,
        first_name: "Prashin",
        last_name: "More",
        email: invite.email,
        mobile: "9324115782",
        username: "pm261",
        password: "P@ssw0rd"
      };
      const res = await new Request(service)
        .document(
          `Signup new user using invite`,
          `Signup user using invitation token`
        )
        .method("signup")
        .send(data);
      res.body.header.should.have.deep.property("status", "OK");
    });

    it("fetch assignable roles", async () => {
      const role = await db.role.findOne({
        where: {
          role_name: "Owner"
        }
      });
      const data = {
        role_id: role.id,
        weight: role.weight
      };
      const res = await new Request(service)
        .setHeader("token", userToken2)
        .method("fetchAssignableRoles")
        .send(data);
      res.body.header.should.have.deep.property("status", "OK");
      res.body.data.should.have.deep.property("success", true);
      res.body.data.should.have.deep.property("roles");
    });
  });
  describe("Merchant Accounts", async () => {
    it("Generate Merchant API Keys", async () => {
      const data = {
        merchant_account_id: ENV.MAID1,
        key_name: "TestKey",
        webhook_url: "http://test.webhook.any/test",
        ips: [],
        permissions: []
      };
      const res = await new Request(service)
        .document("Generate API Key", "Generate API Keys for merchant accounts")
        .setHeader("token", userToken)
        .method("generateApiKeys")
        .send(data);
      res.body.header.should.have.deep.property("status", "OK");
      res.body.data.should.have.deep.property("apiKey");
      res.body.data.should.have.deep.property("secretKey");
      console.log("Sentting", JSON.stringify(res.body.data));
      ENV.TEST_MERCHANT_CREDENTIALS = res.body.data;
    });
  });
  describe("BOT/Support user authentication", async () => {
    it("Support User/Signin", async () => {
      const data = {
        username: "admin",
        password: "Passw0rd"
      };
      const res = await new Request(botService)
        .document(`Support user signin`, `Support/Bot users signin`)
        .method("adminSignin")
        .send(data);
      adminToken = res.body.data.user_token;
      res.body.header.should.have.deep.property("status", "OK");
      res.body.data.should.have.deep.property("user_token");
    });
    it("Support User/Get Profile", async () => {
      const data = {
        username: "admin",
        password: "Passw0rd"
      };
      const res = await new Request(botService)
        .document(
          `Get Support/Bot User Profile`,
          `Get Support/Bot User Profile`
        )
        .setHeader("token", adminToken)
        .method("getAdminProfile")
        .send(data);
      res.body.header.should.have.deep.property("status", "OK");
      // "name", "email", "tel", "status", "login", "superuser"
      res.body.data.should.have.deep.property("user");
      res.body.data.user.should.have.deep.property("name");
      res.body.data.user.should.have.deep.property("login");
    });
    it("Support User/Signout", async () => {
      const res = await new Request(botService)
        .document(
          `Support/Bot user signout`,
          `Support/Bot user signout session`
        )
        .setHeader("token", adminToken)
        .method("adminSignout")
        .send({});
      res.body.header.should.have.deep.property("status", "OK");
    });
  });
});
