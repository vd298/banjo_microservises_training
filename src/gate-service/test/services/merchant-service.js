import chai from "chai";
import chaiHttp from "chai-http";
import pretest from "@lib/pretest";
import { Request, Service } from "../utils";

chai.use(require("chai-like"));
chai.use(require("chai-things"));
chai.use(require("chai-uuid"));

chai.use(chaiHttp);

let ENV,
  userToken,
  service,
  orderId1 = "1234567890",
  payoutId = `234317`,
  transactionId1;

describe("Merchant Services", async () => {
  before(async () => {
    if (!global.ENV) {
      ENV = await pretest.before();
    } else {
      ENV = global.ENV;
    }
    global.ENV = ENV;
    service = new Service(
      "merchant-service",
      global.realmHost,
      ENV.merchantRealm.token,
      {
        lang: "en",
        version: "0.0.1"
      }
    );
  });

  after(async () => {});
  const API_DOC_DESC = `Use this service to create collection request, this service requires sign field in request \`header\` part
### Signature Calculation
1. Normalize/minify data for signature from request body's data part,
E.g.
\`\`\`javascript
const request = {
    "header": {
        "service": "merchant-service",
        "method": "collectionRequest",
        "version":"0.0.1",
        "api_key":"YOUR-API-KEY"
    },
    "data": {
        "currency":"INR",
        "amount":2404,
        "return_url":"https://a.com",
        "user_ip":"2401:4900:562d:6e00:c0cc:f89d:91f:1902"
,        "order":{
            "order_id":"00001239",
            "description":"Test order for payment of 2404"
        }
    }
}
const dataStr = JSON.stringify(request.data)
\`\`\`
2. Calculate signature by normalized JSON string & secret key (private key) to below function
\`\`\`javascript
const calculateSign = (dataStr, secretKey) => {
  const key = crypto.createPrivateKey({
    key: Buffer.from(secretKey, "base64"),
    type: "pkcs8",
    format: "der"
  });
  const sign = crypto.sign("SHA256", Buffer.from(dataStr), key);
  return sign.toString("base64");
};
\`\`\`
3. Send returned value from calculateSign to \`request.header.sign\` parameter`;

  describe.skip("Merchant Services", async () => {
    it("Create Collection Request", async () => {
      const res = await new Request(service)
        .document(`Create Collection Request`, API_DOC_DESC)
        .method("collectionRequest")
        .setHeader("api_key", ENV.TEST_MERCHANT_CREDENTIALS.apiKey)
        .sign(ENV.TEST_MERCHANT_CREDENTIALS.secretKey)
        .send({
          currency: "INR",
          amount: 2500,
          return_url: "https://abc.com",
          user_ip: "127.0.0.1",
          order: {
            order_id: orderId1,
            description: "Test order for payment of 2500"
          }
        });
      res.body.header.should.have.deep.property("status", "OK");
      res.body.data.should.have.deep.property("id");
      res.body.data.should.have.deep.property("payment_url");
      transactionId1 = res.body.data.id;
    });
    it("Create Collection Request without signature", async () => {
      const res = await new Request(service)
        .method("collectionRequest")
        .setHeader("api_key", ENV.TEST_MERCHANT_CREDENTIALS.apiKey)
        .send({
          currency: "INR",
          amount: 2500,
          return_url: "https://abc.com",
          user_ip: "127.0.0.1",
          order: {
            order_id: orderId1,
            description: "Test order for payment of 2500"
          }
        });
      res.body.header.should.have.deep.property("status", "ERROR");
      res.body.error.should.have.deep.property(
        "code",
        "BANJO_ERR_SIGN_MISSING"
      );
    });
    it("Get transaction status by merchant's order id", async () => {
      const res = await new Request(service)
        .document(
          "Get transaction status",
          "Get transaction status by passing merchant's order_id"
        )
        .method("txStatus")
        .setHeader("api_key", ENV.TEST_MERCHANT_CREDENTIALS.apiKey)
        .sign(ENV.TEST_MERCHANT_CREDENTIALS.secretKey)
        .send({ order_id: orderId1 });
      res.body.header.should.have.deep.property("status", "OK");
      res.body.data.should.have.deep.property("transaction_id");
      res.body.data.should.have.deep.property("description");
      res.body.data.should.have.deep.property("status");
      res.body.data.should.have.deep.property("amount");
      res.body.data.should.have.deep.property("currency");
      res.body.data.should.have.deep.property("ctime");
    });
    it("Get transaction status by transaction id", async () => {
      const res = await new Request(service)
        .document(
          "Get transaction status",
          "Get transaction status by passing transaction_id"
        )
        .method("txStatus")
        .setHeader("api_key", ENV.TEST_MERCHANT_CREDENTIALS.apiKey)
        .sign(ENV.TEST_MERCHANT_CREDENTIALS.secretKey)
        .send({ transaction_id: transactionId1 });
      res.body.header.should.have.deep.property("status", "OK");
      res.body.data.should.have.deep.property("transaction_id");
      res.body.data.should.have.deep.property("description");
      res.body.data.should.have.deep.property("status");
      res.body.data.should.have.deep.property("amount");
      res.body.data.should.have.deep.property("currency");
      res.body.data.should.have.deep.property("ctime");
    });
    it("Get transaction status without passing signature", async () => {
      const res = await new Request(service)
        .method("txStatus")
        .setHeader("api_key", ENV.TEST_MERCHANT_CREDENTIALS.apiKey)
        .send({ order_id: "1234" });
      res.body.header.should.have.deep.property("status", "ERROR");
      res.body.error.should.have.deep.property(
        "code",
        "BANJO_ERR_SIGN_MISSING"
      );
    });
    it("Get transaction status by invalid transaction id expect error", async () => {
      const res = await new Request(service)
        .method("txStatus")
        .setHeader("api_key", ENV.TEST_MERCHANT_CREDENTIALS.apiKey)
        .sign(ENV.TEST_MERCHANT_CREDENTIALS.secretKey)
        .send({ transaction_id: "1234" });
      res.body.header.should.have.deep.property("status", "ERROR");
    });
    it("Get transaction status by invalid order id expect error", async () => {
      const res = await new Request(service)
        .method("txStatus")
        .setHeader("api_key", ENV.TEST_MERCHANT_CREDENTIALS.apiKey)
        .sign(ENV.TEST_MERCHANT_CREDENTIALS.secretKey)
        .send({ order_id: "1234" });
      res.body.header.should.have.deep.property("status", "ERROR");
    });
    it.skip("Get Transactions", async () => {
      const res = await new Request(service)
        .setHeader("service", "account-service")
        .document(`User accounts`, `Get user account`)
        .method("getUserAccounts")
        .setHeader("token", userToken)
        .setHeader("account", ENV.acc1.id)
        .send({});
      res.body.header.should.have.deep.property("status", "OK");
      res.body.data[0].should.have.deep.property("account_id");
    });

    // ### Create Payout Request
    // Request payout of funds from your collection wallet to an external User's Bank Account/Destination Address.
    // Payments will be executed
    it("Create Payout Request", async () => {
      const res = await new Request(service)
        .method("payoutRequest")
        .setHeader("api_key", ENV.TEST_MERCHANT_CREDENTIALS.apiKey)
        .sign(ENV.TEST_MERCHANT_CREDENTIALS.secretKey)
        .send({
          currency: "INR",
          amount: 5,
          user_ip: "2401:4900:562d:6e00:c0cc:f89d:91f:1902",
          user_id: "2e214700-27e1-4d7a-8ba8-60c8307a3231",
          order: {
            order_id: orderId1,
            description: "VV Payment Demo",
            destination: "address",
            protocol: "UPI"
          }
        });
      res.body.header.should.have.deep.property("status", "OK");
      res.body.data.should.have.deep.property("id");
      res.body.data.should.have.deep.property("status");
    });
  });
});
