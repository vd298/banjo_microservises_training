//process.env.NODE_ENV = "test";

import chai from "chai";
import chaiHttp from "chai-http";
import { Request, Service } from "../utils";
let should = chai.should();

let userToken;
const service = new Service("auth-service", "test.host", "ajdaskdj", {
  lang: "en",
  version: "0.0.1"
});
chai.use(chaiHttp);

describe("Gate Service", async () => {
  it("Ping, Wrong Version", async () => {
    const res = await new Request(service)
      .setHeader("version", "1")
      .method("signin")
      .send(null);
    res.body.header.status.should.equal("ERROR");
    res.body.error.code.should.equal("BANJO_ERR_VERSION");
  });

  it("Ping skeleton microservice, valid data schema", async () => {
    const res = await new Request(service)
      .document("Sending Nonce", "Sending Unique Nonce")
      .setHeader("service", "skeleton")
      .setHeader("nonce", "1234")
      .method("ping")
      .send({
        text: "test text",
        num: 1234
      });
    res.body.header.should.have.deep.property("status", "OK");
    res.body.data.should.have.deep.property("test-pong", true);
  });

  it("Ping skeleton microservice, invalid data schema", async () => {
    const res = await new Request(service)
      .setHeader("service", "skeleton")
      .method("ping")
      .send({
        text: "test text",
        num: "1234" // should be number
      });
    res.body.header.status.should.equal("ERROR");
    res.body.error.should.have.deep.property(
      "code",
      "BANJO_ERR_DATA_VALIDATION"
    );
  });
  it("Duplicate Nonce Error", async () => {
    const res = await new Request(service)
      .document(
        "Duplicate Nonce Error",
        "Error when sending duplicate nonce header"
      )
      .setHeader("service", "skeleton")
      .setHeader("nonce", "1234")
      .method("ping")
      .send(null);

    res.body.header.status.should.equal("ERROR");
    res.body.error.code.should.equal("BANJO_ERR_NONCE");
  });
});
