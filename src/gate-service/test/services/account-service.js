import chai from "chai";
import chaiHttp from "chai-http";
import db from "@lib/db";
import pretest from "@lib/pretest";
import { Request, Service } from "../utils";

chai.use(require("chai-like"));
chai.use(require("chai-things"));
chai.use(require("chai-uuid"));

let should = chai.should();
const expect = chai.expect;

chai.use(chaiHttp);

let ENV, userToken, invite, service;

async function loginAPI(loginData = { email: "", password: "" }) {
  let data = {
    username: loginData.email,
    password: loginData.password
  };
  const loginRes = await new Request(service)
    .setHeader("service", "auth-service")
    .method("signin")
    .send(data);
  try {
    userToken = loginRes.body.data.user_token;
  } catch (err) {
    console.log("Error in login", loginRes.body);
    throw err;
  }
  return userToken;
}
describe("Account Services", async () => {
  before(async () => {
    if (!global.ENV) {
      ENV = await pretest.before();
    } else {
      ENV = global.ENV;
    }
    global.ENV = ENV;
    service = new Service(
      "account-service",
      global.realmHost,
      ENV.realm.token,
      {
        lang: "en",
        version: "0.0.1"
      }
    );
    await loginAPI({ email: "db154", password: "Passw0rd" });
  });

  after(async () => {});

  describe.skip("Account Operations", async () => {
    // Invite user.
    it("Invite new user", async () => {
      const res = await new Request(service)
        .document(`Invite new user`, `Send invitation link to user`)
        .method("inviteUser")
        .setHeader("token", userToken)
        .setHeader("account", ENV.acc1.id)
        .send({
          email: "at213+1@enovate-it.com",
          role: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4"
        });
      res.body.header.should.have.deep.property("status", "OK");
    });

    // Get Invite Data
    it("Get invite data", async () => {
      invite = await db.user_account_invitations.findOne({
        where: {
          is_registered_user: false
        }
      });
      const res = await new Request(service)
        .document(`Get invite data`, `Get Invitation Details`)
        .method("getInviteData")
        .send({
          token: invite.token
        });
      res.body.header.should.have.deep.property("status", "OK");
    });
    // Signup user using invite link
    it("Signup new user using invite", async () => {
      let data = {
        token: invite.token,
        first_name: "Aditya",
        last_name: "Tapaswi",
        email: invite.email,
        mobile: "9021280822",
        username: "at213",
        password: "P@ssw0rd"
      };
      const res = await new Request(service)
        .document(
          `Signup new user using invite`,
          `Signup user using invitation token`
        )
        .method("registerUserWithInvite")
        .send(data);
      res.body.header.should.have.deep.property("status", "OK");
    });
    // Join user using invite link
    it("Join existing user using invite", async () => {
      const data = {
        email: "at213+1@enovate-it.com",
        role: "8ea6ef78-c4a3-11e9-aa8c-2a2ae2ddcce4"
      };
      let res = await new Request(service)
        .method("inviteUser")
        .setHeader("token", userToken)
        .setHeader("account", ENV.acc2.id)
        .send(data);
      res.body.header.should.have.deep.property("status", "OK");
      const invite = await db.user_account_invitations.findOne({
        where: {
          is_registered_user: true
        }
      });
      const user = await db.user.findOne({
        where: {
          email: invite.email
        }
      });
      await loginAPI({ email: user.username, password: "P@ssw0rd" });
      res = await new Request(service)
        .document("Accept Invitation", "Accept account invitation")
        .method("joinUserWithInvite")
        .setHeader("token", userToken)
        .send({
          token: invite.token
        });
      res.body.header.should.have.deep.property("status", "OK");
    });
  });
  describe("User Account", async () => {
    it("Get user account", async () => {
      const res = await new Request(service)
        .setHeader("service", "account-service")
        .document(`User accounts`, `Get user account`)
        .method("getMerchants")
        .setHeader("token", userToken)
        .setHeader("account", ENV.acc1.id)
        .send({});
      res.body.header.should.have.deep.property("status", "OK");
      res.body.data.userAccounts[0].should.have.deep.property("account_id");
    });
  });
  describe("Merchant Accounts", async () => {
    it("Create Merchant Account 1", async () => {
      const res = await new Request(service)
        .setHeader("token", userToken)
        .method("createMerchantAccount")
        .send({
          account_id: ENV.acc1.id,
          name: "MAID1",
          currency: "INR",
          settlement_currency: "INR",
          status: "ACTIVE"
        });
      ENV.MAID1 = res.body.data.id;
      res.body.header.should.have.deep.property("status", "OK");
      res.body.data.should.have.deep.property("id");
      res.body.data.should.have.deep.property(
        "message",
        "Account created successfully"
      );
    });
    it("Create Merchant Account 2", async () => {
      const res = await new Request(service)
        .method("createMerchantAccount")
        .send({
          account_id: ENV.acc1.id,
          name: "MAID2",
          currency: "INR",
          settlement_currency: "INR",
          status: "ACTIVE"
        });
      ENV.MAID2 = res.body.data.id;
      res.body.header.should.have.deep.property("status", "OK");
      res.body.data.should.have.deep.property("id");
      res.body.data.should.have.deep.property(
        "message",
        "Account created successfully"
      );
    });
    it("Associate Bank Account", async () => {
      const res = await new Request(service)
        .method("associateBankAccount")
        .send({
          merchant_account_id: ENV.MAID1,
          bank_account_id: ENV.bankAccount1.id
        });
      res.body.header.should.have.deep.property("status", "OK");
    });
  });
});
export default {
  loginAPI
};
