import chai from "chai";
import chaiHttp from "chai-http";
import pretest from "@lib/pretest";
import server from "../../src/index.js";

chai.use(require("chai-uuid"));
chai.use(require("chai-like"));
chai.use(require("chai-things"));
chai.use(chaiHttp);

describe("Awaiting Server", async () => {
  before((done) => {
    server.on("server_is_ready", () => {
      done();
    });
  });
  after(async () => {});
  it("Server is ready", async () => {});
});
