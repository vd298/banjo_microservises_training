import chai from "chai";
import doc from "@lib/chai-documentator";
import server from "../src/index.js";

import shared from "@lib/shared";
const Crypto = shared.Crypto;

function titleCase(str) {
  str = str.toLowerCase().split(" ");
  for (var i = 0; i < str.length; i++) {
    str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
  }
  return str.join(" ");
}
export class Service {
  constructor(service, realmHost, realmToken, defaultHeaders) {
    this.service = service;
    this.realmHost = realmHost;
    this.realmToken = realmToken;
    this.headers = defaultHeaders || {};
    this.headers.service = service;
    return this;
  }
}
export class Request {
  doDocument = false;
  constructor(service) {
    this.service = service;
    this.headers = JSON.parse(JSON.stringify(this.service.headers));
    return this;
  }
  method(method) {
    this.headers.method = method;
    return this;
  }
  setHeader(param, value) {
    this.headers[param] = value;
    return this;
  }
  sign(key) {
    this.signRequest = true;
    this.signKey = key;
    return this;
  }

  document(title, description) {
    this.doDocument = true;
    this.title = title;
    this.description = description;
    return this;
  }
  async send(data) {
    if (this.signRequest) {
      this.headers.sign = Crypto.calculateSignature(
        JSON.stringify(data),
        this.signKey,
        "utf8",
        "base64",
        "pem"
      );
    }
    let response;
    if (this.doDocument) {
      if (!this.title) {
        throw "TITLE_MISSING";
      }
      if (!this.description) {
        throw "DESCRIPTION_MISSING";
      }
      response = await doc(
        chai
          .request(server)
          .post("/")
          .set("Origin", "http://" + this.service.realmHost)
          .set("content-type", "application/json")
          .set("authorization", "bearer" + this.service.realmToken)
          .send({ header: this.headers, data }),
        {
          title: titleCase(this.title),
          description: this.description,
          tags: this.service.service
        }
      );
    } else {
      response = await chai
        .request(server)
        .post("/")
        .set("Origin", "http://" + this.service.realmHost)
        .set("content-type", "application/json")
        .set("authorization", "bearer" + this.service.realmToken)
        .send({ header: this.headers, data });
    }
    if (process.env.TEST_DEBUG) {
      console.log(
        `Request`,
        JSON.stringify({ header: this.headers, data }),
        `Response`,
        JSON.stringify(response.body)
      );
    }
    if (this.signRequest && response.body.data) {
      if (!response.body.header.sign) {
        throw "RESPONSE_EMPTY_SIGNATURE";
      }
      if (
        !Crypto.validateSignatureWithPrivateKey(
          response.body.header.sign,
          JSON.stringify(response.body.data),
          this.signKey
        )
      ) {
        throw "RESPONSE_SIGNATURE_NOT_MATCHING";
      }
    }
    return response;
  }
}
