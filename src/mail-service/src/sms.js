import config from "@lib/config";
import rp from "request-promise";

async function send(phone, text) {
  const url = config.smsGateUrl
    .replace("{phone}", phone)
    .replace("{text}", encodeURIComponent(text));
  return await rp({
    url,
    method: "GET",
    json: true
  });
}

export default {
  send
};
