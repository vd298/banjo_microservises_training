// import config from "@lib/config";
// import LBA from "load-balancer-algorithm";
// import SmscGateway from "./SMSGatewaySmsc";
// import D7Gateway from "./D7Gateway";

// const GATEWAYS = {
//   smsc: {
//     name: "Short Message service center",
//     weight: 0.5,
//     cls: SmscGateway,
//     config: config.smsGatewaysConfig.smsc
//   },
//   d7: {
//     name: "Integrate D7 SMS Gateway",
//     weight: 0.5,
//     cls: D7Gateway,
//     config: config.smsGatewaysConfig.d7
//   }
// };

// function getGatewayByCode(code) {
//   if (GATEWAYS[code]) {
//     return new GATEWAYS[code].cls(GATEWAYS[code].config);
//   }
//   throw "GATEWAY_NOT_IMPLEMENTED";
// }

// function selectGateway() {
//   const wrr = new LBA.WeightedRoundRobin(
//     Object.keys(GATEWAYS).map((key) => {
//       return { host: key, weight: GATEWAYS[key].weight };
//     })
//   );
//   return getGatewayByCode(wrr.pick()["host"]);
// }

// async function send(mobile, text, gatewayCode) {
//   let gateway;
//   if (
//     gatewayCode === "automatic" ||
//     gatewayCode == "" ||
//     gatewayCode == undefined
//   ) {
//     gateway = selectGateway();
//   } else {
//     gateway = getGatewayByCode(gatewayCode);
//   }
//   return await gateway.send(mobile, text);
// }
// function getSMSGatewayList() {
//   return Object.keys(GATEWAYS).map((key) => {
//     return { key: key, value: GATEWAYS[key].name };
//   });
// }
// export default {
//   send,
//   getSMSGatewayList
// };
