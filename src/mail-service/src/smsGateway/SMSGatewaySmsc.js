import axios from "axios";
import BaseGateway from "./BaseGateway";

export default class SmscGateway extends BaseGateway {
  async send(phone, text) {
    const smsUrl = this.config.url
      .replace("{phone}", phone)
      .replace("{text}", encodeURIComponent(text));

    return await axios.get(smsUrl);
  }
}
