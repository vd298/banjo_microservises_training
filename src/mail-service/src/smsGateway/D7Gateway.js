import BaseGateway from "./BaseGateway";
import axios from "axios";

export default class D7Gateway extends BaseGateway {
  async send(phone, text) {
    const result = await axios.post(
      this.config.url,
      {
        to: phone,
        from: "D7sms",
        content: text,
        dlr: "yes",
        "dlr-url": this.config.dlrUrl,
        "dlr-level": 3
      },
      {
        auth: {
          username: this.config.username,
          password: this.config.password
        }
      }
    );
    return result;
  }
}
