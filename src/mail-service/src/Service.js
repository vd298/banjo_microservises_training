import Base from "@lib/base";
import db from "@lib/db";
import config from "@lib/config";
import nodemailer from "nodemailer";
import smsClass from "./smsGateway";
import sgMail from "@sendgrid/mail";
const pug = require("pug-async");

const pugOptions = {
  base_url: config.app_url,
  admin_url: config.adminBaseUrl,
  plugins: [
    {
      resolve: (filename) => filename,
      read: async (filename) => {
        const letterDetails = await db.letter.findOne({
          where: {
            code: filename.split(".pug")[0]
          },
          attributes: ["html"]
        });
        if (letterDetails) {
          return letterDetails.html;
        }
        throw new Error("Unknown template" + filename);
      }
    }
  ]
};

export default class Service extends Base {
  publicMethods() {
    return {
      getPublicMethods: {
        realm: true,
        description: "getPublicMethods"
      },
      preview: {
        description: "preview mail body"
      },
      send: {
        realm: true,
        method: this.send,
        description: "send mail"
      },
      sendV2: {
        realm: true,
        method: this.sendV2,
        description: "send mail"
      },
      sms: {
        realm: true,
        method: this.sms,
        description: "send mail"
      },
      getSMSGatewayList: {
        method: smsClass.getSMSGatewayList,
        description: "Get SMS gateway list"
      }
    };
  }

  async preview(data) {
    for (let type of Object.keys(data)) {
      if (type === "data") {
        continue;
      }
      try {
        let template = data[type];
        if (
          [
            "subject",
            "sms_text",
            "push_title",
            "push_message",
            "text"
          ].includes(type)
        ) {
          template = `| ${template}`;
        }
        data[type] = await pug.render(template, {
          ...pugOptions,
          ...data.data
        });
        if (type === "text") {
          data[type] = data[type].replace(/<[^>]{1,}>/g, "");
        }
      } catch (e) {
        data[type] = e.message;
      }
    }
    delete data["data"];
    return data;
  }

  async createMailTpl(data, realmId) {
    const transporter = await db.transporter.findOne({ attributes: ["id"] });

    const dd = {
      transporter: transporter.get("id"),
      realm: realmId,
      code: data.code,
      name: data.code,
      data: JSON.stringify(data, null, 4)
    };

    await db.letter.create(dd);
  }

  async getTemplate(data, realmId, type) {
    const attributes = [
      "id",
      "from_email",
      "to_email",
      "subject",
      "text",
      "html",
      "transporter",
      "data",
      "sms_gateway",
      "to_cc",
      "to_bcc"
    ];
    let letterDetails = await db.letter.findOne({
      where: {
        realm: realmId,
        code: data.code,
        lang: data.lang || "en"
      },
      attributes
    });

    if (!letterDetails) {
      letterDetails = await db.letter.findOne({
        where: {
          realm: realmId,
          code: data.code,
          lang: "en"
        },
        attributes
      });
    }

    if (!letterDetails) {
      await this.createMailTpl(data, realmId);
      throw "MAILTEMPLATENOTFOUND";
    }
    if (!letterDetails.get("data")) {
      await db.letter.update(
        {
          data: JSON.stringify(data, null, 4)
        },
        { where: { id: letterDetails.get("id") } }
      );
    }
    return letterDetails;
  }

  async sms(data, realmId) {
    let letterDetails = await this.getTemplate(data, realmId, "sms");
    if (letterDetails && letterDetails.send_sms === false)
      return "TEMPLATE_NOT_FOUND";
    let text = await pug.render(letterDetails.text, data);

    text = text.replace(/<[^>]{1,}>/g, "");

    //console.log("text to ", data.to, " sms:", text);

    if (config.sendSms) {
      let phone = data.to ? data.to : letterDetails.to_email;
      phone = phone.split(",");
      for (let i = 0; i < phone.length; i++) {
        await smsClass.send(phone[i].trim(), text, letterDetails.sms_gateway);
      }
    }
    // const res = await smsGate.send(data.to, text);
    return {
      success: true
    };
  }

  async send(data, realmId) {
    let letterDetails = await this.getTemplate(data, realmId, "email");
    if (letterDetails && letterDetails.send_email === false)
      return "TEMPLATE_NOT_FOUND";
    let html = await pug.render(letterDetails.get("html"), {
      ...pugOptions,
      ...data
    });
    let text = await pug.render(letterDetails.get("text"), {
      ...pugOptions,
      ...data
    });
    text = text.replace(/<[^>]{1,}>/g, "");
    let subject = await pug.render(
      `| ${data.subject ? data.subject : letterDetails.get("subject")}`,
      {
        ...pugOptions,
        ...data
      }
    );
    subject = subject.replace(/<[^>]{1,}>/g, "");
    if (letterDetails == null) throw "NOLETTERDETAILS";

    let transporterData = await db.transporter.findOne({
      where: {
        id: letterDetails.get("transporter")
      }
    });

    const opt = {
      host: transporterData.get("host_transporter"),
      port: transporterData.get("port_transporter"),
      secure: transporterData.get("secure_transporter"),
      auth: {
        user: transporterData.get("user_transporter"),
        pass: transporterData.get("password_transporter")
      }
    };

    if (!transporterData.get("secure_transporter")) {
      opt.tls = {
        rejectUnauthorized: false
      };
    }

    let transporter = await nodemailer.createTransport(opt);
    const letterData = {
      from: letterDetails.get("from_email"),
      to: data.to ? data.to : letterDetails.get("to_email"),
      subject,
      cc: data.to_cc ? data.to_cc : letterDetails.get("to_cc"),
      bcc: data.to_bcc ? data.to_bcc : letterDetails.get("to_bcc"),
      text,
      html,
      attachments: data.attachments || []
    };
    // console.log(letterData);
    const dummyEnvironments = ["test", "localtest"];
    const test = dummyEnvironments.includes(process.env.NODE_ENV);
    console.log("test", test);
    let info = dummyEnvironments.includes(process.env.NODE_ENV)
      ? { messageId: true }
      : await transporter.sendMail(letterData);

    return {
      success: !!info.messageId
    };
  }

  async sendV2(data, realmId, userId) {
    // let transporterData = await db.transporter.findOne({
    //   where: {
    //     id: data.transporter
    //   }
    // });
    // const opt = {
    //   host: transporterData.get("host_transporter"),
    //   port: transporterData.get("port_transporter"),
    //   secure: transporterData.get("secure_transporter"),
    //   auth: {
    //     user: transporterData.get("user_transporter"),
    //     pass: transporterData.get("password_transporter")
    //   }
    // };

    // if (!transporterData.get("secure_transporter")) {
    //   opt.tls = {
    //     rejectUnauthorized: false
    //   };
    // }

    // let transporter = await nodemailer.createTransport(opt);
    //temptag
    console.log(
      `Received Request to send mail to:`,
      data.to ? data.to : data.to_email
    );
    const letterData = {
      from: data.from || `banjodeve@gmail.com`,
      to: data.to ? data.to : data.to_email,
      subject: data.subject,
      cc: data.to_cc ? data.to_cc : "",
      // bcc: data.to_bcc ? data.to_bcc : data.to_bcc,
      // text: "Hello world"
      html: data.html,
      attachments: data.attachments || []
    };
    // console.log(letterData);
    const dummyEnvironments = ["test", "localtest"];
    const test = dummyEnvironments.includes(process.env.NODE_ENV);
    console.log("test", test);
    // let info = dummyEnvironments.includes(process.env.NODE_ENV)
    //   ? { messageId: true }
    //   : await transporter.sendMail(letterData);

    // const result = await transporter.sendMail(letterData);
    console.log(`Logging API KEY`, process.env.SEND_GRID);
    sgMail.setApiKey(process.env.SEND_GRID);
    // const result = await sgMail.send(letterData);
    if (test) {
      return {
        success: true
      };
    }
    sgMail
      .send(letterData)
      .then((res) => {
        console.log(`Email successfully sent to ${letterData.to}`);
      })
      .catch((err) => {
        console.log(`Email sending failed for ${letterData.to}`);
        console.log(`Mail-service, Func:sendV2. Error:`, err.message);
        if (typeof err == "object") {
          console.log(
            `Mail-service, Func:sendV2. Full Error:`,
            JSON.stringify(err)
          );
        }
      });
    return {
      success: true
    };
  }
}
