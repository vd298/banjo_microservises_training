import db from "@lib/db";
import Queue from "@lib/queue";
import config from "@lib/config";
import { ServiceError } from "@lib/error";

const time = "*/1 * * * *";
const description = "Sync Payout Requests";

async function run(jobId, checkRecord) {
  var me = this;
  try {
    let executeFlag = !config.syncPayouts;
    if (executeFlag) {
      config.syncPayouts = 1;
      return startSyncPayouts({ start: 0 });
    } else {
      console.log(
        "INFO. syncPayouts. executeFlag was returned false. Skipping execution"
      );
      return;
    }
  } catch (e) {
    config.syncPayouts = 0;
    console.error(
      "syncPayouts.js. Func:run. Caught Exception. Error",
      JSON.stringify(e, Object.getOwnPropertyNames(e))
    );
    return;
  }
}

function resetFlag() {
  config.syncPayouts = 0;
}

async function startSyncPayouts(params) {
  try {
    console.log("INFO. startSyncPayouts");
    let query = `select vtt.activity,vtt.id,vtt.merchant_account_id,vtt.src_amount,tm.transfer_request,vtt.transfer_id,vtt.src_currency,vtt.data from ${db.schema}.vw_tx_transfer vtt
      join ${db.schema}.transfers_meta tm on tm.transfer_id = vtt.transfer_id and (tm.transfer_request->'order'->>'protocol' = 'NEFT' or tm.transfer_request->'order'->>'protocol' = 'IMPS' or tm.transfer_request->'order'->>'protocol' = 'UPI') 
      where vtt.transfer_type = 'PAYOUT'  and vtt.activity= 'PENDING' and vtt.src_macc_status = 'ACTIVE' and vtt.auto_payouts=1`;
    const transfers = await db.sequelize.query(query, {
      type: db.sequelize.QueryTypes.SELECT,
      raw: true
    });
    if (!!transfers && transfers.length) {
      for (let transfer of transfers) {
        var res = await Promise.race([
          new Promise(async (resolve, reject) => {
            await Queue.newJob("merchant-service", {
              method: "syncAutoPayout",
              data: {
                transfer_id: transfer.transfer_id
              }
            });
            resolve({
              success: true
            });
          }),
          new Promise((resolve, reject) => {
            setTimeout(() => {
              reject({
                success: false,
                message:
                  "syncAutoPayout api Request Timeout, Please try again after some time."
              });
            }, config.cron_service_time_out * 60000);
          })
        ]);
      }
    }
    resetFlag();
  } catch (baErr) {
    console.error(`Func:startSyncPayouts. Caught Error:`, baErr);
    resetFlag();
  }
}

export default {
  time,
  description,
  run
};
