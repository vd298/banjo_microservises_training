import db from "@lib/db";
import Queue from "@lib/queue";
import config from "@lib/config";
import { ServiceError } from "@lib/error";

const time = "*/1 * * * *";
const description = "Process Payout Requests";

async function run(jobId, checkRecord) {
  var me = this;
  try {
    let executeFlag = !config.processPayouts;
    if (executeFlag) {
      config.processPayouts = 1;
      return startProcessPayouts({ start: 0 });
    } else {
      console.log(
        "INFO. processPayouts. executeFlag was returned false. Skipping execution"
      );
      return;
    }
  } catch (e) {
    config.processPayouts = 0;
    console.error(
      "processPayouts.js. Func:run. Caught Exception. Error",
      JSON.stringify(e, Object.getOwnPropertyNames(e))
    );
    return;
  }
}
async function fetchAllMerchantAccounts(params) {
  try {
    const maResp = await db.merchant_accounts.findAll({
      where: {
        status: db.merchant_accounts.STATUS.ACTIVE,
        removed: 0
      }
    });
    if (!maResp.length) {
      console.log(
        "File: processPayouts.js Func:fetchAllMerchantAccounts Auto payout merchant accounts not found"
      );
    }
    return maResp;
  } catch (err) {
    console.error(
      "File: processPayouts.js Function: fetchAllMerchantAccounts Error: " + err
    );
  }
}
function resetFlag() {
  config.processPayouts = 0;
}
//
async function fetchRequestTransaction(params) {
  try {
    let query = `select tm.* from ${db.schema}.transfers_meta tm 
    where tm.transfer_request->'header'->>'method'=:method and tm.removed=0 and tm.transfer_request->>'processed'='0' and tm.transfer_request->>'merchant_account_id'=:merchant_account_id 
    order by tm.ctime asc`;

    let replacements = {
      merchant_account_id: params.id,
      method: "payoutRequest"
    };
    const transfers = await db.sequelize.query(query, {
      replacements,
      type: db.sequelize.QueryTypes.SELECT,
      raw: true
    });
    console.log("Transfers fetched: ", transfers);
    return transfers;
  } catch (error) {
    console.error(
      "File: processPayouts.js Function: fetchRequestTransaction Error: " +
        error
    );
  }
}
async function getRateOfFee(data) {
  try {
    let Data = {
      trigger: "transaction-service:payoutTrigger",
      variables: [],
      data: {
        action: "APPROVE_TX",
        amount: data.amount
      }
    };
    let service1 = {
      realmId: data.realmId,
      accountId: data.accountId,
      maId: data.maId
    };
    var fee = await Queue.newJob("tariff-service", {
      method: "calculateFee",
      data: Data,
      options: {
        ...service1
      }
    });
    let amount = 0;
    if (
      fee &&
      fee.result &&
      fee.result.transactions &&
      fee.result.transactions.length
    ) {
      for (let i = 0; i < fee.result.transactions.length; i++) {
        if (
          "FEE" === fee.result.transactions[i].txsubtype &&
          !isNaN(fee.result.transactions[i].amount)
        ) {
          amount += fee.result.transactions[i].amount;
        }
      }
    }
    return amount;
  } catch (error) {
    throw error;
  }
}
async function _hasSufficientBalance(data, service) {
  let collectionBalance = await db.sequelize.query(
    `SELECT w.bank_account_id, w.balance FROM  ${db.schema}.wallets w 
            join ${db.schema}.vw_merchant_account_mappings mam on mam.id = w.merchant_account_mapping_id and mam.merchant_account_id = :merchant_account_id
            where w.removed = 0 and w.type = 'COLLECTION' and w.currency= :src_currency and w.balance>=:amount order by w.balance asc`,
    {
      raw: true,
      type: db.sequelize.QueryTypes.SELECT,
      replacements: {
        merchant_account_id: data.merchant_account_id,
        src_currency: data.currency,
        amount: data.amount
      }
    }
  );
  if (collectionBalance && collectionBalance.length) {
    return collectionBalance[0].balance >= data.amount;
  }
  return false;
}
async function startProcessPayouts(params) {
  try {
    console.log("INFO.startProcessPayouts");
    const Op = db.Sequelize.Op;

    let merchantAccounts = await fetchAllMerchantAccounts();
    if (merchantAccounts && merchantAccounts.length) {
      for (let ma of merchantAccounts) {
        console.log("Fetched merchant accounts: ", ma.id, " ", ma.name);
        let transfers = await fetchRequestTransaction(ma);
        let isTimeOut = false;
        if (transfers && transfers.length) {
          for (let currentTransfer of transfers) {
            res = await Promise.race([
              new Promise(async (resolve, reject) => {
                const txControl = await db.sequelize.transaction();
                try {
                  const transferLockSelect = await db.transfers_meta.findOne({
                    where: {
                      id: currentTransfer.id,
                      removed: 0
                    },
                    attributes: ["id"],
                    transaction: txControl,
                    lock: true
                  });

                  //Find the appropriate Bank Account
                  let pickedBankAcc;
                  if (ma.auto_payouts === 1) {
                    pickedBankAcc = await Queue.newJob("merchant-service", {
                      method: "fetchAcquirerBankAccount",
                      data: {
                        merchant_account: ma
                      },
                      options: {
                        ...currentTransfer.transfer_request.service
                      }
                    });
                    console.log(
                      "Picked bank account in auto payouts: ",
                      pickedBankAcc
                    );
                  } else {
                    let feeAmount = await getRateOfFee({
                      maId: ma.id,
                      amount: currentTransfer.transfer_request.amount,
                      accountId: ma.account_id,
                      realmId: currentTransfer.transfer_request.service
                    });

                    let totalAmount =
                      feeAmount + currentTransfer.transfer_request.amount;

                    let hasSufficientBalance = await _hasSufficientBalance(
                      {
                        amount: totalAmount,
                        merchant_account_id: ma.id,
                        currency: currentTransfer.transfer_request.currency
                      },
                      currentTransfer.transfer_request.service
                    );
                    if (!hasSufficientBalance) {
                      console.error(
                        "Func:startProcessPayouts. Caught Error: Insufficient balance for transfer Id: " +
                          currentTransfer.transfer_id
                      );
                      throw new ServiceError("BANJO_ERR_INSUFFICIENT_BALANCE");
                    }
                    pickedBankAcc = await Queue.newJob("merchant-service", {
                      method: "payoutBankAccountCheck",
                      data: {
                        currency: currentTransfer.transfer_request.currency,
                        amount: totalAmount,
                        protocol:
                          currentTransfer.transfer_request.order.protocol,
                        merchant_account_id: ma.id
                      }
                    });
                    console.log("Picked bank account: ", pickedBankAcc);
                  }

                  if (
                    !!pickedBankAcc &&
                    pickedBankAcc.result &&
                    pickedBankAcc.result.bank_account_id
                  ) {
                    console.log(
                      "1. Auto payout merchant account id: ",
                      ma.id,
                      " Transfer id: ",
                      currentTransfer.id
                    );

                    const res = await Queue.newJob("transaction-service", {
                      method: "payoutTrigger",
                      data: {
                        payout_request_transfer_id: currentTransfer.transfer_id,
                        description:
                          currentTransfer.transfer_request.order.description,
                        action: "PAYOUT_REQUEST",
                        currency: currentTransfer.transfer_request.currency,
                        amount: currentTransfer.transfer_request.amount,
                        order_id:
                          currentTransfer.transfer_request.order.order_id,
                        bank_account_id: pickedBankAcc.result.bank_account_id,
                        wallet_id: pickedBankAcc.result.wallet_id,
                        data: {
                          ua: currentTransfer.transfer_request.service.ua,
                          ip: currentTransfer.transfer_request.service.ip,
                          user_ip: currentTransfer.transfer_request.user_ip,
                          user_id: currentTransfer.transfer_request.user_id,
                          beneficiary_id:
                            currentTransfer.transfer_request.beneficiary_id
                        }
                      },
                      options: {
                        ...currentTransfer.transfer_request.service
                      }
                    });
                    console.log("res", res);
                    if (res.result) {
                      if (!res.result.transfer_id) {
                        throw new ServiceError("BANJO_ERR_TARIFF_CONFIG");
                      }

                      if (
                        currentTransfer.transfer_request &&
                        currentTransfer.transfer_request.transfer_id
                      ) {
                        await db.transfers_meta.update(
                          {
                            transfer_request: {
                              ...currentTransfer.transfer_request,
                              processed: 1
                            },
                            transfer_response: res.result,
                            transfer_id: res.result.transfer_id
                          },
                          {
                            where: { id: currentTransfer.id },
                            transaction: txControl
                          }
                        );
                        const beneficiary = await db.beneficiaries.findOne({
                          where: {
                            id: currentTransfer.transfer_request.beneficiary_id
                          },
                          transaction: txControl
                        });
                        if (!beneficiary) {
                          console.error(
                            "Func:startProcessPayouts. Caught Error: Beneficiary not found: " +
                              currentTransfer.transfer_request.beneficiary_id
                          );
                          await Queue.newJob("transaction-service", {
                            method: "txActivityReject",
                            data: {
                              transfer_id: res.result.transfer_id,
                              activity_action: "REJECTED",
                              action_type: "SYSTEM_REJECT",
                              note:
                                "Beneficiary account not found, Please provide valid beneficiary account id",
                              system_reject: 1
                            }
                          });
                          throw new ServiceError(
                            "BANJO_ERR_BENEFICIARIES_ACCOUNT_NOT_FOUND"
                          );
                        } else {
                          await db.transfer.update(
                            {
                              beneficiary_id:
                                currentTransfer.transfer_request.beneficiary_id
                            },
                            {
                              where: {
                                id: res.result.transfer_id
                              }
                            }
                          );
                        }
                      }
                    } else {
                      if (
                        res.error &&
                        res.error.name == "SequelizeUniqueConstraintError" &&
                        res.error.errors.filter((e) => e.path == "order_num")
                          .length
                      ) {
                        throw new ServiceError("BANJO_ERR_UNIQUE_ORDER_ID");
                      } else {
                        throw res.error;
                      }
                    }
                  }
                  await txControl.commit();
                } catch (baErr) {
                  console.error(
                    `Func:startProcessPayouts. Caught Error:`,
                    baErr
                  );

                  await db.transfers_meta.update(
                    {
                      transfer_request: {
                        ...currentTransfer.transfer_request,
                        processed: 1
                      }
                    },
                    {
                      where: { id: currentTransfer.id },
                      transaction: txControl
                    }
                  );
                  await txControl.commit();
                }
                isTimeOut = false;
                resolve({
                  success: true
                });
              }),
              new Promise((resolve, reject) => {
                setTimeout(() => {
                  isTimeOut = true;
                  reject({
                    success: false,
                    message:
                      "Request Timeout, Please try again after some time."
                  });
                }, config.cron_service_time_out * 60000);
              })
            ]);
          }
        }
        if (ma.auto_payouts === 1 && !isTimeOut) {
          console.log("2. Auto payout merchant account id: ", ma.id);
          var res = await Promise.race([
            new Promise(async (resolve, reject) => {
              await Queue.newJob("merchant-service", {
                method: "executeAutoPayout",
                data: {
                  merchant_account_id: ma.id
                }
              });
              resolve({
                success: true
              });
            }),
            new Promise((resolve, reject) => {
              setTimeout(() => {
                reject({
                  success: false,
                  message: `executeAutoPayout api Request Timeout, Please try again after some time. Merchant id: ${ma.id} `
                });
              }, config.cron_service_time_out * 60000);
            })
          ]);
        }
      }
    }
    resetFlag();
  } catch (baErr) {
    console.error(`Func:startProcessPayouts. Caught Error:`, baErr);
    resetFlag();
  }
}

export default {
  time,
  description,
  run
};
