import db from "@lib/db";
import Queue from "@lib/queue";
import config from "@lib/config";
const time = "35 18 * * *";

const description = "Generate Sql Report";

async function run(jobId, checkRecord) {
  try {
    let executeFlag = !config.generateReport;
    if (executeFlag) {
      config.generateReport = 1;
      return startGenerateReport({ start: 0 });
    } else {
      console.log(
        "INFO. generateReport. executeFlag was returned false. Skipping execution"
      );
      return;
    }
  } catch (e) {
    config.generateReport = 0;
    console.error(
      "generateReport.js. Func:run. Caught Exception. Error",
      JSON.stringify(e, Object.getOwnPropertyNames(e))
    );
    return;
  }
}

function resetFlag() {
  config.generateReport = 0;
}
async function fetchUsersList() {
  try {
    const users = await db.sequelize.query(
      `select
        u.email
      from
        ${db.schema}."groups" g
      inner join ${db.schema}.admin_users u on
        g."_id" = u.groupid 
        and u.status = 'ACTIVE'
        and u.email is not null
      where
        g.code = 'Finance'`,
      {
        replacements: {},
        raw: true
      }
    );
    let emails = [];
    if (users && users.length && users[0].length) {
      emails = users[0].map((user) => user.email);
      return emails;
    } else {
      return emails;
    }
  } catch (error) {
    console.error(
      `File: generateReport Func:fetchUsersList. Caught Error:`,
      error
    );
  }
}
async function startGenerateReport(params) {
  try {
    console.log("INFO. startGenerateReport");
    // find fiances admin users
    const emails = await fetchUsersList();
    if (emails && emails.length) {
      const currentDate = new Date();

      currentDate.setHours(0, 0, 0, 0);
      const currentTimeStamp = currentDate.getTime();

      // const previousDayTimeStamp = currentTimeStamp - 24 * 60 * 60 * 1000;
      const previousDayTimeStamp = currentTimeStamp;
      const startDate = new Date(previousDayTimeStamp);
      const endDate = new Date(previousDayTimeStamp);
      endDate.setHours(23, 59, 59, 999);
      const startDateString = startDate
        .toISOString()
        .replace("T", " ")
        .replace("Z", " +0530");
      const endDateString = endDate
        .toISOString()
        .replace("T", " ")
        .replace("Z", " +0530");

      const year = startDate.getFullYear();
      const month = String(startDate.getMonth() + 1).padStart(2, "0");
      const day = String(startDate.getDate()).padStart(2, "0");
      const startString = `${day}-${month}-${year}`;

      let sqlQuery = ` WHERE t.ctime>= '${startDateString}' AND ctime<= '${endDateString}' AND t.mtime>= '${startDateString}'`;
      const merchantAccounts = await db.sequelize.query(
        `select distinct on (merchant_account_id) merchant_account_id,src_macc_name from ${db.schema}.vw_tx_transfer t ${sqlQuery}`,
        {
          replacements: {},
          raw: true
        }
      );

      if (
        !!merchantAccounts &&
        merchantAccounts.length &&
        merchantAccounts[0].length
      ) {
        for (let merchant of merchantAccounts[0]) {
          let res = await Promise.race([
            new Promise(async (resolve, reject) => {
              try {
                const res = await Queue.newJob("account-service", {
                  method: "downloadRecords",
                  data: {
                    startDate,
                    endDate,
                    merchant_account_id: merchant.merchant_account_id,
                    src_macc_name: merchant.src_macc_name,
                    type: "xlsx"
                  }
                });

                if (
                  res &&
                  res.result &&
                  res.result.success &&
                  res.result.code &&
                  res.result.data
                ) {
                  await sendEmailWithAttachment({
                    ...res.result,
                    emails,
                    src_macc_name: merchant.src_macc_name,
                    filename: `rashipay_${merchant.src_macc_name}_${startString}.xlsx`,
                    date: startString
                  });
                }
              } catch (baErr) {
                console.error(`Func:startGenerateReport. Caught Error:`, baErr);
              }

              resolve({
                success: true
              });
            }),
            new Promise((resolve, reject) => {
              setTimeout(() => {
                reject({
                  success: false,
                  message: "Request Timeout, Please try again after some time."
                });
              }, config.cron_service_time_out * 60000);
            })
          ]);
        }
      } else {
        console.log(
          "INFO: Skipped generateReport cron due to empty transfer activity list."
        );
      }
    } else {
      console.log("INFO: Skipped generateReport cron due to empty email list.");
    }
    resetFlag();
  } catch (baErr) {
    console.error(`Func:startGenerateReport. Caught Error:`, baErr);
    resetFlag();
  }
}

async function sendEmailWithAttachment(data) {
  try {
    console.log("Calling notify.invite-email API");
    let res = await Queue.publish(`NOTIFY.RASHIPAY-REPORT-EMAIL`, {
      user_id: null,
      options: {
        send_sms: false,
        send_push: false,
        to_email: data.emails
      },
      payload: {
        acc_name: data.src_macc_name,
        date: data.date,
        email: data.emails.toString(),
        attachments: [
          {
            content: data.data.file,
            filename: data.filename,
            type:
              "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            disposition: "attachment"
          }
        ]
      }
    });
    console.log("Invite sent successfully", res);
  } catch (error) {
    console.log(
      `For registering user email ${data.email} got email failure: ` + error
    );
  }
}
export default {
  time,
  description,
  run
};
