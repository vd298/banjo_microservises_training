import db from "@lib/db";
import config from "@lib/config";

const time = "35 18 * * *";
const description = "Reset materialized view";

async function run(jobId, checkRecord) {
  var me = this;
  try {
    let executeFlag = !config.resetMaterializedView;
    if (executeFlag) {
      config.resetMaterializedView = 1;
      return startResetMaterializedView({ start: 0 });
    } else {
      console.log(
        "INFO. resetMaterializedView. executeFlag was returned false. Skipping execution"
      );
      return;
    }
  } catch (e) {
    config.resetMaterializedView = 0;
    console.error(
      "resetMaterializedView.js. Func:run. Caught Exception. Error",
      JSON.stringify(e, Object.getOwnPropertyNames(e))
    );
    return;
  }
}

function resetFlag() {
  config.resetMaterializedView = 0;
}

async function startResetMaterializedView(params) {
  try {
    console.log("INFO. startResetMaterializedView");
    const viewList = [
      "mvw_fetch_one_year_transactions_activity",
      "mvw_fetch_one_month_transactions_activity",
      "mvw_fetch_one_week_transactions_activity",
      "mvw_fetch_yesterday_transactions_activity"
    ];

    viewList.forEach(async (viewName) => {
      const refreshQuery = `REFRESH MATERIALIZED VIEW ${db.schema}.${viewName};`;
      await db.sequelize.query(refreshQuery, {
        raw: true
      });
      setTimeout(() => {
        console.log(`reset ${viewName} materialized view`);
      }, 1000);
    });

    resetFlag();
  } catch (baErr) {
    console.error(`Func:startResetMaterializedView. Caught Error:`, baErr);
    resetFlag();
  }
}

export default {
  time,
  description,
  run
};
