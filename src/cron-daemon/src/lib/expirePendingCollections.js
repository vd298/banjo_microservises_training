import db from "@lib/db";
import Queue from "@lib/queue";
import config from "@lib/config";

const time = "0 * * * *";
const description = "Expire Pending Collections";

async function run(jobId, checkRecord) {
  var me = this;
  try {
    let executeFlag = !config.expirePendingCollections;
    if (executeFlag) {
      config.expirePendingCollections = 1;
      return startExpirePendingCollections({ start: 0 });
    } else {
      console.log(
        "INFO. expirePendingCollections. executeFlag was returned false. Skipping execution"
      );
      return;
    }
  } catch (e) {
    config.expirePendingCollections = 0;
    console.error(
      "expirePendingCollections.js. Func:run. Caught Exception. Error",
      JSON.stringify(e, Object.getOwnPropertyNames(e))
    );
    return;
  }
}

function resetFlag() {
  config.expirePendingCollections = 0;
}

async function startExpirePendingCollections(params) {
  try {
    console.log("INFO. startExpirePendingCollections");
    let query = `select vtt.activity,vtt.id,vtt.merchant_account_id,vtt.src_amount,vtt.transfer_id,vtt.src_currency, vtt.data from ${db.schema}.vw_tx_transfer vtt 
      where vtt.transfer_type = 'COLLECTION'  and vtt.activity= 'REQUEST' and vtt.ctime <= NOW() - INTERVAL '1 HOUR' * vtt.expiry_hours;`;

    const transfers = await db.sequelize.query(query, {
      type: db.sequelize.QueryTypes.SELECT,
      raw: true
    });
    if (!!transfers && transfers.length) {
      for (let transfer of transfers) {
        var res = await Promise.race([
          new Promise(async (resolve, reject) => {
            await Queue.newJob("transaction-service", {
              method: "txActivityReject",
              data: {
                transfer_id: transfer.transfer_id,
                activity_action: "REJECTED",
                action_type: "SYSTEM_REJECT",
                note: "System Reject, Request cross expiry time",
                system_reject: 1
              }
            });
            resolve({
              success: true
            });
          }),
          new Promise((resolve, reject) => {
            setTimeout(() => {
              reject({
                success: false,
                message:
                  "merchant-service startExpirePendingCollections function  Request Timeout,  Please try again after some time."
              });
            }, config.cron_service_time_out * 60000);
          })
        ]);
      }
    }
    resetFlag();
  } catch (baErr) {
    console.error(`Func:startExpirePendingCollections. Caught Error:`, baErr);
    resetFlag();
  }
}

export default {
  time,
  description,
  run
};
