import axios from "axios";
import dotenv from "dotenv";
dotenv.config({ path: "../../../../.env" });
import config from "@lib/config";

const emoji = {
  error: "🔴",
  warn: "🟡",
  http: "🔵",
  info: "🟢"
};

function format(data) {
  const text = `${emoji[data.level]} *${process.env.NODE_ENV ||
    "development"} [${data.level}]* at _${data.timestamp}_.
  \`${data.message}\`${
    data.details
      ? `
    Details: \`${JSON.stringify(data.details)}\``
      : ``
  }${
    data.process
      ? `
   *Process:* ${data.process}`
      : ""
  }${
    data.method
      ? `
   *Method:* ${data.method}`
      : ""
  }`;
  return text;
}

async function send(data) {
  const text = format(data);
  const url = `https://api.telegram.org/bot${process.env.TELEGRAM_ALARM_BOT_TOKEN}/sendMessage`;
  const res = await axios({
    url,
    method: "post",
    data: {
      chat_id: config.alarmTgGroupId,
      text,
      parse_mode: "Markdown"
    }
  });
  if (res && res.data && res.data.ok === true) return true;
  console.error("Telegram error", res && res.data ? res.data : null);
  throw new Error("Unsuccessful Telegram alarm notification");
}

export default {
  send
};
