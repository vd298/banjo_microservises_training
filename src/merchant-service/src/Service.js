import Base from "@lib/base";
import Schema from "@lib/schema";
import Transactions from "./lib/transactions";
import Acquirer from "./lib/Acquirer";
import wallet from "./lib/wallet";
import transactions from "./lib/transactions";
import details from "./lib/student1";
import microA from "./lib/microserviceA";

export default class Service extends Base {
  publicMethods() {
    return {
      serviceDescription: {},
      DetailsOfStudent: {
        method: details.DetailsOfStudent,
        description: "Details of a student",
        realm: true,
        schema: {
          type: "object",
          properties: {
            studentId: { type: "number" },
          },
        },
      },
      DetailsOfStudentMicroA: {
        method: microA.DetailsOfStudentMicroA,
        description: "Details of a student in Microservice A",
        realm: true,
        schema: {
          type: "object",
          properties: {
            studentId: { type: "number" },
          },
        },
      },
      getPublicMethods: {
        realm: true,
        description: "getPublicMethods",
      },
      collectionRequest: {
        sign: true,
        realm: true,
        merchant: true,
        method: Transactions.collectionRequest,
        description: "Create Collection Transaction",
        schema: Schema.MERCHANT_SCHEMA.COLLECTION_REQUEST,
      },
      payoutRequest: {
        sign: true,
        // realm: true,
        merchant: true,
        method: Transactions.payoutRequest,
        description: "Create Payout Transaction",
        schema: Schema.MERCHANT_SCHEMA.PAYOUT_REQUEST,
      },
      payoutBankAccountCheck: {
        method: Transactions.payoutBankAccountCheck,
        description: "Create Payout Transaction",
        schema: Schema.MERCHANT_SCHEMA.PAYOUT_BANK_ACC_CHECK,
      },
      txStatus: {
        sign: true,
        realm: true,
        merchant: true,
        method: Transactions.txStatus,
        description: "Get Transaction Status",
        schema: Schema.MERCHANT_SCHEMA.TX_STATUS_REQUEST,
      },
      transactions: {
        realm: true,
        merchant: true,
        method: Transactions.transactions,
        description: "Get Transactions List",
        schema: Schema.MERCHANT_SCHEMA.TX_LIST_REQUEST,
      },
      createPayoutRequest: {
        description: "create new payout ",
        user: true,
        method: Transactions.createPayoutRequest,
        schema: {
          type: "object",
          properties: {
            merchant_account_id: {
              type: "string",
            },
            src_currency: {
              type: "string",
            },
            dst_currency: {
              type: "string",
            },
            src_amount: {
              type: "string",
            },
            dst_amount: {
              type: "string",
            },
          },
        },
      },

      CollectionRequestH2H: {
        sign: true,
        realm: true,
        merchant: true,
        method: Transactions.CollectionRequestH2H,
        description:
          "Wrapper around collectionRequest and txActivityPageOpened",
        schema: Schema.MERCHANT_SCHEMA.COLLECTION_REQUEST_H2H,
      },
      fundTransferRequest: {
        description: "Fund Transfer Request",
        method: Acquirer.fundTransferRequest,
        schema: {
          accountNo: {
            type: "string",
          },
          aggregatorId: {
            type: "string",
          },
          amount: {
            type: ["string", "number"],
          },
          bankName: {
            type: "string",
          },
          beneName: {
            type: "string",
          },
          ifscCode: {
            type: "string",
          },
          merchantTransId: {
            type: "string",
          },
          transferType: {
            type: "string",
          },
        },
        required: [
          "accountNo",
          "aggregatorId",
          "amount",
          "bankName",
          "beneName",
          "ifscCode",
          "merchantTransId",
          "transferType",
        ],
      },
      getWalletBalanceDtl: {
        description: "Get Wallet BalanceDtl",
        method: Acquirer.getWalletBalanceDtl,
        schema: {
          aggregatorId: {
            type: "string",
          },
        },
        required: ["aggregatorId"],
      },
      getTransStatus: {
        description: "Get Transaction Status",
        method: Acquirer.getTransStatus,
        schema: {
          aggregatorId: {
            type: "string",
          },
          transid: {
            type: "string",
          },
        },
        required: ["aggregatorId", "transid"],
      },
      executeAutoPayout: {
        description: "fetch auto payout merchant Account list",
        method: Transactions.executeAutoPayout,
        schema: {},
        required: [],
      },
      syncAutoPayout: {
        description: "scan auto payout merchant Account pending list",
        method: Transactions.syncAutoPayout,
        schema: {},
        required: [],
      },
      syncAutoCollection: {
        description: "scan auto collection merchant Account pending list",
        method: Transactions.syncAutoCollection,
        schema: {},
        required: [],
      },
      xazurpayStatus: {
        description: "webhook for collection Request",
        method: Transactions.xazurpayStatus,
        schema: {},
        required: [],
      },
      digiPaymentStatus: {
        description: "webhook for collection Request",
        method: Transactions.digiPaymentStatus,
        schema: {},
        required: [],
      },
      fetchAcquirerBankAccount: {
        description: "Fetch acquirer bank account",
        method: Transactions.fetchAcquirerBankAccount,
        schema: {},
        required: [],
      },
      createDeposit: {
        description: "create new Deposit ",
        user: true,
        method: Transactions.createDeposit,
        schema: {
          type: "object",
          properties: {
            merchant_account_id: {
              type: "string",
            },
            dst_bank_acc_id: {
              type: "string",
            },
            src_bank_acc_id: {
              type: "string",
            },
            src_currency: {
              type: "string",
            },
            dst_currency: {
              type: "string",
            },
            src_amount: {
              oneOf: [{ type: "string" }, { type: "number" }],
            },
            dst_amount: {
              oneOf: [{ type: "string" }, { type: "number" }],
            },
            brn: {
              type: "string",
            },
            fee: {
              oneOf: [{ type: "string" }, { type: "number" }],
            },
            note: {
              type: "string",
            },
          },
        },
      },
      executeLostTransactions: {
        description: "create new Deposit ",
        user: true,
        method: Transactions.executeLostTransactions,
        schema: {
          type: "object",
          properties: {
            account_id: {
              type: "string",
            },
            merchant_account_id: {
              type: "string",
            },
            bank_account_id: {
              type: "string",
            },
            amount: {
              oneOf: [{ type: "string" }, { type: "number" }],
            },
            brn: {
              type: "string",
            },
            note: {
              type: "string",
            },
          },
        },
      },
      executeAutoCollection: {
        description: "create new Deposit ",
        user: true,
        method: Transactions.executeAutoCollection,
        schema: {
          type: "object",
          properties: {
            merchant_account_id: {
              type: "string",
            },
            brn: {
              type: "string",
            },
            transfer_id: {
              type: "string",
            },
          },
          required: ["merchant_account_id", "transfer_id"],
        },
      },
      checkSrcLedgerAccount: {
        description: "create new Deposit ",
        user: true,
        method: wallet.checkSrcLedgerAccount,
        schema: {
          type: "object",
          properties: {
            bank_account_id: {
              type: "string",
            },
            currency: {
              type: "string",
            },
          },
          required: ["bank_account_id", "currency"],
        },
      },
      getRateOfFee: {
        description: "Get rate of fee ",
        user: true,
        method: transactions.getRateOfFee,
      },
    };
  }

  async ping() {
    console.log("ping");
    return { "test-pong": true };
  }
}
