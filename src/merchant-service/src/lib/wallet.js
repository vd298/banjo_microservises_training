import db from "@lib/db";
import Queue from "@lib/queue";
async function getCreateLedgerAccount(bankAccId, currency, service) {
  await Queue.newJob("account-service", {
    method: "checkValidRealmId",
    data: {},
    options: {
      realmId: service.realmId
    }
  });
  const acc = await db.wallet.findOne({
    where: {
      bank_account_id: bankAccId,
      currency,
      type: db.wallet.TYPES.LEDGER
    },
    attributes: ["id", "currency"]
  });
  if (acc) {
    return acc;
  } else {
    const acc = await createLedgerAccount(bankAccId, currency);
    return {
      id: acc.id,
      currency: acc.currency
    };
  }
}

async function getCreateLedgerFeeAccount(bankAccId, currency, service) {
  await Queue.newJob("account-service", {
    method: "checkValidRealmId",
    data: {},
    options: {
      realmId: service.realmId
    }
  });
  const acc = await db.wallet.findOne({
    where: {
      bank_account_id: bankAccId,
      currency,
      type: db.wallet.TYPES.FEE
    },
    attributes: ["id", "currency"]
  });
  if (acc) {
    return acc;
  } else {
    const acc = await createLedgerFeeAccount(bankAccId, currency);
    return {
      id: acc.id,
      currency: acc.currency
    };
  }
}

async function createLedgerAccount(bankAccId, currency) {
  const res = await Queue.newJob("account-service", {
    method: "createWallet",
    data: {
      negative: true,
      currency,
      acc_label: "Bank Ledger",
      type: db.wallet.TYPES.LEDGER,
      bank_account_id: bankAccId
    }
  });
  if (res.result) {
    return res.result;
  } else {
    throw res.error;
  }
}
async function createLedgerFeeAccount(bankAccId, currency) {
  const res = await Queue.newJob("account-service", {
    method: "createWallet",
    data: {
      currency,
      acc_label: "Bank Ledger",
      type: db.wallet.TYPES.FEE,
      bank_account_id: bankAccId
    }
  });
  if (res.result) {
    return res.result;
  } else {
    throw res.error;
  }
}
async function checkSrcLedgerAccount(data, service) {
  const bank = await db.bank_accounts.findOne({
    where: {
      id: data.bank_account_id
    },
    attributes: ["bank_id"]
  });
  if (!bank) {
    throw new service.ServiceError("BANJO_ERR_EXCHANGE_ACCOUNT_NOT_FOUND");
  }
  const srcLedgerAccount = await db.bank_accounts.findOne({
    where: {
      bank_id: bank.bank_id,
      currency: data.currency
    },
    attributes: ["id"]
  });
  if (!srcLedgerAccount) {
    throw new service.ServiceError("BANJO_ERR_EXCHANGE_SRC_ACCOUNT_NOT_FOUND");
  }
  return srcLedgerAccount;
}
async function fetchCollectionWallet(bankAccId, currency, merchant_account) {
  try {
    await Queue.newJob("account-service", {
      method: "checkValidRealmId",
      data: {},
      options: {
        realmId: service.realmId
      }
    });
    let query = `SELECT w.id, w.currency,w.merchant_account_mapping_id,w.bank_account_id  FROM  ${db.schema}.wallets w 
            join ${db.schema}.vw_merchant_account_mappings mam on mam.id = w.merchant_account_mapping_id and mam.merchant_account_id = :merchant_account and mam.active_relation = true
            where w.removed = 0 and w.type = :type and w.currency= :currency and w.bank_account_id = :bank_account_id order by w.ctime DESC`;
    let replacements = {
      currency,
      bank_account_id: bankAccId,
      merchant_account,
      type: db.wallet.TYPES.COLLECTION
    };
    const acc = await db.sequelize.query(query, {
      replacements,
      raw: true
    });
    return acc;
  } catch (error) {
    console.error(
      "File: wallet.js Func: fetchCollectionWallet Error: " + error
    );
    throw error;
  }
}
async function getCreateCollectionAccount(
  bankAccId,
  currency,
  merchant_account
) {
  const acc = await fetchCollectionWallet(
    bankAccId,
    currency,
    merchant_account
  );
  if (acc && acc.length && acc[0].length && acc[0][0].id) {
    return acc[0][0];
  } else {
    const res = await Queue.newJob("account-service", {
      method: "associateBankAccount",
      data: {
        merchant_account_id: merchant_account,
        bank_account_id: bankAccId
      },
      options: {
        realmId: service.realmId
      }
    });
    if (res.result) {
      const acc = await fetchCollectionWallet(
        bankAccId,
        currency,
        merchant_account
      );
      if (acc && acc.length && acc[0].length && acc[0][0].id) {
        return acc[0][0];
      }
    } else {
      throw res.error;
    }
  }
}

export default {
  getCreateLedgerAccount,
  getCreateLedgerFeeAccount,
  checkSrcLedgerAccount,
  getCreateCollectionAccount
};
