import db from "@lib/db";
import Queue from "@lib/queue";
import { transcode } from "buffer";
import crypto from "crypto";
import Transactions from "./transactions";
import config from "@lib/config";
import axios from "axios";
import { protocol } from "socket.io-client";

function getLocalIPAddress() {
  return new Promise((resolve, reject) => {
    const http = require("http");

    http
      .get("http://api.ipify.org", (res) => {
        let data = "";

        res.on("data", (chunk) => {
          data += chunk;
        });

        res.on("end", () => {
          console.log(`Your public IP address is: ${data}`);
          resolve(data);
        });
      })
      .on("error", (error) => {
        console.error(
          "File: Acquirer.js, Func: getLocalIPAddress.  Error:",
          error
        );
        reject(error);
      });
  });
}
async function fundTransferRequest(data, service) {
  const decimalFormat = new Intl.NumberFormat("en-US", {
    minimumFractionDigits: 2
  });
  const damount = data.amount;
  const amount = parseFloat(damount).toFixed(2);
  const amounts = decimalFormat.format(damount);
  const secretKey = await decrypt(data.secretKey);
  const tree_map = {
    accountNo: data.accountNo,
    aggregatorId: data.aggregatorId,
    amount: amount,
    bankName: data.bankName,
    beneName: data.beneName,
    ifscCode: data.ifscCode,
    merchantTransId: data.merchantTransId,
    secretKey,
    transferType: data.transferType
  };
  const requestData = generateRequestData(tree_map);
  const IPIMEI = await getLocalIPAddress();
  const options = {
    method: "POST",
    url: process.env.BANJOPAY_PAYOUT_BASE_URL + "fundTransferRequest",
    headers: {
      "Content-Type": "application/json",
      IPIMEI,
      "User-Agent": "web",
      SecretSalt: "99m4mhb3cEGuIwIQ2we2Sw=="
    },
    body: requestData
  };
  return new Promise((resolve, reject) => {
    var request = require("request");

    request(options, function(error, response) {
      if (error) reject(error);
      console.log(response.body);
      resolve(JSON.parse(response.body));
    });
  });
}
function replaceChar(origString, replaceChar, index) {
  let firstPart = origString.slice(0, index);

  let lastPart = origString.slice(index + 1);

  let newString = firstPart + replaceChar + lastPart;

  return newString;
}
function generateRequestData(tree_map) {
  const hashAlgorithm = "sha256";
  let jsonString = JSON.stringify(tree_map);

  let index = jsonString.indexOf("amount");
  if (index !== -1) {
    index = jsonString.indexOf(":", index);
    jsonString = replaceChar(jsonString, "", index + 1);
    index = jsonString.indexOf('"', index);
    jsonString = replaceChar(jsonString, "", index);
  }
  console.log("jsonString", jsonString);
  const hash = generateHashString(jsonString);
  console.log("***********************Hash = ", hash);

  tree_map.hash = hash;
  delete tree_map.secretKey;

  const jsonStringNew = JSON.stringify(tree_map);
  console.log(jsonStringNew + "*********************" + hash);

  return jsonStringNew;
}

function generateHashString(input) {
  const hash = crypto
    .createHash("sha256")
    .update(input)
    .digest("hex");
  const paddedHash = hash.padStart(64, "0");
  return paddedHash;
}
async function getWalletBalanceDtl(data, service) {
  const secretKey = await decrypt(data.secretKey);

  const tree_map = {
    aggregatorId: data.aggregatorId,
    secretKey
  };
  const requestData = generateRequestData(tree_map);
  const IPIMEI = await getLocalIPAddress();
  const options = {
    method: "POST",
    url: process.env.BANJOPAY_PAYOUT_BASE_URL + "getWalletBalanceDtl",
    headers: {
      "Content-Type": "application/json",
      IPIMEI,
      "User-Agent": "web",
      SecretSalt: "99m4mhb3cEGuIwIQ2we2Sw=="
    },
    body: requestData
  };
  return new Promise((resolve, reject) => {
    var request = require("request");

    request(options, function(error, response) {
      if (error) reject(error);
      console.log(response.body);
      if (response.body) resolve(JSON.parse(response.body));
      else resolve(response.body);
    });
  });
}
async function getTransStatus(data, service) {
  const secretKey = await decrypt(data.secretKey);
  const tree_map = {
    aggregatorId: data.aggregatorId,
    secretKey,
    transid: data.transid
  };
  const requestData = generateRequestData(tree_map);
  const IPIMEI = await getLocalIPAddress();
  const options = {
    method: "POST",
    url: process.env.BANJOPAY_PAYOUT_BASE_URL + "getTransStatus",
    headers: {
      "Content-Type": "application/json",
      IPIMEI,
      "User-Agent": "web",
      SecretSalt: "99m4mhb3cEGuIwIQ2we2Sw=="
    },
    body: requestData
  };
  return new Promise((resolve, reject) => {
    var request = require("request");

    request(options, function(error, response) {
      if (error) reject(error);
      console.log(response.body);
      if (response.body) resolve(JSON.parse(response.body));
      else resolve(response.body);
    });
  });
}
class AcquirerBase {
  constructor(aggregator_id, secretKey) {
    this.aggregatorId = aggregator_id;
    this.secretKey = secretKey;
  }
  async executeAcquirerPayment() {}
  async syncAutoCollectionAcquirer(data, service, transfer, api_credentials) {}
  async _hasCollectionWalletResSufficientBalance(data, service) {
    try {
      let collectionBalance = await db.sequelize.query(
        `SELECT w.bank_account_id, w.balance FROM  ${db.schema}.wallets w 
            join ${db.schema}.vw_merchant_account_mappings mam on mam.id = w.merchant_account_mapping_id and mam.merchant_account_id = :merchant_account_id
            where w.removed = 0 and w.type = 'COLLECTION' and w.currency= :src_currency and w.balance>=:amount order by w.balance asc`,
        {
          raw: true,
          type: db.sequelize.QueryTypes.SELECT,
          replacements: {
            merchant_account_id: data.merchant_account_id,
            src_currency: data.currency || data.src_currency,
            amount: data.amount
          }
        }
      );
      if (collectionBalance && collectionBalance.length) {
        return {
          success: collectionBalance[0].balance >= data.amount,
          collectionBalance
        };
      }

      return {
        success: false,
        collectionBalance
      };
    } catch (error) {
      console.log(
        "File: Acquirer.js Function: hasSufficientBalance Error: " + error
      );
      throw error;
    }
  }
}
class BanjoPay extends AcquirerBase {
  constructor(aggregator_id, secretKey) {
    super(aggregator_id, secretKey);
  }
  async _hasSufficientBalance(data, service) {
    try {
      const res = await getWalletBalanceDtl(data, service);
      if (res && res.response && res.response === "ERROR") {
        return { success: false, res };
      } else if (res && res.code && res.code === "300") {
        if (res.amount && res.amount >= data.amount) {
          return { success: true, res };
        } else {
          return { success: false, res };
        }
      }
      return { success: false, res: [] };
    } catch (error) {
      console.log(
        "File: Acquirer.js Function: hasSufficientBalance Error: " + error
      );
      throw error;
    }
  }
  async executeAcquirerPayment(data, service, beneficiary, transfer) {
    try {
      if (
        beneficiary &&
        beneficiary.acc_no &&
        beneficiary.bank_name &&
        beneficiary.ifsc &&
        beneficiary.first_name
      ) {
        let res = await this._hasSufficientBalance(
          {
            aggregatorId: this.aggregatorId,
            amount: transfer.src_amount,
            src_currency: transfer.src_currency,
            secretKey: this.secretKey
          },
          service
        );
        let collectionWalletRes = await this._hasCollectionWalletResSufficientBalance(
          {
            merchant_account_id: transfer.merchant_account_id,
            amount: transfer.src_amount,
            src_currency: transfer.src_currency
          },
          service
        );
        if (res.success && collectionWalletRes.success) {
          let transferRequest = {
            accountNo: beneficiary.acc_no,
            aggregatorId: this.aggregatorId,
            amount: transfer.src_amount,
            bankName: beneficiary.bank_name,
            beneName: beneficiary.first_name + " " + beneficiary.last_name,
            ifscCode: beneficiary.ifsc,
            merchantTransId: transfer.transfer_id,
            transferType: transfer.transfer_request.order.protocol,
            secretKey: this.secretKey
          };
          let fundTransferResponse = await fundTransferRequest(
            transferRequest,
            service
          );

          data.activity_action = "PENDING_TX";
          data.transfer_id = transfer.transfer_id;
          data.auto_payout = true;
          if (
            fundTransferResponse &&
            ["300", "005", "006", "007"].indexOf(fundTransferResponse.code) >= 0
          ) {
            let transaction = await Queue.newJob("transaction-service", {
              method: "txActivityPending",
              data,
              options: service
            });
            if (transaction.error && transaction.error.message) {
              console.log(
                `File: Acquirer.js Func:executeAcquirerPayment ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
              );
            }
          } else {
            let transaction = await Queue.newJob("transaction-service", {
              method: "txActivityReject",
              data,
              options: service
            });
            if (transaction.error && transaction.error.message) {
              console.log(
                `File: Acquirer.js Func:executeAcquirerPayment ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
              );
            }
          }
        }
      } else {
        console.log(
          `File: Acquirer.js Func:executeAcquirerPayment ${transfer.transfer_id} beneficiary details missing`
        );
        let transaction = await Queue.newJob("transaction-service", {
          method: "txActivityReject",
          data,
          options: service
        });
        if (transaction.error && transaction.error.message) {
          console.log(
            `File: Acquirer.js Func:executeAcquirerPayment ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
          );
        }
      }
    } catch (error) {
      console.log(
        "File: Acquirer.js Function: executeAcquirerPayment Error: " + error
      );
      throw error;
    }
  }

  async syncAutoPayoutAcquirer(data, service, transfer) {
    try {
      let transferRequest = {
        transid: transfer.transfer_id,
        aggregatorId: this.aggregatorId,
        secretKey: this.secretKey
      };
      let transStatusResponse = await getTransStatus(transferRequest, service);

      data.transfer_id = transfer.transfer_id;
      data.auto_payout = true;
      if (transStatusResponse.status === "PENDING") {
        return {
          success: true,
          message: `Transfer is in pending state please try again later`
        };
      } else if (
        transStatusResponse &&
        ["SUCCESS", "ACCEPTED"].indexOf(transStatusResponse.result.state) >= 0
      ) {
        await db.transaction.update(
          { activity: "APPROVED" },
          {
            where: {
              transfer_id: transfer.transfer_id,
              activity: "PENDING"
            }
          }
        );
        setTimeout(async () => {
          try {
            await Queue.newJob("transaction-service", {
              method: "sendWebhook",
              data: {
                transfer_id: transfer.transfer_id,
                type: db.tx_webhooks.TYPE.COLLECTION_UPDATES
              },
              options: service
            });
          } catch (err) {
            console.error("Error while sending transaction webhook", err);
          }
        });
      } else {
        data.activity_action = "REVERSE_TX";
        let transaction = await Queue.newJob("transaction-service", {
          method: "txActivityReverse",
          data,
          options: service
        });

        if (transaction.error && transaction.error.message) {
          console.log(
            `File: Acquirer.js Func:executeAutoPayout ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
          );
          return;
        }
        await db.transaction.update(
          { activity: "REJECTED" },
          {
            where: {
              transfer_id: transfer.transfer_id,
              activity: "PENDING"
            }
          }
        );
        setTimeout(async () => {
          try {
            await Queue.newJob("transaction-service", {
              method: "sendWebhook",
              data: {
                transfer_id: transfer.transfer_id,
                type: db.tx_webhooks.TYPE.COLLECTION_UPDATES
              },
              options: service
            });
          } catch (err) {
            console.error("Error while sending transaction webhook", err);
          }
        });
      }
    } catch (error) {
      console.log(
        "File: Acquirer.js Function: syncAutoPayoutAcquirer Error: " + error
      );
      throw error;
    }
  }
}

class XazurPay extends AcquirerBase {
  constructor(aggregator_id, secretKey) {
    super(aggregator_id, secretKey);
  }

  async generateRequestData(tree_map) {
    const hashAlgorithm = "sha256";
    let jsonString = JSON.stringify(tree_map);

    let index = jsonString.indexOf("amount");
    if (index !== -1) {
      index = jsonString.indexOf(":", index);
      jsonString = replaceChar(jsonString, "", index + 1);
      index = jsonString.indexOf('"', index);
      jsonString = replaceChar(jsonString, "", index);
    }
    console.log("jsonString", jsonString);

    const jsonStringNew = JSON.stringify(tree_map);

    return jsonStringNew;
  }

  async fundTransferRequest(data, service, api_credentials) {
    const requestData = await this.generateRequestData(data);
    let secretKey = decrypt(api_credentials.secret_key);
    const options = {
      method: "POST",
      url: process.env.XAZURPAY_PAYOUT_BASE_URL,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${secretKey}`
      },
      body: requestData
    };
    return new Promise((resolve, reject) => {
      var request = require("request");

      request(options, function(error, response) {
        if (error) reject(error);
        if (response) {
          console.log(
            "File: Acquirer.js .Func: fundTransferRequest. response: ",
            response.body
          );
          response = JSON.parse(response.body);
          if (response.error) {
            reject(response.error);
          } else {
            resolve(response);
          }
        } else {
          reject();
        }
      });
    });
  }

  async executeAcquirerPayment(
    data,
    service,
    beneficiary,
    transfer,
    api_credentials
  ) {
    try {
      if (!beneficiary.vpa) {
        data.note =
          "vpa is required for beneficiary account beneficiary_id is" +
          beneficiary.id;
        Transactions.txActivityReject(data, service);
      }
      let collectionWalletRes = await this._hasCollectionWalletResSufficientBalance(
        {
          merchant_account_id: transfer.merchant_account_id,
          amount: transfer.src_amount,
          src_currency: transfer.src_currency
        },
        service
      );
      if (collectionWalletRes.success) {
        let transferRequest = {
          paymentMethod: transfer.transfer_request.order.protocol,
          currency: transfer.src_currency,
          paymentType: "WITHDRAWAL",
          amount: transfer.src_amount,
          customer: {
            email: beneficiary.email,
            phone: beneficiary.country_code + " " + beneficiary.mobile
          },
          billingAddress: {
            countryCode: "IN"
          },
          webhookUrl: process.env.WEBHOOK_URL + "xazurpay-status",
          additionalParameters: {
            upiVpa: beneficiary.vpa
          }
        };

        let fundTransferResponse = await this.fundTransferRequest(
          transferRequest,
          service,
          api_credentials
        );

        data.activity_action = "PENDING_TX";
        data.transfer_id = transfer.transfer_id;
        data.auto_payout = true;
        if (
          fundTransferResponse &&
          fundTransferResponse.status === 200 &&
          !fundTransferResponse.result.errorCode
        ) {
          let transaction = await Queue.newJob("transaction-service", {
            method: "txActivityPending",
            data,
            options: service
          });
          if (transaction.error && transaction.error.message) {
            console.log(
              `File: Acquirer.js Func:executeAcquirerPayment ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
            );
          }

          db.transfer
            .update(
              {
                data: db.sequelize.literal(
                  `data || '{"xazurpay_id": ${JSON.stringify(
                    fundTransferResponse.result.id
                  )}}'::jsonb`
                )
              },
              { where: { id: data.transfer_id } }
            )
            .then(() => {
              console.log("Data updated successfully");
            })
            .catch((error) => {
              console.error("Error updating data:", error);
            });
        } else {
          let transaction = await Queue.newJob("transaction-service", {
            method: "txActivityReject",
            data,
            options: service
          });
          if (transaction.error && transaction.error.message) {
            console.log(
              `File: Acquirer.js Func:executeAcquirerPayment ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
            );
          }
        }
      }
    } catch (error) {
      console.log(
        "File: Acquirer.js Function: executeAcquirerPayment Error: " + error
      );
      throw error;
    }
  }

  async executeAcquirerCollection(data, service, api_credentials) {
    let transfer = await db.sequelize.query(
      `SELECT transfers.*, transfers_meta.transfer_request, transfers.src_currency
        FROM ${db.schema}.transfers
        JOIN ${db.schema}.transfers_meta ON transfers.id = transfers_meta.transfer_id
        WHERE transfers.id = :transfer_id;
        `,
      {
        raw: true,
        type: db.sequelize.QueryTypes.SELECT,
        replacements: {
          transfer_id: data.transfer_id
        }
      }
    );

    let transferRequest = {
      paymentMethod: transfer[0].transfer_request.order.protocol || "UPI",
      currency: transfer[0].src_currency,
      paymentType: "DEPOSIT",
      amount: transfer[0].src_amount,
      customer: {
        email: transfer[0].transfer_request.email,
        phone: transfer[0].transfer_request.phone
      },
      billingAddress: {
        countryCode: "IN"
      },
      webhookUrl: process.env.WEBHOOK_URL + "xazurpay-status"
    };

    let fundTransferResponse = await this.fundTransferRequest(
      transferRequest,
      service,
      api_credentials
    );
    data.transfer_id = transfer[0].id;
    data.auto_payout = true;
    if (
      fundTransferResponse &&
      fundTransferResponse.status === 200 &&
      !fundTransferResponse.result.errorCode
    ) {
      db.transfer
        .update(
          {
            data: db.sequelize.literal(
              `data || '{"xazurpay_id": ${JSON.stringify(
                fundTransferResponse.result.id
              )}}'::jsonb`
            )
          },
          { where: { id: data.transfer_id } }
        )
        .then(() => {
          console.log("Data updated successfully");
        })
        .catch((error) => {
          console.error("Error updating data:", error);
        });
      return {
        id: data.transfer_id,
        payment_url: fundTransferResponse.result.redirectUrl
      };
    } else {
      let transaction = await Queue.newJob("transaction-service", {
        method: "txActivityReject",
        data,
        options: service
      });
      if (transaction.error && transaction.error.message) {
        console.log(
          `File: Acquirer.js Func:executeAcquirerPayment ${transfer[0].id} ${transaction.error.message} has failed to be marked. Please try again`
        );
      }
    }
  }

  async getTransStatus(data, service, api_credentials, transfer) {
    let secretKey = decrypt(api_credentials.secret_key);
    const options = {
      method: "GET",
      url:
        process.env.XAZURPAY_PAYOUT_BASE_URL + `/${transfer.data.xazurpay_id}`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${secretKey}`
      }
    };
    return new Promise((resolve, reject) => {
      var request = require("request");

      request(options, function(error, response) {
        if (error) reject(error);

        if (response && response.body) {
          console.log(response.body);
          resolve(JSON.parse(response.body));
        } else resolve(response);
      });
    });
  }

  async syncAutoPayoutAcquirer(data, service, transfer, api_credentials) {
    try {
      let transferRequest = {
        transid: transfer.transfer_id
      };
      let transStatusResponse = await this.getTransStatus(
        transferRequest,
        service,
        api_credentials,
        transfer
      );

      data.transfer_id = transfer.transfer_id;
      data.auto_payout = true;
      if (
        transStatusResponse &&
        transStatusResponse.status === 200 &&
        ["CHECKOUT", "PENDING"].indexOf(transStatusResponse.result.state) >= 0
      ) {
        return {
          success: true,
          message: `Transfer is in pending state please try again later`
        };
      } else if (
        transStatusResponse &&
        transStatusResponse.result &&
        transStatusResponse.result.state &&
        ["COMPLETED"].indexOf(transStatusResponse.result.state) >= 0
      ) {
        await db.transaction.update(
          { activity: "APPROVED" },
          {
            where: {
              transfer_id: transfer.transfer_id,
              activity: "PENDING"
            }
          }
        );
        setTimeout(async () => {
          try {
            await Queue.newJob("transaction-service", {
              method: "sendWebhook",
              data: {
                transfer_id: transfer.transfer_id,
                type: db.tx_webhooks.TYPE.COLLECTION_UPDATES
              },
              options: service
            });
          } catch (err) {
            console.error("Error while sending transaction webhook", err);
          }
        });
      } else {
        data.activity_action = "REVERSE_TX";
        let transaction = await Queue.newJob("transaction-service", {
          method: "txActivityReverse",
          data,
          options: service
        });

        if (transaction.error && transaction.error.message) {
          console.log(
            `File: Acquirer.js Func:executeAutoPayout ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
          );
          return;
        }
        await db.transaction.update(
          { activity: "REJECTED" },
          {
            where: {
              transfer_id: transfer.transfer_id,
              activity: "PENDING"
            }
          }
        );
        setTimeout(async () => {
          try {
            await Queue.newJob("transaction-service", {
              method: "sendWebhook",
              data: {
                transfer_id: transfer.transfer_id,
                type: db.tx_webhooks.TYPE.COLLECTION_UPDATES
              },
              options: service
            });
          } catch (err) {
            console.error("Error while sending transaction webhook", err);
          }
        });
      }
    } catch (error) {
      console.log(
        "File: Acquirer.js Function: syncAutoPayoutAcquirer Error: " + error
      );
      throw error;
    }
  }

  async syncAutoCollectionAcquirer(data, service, transfer, api_credentials) {
    try {
      let transferRequest = {
        transid: transfer.transfer_id
      };
      let transStatusResponse = await this.getTransStatus(
        transferRequest,
        service,
        api_credentials,
        transfer
      );

      data.transfer_id = transfer.transfer_id;
      data.auto_payout = true;
      if (
        transStatusResponse &&
        transStatusResponse.status === 200 &&
        ["PENDING", "CHECKOUT"].indexOf(transStatusResponse.result.state) >= 0
      ) {
        return {
          success: true,
          message: `Transfer is in pending state please try again later`
        };
      } else if (
        transStatusResponse &&
        transStatusResponse.status === 200 &&
        ["COMPLETED"].indexOf(transStatusResponse.result.state) >= 0
      ) {
        data.activity_action = "APPROVE_TX";
        let transaction = await Queue.newJob("transaction-service", {
          method: "txActivityApprove",
          data,
          options: service
        });
        if (transaction.error && transaction.error.message) {
          console.log(
            `File: Acquirer.js Func:syncAutoPayoutAcquirer ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
          );
          return;
        }
        setTimeout(async () => {
          try {
            await Queue.newJob("transaction-service", {
              method: "sendWebhook",
              data: {
                transfer_id: transfer.transfer_id,
                type: db.tx_webhooks.TYPE.COLLECTION_UPDATES
              },
              options: service
            });
          } catch (err) {
            console.error("Error while sending transaction webhook", err);
          }
        });
      } else {
        data.activity_action = "REJECT_TX";
        let transaction = await Queue.newJob("transaction-service", {
          method: "txActivityReject",
          data,
          options: service
        });

        if (transaction.error && transaction.error.message) {
          console.log(
            `File: Acquirer.js Func:syncAutoPayoutAcquirer ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
          );
          return;
        }
        setTimeout(async () => {
          try {
            await Queue.newJob("transaction-service", {
              method: "sendWebhook",
              data: {
                transfer_id: transfer.transfer_id,
                type: db.tx_webhooks.TYPE.COLLECTION_UPDATES
              },
              options: service
            });
          } catch (err) {
            console.error("Error while sending transaction webhook", err);
          }
        });
      }
    } catch (error) {
      console.log(
        "File: Acquirer.js Function: syncAutoPayoutAcquirer Error: " + error
      );
      throw error;
    }
  }
}

class DigiPayment extends AcquirerBase {
  constructor(aggregator_id, secretKey) {
    super(aggregator_id, secretKey);
  }

  async generateRequestData(data) {
    const hash = crypto.createHash("md5");
    hash.update(data.data);
    return hash.digest("hex");
  }
  async callDigiPaymentApi(data, service) {
    try {
      const options = {
        method: "POST",
        url: process.env.DIGIPAYMENT_PAYOUT_BASE_URL + data.method,
        headers: {
          "Content-Type": "application/json"
        },
        data: data.requestData
      };
      console.log(
        "File Acquirer.js Func: callDigiPaymentApi request: " + options
      );
      return new Promise((resolve, reject) => {
        axios(options)
          .then((response) => {
            console.log(
              "File Acquirer.js Func: callDigiPaymentApi response: " + response
            );
            if (response.data) {
              resolve(response.data);
            } else {
              reject(response);
            }
          })
          .catch((error) => {
            console.log(
              "File Acquirer.js Func: callDigiPaymentApi error: " + error
            );
            if (error && error.response && error.response.data) {
              reject(error.response.data);
            } else if (error && error.message) {
              reject(error.message);
            } else {
              reject(error);
            }
          });
      });
    } catch (error) {
      console.log("File Acquirer.js Func: callDigiPaymentApi Error: " + error);
      throw error;
    }
  }

  async getAcquirerBanks(data, service, api_credentials) {
    try {
      let transfer = await db.sequelize.query(
        `SELECT transfers.*, transfers_meta.transfer_request, transfers.src_currency
        FROM ${db.schema}.transfers
        JOIN ${db.schema}.transfers_meta ON transfers.id = transfers_meta.transfer_id
        WHERE transfers.id = :transfer_id;
        `,
        {
          raw: true,
          type: db.sequelize.QueryTypes.SELECT,
          replacements: {
            transfer_id: data.transfer_id
          }
        }
      );

      let secretKey = decrypt(api_credentials.secret_key);
      const signature = await this.generateRequestData({
        data: `*${secretKey}|${transfer[0].src_currency}`
      });

      const requestData = {
        apiKey: secretKey,
        currencyCode: transfer[0].src_currency,
        signature
      };
      const resp = await this.callDigiPaymentApi(
        { requestData, method: "getbanks" },
        service
      );
      let selected_bank_account = null;
      if (resp && resp.response && resp.response.length) {
        let bank_accounts = resp.response;
        if (
          !!transfer[0].transfer_request.protocols &&
          transfer[0].transfer_request.protocols.length
        ) {
          if (transfer[0].transfer_request.protocols.indexOf("UPI") >= 0) {
            for (let bank_account of bank_accounts) {
              if (bank_account.dynamic_qr !== null) {
                selected_bank_account = bank_account;
                break;
              }
            }
          } else if (
            transfer[0].transfer_request.protocols.indexOf("IMPS") ||
            transfer[0].transfer_request.protocols.indexOf("NEFT") ||
            transfer[0].transfer_request.protocols.indexOf("RTGS")
          ) {
            for (let bank_account of bank_accounts) {
              if (
                bank_account.name !== null &&
                bank_account.account_number !== null &&
                bank_account.bank_ifsc !== null
              ) {
                selected_bank_account = bank_account;
                break;
              }
            }
          }
        } else {
          for (let bank_account of bank_accounts) {
            if (
              (bank_account.name !== null &&
                bank_account.account_number !== null &&
                bank_account.bank_ifsc !== null) ||
              bank_account.dynamic_qr !== null
            ) {
              selected_bank_account = bank_account;
              break;
            }
          }
        }
        if (selected_bank_account != null) {
          db.transfer
            .update(
              {
                data: db.sequelize.literal(
                  `data || '{"digiPaymentBankAcc": ${JSON.stringify(
                    selected_bank_account
                  )}}'::jsonb`
                )
              },
              { where: { id: data.transfer_id } }
            )
            .then(() => {
              console.log("Data updated successfully");
            })
            .catch((error) => {
              console.error("Error updating data:", error);
            });
        }
      }
    } catch (error) {
      console.log(
        "File Acquirer.js Func: getAcquirerBanks error :" +
          JSON.stringify(error)
      );
      throw error;
    }
  }
  async executeAcquirerCollection(data, service, api_credentials) {
    try {
      const transfer = await db.transfer.findOne({
        attributes: ["id", "data"],
        where: {
          id: data.transfer_id,
          removed: 0
        }
      });
      if (!transfer) {
        console.log(
          "File Acquirer(DigiPayment) func executeAcquirerCollection Invalid transfer Id " +
            data.transfer_id
        );
        return null;
      }
      console.log(
        "executeAcquirerCollection " +
          JSON.stringify(data) +
          "transfer data " +
          JSON.stringify(transfer.data)
      );
      if (
        transfer &&
        transfer.data &&
        transfer.data.digiPaymentBankAcc &&
        data.brn
      ) {
        return await this.createDepositRequest(data, service, api_credentials);
      }
      await this.getAcquirerBanks(data, service, api_credentials);
      return {
        id: data.transfer_id,
        payment_url: `${config.paymentPage.baseUrl}${data.transfer_id}`
      };
    } catch (error) {
      console.log(
        "File Acquirer.js Func: executeAcquirerCollection error :" +
          JSON.stringify(error)
      );
      throw error;
    }
  }
  async createDepositRequest(data, service, api_credentials) {
    try {
      const transfer = await db.transfer.findOne({
        where: {
          id: data.transfer_id,
          removed: 0
        }
      });
      let secretKey = decrypt(api_credentials.secret_key);
      const deposit = 2;

      const webhookUrl = process.env.WEBHOOK_URL + "digiPayment-status";
      const signature = await this.generateRequestData({
        data: `*${secretKey}|${transfer.dst_currency}|${transfer.id}|${deposit}|${transfer.data.digiPaymentBankAcc.name}|${transfer.dst_amount}|${data.brn}|${webhookUrl}`
      });

      const requestData = {
        apiKey: secretKey,
        currencyCode: transfer.dst_currency,
        referenceId: transfer.id,
        type: deposit,
        bank_type: transfer.data.digiPaymentBankAcc.name,
        amount: transfer.dst_amount,
        proof: data.brn,
        webhookUrl,
        signature
      };

      const resp = await this.callDigiPaymentApi(
        { requestData, method: "_initiate" },
        service
      );
      if (resp && resp.code === "1100") {
        db.transfer
          .update(
            {
              data: db.sequelize.literal(
                `data || '{"digiPaymentTrxnSignatureHash": ${JSON.stringify(
                  resp.trxnSignatureHash
                )}}'::jsonb`
              )
            },
            { where: { id: data.transfer_id } }
          )
          .then(() => {
            console.log("Data updated successfully");
          })
          .catch((error) => {
            console.error("Error updating data:", error);
          });
        return true;
      } else {
        console.log(
          "File Acquirer.js Func: createDepositRequest _initiate resp" +
            ` code: ${resp.code}, message: ${resp.message}`
        );
      }
    } catch (error) {
      console.log(
        "File Acquirer.js Func: createDepositRequest error :" +
          JSON.stringify(error)
      );
      throw error;
    }
  }
  async getTransStatus(data, service, api_credentials, transfer) {
    let secretKey = decrypt(api_credentials.secret_key);
    const signature = await this.generateRequestData({
      data: `*${secretKey}|${transfer.data.digiPaymentTrxnSignatureHash}`
    });
    const requestData = {
      apiKey: secretKey,
      trxnSignatureHash: transfer.data.digiPaymentTrxnSignatureHash,
      signature
    };

    const resp = await this.callDigiPaymentApi(
      { requestData, method: "sync" },
      service
    );
    return resp;
  }
  async syncAutoCollectionAcquirer(data, service, transfer, api_credentials) {
    try {
      if (
        transfer &&
        transfer.data &&
        transfer.data.digiPaymentTrxnSignatureHash
      ) {
        let transStatusResponse = await this.getTransStatus(
          data,
          service,
          api_credentials,
          transfer
        );

        data.transfer_id = transfer.transfer_id;
        data.auto_payout = true;
        if (
          transStatusResponse &&
          transStatusResponse.code === "1100" &&
          transStatusResponse.response &&
          transStatusResponse.response.status &&
          ["Pending"].indexOf(transStatusResponse.response.status) >= 0
        ) {
          return {
            success: true,
            message: `Transfer is in pending state please try again later`
          };
        } else if (
          transStatusResponse &&
          transStatusResponse.code === "1100" &&
          transStatusResponse.response &&
          transStatusResponse.response.status &&
          ["Confirmed"].indexOf(transStatusResponse.response.status) >= 0
        ) {
          data.activity_action = "APPROVE_TX";
          let transaction = await Queue.newJob("transaction-service", {
            method: "txActivityApprove",
            data,
            options: service
          });
          if (transaction.error && transaction.error.message) {
            console.log(
              `File: Acquirer.js Func:syncAutoPayoutAcquirer ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
            );
            return;
          }
          setTimeout(async () => {
            try {
              await Queue.newJob("transaction-service", {
                method: "sendWebhook",
                data: {
                  transfer_id: transfer.transfer_id,
                  type: db.tx_webhooks.TYPE.COLLECTION_UPDATES
                },
                options: service
              });
            } catch (err) {
              console.error("Error while sending transaction webhook", err);
            }
          });
        } else if (
          transStatusResponse &&
          transStatusResponse.code === "1100" &&
          transStatusResponse.response &&
          transStatusResponse.response.status &&
          ["Declined"].indexOf(transStatusResponse.response.status) >= 0
        ) {
          data.activity_action = "REJECT_TX";
          let transaction = await Queue.newJob("transaction-service", {
            method: "txActivityReject",
            data,
            options: service
          });

          if (transaction.error && transaction.error.message) {
            console.log(
              `File: Acquirer.js Func:syncAutoPayoutAcquirer ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
            );
            return;
          }
          setTimeout(async () => {
            try {
              await Queue.newJob("transaction-service", {
                method: "sendWebhook",
                data: {
                  transfer_id: transfer.transfer_id,
                  type: db.tx_webhooks.TYPE.COLLECTION_UPDATES
                },
                options: service
              });
            } catch (err) {
              console.error("Error while sending transaction webhook", err);
            }
          });
        }
      } else {
        return;
      }
    } catch (error) {
      console.log(
        "File: Acquirer.js Function: syncAutoPayoutAcquirer Error: " + error
      );
      throw error;
    }
  }
  async syncAutoPayoutAcquirer(data, service, transfer, api_credentials) {
    try {
      if (
        transfer &&
        transfer.data &&
        transfer.data.digiPaymentTrxnSignatureHash
      ) {
        let transStatusResponse = await this.getTransStatus(
          data,
          service,
          api_credentials,
          transfer
        );

        data.transfer_id = transfer.transfer_id;
        data.auto_payout = true;
        if (
          transStatusResponse &&
          transStatusResponse.code === "1100" &&
          transStatusResponse.response &&
          transStatusResponse.response.status &&
          ["Pending"].indexOf(transStatusResponse.response.status) >= 0
        ) {
          return {
            success: true,
            message: `Transfer is in pending state please try again later`
          };
        } else if (
          transStatusResponse &&
          transStatusResponse.code === "1100" &&
          transStatusResponse.response &&
          transStatusResponse.response.status &&
          ["Confirmed"].indexOf(transStatusResponse.response.status) >= 0
        ) {
          await db.transaction.update(
            { activity: "APPROVED" },
            {
              where: {
                transfer_id: transfer.transfer_id,
                activity: "PENDING"
              }
            }
          );
          setTimeout(async () => {
            try {
              await Queue.newJob("transaction-service", {
                method: "sendWebhook",
                data: {
                  transfer_id: transfer.transfer_id,
                  type: db.tx_webhooks.TYPE.COLLECTION_UPDATES
                },
                options: service
              });
            } catch (err) {
              console.error("Error while sending transaction webhook", err);
            }
          });
        } else if (
          transStatusResponse &&
          transStatusResponse.code === "1100" &&
          transStatusResponse.response &&
          transStatusResponse.response.status &&
          ["Declined"].indexOf(transStatusResponse.response.status) >= 0
        ) {
          data.activity_action = "REVERSE_TX";
          let transaction = await Queue.newJob("transaction-service", {
            method: "txActivityReverse",
            data,
            options: service
          });

          if (transaction.error && transaction.error.message) {
            console.log(
              `File: Acquirer.js Func:executeAutoPayout ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
            );
            return;
          }
          await db.transaction.update(
            { activity: "REJECTED" },
            {
              where: {
                transfer_id: transfer.transfer_id,
                activity: "PENDING"
              }
            }
          );
          setTimeout(async () => {
            try {
              await Queue.newJob("transaction-service", {
                method: "sendWebhook",
                data: {
                  transfer_id: transfer.transfer_id,
                  type: db.tx_webhooks.TYPE.COLLECTION_UPDATES
                },
                options: service
              });
            } catch (err) {
              console.error("Error while sending transaction webhook", err);
            }
          });
        }
      }
    } catch (error) {
      console.log(
        "File: Acquirer.js Function: syncAutoPayoutAcquirer Error: " + error
      );
      throw error;
    }
  }
  async getAcquirerSupportNetBank(secretKey, currency, transfer_id, service) {
    try {
      const signature = await this.generateRequestData({
        data: `*${secretKey}|${currency}`
      });

      const requestData = {
        apiKey: secretKey,
        currencyCode: currency,
        signature
      };
      const resp = await this.callDigiPaymentApi(
        { requestData, method: "getbanks" },
        service
      );
      let selected_bank_account = null;
      if (resp && resp.response && resp.response.length) {
        let bank_accounts = resp.response;

        for (let bank_account of bank_accounts) {
          if (
            bank_account.name !== null &&
            bank_account.account_number !== null &&
            bank_account.bank_ifsc !== null
          ) {
            selected_bank_account = bank_account;
            break;
          }
        }

        if (selected_bank_account != null) {
          db.transfer
            .update(
              {
                data: db.sequelize.literal(
                  `data || '{"digiPaymentBankAcc": ${JSON.stringify(
                    selected_bank_account
                  )}}'::jsonb`
                )
              },
              { where: { id: transfer_id } }
            )
            .then(() => {
              console.log("Data updated successfully");
            })
            .catch((error) => {
              console.error("Error updating data:", error);
            });
        }
        return selected_bank_account;
      }
    } catch (error) {
      console.log(
        "File Acquirer.js Func: getAcquirerSupportNetBank error :" +
          JSON.stringify(error)
      );
      throw error;
    }
  }
  async executeAcquirerPayment(
    data,
    service,
    beneficiary,
    transfer,
    api_credentials
  ) {
    try {
      if (
        beneficiary &&
        beneficiary.acc_no &&
        beneficiary.bank_name &&
        beneficiary.ifsc &&
        beneficiary.first_name
      ) {
        let collectionWalletRes = await this._hasCollectionWalletResSufficientBalance(
          {
            merchant_account_id: transfer.merchant_account_id,
            amount: transfer.src_amount,
            src_currency: transfer.src_currency
          },
          service
        );
        if (collectionWalletRes.success) {
          const transfer = await db.transfer.findOne({
            where: {
              id: data.transfer_id,
              removed: 0
            }
          });
          let secretKey = decrypt(api_credentials.secret_key);
          const acquirerBank = await this.getAcquirerSupportNetBank(
            secretKey,
            transfer.dst_currency,
            data.transfer_id,
            service
          );
          if (!acquirerBank) {
            data.note = "Acquirer bank account details is required";

            Transactions.txActivityReject(data, service);
            return;
          }
          const type = 3;

          const webhookUrl = process.env.WEBHOOK_URL + "digiPayment-status";
          const signature = await this.generateRequestData({
            data: `*${secretKey}|${transfer.dst_currency}|${transfer.id}|${type}|${acquirerBank.name}|${transfer.dst_amount}|${webhookUrl}`
          });

          const requestData = {
            apiKey: secretKey,
            currencyCode: transfer.dst_currency,
            referenceId: transfer.id,
            type,
            bank_type: acquirerBank.name,
            amount: transfer.dst_amount,
            webhookUrl,
            bank_name: beneficiary.bank_name,
            account_name: beneficiary.first_name + " " + beneficiary.last_name,
            account_number: beneficiary.acc_no,
            bank_ifsc: beneficiary.ifsc,
            signature
          };

          const resp = await this.callDigiPaymentApi(
            { requestData, method: "_initiate" },
            service
          );
          data.activity_action = "PENDING_TX";
          data.transfer_id = transfer.id;
          data.auto_payout = true;
          if (resp && resp.code === "1100") {
            let transaction = await Queue.newJob("transaction-service", {
              method: "txActivityPending",
              data,
              options: service
            });
            if (transaction.error && transaction.error.message) {
              console.log(
                `File: Acquirer.js Func:executeAcquirerPayment ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
              );
            }
            db.transfer
              .update(
                {
                  data: db.sequelize.literal(
                    `data || '{"digiPaymentTrxnSignatureHash": ${JSON.stringify(
                      resp.trxnSignatureHash
                    )}}'::jsonb`
                  )
                },
                { where: { id: data.transfer_id } }
              )
              .then(() => {
                console.log("Data updated successfully");
              })
              .catch((error) => {
                console.error("Error updating data:", error);
              });
            return true;
          } else {
            console.log(
              "File Acquirer.js Func: createDepositRequest _initiate resp" +
                ` code: ${resp.code}, message: ${resp.message}`
            );

            let transaction = await Queue.newJob("transaction-service", {
              method: "txActivityReject",
              data,
              options: service
            });
            if (transaction.error && transaction.error.message) {
              console.log(
                `File: Acquirer.js Func:executeAcquirerPayment ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
              );
            }
          }
        } else {
          console.log(
            `File: Acquirer.js Func:executeAcquirerPayment ${data.transfer_id}  insufficent balance. Please try again`
          );
        }
      } else {
        data.note =
          "beneficiary bank account details is required for beneficiary account beneficiary_id is" +
          beneficiary.id;
        Transactions.txActivityReject(data, service);
      }
    } catch (error) {
      console.log(
        "File: Acquirer.js Function: executeAcquirerPayment Error: " + error
      );
      throw error;
    }
  }
}

function encrypt(text) {
  try {
    const { iv, derivedKey } = getEncryptionConfiguration();

    const cipher = crypto.createCipheriv("aes-256-cbc", derivedKey, iv);
    let encrypted = cipher.update(text, "utf8", "hex");
    encrypted += cipher.final("hex");
    return encrypted;
  } catch (error) {
    throw error;
  }
}

function decrypt(encryptedText) {
  try {
    const { iv, derivedKey } = getEncryptionConfiguration();
    const decipher = crypto.createDecipheriv("aes-256-cbc", derivedKey, iv);
    let decrypted = decipher.update(encryptedText, "hex", "utf8");
    decrypted += decipher.final("utf8");
    return decrypted;
  } catch (error) {
    throw error;
  }
}

function getEncryptionConfiguration() {
  const iv = Buffer.from(process.env.API_CREDENTIALS_ENCRYPTION_IV, "hex");
  const salt = Buffer.from(process.env.API_CREDENTIALS_ENCRYPTION_SALT, "hex");
  const derivedKey = crypto.pbkdf2Sync(
    process.env.API_CREDENTIALS_ENCRYPTION_SECRET_KEY,
    salt,
    100000,
    32,
    "sha256"
  );
  return {
    iv,
    derivedKey
  };
}

export default {
  fundTransferRequest,
  getWalletBalanceDtl,
  getTransStatus,
  encrypt,
  decrypt,
  BanjoPay,
  XazurPay,
  DigiPayment
};
