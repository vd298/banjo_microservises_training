require("dotenv").config({ path: "../../../../.env" });

const express = require("express");
const { Sequelize, DataTypes } = require("sequelize");

// DB connection
const sequelize = new Sequelize(process.env.DB_CONN_STRING, {
  dialect: "postgres",
});

// Student model
// const Student = sequelize.define(
//   "Student",
//   {
//     id: {
//       type: DataTypes.INTEGER,
//       primaryKey: true,
//       autoIncrement: true,
//     },
//     name: {
//       type: DataTypes.STRING,
//       allowNull: false,
//     },
//   },
//   {
//     schema: process.env.DB_SCHEMA,
//   }
// );
const Student = sequelize.define(
  "Student",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: "Name cannot be empty",
        },
        len: {
          args: [2, 50],
          msg: "Name must be between 2 and 50 characters",
        },
      },
    },
  },
  {
    schema: process.env.DB_SCHEMA,
  }
);

async function DetailsOfStudent(data, service) {
  console.log(`Inside of student details`);
  const studentId = data.studentId;
  try {
    const student = await Student.findOne({
      where: { id: studentId },
    });
    if (!student) {
      throw new service.ServiceError("VINAY_ERROR_TEST");
    }
    //res.json(student);
    return student;
  } catch (error) {
    console.log("Error fetching student:", error);
    //res.status(500).json(error.message);
    throw error;
  }
}

export default { DetailsOfStudent };
