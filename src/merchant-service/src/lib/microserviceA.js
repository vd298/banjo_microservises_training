// // // require("dotenv").config({ path: "../../../../.env" });

// // // const { Sequelize, DataTypes } = require("sequelize");
// // // //const queue = require("./queue"); // Assuming the queue module is in the same directory
// // // import queue from "@lib/queue";

// // // // DB connection
// // // const sequelize = new Sequelize(process.env.DB_CONN_STRING, {
// // //   dialect: "postgres",
// // // });

// // // // Student model
// // // const Student = sequelize.define(
// // //   "Student",
// // //   {
// // //     id: {
// // //       type: DataTypes.INTEGER,
// // //       primaryKey: true,
// // //       autoIncrement: true,
// // //     },
// // //     name: {
// // //       type: DataTypes.STRING,
// // //       allowNull: false,
// // //     },
// // //   },
// // //   {
// // //     schema: process.env.DB_SCHEMA,
// // //   }
// // // );

// // // (async () => {
// // //   await sequelize.sync();
// // //   console.log("Student table synced successfully");
// // // })();

// // // async function DetailsOfStudent(data, service) {
// // //   console.log(`Inside of student details`);
// // //   const studentId = data.studentId;
// // //   try {
// // //     // Publish the studentId to the 'studentId' queue
// // //     queue.publish("studentId", { studentId });
// // //     console.log("StudentId published to the queue.");

// // //     // Subscribe to the 'combinedResult' queue to receive the combined result
// // //     queue.subscribe("combinedResult", (combinedResult) => {
// // //       console.log("Received combined result:", combinedResult);
// // //       // Process the combined result as needed
// // //     });
// // //   } catch (error) {
// // //     console.error("Error publishing studentId:", error);
// // //     // Handle error
// // //   }
// // // }

// // // export default { DetailsOfStudentMicroA };
// // require("dotenv").config({ path: "../../../../.env" });
// // const { Sequelize, DataTypes } = require("sequelize");
// // import queue from "@lib/queue";

// // // DB connection
// // const sequelize = new Sequelize(process.env.DB_CONN_STRING, {
// //   dialect: "postgres",
// // });

// // // Student model
// // const Student = sequelize.define(
// //   "Student",
// //   {
// //     id: {
// //       type: DataTypes.INTEGER,
// //       primaryKey: true,
// //       autoIncrement: true,
// //     },
// //     name: {
// //       type: DataTypes.STRING,
// //       allowNull: false,
// //     },
// //   },
// //   {
// //     schema: process.env.DB_SCHEMA,
// //   }
// // );

// // (async () => {
// //   await sequelize.sync();
// //   console.log("Student table synced successfully");
// // })();

// // async function DetailsOfStudentMicroA(data, service) {
// //   console.log(`Inside of student details`);
// //   const studentId = data.studentId;
// //   try {
// //     queue.publish("studentId", { studentId });
// //     console.log("StudentId published to the queue.");

// //     queue.subscribe("combinedResult", (combinedResult) => {
// //       console.log("Received combined result:", combinedResult);
// //     });
// //     return combinedResult;
// //   } catch (error) {
// //     console.error("Error publishing studentId:", error);
// //   }
// // }

// // export default { DetailsOfStudentMicroA };
// require("dotenv").config({ path: "../../../../.env" });
// const { Sequelize, DataTypes } = require("sequelize");
// import queue from "@lib/queue";

// // DB connection
// const sequelize = new Sequelize(process.env.DB_CONN_STRING, {
//   dialect: "postgres",
// });

// // Student model
// const Student = sequelize.define(
//   "Student",
//   {
//     id: {
//       type: DataTypes.INTEGER,
//       primaryKey: true,
//       autoIncrement: true,
//     },
//     name: {
//       type: DataTypes.STRING,
//       allowNull: false,
//     },
//   },
//   {
//     schema: process.env.DB_SCHEMA,
//   }
// );

// (async () => {
//   await sequelize.sync();
//   console.log("Student table synced successfully");
// })();

// async function DetailsOfStudentMicroA(data, service) {
//   console.log(`Inside of student details`);
//   const studentId = data.studentId;
//   try {
//     // Publish the studentId to the 'studentId' queue
//     queue.publish("studentId", { studentId });
//     console.log("StudentId published to the queue.");

//     // Subscribe to the 'studentInfo' queue to receive the student information
//     const studentInfo = await new Promise((resolve, reject) => {
//       queue.subscribe("studentInfo", (info) => {
//         console.log("Received student info:", info);
//         // Resolve the promise with the student information
//         resolve(info);
//         // Unsubscribe from the queue
//         queue.unsubscribe("studentInfo");
//       });
//     });

//     // Subscribe to the 'isTopper' queue to receive the isTopper value
//     const isTopper = await new Promise((resolve, reject) => {
//       queue.subscribe("isTopper", (topper) => {
//         console.log("Received isTopper:", topper);
//         // Resolve the promise with the isTopper value
//         resolve(topper);
//         // Unsubscribe from the queue
//         queue.unsubscribe("isTopper");
//       });
//     });

//     // Combine the results
//     const combinedResult = {
//       studentInfo,
//       isTopper,
//     };

//     // Return the combined result
//     return combinedResult;
//   } catch (error) {
//     console.error("Error publishing studentId:", error);
//     // Handle error
//     throw error;
//   }
// }

// export default { DetailsOfStudentMicroA };
