require("dotenv").config({ path: "../../../../.env" });

const express = require("express");
const { Sequelize, DataTypes } = require("sequelize");

// DB connection
const sequelize = new Sequelize(process.env.DB_CONN_STRING, {
  dialect: "postgres",
});

// Student model
const Student = sequelize.define(
  "Student",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    schema: process.env.DB_SCHEMA,
  }
);

(async () => {
  await sequelize.sync();
  console.log("Student table synced successfully");
})();

const app = express();
const PORT = process.env.PORT || 8008;

app.get("/students/:id", async (req, res) => {
  const studentId = req.params.id;

  try {
    const student = await Student.findOne({
      where: { id: studentId },
    });

    if (!student) {
      return res.status(404).json({ message: "Student not found" });
    }

    // Send the student details as a JSON response
    res.json(student);
  } catch (error) {
    console.error("Error fetching student:", error);
    res.status(500).json({ message: "Internal server error" });
  }
});

// Start the server
app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
