import db from "@lib/db";
import Queue from "@lib/queue";
import config from "@lib/config";
import QRCode from "qrcode";
import { v4 as uuid } from "uuid";
import Acquirer from "./Acquirer";
import Wallet from "./wallet";
import puppeteer from "puppeteer";
import cheerio from "cheerio";
import QrCode from "qrcode-reader";
import Jimp from "jimp";
import svg2img from "svg2img";
import axios from "axios";

const modules = {
  "Xazur Pay": Acquirer.XazurPay,
  "Banjo Pay": Acquirer.BanjoPay,
  DigiPayment: Acquirer.DigiPayment
};
async function checkVerifiedUser(data, service) {
  try {
    if (!data.user_id) {
      return 0;
    }
    let query = `select count(vtt.transfer_id) from ${db.schema}.vw_tx_transfer vtt
      where vtt.activity = 'APPROVED' and vtt.data->>'user_id' = '${data.user_id}'`;

    const transfers = await db.sequelize.query(query, {
      type: db.sequelize.QueryTypes.SELECT,
      raw: true
    });
    if (
      !!transfers &&
      transfers.length &&
      transfers[0].count &&
      transfers[0].count >= config.limitSuccessTransactions
    ) {
      return 1;
    } else return 0;
  } catch (error) {
    console.log(
      "File: transactions.js Function: checkVerifiedUser Error: " + error
    );
    throw error;
  }
}
async function collectionRequest(data, service) {
  let bankAccountAssociation;
  await Queue.newJob("account-service", {
    method: "checkValidRealmId",
    data: {},
    options: {
      realmId: service.realmId
    }
  });
  const isVerified = await checkVerifiedUser(data, service);
  let merchant_account = await db.merchant_accounts.findOne({
    where: { id: service.maId },
    attributes: ["auto_payouts", "id", "account_id"]
  });
  data.merchant_account = merchant_account;
  if (merchant_account.auto_payouts === 1) {
    bankAccountAssociation = await fetchAcquirerBankAccount(data, service);
  } else {
    bankAccountAssociation = await fetchBankAccount(
      { ...data, isVerified },
      service
    );

    if (!isVerified && !bankAccountAssociation) {
      bankAccountAssociation = await fetchBankAccount(
        { ...data, isVerified: 1 },
        service
      );
    }
  }

  let bankAccountRec;
  if (bankAccountAssociation) {
    const res = await Queue.newJob("transaction-service", {
      method: "collectionTrigger",
      data: {
        description: data.order.description,
        action: "COLLECTION_REQUEST",
        currency: data.currency,
        amount: data.amount,
        order_id: data.order.order_id,
        bank_account_id: bankAccountAssociation.bank_account_id,
        wallet_id: bankAccountAssociation.wallet_id,
        data: {
          ua: service.ua,
          ip: service.ip,
          user_ip: data.user_ip,
          user_id: data.user_id,
          return_url: data.return_url,
          payer_name: data.payer_name,
          payer_email: data.payer_email,
          payer_phone: data.payer_phone,
          payer_vpa: data.payer_vpa
        }
      },
      options: {
        ...service,
        realmId: bankAccountAssociation.realm_id
      }
    });
    if (res.result) {
      if (!res.result.transfer_id) {
        console.error(
          "merchant-service. Func: collectionRequest. No Transfer Id generated"
        );
        throw new service.ServiceError("BANJO_ERR_TARIFF_CONFIG");
      }
      await db.bank_accounts.update(
        {
          request_timestamp: new Date()
        },
        {
          where: {
            id: bankAccountAssociation.bank_account_id
          }
        }
      );
      data.processed = 0;
      data.header = { method: "collectionRequest" };
      await db.transfers_meta.create({
        transfer_request: data,
        transfer_response: res.result,
        transfer_id: res.result.transfer_id
      });
      Queue.newJob("transaction-service", {
        method: "callTelegramBot",
        data: {
          transfer_id: res.result.transfer_id,
          action_type: "user",
          status: "created",
          activity: "request"
        }
      });
      if (merchant_account.auto_payouts === 1) {
        let autoCollection = await executeAutoCollection(
          {
            merchant_account_id: merchant_account.id,
            transfer_id: res.result.transfer_id
          },
          service
        );
        return autoCollection;
      } else {
        return {
          id: res.result.transfer_id,
          payment_url: `${config.paymentPage.baseUrl}${res.result.transfer_id}`
        };
      }
    } else {
      if (
        res.error &&
        res.error.name == "SequelizeUniqueConstraintError" &&
        res.error.errors.filter((e) => e.path == "order_num").length
      ) {
        throw new service.ServiceError("BANJO_ERR_UNIQUE_ORDER_ID");
      } else {
        throw res.error;
      }
    }
  } else {
    throw new service.ServiceError("BANJO_ERR_BANK_ASSOCIATION_MISSING");
  }
}

async function payoutBankAccountCheck(data, service) {
  try {
    console.log("Sufficient balance");
    let query = `SELECT mam.id, vbapp.protocol_type, vbapp.bank_account_id, w.id as wallet_id, vbal.payment_protocol_id 
      from ${db.schema}.merchant_account_mappings mam
      left join ${db.schema}.vw_bank_acc_payment_protocols vbapp on vbapp.bank_account_id = mam.bank_account
      left join ${db.schema}.vw_bank_accounts_limits vbal on (vbapp.payment_protocol_id=vbal.payment_protocol_id and ((vbal.min_amount<=:amount or vbal.min_amount is null) and (vbal.max_amount>=:amount or vbal.max_amount is null)))
      INNER JOIN ${db.schema}.wallets w ON w.merchant_account_mapping_id=mam.id AND w.type=:walletType 
      INNER JOIN ${db.schema}.vw_bank_accounts ba on vbapp.bank_account_id = ba.id
      where mam.merchant_account_id = :merchant_account_id and vbapp.protocol_type = :protocol AND w.balance>=:amount and ba.status = 'ACTIVE' and ba.removed = 0
      order by w.balance asc`;
    let replacements = {
      merchant_account_id: data.merchant_account_id,
      amount: data.amount,
      limit_type: db.ref_limits.LIMIT_TYPES.WITHDRAW,
      walletType: db.wallet.TYPES.COLLECTION,
      protocol: data.protocol
    };

    let bankAccountAssociation = await db.sequelize.query(query, {
      raw: true,
      type: db.sequelize.QueryTypes.SELECT,
      replacements
    });

    if (!bankAccountAssociation || !bankAccountAssociation.length) {
      throw new service.ServiceError("BANJO_ERR_NO_SUPPORT_PROTOCOL");
    }
    let pickedBA,
      protocolSupportFlag = false,
      limitSupportFlag = false;
    for (let currentBA of bankAccountAssociation) {
      if (currentBA && currentBA.protocol_type == undefined) {
        protocolSupportFlag = false;
      } else {
        protocolSupportFlag = true;
        pickedBA = currentBA;
      }

      if (currentBA && currentBA.payment_protocol_id == undefined) {
        limitSupportFlag = false;
      } else {
        limitSupportFlag = true;
        pickedBA = currentBA;
      }

      if (protocolSupportFlag && limitSupportFlag) {
        break;
      }
    }

    if (protocolSupportFlag == false) {
      throw new service.ServiceError("BANJO_ERR_NO_SUPPORT_PROTOCOL");
    }
    if (limitSupportFlag == false) {
      throw new service.ServiceError("BANJO_ERR_LIMITS_DENY");
    }
    return pickedBA;
  } catch (error) {
    throw error;
  }
}
async function payoutRequest(data, service) {
  if (data.order.protocol != "IMPS") {
    throw new service.ServiceError("BANJO_UNSUPPORTED_PROTOCOL_FOR_PAYOUT");
  }
  let beneficiaryExists = await db.beneficiaries.findOne({
    where: { id: data.beneficiary_id }
  });
  if (!beneficiaryExists) {
    throw new service.ServiceError("BANJO_ERR_BENEFICIARIES_ACCOUNT_NOT_FOUND");
  }
  await Queue.newJob("account-service", {
    method: "checkValidRealmId",
    data: {},
    options: {
      realmId: service.realmId
    }
  });
  const orderIdExists = await db.transfer.findOne({
    where: { order_num: data.order.order_id }
  });
  if (orderIdExists) {
    throw new service.ServiceError("BANJO_ERR_UNIQUE_ORDER_ID");
  }
  let transfer_id = uuid();
  await db.transfers_meta.create({
    transfer_request: {
      ...data,
      service,
      merchant_account_id: service.maId,
      transfer_id,
      processed: 0,
      header: {
        method: "payoutRequest"
      }
    },
    transfer_response: {},
    transfer_id: transfer_id
  });
  Queue.newJob("transaction-service", {
    method: "callTelegramBot",
    data: {
      transfer_id: transfer_id,
      action_type: "user",
      status: "created",
      activity: "request"
    }
  });
  return {
    id: transfer_id
  };
}

async function txStatus(data, service) {
  if (data.transaction_id || data.order_id) {
    const t = await db.sequelize.query(
      `SELECT t.id           as transaction_id,
              t.order_num    as order_id,
              t.description,
              t.dst_amount   as amount,
              t.dst_currency as currency,
              t.ctime,
              (SELECT activity
                FROM ${db.schema}.transactions
                WHERE transfer_id = t.id
                  AND activity not in ('FEE','REVERSE_FEE')
                ORDER BY ctime DESC
                LIMIT 1) as status
        FROM ${db.schema}.transfers t
        WHERE ${
          data.transaction_id ? `t.id = :transferId` : `t.order_num = :orderId`
        }
          AND t.merchant_account_id = :maId
          AND t.account_id = :accountId`,
      {
        raw: true,
        type: db.sequelize.QueryTypes.SELECT,
        replacements: {
          accountId: service.accountId,
          maId: service.maId,
          transferId: data.transaction_id,
          orderId: data.order_id
        }
      }
    );
    if (t && t.length) {
      return t[0];
    } else {
      throw new service.ServiceError("BANJO_ERR_TX_NOT_FOUND");
    }
  } else {
    throw new service.ServiceError("BANJO_ERR_TX_ID_OR_ORDER_ID_MISSING");
  }
}

async function CollectionRequestH2H(data, service) {
  let collectionResponse = await collectionRequest(data, service);
  console.log(
    "collectionResponse for collection request: ",
    collectionResponse
  );

  let transferData = await Queue.newJob("transaction-service", {
    method: "txActivityPageOpened",
    data: {
      transfer_id: collectionResponse.id
    }
  });

  if (!transferData.result.success) {
    if (transferData.result.error) {
      throw transferData.result.error.message;
    }
    throw new service.ServiceError("BANJO_ERR_TX_NOT_FOUND");
  }

  let transferDataObj = {
    transfer_id: transferData.result.id,
    merchant_name: transferData.result.merchant_name,
    merchant_account_name: transferData.result.merchant_account_name,
    order_num: transferData.result.order_num,
    description: transferData.result.description,
    bank_name: transferData.result.name,
    destination_amount: transferData.result.dst_amount,
    source_amount: transferData.result.src_amount,
    source_currency: transferData.result.src_currency,
    destination_currency: transferData.result.dst_currency
  };

  let merchant_account = await db.merchant_accounts.findOne({
    where: { id: service.maId },
    attributes: ["auto_payouts", "id", "api_credential_id"]
  });
  data.merchant_account = merchant_account;

  let resultArray = [];
  if (merchant_account.auto_payouts === 1) {
    let transfer = await db.transfer.findOne({
      where: { id: transferData.result.id },
      attributes: ["data"]
    });

    let api_credentials = await db.api_credentials.findOne({
      where: {
        id: merchant_account.api_credential_id,
        removed: 0
      }
    });

    if (api_credentials) {
      let payment_category = await db.merchant_categories.findOne({
        where: {
          id: api_credentials.category,
          removed: 0
        }
      });
      if (payment_category) {
        if (payment_category.name === config.Xazur_Pay) {
          if (transfer && transfer.data.xazurpay_id) {
            const upi_link = await callPatch({
              customerIp: transfer.data.user_ip,
              secret_key: api_credentials.secret_key,
              id: transfer.data.xazurpay_id
            });
            if (upi_link && upi_link.result && upi_link.result.externalRefs) {
              resultArray = await scrapeData(
                upi_link.result.externalRefs.qrCode
              );
            } else {
              console.log(
                "File: transactions.js Function: collectionRequestH2H " +
                  transfer.data.xazurpay_id +
                  " Xazur Pay payment details not available"
              );
            }
          } else {
            console.log(
              "File: transactions.js Function: collectionRequestH2H Xazur Pay transfer not available"
            );
          }
        } else if (payment_category.name === config.DigiPayment) {
          resultArray = await createDigiPaymentOptionObjectArray(transferData);
        } else {
          console.log(
            "File: transactions.js Function: collectionRequestH2H " +
              api_credentials.category +
              " Acquirer not available"
          );
        }
      } else {
        console.log(
          "File: transactions.js Function: collectionRequestH2H " +
            payment_category +
            " payment_category not available"
        );
      }
    } else {
      console.log(
        "File: Account.js Function: executeAutoCollection Api credentials missing "
      );
    }
  } else {
    resultArray = await Promise.all(
      transferData.result.payment_options.map(async (i) =>
        createPaymentOptionObject(i, transferData)
      )
    );
  }

  transferDataObj.payment_options = resultArray;
  return transferDataObj;
}
async function createDigiPaymentOptionObjectArray(transferData) {
  try {
    let resultArray = [];
    if (
      transferData.result &&
      transferData.result.data &&
      transferData.result.data.digiPaymentBankAcc
    ) {
      let transferDataObj = transferData.result.data.digiPaymentBankAcc;
      if (
        transferDataObj.account_number !== null &&
        transferDataObj.bank_ifsc !== null
      ) {
        let wire_protocols = ["RTGS", "NEFT", "IMPS"];
        for (let wire_protocol of wire_protocols) {
          let optionDataObj = {
            account_name: transferDataObj.account_name,
            bank_name: transferDataObj.name,
            protocol_type: wire_protocol,
            account_ifsc: transferDataObj.bank_ifsc,
            account_number: transferDataObj.account_number
          };
          resultArray.push(optionDataObj);
        }
      }
      if (transferDataObj.dynamic_qr !== null) {
        const decodedUrl = decodeURIComponent(transferDataObj.dynamic_qr);

        const amount = transferData.result.src_amount; // Replace "------" with this amount

        const replacedUrl = decodedUrl.replace(/am=[^&]*/, `am=${amount}`);
        console.log("Replaced URL:", replacedUrl);
        const startIndex = replacedUrl.indexOf("data=") + 5;
        const Link = replacedUrl.slice(startIndex);
        const QRLink = encodeURI(Link);
        const url = await QRCode.toDataURL(QRLink, {
          errorCorrectionLevel: "H"
        });
        const base64Data = url.replace(/^data:image\/png;base64,/, "");
        let optionDataObj = {
          account_name: transferDataObj.account_name,
          bank_name: transferDataObj.name,
          protocol_type: "UPI",
          qrCode: base64Data,
          vpa: transferDataObj.bank_upid,
          upi_link: QRLink
        };

        resultArray.push(optionDataObj);
      }
    }
    return resultArray;
  } catch (error) {
    console.log(
      "File: transactions.js Function: createDigiPaymentOptionObjectArray Error: " +
        error
    );
    throw error;
  }
}
async function createPaymentOptionObject(option, transferData) {
  let wire_protocols = ["RTGS", "NEFT", "IMPS"];

  let optionDataObj = {
    account_name: option.account_name,
    bank_name: option.bank_name,
    protocol_type: option.protocol_type
  };
  if (option.protocol_type === "UPI") {
    let QRLink = `upi://pay?pa=${option.identifier}&pn=${transferData.result.merchant_name}&tn=${transferData.result.ref_num}&am=${transferData.result.src_amount}&cu=${transferData.result.src_currency}`;
    const url = await QRCode.toDataURL(QRLink, {
      errorCorrectionLevel: "H"
    });
    const base64Data = url.replace(/^data:image\/png;base64,/, "");
    optionDataObj.qrCode = base64Data;
    optionDataObj.vpa = option.identifier;
    optionDataObj.upi_link = QRLink;
  }
  if (wire_protocols.includes(option.protocol_type)) {
    optionDataObj.account_ifsc = option.identifier;
    optionDataObj.account_number = option.acc_no;
  }
  return optionDataObj;
}

async function transactions(data, service) {}

async function fetchMerchantAccountPayPayoutEnabledList(data, service) {
  try {
    let filter = {
      auto_payouts: 1,
      status: db.merchant_accounts.STATUS.ACTIVE
    };
    if (data.merchant_account_id) filter.id = data.merchant_account_id;
    const maResp = await db.merchant_accounts.findAll({
      where: {
        ...filter
      }
    });
    if (!maResp.length) {
      console.log(
        "File: Account.js Func:fetchMerchantAccountPayPayoutEnabledList Auto payout merchant accounts not found"
      );
    }
    return maResp;
  } catch (err) {
    console.log(
      "File: Account.js Function: fetchMerchantAccountPayPayoutEnabledList Error: " +
        err
    );
    throw err;
  }
}

async function executeAutoCollection(data, service) {
  try {
    let merchant_account = await db.merchant_accounts.findOne({
      where: { id: data.merchant_account_id }
    });
    if (!merchant_account.api_credential_id) {
      console.log(
        "File: Account.js Function: executeAutoCollection Api credentials missing "
      );
      await txActivityReject(data, service);
      return;
    }
    let api_credentials = await db.api_credentials.findOne({
      where: {
        id: merchant_account.api_credential_id,
        removed: 0
      }
    });

    if (api_credentials) {
      let payment_category = await db.merchant_categories.findOne({
        where: {
          id: api_credentials.category,
          removed: 0
        }
      });
      if (payment_category) {
        if (modules[payment_category.name]) {
          let obj = new modules[payment_category.name](
            api_credentials.aggregator_id,
            api_credentials.secret_key
          );
          if (obj && obj.executeAcquirerCollection) {
            return await obj.executeAcquirerCollection(
              data,
              service,
              api_credentials
            );
          }
        } else {
          console.log(
            "File: transactions.js Function: executeAutoCollection " +
              api_credentials.category +
              " Acquirer not available"
          );
          await txActivityReject(data, service);
        }
      } else {
        console.log(
          "File: transactions.js Function: executeAutoCollection " +
            payment_category +
            " payment_category not available"
        );
        await txActivityReject(data, service);
      }
    } else {
      console.log(
        "File: Account.js Function: executeAutoCollection Api credentials missing "
      );
      await txActivityReject(data, service);
    }
  } catch (error) {
    console.log(
      "File: transactions.js, function: executeAutoCollection, error: ",
      error
    );
    await txActivityReject(data, service);
    throw error;
  }
}

async function executeAutoPayout(data, service) {
  try {
    const maResp = await fetchMerchantAccountPayPayoutEnabledList(
      data,
      service
    );

    for (let m of maResp) {
      let query = `select vtt.activity,vtt.id,vtt.merchant_account_id,vtt.src_amount,tm.transfer_request,vtt.transfer_id,vtt.src_currency,vtt.data,vtt.src_macc_status  from ${db.schema}.vw_tx_transfer vtt
      join ${db.schema}.transfers_meta tm on tm.transfer_id = vtt.transfer_id and (tm.transfer_request->'order'->>'protocol' = 'NEFT' or tm.transfer_request->'order'->>'protocol' = 'IMPS' or tm.transfer_request->'order'->>'protocol' = 'UPI') 
      where vtt.transfer_type = 'PAYOUT' and vtt.activity= 'REQUEST' and vtt.merchant_account_id = :merchant_account_id and vtt.src_macc_status = 'ACTIVE' `;
      let replacements = { merchant_account_id: m.id };
      const transfers = await db.sequelize.query(query, {
        replacements,
        type: db.sequelize.QueryTypes.SELECT,
        raw: true
      });
      if (!!transfers && transfers.length) {
        for (let transfer of transfers) {
          data.transfer_id = transfer.transfer_id;
          if (!transfer.data.beneficiary_id) {
            console.error(
              "File transactions.js Func:executeAcquirerPayment Error beneficiary_id required for " +
                transfer.transfer_id
            );
            data.note = "Beneficiary account missing for transfer";
            await txActivityReject(data, service);
            return false;
          }
          let beneficiary = await db.beneficiaries.findOne({
            where: {
              id: transfer.data.beneficiary_id,
              merchant_account_id: transfer.merchant_account_id,
              removed: 0
            }
          });
          if (!beneficiary) {
            console.error(
              "File transactions.js Func:executeAcquirerPayment Error invalid beneficiary_id required for " +
                transfer.transfer_id
            );
            data.note = "invalid Beneficiary account";
            await txActivityReject(data, service);
            return false;
          }
          let merchant_account = await db.merchant_accounts.findOne({
            where: { id: transfer.merchant_account_id }
          });
          if (!merchant_account.api_credential_id) {
            console.log(
              "File: Account.js Function: executeAutoPayout Api credentials missing "
            );
            await txActivityReject(data, service);
            return false;
          }
          let api_credentials = await db.api_credentials.findOne({
            where: {
              id: merchant_account.api_credential_id,
              removed: 0
            }
          });

          if (api_credentials) {
            let payment_category = await db.merchant_categories.findOne({
              where: {
                id: api_credentials.category,
                removed: 0
              }
            });
            if (payment_category) {
              if (modules[payment_category.name]) {
                let obj = new modules[payment_category.name](
                  api_credentials.aggregator_id,
                  api_credentials.secret_key
                );
                if (obj) {
                  await obj.executeAcquirerPayment(
                    data,
                    service,
                    beneficiary,
                    transfer,
                    api_credentials
                  );
                }
              } else {
                console.log(
                  "File: Account.js Function: syncAutoPayout " +
                    payment_category.name +
                    " Acquirer not available"
                );
                data.note = "Payment category missing";
                await txActivityReject(data, service);
              }
            }
          } else {
            console.log(
              "File: Account.js Function: executeAutoPayout Api credentials missing "
            );
            await txActivityReject(data, service);
          }
        }
      }
    }

    return { success: true };
  } catch (error) {
    console.log("File: Account.js Function: executeAutoPayout Error: " + error);
    throw error;
  }
}

// Auto Payout cron job required this function
async function syncAutoPayout(data, service) {
  try {
    let returnData = { success: true };
    await Queue.newJob("account-service", {
      method: "checkValidRealmId",
      data: {},
      options: {
        realmId: service.realmId
      }
    });
    let query = `select vtt.activity,vtt.id,vtt.merchant_account_id,vtt.src_amount,tm.transfer_request,vtt.transfer_id,vtt.src_currency,vtt.data from ${db.schema}.vw_tx_transfer vtt
      join ${db.schema}.transfers_meta tm on tm.transfer_id = vtt.transfer_id and (tm.transfer_request->'order'->>'protocol' = 'NEFT' or tm.transfer_request->'order'->>'protocol' = 'IMPS' or tm.transfer_request->'order'->>'protocol' = 'UPI') 
      where vtt.transfer_type = 'PAYOUT'  and vtt.activity= 'PENDING' and vtt.src_macc_status = 'ACTIVE' and vtt.auto_payouts=1`;

    if (data.transfer_id) {
      query += ` and vtt.transfer_id = '${data.transfer_id}'`;
    }
    const transfers = await db.sequelize.query(query, {
      type: db.sequelize.QueryTypes.SELECT,
      raw: true
    });
    if (!!transfers && transfers.length) {
      for (let transfer of transfers) {
        data.transfer_id = transfer.transfer_id;
        let merchant_account = await db.merchant_accounts.findOne({
          where: { id: transfer.merchant_account_id }
        });
        if (!merchant_account.api_credential_id) {
          console.log(
            "File: Account.js Function: syncAutoPayout Api credentials missing "
          );
          await txActivityReject(data, service);
          return;
        }
        let api_credentials = await db.api_credentials.findOne({
          where: {
            id: merchant_account.api_credential_id,
            removed: 0
          }
        });

        if (api_credentials) {
          let payment_category = await db.merchant_categories.findOne({
            where: {
              id: api_credentials.category,
              removed: 0
            }
          });
          if (payment_category) {
            if (modules[payment_category.name]) {
              let obj = new modules[payment_category.name](
                api_credentials.aggregator_id,
                api_credentials.secret_key
              );
              if (obj) {
                returnData = await obj.syncAutoPayoutAcquirer(
                  data,
                  service,
                  transfer,
                  api_credentials
                );
              }
            } else {
              console.log(
                "File: Account.js Function: syncAutoPayout " +
                  payment_category.name +
                  " Acquirer not available"
              );
            }
          }
        } else {
          console.log(
            "File: Account.js Function: syncAutoPayout Api credentials missing "
          );
          let transaction = await Queue.newJob("transaction-service", {
            method: "txActivityReject",
            data,
            options: service
          });
          if (transaction.error && transaction.error.message) {
            console.log(
              `File: Acquirer.js Func:executeAcquirerPayment ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
            );
          }
        }
      }
    }
    return returnData;
  } catch (error) {
    console.log("File: Account.js Function: syncAutoPayout Error: " + error);
    throw error;
  }
}

async function syncAutoCollection(data, service) {
  try {
    let returnData = { success: true };

    let query = `select vtt.activity,vtt.id,vtt.merchant_account_id,vtt.src_amount,tm.transfer_request,vtt.transfer_id,vtt.src_currency, vtt.data from ${db.schema}.vw_tx_transfer vtt
      join ${db.schema}.transfers_meta tm on tm.transfer_id = vtt.transfer_id  
      where vtt.transfer_type = 'COLLECTION'  and (vtt.activity= 'REQUEST' or vtt.activity= 'BRN_SUBMIT') and vtt.src_macc_status = 'ACTIVE' and vtt.auto_payouts = 1`;

    if (data.transfer_id) {
      query += ` and vtt.transfer_id = '${data.transfer_id}'`;
    }
    const transfers = await db.sequelize.query(query, {
      type: db.sequelize.QueryTypes.SELECT,
      raw: true
    });
    if (!!transfers && transfers.length) {
      for (let transfer of transfers) {
        data.transfer_id = transfer.transfer_id;
        let merchant_account = await db.merchant_accounts.findOne({
          where: { id: transfer.merchant_account_id }
        });
        if (!merchant_account.api_credential_id) {
          console.log(
            "File: Account.js Function: syncAutoCollection Api credentials missing "
          );
          await txActivityReject(data, service);
          return;
        }
        let api_credentials = await db.api_credentials.findOne({
          where: {
            id: merchant_account.api_credential_id,
            removed: 0
          }
        });

        if (api_credentials) {
          let payment_category = await db.merchant_categories.findOne({
            where: {
              id: api_credentials.category,
              removed: 0
            }
          });
          if (payment_category) {
            if (modules[payment_category.name]) {
              let obj = new modules[payment_category.name](
                api_credentials.aggregator_id,
                api_credentials.secret_key
              );
              if (obj) {
                returnData = await obj.syncAutoCollectionAcquirer(
                  data,
                  service,
                  transfer,
                  api_credentials
                );
              }
            } else {
              console.log(
                "File: Account.js Function: syncAutoCollection " +
                  payment_category.name +
                  " Acquirer not available"
              );
            }
          }
        } else {
          console.log(
            "File: Account.js Function: syncAutoCollection Api credentials missing "
          );
          let transaction = await Queue.newJob("transaction-service", {
            method: "txActivityReject",
            data,
            options: service
          });
          if (transaction.error && transaction.error.message) {
            console.log(
              `File: Acquirer.js Func:executeAcquirerPayment ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
            );
          }
        }
      }
    }
    return returnData;
  } catch (error) {
    console.log(
      "File: Account.js Function: syncAutoCollection Error: " + error
    );
    throw error;
  }
}

async function txActivityReject(data, service) {
  try {
    let transaction = await Queue.newJob("transaction-service", {
      method: "txActivityReject",
      data,
      options: service
    });
    if (transaction.error && transaction.error.message) {
      console.error(
        `File: Acquirer.js Func:executeAcquirerPayment ${data.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
      );
    }
  } catch (error) {
    console.error("File: Acquirer.js Func:txActivityReject Error : " + error);
  }
}

async function xazurpayStatus(data, service) {
  try {
    let transfer = await db.sequelize.query(
      `SELECT *
      FROM ${db.schema}.transfers
      WHERE data->>'xazurpay_id' = :xazurpay_id;
    `,
      {
        replacements: {
          xazurpay_id: data.result.id
        }
      }
    );
    if (!transfer && transfer.length) {
      console.log("transfer not found for request:", data);
      return;
    }
    transfer = transfer[0][0];
    data.transfer_id = transfer.id;
    if (
      data &&
      data.status === 200 &&
      ["CHECKOUT", "PENDING"].indexOf(data.result.state) >= 0
    ) {
      return {
        success: true,
        message: `Transfer is in pending state please try again later`
      };
    } else if (data && data.result.state === "COMPLETED") {
      await db.transaction.update(
        { activity: "APPROVED" },
        {
          where: {
            transfer_id: transfer.id
          }
        }
      );
      setTimeout(async () => {
        try {
          await Queue.newJob("transaction-service", {
            method: "sendWebhook",
            data: {
              transfer_id: transfer.id,
              type: db.tx_webhooks.TYPE.COLLECTION_UPDATES
            },
            options: service
          });
        } catch (err) {
          console.error("Error while sending transaction webhook", err);
        }
      });
    } else {
      data.activity_action = "REVERSE_TX";
      let transaction = await Queue.newJob("transaction-service", {
        method: "txActivityReverse",
        data,
        options: service
      });

      if (transaction.error && transaction.error.message) {
        console.log(
          `File: Acquirer.js Func:executeAutoPayout ${transfer.transfer_id} ${transaction.error.message} has failed to be marked. Please try again`
        );
        return;
      }
      await db.transaction.update(
        { activity: "REJECTED" },
        {
          where: {
            transfer_id: transfer.transfer_id,
            activity: "PENDING"
          }
        }
      );
      setTimeout(async () => {
        try {
          await Queue.newJob("transaction-service", {
            method: "sendWebhook",
            data: {
              transfer_id: transfer.transfer_id,
              type: db.tx_webhooks.TYPE.COLLECTION_UPDATES
            },
            options: service
          });
        } catch (err) {
          console.error("Error while sending transaction webhook", err);
        }
      });
    }
  } catch (error) {
    console.log(
      "File: Account.js Function: syncAutoCollection Error: " + error
    );
    throw error;
  }
}
async function digiPaymentStatus(data, service) {
  try {
    let transfer = await db.sequelize.query(
      `SELECT t.*,(SELECT tx.activity FROM ${db.schema}.transactions tx WHERE transfer_id = t.id and tx.activity not in ('FEE','REVERSE_FEE') ORDER BY tx.ctime DESC LIMIT 1) as status
      FROM ${db.schema}.transfers t
      WHERE t.id = :transfer_id
    `,
      {
        replacements: {
          transfer_id: data.referenceId
        }
      }
    );
    if (!transfer && transfer.length) {
      console.log("transfer not found for request:", data);
      return;
    }
    transfer = transfer[0][0];
    data.transfer_id = transfer.id;
    if (data && data.status === "2") {
      if (transfer.transfer_type === "PAYOUT") {
        await db.transaction.update(
          { activity: "APPROVED" },
          {
            where: {
              transfer_id: transfer.id
            }
          }
        );
        setTimeout(async () => {
          try {
            await Queue.newJob("transaction-service", {
              method: "sendWebhook",
              data: {
                transfer_id: transfer.id,
                type: db.tx_webhooks.TYPE.COLLECTION_UPDATES
              },
              options: service
            });
          } catch (err) {
            console.error("Error while sending transaction webhook", err);
          }
        });
      } else {
        let transaction = await Queue.newJob("transaction-service", {
          method: "txActivityApprove",
          data: {
            activity_action: "APPROVE_TX",
            transfer_id: transfer.id,
            note: data.remark
          },
          options: service
        });
        if (transaction.error && transaction.error.message) {
          console.log(
            `File: transactions.js Func:digiPaymentStatus ${transfer.id} ${transaction.error.message} has failed to be marked. Please try again`
          );
          return;
        }
      }
    } else if (data && data.status === "3") {
      if (transfer.transfer_type === "PAYOUT") {
        let transaction = await Queue.newJob("transaction-service", {
          method: "txActivityReverse",
          data: {
            activity_action: "REVERSE_TX",
            transfer_id: transfer.id,
            note: data.remark
          },
          options: service
        });

        if (transaction.error && transaction.error.message) {
          console.log(
            `File: transactions.js Func:digiPaymentStatus ${transfer.id} ${transaction.error.message} has failed to be marked. Please try again`
          );
          return;
        }
        await db.transaction.update(
          { activity: "REJECTED" },
          {
            where: {
              transfer_id: transfer.id,
              activity: "PENDING"
            }
          }
        );
        setTimeout(async () => {
          try {
            await Queue.newJob("transaction-service", {
              method: "sendWebhook",
              data: {
                transfer_id: transfer.id,
                type: db.tx_webhooks.TYPE.COLLECTION_UPDATES
              },
              options: service
            });
          } catch (err) {
            console.error("Error while sending transaction webhook", err);
          }
        });
      } else {
        let transaction = await Queue.newJob("transaction-service", {
          method: "txActivityReject",
          data: {
            activity_action: "REJECT_TX",
            transfer_id: transfer.id,
            note: data.remark
          },
          options: service
        });
        if (transaction.error && transaction.error.message) {
          console.log(
            `File: transactions.js Func:digiPaymentStatus ${transfer.id} ${transaction.error.message} has failed to be marked. Please try again`
          );
          return;
        }
      }
    } else {
      console.log(
        `File: transactions.js Func:digiPaymentStatus Transfer is in pending state please try again later`
      );
      return {
        success: true,
        message: `Transfer is in pending state please try again later`
      };
    }
  } catch (error) {
    console.log(
      "File: transactions.js Function: digiPaymentStatus Error: " + error
    );
    throw error;
  }
}
async function checkCreatePayoutRequest(data, service) {
  try {
    const [walletAccounts] = await Promise.all([
      getFetchWalletsAccount(data.bank_account_id, data.currency, service.maId)
    ]);
    let total_bank_amount = data.amount;
    for (const wallet of walletAccounts) {
      if (total_bank_amount <= 0) break;

      let amount =
        wallet.balance - total_bank_amount ? wallet.balance : total_bank_amount;
      const res = await checkPayoutRequest({ ...data, amount }, service);
      total_bank_amount -= wallet.balance;
    }
  } catch (error) {
    throw error;
  }
}
async function createPayoutRequest(data, service) {
  try {
    let result = null;
    let total_bank_amount;
    for (let current of data.basic_info.payout_data) {
      await checkCreatePayoutRequest(
        {
          ...current,
          ...data.basic_info,
          is_settlement_payout_trigger: data.is_settlement_payout_trigger,
          order: {
            protocol: current.payment_protocol
          }
        },
        service
      );
    }
    for (let current of data.basic_info.payout_data) {
      const [walletAccounts] = await Promise.all([
        getFetchWalletsAccount(
          current.bank_account_id,
          current.currency,
          service.maId
        )
      ]);

      total_bank_amount = current.amount;
      for (const wallet of walletAccounts) {
        if (total_bank_amount <= 0) break;

        let amount =
          wallet.balance - total_bank_amount
            ? total_bank_amount
            : wallet.balance;

        result = await settlementPayoutRequest(
          {
            ...current,
            ...data.basic_info,
            is_settlement_payout_trigger: data.is_settlement_payout_trigger,
            order: {
              protocol: current.payment_protocol
            },
            amount,
            wallet_id: wallet.id,
            action_type: data.action_type,
            admin_id: data.admin_id
          },
          service
        );
        total_bank_amount -= wallet.balance;
      }
    }
    return {
      ...result,
      message: "Settlement payout request created successfully"
    };
  } catch (error) {
    console.log("transactions.js Function:createPayoutRequest error: " + error);
    throw error;
  }
}
async function getFetchWalletsAccount(
  bank_account_id,
  currency,
  merchant_account_id
) {
  let query = `SELECT w.id , w.currency , w.merchant_account_mapping_id,w.balance  FROM  ${db.schema}.wallets w 
            join ${db.schema}.vw_merchant_account_mappings mam on mam.id = w.merchant_account_mapping_id and mam.merchant_account_id = :merchant_account_id
            where w.removed = 0 and w.type = :type and w.currency= :currency and w.bank_account_id = :bank_account_id and w.balance>0 order by w.ctime`;
  try {
    let replacements = {
      currency,
      bank_account_id,
      merchant_account_id
    };
    const acc = await db.sequelize.query(query, {
      replacements: {
        ...replacements,
        type: db.wallet.TYPES.WALLET
      },
      raw: true,
      type: db.sequelize.QueryTypes.SELECT
    });

    return acc;
  } catch (error) {
    throw error;
  }
}

async function settlementPayoutRequest(data, service) {
  let pickedBA = await checkPayoutRequest(data, service);
  await Queue.newJob("account-service", {
    method: "checkValidRealmId",
    data: {},
    options: {
      realmId: service.realmId
    }
  });

  let hasSufficientBalance = await _hasSufficientBalance(data, service);
  if (!hasSufficientBalance) {
    throw new service.ServiceError("BANJO_ERR_INSUFFICIENT_BALANCE");
  }
  const res = await Queue.newJob("transaction-service", {
    method: data.is_settlement_payout_trigger
      ? "settlementPayoutTrigger"
      : "payoutTrigger",
    data: {
      description: data.order.description,
      action: "PAYOUT_REQUEST",
      currency: data.currency,
      amount: data.amount,
      order_id: data.order.order_id,
      bank_account_id: pickedBA.bank_account_id,
      wallet_id:
        data.is_settlement_payout_trigger && data.wallet_id
          ? data.wallet_id
          : pickedBA.wallet_id,
      data: {
        ua: service.ua,
        ip: service.ip,
        user_ip: data.user_ip
      }
    },
    options: {
      ...service
    }
  });
  if (res.result) {
    if (!res.result.transfer_id) {
      throw new service.ServiceError("BANJO_ERR_TARIFF_CONFIG");
    }

    let transfer = await db.vw_tx_transfer.findOne({
      where: { transfer_id: res.result.transfer_id },
      attributes: ["activity"]
    });

    await db.transfers_meta.create({
      transfer_request: data,
      transfer_response: res.result,
      transfer_id: res.result.transfer_id
    });
    Queue.newJob("transaction-service", {
      method: "callTelegramBot",
      data: {
        transfer_id: res.result.transfer_id,
        action_type: data.action_type || "user",
        admin_id: data.admin_id,
        status: "created",
        activity: "request"
      }
    });
    return {
      id: res.result.transfer_id,
      status: transfer.activity
    };
  } else {
    if (
      res.error &&
      res.error.name == "SequelizeUniqueConstraintError" &&
      res.error.errors.filter((e) => e.path == "order_num").length
    ) {
      throw new service.ServiceError("BANJO_ERR_UNIQUE_ORDER_ID");
    } else {
      throw res.error;
    }
  }
}

async function _hasSufficientBalance(data, service) {
  let collectionBalance = await db.sequelize.query(
    `SELECT coalesce(sum(w.balance),0) as total_available_balance FROM  ${db.schema}.wallets w 
            join ${db.schema}.vw_merchant_account_mappings mam on mam.id = w.merchant_account_mapping_id and mam.merchant_account_id = :merchant_account_id
            where w.removed = 0 and w.type = :walletType and w.currency= :src_currency`,
    {
      raw: true,
      type: db.sequelize.QueryTypes.SELECT,
      replacements: {
        merchant_account_id: service.maId,
        walletType: data.is_settlement_payout_trigger
          ? db.wallet.TYPES.WALLET
          : db.wallet.TYPES.COLLECTION,
        src_currency: data.currency
      }
    }
  );
  return collectionBalance[0].total_available_balance >= data.amount;
}

async function checkPayoutRequest(data, service) {
  try {
    let query = `select mam.id, vbapp.protocol_type, vbapp.bank_account_id, w.id as wallet_id, vbal.payment_protocol_id from ${db.schema}.merchant_account_mappings mam
left join ${db.schema}.vw_bank_acc_payment_protocols vbapp on vbapp.bank_account_id = mam.bank_account
left join ${db.schema}.vw_bank_accounts_limits vbal on (vbapp.payment_protocol_id=vbal.payment_protocol_id and ((vbal.min_amount<=:amount or vbal.min_amount is null) and (vbal.max_amount>=:amount or vbal.max_amount is null) ))
INNER JOIN ${db.schema}.wallets w ON w.merchant_account_mapping_id=mam.id AND w.type=:walletType
where mam.merchant_account_id = :merchant_account_id and vbapp.protocol_type = :protocol`;
    let replacements = {
      merchant_account_id: service.maId,
      amount: data.amount,
      limit_type: db.ref_limits.LIMIT_TYPES.WITHDRAW,
      walletType: data.is_settlement_payout_trigger
        ? db.wallet.TYPES.WALLET
        : db.wallet.TYPES.COLLECTION,
      protocol: data.order.protocol
    };

    if (data.bank_account_id) {
      replacements.bank_account_id = data.bank_account_id;
      query += " and vbapp.bank_account_id= :bank_account_id and w.balance>0";
    }
    let bankAccountAssociation = await db.sequelize.query(query, {
      raw: true,
      type: db.sequelize.QueryTypes.SELECT,
      replacements
    });

    if (!bankAccountAssociation || !bankAccountAssociation.length) {
      throw new service.ServiceError("BANJO_ERR_NO_SUPPORT_PROTOCOL");
    }
    let pickedBA,
      protocolSupportFlag = false,
      limitSupportFlag = false;
    for (let currentBA of bankAccountAssociation) {
      if (currentBA && currentBA.protocol_type == undefined) {
        protocolSupportFlag = false;
      } else {
        protocolSupportFlag = true;
        pickedBA = currentBA;
      }

      if (currentBA && currentBA.payment_protocol_id == undefined) {
        limitSupportFlag = false;
      } else {
        limitSupportFlag = true;
        pickedBA = currentBA;
      }

      if (protocolSupportFlag && limitSupportFlag) {
        break;
      }
    }

    if (protocolSupportFlag == false) {
      throw new service.ServiceError("BANJO_ERR_NO_SUPPORT_PROTOCOL");
    }
    if (limitSupportFlag == false) {
      throw new service.ServiceError("BANJO_ERR_LIMITS_DENY_PAYOUT");
    }
    return pickedBA;
  } catch (error) {
    throw error;
  }
}

async function fetchBankAccount(data, service) {
  let protocolFilter = [],
    filterSqlProtocol = ``,
    filterSqlLimits = ``,
    filterSqlBankAccount = ``,
    filterIsVerifiedBankAccount = ``,
    bankAccountAssociation = ``,
    bankTypeSql = ``,
    collectionSql;
  if (data && data.protocols && Array.isArray(data.protocols)) {
    for (let currentProtocol of data.protocols) {
      if (currentProtocol && typeof currentProtocol == "string") {
        protocolFilter.push(currentProtocol);
      }
    }
  }
  let replacements = {
    accountId: service.accountId,
    maId: service.maId,
    currency: data.currency,
    walletType: db.wallet.TYPES.COLLECTION
  };
  let subQuery = ``;
  if (service.realmId) {
    replacements.realm_id = service.realmId;
    bankTypeSql += ` and b.realm_id = :realm_id `;
    subQuery += ` and mam.realm_id = :realm_id `;
    filterSqlLimits += ` and vbal.realm_id = :realm_id `;
  }

  if (data && !isNaN(data.amount)) {
    filterSqlLimits += `and ((vbal.min_amount<=:amount or vbal.min_amount is null) and (vbal.max_amount>=:amount or vbal.max_amount is null))`;
    replacements.amount = data.amount;
  }
  if (protocolFilter && protocolFilter.length) {
    replacements.protocolFilter = protocolFilter;
    filterSqlProtocol += `and vbapp.protocol_type in (:protocolFilter)`;
  }
  if (data && data.bank_account_id) {
    filterSqlBankAccount += ` and ba.id = :bank_account_id`;
    replacements.bank_account_id = data.bank_account_id;
  }
  if ("isVerified" in data) {
    replacements.isVerified = data.isVerified;
    filterIsVerifiedBankAccount = ` and ba.is_verified = :isVerified `;
  }
  if (data.bank_type) {
    replacements.bank_type = data.bank_type;
    bankTypeSql += ` and b.bank_type = :bank_type`;
  } else {
    replacements.bank_type = 0;
    bankTypeSql += ` and b.bank_type = :bank_type`;
  }
  collectionSql = `SELECT mam.realm_id, mam.bank_account_id,w.id as wallet_id,w.currency as wallet_currency,
        vbapp.protocol_type, vbal.min_amount, vbal.max_amount, vbal.payment_protocol_id, mam.merchant_id, mam.merchant_account_id,
        vlimit.daily_deposit, vbal.daily_deposit as daily_deposit_limit
        FROM ${db.schema}.vw_merchant_account_mappings mam
        inner join ${db.schema}.bank_accounts ba on (ba.id=mam.bank_account_id and ba.removed=0 and ba.status='ACTIVE' ${filterSqlBankAccount} ${filterIsVerifiedBankAccount})
        inner join ${db.schema}.banks b on (ba.bank_id=b.id and b.removed=0 ${bankTypeSql})
          INNER JOIN ${db.schema}.wallets w ON w.merchant_account_mapping_id=mam.id AND w.type=:walletType
          left join ${db.schema}.vw_bank_acc_payment_protocols vbapp on (mam.bank_account_id=vbapp.bank_account_id ${filterSqlProtocol})
          inner join ${db.schema}.vw_bank_accounts_limits vbal on (vbapp.payment_protocol_id=vbal.payment_protocol_id ${filterSqlLimits} and vbal.limit_type=0 and mam.bank_account_id = vbal.bank_account_id)
          left join ${db.schema}.vw_bank_acc_limits vlimit on (mam.bank_account_id=vlimit.id)
        WHERE merchant_id = :accountId
          AND merchant_account_id = :maId
          AND bank_account_currency = :currency
          AND active_relation = true
          ${subQuery}
          ORDER BY
          case when (mam.bank_account_request_timestamp is not null) then ((extract(epoch from now())-extract(epoch from mam.bank_account_request_timestamp))*1) else extract(epoch from now()) end desc`;
  bankAccountAssociation = await db.sequelize.query(collectionSql, {
    raw: true,
    type: db.sequelize.QueryTypes.SELECT,
    replacements: replacements
  });
  if (bankAccountAssociation && bankAccountAssociation.length) {
    let pickedBA,
      protocolSupportFlag = false,
      limitSupportFlag = false, // Maximum single Tx Amount limit per protocol flag
      dailyLimitFlag = false; // Daily total limit per protocol flag
    for (let currentBA of bankAccountAssociation) {
      if (currentBA && currentBA.protocol_type == undefined) {
        protocolSupportFlag = false;
      } else {
        protocolSupportFlag = true;
        pickedBA = currentBA;
      }
      if (
        !isNaN(currentBA.daily_deposit_limit) &&
        currentBA.daily_deposit + data.amount <= currentBA.daily_deposit_limit
      ) {
        dailyLimitFlag = true;
      }

      if (currentBA && currentBA.payment_protocol_id == undefined) {
        limitSupportFlag = false;
      } else {
        limitSupportFlag = true;
        pickedBA = currentBA;
      }

      if (protocolSupportFlag && limitSupportFlag) {
        break;
      }
    }
    if (protocolSupportFlag == false) {
      throw new service.ServiceError("BANJO_ERR_NO_SUPPORT_PROTOCOL");
    }
    if (limitSupportFlag == false) {
      throw new service.ServiceError("BANJO_ERR_LIMITS_DENY");
    }
    if (dailyLimitFlag == false) {
      throw new service.ServiceError("BANJO_ERR_DAILY_LIMIT");
    }
    if (pickedBA && protocolSupportFlag && limitSupportFlag) {
      bankAccountAssociation = pickedBA;
    } else {
      throw new service.ServiceError("BANJO_NO_AVAIL_ACC");
    }
  } else if ("isVerified" in data && !data.isVerified) {
    return null;
  } else {
    throw new service.ServiceError("BANJO_ERR_BANK_ASSOCIATION_MISSING");
  }
  return bankAccountAssociation;
}

async function fetchAcquirerBankAccount(data, service) {
  try {
    const filter = {};
    let subQuery = ``;
    if (service.realmId) {
      filter.realm_id = service.realmId;
      subQuery = ` and banks.realm_id = :realm_id`;
    }
    const bank_account = await db.sequelize.query(
      `SELECT bank_accounts.*, wallets.id AS wallet_id, bank_accounts.id as bank_account_id
        FROM ${db.schema}.merchant_account_mappings
        JOIN ${db.schema}.bank_accounts ON merchant_account_mappings.bank_account = bank_accounts.id
        JOIN ${db.schema}.banks ON bank_accounts.bank_id = banks.id
        LEFT JOIN ${db.schema}.wallets ON wallets.merchant_account_mapping_id = merchant_account_mappings.id AND wallets.type = :walletType
        WHERE merchant_account_mappings.merchant_account_id = :merchant_account_id
          AND banks.bank_type = 2;
      `,
      {
        replacements: {
          ...filter,
          merchant_account_id: data.merchant_account.id,
          walletType: db.wallet.TYPES.COLLECTION
        }
      }
    );

    if (bank_account && bank_account.length) {
      return bank_account[0][0];
    } else {
      throw new service.ServiceError("BANJO_ERR_BANK_ASSOCIATION_MISSING");
    }
  } catch (error) {
    throw error;
  }
}

async function createDeposit(data, service) {
  const txControl = await db.sequelize.transaction();
  try {
    await Queue.newJob("account-service", {
      method: "checkValidRealmId",
      data: {},
      options: {
        realmId: service.realmId
      }
    });
    const checkBrn = await db.transaction.findOne({
      where: {
        brn: data.brn
      }
    });
    if (!!checkBrn) {
      throw new service.ServiceError("BANJO_ERR_INVALID_BANK_REFERENCE_NUMBER");
    }
    let account = await db.account.findOne({
      where: { id: data.account, removed: 0 }
    });
    const exchangeLedgerWallet = await Wallet.getCreateLedgerAccount(
      data.src_bank_acc_id,
      data.src_currency,
      service
    );
    let exchange_id = null;
    let collection;
    if (data.src_currency !== data.dst_currency) {
      const srcLedgerAccount = await Wallet.checkSrcLedgerAccount(
        {
          currency: data.dst_currency,
          bank_account_id: data.src_bank_acc_id
        },
        service
      );
      collection = await Wallet.getCreateCollectionAccount(
        srcLedgerAccount.id,
        data.dst_currency,
        data.merchant_account_id
      );
      const exchanges = await db.exchange.create(
        {
          ...data,
          applied_rate: data.conversion_rate,
          actual_rate: data.conversion_rate,
          ledger_wallet_id: exchangeLedgerWallet.id,
          realm_id: service.realmId
        },
        {
          transaction: txControl
        }
      );
      exchange_id = exchanges.id;
    } else {
      collection = await Wallet.getCreateCollectionAccount(
        data.src_bank_acc_id,
        data.dst_currency,
        data.merchant_account_id
      );
    }
    const collectionWalletId = await fetchBankAccount(
      {
        amount: data.dst_amount,
        bank_account_id: collection.bank_account_id,
        currency: data.dst_currency,
        bank_type: 1
      },
      { ...service, maId: data.merchant_account_id, accountId: data.account }
    );
    let response = null;
    if (!!account) {
      let d = {
        dst_wallet_id: collectionWalletId.wallet_id,
        src_wallet_id: exchangeLedgerWallet.id,
        src_currency: data.src_currency,
        src_amount: Number(data.src_amount),
        dst_currency: data.dst_currency,
        dst_amount: Number(data.dst_amount),
        merchant_account_id: data.merchant_account_id,
        realm_id: account.realm_id,
        account_id: data.account,
        bank_account_id: collection.bank_account_id,
        exchange_id,
        is_exchange: !!exchange_id,
        data: {
          ua: service.ua,
          ip: service.ip,
          user_ip: data.user_ip,
          src_currency: data.dst_currency,
          src_bank_account_id: data.src_bank_acc_id,
          src_amount: Number(data.src_amount)
        }
      };

      const res = await Queue.newJob("transaction-service", {
        method: "directDepositTrigger",
        data: {
          action: "DEPOSIT_REQUEST",
          ...d
        },
        options: {
          ...service,
          maId: data.merchant_account_id,
          accountId: data.account,
          realm_id: account.realm_id
        }
      });
      if (res && res.result) {
        if (!res.result.transfer_id) {
          throw new service.ServiceError("BANJO_ERR_TARIFF_CONFIG");
        }
        response = await Queue.newJob("transaction-service", {
          method: "txActivityApprove",
          data: {
            transfer_id: res.result.transfer_id,
            brn: data.brn,
            fee: data.fee,
            note: data.note
          },
          options: {
            ...service,
            maId: data.merchant_account_id,
            accountId: data.account,
            realm_id: account.realm_id
          }
        });
      }
    }
    txControl.commit();
    return response;
  } catch (error) {
    console.log("File: transactions.js func: createDeposit Error: " + error);
    await txControl.rollback();
    throw error;
  }
}
function callPatch(data) {
  let secretKey = Acquirer.decrypt(data.secret_key);
  const apiUrl = process.env.XAZURPAY_PAYOUT_BASE_URL + `/${data.id}`;
  const requestData = {
    customerIp: data.customerIp
  };

  const options = {
    method: "PATCH",
    url: apiUrl,
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${secretKey}`
    },
    data: requestData
  };

  return new Promise((resolve, reject) => {
    axios(options)
      .then((response) => {
        console.log(response.data);
        if (response.data) {
          resolve(response.data);
        } else {
          reject(response);
        }
      })
      .catch((error) => {
        reject(error.message);
      });
  });
}
async function scrapeData(upi_link) {
  const vpaRegex = /pa=([^&]+)/;
  const vpaMatch = upi_link.match(vpaRegex);
  const vpa = vpaMatch ? vpaMatch[1] : null;
  const qrUrl = await QRCode.toDataURL(upi_link, {
    errorCorrectionLevel: "H"
  });
  const base64Data = qrUrl.replace(/^data:image\/png;base64,/, "");
  return [{ protocol_type: "UPI", upi_link, vpa, qrCode: base64Data }];
}

async function getSvgElementFromUrl(url) {
  const browser = await puppeteer.launch({
    executablePath: config.chromium_path,
    headless: true,
    args: [
      "--disable-gpu",
      "--disable-dev-shm-usage",
      "--disable-setuid-sandbox",
      "--no-sandbox"
    ]
  });
  const page = await browser.newPage();

  await page.goto(url);
  await page.waitForSelector("#content");

  // Extract the rendered HTML content from the page
  const htmlContent = await page.content();
  let decodedText;
  try {
    decodedText = await getThirdSvgFromHtml(htmlContent);
    console.log("Decoded text:", decodedText);
  } catch (error) {
    console.error("Error:", error);
  }

  await browser.close();

  return decodedText;
}

async function getThirdSvgFromHtml(htmlString) {
  const $ = cheerio.load(htmlString);
  const svgTags = $("svg");

  if (svgTags.length >= 3) {
    const thirdSvgTag = svgTags.eq(2).toString();

    // Convert SVG to PNG buffer
    const pngBuffer = await new Promise((resolve, reject) => {
      svg2img(thirdSvgTag, { format: "png" }, (error, buffer) => {
        if (error) {
          reject(error);
        } else {
          resolve(buffer);
        }
      });
    });

    // Load the PNG buffer as an image with Jimp
    const image = await Jimp.read(pngBuffer);

    // Initialize the QR code reader
    const qr = new QrCode();

    // Decode the QR code from the image
    const decodedText = await new Promise((resolve, reject) => {
      qr.callback = (error, result) => {
        if (error) {
          reject(error);
        } else {
          resolve(result.result);
        }
      };

      qr.decode(image.bitmap);
    });

    return decodedText;
  } else {
    throw new Error("No matching SVG found");
  }
}
async function executeLostTransactions(data, service) {
  const txControl = await db.sequelize.transaction();
  try {
    await Queue.newJob("account-service", {
      method: "checkValidRealmId",
      data: {},
      options: {
        realmId: service.realmId
      }
    });
    const checkBrn = await db.transaction.findOne({
      where: {
        brn: data.brn
      }
    });
    if (!!checkBrn) {
      throw new service.ServiceError(
        "BANJO_ERR_INVALID_BANK_REFERENCE_NUMBER_LOST_TRANSACTION"
      );
    }
    let account = await db.account.findOne({
      where: { id: data.account_id, removed: 0 }
    });
    let merchant_account = await db.merchant_accounts.findOne({
      where: { id: data.merchant_account_id, removed: 0 }
    });

    let response = null;
    if (!!account && !!merchant_account) {
      const collectionWalletId = await fetchBankAccount(
        {
          amount: data.amount,
          bank_account_id: data.bank_account_id,
          currency: merchant_account.currency
        },
        {
          ...service,
          maId: data.merchant_account_id,
          accountId: data.account_id
        }
      );
      if (collectionWalletId) {
        const res = await Queue.newJob("transaction-service", {
          method: "collectionTrigger",
          data: {
            description: data.note,
            action: "COLLECTION_REQUEST",
            currency: merchant_account.currency,
            amount: Number(data.amount),
            bank_account_id: collectionWalletId.bank_account_id,
            wallet_id: collectionWalletId.wallet_id,
            data: {
              ua: service.ua,
              ip: service.ip,
              user_ip: data.user_ip,
              user_id: data.user_id
            }
          },
          options: {
            ...service,
            maId: data.merchant_account_id,
            accountId: data.account_id,
            realm_id: account.realm_id
          }
        });

        if (res && res.result) {
          if (!res.result.transfer_id) {
            throw new service.ServiceError("BANJO_ERR_TARIFF_CONFIG");
          }
          response = await Queue.newJob("transaction-service", {
            method: "txActivityApprove",
            data: {
              transfer_id: res.result.transfer_id,
              brn: data.brn,
              note: data.note
            },
            options: {
              ...service,
              maId: data.merchant_account_id,
              accountId: data.account_id,
              realm_id: account.realm_id
            }
          });
        }
      }
    }

    txControl.commit();
    return response.result;
  } catch (error) {
    console.log("File: transactions.js func: createDeposit Error: " + error);
    await txControl.rollback();
    throw error;
  }
}
export default {
  collectionRequest,
  payoutRequest,
  txStatus,
  transactions,
  CollectionRequestH2H,
  payoutBankAccountCheck,
  syncAutoPayout,
  executeAutoPayout,
  syncAutoCollection,
  xazurpayStatus,
  digiPaymentStatus,
  createPayoutRequest,
  fetchAcquirerBankAccount,
  txActivityReject,
  createDeposit,
  executeAutoCollection,
  executeLostTransactions,
  executeAutoCollection
};
